# Dependencies as package.json
FROM alpine:latest as dependencies


# Build. Do not use alpine here. Node-sass will not work
FROM node:14-alpine as build
RUN echo fs.inotify.max_user_watches=524288 | tee -a /etc/sysctl.conf

ADD . ./app

WORKDIR /app

RUN npm install --global npm@7.10.0
RUN npm ci --legacy-peer-deps --network-timeout 1000000
# RUN npm ci --network-timeout 1000000

COPY . ./
