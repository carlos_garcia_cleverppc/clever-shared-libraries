import React from "react";

import { SnackbarProvider } from "notistack";

import Demo from "./demo";
import Provider from "clever-react/ui/Provider";
import ReduxProvider from "clever-react/ui/Provider/extensions/redux";
import AnalyticsProvider from "clever-react/ui/Provider/extensions/analytics";
import MixpanelProvider from "clever-react/ui/Provider/extensions/mixpanel";
import SmartlookProvider from "clever-react/ui/Provider/extensions/smartlook";
import StickyNotification from "clever-react/ui/StickyNotification";

const App = () => (
  <ReduxProvider>
    <AnalyticsProvider analyticsId="SAMPLEID" sandbox>
      <MixpanelProvider mixpanelId="SAMPLEID" sandbox>
        <SmartlookProvider smartlookId="SAMPLEID" sandbox>
          <Provider theme={{}}>
            <SnackbarProvider
              anchorOrigin={{ vertical: "top", horizontal: "right" }}
              content={(key, props) => (
                <StickyNotification id={key} {...props} />
              )}
            >
              <Demo />
            </SnackbarProvider>
          </Provider>
        </SmartlookProvider>
      </MixpanelProvider>
    </AnalyticsProvider>
  </ReduxProvider>
);

export default App;
