import React from "react";
import Typography from "clever-react/ui/Typography";
import BorderedBlock from "clever-react/ui/BorderedBlock";
import Button from "clever-react/ui/Button";
import Link from "clever-react/ui/Link";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  footerText: { ...spacing["mr-1"], display: "inline" },
  container: { ...spacing["mb-2"] },
}));

const BorderedBlocks = () => {
  const classes = getClasses();

  return (
    <>
      <Typography
        variant="h2"
        shade={900}
        className={classes.title}
        gutterBottom
      >
        Bordered Blocks
      </Typography>

      <BorderedBlock
        image="/assets/img/block-example.svg"
        title="¡Buen trabajo! Continúa así…"
        description="Desde que comenzaste tus campañas no para de mejorar la calidad del tráfico que llega a tu tienda. Tener potenciales clientes es la clave para conseguir más ventas."
        footerContent={
          <>
            <Typography variant="body0" className={classes.footerText}>
              Si la tendencia del tráfico es decreciente puedes probar a
              incrementar la inversion y explorar nuevas nuevas plataformas que
              no estas trabajando como Google Ads.
            </Typography>

            <Link onClick={() => console.log("click_link")}>
              <Typography variant="body0" color="primary" weight="bold">
                Crear Campañas...
              </Typography>
            </Link>
          </>
        }
        borderColor="secondary"
        actionOnClick={() => console.log("click_link")}
        actionText="Go to shopping campaigns"
        quantityImage={1876}
        textImage="CONVERSIONS"
        textBadge="STILL LEARNING..."
        content={
          <Button onClick={() => console.log("click_button")}>button</Button>
        }
      >
        <div className={classes.container}>
          <Typography variant="body0" gutterBottom>
            Conoce como tus clientes interactúan con tu web.
          </Typography>

          <Button onClick={() => console.log("click_link")}>
            Go to analytics
          </Button>
        </div>
      </BorderedBlock>
    </>
  );
};

export default BorderedBlocks;
