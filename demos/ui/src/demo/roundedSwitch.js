/* eslint-disable no-console */
import React, { useState } from "react";

import Typography from "clever-react/ui/Typography";
import RoundedSwitch from "clever-react/ui/RoundedSwitch";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({ root: { width: "400px" } }));

const RoundedSwitches = () => {
  const classes = getClasses();

  const [checked, setChecked] = useState(false);

  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        Rounded Switch
      </Typography>

      <RoundedSwitch
        className={classes.root}
        checked={checked}
        options={["All", "Clever"]}
        color="secondary"
        buttonWidth="200px"
        onChange={() => {
          setTimeout(() => console.log("Hello"), 3000);
          setChecked(!checked);
        }}
      />
    </div>
  );
};

export default RoundedSwitches;
