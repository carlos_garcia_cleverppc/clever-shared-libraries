import React, { useState } from "react";

import Grid from "clever-react/ui/Grid";
import makeStyles from "clever-react/ui/styles/makeStyles";
import FeedbackThumb from "clever-react/ui/FeedbackThumb";
import Typography from "clever-react/ui/Typography";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  root: { ...spacing["mt-5"], textAlign: "center" },
}));

const FeedbackBlock = () => {
  const classes = getClasses();

  const [isSelectedThumbUp, setIsSelectedThumbUp] = useState(undefined);
  const [isSelectedThumbDown, setIsSelectedThumbDown] = useState(undefined);

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Feedback Thumbs
      </Typography>

      <Grid
        container
        justify="center"
        alignItems="center"
        className={classes.root}
      >
        <Grid item>
          <FeedbackThumb
            handleThumbClick={() => setIsSelectedThumbUp("up")}
            isSelected={isSelectedThumbUp}
            thumbUp
            size={50}
          />
        </Grid>

        <Grid item>
          <FeedbackThumb
            handleThumbClick={() => setIsSelectedThumbDown("down")}
            isSelected={isSelectedThumbDown}
            thumbUp={false}
            size={50}
          />
        </Grid>
      </Grid>
    </>
  );
};

FeedbackBlock.propTypes = {};

FeedbackBlock.defaultProps = {};

export default FeedbackBlock;
