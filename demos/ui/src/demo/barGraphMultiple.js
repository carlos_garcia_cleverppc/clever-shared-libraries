import React from "react";

import BarGraphMultiplePreview from "clever-react/ui/BarGraphMultiple";
import BrandIcon from "clever-react/ui/BrandIcon";

import Typography from "clever-react/ui/Typography";

const graphData = [
  {
    date: "01/04/21",
    google: 4,
    facebook: 5,
  },
  {
    date: "02/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
  {
    date: "03/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
  {
    date: "04/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
  {
    date: "05/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
  {
    date: "06/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
  {
    date: "07/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
  {
    date: "08/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
  {
    date: "09/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
  {
    date: "10/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
  {
    date: "11/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
  {
    date: "12/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
  {
    date: "13/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
  {
    date: "14/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
  {
    date: "15/04/21",
    google: Math.floor(Math.random() * 11),
    facebook: Math.floor(Math.random() * 11),
  },
];

const BarStackExample = () => {
  const colorArray = ["primary", "danger"];
  const legendVisualizer = [
    <BrandIcon icon="GoogleAdsCircle" size={20} />,
    <BrandIcon icon="FacebookCircle" size={20} />,
  ];

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        BarGraphMultiple
      </Typography>

      <BarGraphMultiplePreview
        graphData={graphData}
        colorArray={colorArray}
        colorShade={[]}
        legendVisualizer={legendVisualizer}
        type="impresiones"
      />
    </>
  );
};

export default BarStackExample;
