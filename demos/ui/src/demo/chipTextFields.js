/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-confusing-arrow */
import React, { useState } from "react";

import ChipTextField from "clever-react/ui/ChipTextField";
import Grid from "clever-react/ui/Grid";
import Typography from "clever-react/ui/Typography";

import { urlRegExp, dots } from "clever-react/ui/ChipTextField/validations";

const AutocompleteDemo = () => {
  const [values, setValues] = useState([]);

  const addValue = newValue => {
    setValues([...values, { text: newValue }]);
  };

  const deleteValue = index => {
    const auxValues = [...values];

    auxValues.splice(index, 1);
    setValues(auxValues);
  };

  const deleteAllValues = () => {
    setValues([]);
  };

  const validation = (input, maxLength) =>
    input.length <= maxLength && !urlRegExp.test(input) && !dots.test(input);

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Textfield with chips
      </Typography>

      <Grid container spacing={2}>
        <Grid item xs={6}>
          <ChipTextField
            type="Headlines"
            chips={values}
            addChip={addValue}
            deleteChip={deleteValue}
            clearAll={deleteAllValues}
            maxChars={35}
            update={() => {}}
            validate={validation}
            /* copies={{
            placeholder: "Introduce un item",
            maxChipsAlert: "Ya has introducido el maximo de chips posibles",
            minLengthAlert: "El texto no contiene el minimo de caracteres",
            duplicatedItemAlert: "El texto ya esta insertado",
            maxLengthAlert: "El texto excede el maximo de caracteres",
            inputError: "El texto contiene un caracter no permitido",
            clearAll: "Clear all",
            saveWarning: "Recuerda pulsar el intro para introducir el elemento",
            chipsAdded: "chips añadidos",
          }} */
          />
        </Grid>
      </Grid>
    </>
  );
};

export default AutocompleteDemo;
