/* eslint-disable no-console */
import React, { useRef } from "react";

import Button from "@material-ui/core/Button";

import Typography from "clever-react/ui/Typography";
import Table from "clever-react/ui/Table";
import DataTable from "clever-react/ui/DataTable";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  table: { ...spacing["mx-3"] },
}));

const TableDemo = () => {
  const classes = getClasses();
  const childRef = useRef();

  const headers = [
    {
      Header: "First Name",
      accessor: "firstName",
      hideSort: true, // hides de sorting icon
    },
    {
      Header: "Age",
      accessor: "age",
    },
    {
      Header: "Gender",
      accessor: "gender",
    },
    {
      Header: "Grade",
      accessor: "grade",
      custom: true,
    },
  ];

  const data = [
    {
      firstName: "Alice",
      age: 0.3,
      gender: "F",
      grade: 4,
    },
    {
      firstName: "Mike",
      age: 0.04,
      gender: "M",
      grade: 1,
    },
    {
      firstName: "John",
      age: 1,
      gender: "M",
      grade: 4,
    },
    {
      firstName: "Joe",
      age: 0.5,
      gender: "M",
      grade: 6,
    },
    {
      firstName: "Linda",
      age: 0.1,
      gender: "F",
      grade: 5,
    },
    {
      firstName: "Noah",
      age: 0.03,
      gender: "M",
      grade: 3,
    },
    {
      firstName: "Emma",
      age: 7,
      gender: "F",
      grade: 3,
    },
    {
      firstName: "James",
      age: 10,
      gender: "M",
      grade: 5,
    },
    {
      firstName: "Mia",
      age: 8,
      gender: "F",
      grade: 5,
    },
    {
      firstName: "William",
      age: 11,
      gender: "M",
      grade: 6,
    },
    {
      firstName: "Alice",
      age: 9,
      gender: "F",
      grade: 4,
    },
    {
      firstName: "Mike",
      age: 5,
      gender: "M",
      grade: 1,
    },
    {
      firstName: "aaaaaaJohn",
      age: 8,
      gender: "M",
      grade: 4,
    },
    {
      firstName: "aaaaaaJoe",
      age: 11,
      gender: "M",
      grade: 6,
    },
    {
      firstName: "aaaaaaLinda",
      age: 8,
      gender: "F",
      grade: 5,
    },
    {
      firstName: "aaaaaaNoah",
      age: 9,
      gender: "M",
      grade: 3,
    },
    {
      firstName: "aaaaaaEmma",
      age: 7,
      gender: "F",
      grade: 3,
    },
    {
      firstName: "aaaaaaJames",
      age: 10,
      gender: "M",
      grade: 5,
    },
  ];

  const printBody = (value, key) => {
    if (key === "grade" && value < 5)
      return (
        <Typography color="danger" align="right">
          {value}
        </Typography>
      );

    if (key === "grade" && value >= 5)
      return (
        <Typography color="success" align="right">
          {value}
        </Typography>
      );

    return value;
  };

  const selectCallback = selectedRows => {
    console.log(selectedRows);
  };

  const sortFunction = (column, order) => {
    console.log(column, order);
  };

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Table
      </Typography>

      <Button onClick={() => childRef.current.checkAllRows(true)}>
        Check all
      </Button>
      <Button onClick={() => childRef.current.checkAllRows(false)}>
        Uncheck all
      </Button>
      <Button onClick={() => childRef.current.checkAllPageRows(true)}>
        Check all in page
      </Button>
      <Button onClick={() => childRef.current.checkAllPageRows(false)}>
        Uncheck all in page
      </Button>

      <DataTable
        ref={childRef}
        headers={headers}
        data={data}
        className={classes.table}
        printBody={printBody}
        selection
        selectionCallback={selectCallback}
        sortCallback={sortFunction}
        createRow={{
          callback: () => {},
          label: "New person",
        }}
      />

      <Table
        headers={headers}
        data={data}
        className={classes.table}
        createRow={{
          callback: () => {},
          label: "New person",
          loading: true,
        }}
        printBody={printBody}
      />
    </>
  );
};

export default TableDemo;
