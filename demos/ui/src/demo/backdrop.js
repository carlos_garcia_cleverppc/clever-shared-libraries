import React from "react";

import Backdrop from "clever-react/ui/Backdrop";
import Button from "clever-react/ui/Button";
import Typography from "clever-react/ui/Typography";
import Loading from "clever-react/ui/Loading";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({
  backdrop: { color: "#fff", zIndex: 1000 },
}));

const BackdropPreview = () => {
  const classes = getClasses();

  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };
  const handleToggle = () => {
    setOpen(!open);
  };

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade={900} gutterBottom>
        Backdrop
      </Typography>

      <Button onClick={handleToggle}>Open</Button>

      <Backdrop className={classes.backdrop} open={open} onClick={handleClose}>
        <Loading color="white" />
      </Backdrop>
    </div>
  );
};

export default BackdropPreview;
