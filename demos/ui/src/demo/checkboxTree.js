import React, { useState } from "react";

import CheckboxTree from "clever-react/ui/CheckboxTree";
import Typography from "clever-react/ui/Typography";

// ¡¡¡¡¡¡¡ Importar esta hoja de estilos siempre !!!!!!!
// import "clever-react/ui/CheckboxTree/styles.scss";
// ¡¡¡¡¡¡¡ Importar esta hoja de estilos siempre !!!!!!!

const nodes = [
  {
    value: "mars",
    label: "Mars",
    children: [
      { value: "phobos", label: "Phobos" },
      { value: "deimos", label: "Deimos" },
    ],
  },
];

const CheckboxTreeDemo = () => {
  const [checked, setChecked] = useState([]);
  const [expanded, setExpanded] = useState([]);

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Checkbox Tree
      </Typography>

      <CheckboxTree
        nodes={nodes}
        checked={checked}
        expanded={expanded}
        onCheck={setChecked}
        onExpand={setExpanded}
        onClick={
          // Fix para permitir que se expanda y se seleccione un nodo pulsando en la etiqueta
          clickedNode => {
            if (clickedNode.showCheckbox && !clickedNode.disabled) {
              let newCheckedList;

              if (clickedNode.checked) {
                newCheckedList = checked.filter(
                  currentValue => currentValue !== clickedNode.value
                );
              } else {
                newCheckedList = [...checked, clickedNode.value];
              }

              setChecked(newCheckedList);
            }
          }
        }
      />
    </>
  );
};

export default CheckboxTreeDemo;
