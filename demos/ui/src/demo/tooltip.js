/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-console */
import React from "react";

import makeStyles from "clever-react/ui/styles/makeStyles";
import Tooltip from "clever-react/ui/Tooltip";
import Chip from "clever-react/ui/Chip";
import Circle from "clever-react/ui/Icon/Circle";
import Typography from "clever-react/ui/Typography";
import Button from "clever-react/ui/Button";
import Grid from "clever-react/ui/Grid";
import Link from "clever-react/ui/Link";
import Warning from "clever-react/ui/Icon/Warning";

const getClasses = makeStyles(() => ({
  root: { width: "400px" },
  vertical: { height: "30px" },
}));

const TooltipPreview = () => {
  const classes = getClasses();

  const test = () => {
    console.log("click");
  };

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Tooltips
      </Typography>

      <Tooltip
        title={
          <>
            <Typography>interactuable tooltip</Typography>
            <Button onClick={() => test()}> click</Button>
          </>
        }
        color="white"
        interactive
      >
        <Chip label="complete" />
      </Tooltip>

      <Tooltip
        title="Evaluating score..."
        icon={<Circle size={24} color="warning" className={classes.icon} />}
        type="complete"
      >
        <Chip label="complete" />
      </Tooltip>

      <Tooltip
        title={
          <Grid container spacing justify="flex-start">
            <Grid item xs={12}>
              <Typography shade={900} variant="body0">
                emails accounts:
              </Typography>

              <Typography variant="body0">asdfasdfsadf@gmail.com</Typography>

              <Typography variant="body0">qwerwqrwqer@gmail.com</Typography>
            </Grid>

            <Grid item xs={12}>
              <Typography shade={900} variant="body0">
                google accounts:
              </Typography>

              <Typography variant="body0">123456789</Typography>
            </Grid>

            <Grid item xs={12}>
              <Link onClick={() => {}}>
                <Typography variant="body0" color="primary">
                  Change Merchant Account
                </Typography>
              </Link>
            </Grid>
          </Grid>
        }
        type="simple"
        spacing={2}
        color="white"
        textAlign="left"
      >
        <Chip label="simple" />
      </Tooltip>

      <Tooltip title="title" type="simple" spacing={2}>
        <Warning color="warning" />
      </Tooltip>

      <Tooltip title="title" type="simple" spacing={3}>
        <Typography> probando a ver </Typography>
      </Tooltip>

      <Tooltip title="Metricas" type="simple">
        <Typography>Prueba</Typography>
      </Tooltip>
    </>
  );
};

export default TooltipPreview;
