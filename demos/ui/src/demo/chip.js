/* eslint-disable no-console */
import React from "react";

import Typography from "clever-react/ui/Typography";
import makeStyles from "clever-react/ui/styles/makeStyles";

import Chip from "clever-react/ui/Chip";

const getClasses = makeStyles(() => ({ root: {} }));

const Chips = () => {
  const classes = getClasses();

  const handleDelete = () => {
    console.log("You clicked the delete icon.");
  };

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade={900} gutterBottom>
        Chips
      </Typography>

      <Chip
        label="chips chips chips chips chips chips chips chips chips chips "
        // onDelete={handleDelete}
        color="primary"
        // loading
      />

      <Chip label="chips" onDelete={handleDelete} color="secondary" loading />

      <Chip label="chips" onDelete={handleDelete} color="warning" />

      <Chip label="chips" onDelete={handleDelete} color="danger" />

      <Chip label="chips" variant="outlined" color="secondary" size="small" />

      <Chip
        label="submitted"
        variant="outlined"
        color="secondary"
        size="extraSmall"
      />
    </div>
  );
};

export default Chips;
