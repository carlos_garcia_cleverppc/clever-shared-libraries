import React from "react";

import Typography from "clever-react/ui/Typography";
import Checkbox from "clever-react/ui/Checkbox";

const Checkboxes = () => {
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true,
    checkedC: true,
  });

  const handleChange = name => {
    setState({ ...state, [name]: !state[name] });
  };

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Checkbox
      </Typography>

      <Checkbox
        checked={state.checkedA}
        onChange={() => handleChange("checkedA")}
        value="Single line"
      >
        <Typography>An option</Typography>
      </Checkbox>

      <Checkbox
        checked={state.checkedB}
        onChange={() => handleChange("checkedB")}
        value="Single line"
        color="secondary"
      >
        <Typography>Another option</Typography>
      </Checkbox>

      <Checkbox
        checked={state.checkedC}
        onChange={() => handleChange("checkedC")}
        value="privacy"
        disabled
      >
        <Typography variant="body1" shade={900} weight="bold">
          My account
        </Typography>

        <Typography variant="body1">EUR - Madrid</Typography>

        <Typography variant="body3">123456789</Typography>
      </Checkbox>
    </>
  );
};

export default Checkboxes;
