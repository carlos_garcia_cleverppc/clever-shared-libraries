import React from "react";

import Grid from "@material-ui/core/Grid";

import Typography from "clever-react/ui/Typography";
import Loading from "clever-react/ui/Loading";

const Loadings = () => (
  <>
    <Typography variant="h2" shade={900} gutterBottom>
      Loading
    </Typography>

    <Grid container>
      <Grid item xs>
        <Loading color="primary" center />
      </Grid>

      <Grid item xs>
        <Loading color="secondary" center />
      </Grid>
    </Grid>
  </>
);

export default Loadings;
