import React from "react";

import Snackbar from "clever-react/ui/Snackbar";
import Typography from "clever-react/ui/Typography";
import Button from "clever-react/ui/Button";

const SliderPreview = () => {
  const [open, setOpen] = React.useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        Snackbars
      </Typography>

      <Button onClick={() => setOpen(true)}>Open simple snackbar</Button>

      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={open}
        autoHideDuration={4000}
        onClose={handleClose}
        text="Alert!"
      />

      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        open={open}
        autoHideDuration={4000}
        onClose={handleClose}
        text="Working on your account..."
        isLoading
      />
    </div>
  );
};

export default SliderPreview;
