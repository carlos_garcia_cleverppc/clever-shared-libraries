import React from "react";

import CountryFlag from "clever-react/ui/CountryFlag";

import Typography from "clever-react/ui/Typography";
import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({
  flag: {
    width: "50px !important",
    height: "50px !important",
  },
}));

const Flags = () => {
  const classes = getClasses();

  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        Flags
      </Typography>

      <CountryFlag countryCode="US" svg className={classes.flag} />

      <CountryFlag countryCode="ES" svg />

      <CountryFlag countryCode="GB" />
    </div>
  );
};

export default Flags;
