import React from "react";

import Typography from "clever-react/ui/Typography";
import Radio from "clever-react/ui/Radio";

const Radios = () => {
  const [state, setState] = React.useState("a");

  const handleChange = event => {
    setState(event.target.value);
  };

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Radios
      </Typography>

      <Radio
        checked={state === "a"}
        onChange={handleChange}
        value="a"
        color="secondary"
      >
        <Typography variant="body0">An option</Typography>
      </Radio>

      <Radio checked={state === "b"} onChange={handleChange} value="b">
        <Typography>Another option</Typography>
      </Radio>

      <Radio checked={state === "c"} onChange={handleChange} value="c">
        <Typography variant="body1" shade={900} weight="bold">
          My account
        </Typography>
      </Radio>
    </>
  );
};

export default Radios;
