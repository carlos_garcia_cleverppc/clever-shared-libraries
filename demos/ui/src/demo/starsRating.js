import React, { useState } from "react";

import StarsRating from "clever-react/ui/StarsRating";
import Typography from "clever-react/ui/Typography";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({}));

const StarsRatingPreview = () => {
  const classes = getClasses();

  const [starsSelected, setStarsSelected] = useState(undefined);

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade="darker" gutterBottom>
        Stars Rating
      </Typography>

      <StarsRating
        selected={starsSelected}
        setSelected={setStarsSelected}
        disabled={!!starsSelected}
      />
    </div>
  );
};

export default StarsRatingPreview;
