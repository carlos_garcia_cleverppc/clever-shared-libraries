import React from "react";

import Grid from "clever-react/ui/Grid";
import Typography from "clever-react/ui/Typography";
import RoundedSelect from "clever-react/ui/RoundedSelect";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({ bold: { fontWeight: "bold" } }));

const RoundedSelects = () => {
  const [text, setText] = React.useState("");
  const options = [
    {
      label: "Today",
      value: 1,
      disabled: true,
      disabledMessage: "Este texto va para las opciones deshabilitadas",
    },
    { label: "Last 7 days", value: 7, icon: "ruler", iconColor: "primary" },
    { label: "Last 14 days", value: 14, icon: "rocket" },
    { label: "Last 30 days", value: 30 },
  ];
  const classes = getClasses();

  const handleChange = event => {
    setText(event.target.value);
  };

  const printMenuItem = (label, value) => {
    if (value === 1) return <span className={classes.bold}>{label}</span>;
    return label;
  };

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Rounded Selects
      </Typography>

      <Grid container spacing={2}>
        <Grid item xs={8}>
          <RoundedSelect
            name="select"
            placeholder="Select label rounded select"
            value={text}
            onChange={handleChange}
            options={options}
            printMenuItem={printMenuItem}
            textAlignRight={false}
          />
        </Grid>
      </Grid>
    </>
  );
};

export default RoundedSelects;
