import React from "react";

import AdPreview from "clever-react/ui/AdPreview";
import Typography from "clever-react/ui/Typography";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({
  backdrop: { color: "#fff", zIndex: 1000 },
}));

const AdPreviewDemo = () => {
  const classes = getClasses();

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade={900} gutterBottom>
        AdPreview
      </Typography>

      <AdPreview
        url="www.mitienda.com"
        name="Mi tienda"
        headlines={["Headline1", "Headline2", "Headline3", "Headline4"]}
        headlinesDefault="Your default headline"
        descriptions={[
          "Description1",
          "Description2",
          "Description3",
          "Description4",
        ]}
        descriptionsDefault="Your default description"
        services={{
          customerService: {
            name: "24-7 customer service",
            value: "",
            selected: true,
          },
          easyReturns: { name: "Easy returns", value: "", selected: true },
          freeShipping: {
            name: "Free shipping over",
            value: "50",
            selected: true,
          },
        }}
        servicesDefault="Your default service"
        usePhone
        phoneText="Call +34 952845778"
        currencySymbol="$"
      />
    </div>
  );
};

export default AdPreviewDemo;
