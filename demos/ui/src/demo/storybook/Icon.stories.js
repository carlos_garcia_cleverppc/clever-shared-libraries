/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from "react";
import filter from "lodash-es/filter";
import includes from "lodash-es/includes";
import toLower from "lodash-es/toLower";

import { icons, brandIcons, iconSizes } from "../clever-react/ui/styles/icons";
import makeStyles from "../clever-react/ui/styles/makeStyles";
import { getColorsLabels, getShades } from "../clever-react/ui/styles/colors";

import Icon from "../clever-react/ui/Icon/index";
import Grid from "../clever-react/ui/Grid/index";
import BrandIcon from "../clever-react/ui/BrandIcon/index";
import Tooltip from "../clever-react/ui/Tooltip";
import Typography from "../clever-react/ui/Typography";
import Snackbar from "../clever-react/ui/Snackbar";
import TextField from "../clever-react/ui/TextField";

export default {
  title: "Icon",
  component: BrandIcon,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
    shade: {
      options: getShades(),
      control: {
        type: "select",
      },
    },
    size: {
      options: iconSizes,
      control: {
        type: "select",
      },
    },
  },
};

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  icon: { ...spacing["m-1"], ...spacing["mr-3"], cursor: "pointer" },
  iconContainer: {
    display: "flex",
    alignItems: "center",
    "&:hover": { cursor: "pointer" },
  },
  textField: {
    ...spacing["mb-4"],
    maxWidth: "400px",
  },
}));

const IconsTemplate = args => {
  const classes = getClasses();
  const [open, setOpen] = useState(false);
  const [text, setText] = useState("");
  const [iconsFiltered, setIconsFiltered] = useState(icons);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const handleSearchChange = ev => {
    setText(ev.target.value);

    const list = filter(icons, icon =>
      includes(toLower(icon), toLower(ev.target.value))
    );

    setIconsFiltered(list);
  };

  return (
    <>
      <Typography variant="body0" gutterBottom>
        Search:{" "}
      </Typography>

      <TextField
        value={text}
        onChange={handleSearchChange}
        variant="outlined"
        className={classes.textField}
        size="small"
      />

      <Typography variant="body0" gutterBottom>
        Tip: Click an icon to copy its code to the clipboard!
      </Typography>

      <Grid container spacing>
        {iconsFiltered.map(icon => (
          <Grid item xs={3}>
            <div
              onClick={() => {
                navigator.clipboard.writeText(`<${icon} />`);
                setOpen(true);
              }}
              className={classes.iconContainer}
            >
              <Icon {...args} icon={icon} className={classes.icon} size={24} />

              <Typography>{icon}</Typography>
            </div>
          </Grid>
        ))}
      </Grid>

      <Snackbar
        open={open}
        onClose={handleClose}
        autoHideDuration={4000}
        text="Copied to clipboard!"
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      />
    </>
  );
};

const BrandIconsTemplate = args => {
  const classes = getClasses();

  const [open, setOpen] = useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <>
      <Typography variant="body0" gutterBottom>
        Tip: Click an icon to copy its code to the clipboard!
      </Typography>

      {brandIcons.map(icon => (
        <Tooltip key={`icon-tooltip-${icon}`} title={icon}>
          <div
            onClick={() => {
              navigator.clipboard.writeText(`<${icon} />`);
              setOpen(true);
            }}
          >
            <BrandIcon {...args} className={classes.icon} icon={icon} />
          </div>
        </Tooltip>
      ))}

      <Snackbar
        open={open}
        onClose={handleClose}
        autoHideDuration={4000}
        text="Copied to clipboard!"
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      />
    </>
  );
};

export const SimpleIcons = IconsTemplate.bind({});
export const BrandIcons = BrandIconsTemplate.bind({});
BrandIcons.args = { size: 64 };

export const CustomIcons = IconsTemplate.bind({});
CustomIcons.args = { color: "green", size: 32 };
