/* eslint-disable max-len */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import capitalize from "lodash-es/capitalize";

import Typography from "clever-react/ui/Typography";
import Snackbar from "clever-react/ui/Snackbar";
import Tooltip from "clever-react/ui/Tooltip";

import useTheme from "clever-react/ui/styles/useTheme";
import makeStyles from "clever-react/ui/styles/makeStyles";
import colorsFullList from "clever-react/ui/Provider/colors";

const getClasses = makeStyles(({ cleverUI: { spacing, typography } }) => ({
  color: { marginRight: "20px", "&:hover": { cursor: "pointer" } },
  block: { ...spacing["p-2"], maxHeight: "50px", height: "36px" },
  label: { fontFamily: typography.font2 },
  blob: {
    width: "50px",
    height: "50px",
    borderRadius: "50%",
    marginBottom: "10px",
  },
}));

export default {
  title: "Colors",
  component: <div />,
};

const getBlocks = (
  colorsList,
  shadesList,
  colors,
  classes,
  open,
  setOpen,
  handleClose
) => (
  <>
    <Typography variant="body0" gutterBottom>
      Tip: Click a color to copy its name to the clipboard!
    </Typography>

    {colorsList.map(c => (
      <div
        key={`block-${c}`}
        style={{ display: "flex", alignItems: "center", marginBottom: "20px" }}
      >
        <div style={{ width: "150px" }}>
          <Typography shade={900}>{capitalize(c)}</Typography>
        </div>

        {shadesList.map(shade => {
          if (colors[c][shade]) {
            return (
              <div
                key={`block-${c}-${shade}`}
                onClick={() => {
                  navigator.clipboard.writeText(`colors.${c}.${shade}`);
                  setOpen(true);
                }}
                className={classes.color}
              >
                <Tooltip title={colors[c][shade]}>
                  <div
                    className={classes.blob}
                    style={{ backgroundColor: colors[c][shade] }}
                  />
                </Tooltip>

                <Typography variant="body0" shade={700} align="center">
                  {shade}
                </Typography>
              </div>
            );
          }
          return "";
        })}
      </div>
    ))}

    <Snackbar
      open={open}
      onClose={handleClose}
      autoHideDuration={4000}
      text="Copied to clipboard!"
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
    />
  </>
);

const MainColorsDiv = () => {
  const {
    cleverUI: { colors },
  } = useTheme();

  const [open, setOpen] = React.useState(false);

  const classes = getClasses();

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return getBlocks(
    Object.keys(colorsFullList.main),
    [50, 100, 200, 300, 400, 500, 600, 700, 800, 900],
    colors,
    classes,
    open,
    setOpen,
    handleClose
  );
};

const ColorsDiv = () => {
  const {
    cleverUI: { colors },
  } = useTheme();

  const [open, setOpen] = React.useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const classes = getClasses();

  return getBlocks(
    Object.keys(colorsFullList.colors),
    [50, 100, 200, 300, 400, 500, 600, 700, 800, 900],
    colors,
    classes,
    open,
    setOpen,
    handleClose
  );
};

const ProductColorsDiv = () => {
  const {
    cleverUI: { colors },
  } = useTheme();

  const [open, setOpen] = React.useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const classes = getClasses();

  return getBlocks(
    Object.keys(colorsFullList.product),
    [50, 100, 200, 300, 400, 500, 600, 700, 800, 900],
    colors,
    classes,
    open,
    setOpen,
    handleClose
  );
};

const PlatformColorsDiv = () => {
  const {
    cleverUI: { colors },
  } = useTheme();

  const [open, setOpen] = React.useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const classes = getClasses();

  return getBlocks(
    Object.keys(colorsFullList.platform),
    [50, 100, 200, 300, 400, 500, 600, 700, 800, 900],
    colors,
    classes,
    open,
    setOpen,
    handleClose
  );
};

const OtherColorsDiv = () => {
  const {
    cleverUI: { colors },
  } = useTheme();

  const [open, setOpen] = React.useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  const classes = getClasses();

  return getBlocks(
    Object.keys(colorsFullList.other),
    [50, 100, 200, 300, 400, 500, 600, 700, 800, 900],
    colors,
    classes,
    open,
    setOpen,
    handleClose
  );
};

export const MainColors = MainColorsDiv.bind({});
export const Colors = ColorsDiv.bind({});
export const ProductColors = ProductColorsDiv.bind({});
export const PlatformColors = PlatformColorsDiv.bind({});
export const OtherColors = OtherColorsDiv.bind({});
