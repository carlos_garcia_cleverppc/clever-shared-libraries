import React from "react";

import Tabs from "clever-react/ui/Tabs";
import Tab from "clever-react/ui/Tab";
import Image from "clever-react/ui/Image";

const TabsDemo = () => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <Tabs value={value} onChange={handleChange} variant="scrollable">
        <Tab
          label="Item One"
          value={0}
          icon={
            <Image
              size="50x50"
              center={false}
              src="/assets/img/avatar-google.svg"
            />
          }
        />
        <Tab
          label="Item Two"
          value={1}
          icon={
            <Image
              size="50x50"
              center={false}
              src="/assets/img/avatar-ms.svg"
            />
          }
        />
        <Tab
          label="Item One"
          value={0}
          icon={
            <Image
              size="50x50"
              center={false}
              src="/assets/img/avatar-google.svg"
            />
          }
        />
        <Tab
          label="Item Two"
          value={1}
          icon={
            <Image
              size="50x50"
              center={false}
              src="/assets/img/avatar-ms.svg"
            />
          }
        />
      </Tabs>

      {value === 0 && <h1>Item1</h1>}
      {value === 1 && <h1>Item2</h1>}
    </>
  );
};

export default TabsDemo;
