import React, { useState } from "react";

import SidebarItem from "clever-react/ui/Sidebar/Item";
import Typography from "clever-react/ui/Typography";

const SidebarItemPreview = () => {
  const [selectedHome, setSelectedHome] = useState(false);

  const [selectedSettings, setSelectedSettings] = useState(false);

  const [selectedMetricsChild1, setSelectedMetricsChild1] = useState(false);
  const [selectedMetricsChild2, setSelectedMetricsChild2] = useState(false);
  const selectedMetrics = selectedMetricsChild1 || selectedMetricsChild2;

  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        SidebarItem
      </Typography>

      <SidebarItem
        name="Home"
        icon="Home"
        isSelected={selectedHome}
        onClick={() => setSelectedHome(!selectedHome)}
        isSidebarCollapsed
      />

      <SidebarItem
        name="Settings"
        icon="Home"
        isSelected={selectedSettings}
        onClick={() => setSelectedSettings(!selectedSettings)}
        label="Label Pro"
      />

      <SidebarItem
        name="Metrics"
        icon="Home"
        isSelected={selectedMetrics}
        childrenItems={[
          <SidebarItem
            name="Child1"
            icon="Home"
            isSelected={selectedMetricsChild1}
            onClick={() => setSelectedMetricsChild1(!selectedMetricsChild1)}
            isChild
          />,
          <SidebarItem
            name="Child2"
            icon="Home"
            isSelected={selectedMetricsChild2}
            onClick={() => setSelectedMetricsChild2(!selectedMetricsChild2)}
            isChild
          />,
        ]}
      />
    </div>
  );
};

export default SidebarItemPreview;
