import React, { useEffect } from "react";

import Grid from "clever-react/ui/Grid";
import Typography from "clever-react/ui/Typography";
import Skeleton from "clever-react/ui/Skeleton";

const Skeletons = () => {
  const [loading, setLoading] = React.useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setLoading(false);
    }, 4000);
    return () => clearTimeout(timer);
  }, []);

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Skeleton
      </Typography>

      <Grid container>
        <Grid item xs={12}>
          {loading ? (
            <Skeleton>
              <Typography>Skeleton adaptable</Typography>
            </Skeleton>
          ) : (
            <Typography>Skeleton adaptable</Typography>
          )}
        </Grid>

        <Grid item xs={12}>
          <Skeleton variant="text" />
          <Skeleton variant="circle" width={25} height={25} />
          <Skeleton variant="rect" width="75%" />
        </Grid>
      </Grid>
    </>
  );
};

export default Skeletons;
