/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-confusing-arrow */
import React from "react";

import TextField from "clever-react/ui/TextField";
import Autocomplete from "clever-react/ui/Autocomplete";
import Grid from "clever-react/ui/Grid";
import Typography from "clever-react/ui/Typography";

const top10Films = [
  { title: "The Shawshank Redemption", year: 1994 },
  { title: "The Godfather", year: 1972 },
  { title: "The Godfather: Part II", year: 1974 },
  { title: "The Dark Knight", year: 2008 },
  { title: "12 Angry Men", year: 1957 },
];

const AutocompleteDemo = () => {
  const [value, setValue] = React.useState("");

  const handleChange = newValue => {
    if (newValue) setValue(newValue.title);
    else setValue("");
  };

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Autocomplete
      </Typography>

      <Grid container spacing={2}>
        <Grid item xs={6}>
          <Autocomplete
            inputValue={value}
            onChange={(event, newValue) => handleChange(newValue)}
            options={top10Films}
            getOptionLabel={option => (option ? option.title : "")}
            onInputChange={ev =>
              ev ? setValue(ev.target.value) : setValue("")
            }
            getOptionDisabled={option => option.title === "count"}
            ListboxComponent={listboxProps => (
              <ul {...listboxProps}>
                {listboxProps.children}

                <div>
                  <Typography variant="body1" weight="bold">
                    5 suggestions
                  </Typography>
                </div>
              </ul>
            )}
            renderInput={params => (
              <TextField
                {...params}
                label="Combo box"
                variant="outlined"
                fullWidth
              />
            )}
            renderOption={option => (
              <div>
                <Typography variant="body1" weight="bold">
                  {option.title}
                </Typography>

                <Typography variant="body1">{option.year}</Typography>
              </div>
            )}
          />
        </Grid>
      </Grid>
    </>
  );
};

export default AutocompleteDemo;
