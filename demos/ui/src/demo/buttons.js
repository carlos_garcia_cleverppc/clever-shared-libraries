/* eslint-disable no-console */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";

import Typography from "clever-react/ui/Typography";
import Link from "clever-react/ui/Link";
import Button from "clever-react/ui/Button";
import ConnectButton from "clever-react/ui/ConnectButton";
import Grid from "clever-react/ui/Grid";

const Buttons = () => (
  <div>
    <Typography variant="h2" shade={900} gutterBottom>
      Buttons and Links
    </Typography>

    <Grid container spacing>
      <Grid item xs>
        <Button onClick={() => console.log("You clicked the button")}>
          Medium button
        </Button>
      </Grid>

      <Grid item xs>
        <Button
          size="small"
          onClick={() => console.log("You clicked the button")}
        >
          Small button
        </Button>
      </Grid>
      <Grid item xs>
        <Button
          onClick={() => console.log("You clicked the button")}
          size="large"
        >
          Large button
        </Button>
      </Grid>

      <Grid item xs>
        <Button
          disabled
          isLoading
          onClick={() => console.log("You clicked the button")}
        >
          Next
        </Button>
      </Grid>

      <Grid item xs>
        <Button
          variant="secondary"
          onClick={() => console.log("You clicked the button")}
        >
          Previous
        </Button>
      </Grid>

      <Grid item xs>
        <Button
          variant="secondary"
          onClick={() => console.log("You clicked the button")}
          disabled
        >
          Previous
        </Button>
      </Grid>

      <Grid item xs>
        <Button
          onClick={() => console.log("You clicked the button")}
          size="large"
        >
          Comenzar
        </Button>
      </Grid>

      <Grid item xs>
        <Link to="/home" target="_blank" color="secondary">
          Links and terciary buttons
        </Link>
      </Grid>

      <Grid item xs>
        <Link
          onClick={() => console.log("You clicked the link")}
          color="danger"
        >
          Link onClick
        </Link>
      </Grid>

      <Grid item xs>
        <Link to="https://cleverecommerce.com" target="_blank">
          Links disabled
        </Link>
      </Grid>

      <Grid item xs>
        <ConnectButton
          label="Connect Google Account"
          image="/assets/img/google.svg"
        />
      </Grid>
    </Grid>
  </div>
);

export default Buttons;
