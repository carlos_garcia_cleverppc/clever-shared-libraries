import React, { useState } from "react";

import Grid from "@material-ui/core/Grid";

import Typography from "clever-react/ui/Typography";
import MultiSelect from "clever-react/ui/MultiSelect";

const films = [
  { id: "0", label: "The Shawshank Redemption" },
  { id: "1", label: "The Godfather" },
  { id: "2", label: "The Godfather: Part II" },
  { id: "3", label: "The Dark Knight" },
  { id: "4", label: "12 Angry Men" },
  { id: "5", label: "Schindler's List" },
  { id: "6", label: "Pulp Fiction" },
  { id: "7", label: "The Lord of the Rings: The Return of the King" },
  { id: "8", label: "The Good, the Bad and the Ugly" },
];

const MultiSelectPreview = () => {
  const [checked, setState] = useState(films.map(() => false));

  const handleChange = id =>
    setState(prevState => ({ ...prevState, [id]: !checked[id] }));

  return (
    <Grid container>
      <Grid item xs={12}>
        <Typography variant="h2" shade={900}>
          MultiSelect
        </Typography>
      </Grid>

      <Grid item xs={6}>
        <MultiSelect
          options={films}
          onChange={handleChange}
          checked={checked}
        />
      </Grid>
    </Grid>
  );
};

export default MultiSelectPreview;
