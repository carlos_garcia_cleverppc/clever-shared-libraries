import React from "react";

import Typography from "clever-react/ui/Typography";
import Currency from "clever-react/ui/Currency";

const CurrencyDemo = () => (
  <>
    <Typography variant="h2" shade={900} gutterBottom>
      Currency
    </Typography>

    <Typography variant="body2">
      <Currency quantity={4556.04} currency="EUR" />
    </Typography>

    <Typography variant="body2">
      <Currency quantity={4556} currency="GBP" />
    </Typography>

    <Typography variant="body2">
      <Currency symbolOnly currency="GBP" />
    </Typography>
  </>
);

export default CurrencyDemo;
