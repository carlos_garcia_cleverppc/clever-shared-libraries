import React from "react";

import Typography from "clever-react/ui/Typography";
import ChallengeBar from "clever-react/ui/ChallengeBar";
import Grid from "clever-react/ui/Grid";

const ChallengeBarDemo = () => {
  const [positionSelected, setPositionSelected] = React.useState(0);
  const onClick = index => {
    console.log("click en task", index);
    setPositionSelected(index);
  };

  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        ChallengeBar
      </Typography>

      <Grid container spacing>
        <Grid item xs={12}>
          <ChallengeBar
            statuses={["PENDING", "PENDING", "PENDING", "PENDING"]}
            onClickStatus={onClick}
            positionFocused={positionSelected}
          />

          <ChallengeBar
            statuses={["COMPLETED", "FAILED", "PENDING", "PENDING"]}
            onClickStatus={onClick}
            positionFocused={positionSelected}
          />

          <ChallengeBar
            statuses={["COMPLETED", "COMPLETED", "COMPLETED", "COMPLETED"]}
            onClickStatus={onClick}
            positionFocused={positionSelected}
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default ChallengeBarDemo;
