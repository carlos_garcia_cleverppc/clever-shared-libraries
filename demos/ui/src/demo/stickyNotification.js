/* eslint-disable no-console */
/* eslint-disable no-use-before-define */
import React from "react";
import { useSnackbar } from "notistack";

import Button from "clever-react/ui/Button";
import Typography from "clever-react/ui/Typography";

const StickyNotificationPreview = () => {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const notifications = {
    1: {
      key: "1",
      open: true,
      onClose: () => {
        console.log("me cierro");
        closeSnackbar("1");
      },
      title: "Titulo",
      description: "Descripción es una descripción muy larga con muchas cosas",
      textLink: "Ir a algun sitio",
      onClickLink: () => {
        console.log("Accion");
      },
      image: "assets/img/illustration-flags.svg",
      footer: (
        <>
          <Button>button footer 1</Button>
          <Button>button footer 2</Button>
        </>
      ),
    },
    2: {
      key: "2",
      open: true,
      onClose: () => {
        console.log("me cierro");
        closeSnackbar("2");
      },
      title: "Titulo2",
      description: "Descripción2 es una descripción muy larga con muchas cosas",
      textLink: "Ir a algun sitio2",
      onClickLink: () => {
        console.log("Accion");
      },
      image: "assets/img/illustration-cookies.svg",
    },
  };

  const handleOpen = position => {
    const notification = notifications[position];
    enqueueSnackbar(notification, {
      autoHideDuration: null,
      key: notification.key,
    });
  };

  const handleButton = () => {
    handleOpen("1");
    handleOpen("2");
  };

  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        StickyNotification
      </Typography>

      <Button onClick={() => handleButton()}>Open</Button>
    </div>
  );
};

export default StickyNotificationPreview;
