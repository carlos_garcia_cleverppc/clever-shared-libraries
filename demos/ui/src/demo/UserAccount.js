import React from "react";

import UserAccount from "clever-react/ui/Sidebar/UserAccount";
import Typography from "clever-react/ui/Typography";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({ root: { maxWidth: "220px" } }));

const UserAccountPreview = () => {
  const classes = getClasses();

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade={900} gutterBottom>
        SidebarUserAccount
      </Typography>

      <UserAccount
        accountSelected={{ name: "Test name", accountId: "123456789" }}
      />

      <UserAccount
        accountSelected={{ name: "Test name", accountId: "123456789" }}
        label="PREMIUM"
        isSidebarCollapsed
      />

      <UserAccount accountSelected={{ name: "Test name" }} label="PRO" />

      <UserAccount
        avatarSrc="https://i.blogs.es/16b956/gmail-nuevo-logo-google-workspace/1366_2000.jpg"
        accountSelected={{ name: "Test name", accountId: "123456789" }}
        label="PRO"
        allAccounts={[
          { name: "Test name", accountId: "123456789", isSelected: true },
          { name: "Test name2", accountId: "223456789", isSelected: false },
          { name: "Test name3", accountId: "323456789", isSelected: false },
        ]}
        titleCopy="titleCopy"
        noAccountsCopy="noAccountsCopy"
        addAccountCopy="addAccountCopy"
        logOutCopy="logOutCopy"
      />
    </div>
  );
};

export default UserAccountPreview;
