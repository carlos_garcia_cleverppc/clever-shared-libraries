import React, { useState } from "react";
import isEmpty from "lodash-es/isEmpty";
import remove from "lodash-es/remove";

import Grid from "clever-react/ui/Grid";
import Typography from "clever-react/ui/Typography";
import Divider from "clever-react/ui/Divider";
import Checkbox from "clever-react/ui/Checkbox";
import Block from "clever-react/ui/Block";
import FloatingMenu from "clever-react/ui/FloatingMenu";
import Trash from "clever-react/ui/Icon/Trash";
import Edit from "clever-react/ui/Icon/Edit";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  element: { ...spacing["m-4"] },
}));

const FloatingMenuDemo = () => {
  const classes = getClasses();

  const [anchorEl, setAnchorEl] = useState(null);
  const [list, setList] = useState({});
  const [selected, setSelected] = useState([]);

  const handleChange = name => event => {
    if (list[name]) {
      // item was selected
      const selectedAux = selected;

      remove(selected, s => name === s);

      setList({ ...list, [name]: false });
      setSelected(selectedAux);

      if (isEmpty(selectedAux)) setAnchorEl(null);
    } else {
      // new item
      const aux = selected;
      aux.push(name);

      setSelected(aux);
      setList({ ...list, [name]: true });

      if (!anchorEl) {
        const anchor = event.currentTarget.closest("div").childNodes[1];

        setAnchorEl(anchor);
      }
    }
  };

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Floating Menu
      </Typography>

      <FloatingMenu anchorEl={anchorEl} color="primary">
        <Grid container justify="center" align="center" alignItems="center">
          <Grid item className={classes.element}>
            <Edit color="white" size={24} />

            <Typography color="white">Update</Typography>
          </Grid>

          <Grid item className={classes.element} key="divider">
            <Divider position="vertical" />
          </Grid>

          <Grid item className={classes.element}>
            <Trash color="white" size={24} />

            <Typography color="white">Delete</Typography>
          </Grid>
        </Grid>
      </FloatingMenu>

      <Checkbox
        checked={!!list.block1}
        onChange={handleChange("block1")}
        value="Single line"
      >
        <Block
          color="magenta"
          title="holi"
          description="adios"
          image="/assets/img/block-example.svg"
          size="1x2"
          selected
        />
      </Checkbox>

      <Checkbox
        checked={!!list.block2}
        onChange={handleChange("block2")}
        value="Single line"
      >
        <Block
          color="magenta"
          title="holi"
          description="adios"
          image="/assets/img/block-example.svg"
          size="1x2"
          selected
        />
      </Checkbox>
    </>
  );
};

export default FloatingMenuDemo;
