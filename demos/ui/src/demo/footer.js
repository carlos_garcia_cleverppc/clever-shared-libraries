import React, { useState } from "react";

import FooterActions from "clever-react/ui/Sidebar/FooterActions";
import UserAccount from "clever-react/ui/Sidebar/UserAccount";
import Footer from "clever-react/ui/Sidebar/Footer";
import Typography from "clever-react/ui/Typography";
import Checkbox from "clever-react/ui/Checkbox";

const FooterPreview = () => {
  const [contactSelected, setContactSelected] = useState(false);
  const [metricsSelected, setMetricsSelected] = useState(false);

  const [collapsed, setCollapsed] = useState(false);

  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        SidebarFooter
      </Typography>

      <Checkbox
        checked={collapsed}
        onChange={() => setCollapsed(!collapsed)}
        value="Single line"
      >
        <Typography>IsSidebarCollapsed</Typography>
      </Checkbox>

      <Footer
        actionsPosition="right"
        isSidebarCollapsed={collapsed}
        userAccount={
          <UserAccount
            accountSelected={{ name: "Test name", accountId: "123456789" }}
            label="PRO"
            isSidebarCollapsed={collapsed}
          />
        }
        footerActions={
          <FooterActions
            items={[
              {
                icon: "Support",
                name: "Contact",
                isSelected: contactSelected,
                onClick: () => setContactSelected(!contactSelected),
              },
              {
                icon: "Ruler",
                name: "Metrics",
                isSelected: metricsSelected,
                onClick: () => setMetricsSelected(!metricsSelected),
              },
            ]}
            isSidebarCollapsed={collapsed}
          />
        }
      />
    </div>
  );
};

export default FooterPreview;
