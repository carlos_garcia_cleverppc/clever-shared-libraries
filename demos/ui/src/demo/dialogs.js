import React from "react";

import Typography from "clever-react/ui/Typography";
import Dialog from "clever-react/ui/Dialog";
import Button from "clever-react/ui/Button";
import Link from "clever-react/ui/Link";

const Dialogs = () => {
  const [open, setOpen] = React.useState(false);
  const [open2, setOpen2] = React.useState(false);
  const [open3, setOpen3] = React.useState(false);
  const [open4, setOpen4] = React.useState(false);

  const handleOpen = () => {
    setOpen(!open);
  };

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Dialog
      </Typography>

      <Button color="primary" onClick={handleOpen}>
        Open
      </Button>

      <Dialog
        open={open}
        // onClose={handleOpen}
        title="This is a title for a modal dialog"
        description="This is the description placeholder for any modal dialog. Any component would be optional and depends on its functional purpose."
        showImageMobile
        onClose={() => setOpen(!open)}
        buttons={
          <>
            <Link to="https://cleverecommerce.com" target="_blank" external>
              Links and terciary buttons
            </Link>

            <Button onClick={handleOpen}>Create new account</Button>
          </>
        }
      >
        <Typography>This is the dialog content</Typography>
      </Dialog>

      <Button color="primary" onClick={() => setOpen2(!open2)}>
        Open Fullwidth Dialog
      </Button>

      <Dialog
        open={open2}
        onClose={() => setOpen2(!open2)}
        title="This is a title for a modal dialog"
        description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. "
        showImageMobile
        image="/assets/img/block-example.svg"
        contentSpacing={2}
        fullWidthContent
        buttonPrimary={{
          label: "Create new account",
          onClick: () => setOpen2(!open2),
          disabled: true,
        }}
        buttonSecondary={{
          label: "Links and terciary buttons",
          to: "https://cleverecommerce.com",
          disabled: true,
        }}
      >
        <Typography>
          This is the dialog content Nam sagittis erat eu nibh accumsan, ut
          consectetur turpis dignissim.
        </Typography>
      </Dialog>

      <Button color="primary" onClick={() => setOpen3(true)}>
        Open DarkMode Dialog
      </Button>

      <Dialog
        open={open3}
        title="This is a title for a modal dialog"
        description="This is the description placeholder for any modal dialog. Any component would be optional and depends on its functional purpose."
        showImageMobile
        darkMode
        onClose={() => setOpen3(!open3)}
        buttonPrimary={{
          label: "Create new account",
          onClick: () => setOpen3(false),
        }}
      >
        <Typography>This is the dialog content darkmode</Typography>
      </Dialog>

      <Button color="primary" onClick={() => setOpen4(true)}>
        Open empty dialog
      </Button>

      <Dialog
        variant="empty"
        open={open4}
        // onClose={handleOpen}
        onClose={() => setOpen4(false)}
        maxWidth="sm"
        buttonPrimary={{
          label: "Create new account",
          onClick: () => setOpen4(false),
        }}
      >
        <Typography>This is the dialog content</Typography>
      </Dialog>
    </>
  );
};

export default Dialogs;
