/* eslint-disable import/no-duplicates */
import React from "react";

import makeStyles from "clever-react/ui/styles/makeStyles";

import Typography from "clever-react/ui/Typography";
import Icon from "clever-react/ui/Icon";
import Tooltip from "clever-react/ui/Tooltip";

import { icons } from "clever-react/ui/styles/icons";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  icon: { ...spacing["mx-1"] },
}));

const Icons = () => {
  const classes = getClasses();

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Icons
      </Typography>

      {icons.map(icon => (
        <Tooltip key={`icon-tooltip-${icon}`} title={icon}>
          <Icon icon={icon} className={classes.icon} size={24} />
        </Tooltip>
      ))}
    </>
  );
};

export default Icons;
