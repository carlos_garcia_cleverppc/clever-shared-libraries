import React from "react";

import Typography from "clever-react/ui/Typography";
import Popper from "clever-react/ui/Popper";
import IconButton from "clever-react/ui/IconButton";
import Menu from "clever-react/ui/Icon/Menu";
import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({
  card: { borderRadius: "8px" },
  title: { color: "red" },
}));

const Poppers = () => {
  const classes = getClasses();

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleOpen = event => {
    setAnchorEl(anchorEl ? null : event.currentTarget);
  };

  const open = Boolean(anchorEl);
  const id = open ? "transitions-popper" : undefined;

  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        Popper
      </Typography>

      <IconButton
        icon={<Menu />}
        color="neutral"
        shade={900}
        onClick={handleOpen}
        aria-describedby={id}
      >
        Open
      </IconButton>

      <Popper
        arrow
        placement="top-start"
        open={open}
        anchorEl={anchorEl}
        transition
        id={id}
        arrowColor="white"
        handleClose={() => setAnchorEl(null)}
        className={classes.card}
      >
        <h1 className={classes.title}>holaaaa</h1>
      </Popper>
    </div>
  );
};

export default Poppers;
