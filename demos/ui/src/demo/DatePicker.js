import React from "react";

import Typography from "clever-react/ui/Typography";
import DatePicker from "clever-react/ui/DatePicker";
import Calendar from "clever-react/ui/Icon/Calendar";

const DatePickerDemo = () => {
  const [date, setDate] = React.useState(new Date());

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Datepicker
      </Typography>

      <DatePicker
        onChange={setDate}
        value={date}
        label="Datepicker"
        disablePast
      />

      <DatePicker
        onChange={setDate}
        value={date}
        disablePast
        keyboardIcon={<Calendar color="primary" />}
        positionIcon="start"
        size="small"
        format="dd/MM/yyyy"
        type="keyboard"
        locale="fr"
      />

      <DatePicker
        onChange={setDate}
        value={date}
        disablePast
        keyboardIcon={<Calendar color="primary" />}
        size="small"
        locale="es"
      />
    </>
  );
};
export default DatePickerDemo;
