import React, { useState } from "react";

import Typography from "clever-react/ui/Typography";
import ChannelSelector from "clever-react/ui/ChannelSelector";

const ChannelSelectorPreview = () => {
  const [allActive, setAllActive] = useState(true);
  const [googleAdsActive, setGoogleAdsActive] = useState(false);
  const [facebookAdsActive, setFacebookAdsActive] = useState(false);
  const [twitterAdsActive, setTwitterAdsActive] = useState(false);
  const [microsoftAdsActive, setMicrosoftAdsActive] = useState(false);

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        ChannelSelector
      </Typography>

      <ChannelSelector
        allCopy="All"
        // position="horizontal"
        channels={{
          all: {
            connected: true,
            active: allActive,
            onClick: () => {
              setAllActive(!allActive);
            },
          },
          googleAds: {
            connected: true,
            active: googleAdsActive,
            onClick: () => {
              setGoogleAdsActive(!googleAdsActive);
            },
          },
          facebookAds: {
            connected: false,
            active: facebookAdsActive,
            onClick: setFacebookAdsActive,
          },
          twitterAds: {
            connected: true,
            active: twitterAdsActive,
            onClick: () => {
              setTwitterAdsActive(!twitterAdsActive);
            },
          },
          microsoftAds: {
            connected: false,
            active: microsoftAdsActive,
            onClick: setMicrosoftAdsActive,
          },
        }}
      />
    </>
  );
};

export default ChannelSelectorPreview;
