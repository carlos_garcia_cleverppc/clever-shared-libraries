import React from "react";
import { version } from "../../package.json";

import Typography from "clever-react/ui/Typography";
import Grid from "clever-react/ui/Grid";

import Cards from "./cards";
import Buttons from "./buttons";
import Typo from "./typography";
import TextField from "./textfields";
import List from "./list";
import Select from "./select";
import RoundedSelect from "./roundedSelect";
import Radios from "./radios";
import Loading from "./loading";
import Checkbox from "./checkbox";
import Avatar from "./avatar";
import Colors from "./colors";
import Icons from "./icons";
import BrandIcons from "./brandIcons";
import Dialogs from "./dialogs";
import Badge from "./badge";
import Currency from "./currency";
import IconButton from "./iconButton";
import Slider from "./slider";
import Autocomplete from "./autocomplete";
import Backdrop from "./backdrop";
import Chips from "./chip";
import ActionList from "./actionList";
import Divider from "./divider";
import MultiSelect from "./multiSelect";
import Tooltip from "./tooltip";
import Tabs from "./tabs";
import Table from "./table";
import Skeleton from "./skeleton";
import Drawer from "./drawer";
import Blocks from "./blocks";
import Menu from "./menu";
import BarGraph from "./barGraph";
import BarGraphMultiple from "./barGraphMultiple";
import ProgressBar from "./progressBar";
import RoundedProgressBar from "./RoundedProgressBar";
import RoundedButton from "./roundedButton";
import RoundedSwitch from "./roundedSwitch";
import Switch from "./switches";
import FloatButton from "./floatButton";
import Popper from "./popper";
import ToggleButton from "./toggleButtonGroup";
import FloatingMenu from "./floatingMenus";
import CheckboxTree from "./checkboxTree";
import BudgetSelector from "./budgetSelector";
import BudgetManagement from "./budgetManagement";
import ModalInput from "./modalInput";
/* THESE COMPONENTS ARE IN SIDEBARCONTAINER */
// import PlatformLogo from "./PlatformLogo";
// import SidebarItem from "./SidebarItem";
// import UserAccount from "./UserAccount";
// import FooterActions from "./footerActions";
// import Footer from "./footer";
import SidebarContainer from "./SidebarContainer";
import Popover from "./popover";
import Snackbar from "./snackbar";
import AlertCard from "./alertCard";
import Accordion from "./Accordion";
import Flags from "./flags";
import DatePicker from "./DatePicker";
import TimePicker from "./timePicker";
import AdPreview from "./adPreview";
import PromoCard from "./promoCard";
import ChannelSelector from "./channelSelector";
import FeedbackThumb from "./feedbackThumbs";
import StickyNotification from "./stickyNotification";
import FeedbackCard from "./feedbackCard";
import StarsRating from "./starsRating";
import BorderedBlocks from "./borderedBlocks";
import ChipTextfield from "./chipTextFields";
import ImageUploader from "./imageUploader";
import ChallengeBar from "./challengeBar";
import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  title: { ...spacing["my-6"] },
}));

const Demo = () => {
  const classes = getClasses();

  return (
    <>
      <FloatButton />

      <SidebarContainer />

      <Grid container justify="center">
        <Grid item md={11} lg={10}>
          <Typography
            variant="h1"
            shade={900}
            className={classes.title}
            gutterBottom
          >
            Clever UI - v{version}
          </Typography>

          <Grid container spacing={3}>
            <Grid item xs={12} md={8}>
              <Colors />
            </Grid>

            <Grid item xs={12} sm={4}>
              <Typo />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} md={6}>
              <Icons />

              <BrandIcons />
            </Grid>

            <Grid item xs={12} sm={6}>
              <Buttons />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} md={8}>
              <Popover />
            </Grid>

            <Grid item xs={12} md={4}>
              <ChallengeBar />
            </Grid>

            <Grid item xs={12} md={4}>
              <FeedbackThumb />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} sm={4}>
              <TextField />
            </Grid>

            <Grid item xs={12} sm={4}>
              <Cards />
            </Grid>

            <Grid item xs={12} sm={4}>
              <Tooltip />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} sm={4}>
              <RoundedSelect />
            </Grid>

            <Grid item xs={12} md={4}>
              <RoundedSwitch />
            </Grid>

            <Grid item xs={12} md={2}>
              <Switch />
            </Grid>

            <Grid item xs={12} md={2}>
              <PromoCard />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} md={5}>
              <ChipTextfield />
            </Grid>

            <Grid item xs={12} md={4}>
              <RoundedButton />
            </Grid>

            <Grid item xs={12} md={3}>
              <StarsRating />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} sm={3}>
              <Select />
            </Grid>

            <Grid item xs={12} md={3}>
              <Autocomplete />
            </Grid>

            <Grid item xs={12} sm={3}>
              <Radios />
            </Grid>

            <Grid item xs={12} sm={3}>
              <Checkbox />
            </Grid>

            <Grid item xs={12} sm={3}>
              <Avatar />
            </Grid>

            {/* <Grid item xs={12} sm={3}>
              <PlatformLogo />
            </Grid>

            <Grid item xs={12} md={3}>
              <SidebarItem />
            </Grid>

            <Grid item xs={12} sm={3}>
              <UserAccount />
            </Grid>

            <Grid item xs={12} sm={3}>
              <FooterActions />
            </Grid>

            <Grid item xs={12} sm={3}>
              <Footer />
            </Grid> */}

            <Grid item xs={12} sm={3}>
              <Snackbar />
            </Grid>

            <Grid item xs={12} md={3}>
              <Flags />
            </Grid>

            <Grid item xs={12} md={3}>
              <ImageUploader />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} md={4}>
              <IconButton />
            </Grid>

            <Grid item xs={12} sm={4}>
              <Currency />
            </Grid>

            <Grid item xs={12} md={4}>
              <Dialogs />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} md={6}>
              <FloatingMenu />
            </Grid>

            <Grid item xs={12} md={3}>
              <BarGraph />
            </Grid>

            <Grid item xs={12} md={3}>
              <BarGraphMultiple />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} md={3}>
              <List />
            </Grid>

            <Grid item xs={12} md={3}>
              <Loading />
            </Grid>

            <Grid item xs={12} sm={3}>
              <Skeleton />
            </Grid>

            <Grid item xs={12} md={3}>
              <Backdrop />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} md={3}>
              <Popper />
            </Grid>

            <Grid item xs={12} md={3}>
              <Badge />
            </Grid>

            <Grid item xs={12} md={3}>
              <Chips />
            </Grid>

            <Grid item xs={12} md={3}>
              <CheckboxTree />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} md={6}>
              <ActionList />
            </Grid>

            <Grid item xs={12} md={6}>
              <MultiSelect />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} md={3}>
              <Drawer />
            </Grid>

            <Grid item xs={12} md={3}>
              <Menu />
            </Grid>

            <Grid item xs={12} md={6}>
              <Tabs />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} md={4}>
              <Popover />
            </Grid>

            <Grid item xs={12} md={4}>
              <ToggleButton />
            </Grid>

            <Grid item xs={12} md={4}>
              <BudgetSelector />
            </Grid>

            <Grid item xs={12} md={4}>
              <BudgetManagement />
            </Grid>

            <Grid item xs={12} md={4}>
              <ModalInput />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} md={4}>
              <Divider />
            </Grid>

            <Grid item xs={12} md={4}>
              <ProgressBar />
            </Grid>

            <Grid item xs={12} md={4}>
              <RoundedProgressBar />
            </Grid>

            <Grid item xs={12} md={4}>
              <Slider />
            </Grid>

            <Grid item xs={12} md={3}>
              <AlertCard />
            </Grid>

            <Grid item xs={12} md={3}>
              <Accordion />
            </Grid>

            <Grid item xs={12} md={3}>
              <DatePicker />
            </Grid>

            <Grid item xs={12} md={3}>
              <TimePicker />
            </Grid>

            <Grid item xs={12} md={6}>
              <AdPreview />
            </Grid>

            <Grid item xs={12} md={3}>
              <ChannelSelector />
            </Grid>

            <Grid item xs={12} md={3}>
              <StickyNotification />
            </Grid>

            <Grid item xs={12} md={3}>
              <FeedbackCard />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} md={6}>
              <Blocks />
            </Grid>

            <Grid item xs={12} md={6}>
              <Table />
            </Grid>

            <Grid item xs={12} />

            <Grid item xs={12} md={6}>
              <BorderedBlocks />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default Demo;
