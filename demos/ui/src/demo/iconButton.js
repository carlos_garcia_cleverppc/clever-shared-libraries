/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";

import makeStyles from "clever-react/ui/styles/makeStyles";

import Typography from "clever-react/ui/Typography";
import IconButton from "clever-react/ui/IconButton";
import Times from "clever-react/ui/Icon/Times";
import Link from "clever-react/ui/Icon/Link";
import Bell from "clever-react/ui/Icon/Bell";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  icon: { ...spacing["mx-1"] },
}));

const IconButtons = () => {
  const classes = getClasses();
  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        Icon Buttons
      </Typography>

      <IconButton
        className={classes.icon}
        target="_blank"
        size="small"
        icon={<Times />}
      />
      <IconButton
        className={classes.icon}
        target="_blank"
        color="primary"
        icon={<Link />}
        href="www.google.es"
        iconSize={10}
        outlined={false}
      />
      <IconButton
        className={classes.icon}
        target="_blank"
        color="primary"
        icon={<Link />}
        iconSize={20}
        active
      />
      <IconButton
        className={classes.icon}
        target="_blank"
        color="primary"
        icon={<Link />}
        disabled
      />
      {/* If square -> outlined=true */}
      <IconButton
        className={classes.icon}
        target="_blank"
        color="primary"
        hover="square"
        outlined={false}
        icon={<Link />}
      />
      <IconButton className={classes.icon} target="_blank" icon={<Link />} />
      <IconButton
        className={classes.icon}
        target="_blank"
        hover="zoomIn"
        icon={<Bell />}
      />
    </div>
  );
};

export default IconButtons;
