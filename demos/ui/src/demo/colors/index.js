import React from "react";
import capitalize from "lodash-es/capitalize";

import Tooltip from "clever-react/ui/Tooltip";
import Typography from "clever-react/ui/Typography";
import Link from "clever-react/ui/Link";
import useTheme from "clever-react/ui/styles/useTheme";
import Snackbar from "clever-react/ui/Snackbar";

import makeStyles from "clever-react/ui/styles/makeStyles";
import colorsList from "clever-react/ui/Provider/colors";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  row: { textAlign: "center" },
  color: { marginRight: "20px", "&:hover": { cursor: "pointer" } },
  cell: { ...spacing["p-3"] },
  blob: {
    width: "50px",
    height: "50px",
    borderRadius: "50%",
    marginBottom: "10px",
  },
}));

const ColorsPreview = () => {
  const classes = getClasses();

  const [open, setOpen] = React.useState(false);

  const {
    cleverUI: { colors },
  } = useTheme();

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade={900} gutterBottom>
        Main Colors
      </Typography>

      <Typography variant="body1" gutterBottom>
        Click on the color to copy its hex code. Check the full list of colors{" "}
        <Link
          external
          target="_blank"
          to="http://clever-react.cleverecommerce.com/?path=/story/colors--main-colors"
        >
          here
        </Link>
        .
      </Typography>

      {Object.keys(colorsList.main).map(c => (
        <div
          key={`block-${c}`}
          style={{
            display: "flex",
            alignItems: "center",
            marginBottom: "20px",
          }}
        >
          <div style={{ width: "150px" }}>
            <Typography shade={900}>{capitalize(c)}</Typography>
          </div>

          {[100, 300, 500, 700, 900].map(shade => {
            if (colors[c][shade]) {
              return (
                <div
                  key={`block-${c}-${shade}`}
                  onClick={() => {
                    navigator.clipboard.writeText(`colors.${c}.${shade}`);
                    setOpen(true);
                  }}
                  className={classes.color}
                >
                  <Tooltip title={colors[c][shade]}>
                    <div
                      className={classes.blob}
                      style={{ backgroundColor: colors[c][shade] }}
                    />
                  </Tooltip>

                  <Typography variant="body0" shade={700} align="center">
                    {shade}
                  </Typography>
                </div>
              );
            }
            return "";
          })}
        </div>
      ))}

      <Snackbar
        open={open}
        onClose={handleClose}
        autoHideDuration={4000}
        text="Copied to clipboard!"
        anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      />
    </div>
  );
};

export default ColorsPreview;
