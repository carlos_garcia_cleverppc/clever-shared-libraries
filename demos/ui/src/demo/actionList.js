/* eslint-disable no-console */
import React from "react";

import Grid from "@material-ui/core/Grid";

import Typography from "clever-react/ui/Typography";
import ActionList from "clever-react/ui/ActionList";

const listInfo = [
  {
    id: "1",
    label:
      "The Shawshank Redemption The Shawshank Redemption The Shawshank Redemption The Shawshank Redemption ",
  },
  {
    id: "2",
    label:
      "The Godfatherrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr",
  },
  { id: "3", label: "The Godfather: Part II" },
  { id: "4", label: "The Dark Knight" },
  {
    id: "5",
    label:
      "12 Angry Menasdfsadfsadfsadfsadfasdfasdfsadfsdfsadfsadfsadf fasdf sdaf asdf sadf 12 Angry Menasdfsadfsadfsadfsadfasdfasdfsadfsdfsadfsadfsadf fasdf sdaf asdf sadf ",
  },
  { id: "6", label: "Schindler's List" },
  { id: "7", label: "Pulp Fiction" },
  { id: "8", label: "The Lord of the Rings: The Return of the King" },
  { id: "10", label: "The Good, the Bad and the Ugly" },
  {
    id: "11",
    label:
      "The Good, the Bad and the Uglyfffffffffffffffffffffffffffffffffff ddddddddddddd",
  },
  { id: "12", label: "The Good, the Bad and the Ugly" },
  {
    id: "13",
    label:
      "The Good, the Bad and the Uglyaaaaaaaaaaaaaaaaaaaaaaaaaaaaa sdsssssssssssss",
  },
  { id: "14", label: "The Shawshank Redemption" },
  {
    id: "15",
    label:
      "The Godfatheraaaaaaaaaaaaaaaaaaaaaaaaaa dsfdfffffffffffffffffffffff",
  },
  { id: "16", label: "The Godfather: Part II" },
  { id: "17", label: "The Dark Knight" },
  {
    id: "18",
    label:
      "12 Angry Menasdfsadfsadfsadfsadfasdfasdfsadfsdfsadfsadfsadf fasdf sdaf asdf sadf ",
  },
  { id: "19", label: "Schindler's List" },
  { id: "20", label: "Pulp Fiction" },
  { id: "21", label: "The Lord of the Rings: The Return of the King" },
  { id: "22", label: "The Good, the Bad and the Ugly" },
  {
    id: "23",
    label:
      "The Good, the Bad and the Uglyfffffffffffffffffffffffffffffffffff ddddddddddddd",
  },
  { id: "24", label: "The Good, the Bad and the Ugly" },
  {
    id: "25",
    label:
      "The Good, the Bad and the Uglyaaaaaaaaaaaaaaaaaaaaaaaaaaaaa sdsssssssssssss",
  },
];

const listInfo2 = [
  {
    id: "1",
    label:
      "The Shawshank Redemption The Shawshank Redemption The Shawshank Redemption The Shawshank Redemption ",
  },
  { id: "2", label: "The Godfather" },
];

const ActionListPreview = () => {
  const handleClick = (event, data) => {
    console.log("click", data);
  };

  return (
    <Grid container>
      <Grid item xs={12}>
        <Typography variant="h2" align="left" gutterBottom shade={900}>
          ActionList
        </Typography>
      </Grid>

      <Grid item xs={6}>
        <ActionList
          variant="primary"
          listInfo={listInfo}
          onClick={handleClick}
          action="remove"
        />
      </Grid>

      <Grid item xs={6}>
        <ActionList
          variant="primary"
          listInfo={listInfo2}
          onClick={handleClick}
          action="add"
          emptyText="No hay"
        />
      </Grid>
    </Grid>
  );
};

export default ActionListPreview;
