/* eslint-disable no-console */
import React from "react";

import Button from "clever-react/ui/Button";

import Typography from "clever-react/ui/Typography";
import Popover from "clever-react/ui/Popover";
import makeStyles from "clever-react/ui/styles/makeStyles";
import Grid from "clever-react/ui/Grid";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  popover: {
    ...spacing["p-4"],
    width: "250px",
  },
}));

/*  IMPORTANT  */
/* Popover must be outside button or div who open it */

const Popovers = () => {
  const classes = getClasses();

  const [anchorEl1, setAnchorEl1] = React.useState(null);
  const open1 = Boolean(anchorEl1);
  const id1 = open1 ? "simple-popover1" : undefined;

  const handleClick1 = event => {
    setAnchorEl1(event.currentTarget);
  };

  const [anchorEl2, setAnchorEl2] = React.useState(null);
  const open2 = Boolean(anchorEl2);
  const id2 = open2 ? "simple-popover2" : undefined;

  const handleClick2 = event => {
    setAnchorEl2(event.currentTarget);
  };

  const [anchorEl3, setAnchorEl3] = React.useState(null);
  const open3 = Boolean(anchorEl3);
  const id3 = open3 ? "simple-popover3" : undefined;

  const handleClick3 = event => {
    setAnchorEl3(event.currentTarget);
  };

  const [anchorEl4, setAnchorEl4] = React.useState(null);
  const open4 = Boolean(anchorEl4);
  const id4 = open4 ? "simple-popover4" : undefined;

  const handleClick4 = event => {
    setAnchorEl4(event.currentTarget);
  };

  const [anchorEl5, setAnchorEl5] = React.useState(null);
  const open5 = Boolean(anchorEl5);
  const id5 = open5 ? "simple-popover5" : undefined;

  const handleClick5 = event => {
    setAnchorEl5(event.currentTarget);
  };

  const [anchorEl6, setAnchorEl6] = React.useState(null);
  const open6 = Boolean(anchorEl6);
  const id6 = open6 ? "simple-popover6" : undefined;

  const handleClick6 = event => {
    setAnchorEl6(event.currentTarget);
  };

  const [anchorEl7, setAnchorEl7] = React.useState(null);
  const open7 = Boolean(anchorEl7);
  const id7 = open7 ? "simple-popover7" : undefined;

  const handleClick7 = event => {
    setAnchorEl7(event.currentTarget);
  };

  const [anchorEl8, setAnchorEl8] = React.useState(null);
  const open8 = Boolean(anchorEl8);
  const id8 = open8 ? "simple-popover8" : undefined;

  const handleClick8 = event => {
    setAnchorEl8(event.currentTarget);
  };

  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        Popovers
      </Typography>

      <Grid container spacing={1} justify="center">
        <Grid item xs={12}>
          <Grid container spacing={1} justify="center">
            <Grid item xs={4}>
              <Button onClick={handleClick1} aria-describedby={id1}>
                top-left
              </Button>
            </Grid>

            <Grid item xs={4}>
              <Button onClick={handleClick2} aria-describedby={id2}>
                top-center
              </Button>
            </Grid>

            <Grid item xs={4}>
              <Button onClick={handleClick3} aria-describedby={id3}>
                top-right
              </Button>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Grid container spacing={1} justify="center">
            <Grid item xs={4}>
              <Button onClick={handleClick5} aria-describedby={id5}>
                center-right
              </Button>
            </Grid>

            <Grid item xs={4} />

            <Grid item xs={4}>
              <Button onClick={handleClick4} aria-describedby={id4}>
                center-left
              </Button>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Grid container spacing={1} justify="center">
            <Grid item xs={4}>
              <Button onClick={handleClick6} aria-describedby={id6}>
                bottom-left
              </Button>
            </Grid>

            <Grid item xs={4}>
              <Button onClick={handleClick7} aria-describedby={id7}>
                bottom-center
              </Button>
            </Grid>

            <Grid item xs={4}>
              <Button onClick={handleClick8} aria-describedby={id8}>
                bottom-right
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>

      <Popover
        arrow
        arrowPosition="center"
        id={id1}
        open={open1}
        anchorEl={anchorEl1}
        onClose={() => setAnchorEl1(null)}
        placement="top-left"
        className={classes.popover}
      >
        <Typography variant="h2" shade={900} gutterBottom>
          Buttons and Links
        </Typography>

        <Grid container spacing>
          <Grid item xs>
            <Button
              color="warning"
              onClick={() => console.log("You click the link")}
            >
              Next
            </Button>
          </Grid>

          <Grid item xs>
            <Button onClick={() => console.log("You click the link")}>
              Next
            </Button>
          </Grid>
        </Grid>
      </Popover>

      <Popover
        arrow
        arrowPosition="center"
        id={id2}
        open={open2}
        anchorEl={anchorEl2}
        onClose={() => setAnchorEl2(null)}
        placement="top-center"
        className={classes.popover}
      >
        <Typography variant="h2" shade={900} gutterBottom>
          Buttons and Links
        </Typography>

        <Grid container spacing>
          <Grid item xs>
            <Button
              color="warning"
              onClick={() => console.log("You click the link")}
            >
              Next
            </Button>
          </Grid>

          <Grid item xs>
            <Button onClick={() => console.log("You click the link")}>
              Next
            </Button>
          </Grid>
        </Grid>
      </Popover>

      <Popover
        arrow
        arrowPosition="center"
        id={id3}
        open={open3}
        anchorEl={anchorEl3}
        onClose={() => setAnchorEl3(null)}
        placement="top-right"
        className={classes.popover}
      >
        <Typography variant="h2" shade={900} gutterBottom>
          Buttons and Links
        </Typography>

        <Grid container spacing>
          <Grid item xs>
            <Button
              color="warning"
              onClick={() => console.log("You click the link")}
            >
              Next
            </Button>
          </Grid>

          <Grid item xs>
            <Button onClick={() => console.log("You click the link")}>
              Next
            </Button>
          </Grid>
        </Grid>
      </Popover>

      <Popover
        arrow
        arrowPosition="center"
        id={id4}
        open={open4}
        anchorEl={anchorEl4}
        onClose={() => setAnchorEl4(null)}
        placement="center-left"
        className={classes.popover}
      >
        <Typography variant="h2" shade={900} gutterBottom>
          Buttons and Links
        </Typography>

        <Grid container spacing>
          <Grid item xs>
            <Button
              color="warning"
              onClick={() => console.log("You click the link")}
            >
              Next
            </Button>
          </Grid>

          <Grid item xs>
            <Button onClick={() => console.log("You click the link")}>
              Next
            </Button>
          </Grid>
        </Grid>
      </Popover>

      <Popover
        arrow
        arrowPosition="center"
        id={id5}
        open={open5}
        anchorEl={anchorEl5}
        onClose={() => setAnchorEl5(null)}
        placement="center-right"
        className={classes.popover}
      >
        <Typography variant="h2" shade={900} gutterBottom>
          Buttons and Links
        </Typography>

        <Grid container spacing>
          <Grid item xs>
            <Button
              color="warning"
              onClick={() => console.log("You click the link")}
            >
              Next
            </Button>
          </Grid>

          <Grid item xs>
            <Button onClick={() => console.log("You click the link")}>
              Next
            </Button>
          </Grid>
        </Grid>
      </Popover>

      <Popover
        arrow
        arrowPosition="center"
        id={id6}
        open={open6}
        anchorEl={anchorEl6}
        onClose={() => setAnchorEl6(null)}
        placement="bottom-left"
        className={classes.popover}
      >
        <Typography variant="h2" shade={900} gutterBottom>
          Buttons and Links
        </Typography>

        <Grid container spacing>
          <Grid item xs>
            <Button
              color="warning"
              onClick={() => console.log("You click the link")}
            >
              Next
            </Button>
          </Grid>

          <Grid item xs>
            <Button onClick={() => console.log("You click the link")}>
              Next
            </Button>
          </Grid>
        </Grid>
      </Popover>

      <Popover
        arrow
        arrowPosition="center"
        id={id7}
        open={open7}
        anchorEl={anchorEl7}
        onClose={() => setAnchorEl7(null)}
        placement="bottom-center"
        className={classes.popover}
      >
        <Typography variant="h2" shade={900} gutterBottom>
          Buttons and Links
        </Typography>

        <Grid container spacing>
          <Grid item xs>
            <Button
              color="warning"
              onClick={() => console.log("You click the link")}
            >
              Next
            </Button>
          </Grid>

          <Grid item xs>
            <Button onClick={() => console.log("You click the link")}>
              Next
            </Button>
          </Grid>
        </Grid>
      </Popover>

      <Popover
        arrow
        arrowPosition="center"
        id={id8}
        open={open8}
        anchorEl={anchorEl8}
        onClose={() => setAnchorEl8(null)}
        placement="bottom-right"
        className={classes.popover}
      >
        <Typography variant="h2" shade={900} gutterBottom>
          Buttons and Links
        </Typography>

        <Grid container spacing>
          <Grid item xs>
            <Button
              color="warning"
              onClick={() => console.log("You click the link")}
            >
              Next
            </Button>
          </Grid>

          <Grid item xs>
            <Button onClick={() => console.log("You click the link")}>
              Next
            </Button>
          </Grid>
        </Grid>
      </Popover>
    </div>
  );
};

export default Popovers;
