import React, { useState } from "react";

import Calendar from "clever-react/ui/Icon/Calendar";
import Balance from "clever-react/ui/Icon/Balance";
import Typography from "clever-react/ui/Typography";
import Select from "clever-react/ui/Select";
import Button from "clever-react/ui/Button";
import ModalInput from "clever-react/ui/ModalInput";
import Snackbar from "clever-react/ui/Snackbar";

const ModalInputs = () => {
  const [openModalInput, setOpenModalInput] = useState(false);
  const [inputValue, setInputValue] = useState("");
  const [metric, setMetric] = useState("");
  const [openSnackbar, setOpenSnackbar] = useState(false);

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Modal Input
      </Typography>

      <Button onClick={() => setOpenModalInput(true)}>Open Modal Input</Button>

      <ModalInput
        open={openModalInput}
        onSubmit={value => {
          setInputValue(value);
          setOpenSnackbar(true);
          setOpenModalInput(false);
        }}
        inputProps={{
          value: inputValue,
          onChange: ev => setInputValue(ev.target.value),
        }}
        buttonDisabled
        onClose={() => setOpenModalInput(false)}
        icon={<Calendar />}
        title="Modal input title"
        placeholder="Enter your value here..."
        buttonText="Submit text"
        menuOptions={[
          {
            label: "Option 1",
            onClick: () => {
              setInputValue("Option 1 clicked!");
              setOpenSnackbar(true);
            },
          },
          {
            label: "Option 2",
            onClick: () => {
              setInputValue("Option 2 clicked!");
              setOpenSnackbar(true);
            },
            color: "danger",
          },
        ]}
        footer={
          <>
            <Select
              name="select_metric"
              value={metric}
              onChange={ev => setMetric(ev.target.value)}
              options={[{ label: "metric1", value: "1" }]}
              size="small"
              startAdornment={<Balance color="primary" />}
              placeholder="Choose a metric"
            />
          </>
        }
      />

      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
        open={openSnackbar}
        autoHideDuration={3000}
        onClose={() => setOpenSnackbar(false)}
        text={inputValue}
      />
    </>
  );
};

export default ModalInputs;
