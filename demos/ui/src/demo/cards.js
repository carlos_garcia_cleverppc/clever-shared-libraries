import React from "react";

import Typography from "clever-react/ui/Typography";
import Card from "clever-react/ui/Card";
import Grid from "clever-react/ui/Grid";

const Cards = () => (
  <div>
    <Typography variant="h2" shade={900} gutterBottom>
      Cards
    </Typography>

    <Grid container spacing>
      <Grid item xs={12}>
        <Card>This is a default card.</Card>
      </Grid>

      <Grid item xs={12}>
        <Card clickable>This is a default clickable card.</Card>
      </Grid>

      <Grid item xs={12}>
        <Card variant="striped">
          This is a striped card This is a striped card This is a striped card
          This is a striped card This is a striped card This is a striped card
          This is a striped card This is a striped card This is a striped card
        </Card>
      </Grid>
    </Grid>
  </div>
);

export default Cards;
