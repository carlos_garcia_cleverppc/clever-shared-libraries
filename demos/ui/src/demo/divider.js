import React from "react";

import Typography from "clever-react/ui/Typography";
import Divider from "clever-react/ui/Divider";
import Grid from "clever-react/ui/Grid";
import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({
  root: { width: "400px" },
  vertical: { height: "30px" },
}));

const Dividers = () => {
  const classes = getClasses();

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Divider
      </Typography>

      <Grid container spacing>
        <Grid item xs={12}>
          <Divider className={classes.root} />
        </Grid>

        <Grid item xs={12}>
          <Divider className={classes.vertical} position="vertical" />
        </Grid>
      </Grid>
    </>
  );
};

export default Dividers;
