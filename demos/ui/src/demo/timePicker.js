import React from "react";

import Typography from "clever-react/ui/Typography";
import TimePicker from "clever-react/ui/TimePicker";
import Clock from "clever-react/ui/Icon/Clock";

const TimePickerDemo = () => {
  const [time, setTime] = React.useState(new Date());

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        TimePicker
      </Typography>

      <TimePicker
        onChange={setTime}
        value={time}
        label="TimePicker"
        disablePast
      />

      <TimePicker
        onChange={setTime}
        value={time}
        disablePast
        keyboardIcon={<Clock color="primary" />}
        positionIcon="start"
        size="small"
        type="keyboard"
        locale="fr"
      />

      <TimePicker
        onChange={setTime}
        value={time}
        disablePast
        keyboardIcon={<Clock color="primary" />}
        size="small"
        locale="es"
      />
    </>
  );
};
export default TimePickerDemo;
