import React from "react";

import MenuItem from "clever-react/ui/Menu/Item";
import Menu from "clever-react/ui/Menu";
import Button from "clever-react/ui/Button";
import Typography from "clever-react/ui/Typography";

const MenuPreview = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Menu
      </Typography>

      <Button onClick={handleClick}>Open Menu</Button>

      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        keepMounted
      >
        <MenuItem onClick={handleClose}>Profile</MenuItem>
        <MenuItem onClick={handleClose}>My account</MenuItem>
        <MenuItem onClick={handleClose}>Logout</MenuItem>
      </Menu>
    </>
  );
};

export default MenuPreview;
