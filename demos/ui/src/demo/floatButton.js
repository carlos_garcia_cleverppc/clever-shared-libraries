/* eslint-disable no-console */
import React from "react";
import { CSSTransition } from "react-transition-group";

import MenuList from "clever-react/ui/Menu/MenuList";
import MenuItem from "clever-react/ui/Menu/Item";

import FloatButton from "clever-react/ui/FloatButton";
import Typography from "clever-react/ui/Typography";

import Grid from "clever-react/ui/Grid";

import ShoppingCart from "clever-react/ui/Icon/ShoppingCart";
import Calendar from "clever-react/ui/Icon/Calendar";
import Megaphone from "clever-react/ui/Icon/Megaphone";
import Discount from "clever-react/ui/Icon/Discount";
import ChevronRight from "clever-react/ui/Icon/ChevronRight";
import ArrowRight from "clever-react/ui/Icon/ArrowRight";
import Plus from "clever-react/ui/Icon/Plus";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { colors, spacing } }) => ({
  button: { bottom: "20px" },
  button2: { bottom: "120px" },
  paper: {
    ...spacing["mb-4"],
    borderRadius: "8px",
    maxWidth: "350px",
    backgroundColor: colors.secondary[500],
  },
  paper2: {
    ...spacing["p-4"],
    borderRadius: "8px",
    backgroundColor: colors.neutral[500],
  },
  content: {
    width: "fit-content",
    "&:hover": { borderRadius: "8px" },
  },
  inline: {
    display: "inline-flex",
    justifyContent: "center",
    alignItems: "center",
  },
  item: {
    ...spacing["py-2"],
    "& span": { backgroundColor: "white" },
  },
  title: {
    ...spacing["py-2"],
    display: "flex",
  },
  textTitle: {
    ...spacing["px-2"],
    fontWeight: "bold",
  },
  chevron: { ...spacing["mt-2"] },
  iconChevronOpen: { transform: "rotate(90deg)", marginTop: "10px" },
  iconPlusOpen: { transform: "rotate(45deg)", verticalAlign: "middle" },
  iconChevronClose: { transform: "rotate(-90deg)", marginTop: "10px" },
  iconPlusClose: { transform: "rotate(-90deg)", verticalAlign: "middle" },
  rotateEnter: { transform: "rotate(-90deg)" },
  rotateEnterActive: {
    transform: "rotate(90deg)",
    transition: "all 300ms cubic-bezier(.4,1.39,.93,1.01)",
  },
  rotateExit: { transform: "rotate(90deg)" },
  rotateExitActive: {
    transform: "rotate(-90deg)",
    transition: "all 300ms cubic-bezier(.4,1.39,.93,1.01)",
  },
  rotatePlusEnter: { transform: "rotate(-90deg)" },
  rotatePlusEnterActive: {
    transform: "rotate(45deg)",
    transition: "all 300ms cubic-bezier(.4,1.39,.93,1.01)",
  },
  rotatePlusExit: { transform: "rotate(45deg)" },
  rotatePlusExitActive: {
    transform: "rotate(-90deg)",
    transition: "all 300ms cubic-bezier(.4,1.39,.93,1.01)",
  },
  plusContainer: { height: "26px", width: "26px" },
  icons: { marginTop: "-2px", display: "flex", alignItems: "center" },
  iconRight: { marginLeft: "-3px" },
  iconLeft: { marginRight: "-3px" },
  icon: { ...spacing["pl-2"] },
  textButton: {
    display: "flex",
    textTransform: "capitalize",
  },
}));

const FloatButtons = () => {
  const classes = getClasses();

  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const [open2, setOpen2] = React.useState(false);
  const anchorRef2 = React.useRef(null);

  const actions = [
    {
      icon: <ShoppingCart size={24} color="neutral" shade={100} />,
      name: "hola",
      description:
        "hola hola hola hola hola hola hola hola hola hola hola hola hola hola",
      onClick: () => {
        console.log("/shopping");
      },
    },
    {
      icon: <Calendar size={24} color="neutral" shade={100} />,
      name: "hola2",
      description:
        "hola hola hola hola hola hola hola hola hola hola hola hola hola hola",
      onClick: () => {
        console.log("/marketing_activities");
      },
    },
    {
      icon: <Megaphone size={24} color="neutral" shade={100} />,
      name: "hola3",
      description:
        "hola hola hola hola hola hola hola hola hola hola hola hola hola hola",
      onClick: () => {
        console.log("/banners");
      },
    },
    {
      icon: <Discount size={24} color="neutral" shade={100} />,
      name: "hola4",
      description:
        "hola hola hola hola hola hola hola hola hola hola hola hola hola hola",
      onClick: () => {
        console.log("/discounts");
      },
    },
  ];

  const handleClick = action => event => {
    action.onClick();
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  const renderMenu = () =>
    actions.map(action => (
      <MenuItem
        onClick={handleClick(action)}
        className={classes.content}
        key={action.name}
      >
        <Grid container className={classes.item}>
          <div className={classes.inline}>
            <Grid item className={classes.title}>
              {action.icon}
            </Grid>

            <Grid item>
              <Typography
                color="neutral"
                shade={100}
                className={classes.textTitle}
              >
                {action.name}
              </Typography>
            </Grid>

            <Grid item>
              <ChevronRight
                size={24}
                color="neutral"
                shade={100}
                className={classes.chevron}
              />
            </Grid>
          </div>

          <Grid item xs={12}>
            <Typography color="secondary" shade={100} variant="body0">
              {action.description}
            </Typography>
          </Grid>
        </Grid>
      </MenuItem>
    ));

  return (
    <>
      <FloatButton
        open={open}
        setOpen={setOpen}
        anchorRef={anchorRef}
        buttonLabel={
          <div className={classes.icons}>
            <div className={classes.iconLeft}>
              <CSSTransition
                in={open}
                exit={!open}
                timeout={300}
                classNames={{
                  enter: classes.rotateEnter,
                  enterActive: classes.rotateEnterActive,
                  exit: classes.rotateExit,
                  exitActive: classes.rotateExitActive,
                }}
              >
                <ChevronRight
                  size={26}
                  color="neutral"
                  shade={100}
                  className={
                    open ? classes.iconChevronOpen : classes.iconChevronClose
                  }
                />
              </CSSTransition>
            </div>

            <div className={classes.iconRight}>
              <div className={classes.plusContainer}>
                <CSSTransition
                  in={open}
                  exit={!open}
                  timeout={300}
                  classNames={{
                    enter: classes.rotatePlusEnter,
                    enterActive: classes.rotatePlusEnterActive,
                    exit: classes.rotatePlusExit,
                    exitActive: classes.rotatePlusExitActive,
                  }}
                >
                  <Plus
                    size={15}
                    color="neutral"
                    shade={100}
                    className={
                      open ? classes.iconPlusOpen : classes.iconPlusClose
                    }
                  />
                </CSSTransition>
              </div>
            </div>
          </div>
        }
        buttonColor="secondary"
        classes={{
          paper: classes.paper,
          button: classes.button,
        }}
        content={
          <MenuList autoFocusItem={open} id="menu-list-grow">
            {renderMenu()}
          </MenuList>
        }
      />

      <FloatButton
        open={open2}
        setOpen={setOpen2}
        anchorRef={anchorRef2}
        variant="extended"
        buttonLabel={
          <div className={classes.textButton}>
            <Typography variant="body0" color="neutral" shade={100}>
              Feedback
            </Typography>

            <ArrowRight
              color="neutral"
              shade={100}
              size={12}
              className={classes.icon}
            />
          </div>
        }
        buttonColor="navyBlue"
        classes={{
          paper: classes.paper2,
          button: classes.button2,
        }}
        content={
          <>
            <Typography
              variant="body0"
              weight="bold"
              color="white"
              gutterBottom
            >
              Tu opinión es muy importante para nosotros.
            </Typography>

            <Typography variant="body0" weight="bold" color="white">
              ¡Nos encantaría saber qué opinas del nuevo Dashboard!
            </Typography>
          </>
        }
        onClick={() => console.log("Feedback button pressed")}
      />
    </>
  );
};

export default FloatButtons;
