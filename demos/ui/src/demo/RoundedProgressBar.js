import React from "react";

import Avatar from "clever-react/ui/Avatar";
import RoundedProgressBar from "clever-react/ui/RoundedProgressBar";
import Typography from "clever-react/ui/Typography";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({
  backdrop: { color: "#fff", zIndex: 1000 },
}));

const RoundedProgressBarPreview = () => {
  const classes = getClasses();

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade={900} gutterBottom>
        Rounded Progress Bar
      </Typography>

      <RoundedProgressBar
        size={145}
        progress={40}
        strokeWidth={4}
        progressesArray={[
          { color: "blue", progress: 20 },
          { color: "danger", progress: 5 },
          { color: "green", progress: 75 },
        ]}
      >
        <Avatar src="https://i.blogs.es/16b956/gmail-nuevo-logo-google-workspace/1366_2000.jpg" />
      </RoundedProgressBar>
    </div>
  );
};

export default RoundedProgressBarPreview;
