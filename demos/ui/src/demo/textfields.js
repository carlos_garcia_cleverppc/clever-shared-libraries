import React, { useState } from "react";

import Grid from "clever-react/ui/Grid";

import Typography from "clever-react/ui/Typography";
import TextField from "clever-react/ui/TextField";
import DatePicker from "clever-react/ui/DatePicker";
import Calendar from "clever-react/ui/Icon/Calendar";

import makeStyles from "clever-react/ui/styles/makeStyles";

// eslint-disable-next-line no-unused-vars
const getClasses = makeStyles(() => ({
  input: { "&::placeholder": { color: "red !important" } },
}));

const TextFields = () => {
  const classes = getClasses();

  const [value, setValue] = React.useState("Cat in the Hat");
  const [date, setDate] = React.useState("");
  const [selectedDate, handleDateChange] = useState(new Date());

  const handleChange = event => {
    setValue(event.target.value);
  };

  const [, setErrorType] = useState(false);

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        TextFields
      </Typography>

      <Grid container spacing={2}>
        <Grid item>
          <TextField
            type="search"
            value={value}
            error={value.length > 20}
            warning={value.length < 5}
            onChange={handleChange}
            label="*Shop target audience language"
          />
        </Grid>

        <Grid item>
          <TextField
            id="b"
            size="small"
            value={value}
            onChange={handleChange}
            checkErrorType="headline"
            hasErrorType={err => setErrorType(err)}
          />
        </Grid>

        <Grid item>
          <TextField
            id="c"
            value={value}
            onChange={handleChange}
            variant="standard"
            placeholder="holii"
          />
        </Grid>

        <Grid item>
          <TextField
            id="d"
            value={value}
            label="This is the placeholder"
            onChange={handleChange}
          />
        </Grid>

        <Grid item>
          <TextField
            id="e"
            label="End Date"
            // defaultValue="2017-05-24"
            value={date}
            onChange={ev => setDate(ev.target.value)}
            type="date"
            InputLabelProps={{ shrink: true }}
          />
        </Grid>

        <Grid item>
          <DatePicker
            id="g"
            label="Datepicker"
            disablePast
            value={selectedDate}
            onChange={handleDateChange}
          />
        </Grid>

        <Grid item>
          <DatePicker
            id="h"
            label="Datepicker"
            type="keyboard"
            disablePast
            value={selectedDate}
            onChange={handleDateChange}
          />
        </Grid>

        <Grid item>
          <DatePicker
            id="h"
            label="Select Datepicker"
            positionIcon="start"
            disablePast
            keyboardIcon={<Calendar color="primary" />}
            value={selectedDate}
            onChange={handleDateChange}
          />
        </Grid>

        <Grid item>
          <TextField
            id="f"
            value={value}
            placeholder="This is the placeholder"
            multiline
            rows={4}
            onChange={handleChange}
            inputClass={classes.input}
          />
        </Grid>
      </Grid>
    </>
  );
};

export default TextFields;
