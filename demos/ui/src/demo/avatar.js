import React from "react";

import Avatar from "clever-react/ui/Avatar";
import Typography from "clever-react/ui/Typography";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({
  backdrop: { color: "#fff", zIndex: 1000 },
}));

const AvatarPreview = () => {
  const classes = getClasses();

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade={900} gutterBottom>
        Avatar
      </Typography>

      <Avatar src="" />

      <Avatar src="https://i.blogs.es/16b956/gmail-nuevo-logo-google-workspace/1366_2000.jpg" />

      <Avatar colorCircle />

      <Avatar
        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQxpKVO-ARFzzfPtM-l3wwHcX3tdP328WS1iA&usqp=CAU"
        colorCircle
        label="PRO"
      />
    </div>
  );
};

export default AvatarPreview;
