import React from "react";

import FeedbackCard from "clever-react/ui/FeedbackCard";
import Typography from "clever-react/ui/Typography";

const FeedbackCardPreview = () => (
  <div>
    <Typography variant="h2" shade={900} gutterBottom>
      FeedbackCard
    </Typography>

    <FeedbackCard
      timeCopy="Hoy a las 16:32"
      showBadge
      comment="Hola! Acabo de recibir tu solicitud. En cuanto lo tenga, te lo envio por email."
      thumbDownSelected
      cardSelected
    />
  </div>
);

export default FeedbackCardPreview;
