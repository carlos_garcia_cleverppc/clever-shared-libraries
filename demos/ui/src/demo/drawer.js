import React from "react";

import Drawer from "clever-react/ui/Drawer";
import Typography from "clever-react/ui/Typography";
import Button from "clever-react/ui/Button";

const DrawerPreview = () => {
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(!open);
  };

  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        Drawer
      </Typography>

      <Button color="primary" onClick={handleOpen}>
        Open
      </Button>

      <Drawer open={open} onClose={handleOpen}>
        This is a drawer
      </Drawer>
    </div>
  );
};

export default DrawerPreview;
