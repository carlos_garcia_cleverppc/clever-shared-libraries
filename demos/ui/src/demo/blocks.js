import React from "react";

import Typography from "clever-react/ui/Typography";
import Block from "clever-react/ui/Block";
import Container from "clever-react/ui/Container";
import Item from "clever-react/ui/Container/Item";
import Clock from "clever-react/ui/Icon/Clock";
import Eye from "clever-react/ui/Icon/Eye";
import CirclePlus from "clever-react/ui/Icon/CirclePlus";

const Dialogs = () => {
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(!open);
  };

  const title = "Programa tus campañas estacionales";
  const description =
    "This is the description of the block. Lorem ipsum dolor sit amet. This is the description of the block. Lorem ipsum dolor sit amet.";

  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        Blocks
      </Typography>

      <Container
        rows={2}
        columns={4}
        justify="center"
        layout="blocks"
        maxWidth="blocks"
        columnSize="blocks"
      >
        <Item columns={1} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="secondary"
            variant="custom"
            size="1x1"
          >
            This is a custom block
          </Block>
        </Item>

        <Item columns={1} rows={1}>
          <Block
            onClick={() => window.open("http://google.com")}
            handleOpen={handleOpen}
            color="primary"
            variant="metric"
            description="conversiones"
            value={{
              value: "125",
              currency: "EUR",
            }}
          />
        </Item>

        <Item columns={1} rows={1}>
          <Block
            color="primary"
            variant="metric"
            description="conversiones"
            value="141"
            selected
          />
        </Item>

        <Item columns={1} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="primary"
            variant="metric"
            description="conversiones"
            value="141"
            active
          />
        </Item>

        <Item columns={1} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="neutral"
            variant="dashed"
            description="Dashed - Optimiza tus pujas por franja horaria"
            icon={<CirclePlus />}
          />
        </Item>

        <Item columns={1} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="neutral"
            variant="dashed"
            description="Active Dashed - Optimiza tus pujas por franja horaria"
            active
            icon={<CirclePlus />}
          />
        </Item>

        <Item columns={1} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="neutral"
            variant="icon"
            description="Optimiza tus pujas por franja horaria"
          />
        </Item>

        <Item columns={1} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="neutral"
            variant="icon"
            description="Optimiza tus pujas por franja horaria"
            applied
            icon={<Clock color="primary" />}
          />
        </Item>

        <Item columns={1} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="neutral"
            variant="icon"
            description="Optimiza tus pujas por franja horaria"
            active
            icon={<Clock color="primary" />}
          />
        </Item>

        <Item columns={1} rows={1}>
          <Block
            color="neutral"
            variant="icon"
            description="Optimiza tus pujas por franja horariaaaaa"
            icon={<Eye color="neutral" />}
          />
        </Item>

        <Item columns={1} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="primary"
            variant="icon"
            description="Optimiza tus pujas por franja horaria"
          />
        </Item>

        <Item columns={1} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="primary"
            variant="icon"
            description="Optimiza tus pujas por franja horaria"
            applied
            icon={<Clock color="primary" />}
          />
        </Item>

        <Item columns={1} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="christmas"
            variant="icon"
            description="Optimiza tus pujas por franja horaria"
            applied
            active
            icon={<Clock color="primary" />}
          />
        </Item>

        <Item columns={1} rows={1}>
          <Block
            color="christmas"
            variant="icon"
            description="Optimiza tus pujas por franja horariaaaaa"
            icon={<Eye color="neutral" />}
          />
        </Item>
      </Container>

      <Container rows={4} columns={4} layout="blocks">
        <Item columns={1} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="primary"
            title="aaaaaaaa aaaaaaaaaaa"
            description={description}
          />
        </Item>

        {true && (
          <Item columns={1} rows={1}>
            <Block
              color="primary"
              title={title}
              description={description}
              selected
            />
          </Item>
        )}

        <Item columns={1} rows={1}>
          <Block
            handleOpen={handleOpen}
            handleClose={() => setOpen(false)}
            color="primary"
            title={title}
            description={description}
            active
          />
        </Item>

        <Item columns={2} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="primary"
            title={title}
            description={description}
            link="Link example"
            image="/assets/img/block-example.svg"
            size="1x2"
            descriptionLines={2}
          />
        </Item>

        <Item columns={2} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="yellow"
            title={title}
            description={description}
            image="/assets/img/block-example.svg"
            size="1x2"
            descriptionLines={2}
          />
        </Item>

        <Item columns={2} rows={1}>
          <Block
            color="yellow"
            title={title}
            description={description}
            image="/assets/img/block-example.svg"
            size="1x2"
            selected
            descriptionLines={2}
          />
        </Item>

        <Item columns={2} rows={2}>
          <Block
            // handleOpen={handleOpen}
            color="magenta"
            title={title}
            description={description}
            link="link"
            image="/assets/img/block-example.svg"
            size="2x2"
          />
        </Item>

        <Item columns={2} rows={2}>
          <Block
            handleOpen={handleOpen}
            color="warning"
            title={title}
            description={description}
            size="2x2"
          />
        </Item>

        <Item columns={2} rows={2}>
          <Block
            // handleOpen={handleOpen}
            color="magenta"
            title={title}
            description={description}
            image="/assets/img/bullet-point.svg"
            size="2x2"
            selected
            imageSize="sm"
          />
        </Item>

        <Item columns={2} rows={2}>
          <Block
            handleOpen={handleOpen}
            color="magenta"
            title={title}
            description={description}
            image="/assets/img/block-example.svg"
            size="2x2"
            active
          />
        </Item>

        <Item columns={4} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="secondary"
            title={title}
            description={description}
            image="/assets/img/block-example.svg"
            size="1x4"
          />
        </Item>

        <Item columns={4} rows={1}>
          <Block
            handleClose={() => setOpen(false)}
            color="primary"
            title={title}
            description={description}
            image="/assets/img/block-example.svg"
            size="1x4"
            selected
          />
        </Item>

        <Item columns={4} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="secondary"
            title={title}
            description={description}
            image="/assets/img/block-example.svg"
            size="1x4"
            active
          />
        </Item>

        <Item columns={4} rows={1}>
          <Block
            handleOpen={handleOpen}
            color="primary"
            title={title}
            description={description}
            size="1x4"
          />
        </Item>
      </Container>
    </div>
  );
};

export default Dialogs;
