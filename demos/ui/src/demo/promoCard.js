import React from "react";

import Typography from "clever-react/ui/Typography";
import PromoCard from "clever-react/ui/PromoCard";

const PromoCards = () => (
  <>
    <Typography variant="h2" shade={900} gutterBottom>
      PromoCard
    </Typography>

    <PromoCard
      promoQuantity={120.0}
      promoCurrency="GBP"
      promoText="promotional discount"
    />
  </>
);

export default PromoCards;
