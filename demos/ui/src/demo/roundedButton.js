import React from "react";

import Typography from "clever-react/ui/Typography";
import RoundedButton from "clever-react/ui/RoundedButton";
import Grid from "clever-react/ui/Grid";

const RoundedButtons = () => (
  <div>
    <Typography variant="h2" shade={900} gutterBottom>
      Rounded Buttons
    </Typography>

    <Grid container spacing>
      <Grid item xs>
        <RoundedButton>Comenzar</RoundedButton>
      </Grid>

      <Grid item xs>
        <RoundedButton size="large">Comenzar</RoundedButton>
      </Grid>
      <Grid item xs>
        <RoundedButton height="narrow" color="secondary" hover={false}>
          Comenzar
        </RoundedButton>
      </Grid>

      <Grid item xs>
        <RoundedButton size="large" height="narrow">
          Comenzar
        </RoundedButton>
      </Grid>
    </Grid>
  </div>
);

export default RoundedButtons;
