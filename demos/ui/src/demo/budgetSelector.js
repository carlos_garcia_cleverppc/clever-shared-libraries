import React, { useState } from "react";

import BudgetSelector from "clever-react/ui/BudgetSelector";
import Typography from "clever-react/ui/Typography";

const BudgetSelectorPreview = () => {
  const [value, setValue] = useState(5);

  const onChange = newValue => {
    setValue(newValue);
  };

  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        Budget Selector
      </Typography>

      <BudgetSelector
        selectedBudget={value}
        recommendedBudget={20}
        mainCurrency="USD"
        adwordsCurrencyName="EUR"
        adwordsCurrencyRate={0.86}
        onChange={onChange}
        description="This is the description"
      />
    </div>
  );
};

export default BudgetSelectorPreview;
