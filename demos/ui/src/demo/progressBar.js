import React from "react";

import Typography from "clever-react/ui/Typography";
import ProgressBar from "clever-react/ui/ProgressBar";

const Progressbar = () => (
  <>
    <Typography variant="h2" shade={900} gutterBottom>
      Progress Bar
    </Typography>

    <ProgressBar value={75} color="secondary" />
  </>
);

export default Progressbar;
