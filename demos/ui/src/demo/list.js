import React from "react";

import Typography from "clever-react/ui/Typography";
import List from "clever-react/ui/List";
import Item from "clever-react/ui/List/Item";
import ItemIcon from "clever-react/ui/List/Item/Icon";
import Alert from "clever-react/ui/Icon/Alert";
import withStyles from "clever-react/ui/styles/withStyles";

const styles = ({ cleverUI: { colors } }) => ({
  list: {
    backgroundColor: colors.neutral[300],
    padding: "1rem",
  },
});

const ListPreview = ({ classes }) => (
  <>
    <Typography variant="h2" shade={900} gutterBottom>
      List
    </Typography>

    <div className={classes.list}>
      <List>
        <Item disabled onClick={() => {}}>
          <Typography>This is an item</Typography>
        </Item>

        <Item>
          <Typography>This is an item</Typography>
        </Item>

        <Item>
          <Typography>This is an item</Typography>

          <ItemIcon icon={<Alert />} />
        </Item>
      </List>
    </div>
  </>
);

export default withStyles(styles)(ListPreview);
