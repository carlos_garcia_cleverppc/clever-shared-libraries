import React from "react";

import Typography from "clever-react/ui/Typography";
import Badge from "clever-react/ui/Badge";
import Bell from "clever-react/ui/Icon/Bell";

const IconButtons = () => (
  <div>
    <Typography variant="h2" shade={900} gutterBottom>
      Badge
    </Typography>

    <Badge variant="dot" invisible={false} color="danger">
      <Bell color="primary" />
    </Badge>
  </div>
);

export default IconButtons;
