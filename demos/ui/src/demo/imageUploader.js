/* eslint-disable no-console */
import React, { useState } from "react";
import PropTypes from "prop-types";

import Grid from "clever-react/ui/Grid";
import Typography from "clever-react/ui/Typography";

import { getBase64MbSize } from "clever-react/utils/files";
import { TYPES_ERRORS } from "clever-react/ui/ImageField";
import ImageFieldWithUrl from "clever-react/ui/ImageFieldWithUrl";
import DialogCropper from "clever-react/ui/DialogCropper";
import exampleImage from "clever-react/ui/ImageUploader/exampleImage";

const ERROR_MESSAGES = {
  [TYPES_ERRORS.limitSize]: "Error size",
  [TYPES_ERRORS.invalidFormat]: "Invalid format",
};

const getBase64FromUrl = async () => ({ base64: exampleImage });

const ImageUploader = ({ mbLimit, onSubmit, size, className }) => {
  const [value, setValue] = useState("");
  const [openCropper, setOpenCropper] = useState(false);

  const showError = message => console.error(message);

  const isValidBase64Size = base64 => getBase64MbSize(base64) <= mbLimit;

  const onSubmitUrl = async url => {
    try {
      const { base64 } = await getBase64FromUrl(url);

      if (isValidBase64Size(base64)) {
        setOpenCropper(true);
        setValue(base64);
      } else {
        showError("Error size");
      }
    } catch (err) {
      showError("Error Format");
    }
  };

  const onCrop = imageCropped => {
    setOpenCropper(false);
    setValue(imageCropped);
    onSubmit(imageCropped);
  };

  const closeCropper = () => {
    setOpenCropper(false);
    setValue("");
  };

  const onChangeImageUrl = imageUrl => {
    setOpenCropper(true);
    setValue(imageUrl);
  };

  const handleErrors = errorType => {
    const errorMessage = ERROR_MESSAGES[errorType] || "Server error";

    showError(errorMessage);
  };
  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Image Uploader
      </Typography>

      <Grid container>
        <Grid item xs>
          <ImageFieldWithUrl
            onError={handleErrors}
            value={value}
            onChange={onChangeImageUrl}
            onSubmitUrl={onSubmitUrl}
            size={size}
            mbLimit={mbLimit}
            className={className}
          />

          <DialogCropper
            title="Crop your image"
            description="Tip: You can zoom in and out of the image."
            buttonText="Done"
            open={openCropper}
            onClose={closeCropper}
            onSubmit={onCrop}
            value={value}
          />
        </Grid>
      </Grid>
    </>
  );
};

ImageUploader.propTypes = {
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  mbLimit: PropTypes.number,
  onSubmit: PropTypes.func,
};

ImageUploader.defaultProps = {
  onSubmit: () => {},
  size: 120,
  mbLimit: 10,
};

export default ImageUploader;
