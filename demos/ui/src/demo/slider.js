import React from "react";

import Slider from "clever-react/ui/Slider";
import Typography from "clever-react/ui/Typography";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({
  root: {
    color: "#52af77",
    height: 8,
  },
  valueLabel: {
    boxShadow: "none",
    "& *": {
      color: "red",
      backgroundColor: "transparent",
    },
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
}));

const SliderPreview = () => {
  const [value, setValue] = React.useState([10, 70]);
  const classes = getClasses();

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade={900} gutterBottom>
        Slider
      </Typography>

      <div className={classes.slider}>
        <Slider
          value={value}
          min={0}
          max={20}
          step={5}
          valueLabelClass={classes.valueLabel}
          onChange={(event, newValue) => setValue(newValue)}
          valueLabelDisplay="on"
          // valueLabelFormat={formatLabel}
        />
      </div>
    </div>
  );
};

export default SliderPreview;
