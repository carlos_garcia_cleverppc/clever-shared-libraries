import React from "react";

import Grid from "clever-react/ui/Grid";
import Typography from "clever-react/ui/Typography";
import Select from "clever-react/ui/Select";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  root: { ...spacing["my-2"] },
  bold: { fontWeight: "bold" },
}));

const Selects = () => {
  const [selected, setSelected] = React.useState("none");
  const options = [
    { label: "Today", value: 1 },
    { label: "Last 7 days", value: 7, disabled: true },
  ];
  const classes = getClasses();

  const handleChange = event => {
    setSelected(event.target.value);
  };

  const printMenuItem = (label, value) => {
    if (value === 1) return <span className={classes.bold}>{label}</span>;
    return label;
  };

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Selects
      </Typography>

      <Grid container spacing={2}>
        <Grid item xs={8}>
          <Select
            name="select"
            label="Select label"
            value={selected}
            onChange={handleChange}
            options={options}
            className={classes.root}
            printMenuItem={printMenuItem}
          />
        </Grid>

        <Grid item xs={8}>
          <Select
            size="small"
            name="select"
            value={selected}
            placeholder="Choose period..."
            onChange={handleChange}
            options={options}
            className={classes.root}
            printMenuItem={printMenuItem}
          />
        </Grid>
      </Grid>
    </>
  );
};

export default Selects;
