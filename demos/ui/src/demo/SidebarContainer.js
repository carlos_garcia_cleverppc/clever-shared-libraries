import React, { useState } from "react";

import SidebarContainer from "clever-react/ui/Sidebar/Container";
import PlatformLogo from "clever-react/ui/Sidebar/PlatformLogo";
import SidebarItem from "clever-react/ui/Sidebar/Item";

import Menu from "clever-react/ui/Icon/Menu";
import IconButton from "clever-react/ui/IconButton";
/* import Footer from "clever-react/ui/Sidebar/Footer";
import UserAccount from "clever-react/ui/Sidebar/UserAccount";
import FooterActions from "clever-react/ui/Sidebar/FooterActions"; */

import makeStyles from "clever-react/ui/styles/makeStyles";
import ManagementFooter from "clever-react/ui/Sidebar/ManagementFooter";

const getClasses = makeStyles(() => ({
  container: {
    height: "100vh",
    maxWidth: "350px",
    position: "fixed",
    zIndex: 1000,
  },
  menuItem: { position: "fixed", top: "30px", left: "30px", zIndex: 900 },
}));

const SidebarContainerPreview = () => {
  const [selectedHome, setSelectedHome] = useState(false);

  const [selectedSettings, setSelectedSettings] = useState(false);

  const [selectedMetricsChild1, setSelectedMetricsChild1] = useState(false);
  const [selectedMetricsChild2, setSelectedMetricsChild2] = useState(false);
  const selectedMetrics = selectedMetricsChild1 || selectedMetricsChild2;

  const [contactSelected, setContactSelected] = useState(false);
  const [metricsSelected, setMetricsSelected] = useState(false);

  const [collapsed, setCollapsed] = useState(false);

  const [openSidebar, setOpenSidebar] = useState(false);

  const color = "secondary";

  const classes = getClasses();

  return (
    <div className={classes.container}>
      {!openSidebar && (
        <IconButton
          onClick={() => setOpenSidebar(!openSidebar)}
          icon={<Menu />}
          color="blue"
          className={classes.menuItem}
        />
      )}

      {openSidebar && (
        <SidebarContainer
          isSidebarCollapsed={collapsed}
          platformLogo={
            <PlatformLogo
              src={
                collapsed
                  ? "https://www.cleverecommerce.com/wp-content/uploads/2018/10/Icon_clever_ecommerce_no_background.png"
                  : "https://www.cleverecommerce.com/wp-content/uploads/2016/06/logoCleverEcommerce01.png"
              }
              isSidebarCollapsed={collapsed}
            />
          }
          body={
            <>
              <SidebarItem
                name="Home"
                icon="Home"
                isSelected={selectedHome}
                onClick={() => setSelectedHome(!selectedHome)}
                isSidebarCollapsed={collapsed}
                color={color}
              />

              <SidebarItem
                name="Settings"
                icon="Home"
                isSelected={selectedSettings}
                onClick={() => setSelectedSettings(!selectedSettings)}
                label="PRO+"
                isSidebarCollapsed={collapsed}
                color={color}
              />

              <SidebarItem
                name="Metrics"
                icon="Home"
                isSelected={selectedMetrics}
                isSidebarCollapsed={collapsed}
                color={color}
                childrenItems={[
                  <SidebarItem
                    name="Child1"
                    key="Child1"
                    icon="Home"
                    isSelected={selectedMetricsChild1}
                    onClick={() =>
                      setSelectedMetricsChild1(!selectedMetricsChild1)
                    }
                    isChild
                    isSidebarCollapsed={collapsed}
                  />,
                  <SidebarItem
                    name="Child2"
                    key="Child2"
                    icon="Home"
                    isSelected={selectedMetricsChild2}
                    onClick={() =>
                      setSelectedMetricsChild2(!selectedMetricsChild2)
                    }
                    isChild
                    isSidebarCollapsed={collapsed}
                  />,
                ]}
              />

              <SidebarItem
                name="Collapse"
                icon={collapsed ? "chevronRight" : "chevronLeft"}
                isSelected={collapsed}
                onClick={() => setCollapsed(!collapsed)}
                isSidebarCollapsed={collapsed}
                color={color}
              />

              <SidebarItem
                name="Close sidebar"
                icon="Uncheck"
                isSelected={!openSidebar}
                onClick={() => setOpenSidebar(false)}
                isSidebarCollapsed={collapsed}
                color={color}
              />
            </>
          }
          footer={
            <ManagementFooter
              avatarSrc="https://cdn.logo.com/hotlink-ok/logo-social.png"
              avatarDefaultName="CL"
              color="secondary"
              accountName="My Clever account"
              isSidebarCollapsed={collapsed}
              showProgress // ={false}
              onClickProgress={() => {
                console.log("On click progress");
              }}
              percentageDone={60}
              percentageCopy="Te faltan 4 tareas para terminar tu configuración bien"
              actions={[
                {
                  icon: "Support",
                  name: "Contact",
                  color: "danger",
                  onClick: () => setContactSelected(!contactSelected),
                },
                {
                  icon: "Ruler",
                  name: "Metrics",
                  onClick: () => setMetricsSelected(!metricsSelected),
                },
              ]}
              logoutCopy="Log out"
              accountsTitleCopy="Linked accounts"
              label="PRO"
              /* alert={{
                copyAlert: "This is an alert",
                copyOnClick: "Click here",
                onClick: () => { },
                color: "orange",
              }} */
              allowMultipleAccounts={false}
              allAccounts={[
                {
                  googleAds: {
                    name: "Google Ads",
                    addAccount: {
                      copy: "Add Google Ads account",
                      onClick: () => {},
                    },
                    accounts: [
                      {
                        name: "My account nameeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee",
                        id: "123456789",
                        active: true,
                        setActive: () => {},
                        alert: {
                          copyAlert: "Copy alert",
                          color: "orange",
                          onClick: () => {},
                        },
                        refresh: { copy: "Change account", onClick: () => {} },
                      },
                      {
                        name: "My account 2",
                        id: "223456789",
                        active: false,
                        setActive: () => {},
                        refresh: { copy: "Change account", onClick: () => {} },
                      },
                      {
                        name: "My acc3",
                        id: "333456789",
                        active: true,
                        setActive: () => {},
                      },
                    ],
                  },
                },
                {
                  googleAnalytics: {
                    name: "Google Analytics",
                    addAccount: {
                      copy: "Add Google Analitycs account",
                      onClick: () => {},
                    },
                    accounts: [
                      {
                        name: "My account 2",
                        id: "223456789",
                        active: false,
                        setActive: () => {},
                        refresh: { copy: "Change account", onClick: () => {} },
                      },
                      {
                        name: "My acc3",
                        id: "333456789",
                        active: true,
                        setActive: () => {},
                        alert: {
                          copyAlert: "Copy alert",
                          color: "orange",
                          onClick: () => {},
                        },
                      },
                    ],
                  },
                },
                {
                  googleMerchant: {
                    name: "Google Merchant",
                    addAccount: {
                      copy: "Add Google Merchant account",
                      onClick: () => {},
                    },
                    accounts: [],
                  },
                },
                {
                  facebookAds: {
                    name: "Facebook Ads",
                    addAccount: {
                      copy: "Add Facebook Ads account",
                      onClick: () => {},
                    },
                    accounts: [],
                  },
                },
                {
                  twitterAds: {
                    name: "Twitter Ads",
                    addAccount: {
                      copy: "Add Twitter Ads account",
                      onClick: () => {},
                    },
                    accounts: [
                      {
                        name: "My account 2",
                        id: "223456789",
                        active: false,
                        setActive: () => {},
                        refresh: { copy: "Change account", onClick: () => {} },
                        logout: { copy: "Log out", onClick: () => {} },
                      },
                      {
                        name: "My acc3",
                        id: "333456789",
                        active: true,
                        setActive: () => {},
                      },
                      {
                        name: "My account 2",
                        id: "223456789",
                        active: false,
                        setActive: () => {},
                        refresh: { copy: "Change account", onClick: () => {} },
                      },
                      {
                        name: "My acc3",
                        id: "333456789",
                        active: true,
                        setActive: () => {},
                      },
                    ],
                  },
                },
              ]}
            />

            /* <Footer
              actionsPosition="top"
              divider
              isSidebarCollapsed={collapsed}
              userAccount={(
                <UserAccount
                  avatarSrc="https://i.blogs.es/16b956/gmail-nuevo-logo-google-workspace/1366_2000.jpg"
                  accountSelected={{ name: "Test name", accountId: "123456789" }}
                  label="PRO"
                  allAccounts={[{ name: "Test name", accountId: "123456789", isSelected: true },
                  {
                    name: "Veryyyy looooooong testttt nameeeee",
                    accountId: "223456789",
                    isSelected: false,
                  },
                  { name: "Test name3", accountId: "323456789", isSelected: false }]}
                  titleCopy="titleCopy"
                  noAccountsCopy="noAccountsCopy"
                  addAccountCopy="addAccountCopy"
                  logOutCopy="logOutCopy"
                  isSidebarCollapsed={collapsed}
                  color={color}
                />
              )}
              footerActions={(
                <FooterActions
                  color={color}
                  items={[
                    {
                      icon: "Support",
                      name: "Contact",
                      isSelected: contactSelected,
                      color: "danger",
                      onClick: () => setContactSelected(!contactSelected),
                    },
                    {
                      icon: "Ruler",
                      name: "Metrics",
                      tooltip: false,
                      isSelected: metricsSelected,
                      onClick: () => setMetricsSelected(!metricsSelected),
                    },
                    {
                      icon: "Support",
                      name: "Contact",
                      isSelected: contactSelected,
                      onClick: () => setContactSelected(!contactSelected),
                    },
                    {
                      icon: "Ruler",
                      name: "Metrics",
                      isSelected: metricsSelected,
                      onClick: () => setMetricsSelected(!metricsSelected),
                    },
                  ]}
                  isSidebarCollapsed={collapsed}
                />
              )}
            /> */
          }
        />
      )}
    </div>
  );
};

export default SidebarContainerPreview;
