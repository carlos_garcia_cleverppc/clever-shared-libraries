import React from "react";

import Typography from "clever-react/ui/Typography";
import makeStyles from "clever-react/ui/styles/makeStyles";

const styles = makeStyles({ root: { textAlign: "left" } });

const TypographyPreview = () => {
  const classes = styles();

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade={900} gutterBottom>
        Typography
      </Typography>

      <Typography variant="h1" align="justify">
        h1 - Montserrat 36
      </Typography>

      <Typography variant="h2" gutterBottom>
        h2 - Montserrat 24
      </Typography>

      <Typography variant="h3" gutterBottom>
        h3 - Montserrat 21
      </Typography>

      <Typography variant="h6" gutterBottom>
        h6 - Montserrat 18
      </Typography>

      <Typography variant="body0" gutterBottom>
        body0 - Nunito 14
      </Typography>

      <Typography variant="body1" gutterBottom>
        body1 - Nunito 18
      </Typography>

      <Typography weight="bold" variant="body2" gutterBottom>
        body2 - Nunito 21 bold
      </Typography>

      <Typography variant="body3" gutterBottom>
        body3 - Nunito 24
      </Typography>

      <Typography variant="f1-12" upperCase gutterBottom>
        f1-12 - Montserrat 12 upperCase
      </Typography>

      <Typography variant="f2-20" gutterBottom>
        f2-20 - Nunito 20
      </Typography>

      <Typography variant="f1-16" gutterBottom>
        f1-16 - Montserrat 16
      </Typography>

      <Typography component="form" variant="f2-24" gutterBottom>
        Form component Nunito 24
      </Typography>
    </div>
  );
};

export default TypographyPreview;
