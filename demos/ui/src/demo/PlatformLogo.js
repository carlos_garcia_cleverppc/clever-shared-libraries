import React from "react";

import PlatformLogo from "clever-react/ui/Sidebar/PlatformLogo";
import Typography from "clever-react/ui/Typography";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({
  backdrop: { color: "#fff", zIndex: 1000 },
}));

const PlatformLogoPreview = () => {
  const classes = getClasses();

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade={900} gutterBottom>
        SidebarPlatformLogo
      </Typography>

      <PlatformLogo
        src="https://www.cleverecommerce.com/wp-content/uploads/2016/06/logoCleverEcommerce01.png"
        onClick={() => {}}
      />

      <PlatformLogo
        src="https://www.cleverecommerce.com/wp-content/uploads/2018/10/Icon_clever_ecommerce_no_background.png"
        isSidebarCollapsed
      />
    </div>
  );
};

export default PlatformLogoPreview;
