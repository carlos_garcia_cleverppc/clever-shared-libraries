import React from "react";

import Typography from "clever-react/ui/Typography";
import Switch from "clever-react/ui/Switch";

const Switches = () => {
  const [checked, setChecked] = React.useState(false);

  return (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Switch
      </Typography>

      <Switch
        checked={checked}
        onChange={() => setChecked(!checked)}
        value="Switch example"
      />

      <Switch
        checked={checked}
        onChange={() => setChecked(!checked)}
        value="Switch example"
        spinner="/assets/img/switch_spinner.gif"
        loading
        // size="small"
      />

      <Switch
        checked={checked}
        onChange={() => setChecked(!checked)}
        value="Switch example"
        size="small"
      />
    </>
  );
};

export default Switches;
