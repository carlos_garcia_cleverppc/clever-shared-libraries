import React, { useState } from "react";

import ToggleButtonGroup from "clever-react/ui/ToggleButtonGroup";
import Grid from "clever-react/ui/Grid";

import Typography from "clever-react/ui/Typography";
import AlignRight from "clever-react/ui/Icon/AlignRight";
import AlignLeft from "clever-react/ui/Icon/AlignLeft";
import AlignCenter from "clever-react/ui/Icon/AlignCenter";
import AlignJustify from "clever-react/ui/Icon/AlignJustify";

import Bold from "clever-react/ui/Icon/Bold";
import Underline from "clever-react/ui/Icon/Underline";
import Italic from "clever-react/ui/Icon/Italic";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  root: { ...spacing["m-4"], backgroundColor: "white" },
}));

const ToggleButtonGroupPreview = () => {
  const classes = getClasses();

  const [alignment, setAlignment] = useState("left");
  const [formats, setFormats] = useState(() => ["bold"]);

  const handleFormat = (event, newFormats) => {
    setFormats(newFormats);
  };

  const handleAlignment = (event, newAlignment) => {
    setAlignment(newAlignment);
  };

  return (
    <div>
      <Typography variant="h2" shade={900} gutterBottom>
        Toggle Button Group
      </Typography>

      <Grid container spacing={1} className={classes.root}>
        <Grid item>
          <ToggleButtonGroup
            value={alignment}
            exclusive
            onChange={handleAlignment}
            aria-label="text alignment"
            options={[
              { value: "left", label: <AlignLeft /> },
              { value: "center", label: <AlignCenter /> },
              { value: "right", label: <AlignRight /> },
              { value: "justify", label: <AlignJustify /> },
            ]}
          />
        </Grid>

        <Grid item>
          <ToggleButtonGroup
            value={formats}
            onChange={handleFormat}
            aria-label="text alignment"
            options={[
              { value: "bold", label: <Bold /> },
              { value: "italic", label: <Italic /> },
              { value: "underlined", label: <Underline /> },
            ]}
          />
        </Grid>
      </Grid>
    </div>
  );
};

export default ToggleButtonGroupPreview;
