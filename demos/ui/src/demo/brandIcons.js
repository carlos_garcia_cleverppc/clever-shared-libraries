/* eslint-disable import/no-duplicates */
import React from "react";

import makeStyles from "clever-react/ui/styles/makeStyles";

import Typography from "clever-react/ui/Typography";
import BrandIcon from "clever-react/ui/BrandIcon";
import Tooltip from "clever-react/ui/Tooltip";

import { brandIcons } from "clever-react/ui/styles/icons";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  icon: { ...spacing["mx-1"] },
  title: { ...spacing["mt-3"] },
}));

const BrandIcons = () => {
  const classes = getClasses();

  return (
    <>
      <Typography
        variant="h2"
        shade={900}
        className={classes.title}
        gutterBottom
      >
        Brand Icons
      </Typography>

      {brandIcons.map(icon => (
        <Tooltip key={`icon-tooltip-${icon}`} title={icon}>
          <BrandIcon icon={icon} className={classes.icon} size={64} />
        </Tooltip>
      ))}
    </>
  );
};

export default BrandIcons;
