import React from "react";

import AlertCard from "clever-react/ui/AlertCard";
import Typography from "clever-react/ui/Typography";

const AlertCardPreview = () => (
  <div>
    <Typography variant="h2" shade={900} gutterBottom>
      AlertCard
    </Typography>

    <AlertCard text="This is the text in the alert card" linkText="LinkText" />

    <AlertCard
      text="This is the text in the alert card"
      color="danger"
      linkText="LinkText"
    />

    <AlertCard
      text="This is the text in the alert card"
      color="warning"
      linkText="LinkText"
    />

    <AlertCard
      text="This is the text in the alert card"
      color="green"
      linkText="LinkText"
    />

    <AlertCard
      text="This is the text in the alert card"
      color="orange"
      linkText="LinkText"
    />
  </div>
);

export default AlertCardPreview;
