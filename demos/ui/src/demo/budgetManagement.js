import React, { useState } from "react";

import BudgetManagement from "clever-react/ui/BudgetManagement";
import Typography from "clever-react/ui/Typography";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({
  backdrop: { color: "#fff", zIndex: 1000 },
}));

const BudgetManagementPreview = () => {
  const classes = getClasses();

  const [channels, setChannels] = useState({
    googleAds: 10,
    facebookAds: 50,
    twitterAds: 100,
  });

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade={900} gutterBottom>
        Budget Management
      </Typography>

      <BudgetManagement
        budgetCopy="daily budget"
        currency="USD"
        channels={channels}
        textButton="Apply new budget"
        onClickButton={setChannels}
        minAlert={{
          copyAlert: "This is an alert",
          copyOnClick: "Click here",
          onClick: () => {},
        }}
        maxAlert={{
          copyAlert: "This is an alert",
          copyOnClick: "Click here",
          onClick: () => {},
        }}
      />
    </div>
  );
};

export default BudgetManagementPreview;
