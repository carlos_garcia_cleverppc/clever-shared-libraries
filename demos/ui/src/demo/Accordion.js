import React from "react";

import Accordion from "clever-react/ui/Accordion";
import Typography from "clever-react/ui/Typography";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({ backdrop: { color: "#fff", zIndex: 1000 } }));

const AccordionPreview = () => {
  const classes = getClasses();

  const [expanded, setExpanded] = React.useState(false);

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade={900} gutterBottom>Accordion</Typography>

      <Accordion
        header={<Typography>Header</Typography>}
        expanded={expanded}
        onChange={() => setExpanded(!expanded)}
      >
        <Typography>This is the children</Typography>
      </Accordion>

      <Accordion
        title="Header"
        subtitle="resumen del acordeón"
        expanded={expanded}
        onChange={() => setExpanded(!expanded)}
        variant="custom"
        badge
      >
        <Typography>This is the children</Typography>
      </Accordion>
    </div>
  );
};

export default AccordionPreview;
