import React, { useState } from "react";

import FooterActions from "clever-react/ui/Sidebar/FooterActions";
import Typography from "clever-react/ui/Typography";
import Divider from "clever-react/ui/Divider";

import makeStyles from "clever-react/ui/styles/makeStyles";

const getClasses = makeStyles(() => ({
  backdrop: { color: "#fff", zIndex: 1000 },
  sameLine: {
    display: "flex",
    alignItems: "center",
  },
}));

const FooterActionsPreview = () => {
  const classes = getClasses();

  const [settingsSelected, setSettingsSelected] = useState(false);
  const [notificationsSelected, setNotificationsSelected] = useState(false);

  const [contactSelected, setContactSelected] = useState(false);
  const [metricsSelected, setMetricsSelected] = useState(false);

  return (
    <div className={classes.root}>
      <Typography variant="h2" shade={900} gutterBottom>
        SidebarFooterActions
      </Typography>

      <Typography variant="body0" gutterBottom>
        Not collapsed:
      </Typography>

      <FooterActions
        items={[
          {
            icon: "Cog",
            name: "Settings",
            isSelected: settingsSelected,
            onClick: () => setSettingsSelected(!settingsSelected),
          },
          {
            icon: "BellOutlined",
            name: "Notifications",
            isSelected: notificationsSelected,
            badgeActive: true,
            onClick: () => setNotificationsSelected(!notificationsSelected),
          },
        ]}
      />

      <Divider />

      <Typography variant="body0" gutterBottom>
        Collapsed:
      </Typography>

      <FooterActions
        isSidebarCollapsed
        items={[
          {
            icon: "Support",
            name: "Contact",
            isSelected: contactSelected,
            onClick: () => setContactSelected(!contactSelected),
          },
          {
            icon: "Ruler",
            name: "Metrics",
            isSelected: metricsSelected,
            onClick: () => setMetricsSelected(!metricsSelected),
          },
        ]}
      />
    </div>
  );
};

export default FooterActionsPreview;
