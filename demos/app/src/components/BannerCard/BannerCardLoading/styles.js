import makeStyles from "clever-react/ui/styles/makeStyles";
import { sizing } from "clever-react/ui/styles/typography";

const styles = ({ cleverUI: { spacing, colors } }) => ({
  loading: {
    paddingBottom: "calc(280 / 420 * 100%)",
  },
  loadingImage: {
    position: "absolute",
    top: "18%",
    left: "50%",
    transform: "translateX(-50%)",
    width: "32%",
    height: "auto",
  },
  progress: {
    position: "absolute",
    bottom: 0,
    left: 0,
    width: "100%",
    height: "18%",
    display: "flex",
    boxSizing: "border-box",
    alignItems: "center",
    ...spacing["px-5"],
    paddingTop: "3px",
    transition: "background-size .3s",
    backgroundSize: "0 100%",
    backgroundRepeat: "no-repeat",
    background: `linear-gradient(0deg, ${colors.primary.lighter} calc(100% - 4px), ${colors.primary.main} calc(100% - 4px), ${colors.primary.main} 100%)`,
  },
  label: {
    "&::after": {
      content: "''",
      marginLeft: "1px",
      animation: "$dots 1s steps(3, end) infinite alternate",
    },
  },
  "@keyframes dots": {
    "0%": {
      content: "''",
    },
    "33%": {
      content: "'.'",
    },
    "66%": {
      content: "'..'",
    },
    "100%": {
      content: "'...'",
    },
  },
});

export default makeStyles(styles);
