import React, { useState, useEffect } from "react";
import Typography from "clever-react/ui/Typography";

import getClasses from "./styles";

const random = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

const BannerCardLoading = ({ className, label }) => {
  const [percentage, setPercentage] = useState(5);
  const classes = getClasses();

  useEffect(() => {
    const PERCENTAGE_MAXIMUM = 96;

    const timer = setTimeout(() => {
      const max = percentage < 50 ? random(5, 20) : random(1, 4);
      const newPercentage = percentage + random(1, max);

      setPercentage(Math.min(newPercentage, PERCENTAGE_MAXIMUM));
    }, random(500, 2000));

    return () => clearTimeout(timer);
  }, [percentage]);

  return (
    <div className={`${className} ${classes.loading}`}>
      <div
        className={classes.progress}
        style={{ backgroundSize: `${percentage}% 100%` }}
      >
        <Typography
          className={classes.label}
          variant="f2-14"
          weight="bold"
          color="primary"
        >
          {label}
        </Typography>
      </div>
    </div>
  );
};

export default BannerCardLoading;
