import makeStyles from "clever-react/ui/styles/makeStyles";
import { sizing } from "clever-react/ui/styles/typography";

const styles = ({ cleverUI: { spacing, colors }, breakpoints }) => ({
  iconButton: ({ color, size }) => ({
    border: 0,
    outline: 0,
    height: size === "small" ? 24 : 32,
    width: size === "small" ? 24 : 32,
    display: "inline-flex",
    justifyContent: "center",
    alignItems: "center",
    color: colors[color].main,
    background: "#ffffff",
    borderRadius: "50%",
    "&:not([aria-pressed])&:hover": {
      color: colors[color].dark,
    },
    "&[aria-pressed=true]": {
      background: colors[color].main,
      color: "#ffffff",
    },
    // "& > svg": {
    //   width: "60%",
    //   height: "60%",
    // },
  }),
});

export default makeStyles(styles);
