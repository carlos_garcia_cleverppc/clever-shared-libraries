/* eslint-disable react/require-default-props */
/* eslint-disable react/jsx-props-no-spreading */
import React, { cloneElement } from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import getClasses from "./styles";

const IconButton = props => {
  const {
    icon,
    active,
    color,
    href,
    target,
    disabled,
    label,
    size,
    onClick,
    className,
    ...attrs
  } = props;

  const classes = getClasses({ color, size });

  if (href) {
    return (
      <a
        {...attrs}
        href={href}
        target={target}
        aria-label={label}
        role={active ? "button" : "link"}
        aria-pressed={active}
        className={cx(classes.iconButton, className)}
      >
        {cloneElement(icon, { ...icon.props, color: "currentColor" })}
      </a>
    );
  }

  return (
    <button
      {...attrs}
      type="button"
      onClick={onClick}
      disabled={disabled}
      aria-label={label}
      aria-pressed={active}
      className={cx(classes.iconButton, className)}
    >
      {cloneElement(icon, { ...icon.props, color: "currentColor" })}
    </button>
  );
};

export default IconButton;

IconButton.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  icon: PropTypes.element.isRequired,
  href: PropTypes.string,
  target: PropTypes.string,
  size: PropTypes.oneOf(["small", "medium"]),
  color: PropTypes.oneOf(["primary", "secondary", "warning", "danger"]),
  active: PropTypes.bool,
  label: PropTypes.string,
};

IconButton.defaultProps = {
  onClick: () => {},
  disabled: false,
  size: "medium",
  color: "primary",
  active: false,
};
