import makeStyles from "clever-react/ui/styles/makeStyles";

const styles = ({ cleverUI: { typography, spacing, spaces, colors } }) => ({
  image: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    objectFit: "cover",
  },
  card: {
    borderRadius: 8,
    border: `1px solid ${colors.gray.light}`,
    position: "relative",
    overflow: "hidden",
    backgroundColor: "#ffffff",
  },
  body: {
    padding: spaces.sm,
    marginBottom: spaces.xs,
  },
  imageWrapper: {
    position: "relative",
    paddingBottom: "calc(120 / 300 * 100%)",
    border: 0,
    display: "block",
    backgroundColor: colors.gray.main,
  },
  actions: {
    position: "absolute",
    top: spaces.sm,
    right: spaces.sm,
    zIndex: 1,
  },
  title: {
    whiteSpace: "nowrap",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  resetLink: {
    textDecoration: "none",
    color: "currentColor",
  },
});

export default makeStyles(styles);
