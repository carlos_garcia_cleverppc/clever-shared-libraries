const DEFAULT_LANGUAGE = navigator?.language || "en-En";

const DEFAULT_OPTIONS = {
  year: "numeric",
  month: "numeric",
  day: "numeric",
  hour: "numeric",
  minute: "numeric",
  second: "numeric",
};

export const formatDate = (timestamp, options = DEFAULT_OPTIONS) => {
  const date = new Date(timestamp);

  return new Intl.DateTimeFormat(DEFAULT_LANGUAGE, options).format(date);
};

export default function useDateTimeFormat(timestamp, options) {
  return formatDate(timestamp, options);
}
