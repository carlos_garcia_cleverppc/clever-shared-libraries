import { formatDate } from "./useDateTimeFormat";

const DEFAULT_LANGUAGE = navigator?.language || "en-En";

const isRelativeTimeFormatSupported =
  typeof Intl !== "undefined" && Intl.RelativeTimeFormat;

const DATE_UNITS = [
  ["day", 86400],
  ["hour", 3600],
  ["minute", 60],
  ["second", 1],
];

const getDateDiffs = timestamp => {
  const now = Date.now();
  const elapsed = (timestamp - now) / 1000;

  for (const [unit, secondsInUnit] of DATE_UNITS) {
    if (Math.abs(elapsed) > secondsInUnit || unit === "second") {
      const value = Math.round(elapsed / secondsInUnit);
      return { value, unit };
    }
  }
};

const isTooLongAgo = ({ value, unit }) => Math.abs(value) > 30 && unit === "day";

export default function useTimeAgo(timestamp) {
  const timeago = getDateDiffs(timestamp);

  if (!isRelativeTimeFormatSupported) {
    return formatDate(timestamp);
  }

  const rtf = new Intl.RelativeTimeFormat(DEFAULT_LANGUAGE, {
    style: "short",
    numeric: "auto",
  });

  if (isTooLongAgo(timeago)) {
    const options = {
      year: "numeric",
      month: "short",
      day: "numeric",
    };
    return formatDate(timestamp, options);
  }

  const { value, unit } = timeago;
  return rtf.format(value, unit);
}
