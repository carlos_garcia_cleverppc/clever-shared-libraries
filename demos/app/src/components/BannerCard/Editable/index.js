import React, { useState, useRef, useEffect } from "react";

const Editable = ({ defaultValue, active, onSubmit, preview, className }) => {
  const [value, setValue] = useState(defaultValue);
  const inputRef = useRef();

  useEffect(() => {
    if (active) {
      inputRef.current.focus();
    }
  }, [active]);

  const handleChange = ev => setValue(ev.target.value);
  const validate = string => {
    const normalized = string.trim();
    if (normalized.length > 0) {
      return normalized;
    }
    return defaultValue;
  };
  const handleSubmit = ev => {
    ev.preventDefault();
    send();
  };

  const send = () => {
    const newValue = validate(value);
    onSubmit(newValue);
    setValue(newValue);
  };

  if (active) {
    return (
      <form onSubmit={handleSubmit}>
        <input
          className={className}
          ref={inputRef}
          value={value}
          onChange={handleChange}
          onBlur={send}
        />
      </form>
    );
  }

  return typeof preview === "function" ? preview(value) : value;
};

export default Editable;
