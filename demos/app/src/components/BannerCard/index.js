import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import Menu from "clever-react/ui/Menu";
import MenuItem from "clever-react/ui/Menu/Item";
import ShowMore from "clever-react/ui/Icon/ShowMore";
import Typography from "clever-react/ui/lab/Typography";

import IconButton from "./IconButton";
import Time from "./Time";
import Editable from "./Editable";
import BannerCardLoading from "./BannerCardLoading";
import getClasses from "./styles";

const isSameDates = (d1, d2) => d1.getTime() === d2.getTime();

const BannerCard = props => {
  const {
    title: initialTitle,
    lastUpdate,
    creationDate,
    uploaded,
    image,
    id,
    loading,
  } = props;

  const history = useHistory();
  const EDITOR_URL = `/editor/?id=${id}`;

  const [enableEdit, setEnableEdit] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [title, setTitle] = useState(initialTitle);
  const classes = getClasses({ loadingProgress: 35 });

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleRename = () => {
    setAnchorEl(null);
    setEnableEdit(true);
  };
  const handleLink = ev => {
    ev.preventDefault();
    history.push(EDITOR_URL);
  };

  const handleEditableSubmit = value => {
    setEnableEdit(false);
    setTitle(value);
  };

  if (loading) {
    return (
      <BannerCardLoading
        className={classes.card}
        label="Generando pack de banners"
      />
    );
  }
  return (
    <div className={classes.card}>
      <div className={classes.actions}>
        <IconButton label="options" icon={<ShowMore />} onClick={handleClick} />

        <Menu
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={handleClose}
          keepMounted
        >
          <MenuItem onClick={handleClose}>Download banners</MenuItem>
          <MenuItem onClick={handleRename}>Rename pack</MenuItem>
          <MenuItem onClick={handleClose}>Duplicate banners</MenuItem>
          <MenuItem onClick={handleClose}>Remove banners</MenuItem>
        </Menu>
      </div>
      <a
        href={EDITOR_URL}
        onClick={handleLink}
        className={classes.imageWrapper}
      >
        <img src={image} loading="lazy" className={classes.image} />
      </a>

      <div className={classes.body}>
        <Editable
          className={classes.editable}
          defaultValue={title}
          active={enableEdit}
          onSubmit={handleEditableSubmit}
          preview={value => (
            <a
              href={EDITOR_URL}
              onClick={handleLink}
              className={classes.resetLink}
            >
              <Typography
                className={classes.title}
                variant="title2"
                weight="bold"
                shade={700}
              >
                {value}
              </Typography>
            </a>
          )}
        />
        <Typography>
          Created <Time date={creationDate} />
          {lastUpdate && !isSameDates(creationDate, lastUpdate) && (
            <>
              {" "}
              - Updated <Time date={lastUpdate} />
            </>
          )}
        </Typography>
      </div>
    </div>
  );
};

export default BannerCard;
