import React from "react";

import useTimeAgo from "../hooks/useTimeAgo";
import useDateTimeFormat from "../hooks/useDateTimeFormat";

const Time = ({ date }) => {
  const timeago = useTimeAgo(date);
  const timeFormated = useDateTimeFormat(date);
  return <time title={timeFormated}>{timeago}</time>;
};

export default Time;
