import makeStyles from "clever-react/ui/styles/makeStyles";

const useStyles = makeStyles(() => ({
  "@global": {
    "h1, h2, h3, h4, h5, h6, p": {
      margin: 0,
    },
  },
}));

const GlobalStyles = () => {
  useStyles();

  return null;
};

export default GlobalStyles;
