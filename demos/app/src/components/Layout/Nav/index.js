import { useContext } from "react";
import ChevronDoubleRight from "clever-react/ui/Icon/ChevronDoubleRight";
import IconMenu from "clever-react/ui/Icon/Menu";
import cx from "classnames";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

import getClasses from "./styles";
import LayoutContext from "../LayoutContext";

const Nav = ({ children }) => {
  const classes = getClasses();
  const { collapseNav, setCollapseNav, openNav, setOpenNav, enableCollapse } =
    useContext(LayoutContext);

  const toggle = () => setCollapseNav(!collapseNav);
  const handleClick = () => setOpenNav(true);

  if (collapseNav) {
    return (
      <div className={cx(classes.collapse, { [classes.open]: openNav })}>
        <button className={classes.iconMenu} onClick={handleClick}>
          <IconMenu color="currentColor" size={18} />
        </button>
        {openNav && (
          <ClickAwayListener onClickAway={() => setOpenNav(false)}>
            <div className={classes.collapseContainer}>
              {enableCollapse && (
                <button className={classes.buttonCollapse} onClick={toggle}>
                  <ChevronDoubleRight color="currentColor" size={24} />
                </button>
              )}
              {children}
            </div>
          </ClickAwayListener>
        )}
      </div>
    );
  }
  return (
    <div className={classes.nav}>
      <button className={classes.buttonCollapse} onClick={toggle}>
        <ChevronDoubleRight color="currentColor" size={24} />
      </button>
      {children}
    </div>
  );
};

export default Nav;
