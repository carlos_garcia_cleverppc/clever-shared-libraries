import makeStyles from "clever-react/ui/styles/makeStyles";

const styles = ({ cleverUI: { spacing, colors } }) => ({
  iconMenu: {
    border: 0,
    backgroundColor: "#ffffff",
    width: "40px",
    height: "40px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: "50%",
    outline: 0,
    cursor: "pointer",
    color: colors.gray.main,
    "&:hover": {
      color: colors.primary.main,
    },
  },
  nav: {
    position: "relative",
    flex: "0 0 25%",
    maxWidth: 480,
    "&:hover $buttonCollapse": {
      opacity: 1,
    },
  },
  collapse: {
    position: "fixed",
    top: "calc(var(--app-ui-header-height) + 16px)",
    left: 8,
    zIndex: 10,
    "@media(min-width: 600px)": {
      left: 24,
    },
    "& $buttonCollapse": {
      right: "0",
      opacity: 0,
      top: "94px",
      transform: "translateX(50%)",
      "& svg": {
        transform: "translateX(0px)",
      },
    },
  },
  collapseContainer: {
    marginTop: 8,
  },
  open: {
    "& $collapseContainer": {
      display: "block",
      opacity: 1,
      "&:hover $buttonCollapse": {
        opacity: 1,
      },
    },
  },
  buttonCollapse: {
    border: 0,
    backgroundColor: "#ffffff",
    width: "40px",
    height: "40px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: "50%",
    boxShadow:
      "0 0.1px 0.3px rgba(0, 0, 0, 0.06), 0 1px 2px rgba(0, 0, 0, 0.12)",
    outline: 0,
    position: "absolute",
    top: "82px",
    right: 0,
    transition: "opacity .3s",
    transform: "translateX(50%)",
    zIndex: 4,
    cursor: "pointer",
    opacity: 0,
    color: colors.gray.main,
    "&:hover": {
      color: colors.primary.main,
    },
    "& svg": {
      transform: "scaleX(-1) translateX(1px)",
    },
  },
});

export default makeStyles(styles);
