import getClasses from "./styles";

const Content = ({ children }) => {
  const classes = getClasses();
  return <main className={classes.content}>{children}</main>;
};

export default Content;
