import React, { forwardRef, useState, useEffect } from "react";
import useMediaQuery from "clever-react/ui/styles/useMediaQuery";

import getClasses from "./styles";
import useTheme from "clever-react/ui/styles/useTheme";

import LayoutContext from "./LayoutContext";

const Layout = forwardRef(({ children }, ref) => {
  const [openSidebar, setOpenSidebar] = useState(false);
  const [openNav, setOpenNav] = useState(false);

  const [sidebarContent, setSidebarContent] = useState("");
  const [headerHeight, setHeaderHeight] = useState(0);

  const { cleverUI } = useTheme();
  const isDesktop = useMediaQuery(cleverUI.breakpoints.up("sm"));
  const [collapseNav, setCollapseNav] = useState(false);

  useEffect(() => {
    setCollapseNav(!isDesktop);
  }, [isDesktop]);

  const classes = getClasses({ headerHeight });

  return (
    <LayoutContext.Provider
      value={{
        openSidebar,
        setOpenSidebar,
        setHeaderHeight,
        sidebarContent,
        setSidebarContent,

        collapseNav,
        setCollapseNav,
        openNav,
        setOpenNav,
        enableCollapse: isDesktop,
      }}
    >
      <div className={classes.root}>
        {children}
        {openSidebar && <div className={classes.sidebar}>{sidebarContent}</div>}
      </div>
    </LayoutContext.Provider>
  );
});

Layout.displayName = "Layout";

export default Layout;
