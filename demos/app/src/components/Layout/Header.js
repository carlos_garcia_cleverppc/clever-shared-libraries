import { useLayoutEffect, useContext, useRef } from "react";
import getClasses from "./styles";
import layoutContext from "./LayoutContext";
import cx from "classnames";

const Header = ({ className, children }) => {
  const classes = getClasses();
  const headerRef = useRef();
  const { setHeaderHeight } = useContext(layoutContext);

  useLayoutEffect(() => {
    const resizeObserver = new ResizeObserver(entries => {
      for( let entry of entries ) {
        setHeaderHeight(entry.contentRect.height);
      }
    })
    resizeObserver.observe(headerRef.current);

    return () => resizeObserver.disconnect()
  }, [headerRef])



  return (
    <header ref={headerRef} className={cx(classes.header, className)}>
      {children}
    </header>
  );
};

export default Header;
