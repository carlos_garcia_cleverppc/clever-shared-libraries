import makeStyles from "clever-react/ui/styles/makeStyles";

const styles = ({ cleverUI: { spacing, colors, breakpoints, spaces } }) => ({
  root: ({ headerHeight }) => ({
    "--app-ui-header-height": `${headerHeight}px`,
    [breakpoints.up("sm")]: {
      display: "flex",
      top: "var(--app-ui-header-height)",
      position: "relative",
      height: "calc(100vh - var(--app-ui-header-height))",
      overflow: "hidden",
    },
  }),
  content: {
    position: "relative",
    zIndex: 1,
    backgroundColor: colors.gray.lighter,
    padding: spaces.sm,
    paddingTop: spaces.lg,
    boxSizing: "border-box",
    minHeight: "100vh",
    [breakpoints.up("sm")]: {
      padding: spaces.md,
      paddingTop: spaces.lg,
      minHeight: "0",
      height: "100%",
      overflow: "auto",
      WebkitOverflowScrolling: "touch",
      boxSizing: "border-box",
      flex: "1 1 auto",
    },
    [breakpoints.up("lg")]: {
      padding: spaces.lg,
      paddingTop: spaces.xl,
    },
  },
  sidebarClose: {
    border: 0,
    outline: 0,
    padding: 0,
    zIndex: 1,
    cursor: "pointer",
    display: "inline-flex",
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    right: "15px",
    top: "15px",
    width: "32px",
    height: "32px",
    backgroundColor: "transparent",
    backgroundImage:
      "radial-gradient(circle, rgba(255,255,255,0.75) 50%, rgba(255,255,255,0) 50%, rgba(255,255,255,0) 100%)",
  },
  sidebar: {
    backgroundColor: "#ffffff",
    [breakpoints.up("sm")]: {
      flex: "0 0 25%",
      maxWidth: 480,
      height: "100%",
      position: "relative",
    },
    [breakpoints.down("sm")]: {
      zIndex: 5,
      position: "fixed",
      bottom: 0,
      width: "100%",
      maxWidth: "100%",
      left: 0,
      height: "76vh",
      boxShadow: "0 0 40px rgba(0,0,0,.4)",
    },
  },
  header: {
    position: "sticky",
    width: "100%",
    left: "0",
    top: "0",
    zIndex: 3,
    boxSizing: "border-box",
    [breakpoints.up("sm")]: {
      position: "fixed",
    },
  },
});

export default makeStyles(styles);
