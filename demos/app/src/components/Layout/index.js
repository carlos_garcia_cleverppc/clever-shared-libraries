import Layout from "./Layout";

import Header from "./Header";
import Content from "./Content";
import Sidebar from "./Sidebar";
import Nav from "./Nav";

Layout.Header = Header;
Layout.Content = Content;
Layout.Sidebar = Sidebar;
Layout.Nav = Nav;

export default Layout;
