import { useContext, useEffect } from "react";
import LayoutContext from "./LayoutContext";
import getClasses from "./styles";
import TimesRound from "clever-react/ui/Icon/TimesRound";

const Sidebar = ({ open, children, closable, onClose }) => {
  const classes = getClasses();
  const { setOpenSidebar, setSidebarContent } = useContext(LayoutContext);

  useEffect(() => {
    setOpenSidebar(open);
  }, [open]);

  useEffect(() => {
    const content = (
      <>
        {children}
        {closable && (
            <button onClick={onClose} className={classes.sidebarClose}>
            <TimesRound size={32} color="black" />
            </button>
        )}
      </>
    );
    setSidebarContent(content);
  }, [children]);

  return null;
};

export default Sidebar;
