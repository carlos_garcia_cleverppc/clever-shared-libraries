import React from "react";

import Layout from "../Layout";
import Sidebar from "./Sidebar";

const MainView = ({ children }) => {
  return (
    <Layout>
      <Layout.Nav>
        <Sidebar />
      </Layout.Nav>
      <Layout.Content>{children}</Layout.Content>
    </Layout>
  );
};

export default MainView;
