import React from "react";

import Icon from "clever-react/ui/Icon";
import { useLocation } from "react-router-dom";

import Logo from "./logo-clever.svg";
import getClasses from "./styles";
import SidebarItem from "./SidebarItem";

const Sidebar = () => {
  const classes = getClasses();
  const { pathname } = useLocation();

  return (
    <div className={classes.root}>
      <div className={classes.header}>
        <img
          width="340"
          height="63"
          alt=""
          className={classes.logo}
          src={Logo}
        />
      </div>
      <nav className={classes.nav}>
        <SidebarItem icon="Home" isSelected={pathname === "/"} to="/">
          Home
        </SidebarItem>
        <SidebarItem
          icon="Home"
          isSelected={pathname === "/campaign_creator"}
          to="/campaign_creator"
        >
          Campaign Creator
        </SidebarItem>
        <SidebarItem icon="Home" isSelected={pathname === "/audit"} to="/audit">
          Audit
        </SidebarItem>
        <SidebarItem
          icon="Home"
          isSelected={pathname === "/banner_creator"}
          to="/banner_creator"
        >
          Banner Creator
        </SidebarItem>
      </nav>
    </div>
  );
};

export default Sidebar;
