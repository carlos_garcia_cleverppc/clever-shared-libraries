import React from "react";
import { Link } from "react-router-dom";
import cx from "classnames";

import Icon from "clever-react/ui/Icon";
import Typography from "clever-react/ui/lab/Typography";
import makeStyles from "clever-react/ui/styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing, spaces, breakpoints } }) => ({
  root: {
    padding: `${spaces.xs}px ${spaces.sm}px`,
    display: "grid",
    gridTemplateColumns: "auto 1fr",
    gap: spaces.xs,
    borderRadius: 4,
    marginLeft: spaces.sm * -1,
    boxSizing: "border-box",
    width: `calc(100% + ${spaces.sm * 2}px)`,
    alignItems: "center",
    "&:hover": {
      backgroundColor: colors.neutral[100],
    },
  },
  fixVerticalAlign: {
    paddingTop: 3,
  },
  isSelected: {
    position: "relative",
    "&::before": {
      content: '"  "',
      backgroundColor: colors.blue[500],
      width: 3,
      top: spaces.xs + 2,
      bottom: spaces.xs + 2,
      left: 6,
      position: "absolute",
      borderRadius: 99999,
    },
  },
});

const getClasses = makeStyles(styles);

const SidebarItem = ({ children, isSelected, icon, to }) => {
  const classes = getClasses();

  return (
    <Link
      to={to}
      className={cx(classes.root, { [classes.isSelected]: isSelected })}
    >
      <Icon
        size="24"
        icon={icon}
        color={isSelected ? "blue" : "neutral"}
        shade={isSelected ? 500 : 400}
      />

      <Typography
        className={classes.fixVerticalAlign}
        component="div"
        variant="title2"
        shade={isSelected ? 700 : 400}
      >
        {children}
      </Typography>
    </Link>
  );
};

export default SidebarItem;
