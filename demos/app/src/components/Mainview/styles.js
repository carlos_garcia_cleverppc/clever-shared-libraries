import makeStyles from "clever-react/ui/styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing, spaces, breakpoints } }) => ({
  root: {
    backgroundColor: colors.white[500],
    paddingTop: spaces.lg,
    paddingLeft: spaces.md,
    paddingRight: spaces.md,
    paddingBottom: spaces.lg,
    [breakpoints.up("md")]: {
      paddingTop: spaces.xl,
      paddingLeft: spaces.lg,
      paddingRight: spaces.lg,
      paddingBottom: spaces.xl,
    },
  },
  header: {
    paddingBottom: spaces.xl,
    [breakpoints.up("md")]: {
      paddingBottom: spaces.xxl,
    },
  },
  nav: {
    display: "grid",
    gap: 4,
  },
  logo: {
    width: "100%",
    display: "block",
    height: "auto",
    maxWidth: 254,
  },
});

export default makeStyles(styles);
