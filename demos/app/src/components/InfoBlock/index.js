/* eslint-disable no-redeclare */
/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import Typography from "clever-react/ui/lab/Typography";
import OpenBlock from "clever-react/ui/Icon/OpenBlock";
import getClasses from "./styles";

import Handle from "./Handle";

const InfoBlock = props => {
  const {
    title,
    description,
    link,
    vertical,
    selected,
    showArrow,
    color,
    image,
    className,
    ...attrs
  } = props;

  const classes = getClasses({
    color,
    vertical,
    selected,
    hasLink: Boolean(link),
  });

  const getTextColor = () => (selected ? "white" : color);

  return (
    <Handle
      className={classnames([className, classes.root])}
      aria-pressed={selected}
      {...attrs}
    >
      {showArrow && !selected && (
        <OpenBlock color={color} className={classes.icon} />
      )}
      <div>
        <div className={classes.content}>
          <Typography variant="title2" color={getTextColor()}>
            {title}
          </Typography>

          {description && (
            <Typography color={getTextColor()}>{description}</Typography>
          )}
        </div>

        {link && (
          <Typography
            variant="small"
            weight="bold"
            color={getTextColor()}
            className={classes.link}
          >
            {link}
          </Typography>
        )}
      </div>

      {image && (
        <img src={image} className={classnames(classes.smallImage)} alt="" />
      )}
    </Handle>
  );
};

export default InfoBlock;

InfoBlock.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  link: PropTypes.string,
  vertical: PropTypes.bool,
  selected: PropTypes.bool,
  onClick: PropTypes.func,
  showArrow: PropTypes.bool,
  image: PropTypes.string.isRequired,
  color: PropTypes.oneOf([
    "primary",
    "secondary",
    "warning",
    "gray",
    "danger",
    "magenta",
    "yellow",
    "orange",
    "pink",
    "navyBlue",
    "purple",
    "mobileApp",
    "audit",
    "promo",
    "campaignCreator",
    "keywordPlanner",
    "bannerCreator",
    "adsAssistant",
    "alt1",
    "alt2",
    "alt3",
    "christmas",
  ]),
};

InfoBlock.defaultProps = {
  title: "",
  description: "",
  link: "",
  vertical: false,
  selected: false,
  onClick: undefined,
  showArrow: false,
  color: "primary",
};
