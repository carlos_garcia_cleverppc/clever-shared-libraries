import makeStyles from "clever-react/ui/styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing, spaces, breakpoints } }) => ({
  root: ({ color, vertical, selected }) => ({
    padding: spaces.sm,
    borderRadius: "8px",
    width: "100%",
    display: "grid",
    backgroundColor: selected ? colors[color].main : colors[color].lighter,
    color: selected ? "white" : colors[color].main,
    gridTemplateColumns: vertical ? "1fr" : "3fr 2fr",
    cursor: "pointer",
    textAlign: "left",
    boxSizing: "border-box",
    transition: "box-shadow .3s ease-in-out",
    position: "relative",
    alignItems: "center",
    "&:hover": { boxShadow: "0 2px 20px rgba(0,0,0,0.1)" },
    "&:focus": { boxShadow: "0 2px 20px rgba(0,0,0,0.1)", outline: 0 },
    [breakpoints.up("md")]: {
      padding: spaces.md,
    },
  }),
  content: ({ hasLink }) => ({
    display: "-webkit-box",
    overflow: "hidden",
    "-webkit-line-clamp": hasLink ? "4" : "6",
    "-webkit-box-orient": "vertical",
    "& > *:not(:last-child)": { marginbottom: spaces.sm },
  }),
  link: { marginTop: spaces.sm },
  icon: ({ color }) => ({
    width: "32px",
    height: "32px",
    position: "absolute",
    top: spaces.sm,
    right: spaces.sm,
    "& path": { fill: colors[color].darker },
  }),
  smallImage: {
    marginLeft: "auto",
    marginRight: "auto",
    maxHeight: "115px",
    maxWidth: "100%",
  },
});

export default makeStyles(styles);
