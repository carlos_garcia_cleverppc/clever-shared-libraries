import React, { useEffect } from "react";
import { withRouter, Switch, Route } from "react-router-dom";

import Home from "./pages/Home";
import BannerCreator from "./pages/BannerCreator";
import Page404 from "./pages/Page404";

const Routes = ({ location }) => {
  useEffect(() => window.scrollTo(0, 0), [location.pathname]);

  return (
    <Switch location={location}>
      <Route exact path="/" component={Home} />
      <Route exact path="/banner_creator" component={BannerCreator} />
      <Route path="*" component={Page404} />
    </Switch>
  );
};

export default withRouter(Routes);
