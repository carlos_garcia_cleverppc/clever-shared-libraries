import React from "react";

import Grid from "clever-react/ui/Grid";
import makeStyles from "clever-react/ui/styles/makeStyles";
import Typography from "clever-react/ui/lab/Typography";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  root: { ...spacing["mt-5"] },
}));

const Page404 = () => {
  const classes = getClasses();

  return (
    <Grid container align="center" justify="center" className={classes.root}>
      <Typography variant="title" component="h1">
        Page 404
      </Typography>
    </Grid>
  );
};

export default Page404;
