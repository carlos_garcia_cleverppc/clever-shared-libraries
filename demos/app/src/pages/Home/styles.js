import makeStyles from "clever-react/ui/styles/makeStyles";

const styles = ({ cleverUI: { spaces } }) => ({
  container: {
    maxWidth: "48em",
  },
  mb: {
    marginBottom: spaces.xs,
  },
});

export default makeStyles(styles);
