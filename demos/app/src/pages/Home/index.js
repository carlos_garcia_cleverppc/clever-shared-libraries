import React from "react";
import Typography from "clever-react/ui/lab/Typography";

import getClasses from "./styles";

const Home = () => {
  const classes = getClasses();

  return (
    <div className={classes.container}>
      <Typography
        variant="title"
        shade={900}
        component="h1"
        className={classes.mb}
      >
        Page title
      </Typography>
      <Typography variant="subtitle" gutterBottom>
        In cursus sem ut consectetur auctor
      </Typography>
      <Typography gutterBottom>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In cursus sem
        ut consectetur auctor. Fusce laoreet tortor ac sem euismod fermentum.
        Praesent ultricies facilisis velit, in condimentum quam volutpat vel.
        Mauris tristique nisi quam, vel suscipit justo sodales at. Aliquam
        dapibus sodales risus, tincidunt porttitor arcu varius et. Curabitur
        condimentum ultrices lorem, vel sagittis lorem tincidunt et. Suspendisse
        imperdiet iaculis felis, a dignissim nisi dapibus eget. Maecenas
        efficitur rhoncus est non pretium. Maecenas eget vehicula tellus,
        euismod posuere eros. Morbi nibh mi, consectetur non auctor vel,
        tristique in nibh.
      </Typography>
      <Typography gutterBottom>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In cursus sem
        ut consectetur auctor. Fusce laoreet tortor ac sem euismod fermentum.
        Praesent ultricies facilisis velit, in condimentum quam volutpat vel.
        Mauris tristique nisi quam, vel suscipit justo sodales at. Aliquam
        dapibus sodales risus, tincidunt porttitor arcu varius et. Curabitur
        condimentum ultrices lorem, vel sagittis lorem tincidunt et. Suspendisse
        imperdiet iaculis felis, a dignissim nisi dapibus eget. Maecenas
        efficitur rhoncus est non pretium. Maecenas eget vehicula tellus,
        euismod posuere eros. Morbi nibh mi, consectetur non auctor vel,
        tristique in nibh.
      </Typography>
      <Typography gutterBottom>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. In cursus sem
        ut consectetur auctor. Fusce laoreet tortor ac sem euismod fermentum.
        Praesent ultricies facilisis velit, in condimentum quam volutpat vel.
        Mauris tristique nisi quam, vel suscipit justo sodales at. Aliquam
        dapibus sodales risus, tincidunt porttitor arcu varius et. Curabitur
        condimentum ultrices lorem, vel sagittis lorem tincidunt et. Suspendisse
        imperdiet iaculis felis, a dignissim nisi dapibus eget. Maecenas
        efficitur rhoncus est non pretium. Maecenas eget vehicula tellus,
        euismod posuere eros. Morbi nibh mi, consectetur non auctor vel,
        tristique in nibh.
      </Typography>
    </div>
  );
};

export default Home;
