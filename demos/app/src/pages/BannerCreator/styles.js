import makeStyles from "clever-react/ui/styles/makeStyles";

const styles = ({ cleverUI: { spaces }, breakpoints }) => ({
  header: {
    display: "grid",
    gap: spaces.sm,
    alignItems: "center",
    [breakpoints.up("md")]: {
      gridTemplateColumns: "1fr auto",
      alignItems: "flex-start",
    },
  },
  container: {
    display: "grid",
    gap: spaces.md,
    [breakpoints.up("md")]: {
      gap: spaces.lg,
    },
  },
  title: {
    marginBottom: spaces.xs,
  },
  banner: {
    maxWidth: "740px",
  },
  grid: {
    display: "grid",
    gridTemplateColumns: "repeat(auto-fill, minmax(310px, 1fr))",
    gap: spaces.sm,
  },
});

export default makeStyles(styles);
