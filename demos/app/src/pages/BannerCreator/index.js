/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from "react";
import Typography from "clever-react/ui/lab/Typography";
import Button from "clever-react/ui/lab/Button";

import Layout from "../../components/Layout";
import InfoBlock from "../../components/InfoBlock";

import getClasses from "./styles";
import Illustration from "./illustration-couple-traveling.svg";
import BannerCard from "../../components/BannerCard";
import AutomaticGeneration from "./AutomaticGeneration";

const banners = Array.from({ length: 25 }, (_, id) => ({
  id,
  title: "Título",
  lastUpdate: new Date(Date.now() - 5 * 24 * 60 * 60 * 1000),
  creationDate: new Date(Date.now() - 10 * 30 * 24 * 60 * 60 * 1000),
  image: "https://via.placeholder.com/300x120",
  uploaded: id % 2 === 0,
  error: false,
  loading: id === 3,
}));

const BannerCreator = () => {
  const classes = getClasses();
  const [openSidebar, setOpenSidebar] = useState(false);
  return (
    <>
      <div className={classes.container}>
        <div className={classes.header}>
          <div>
            <Typography
              variant="title"
              component="h1"
              shade={900}
              className={classes.title}
            >
              My banners (25)
            </Typography>

            <Typography variant="subTitle">
              Create banners for your display campaigns by automatic or
              manually.
            </Typography>
          </div>
          <div>
            <Button variant="primary" onClick={() => alert("create!")}>
              Create banner
            </Button>
          </div>
        </div>

        <div className={classes.banner}>
          <InfoBlock
            onClick={() => setOpenSidebar(true)}
            color="purple"
            title="Try Automatic banner generation"
            image={Illustration}
            description="Enter your website URL and we generate automatic banners"
          />
        </div>

        <div className={classes.grid}>
          {banners.map(({ id, ...props }) => (
            <BannerCard key={id} id={id} {...props} />
          ))}
        </div>
      </div>

      <Layout.Sidebar
        closable
        onClose={() => setOpenSidebar(false)}
        open={openSidebar}
      >
        <AutomaticGeneration />
      </Layout.Sidebar>
    </>
  );
};

export default BannerCreator;
