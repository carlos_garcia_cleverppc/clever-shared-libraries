/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import getClasses from "./styles";

const AutomaticGeneration = () => {
  const classes = getClasses();
  return (
    <aside>
      <header className={classes.header}></header>
      <div className={classes.body}>body!</div>
    </aside>
  );
};
export default AutomaticGeneration;
