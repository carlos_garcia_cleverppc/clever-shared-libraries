import makeStyles from "clever-react/ui/styles/makeStyles";
import { sizing } from "clever-react/ui/styles/typography";

const styles = ({ cleverUI: { spacing, colors }, breakpoints }) => ({
  header: {
    color: "#ffffff",
    backgroundColor: colors.primary.main,
    height: "20vh",
    ...spacing["p-4"],
  },
  body: {
    ...spacing["p-4"],
    backgroundColor: "#ffffff",
  },
});

export default makeStyles(styles);
