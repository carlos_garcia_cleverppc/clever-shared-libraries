import React from "react";
import ReduxProvider from "clever-react/ui/Provider/extensions/redux";
import { BrowserRouter } from "react-router-dom";
import ThemeProvider from "clever-react/ui/Provider";
import Routes from "./routes";
import Mainview from "./components/Mainview";
import GlobalStyles from "./components/GlobalStyles";

function App() {
  return (
    <ReduxProvider>
      <ThemeProvider>
        <GlobalStyles />

        <BrowserRouter>
          <Mainview>
            <Routes />
          </Mainview>
        </BrowserRouter>
      </ThemeProvider>
    </ReduxProvider>
  );
}

export default App;
