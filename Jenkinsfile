pipeline {
  agent {
    kubernetes {
      cloud 'kubernetes'
      defaultContainer 'jnlp'
      yamlFile 'jenkins-pod.yaml'
    }
  }

  stages {
    stage('Checkout code') {
        steps {
            checkout scm
        }
    }

    stage('Configure gcloud SDK') {
      steps {
        script {
          def props = readJSON file: './clever-react/package.json'
          props.version = "${BUILD_NUMBER}.${BUILD_NUMBER}.${BUILD_NUMBER}" as String
          sh 'mv ./clever-react/package.json ./clever-react/package_backup.json'
          writeJSON file:"./clever-react/package.json", json: props
        }
        container('gcloud') {
          sh 'gcloud config set auth/credential_file_override /jenkins-serviceaccount.json'
          sh 'gcloud config set project clever-ecommerce'
        }
      }
    }

    stage('Tests') {
      steps{
        container('node') {
          sh 'npm install --global npm@7.10.0'
          sh 'npm ci --legacy-peer-deps'
          sh 'npm test --watchAll=false'
        }
      }           
    }

    stage('Build & Deploy Staging image') {
      when {
        expression { env.BRANCH_NAME == 'develop' }
      }
      steps {
        container('gcloud') {
          sh 'gcloud builds submit --config=cloudbuild.yaml --substitutions=BRANCH_NAME=${GIT_BRANCH#*/},REVISION_ID=$BUILD_NUMBER,TAG_NAME=staging,_CLUSTER=clever-staging .'
        }
      }
    }

    stage('Build & Deploy Production image') {
      when {
        expression { env.BRANCH_NAME == 'master' }
      }
      steps {
        container('gcloud') {
          sh 'gcloud builds submit --config=cloudbuild.yaml --substitutions=BRANCH_NAME=${GIT_BRANCH#*/},REVISION_ID=$BUILD_NUMBER,TAG_NAME=latest,_CLUSTER=production-v2 .'
        }
      }
    }
  }
}
