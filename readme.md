# How to add a new icon

1. Add the icon in **SVG** inside the scripts/assets/svg folder
2.  The name of the Icon Component will be the same as the svg with the first one in capital letters
3. Run ```npm run icons``` to regenerate all icons

# How to start
Install dependencies run ```npm install --legacy-peer-deps```
run ```npm run ui```

# Workspaces npm 
- https://docs.npmjs.com/cli/v7/using-npm/workspaces