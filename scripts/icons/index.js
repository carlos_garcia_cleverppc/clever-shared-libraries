#!/usr/bin/env node
/* eslint-disable no-console */

const fs = require("fs");
const glob = require("glob");
const uppercamelcase = require("uppercamelcase");
const camelcase = require("lodash/camelCase");
const path = require("path");
const cheerio = require("cheerio");
const prettier = require("prettier");
const mkdirp = require("mkdirp");
const ejs = require("ejs");
const chalk = require("chalk");
const { optimize } = require("svgo");

const optimizePlugins = [
  "removeDoctype",
  "removeMetadata",
  "removeXMLNS",
  "removeEditorsNSData",
  "cleanupAttrs",
  "minifyStyles",
  "cleanupIDs",
  "removeUselessDefs",
  "cleanupNumericValues",
  "cleanupListOfValues",
  "removeUnknownsAndDefaults",
  "removeNonInheritableGroupAttrs",
  "removeUselessStrokeAndFill",
  "cleanupEnableBackground",
  "removeHiddenElems",
  "removeEmptyText",
  "convertShapeToPath",
  "moveElemsAttrsToGroup",
  "moveGroupAttrsToElems",
  "collapseGroups",
  "convertPathData",
  "convertTransform",
  "removeEmptyAttrs",
  "removeEmptyContainers",
  "mergePaths",
  "removeUnusedNS",
  "removeTitle",
  "removeDesc",
];

/**
 * Normalize paths relative to working directory
 * @param {*} path
 */

function normalizePath(p) {
  return path.join(process.cwd(), p);
}

/**
 * Generate react components and index files
 * @param {*} param0
 */

async function generateIcons({ source, assetsPaths, destination }) {
  const ext = ".js";
  const iconsList = [];

  const indexFilePath = path.join(destination, "index.js");

  // make the directory
  const directory = path.parse(indexFilePath).dir;
  await mkdirp(directory);

  // make the index file
  fs.writeFileSync(indexFilePath, "", "utf-8");

  // make the icon folder
  await mkdirp(path.join(destination, "icons"));

  // read the icon source
  return new Promise((resolve) => {
    glob(source, (err, icons) => {
      if (err) {
        throw err;
      }

      if (icons.length === 0) {
        throw Error(
          "Unable to find any icons. Make sure you use a glob format. [path/**.svg]"
        );
      }

      const ejsTemplate = fs.readFileSync(`${assetsPaths}/icon.ejs`, "utf8");
      const ejsIndexTemplate = fs.readFileSync(
        `${assetsPaths}/index.ejs`,
        "utf8"
      );

      icons.forEach((iconPath) => {
        const svg = fs.readFileSync(iconPath, "utf-8");

        const iconName = path.basename(iconPath, path.extname(iconPath));
        const componentName = uppercamelcase(iconName);

        const { data } = optimize(svg, {
          input: "file",
          plugins: optimizePlugins,
        });

        const $ = cheerio.load(data, { xmlMode: true });

        const iconDestination = path.join(destination, componentName + ext);

        // normalize the icon. This code is based on code found in react-icons
        // and react-feather.

        $("*").each((_, el) => {
          Object.keys(el.attribs).forEach((x) => {
            if (x.includes("-")) {
              $(el).attr(camelcase(x), el.attribs[x]).removeAttr(x);
            }

            if (x.includes(":")) {
              $(el).removeAttr(x);
            }

            if (x === "class") {
              $(el).removeAttr("class");
            }

            if (x === "fill") {
              const val = $(el).attr(x);
              if (val !== "none") {
                $(el).attr(x, "currentColor");
              }
            }

            if (x === "stroke") {
              const val = $(el).attr(x);
              if (val !== "none") {
                $(el).attr(x, "currentColor");
              }
            }

            if (x === "style") {
              const val = $(el).attr(x);
              // https://stackoverflow.com/a/38137700
              // Build camelCase JS object from style string.
              if (val !== "none") {
                const regexRule = /(?<=^|;)\s*([^:]+)\s*:\s*([^;]+)\s*/g;
                const cssObject = {};
                // eslint-disable-next-line no-return-assign
                val.replace(
                  regexRule,
                  (match, cssProperty, cssValue) =>
                    (cssObject[camelcase(cssProperty)] = cssValue)
                );
                $(el).attr(x, cssObject);
              }
            }
          });

          if (el.name === "svg") {
            $(el)
              .removeAttr("width")
              .removeAttr("height")
              .attr("height", 24)
              .attr("width", 24)
              .attr("other", "...");
          }
        });

        const svgString = $("svg")
          .toString()
          .replace(/<!--[\s\S]*?-->/g, "")
          .replace(
            new RegExp('stroke="currentColor"', "g"),
            "stroke={colorIcon}"
          )
          .replace(new RegExp('fill="currentColor"', "g"), "fill={colorIcon}")
          .replace(new RegExp("fill-opacity=", "g"), "fillOpacity=")
          .replace('width="24"', "width={sizeIcon}")
          .replace('height="24"', "height={sizeIcon}")
          .replace('other="..."', "{...otherProps}");

        const generatedFile = prettier.format(
          ejs.render(ejsTemplate, { svg: svgString, componentName }),
          { parser: "babel" }
        );

        fs.writeFileSync(iconDestination, generatedFile, "utf8");

        // append the export statement to the index file
        iconsList.push(componentName);
      });

      const indexFile = prettier.format(
        ejs.render(ejsIndexTemplate, { iconsList }),
        { parser: "babel" }
      );

      fs.writeFileSync(path.join(destination, "index.js"), indexFile, "utf-8");

      resolve(iconsList);
    });
  });
}

async function main() {
  const destination = normalizePath("./clever-react/src/ui/Icon");
  const assetsPaths = normalizePath("./scripts/icons/assets");
  const source = normalizePath("./scripts/icons/assets/svg/**.svg");

  console.log(chalk.blue("source: ", source));
  console.log(chalk.blue("Assets Path: ", assetsPaths));
  console.log(chalk.blue("destination: ", destination));

  const icons = await generateIcons({ destination, source, assetsPaths });

  console.log(chalk.green(`${icons.length} Icons exported to: ${destination}`));
}

main();
