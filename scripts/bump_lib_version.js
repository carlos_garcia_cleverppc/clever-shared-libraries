const fs = require("fs");

const packages = process.argv.slice(2);

const currentPackageLib = JSON.parse(fs.readFileSync("package.json", "utf-8"));
const { version, name } = currentPackageLib;

packages.forEach(folder => {
  const packageUrl = `${folder}/package.json`;

  const packageJson = JSON.parse(fs.readFileSync(packageUrl, "utf-8"));
  if (packageJson.dependencies[name]) {
    packageJson.dependencies[name] = version;
    const packageUpdated = JSON.stringify(packageJson, null, 2);
    fs.writeFileSync(packageUrl, packageUpdated, "utf-8");
  }
});
