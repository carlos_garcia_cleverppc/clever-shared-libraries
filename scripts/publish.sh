#!/bin/bash
echo "Loading Version"

VERSION=$(cat ../clever-react/package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')

echo "Commit new version $VERSION"

git add --all
git commit -m "Build Version $VERSION"

git tag "v$VERSION"
git push origin --tags
git push

echo "run npm publish for publish $VERSION version"
