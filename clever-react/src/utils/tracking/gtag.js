/* eslint-disable no-console */
import { getEnvConfig } from "../../ui/Provider/themeConfig";

const gtagConfig = getEnvConfig.gtag;

const event = (type, name, data) => {
  if (!gtagConfig.isSandbox && !!window.gtag) {
    if (window.gtag) {
      window.gtag(type, name, data);
    }
  } else {
    console.log(`SANDBOX: Gtag --> In ${gtagConfig.id} Type: ${type}, Name: ${name}, Data: ${JSON.stringify(data)}`);
  }
};

export default ({ event });
