/* eslint-disable no-console */
import mixpanel from "mixpanel-browser";

import { getStorageInstance } from "../storage";

const storage = getStorageInstance();
let config = { isSandbox: true, isInit: false, id: "", options: {} };

export const init = ({ options, isSandbox, id }) => {
  if (!config.isInit) {
    config = { ...config, isSandbox, options, id, isInit: true };
  }
};

function getLastEvent(category) {
  const auxEvent = storage.getItem(`LAST_MIXPANEL_${category}`);

  if (!auxEvent) {
    storage.setItem(`LAST_MIXPANEL_${category}`, "");

    return "no-event";
  }
  return auxEvent;
}

function updateLastEvent(category, eventName) {
  storage.setItem(`LAST_MIXPANEL_${category}`, eventName);
}

const identify = userId => (mixpanel.identify(userId));

const genericHandler = category => (eventName, userId, platform, userEmail) => {
  const lastEvent = getLastEvent(category);

  if (lastEvent && lastEvent.toLowerCase() !== eventName.toLowerCase()) {
    updateLastEvent(category, eventName);

    if (!config.isSandbox) {
      mixpanel.people.set({ $email: userEmail });
      mixpanel.track(eventName, { platform });
    } else {
      console.log(`SANDBOX: Mixpanel Track --> In ${config.id} Category: ${category}, Action: ${eventName}`);
    }
  }
};

const conversion = (action, value) => {
  const lastEvent = getLastEvent("conversion");

  if (lastEvent && lastEvent.toLowerCase() !== action.toLowerCase()) {
    updateLastEvent("conversion", action);

    if (!config.isSandbox) {
      mixpanel.event({ category: "Conversion", action, value });
    } else {
      console.log(`SANDBOX: Google Track --> In ${config.id} Category: Conversion, Action: ${action}`);
    }
  }
};

export default ({
  pageView: genericHandler("PageView"),
  event: genericHandler("Event"),
  label: genericHandler("Label"),
  conversion,
  identify,
});
