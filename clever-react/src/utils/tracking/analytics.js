/* eslint-disable no-console */
import ReactGA from "react-ga";

import { getStorageInstance } from "../storage";

const storage = getStorageInstance();
let config = { isSandbox: true, isInit: false, id: "", options: {} };

export const init = ({ options, isSandbox, id }) => {
  if (!config.isInit) {
    config = { ...config, isSandbox, options, id, isInit: true };
  }
};

function getLastEvent(category) {
  const auxEvent = storage.getItem(`LAST_ANALYTICS_${category}`);

  if (!auxEvent) {
    storage.setItem(`LAST_ANALYTICS_${category}`, "");

    return "no-event";
  }
  return auxEvent;
}

function updateLastEvent(category, eventName) {
  storage.setItem(`LAST_ANALYTICS_${category}`, eventName);
}

const genericHandler = category => (action, label = false) => {
  const lastEvent = getLastEvent(category);

  if (lastEvent && lastEvent.toLowerCase() !== action.toLowerCase()) {
    updateLastEvent(category, action);

    if (!config.isSandbox) {
      if (!label) {
        ReactGA.event({ category, action });
      } else {
        ReactGA.event({ category, action, label });
      }
    } else {
      // eslint-disable-next-line no-console
      console.log(`SANDBOX: Google Track --> In ${config.id} Category: ${category}, Action: ${action}`);
    }
  }
};

const conversion = (action, label, value, force = false) => {
  const lastEvent = getLastEvent("Conversion");

  if (force || (lastEvent && lastEvent.toLowerCase() !== action.toLowerCase())) {
    updateLastEvent("Conversion", action);

    if (!config.isSandbox) {
      if (!label) {
        ReactGA.event({ category: "Conversion", action, value });
      } else {
        ReactGA.event({ category: "Conversion", action, value, label });
      }
    } else {
      console.log(`SANDBOX: Google Track --> In ${config.id} Category: Conversion, Action: ${action}, Value: ${value}`);
    }
  }
};

export default ({
  pageView: genericHandler("PageView"),
  event: genericHandler("Event"),
  label: genericHandler("Label"),
  conversion,
});
