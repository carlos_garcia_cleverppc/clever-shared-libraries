import smartlookClient from "smartlook-client";

let config = { isSandbox: true, isInit: false, id: "", properties: [] };

export const init = (id, isSandbox) => {
  if (!isSandbox && !config.isInit) {
    smartlookClient.init(id);
  }

  if (!config.isInit) {
    config = { ...config, isSandbox, id, isInit: true };
  }
};

export const trackEvent = (eventId, properties = {}) => {
  if (!config.isSandbox && config.isInit) {
    smartlookClient.track(eventId, properties);
  }
};

export default ({
  init,
  trackEvent,
});
