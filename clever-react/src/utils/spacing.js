import forEach from "lodash-es/forEach";
import range from "lodash-es/range";

export default (calculateSpace, unit, spacersSize) => {
  const spacersProps = { margin: "m", padding: "p" };

  const sizesProps = { height: "h", width: "w" };
  const sizes = {
    0: 0,
    10: "10%",
    20: "20%",
    25: "25%",
    30: "30%",
    40: "40%",
    50: "50%",
    60: "60%",
    70: "70%",
    75: "75%",
    80: "80%",
    90: "90%",
    100: "100%",
    auto: "auto",
  };

  const styles = {
    "m-auto": { margin: "auto" },

    "mt-auto": { marginTop: "auto" },
    "mr-auto": { marginRight: "auto" },
    "mb-auto": { marginBottom: "auto" },
    "ml-auto": { marginLeft: "auto" },

    "my-auto": { marginTop: "auto", marginBottom: "auto" },
    "mx-auto": { marginLeft: "auto", marginRight: "auto" },
  };

  forEach(range(spacersSize), key => {
    forEach(Object.keys(spacersProps), prop => {
      styles[`${spacersProps[prop]}-${key}`] = {};
      styles[`${spacersProps[prop]}t-${key}`] = {};
      styles[`${spacersProps[prop]}r-${key}`] = {};
      styles[`${spacersProps[prop]}b-${key}`] = {};
      styles[`${spacersProps[prop]}l-${key}`] = {};
      styles[`${spacersProps[prop]}x-${key}`] = {};
      styles[`${spacersProps[prop]}y-${key}`] = {};

      styles[`${spacersProps[prop]}-${key}`][`${prop}`] = `${calculateSpace(key)}${unit}`;

      styles[`${spacersProps[prop]}t-${key}`][`${prop}Top`] = `${calculateSpace(key)}${unit}`;
      styles[`${spacersProps[prop]}r-${key}`][`${prop}Right`] = `${calculateSpace(key)}${unit}`;
      styles[`${spacersProps[prop]}b-${key}`][`${prop}Bottom`] = `${calculateSpace(key)}${unit}`;
      styles[`${spacersProps[prop]}l-${key}`][`${prop}Left`] = `${calculateSpace(key)}${unit}`;

      styles[`${spacersProps[prop]}x-${key}`][`${prop}Left`] = `${calculateSpace(key)}${unit}`;
      styles[`${spacersProps[prop]}x-${key}`][`${prop}Right`] = `${calculateSpace(key)}${unit}`;

      styles[`${spacersProps[prop]}y-${key}`][`${prop}Top`] = `${calculateSpace(key)}${unit}`;
      styles[`${spacersProps[prop]}y-${key}`][`${prop}Bottom`] = `${calculateSpace(key)}${unit}`;

      if (spacersProps[prop] === "m") {
        styles[`${spacersProps[prop]}n-${key}`] = {};
        styles[`${spacersProps[prop]}nt-${key}`] = {};
        styles[`${spacersProps[prop]}nr-${key}`] = {};
        styles[`${spacersProps[prop]}nb-${key}`] = {};
        styles[`${spacersProps[prop]}nl-${key}`] = {};
        styles[`${spacersProps[prop]}nx-${key}`] = {};
        styles[`${spacersProps[prop]}ny-${key}`] = {};

        styles[`${spacersProps[prop]}n-${key}`][`${prop}`] = `-${calculateSpace(key)}${unit}`;

        styles[`${spacersProps[prop]}nt-${key}`][`${prop}Top`] = `-${calculateSpace(key)}${unit}`;
        styles[`${spacersProps[prop]}nr-${key}`][`${prop}Right`] = `-${calculateSpace(key)}${unit}`;
        styles[`${spacersProps[prop]}nb-${key}`][`${prop}Bottom`] = `-${calculateSpace(key)}${unit}`;
        styles[`${spacersProps[prop]}nl-${key}`][`${prop}Left`] = `-${calculateSpace(key)}${unit}`;

        styles[`${spacersProps[prop]}nx-${key}`][`${prop}Left`] = `-${calculateSpace(key)}${unit}`;
        styles[`${spacersProps[prop]}nx-${key}`][`${prop}Right`] = `-${calculateSpace(key)}${unit}`;

        styles[`${spacersProps[prop]}ny-${key}`][`${prop}Top`] = `-${calculateSpace(key)}${unit}`;
        styles[`${spacersProps[prop]}ny-${key}`][`${prop}Bottom`] = `-${calculateSpace(key)}${unit}`;
      }
    });
  });

  forEach(Object.keys(sizes), key => {
    forEach(Object.keys(sizesProps), prop => {
      styles[`${sizesProps[prop]}-${key}`] = {};

      styles[`${sizesProps[prop]}-${key}`][`${prop}`] = sizes[key];
    });
  });

  return styles;
};
