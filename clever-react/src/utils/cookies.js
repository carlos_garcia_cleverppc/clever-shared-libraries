import { useCookies as useCookiesLib, withCookies as withCookiesLib } from "react-cookie";

export const useCookies = useCookiesLib;
export const withCookies = withCookiesLib;

export default useCookies;
