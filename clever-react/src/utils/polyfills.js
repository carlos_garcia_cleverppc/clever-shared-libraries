// Polyfill stable language features. (stage 4)
// It's recommended to use @babel/preset-env and package.json > browserslist
// include the polyfills necessary for the target browsers.
// https://babeljs.io/docs/en/babel-polyfill

// This must be the first line in src/index.js
import "react-app-polyfill/stable";
