import forEach from "lodash-es/forEach";

export const actions = { LAUNCH_ERROR: "LAUNCH_ERROR" };

function hasError(payload) {
  return !!payload && Object.keys(payload).length > 0
                   && payload?.error?.trim().length > 0
                   && payload.status;
}

export const launchAction = (dispatch, type, payload) => {
  let hasErrorPayload = false;
  let statusCode = 200;

  forEach(Object.keys(payload), key => {
    if (hasError(payload[key])) {
      hasErrorPayload = true;
      statusCode = payload[key].status;
    }
  });

  if (hasErrorPayload) {
    dispatch({ type: actions.LAUNCH_ERROR, payload: { hasError: true, code: statusCode } });
  }

  dispatch({ type, payload });
};
