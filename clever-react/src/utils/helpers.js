/* eslint-disable no-param-reassign */
import camelCase from "lodash-es/camelCase";

export const isValidString = candidateString => candidateString?.trim().length > 0;
export const isValidUrl = urlString => {
  if (!isValidString(urlString)) return false;

  urlString = urlString.toLowerCase();

  if (!/^(?:http(s)?:\/\/)?[\w.-]+(?:.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,;=.]+$/.test(urlString)) return false;
  if (urlString.indexOf("youtube.") !== -1) return false;
  if (urlString.indexOf("youtu.") !== -1) return false;
  if (urlString.indexOf("twitter.") !== -1) return false;
  if (urlString.indexOf("facebook.") !== -1) return false;
  if (urlString.indexOf("google.") !== -1) return false;

  return true;
};

export const isValidEmail = email => isValidString(email) && (/^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+.)+[^<>()[\].,;:\s@"]{2,})$/i).test(email);

export const handleCheckboxChange = name => (setState, event) => {
  setState({ [name]: event.target.checked });
};
export const handleTextChange = name => (setState, event) => {
  setState({ [name]: event.target.value });
};

export const isObject = (o => o instanceof Object && o.constructor === Object);
export const resolveCamelCaseObject = object => {
  if (isObject(object)) {
    const newObject = {};

    Object.keys(object).forEach(key => {
      newObject[camelCase(key)] = resolveCamelCaseObject(object[key]);
    });

    return newObject;
  } if (object instanceof Array) {
    return object.map(value => resolveCamelCaseObject(value));
  }
  return object;
};

export const onKeyDown = action => ev => {
  const { key, keyCode } = ev;

  if (key === "Enter" || keyCode === 32) {
    ev.preventDefault();
    action();
  }
};

export default ({
  isValidString,
  isValidUrl,
  handleCheckboxChange,
  handleTextChange,
  isObject,
  resolveCamelCaseObject,
  onKeyDown,
});
