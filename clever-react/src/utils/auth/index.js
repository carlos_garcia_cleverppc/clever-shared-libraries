import queryString from "query-string";

import { getStorageInstance } from "../storage";

export const AUTH_TOKEN = "AUTH_TOKEN";

const storage = getStorageInstance();

function getFromStorage() {
  const token = storage.getItem(AUTH_TOKEN);

  return token;
}

function getFromUrl() {
  const { jwt } = queryString.parse(window.location.search);

  return jwt;
}

function hasTokenInStorage() {
  const token = storage.getItem(AUTH_TOKEN);

  return !!token && token.trim().length > 0;
}

function hasTokenInUrl() {
  const { jwt } = queryString.parse(window.location.search);

  return !!jwt && jwt.trim().length > 0;
}

export const getAuthToken = () => {
  if (hasTokenInStorage()) return getFromStorage();
  if (hasTokenInUrl()) return getFromUrl();
  return false;
};

export const saveAuthToken = token => {
  if (storage.isSupported()) {
    storage.setItem(AUTH_TOKEN, token);
  }
};

export const deleteAuthToken = () => {
  storage.setItem(AUTH_TOKEN, "");
};

export const hasAuthToken = () => hasTokenInStorage() || hasTokenInUrl();
