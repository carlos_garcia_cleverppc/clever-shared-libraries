// eslint-disable-next-line no-restricted-properties
export const bytesToMb = bytes => bytes / Math.pow(1024, 2);

export const getBase64Image = file => new Promise((resolve, reject) => {
  const reader = new FileReader(file);

  reader.onload = e => {
    resolve(e.target.result);
  };
  reader.onError = e => {
    reject(e);
  };

  try {
    reader.readAsDataURL(file);
  } catch (e) {
    reject(e);
  }
});

export const getBase64MbSize = base64 => {
  const strFormated = base64.split(",")[1].split("=")[0];
  const strLength = strFormated.length;
  const bytes = strLength - (strLength / 8) * 2;

  return bytesToMb(bytes);
};
