import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import MaterialPaper from "@material-ui/core/Paper";

import { getColorsLabels, getShades } from "../styles/colors";

import getClasses from "./styles";

const colorsList = getColorsLabels();
const shadesList = getShades();

const Paper = props => {
  const { children, className, color, shade } = props;

  const classes = getClasses({ color, shade });

  return (
    <MaterialPaper className={classnames(classes.root, className)}>
      {children}
    </MaterialPaper>
  );
};

Paper.propTypes = {
  className: PropTypes.string,
  color: PropTypes.oneOf(colorsList),
  shade: PropTypes.oneOf(shadesList),
};

Paper.defaultProps = {
  className: "",
  color: "primary",
  shade: 500,
};

export default Paper;
