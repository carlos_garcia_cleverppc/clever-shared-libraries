import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing, shadows } }) => ({
  root: ({ color, shade }) => ({
    ...spacing["px-4"],
    ...spacing["py-2"],
    ...spacing["my-2"],
    backgroundColor: colors[color][shade],
    borderRadius: "32px",
    boxShadow: shadows.normal,
    zIndex: "10000",
  }),
});

export default makeStyles(styles);
