import allColors from "../Provider/colors";

export function getColors(colors = allColors, level = 0) {
  const flattened = {};

  Object.keys(colors).forEach(key => {
    if (typeof colors[key] === "object" && colors[key] !== null && level < 1) {
      Object.assign(flattened, getColors(colors[key], 1));
    } else {
      flattened[key] = colors[key];
    }
  });

  return flattened;
}

export function getColorsLabels(inherit = false) {
  const colors = Object.keys(getColors());

  if (inherit) colors.push("inherit");

  return colors;
}

export function getShades() {
  return ["lighter", "light", "main", "dark", "darker", 50, 100, 200, 300, 400, 500, 600, 700, 800, 900];
}

export function hex2rgb(hex) {
  return hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i,
    (m, r, g, b) => `#${r}${r}${g}${g}${b}${b}`)
    .substring(1).match(/.{2}/g)
    .map(x => parseInt(x, 16));
}

export function hex2rgba(hex) {
  const rgb = hex2rgb(hex);

  const min = Math.min(...rgb);

  const alpha = (255 - min) / 255;
  const [r, g, b] = [
    (rgb[0] - min) / alpha,
    (rgb[1] - min) / alpha,
    (rgb[2] - min) / alpha,
  ];

  return [r, g, b, alpha];
}

export function cssStringHex2rgba(hex) {
  const [r, g, b, a] = hex2rgba(hex);

  return `rgba(${r}, ${g}, ${b}, ${a.toFixed(3)})`;
}
