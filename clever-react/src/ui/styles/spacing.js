import { getCurrentConfig } from "../Provider/themeConfig";
import generateSpacing from "../../utils/spacing";

const { calculateSpace, unit } = getCurrentConfig().cleverUI.spacers;

export default generateSpacing(calculateSpace, unit);
