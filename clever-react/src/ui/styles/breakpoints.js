export const mediaBreakpointUpXs = "599.98px";
export const mediaBreakpointDownSm = "600px";
export const mediaBreakpointUpSm = "959.98px";
export const mediaBreakpointDownMd = "960px";
export const mediaBreakpointUpMd = "1279.98px";
export const mediaBreakpointDownLg = "1280px";
export const mediaBreakpointUpLg = "1919.98px";
export const mediaBreakpointDownXl = "1920px";

export const screenXs = `(max-width:${mediaBreakpointUpXs})`;
export const screenSm = `(max-width:${mediaBreakpointUpSm}) and (min-width:${mediaBreakpointDownSm})`;
export const screenMd = `(max-width:${mediaBreakpointUpMd}) and (min-width:${mediaBreakpointDownMd})`;
export const screenLg = `(max-width:${mediaBreakpointUpLg}) and (min-width:${mediaBreakpointDownLg})`;
export const screenXl = `(min-width:${mediaBreakpointDownXl})`;
