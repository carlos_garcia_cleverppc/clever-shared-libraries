/* eslint-disable no-multi-spaces */
import { getCurrentConfig } from "../Provider/themeConfig";

const { constant, unit } = getCurrentConfig().cleverUI.typography.fontSizeBase;
const { font1, font2 } = getCurrentConfig().cleverUI.typography;

export const sizing = {
  // Font1
  h1: (constant + 18) + unit,    // 36px
  h2: (constant + 6) + unit,     // 24px
  h3: (constant + 3) + unit,     // 21px
  h4: (constant + 0) + unit,
  h5: (constant + 0) + unit,
  h6: (constant + 0) + unit,     // 18px
  // Font2
  body0: (constant - 4) + unit,   // 14px
  body1: (constant + 0) + unit,   // 18px
  body2: (constant + 3) + unit,   // 21px
  body3: (constant + 6) + unit,   // 24px
  span: (constant + 0) + unit,
};

export const Font1 = font1;

export const Font2 = font2;

export default { sizing };
