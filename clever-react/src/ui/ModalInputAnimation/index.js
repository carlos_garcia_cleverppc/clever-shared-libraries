import React, { useState, useLayoutEffect, useRef } from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import Modal from "@material-ui/core/Modal";

import ShowMore from "../Icon/ShowMore";
import IconButton from "../IconButton";
import Button from "../Button";
import Popper from "../Popper";
import MenuItem from "../Menu/Item";
import TextField from "../TextField";
import Typography from "../Typography";

import getClasses from "./styles";

const ModalInput = props => {
  const { open, onSubmit, onClose } = props;
  const { title, icon } = props;
  const { defaultValue, placeholder, maxLength } = props;
  const { menuOptions, footer, disablePortal, className } = props;
  const { buttonText, buttonDisabled, inputProps } = props;

  const [position, setPosition] = useState({ x: 0, y: 0 });
  const [inputValue, setInputValue] = useState(defaultValue);
  const [anchorEl, setAnchorEl] = useState(null);
  const [isClosing, setIsClosing] = useState(false);

  const paperRef = useRef();

  const classes = getClasses({ target: position });

  const handleClose = () => {
    setIsClosing(true);

    setTimeout(() => {
      setAnchorEl(null);

      onClose();

      setIsClosing(false);
    }, 1000);
  };

  const isButtonDisabled = () => {
    if (inputProps?.value) return inputProps.value === "" || buttonDisabled;
    return inputValue === "" || buttonDisabled;
  };

  useLayoutEffect(() => {
    const element = document.getElementById("simple-popover-button1");

    if (!element || !paperRef.current) return;

    const rect = element.getBoundingClientRect();

    const target = {
      top: Math.floor(rect.y + rect.height / 2),
      left: Math.floor(rect.x + rect.width / 2),
    };

    const { width, height, top, left } = paperRef.current.getBoundingClientRect();

    const origin = {
      top: Math.floor(top + height / 2),
      left: Math.floor(left + width / 2),
    };

    const x = target.left - origin.left;
    const y = target.top - origin.top;

    setPosition({ x, y });
  }, [paperRef.current]);

  const tmp = {
    [classes.modalOut]: isClosing,
  };

  return (
    <Modal
      open={open}
      onClose={handleClose}
      disablePortal={disablePortal}
      className={cx(classes.modal)}
      hideBackdrop={isClosing}
    >
      <div className={cx(classes.paper, tmp, className)} ref={paperRef}>
        {menuOptions && (
          <>
            <IconButton
              onClick={ev => setAnchorEl(ev.currentTarget)}
              icon={<ShowMore />}
              iconSize={18}
              shade={400}
              className={classes.optionsIcon}
            />

            <Popper
              className={classes.menu}
              arrow
              placement="bottom-end"
              open={Boolean(anchorEl)}
              handleClose={() => setAnchorEl(null)}
              anchorEl={anchorEl}
              transition
              color="white"
              disablePortal={disablePortal}
            >
              <ul className={classes.menuList}>
                {menuOptions.map(option => (
                  <MenuItem onClick={option.onClick} key={`modal-input-option-${option.label}`}>
                    {option.icon}

                    <Typography color={option.color} variant="f2-16">
                      {option.label}
                    </Typography>
                  </MenuItem>
                ))}
              </ul>
            </Popper>
          </>
        )}

        {(title || icon) && (
          <div className={classes.title}>
            {icon}

            {title && (
              <Typography variant="f2-16" weight="bold" shade={700}>
                {title}
              </Typography>
            )}
          </div>
        )}

        <form
          onSubmit={ev => {
            ev.preventDefault();
            onSubmit(inputValue);
          }}
          className={classes.inputContainer}
        >
          <TextField
            variant="standard"
            value={inputValue}
            onChange={ev => setInputValue(ev.target.value)}
            placeholder={placeholder}
            autoFocus
            inputProps={maxLength ? { maxLength } : undefined}
            inputClass={classes.textField}
            {...inputProps}
          />

          {buttonText && (
            <Button
              type="submit"
              size="small"
              disabled={isButtonDisabled()}
              className={classes.submitButton}
            >
              {buttonText}
            </Button>
          )}
        </form>

        {footer && (
          <div className={classes.footer}>
            {footer}
          </div>
        )}
      </div>
    </Modal>
  );
};

ModalInput.propTypes = {
  open: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  icon: PropTypes.element,
  placeholder: PropTypes.string,
  defaultValue: PropTypes.string,
  maxLength: PropTypes.number,
  buttonText: PropTypes.string,
  buttonDisabled: PropTypes.bool,
  menuOptions: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    color: PropTypes.string,
  })),
  footer: PropTypes.element,
  disablePortal: PropTypes.bool,
  className: PropTypes.string,
};

ModalInput.defaultProps = {
  placeholder: "",
  defaultValue: "",
  disablePortal: false,
  className: "",
  buttonDisabled: false,
};

export default ModalInput;
