/* eslint-disable react/no-multi-comp */
import React, { useState } from "react";

import ModalInput from "./index";
import Calendar from "../Icon/Calendar";
import Button from "../Button";
import Typography from "../Typography";
import Snackbar from "../Snackbar";

export default {
  title: "ModalInput",
  component: ModalInput,
  argTypes: {},
};

const Template = args => {
  function Parent({ children }) {
    const [openModalInput, setOpenModalInput] = useState(false);
    const [inputValue, setInputValue] = useState("");
    const [openSnackbar, setOpenSnackbar] = useState(false);

    const id = "simple-modal-input";

    return (
      <div>
        <Button onClick={() => setOpenModalInput(true)} aria-describedby={id}>
          Open
        </Button>

        {children(
          openModalInput,
          setOpenModalInput,
          inputValue,
          setInputValue,
          openSnackbar,
          setOpenSnackbar,
          id,
        )}
      </div>
    );
  }

  return (
    <Parent>
      {(
        openModalInput,
        setOpenModalInput,
        inputValue,
        setInputValue,
        openSnackbar,
        setOpenSnackbar,
        id,
      ) => (
        <>
          <ModalInput
            {...args}
            id={id}
            open={openModalInput}
            onSubmit={value => {
              setInputValue(value);
              setOpenSnackbar(true);
              setOpenModalInput(false);
            }}
            onClose={() => setOpenModalInput(false)}
            menuOptions={[
              {
                label: "Option 1",
                onClick: () => {
                  setInputValue("Option 1 clicked!");
                  setOpenSnackbar(true);
                },
              },
              {
                label: "Option 2",
                onClick: () => {
                  setInputValue("Option 2 clicked!");
                  setOpenSnackbar(true);
                },
              },
            ]}
          />

          <Snackbar
            anchorOrigin={{
              vertical: "top",
              horizontal: "center",
            }}
            open={openSnackbar}
            autoHideDuration={3000}
            onClose={() => setOpenSnackbar(false)}
            text={inputValue}
          />
        </>
      )}
    </Parent>
  );
};

export const SimpleModalInput = Template.bind({});
SimpleModalInput.args = {
  defaultValue: "",
  title: "Modal input title",
  icon: <Calendar />,
  placeholder: "Enter your value here...",
  footer: <Typography>This is an example footer component</Typography>,
};
