import React from "react";
import PropTypes from "prop-types";

import { Group } from "@vx/group";
import { Bar } from "@vx/shape";
import { scaleLinear, scaleBand } from "@vx/scale";
import { useTooltip, TooltipWithBounds } from "@vx/tooltip";
import { localPoint } from "@vx/event";
import { Text } from "@vx/text";
import { AxisLeft } from "@vx/axis";
import { GridRows } from "@vx/grid";

import compact from "lodash-es/compact";
import isEmpty from "lodash-es/isEmpty";
import Typography from "../Typography";
import getClasses from "./styles";
import useTheme from "../styles/useTheme";

const BarGraph = props => {
  const { color, type, data, height, width } = props;
  const { showValues, axis } = props;
  const axisNum = 3;

  const classes = getClasses(props);
  const { cleverUI: { colors, spacing } } = useTheme();

  const myChartRef = React.createRef();

  const getShow = () => {
    if (showValues === "responsive") {
      if ((width / data.length) < 40) {
        return "odd";
      }
      return "all";
    }
    return showValues;
  };

  const show = getShow();

  const showAxis = axis && !isEmpty(compact(data.map(v => v.y !== 0)));

  const marginTop = show === "" ? 0 : 20;
  const marginLeft = showAxis ? 30 : 0;

  // Then we'll create some bounds
  const xMax = width - marginLeft;
  const yMax = height - marginTop;

  // We'll make some helpers to get at the data we want
  const x = d => d.x;
  const y = d => +d.y * 100;

  // And then scale the graph by our data
  const xScale = scaleBand({
    range: [0, xMax],
    round: true,
    domain: data.map(x),
    padding: 0.4,
  });

  const yMaxValue = Math.max(...data.map(y));

  const yScale = scaleLinear({
    range: [yMax, 0],
    round: true,
    domain: [0, yMaxValue === 0 ? yMax : yMaxValue],
  });

  // Compose together the scale and accessor functions to get point functions
  const compose = (scale, accessor) => data2 => scale(accessor(data2));
  const xPoint = compose(xScale, x);
  const yPoint = compose(yScale, y);

  const {
    tooltipData,
    tooltipLeft,
    tooltipTop,
    tooltipOpen,
    showTooltip,
    hideTooltip,
  } = useTooltip();

  const handleMouseOver = (event, value) => {
    const position = myChartRef.current.getBoundingClientRect();
    const coords = localPoint(event.target.ownerSVGElement, event);
    showTooltip({
      tooltipLeft: coords.x,
      tooltipTop: coords.y - position.height,
      tooltipData: value,
    });
  };

  const tooltipStyles = {
    zIndex: 100000,
    backgroundColor: colors.navyBlue[500],
    minWidth: "110px",
    borderRadius: "8px",
    display: "inline-block",
    whiteSpace: "pre-line",
    ...spacing["pl-3"],
    ...spacing["pr-5"],
    ...spacing["pt-3"],
    ...spacing["pb-4"],
    textAlign: "left",
  };

  const yScaleGrid = scaleLinear({
    domain: [0, Math.max(...data.map(v => v.y))],
    nice: true,
  });

  yScaleGrid.range([yMax, 0]);
  yScaleGrid.ticks(axisNum);

  const ticks = yScaleGrid.ticks(axisNum);
  const tickFormats = yScaleGrid.tickFormat(axisNum, "~s");
  ticks.map(tickFormats);

  // Finally we'll embed it all in an SVG
  return (
    <div className={classes.root}>
      <svg width={width} height={height} ref={myChartRef}>
        {!!showAxis && (
          <GridRows
            scale={yScaleGrid}
            width={xMax}
            left={marginLeft + 5}
            numTicks={axisNum}
          />
        )}

        {data.map((d, i) => {
          const barHeight = (yMax - yPoint(d)) === 0 ? 2 : yMax - yPoint(d);
          const yValue = yMax - barHeight - marginTop;
          return (
            <Group key={`bar-${d.x}`} top={show !== "" ? marginTop : 0} left={showAxis ? marginLeft : 0}>
              {(show === "all" || (show === "even" && i % 2 === 0) || (show === "odd" && i % 2 !== 0)) && (
                <Text
                  verticalAnchor="start"
                  textAnchor="middle"
                  style={{ fontSize: "14px", zIndex: 1000000, paddingTop: "15px" }}
                  fill={colors[color][500]}
                  fontFamily="Helvetica"
                  dx={xPoint(d) + 3}
                  dy={yValue}
                  fontWeight={600}
                >
                  {d.y ? d.y : "0"}
                </Text>
              )}

              {!!showAxis && (
                <AxisLeft
                  scale={yScaleGrid}
                  stroke="transparent"
                  tickStroke="transparent"
                  tickValues={ticks}
                  tickFormat={tickFormats}
                  left={-20}
                  tickLabelProps={() => ({
                    fill: colors.neutral[300],
                    fontSize: 12,
                    fontFamily: "Helvetica",
                  })}
                />
              )}

              <Bar
                x={xPoint(d)}
                y={yMax - barHeight}
                height={barHeight}
                width={6}
                fill={colors[color][500]}
                rx={3}
                onMouseOver={ev => handleMouseOver(ev, d)}
                onMouseOut={hideTooltip}
              />
            </Group>
          );
        })}
      </svg>

      {tooltipOpen && (
        <TooltipWithBounds
          // set this to random so it correctly updates with parent bounds
          key={Math.random()}
          top={tooltipTop}
          left={tooltipLeft}
          style={tooltipStyles}
        >
          <Typography variant="f1-12" weight="bold" color="white">
            {tooltipData.y}
            {" "}
            {type}
          </Typography>

          <Typography variant="f2-12">{tooltipData.x}</Typography>
        </TooltipWithBounds>
      )}
    </div>
  );
};

BarGraph.propTypes = {
  showValues: PropTypes.oneOf(["", "all", "odd", "even", "responsive"]),
  height: PropTypes.number,
  width: PropTypes.number,
  axis: PropTypes.bool,
  data: PropTypes.instanceOf(Array),
  type: PropTypes.string,
  color: PropTypes.string,
};

BarGraph.defaultProps = {
  showValues: "",
  height: 300,
  width: 300,
  axis: false,
  data: [],
  type: "",
  color: "primary",
};

export default BarGraph;
