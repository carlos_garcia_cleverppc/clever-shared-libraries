/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import BarGraph from "./index";

export default {
  title: "BarGraph",
  component: BarGraph,
};

const Template = args => <BarGraph {...args} />;

export const SimpleBarGraph = Template.bind({});
SimpleBarGraph.args = {
  data: [
    { x: "30/06/20 - 06/07/20", y: 144955 },
    { x: "07/07/20 - 13/07/20", y: 135147 },
    { x: "14/07/20 - 20/07/20", y: 134081 },
    { x: "21/07/20 - 27/07/20", y: 140636 },
    { x: "28/07/20 - 03/08/20", y: 136141 },
    { x: "04/08/20 - 10/08/20", y: 136701 },
    { x: "11/08/20 - 17/08/20", y: 140298 },
    { x: "18/08/20 - 24/08/20", y: 144319 },
    { x: "25/08/20 - 31/08/20", y: 155409 },
    { x: "01/09/20 - 07/09/20", y: 107166 },
    { x: "08/09/20 - 14/09/20", y: 34671 },
    { x: "15/09/20 - 21/09/20", y: 88946 },
    { x: "22/09/20 - 27/09/20", y: 43877 }],
  color: "blue",
};

export const ValuesAndAxisBarGraph = Template.bind({});
ValuesAndAxisBarGraph.args = {
  data: [
    { x: "30/06/20 - 06/07/20", y: 144955 },
    { x: "07/07/20 - 13/07/20", y: 135147 },
    { x: "14/07/20 - 20/07/20", y: 134081 },
    { x: "21/07/20 - 27/07/20", y: 140636 },
    { x: "28/07/20 - 03/08/20", y: 136141 },
    { x: "04/08/20 - 10/08/20", y: 136701 },
    { x: "11/08/20 - 17/08/20", y: 140298 },
    { x: "18/08/20 - 24/08/20", y: 144319 },
    { x: "25/08/20 - 31/08/20", y: 155409 },
    { x: "01/09/20 - 07/09/20", y: 107166 },
    { x: "08/09/20 - 14/09/20", y: 34671 },
    { x: "15/09/20 - 21/09/20", y: 88946 },
    { x: "22/09/20 - 27/09/20", y: 43877 }],
  color: "green",
  showValues: "all",
  axis: true,
  width: 900,
};
