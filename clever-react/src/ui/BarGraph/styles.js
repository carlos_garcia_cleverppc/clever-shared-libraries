import makeStyles from "../styles/makeStyles";

const styles = () => ({
  root: ({ height, width }) => ({ height, width }),
});

export default makeStyles(styles);
