import React, { useCallback } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { VariableSizeList as List } from "react-window";

import Typography from "../Typography";
import Grid from "../Grid";

import getClasses from "./styles";

// eslint-disable-next-line import/no-cycle
import Row from "./row";

export const ListContext = React.createContext({});

const ActionList = ({ className, listInfo, onClick, variant, action, emptyText }) => {
  const classes = getClasses({ variant });

  const sizeMap = React.useRef({});
  const listRef = React.useRef();

  const setSize = useCallback((index, size) => {
    if (!sizeMap[index]) {
      sizeMap.current = { ...sizeMap.current, [index]: size };
      listRef.current.resetAfterIndex(index);
    }
  }, []);

  const getSize = useCallback(index => (sizeMap.current[index] || 45), []);

  return (
    <ListContext.Provider value={{ setSize }}>
      <div className={classes.optimizedList}>
        {listInfo.length > 0 && (
          <List
            className={classes.root}
            height={400}
            itemCount={listInfo.length}
            itemSize={getSize}
            width="100%"
            ref={listRef}
          >
            {({ index, style }) => (
              <div style={style}>
                <Row
                  variant={variant}
                  index={index}
                  item={listInfo[index]}
                  className={className}
                  onClick={onClick}
                  action={action}
                />
              </div>
            )}
          </List>
        )}

        {listInfo.length === 0 && (
          <Grid container justify="center" alignItems="center" className={classnames(classes.root, classes.emptyList)}>
            <Grid item>
              <Typography variant="body1">
                {emptyText}
              </Typography>
            </Grid>
          </Grid>
        )}
      </div>
    </ListContext.Provider>
  );
};

ActionList.propTypes = {
  className: PropTypes.string,
  listInfo: PropTypes.instanceOf(Object).isRequired,
  onClick: PropTypes.func,
  variant: PropTypes.oneOf(["", "primary"]),
  action: PropTypes.oneOf(["add", "remove"]),
  emptyText: PropTypes.string,
};

ActionList.defaultProps = {
  className: "",
  onClick: () => { },
  variant: "",
  action: "add",
  emptyText: "",
};

export default ActionList;
