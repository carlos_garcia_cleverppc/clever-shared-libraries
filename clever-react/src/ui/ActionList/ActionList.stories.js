/* eslint-disable no-console */
import React from "react";

import ActionList from "./index";

export default {
  title: "ActionList",
  component: ActionList,
};

const Template = args => {
  const listInfo = [
    { id: "1", label: "The Shawshank Redemption" },
    { id: "2", label: "The Godfather" },
    { id: "3", label: "The Godfather: Part II" },
    { id: "4", label: "The Dark Knight" },
    { id: "5", label: "12 Angry Men" },
    { id: "6", label: "Schindler's List" },
    { id: "7", label: "Pulp Fiction" },
    { id: "8", label: "The Lord of the Rings: The Return of the King" },
    { id: "10", label: "The Good, the Bad and the Ugly" },
  ];

  const handleClick = (event, data) => {
    console.log("click", data);
  };

  return <ActionList {...args} listInfo={listInfo} onClick={handleClick} />;
};

export const SimpleActionList = Template.bind({});
SimpleActionList.args = {};

const EmptyTemplate = args => {
  const listInfo = [];

  const handleClick = (event, data) => {
    console.log("click", data);
  };

  return <ActionList {...args} listInfo={listInfo} onClick={handleClick} />;
};

export const EmptyActionList = EmptyTemplate.bind({});
EmptyActionList.args = { emptyText: "No entries" };
