/* eslint-disable react/no-multi-comp */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { CSSTransition, TransitionGroup } from "react-transition-group";
// import ReactCSSTransitionGroup from "react-addons-css-transition-group";

import ListItem from "../List/Item";
import IconButton from "../IconButton";
import Typography from "../Typography";
import Grid from "../Grid";

import Times from "../Icon/Times";
import Plus from "../Icon/Plus";

import getClasses from "./styles";

const ActionList = ({ className, listInfo, onClick, variant, action, emptyText }) => {
  const classes = getClasses({ variant });

  const Row = index => (
    <CSSTransition
      key={index}
      timeout={500}
      classNames={{
        enter: classes.moveEnter,
        enterActive: classes.moveEnterActive,
        exit: classes.moveExit,
        exitActive: classes.moveExitActive,
      }}
    >
      <div>
        <ListItem className={
          classnames(index % 2 === 1 ? classes.rowColor : classes.rowColorWhite,
            classes.list, className)
        }
        >
          <Typography color={variant || "inherit"} className={classnames(classes.itemText)}>
            {listInfo[index].label}
          </Typography>

          <IconButton
            size="small"
            onClick={e => onClick(e, listInfo[index].id)}
            icon={action === "remove" ? <Times /> : <Plus />}
            color={!variant ? "neutral" : variant}
            shade={100}
            outlined={false}
            className={classes.icon}
          />
        </ListItem>
      </div>
    </CSSTransition>

  );

  return (
    <>
      {listInfo.length > 0 && (
        <TransitionGroup>
          {listInfo.map((item, index) => (Row(index)))}
        </TransitionGroup>

      )}

      {listInfo.length === 0 && (
        <Grid
          container
          justify="center"
          alignItems="center"
          className={classnames(classes.root, classes.emptyList)}
        >
          <Grid item>
            <Typography variant="body1">
              {emptyText}
            </Typography>
          </Grid>
        </Grid>
      )}
    </>
  );
};

ActionList.propTypes = {
  className: PropTypes.string,
  listInfo: PropTypes.instanceOf(Object).isRequired,
  onClick: PropTypes.func,
  variant: PropTypes.oneOf(["", "primary"]),
  action: PropTypes.oneOf(["add", "remove"]),
  emptyText: PropTypes.string,
};

ActionList.defaultProps = {
  className: "",
  onClick: () => { },
  variant: "primary",
  action: "add",
  emptyText: "",
};

export default ActionList;
