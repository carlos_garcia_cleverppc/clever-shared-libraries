import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  root: {
    overflow: "auto",
    border: `solid 1px ${colors.neutral[300]}`,
    borderRadius: "5px",
    "&::-webkit-scrollbar": {
      width: "12px",
      backgroundColor: "#fff",
    },
    "&::-webkit-scrollbar-track": { backgroundColor: "#fff" },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: colors.neutral[300],
      borderRadius: "16px",
      border: "4px solid #fff",
    },
  },
  emptyList: { height: "400px" },
  rowColor: ({ variant }) => ({
    backgroundColor: !variant ? `${colors.neutral[200]} !important` : `${colors[variant][100]} !important`,
  }),
  rowColorWhite: { backgroundColor: "white !important" },
  icon: { ...spacing["ml-auto"] },
  list: {
    ...spacing["p-3"],
    "& p": { wordBreak: "break-word" },
  },
  itemText: ({ variant }) => ({ color: !variant ? colors.neutral[700] : colors[variant][500] }),
  enter: {
    opacity: "0.01",
    transform: "translate(-40px, 0)",
  },
  enterActive: {
    opacity: "1",
    transform: "translate(0, 0)",
    transition: "all 500ms ease-in",
  },
  leave: {
    opacity: "1",
    transform: "translate(0, 0)",
  },
  leaveActive: {
    opacity: "0.01",
    transform: "translate(40px, 0)",
    transition: "all 500ms ease-in",
  },
  moveEnter: {
    opacity: "0.01",
    transform: "translate(-40px, 0)",
  },
  moveEnterActive: {
    opacity: "1",
    transform: "translate(0, 0)",
    transition: "all 500ms ease-in",
  },
  moveExit: {
    opacity: "1",
    transform: "translate(0, 0)",
  },
  moveExitActive: {
    opacity: "0.01",
    transform: "translate(-40px, 0)",
    transition: "all 500ms ease-in-out",
  },
  optimizedList: {
    width: "100%",
    height: "400px",
  },
});

export default makeStyles(styles);
