/* eslint-disable react-hooks/exhaustive-deps */
import React, { forwardRef } from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

import ListItem from "../List/Item";
import IconButton from "../IconButton";
import Typography from "../Typography";

import Times from "../Icon/Times";
import Plus from "../Icon/Plus";

import getClasses from "./styles";

// eslint-disable-next-line import/no-cycle
import { ListContext } from "./index";

// eslint-disable-next-line no-unused-vars
const Row = forwardRef((props, ref) => {
  const { item, index, variant, className, onClick, action } = props;

  const classes = getClasses({ variant });

  const { setSize } = React.useContext(ListContext);

  const root = React.useRef();

  React.useEffect(() => {
    setSize(index, root.current.getBoundingClientRect().height);
  }, []);

  return (
    <div ref={root}>
      <ListItem
        ref={root}
        padding={false}
        className={
          classnames(index % 2 === 1 ? classes.rowColor : classes.rowColorWhite,
            classes.list, className)
        }
      >
        <Typography color={variant || "neutral"} className={classnames(classes.itemText)}>
          {item.label}
        </Typography>

        <IconButton
          size="small"
          onClick={e => onClick(e, item.id)}
          icon={action === "remove" ? <Times /> : <Plus />}
          color={!variant ? "neutral" : variant}
          shade={100}
          outlined={false}
          className={classes.icon}
        />
      </ListItem>
    </div>
  );
});

Row.propTypes = { variant: PropTypes.string };

Row.defaultProps = { variant: "neutral" };

export default Row;
