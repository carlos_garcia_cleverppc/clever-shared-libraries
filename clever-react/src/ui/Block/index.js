/* eslint-disable react/no-multi-comp */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import split from "lodash-es/split";

import { onKeyDown } from "../../utils/helpers";
import { getColorsLabels } from "../styles/colors";

import Typography from "../Typography";
import OpenBlock from "../Icon/OpenBlock";
import CheckRound from "../Icon/CheckRound";
import Grid from "../Grid";
import Image from "../Image";
import At from "../Icon/At";

import getClasses from "./styles";

const colorsList = getColorsLabels(true);

const Block = props => {
  const { children, size, title, description, link, color } = props;
  const { active, selected, applied, inactive, image } = props;
  const { handleOpen, className, variant, icon, value } = props;
  const { onClick, titleLines, descriptionLines, imageSize } = props;

  const width = split(size, "x")[1];
  const height = split(size, "x")[0];
  const imageWidth = () => {
    switch (imageSize) {
      case "sm":
        return "25";
      case "md":
        return "50";
      case "lg":
        return "75";
      case "xl":
        return "100";
      default:
        return "100";
    }
  };

  const classes = getClasses({
    width,
    height,
    color,
    titleLines,
    descriptionLines,
    active,
    variant,
    link,
    imageWidth: imageWidth(),
  });

  const getStyle = () => {
    const styles = [classes.block];

    if (variant === "dashed") {
      styles.push(classes.dashedBlock);
    } else if (selected) styles.push(classes.selected);
    else if (active) styles.push(classes.active);
    else if (applied) styles.push(classes.applied);
    else if (inactive) styles.push(classes.inactive);
    else styles.push(classes.normal);

    if (!!handleOpen || !!onClick) styles.push(classes.hoverable);
    styles.push(className);

    return styles;
  };

  const getOpenIconButton = () => {
    if (!!handleOpen && !selected) {
      return (
        <OpenBlock
          className={active
            ? classnames(classes.icon, classes.iconButton, classes.iconActive)
            : classnames(classes.icon, classes.iconButton)}
        />
      );
    }
    return "";
  };

  const getCheckIconButton = () => (
    <CheckRound
      className={classnames(classes.icon, classes.iconButton, classes.iconCheck)}
    />
  );

  const getInfoBlock = () => {
    if (height === "1" && width === "1") {
      return (
        <>
          <div className={classes.closeIconContainer}>
            {getOpenIconButton()}
          </div>

          <div className={classes.marginAuto}>
            <Typography variant="h6" className={classes.title}>{title}</Typography>
          </div>
        </>
      );
    } if (height === "1" && width === "2") {
      return (
        <>
          <div className={classes.closeIconContainer}>
            {getOpenIconButton()}
          </div>

          <Grid container className={classes.bigBlockContainer} justify="space-between">
            <Grid item xs={7}>
              <Typography variant="h6" className={classes.title}>{title}</Typography>
              {!!description && (
                <div className={classes.description}>
                  <Typography variant="body0">{description}</Typography>
                </div>
              )}

              {!!link && (
                <div className={classes.link}>
                  <Typography variant="body0" weight="bold">{link}</Typography>
                </div>
              )}
            </Grid>

            {!!image && (
              <img src={image} className={classnames(classes.smallImage)} alt="block illustration" />
            )}
          </Grid>
        </>
      );
    } if (height === "1" && width > "2") {
      return (
        <>
          <Grid container justify="space-between" className={classes.h100}>
            <Grid item xs={7} className={classes.bigBlockContainer}>
              <Typography variant="h6" className={classes.title}>{title}</Typography>

              {!!description && (
                <div className={classes.description}>
                  <Typography variant="body0">{description}</Typography>
                </div>
              )}

              {!!link && (
                <div className={classes.link}>
                  <Typography variant="body0" weight="bold">{link}</Typography>
                </div>
              )}
            </Grid>

            {!!image && (
              <Grid item xs className={classes.centerImage}>
                <img src={image} className={classes.fullHeightImage} alt="block illustration" />
              </Grid>
            )}

            {handleOpen && (
              <Grid item>
                {getOpenIconButton()}
              </Grid>
            )}
          </Grid>
        </>
      );
    }
    return (
      <>
        <div className={classes.closeIconContainer}>
          {getOpenIconButton()}
        </div>

        <div className={classes.bigBlockContainer}>
          <Typography variant="h6" className={classes.title}>{title}</Typography>

          {!!description && (
            <div className={classes.description}>
              <Typography variant="body0">{description}</Typography>
            </div>
          )}

          {!!link && (
            <div className={classes.link}>
              <Typography variant="body0" weight="bold">{link}</Typography>
            </div>
          )}

          {!!image && (
            <Image size="auto" src={image} className={classes.image} alt="block illustration" />
          )}
        </div>
      </>
    );
  };

  const getMetricText = () => (typeof value === "object" ? value : { value, currency: "" });

  const getMetricBlock = () => (
    <>
      <div className={classes.closeIconContainer}>
        {getOpenIconButton()}
      </div>

      <div className={classes.metricIconContainer}>
        {icon}
      </div>

      <div className={classes.marginAuto}>
        <Typography
          variant="body0"
          className={classes.description}
        >
          {description}
        </Typography>

        <div className={classnames.description}>
          <Typography
            variant="h2"
            color={color}
            shade={active ? 100 : 500}
            className={classes.value}
            weight="bold"
          >
            {getMetricText().value}
          </Typography>

          {getMetricText().currency !== "" && (
            <Typography
              variant="f2-12"
              color={color}
              shade={active ? 100 : 500}
              className={classes.currency}
              upperCase
            >
              {getMetricText().currency}
            </Typography>
          )}
        </div>
      </div>
    </>
  );

  const getCustomBlock = () => (
    <>
      <div className={classes.closeIconContainer}>
        {getOpenIconButton()}
      </div>

      {children}
    </>
  );

  const getIconBlock = () => (
    <>
      <div className={classes.closeIconContainer}>
        {applied ? getCheckIconButton() : getOpenIconButton()}
      </div>

      <div className={classes.iconContainer}>
        {icon}
      </div>

      <div className={classes.marginAuto}>
        <Typography
          variant="body0"
          className={classes.description}
          gutterBottom
        >
          {description}
        </Typography>
      </div>
    </>
  );

  const getDashedBlock = () => (
    <>
      <div className={classes.closeIconContainer}>
        {getOpenIconButton()}
      </div>

      {icon}

      <div className={classes.marginAuto}>
        <Typography
          variant="body0"
          shade={active ? 900 : 500}
          className={classes.description}
          color={color}
        >
          {description}
        </Typography>
      </div>
    </>
  );

  const getAction = () => {
    if (onClick) return onClick;
    if (handleOpen) return handleOpen;
    return undefined;
  };

  return (
    <div
      className={classnames(getStyle())}
      onClick={getAction()}
      onKeyDown={onKeyDown(getAction())}
      role="button"
      tabIndex={0}
    >
      {variant === "info" && getInfoBlock()}

      {variant === "metric" && getMetricBlock()}

      {variant === "icon" && getIconBlock()}

      {variant === "custom" && getCustomBlock()}

      {variant === "dashed" && getDashedBlock()}
    </div>
  );
};

Block.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  link: PropTypes.string,
  size: PropTypes.oneOf(["1x1", "1x2", "2x2", "1x4"]),
  active: PropTypes.bool,
  selected: PropTypes.bool,
  applied: PropTypes.bool,
  inactive: PropTypes.bool,
  handleOpen: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  onClick: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  titleLines: PropTypes.number,
  descriptionLines: PropTypes.number,
  variant: PropTypes.oneOf(["info", "metric", "icon", "dashed", "custom"]),
  icon: PropTypes.instanceOf(Object),
  color: PropTypes.oneOf(colorsList),
  imageSize: PropTypes.oneOf(["sm", "md", "lg", "xl"]),
};

Block.defaultProps = {
  className: "",
  title: "",
  description: "",
  link: "",
  size: "1x1",
  active: false,
  selected: false,
  applied: false,
  inactive: false,
  handleOpen: undefined,
  onClick: undefined,
  titleLines: 3,
  descriptionLines: 4,
  variant: "info",
  icon: <At color="primary" />,
  color: "primary",
  imageSize: "xl",
};

export default Block;
