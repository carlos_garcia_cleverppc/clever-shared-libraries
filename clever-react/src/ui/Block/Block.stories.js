/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import { getColorsLabels } from "../styles/colors";

import At from "../Icon/At";

import Container from "../Container";
import Item from "../Container/Item";
import Block from "./index";
import Typography from "../Typography";

export default {
  title: "Block",
  component: Block,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
  decorators: [Story => <div style={{ backgroundColor: "#f5f7f9", padding: "1rem" }}><Story /></div>],
};

const Template = args => {
  if (args.variant === "metric" || args.variant === "icon" || args.variant === "dashed") {
    return (
      <>
        <Typography variant="body1" weight="bold" shade={900}> 1x1</Typography>

        <Block {...args} size="1x1" icon={<At color={args.color} />} />
      </>
    );
  }
  return (
    <Container rows={2} columns={4} justify="center" layout="blocks" maxWidth="blocks" columnSize="blocks">
      {args.variant !== "metric" && (
        <Item columns={2} rows={2}>
          <Typography variant="body1" weight="bold" shade={900}>2x2</Typography>

          <Block {...args} size="2x2" />
        </Item>
      )}

      <Item columns={1} rows={1}>
        <Typography variant="body1" weight="bold" shade={900}>1x1</Typography>

        <Block {...args} size="1x1" />
      </Item>

      {args.variant !== "metric" && (
        <Item columns={2} rows={1}>
          <Typography variant="body1" weight="bold" shade={900}>1x2</Typography>

          <Block {...args} size="1x2" />
        </Item>
      )}

      {args.variant !== "metric" && (
        <Item columns={4} rows={1}>
          <Typography variant="body1" weight="bold" shade={900}>1x4</Typography>

          <Block {...args} size="1x4" />
        </Item>
      )}
    </Container>
  );
};

export const InfoBlock = Template.bind({});
InfoBlock.args = {
  title: "Title of the block",
  description: "Description of the block",
  color: "primary",
  active: false,
  selected: false,
  titleLines: 3,
  descriptionLines: 4,
  image: "/assets/img/block-example.svg",
};
export const MetricBlock = Template.bind({});
MetricBlock.args = {
  value: {
    value: "125",
    currency: "EUR",
  },
  color: "primary",
  active: false,
  selected: false,
  variant: "metric",
};
export const IconBlock = Template.bind({});
IconBlock.args = {
  description: "conversions",
  value: "141",
  color: "primary",
  active: false,
  selected: false,
  applied: false,
  inactive: false,
  variant: "icon",

};
export const DashedBlock = Template.bind({});
DashedBlock.args = {
  description: "Optimiza tus pujas por franja horaria",
  color: "primary",
  active: false,
  variant: "dashed",
};
export const CustomBlock = Template.bind({});
CustomBlock.args = {
  children: <Typography variant="body1">This is a custom block</Typography>,
  color: "primary",
  active: false,
  selected: false,
  variant: "custom",
};
