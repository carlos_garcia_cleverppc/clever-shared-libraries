import makeStyles from "../styles/makeStyles";

function getNormalBgColor(colors, color, variant) {
  if (variant === "metric") return "white";
  const shade = color === "neutral" ? 300 : 100;
  return colors[color][shade];
}

function getTextColor(colors, color) {
  const shade = color === "neutral" ? 900 : 500;
  return colors[color][shade];
}

const styles = ({ cleverUI: { colors, spacing, shadows, typography } }) => {
  const { sizing } = typography;

  return ({
    block: ({ width, height }) => ({
      ...spacing["p-4"],
      borderRadius: "8px",
      width: `${160 * width + (width - 1) * 24}px`,
      height: `${160 * height + (height - 1) * 24}px`,
      boxSizing: "border-box",
      display: "flex",
      flexDirection: "column",
      "&:focus": { outline: "none" },
      "& a": { fontSize: sizing.body0 },
    }),
    hoverable: {
      cursor: "pointer",
      "&:hover": { boxShadow: shadows.hover, transition: "all .3s ease-in-out" },
      "&:focus": { boxShadow: shadows.hover, transition: "all .3s ease-in-out" },
    },
    normal: ({ color, variant }) => ({
      backgroundColor: getNormalBgColor(colors, color, variant),
      "& h6": { color: getTextColor(colors, color) },
      "& p": { color: getTextColor(colors, color) },
      "& a": { color: getTextColor(colors, color) },
    }),
    selected: ({ color }) => ({
      border: `3px solid ${getTextColor(colors, color)}`,
      "& h6": { color: getTextColor(colors, color) },
      "& p": { color: getTextColor(colors, color) },
      "& a": { color: getTextColor(colors, color) },
    }),
    active: ({ color }) => ({
      backgroundColor: color === "yellow" || color === "neutral" ? colors[color][900] : colors[color][500],
      "& h6": { color: colors[color][100] },
      "& p": { color: colors[color][100] },
      "& a": { color: colors[color][100] },
    }),
    drawerRoot: { maxWidth: "400px" },
    drawerHeader: ({ color }) => ({
      ...spacing["p-5"],
      minWidth: 250,
      backgroundColor: colors[color][700],
      display: "flex",
      flexDirection: "column",
      "& h6": { color: "white !important" },
      "& p": { color: "white !important" },
    }),
    drawerContent: { ...spacing["p-5"] },
    iconButton: { padding: 0 },
    image: ({ imageWidth }) => ({
      ...spacing["mx-auto"],
      ...spacing[`w-${imageWidth}`],
      ...spacing[`h-${imageWidth}`],
      ...spacing[`mt-${imageWidth === "25" ? "8" : "3"}`],
      maxHeight: "147px",
    }),
    smallImage: { ...spacing["mx-auto"], ...spacing["mt-3"], maxHeight: "115px", maxWidth: "110px" },
    icon: ({ color }) => ({
      width: "32px !important",
      height: "32px !important",
      "& path": { fill: colors[color][900] },
    }),
    iconActive: ({ color }) => ({ "& path": { fill: colors[color][100] } }),
    closeIconContainer: { height: 0, textAlign: "right" },
    marginNegative: { ...spacing["mnt-3"] },
    marginAuto: { ...spacing["mt-auto"] },
    bigBlockContainer: { ...spacing["my-auto"] },
    description: ({ descriptionLines, link }) => ({
      ...spacing[`mb-${link ? "2" : "0"}`],
      ...spacing["pr-1"],
      overflow: "hidden",
      textOverflow: "ellipsis",
      display: "-webkit-box",
      "-webkit-line-clamp": descriptionLines,
      "-webkit-box-orient": "vertical",
    }),
    title: ({ titleLines }) => ({
      ...spacing["mb-2"],
      overflow: "hidden",
      textOverflow: "ellipsis",
      display: "-webkit-box",
      "-webkit-line-clamp": titleLines,
      "-webkit-box-orient": "vertical",
    }),
    negativeMargin: { ...spacing["mnt-3"] },
    h100: { ...spacing["h-100"] },
    fullHeightImage: { ...spacing["my-auto"], maxHeight: "120px", maxWidth: "270px" },
    centerImage: { display: "flex", justifyContent: "center" },

    // Metric block
    metricIconContainer: ({ color }) => ({
      ...spacing["mt-3"],
      ...spacing["mb-4"],
      backgroundColor: colors[color][100],
      borderRadius: "8px",
      width: "50px",
      height: "50px",
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      "& svg": { fill: `${colors[color][500]} !important` },
    }),
    metricIcon: ({ color }) => ({ "& path": { fill: colors[color][500] } }),
    iconCheck: ({ active, color }) => ({
      "& path": { fill: `${active ? colors[color][100] : colors.secondary[700]} !important` },
    }),
    value: {
      ...spacing["pr-1"],
      display: "inline-block",
    },
    currency: { display: "inline-block" },

    // Icon block
    iconContainer: ({ color, active }) => {
      let iconColor = colors[color][500];

      if (color === "neutral" && !active) iconColor = colors[color][900];
      else if (active) iconColor = colors[color][100];

      return {
        "& svg": {
          height: "26px",
          width: "26px",
          "& path": { fill: `${iconColor} !important` },
        },
      };
    },
    applied: ({ color }) => ({
      border: `3px solid ${color === "neutral" ? colors[color][900] : colors[color][500]}`,
      "& h6": { color: getTextColor(colors, color) },
      "& p": { color: getTextColor(colors, color) },
      "& a": { color: getTextColor(colors, color) },
    }),
    inactive: ({ color }) => ({
      border: `3px solid ${colors[color][300]}`,
      "& h6": { color: colors[color][500] },
      "& p": { color: colors[color][500] },
      "& a": { color: colors[color][500] },
    }),

    // dashed block
    dashedBlock: ({ color, active }) => ({
      border: `2px dashed ${active ? colors[color][900] : colors[color][500]}`,
      "& svg": { "& path": { fill: active ? colors[color][900] : colors[color][500] } },
    }),
  });
};

export default makeStyles(styles);
