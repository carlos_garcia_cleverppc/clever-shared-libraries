import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getColorsLabels } from "../styles/colors";

import Typography from "../Typography";
import Grid from "../Grid";
import Loading from "../Loading";
import IconButton from "../IconButton";
import Icon from "../Icon";

import getClasses from "./styles";

const colorsList = getColorsLabels(true);

const Chip = props => {
  const { className, label, color, onDelete, loading } = props;
  const { variant, size, icon } = props;

  const classes = getClasses({ color });

  const getIcon = () => {
    if (loading) {
      return (
        <Loading className={classes.loading} />
      );
    }
    if (onDelete) {
      return (
        <IconButton
          onClick={onDelete}
          className={classes.deleteIcon}
          size="small"
          outlined={false}
          color={color}
          icon={<Icon icon={icon} size={18} />}
        />
      );
    }
    return "";
  };

  return (
    <Grid container alignItems="center" className={classnames(classes.root, classes[variant], classes[size], className)}>
      <Grid item xs className={classes.itemFlex}>
        <Typography color={color}>
          {label}
        </Typography>
      </Grid>

      <Grid item className={classes.itemFlex}>
        {getIcon()}
      </Grid>
    </Grid>
  );
};

Chip.propTypes = {
  variant: PropTypes.oneOf(["contained", "outlined"]),
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  color: PropTypes.oneOf(colorsList),
  size: PropTypes.oneOf(["extraSmall", "small", "medium"]),
  onDelete: PropTypes.func,
  loading: PropTypes.bool,
  icon: PropTypes.string,
};

Chip.defaultProps = {
  className: "",
  variant: "contained",
  color: "primary",
  size: "medium",
  onDelete: undefined,
  loading: false,
  icon: "Times",
};

export default Chip;
