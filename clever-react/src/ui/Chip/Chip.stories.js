/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import { getColorsLabels } from "../styles/colors";

import Chip from "./index";

export default {
  title: "Chip",
  component: Chip,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => <Chip {...args} />;

export const SimpleChip = Template.bind({});
SimpleChip.args = { label: "Simple chip" };

export const LoadingChip = Template.bind({});
LoadingChip.args = {
  label: "Loading chip",
  loading: true,
};

export const OutlinedSmallChip = Template.bind({});
OutlinedSmallChip.args = {
  label: "Outlined small chip",
  variant: "outlined",
  color: "secondary",
  size: "small",
};
