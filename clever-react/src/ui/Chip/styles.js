import makeStyles from "../styles/makeStyles";

/*
function getBackgroundColor(colors, typeColor) {
  switch (typeColor) {
    case "primary":
      return colors.primary[100];
    case "secondary":
      return colors.secondary[100];
    case "warning":
      return colors.warning[100];
    case "danger":
      return colors.danger[100];
    default:
      return colors.primary[100];
  }
}

function getColor(colors, typeColor) {
  switch (typeColor) {
    case "primary":
      return colors.primary[700];
    case "secondary":
      return colors.secondary[700];
    case "warning":
      return colors.warning[700];
    case "danger":
      return colors.danger[700];
    default:
      return colors[500];
  }
}
*/

const styles = ({ cleverUI: { colors, spacing, typography } }) => {
  const { sizing } = typography;

  return ({
    root: ({ color }) => ({
      borderRadius: "16px",
      backgroundColor: colors[color][100],
      color: colors[color][500],
      height: "auto",
      width: "fit-content",
      "& p": { wordBreak: "break-word" },
    }),
    medium: {
      ...spacing["px-3"],
      "& p": { fontSize: sizing.body1 },
    },
    small: {
      ...spacing["px-2"],
      "& p": { fontSize: "12px" },
    },
    extraSmall: {
      ...spacing["px-2"],
      paddingTop: "1px !important",
      paddingBottom: "1px !important",
      "& p": { fontSize: "12px" },
    },
    contained: { ...spacing["py-2"] },
    outlined: ({ color }) => ({
      ...spacing["py-1"],
      border: `solid 1px ${colors[color][500]}`,
    }),
    deleteIcon: { ...spacing["ml-2"] },
    loading: {
      ...spacing["m-0"],
      ...spacing["ml-2"],
      marginBottom: "-2px",
      width: "15px !important",
      height: "fit-content !important",
      maxHeight: "15px",
    },
    itemFlex: { display: "flex" },
  });
};

export default makeStyles(styles);
