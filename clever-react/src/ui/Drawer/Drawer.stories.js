/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Drawer from "./index";
import Button from "../Button";

export default {
  title: "Drawer",
  component: Drawer,
};

const Template = args => {
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(!open);
  };

  return (
    <div>
      <Button color="primary" onClick={handleOpen}>
        Open drawer
      </Button>

      <Drawer {...args} open={open} onClose={handleOpen} />
    </div>
  );
};

export const SimpleDrawer = Template.bind({});
SimpleDrawer.args = { children: "Simple drawer" };
