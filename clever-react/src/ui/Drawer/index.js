import React from "react";
import PropTypes from "prop-types";

import MaterialDrawer from "@material-ui/core/Drawer";

const Drawer = props => {
  const { children, open, onClose } = props;
  const { anchor } = props;

  return (
    <MaterialDrawer anchor={anchor} open={open} onClose={onClose}>
      {children}
    </MaterialDrawer>
  );
};

Drawer.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
};

Drawer.defaultProps = {
  open: false,
  onClose: () => { },
};

export default Drawer;
