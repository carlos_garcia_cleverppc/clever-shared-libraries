import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import SliderMaterial from "@material-ui/core/Slider";

import getClasses from "./styles";
import { getColorsLabels } from "../styles/colors";

const colorsList = getColorsLabels(true);

const Slider = props => {
  const { color, value, onChange, disabled, max, min, step } = props;
  const { onDragEnd, className, valueLabelDisplay, valueLabelFormat, valueLabelClass } = props;
  const { thumbClass, trackClass } = props;

  const classes = getClasses({ color });

  return (
    <SliderMaterial
      value={value}
      onChange={onChange}
      disabled={disabled}
      max={max}
      min={min}
      step={step}
      onDragEnd={onDragEnd}
      className={className}
      valueLabelDisplay={valueLabelDisplay}
      classes={{
        rail: classes.rail,
        track: classnames(classes.track, trackClass),
        valueLabel: valueLabelClass
          ? classnames(classes.valueLabel, valueLabelClass)
          : classes.valueLabel,
        thumb: classnames(classes.thumb, thumbClass),
      }}
      valueLabelFormat={valueLabelFormat}
    />
  );
};

Slider.propTypes = {
  color: PropTypes.oneOf(colorsList),
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.array]).isRequired,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  max: PropTypes.number,
  min: PropTypes.number,
  onDragEnd: PropTypes.func,
  step: PropTypes.number,
  className: PropTypes.string,
  valueLabelClass: PropTypes.string,
  thumbClass: PropTypes.string,
  trackClass: PropTypes.string,
  valueLabelDisplay: PropTypes.oneOf(["on", "off", "auto"]),
  valueLabelFormat: PropTypes.func,
};

Slider.defaultProps = {
  color: "secondary",
  onChange: () => { },
  disabled: false,
  max: 100,
  min: 0,
  onDragEnd: () => { },
  step: 1,
  className: "",
  valueLabelClass: "",
  thumbClass: "",
  trackClass: "",
  valueLabelDisplay: "auto",
  valueLabelFormat: v => v,
};

export default Slider;
