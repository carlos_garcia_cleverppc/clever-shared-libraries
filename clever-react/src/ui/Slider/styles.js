import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, typography, shadows } }) => ({
  rail: {
    backgroundColor: colors.neutral[500],
    height: 3,
  },
  track: ({ color }) => ({ height: 3, color: colors[color][500] }),
  thumb: ({ color }) => ({
    height: 20,
    width: 20,
    marginTop: -8,
    marginLeft: -9,
    color: colors[color][500],
  }),
  valueLabel: {
    fontFamily: typography.font1,
    fontWeight: 700,
    fontSize: "14px",
    width: "42px",
    height: "42px",
    top: "-44px",
    borderRadius: "50%",
    boxShadow: shadows.normal,
    left: "calc(-51% + 0px)",
    "& > :first-child": {
      width: "42px",
      height: "42px",
    },
  },
});

export default makeStyles(styles);
