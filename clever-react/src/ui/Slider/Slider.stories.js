/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Slider from "./index";

export default {
  title: "Slider",
  component: Slider,
};

const Template = args => (
  <div style={{ marginTop: "100px" }}>
    <Slider {...args} />
  </div>
);

export const SimpleSlider = Template.bind({});
SimpleSlider.args = {};

export const CustomSlider = Template.bind({});
CustomSlider.args = { color: "primary", min: 0, max: 250, step: 10 };
