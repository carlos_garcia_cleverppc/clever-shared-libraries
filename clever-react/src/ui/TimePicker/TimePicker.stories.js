/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import TimePicker from "./index";

export default {
  title: "TimePicker",
  component: TimePicker,
};

const Template = args => {
  const [date, setDate] = React.useState(new Date());

  return (<TimePicker {...args} onChange={setDate} value={date} />);
};

export const SimpleTimePicker = Template.bind({});
SimpleTimePicker.args = {
  label: "TimePicker",
  disablePast: true,
};
