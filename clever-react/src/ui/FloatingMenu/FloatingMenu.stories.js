import React, { useState } from "react";
import isEmpty from "lodash-es/isEmpty";
import remove from "lodash-es/remove";

import makeStyles from "../styles/makeStyles";
import { getColorsLabels } from "../styles/colors";

import Grid from "../Grid";
import Typography from "../Typography";
import Divider from "../Divider";
import Checkbox from "../Checkbox";
import Block from "../Block";
import FloatingMenu from "./index";
import Trash from "../Icon/Trash";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({ element: { ...spacing["m-4"] }, root: {} }));

export default {
  title: "FloatingMenu",
  component: FloatingMenu,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = ({ color }) => {
  const classes = getClasses();

  const [anchorEl, setAnchorEl] = useState(null);
  const [list, setList] = useState({});
  const [selected, setSelected] = useState([]);

  const handleChange = name => event => {
    if (list[name]) { // item was selected
      const selectedAux = selected;

      remove(selected, s => name === s);

      setList({ ...list, [name]: false });
      setSelected(selectedAux);

      if (isEmpty(selectedAux)) setAnchorEl(null);
    } else { // new item
      const aux = selected;
      aux.push(name);

      setSelected(aux);
      setList({ ...list, [name]: true });

      if (!anchorEl) {
        const anchor = event.currentTarget.closest("div").childNodes[1];

        setAnchorEl(anchor);
      }
    }
  };

  return (
    <>
      <FloatingMenu
        anchorEl={anchorEl}
        color={color}
      >
        <Grid container justify="center" align="center" alignItems="center">
          <Grid item className={classes.element}>
            <Trash color="white" size={24} />

            <Typography color="white">Update</Typography>
          </Grid>

          <Grid item className={classes.element} key="divider">
            <Divider position="vertical" />
          </Grid>

          <Grid item className={classes.element}>
            <Trash color="white" size={24} />

            <Typography color="white">Delete</Typography>
          </Grid>
        </Grid>
      </FloatingMenu>

      <Checkbox
        checked={!!list.block1}
        onChange={handleChange("block1")}
        value="Single line"
      >
        <Block
          color="magenta"
          title="holi"
          description="adios"
          image="/assets/img/block-example.svg"
          size="1x2"
          selected
        />
      </Checkbox>

      <Checkbox
        checked={!!list.block2}
        onChange={handleChange("block2")}
        value="Single line"
      >
        <Block
          color="magenta"
          title="holi"
          description="adios"
          image="/assets/img/block-example.svg"
          size="1x2"
          selected
        />
      </Checkbox>
    </>
  );
};

export const SimpleFloatingMenu = Template.bind({});
