import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

import makeStyles from "../styles/makeStyles";
import { getColorsLabels } from "../styles/colors";

import Popper from "../Popper";

const colorsList = getColorsLabels(true);

const getClasses = makeStyles(() => ({
  paper: {
    position: "fixed !important",
    transform: "translateX(-120px) translateY(-64px)",
    minWidth: "250px",
  },
}));

const FloatingMenu = props => {
  const classes = getClasses();

  const { className, children, color, anchorEl } = props;

  return (
    <Popper
      placement="top"
      open={!!anchorEl}
      anchorEl={anchorEl}
      transition
      color={color}
      shade={500}
      className={classnames(classes.paper, className)}
    >
      {children}
    </Popper>
  );
};

FloatingMenu.propTypes = {
  className: PropTypes.string,
  color: PropTypes.oneOf(colorsList),
  anchorEl: PropTypes.instanceOf(Object),
};

FloatingMenu.defaultProps = {
  className: "",
  color: "neutral",
  anchorEl: null,
};

export default FloatingMenu;
