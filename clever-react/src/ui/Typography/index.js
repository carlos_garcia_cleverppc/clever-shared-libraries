/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import { getColorsLabels, getShades } from "../styles/colors";

import getClasses from "./styles";
import fontVariants from "./fontVariants";

const colorsList = getColorsLabels(true);
const shadesList = getShades();

const availableVariantTags = {
  h1: "h1",
  h2: "h2",
  h3: "h3",
  h4: "h4",
  h5: "h5",
  h6: "h6",
  span: "span",
};

const getTagFromVariant = variant => {
  const name = variant.toLocaleLowerCase();
  return availableVariantTags[name] || "p";
};

const Typography = React.forwardRef((props, ref) => {
  const {
    upperCase, downCase, capitalize, align, color,
    shade, variant, children, className, weight,
    gutterBottom, component, ...otherProps
  } = props;

  const classes = getClasses({ variant, align, color, shade });

  const utilities = {
    [classes.upperCase]: upperCase,
    [classes.downCase]: downCase,
    [classes.capitalize]: capitalize,
    [classes.mb]: gutterBottom,
    [classes.light]: weight === "thin",
    [classes.normal]: weight === "normal",
    [classes.bold]: weight === "bold",
  };

  const classNames = cx(
    className,
    classes.root,
    utilities,
  );

  const tagName = component || getTagFromVariant(variant);

  return React.createElement(tagName, {
    className: classNames,
    ref,
    ...otherProps,
  }, children);
});

Typography.propTypes = {
  variant: PropTypes.oneOf(fontVariants),
  className: PropTypes.string,
  gutterBottom: PropTypes.bool,
  weight: PropTypes.oneOf(["thin", "normal", "bold"]),
  color: PropTypes.oneOf(colorsList),
  shade: PropTypes.oneOf(shadesList),
  align: PropTypes.oneOf(["left", "right", "center", "justify"]),
  upperCase: PropTypes.bool,
  downCase: PropTypes.bool,
  capitalize: PropTypes.bool,
  component: PropTypes.node,
};

Typography.defaultProps = {
  variant: "body1",
  gutterBottom: false,
  color: "neutral",
  shade: 500,
  upperCase: false,
  downCase: false,
  capitalize: false,
};

export default Typography;
