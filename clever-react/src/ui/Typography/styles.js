import makeStyles from "../styles/makeStyles";

const isHeading = name => name.match(/h[1,2,3,4,5,6]/i);

const styles = ({ cleverUI: { colors, typography, spacing }, breakpoints }) => {
  const { sizing, font1, font2 } = typography;

  const getSpecialStyles = variant => {
    switch (variant) {
      case "h1":
        return {
          [breakpoints.down("sm")]: {
            fontSize: "28px",
          },
        };

      case "h3":
        return {
          textTransform: "uppercase",
        };

      default:
        return {};
    }
  };

  const getStylesFromSizing = variant => {
    const heading = isHeading(variant);

    const fontSize = sizing[variant];
    const fontFamily = heading ? font1 : font2;
    const specialStyles = getSpecialStyles(variant);

    return { fontSize, fontFamily, ...(heading && { fontWeight: "700" }), ...specialStyles };
  };

  const getStylesFromString = str => {
    const regexpStyles = /(?<family>f[1,2])-(?<size>\d+)/;
    const match = regexpStyles.exec(str);

    if (!match) {
      // eslint-disable-next-line no-console
      console.warn("[Typography]: invalid variant");
      return {};
    }

    const FONT_FAMILIES = {
      f1: font1,
      f2: font2,
    };

    const { family, size } = match.groups;

    return {
      fontFamily: FONT_FAMILIES[family],
      fontSize: `${size}px`,
    };
  };

  const getVariantStyles = variant => {
    const styleObject = sizing[variant]
      ? getStylesFromSizing(variant)
      : getStylesFromString(variant);

    return styleObject;
  };

  return {
    root: ({ color, shade, align, variant }) => ({
      margin: 0,
      color: colors[color] ? colors[color][shade] : color,
      whiteSpace: "pre-line",
      lineHeight: typography.lineHeight,
      textAlign: align,
      ...getVariantStyles(variant),
    }),
    mb: () => spacing["mb-3"],
    upperCase: { textTransform: "uppercase!important" },
    downCase: { textTransform: "lowercase!important" },
    capitalize: { textTransform: "capitalize!important" },
    light: { fontWeight: "300!important" },
    normal: { fontWeight: "400!important" },
    bold: { fontWeight: "700!important" },
  };
};

export default makeStyles(styles);
