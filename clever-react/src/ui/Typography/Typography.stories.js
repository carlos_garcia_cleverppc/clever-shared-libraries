/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import { getColorsLabels, getShades } from "../styles/colors";

import Typography from "./index";
import fontVariants from "./fontVariants";

export default {
  title: "Typography",
  component: Typography,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
    shade: {
      options: getShades(),
      control: {
        type: "select",
      },
    },
    variant: {
      options: fontVariants,
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => (
  <Typography {...args} />
);

export const Font1Typography = Template.bind({});
Font1Typography.args = {
  variant: "f1-14",
  children: "Montserrat size 14",
};

export const Font2Typography = Template.bind({});
Font2Typography.args = {
  variant: "f2-14",
  children: "Nunito size 14",
};
