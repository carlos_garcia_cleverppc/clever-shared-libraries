import React from "react";
import { screen, render } from "@testing-library/react";
import Typography from "./index";
import ThemeProvider from "../Provider";

const sizing = {
  h1: 30,
  h2: 25,
  h3: 22,
  h4: 20,
  h5: 16,
  h6: 14,
  body1: 20,
  span: 16,
};
const font1 = "Heading-font, sans-serif";
const font2 = "Body-font, sans-serif";
const theme = {
  cleverUI: {
    typography: {
      font1,
      font2,
      sizing,
    },
  },
};
const renderWithProviders = ui => render(<ThemeProvider theme={theme}>{ui}</ThemeProvider>);

describe("Typography", () => {
  test("Capitalize", () => {
    renderWithProviders(<Typography capitalize>Hello World</Typography>);

    const element = screen.getByText("Hello World");

    expect(element).toHaveStyle("text-transform: capitalize");
  });

  test("downCase", () => {
    renderWithProviders(<Typography downCase>Hello World</Typography>);

    const element = screen.getByText("Hello World");

    expect(element).toHaveStyle("text-transform: lowercase");
  });

  test("upperCase", () => {
    renderWithProviders(<Typography upperCase>Hello World</Typography>);

    const element = screen.getByText("Hello World");
    expect(element).toHaveStyle("text-transform: uppercase");
  });

  test("Heading", () => {
    renderWithProviders(<Typography variant="h2">Hello World</Typography>);

    const element = screen.getByRole("heading", { level: 2, name: "Hello World" });

    expect(element).toHaveStyle("font-weight: 700");
    expect(element).toHaveStyle(`font-size: ${sizing.h2}px`);
    expect(element).toHaveStyle(`font-family: ${font1}`);
  });

  test("body1", () => {
    renderWithProviders(<Typography variant="body1">Hello World</Typography>);

    const element = screen.getByText("Hello World");

    expect(element).toHaveStyle(`font-size: ${sizing.body1}px`);
    expect(element).toHaveStyle(`font-family: ${font2}`);
  });

  test("weight", () => {
    renderWithProviders(<Typography variant="body1" weight="bold">Hello World</Typography>);

    const element = screen.getByText("Hello World");

    expect(element).toHaveStyle("font-weight: 700");
  });

  test("special variant", () => {
    renderWithProviders(<Typography variant="f2-24">Hello World</Typography>);

    const element = screen.getByText("Hello World");

    expect(element).toHaveStyle("font-size: 24px");
    expect(element).toHaveStyle(`font-family: ${font2}`);
  });

  test("component", () => {
    renderWithProviders(<Typography variant="f2-24" component="button">Hello World</Typography>);

    const element = screen.getByRole("button", { name: "Hello World" });

    expect(element).toHaveStyle("font-size: 24px");
    expect(element).toHaveStyle(`font-family: ${font2}`);
  });

  test("html Props", () => {
    renderWithProviders(<Typography variant="f1-20" style={{ textDecoration: "underline" }}>Hello World</Typography>);

    const element = screen.getByText("Hello World");

    expect(element).toHaveStyle("font-size: 20px");
    expect(element).toHaveStyle(`font-family: ${font1}`);
    expect(element).toHaveStyle("text-decoration: underline");
  });
});
