import React from "react";

import { getColorsLabels } from "../styles/colors";

import Button from "../Button";

import Snackbar from "./index";

export default {
  title: "Snackbar",
  component: Snackbar,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => {
  const [open, setOpen] = React.useState(false);

  return (
    <div>
      <Button onClick={() => setOpen(true)}>Open snackbar</Button>

      <Snackbar {...args} open={open} onClose={() => setOpen(false)} />
    </div>
  );
};

const TemplateWithAnchorEl = args => {
  const [open, setOpen] = React.useState(false);
  const buttonRef = React.useRef(null);

  return (
    <div style={{ minHeight: "60vh", display: "flex" }}>
      <Button onClick={() => setOpen(true)} ref={buttonRef} style={{ margin: "auto" }}>Open snackbar</Button>

      <Snackbar {...args} open={open} onClose={() => setOpen(false)} anchorEl={buttonRef.current} />
    </div>
  );
};

export const SuccessSnackbar = Template.bind({});
SuccessSnackbar.args = {
  text: "We recommend you review your keywords weekly",
  textAction: "Set reminder",
  color: "success",
  anchorOrigin: {
    vertical: "bottom",
    horizontal: "left",
  },
};

export const ErrorSnackbar = Template.bind({});
ErrorSnackbar.args = {
  text: "We recommend you review your keywords weekly",
  textAction: "Ok",
  color: "danger",
  anchorOrigin: {
    vertical: "bottom",
    horizontal: "left",
  },
};

export const DefaultSnackbar = Template.bind({});
DefaultSnackbar.args = {
  text: "Default snackbar",
  anchorOrigin: {
    vertical: "bottom",
    horizontal: "left",
  },
};

export const SnackbarWithAnchorEl = TemplateWithAnchorEl.bind({});
SnackbarWithAnchorEl.args = {
  text: "We recommend you review your keywords weekly",
  textAction: "Set reminder",
  color: "success",
  placement: "bottom-start",
};
