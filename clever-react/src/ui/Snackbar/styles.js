/* eslint-disable object-curly-newline */
import makeStyles from "../styles/makeStyles";
import getContrastText from "../styles/contrastText";

const styles = ({ cleverUI: { colors, spacing, typography, shadows }, breakpoints }) => {
  const { sizing } = typography;

  return ({
    root: {
      [breakpoints.up("sm")]: {
        maxWidth: 360,
        width: "100%",
      },
    },
    content: ({ color }) => ({
      flexWrap: "nowrap",
      border: `1px solid ${colors[color]?.[500] || color}`,
      color: colors[color]?.[500] || color,
      backgroundColor: colors[color]?.[100] || getContrastText(color),
      borderRadius: "8px",
      fontFamily: typography.font2,
      lineHeight: typography.lineHeight,
      fontWeight: "bold",
      flexGrow: 1,
      boxShadow: shadows.normal,
      "& > :last-child": {
        marginRight: 0, // remove negative margin of Material ui
      },
    }),
    text: {
      fontSize: sizing.body0,
      display: "-webkit-box",
      "-webkit-box-orient": "vertical",
      "-webkit-line-clamp": 2,
      overflow: "hidden",
    },
    action: {
      whiteSpace: "nowrap",
      textTransform: "uppercase",
      fontSize: sizing.body0,
      "& svg": {
        display: "block",
      },
    },
    loading: { ...spacing["m-0"] },
  });
};

export default makeStyles(styles);
