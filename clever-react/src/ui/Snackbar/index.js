/* eslint-disable no-console */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import SnackbarMaterial from "@material-ui/core/Snackbar";
import Fade from "@material-ui/core/Fade";
import SnackbarContent from "@material-ui/core/SnackbarContent";

import { getColorsLabels } from "../styles/colors";

import Button from "../Link";
import Times from "../Icon/Times";
import Loading from "../Loading";

import getClasses from "./styles";
import TooltipBar from "./TooltipBar";

const colorsList = getColorsLabels();

const Snackbar = props => {
  const {
    className, onClose, text, textAction, textColor, onAction, placement,
    color, anchorEl, showActionButton, isLoading, ...otherProps
  } = props;

  const classes = getClasses({ color });

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    onClose();
  };

  const getAction = () => {
    if (showActionButton && !isLoading) {
      return (
        <Button className={classes.action} color={color} onClick={onAction || handleClose}>
          {textAction || <Times size={18} color={color} />}
        </Button>
      );
    } if (isLoading) {
      return (
        <Loading size={16} color={color} className={classes.loading} />
      );
    } return <></>;
  };

  if (anchorEl) {
    return (
      <TooltipBar
        anchorEl={anchorEl}
        className={classnames(className, classes.content)}
        placement={placement}
        onClose={handleClose}
        TransitionComponent={Fade}
        message={<div className={classes.text}>{text}</div>}
        action={getAction()}
        {...otherProps}
      />
    );
  }

  return (
    <SnackbarMaterial
      className={classnames(className, classes.root)}
      onClose={handleClose}
      TransitionComponent={Fade}
      {...otherProps}
    >
      <SnackbarContent
        className={classes.content}
        message={<div className={classes.text}>{text}</div>}
        action={getAction()}

      />
    </SnackbarMaterial>
  );
};

Snackbar.propTypes = {
  text: PropTypes.string.isRequired,
  open: PropTypes.bool,
  /**
  Executes when the visibility time has expired.
  */
  onClose: PropTypes.func,
  /**
  Executes when interacting with the action text.
  */
  onAction: PropTypes.func,
  showActionButton: PropTypes.bool,
  color: PropTypes.oneOf(colorsList),
  anchorOrigin: PropTypes.exact({
    vertical: PropTypes.oneOf(["top", "bottom"]),
    horizontal: PropTypes.oneOf(["left", "right", "center"]),
  }),
  autoHideDuration: PropTypes.number,
  /**
  Shows Loading component instead of action button.
  */
  isLoading: PropTypes.bool,
  textAction: PropTypes.string,
  /**
  Allows the snackbar relative to a DOM element.
  */
  anchorEl: PropTypes.instanceOf(Element),
  /**
  Position relative to the anchorEl.
  */
  placement: PropTypes.oneOf([
    "top-start", "top", "top-end",
    "left-start", "left", "left-end",
    "right-start", "right", "right-end",
    "bottom-start", "bottom", "bottom-end",
  ]),
};

Snackbar.defaultProps = {
  open: false,
  color: "info",
  onClose: () => { },
  showActionButton: true,
  isLoading: false,
  anchorOrigin: {
    vertical: "bottom",
    horizontal: "left",
  },
  autoHideDuration: 3000,
  placement: "bottom-start",
};

export default Snackbar;
