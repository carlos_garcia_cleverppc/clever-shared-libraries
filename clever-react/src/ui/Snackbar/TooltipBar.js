/* eslint-disable react/require-default-props */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import Fade from "@material-ui/core/Fade";
import Popper from "@material-ui/core/Popper";

import makeStyles from "../styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  root: {
    width: "100%",
    maxWidth: "344px",
  },
  grid: {
    display: "grid",
    padding: "6px 16px",
    columnGap: "16px",
    alignItems: "center",
    gridTemplateColumns: "1fr auto",
    boxXizing: "border-box",
  },
  content: spacing["py-2"],
  action: {
    display: "flex",
    placeItems: "center",
  },
}));

const TooltipBar = props => {
  const {
    open, className, onClose,
    autoHideDuration, message, action, placement, anchorEl, TransitionComponent,
  } = props;

  const classes = getClasses();

  useEffect(() => {
    let mounted = true;
    let timer;

    if (open) {
      timer = setTimeout(() => {
        if (mounted) {
          onClose();
        }
      }, autoHideDuration);
    } else {
      clearTimeout(timer);
    }

    return () => {
      clearTimeout(timer);
      mounted = false;
    };
  }, [open, autoHideDuration, onClose]);

  return (
    <Popper
      open={open}
      anchorEl={anchorEl}
      className={classes.root}
      placement={placement}
      modifiers={{ offset: { offset: "0,16" } }}
      transition
    >
      {({ TransitionProps }) => (

        <TransitionComponent {...TransitionProps} timeout={300}>
          <div className={classnames(className, classes.grid)}>
            <div className={classes.content}>{message}</div>

            <div className={classes.content}>{action}</div>
          </div>
        </TransitionComponent>

      )}
    </Popper>
  );
};

TooltipBar.propTypes = {
  action: PropTypes.func.isRequired,
  message: PropTypes.oneOf([PropTypes.string, PropTypes.node]).isRequired,
  anchorEl: PropTypes.instanceOf(Element).isRequired,
  placement: PropTypes.oneOf([
    "top-start", "top", "top-end",
    "left-start", "left", "left-end",
    "right-start", "right", "right-end",
    "bottom-start", "botton", "bottom-end",
  ]),
  onClose: PropTypes.func,
  TransitionComponent: PropTypes.func,
  autoHideDuration: PropTypes.number,
  open: PropTypes.bool,
};

TooltipBar.defaultProps = {
  onClose: () => { },
  autoHideDuration: 3000,
  placement: "bottom-start",
  TransitionComponent: Fade,
  open: false,
};

export default TooltipBar;
