import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  containedCard: ({ variant, color }) => ({
    ...spacing["px-1"],
    ...spacing["pt-1"],
    position: "relative",
    display: "flex",
    flexDirection: "column",
    border: "1px solid",
    borderColor: colors.neutral[300],
    borderRadius: "8px",
    minWidth: 0,
    backgroundClip: "border-box",
    backgroundColor: (!!colors[color] && variant === "contained") ? colors[color][500] : "white",
    color: (!!colors[color] && variant === "contained") ? colors[color].contrastText : colors.neutral[900],
    "& .card__header": { order: 0, display: "flex" },
    "& .card__body": { order: 1, display: "flex" },
  }),

  containerCardClickable: { "&:hover": { boxShadow: "1px 4px 16px 0px rgba(0,0,0,0.24)" } },

  stripedCard: ({ variant, color }) => {
    const strokeWidth = "5"; /* width's dashes */
    const dashArray = "10 20"; /* space between dashes should be > strokeWidth */
    const lineCap = "square"; /* square, round, butt; type of border of dashes */
    const dashOffset = "5"; /* move clockside dashes */
    const borderRadius = "16";
    const strokeColor = colors.neutral[300]; // codigo hexadecimal

    return {
      backgroundImage: `url("data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' rx='${borderRadius}' ry='${borderRadius}' stroke='%23${strokeColor.replace("#", "")}FF' stroke-width='${strokeWidth}' stroke-dasharray='${dashArray}' stroke-dashoffset='${dashOffset}' stroke-linecap='${lineCap}'/%3e%3c/svg%3e")`,
      borderRadius: `${borderRadius}px`,

      ...spacing["mb-3"],
      ...spacing["px-1"],
      ...spacing["pt-1"],
      position: "relative",
      display: "flex",
      color: (!!colors[color] && variant === "contained") ? colors[color].contrastText : colors.neutral[900],
    };
  },
});

export default makeStyles(styles);
