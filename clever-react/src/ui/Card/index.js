/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import getClasses from "./styles";

const Card = props => {
  const { children, className, onClick } = props;
  const { variant, color, clickable } = props;

  const classes = getClasses({ variant, color });

  const getCardClassNames = () => {
    const res = [className];

    if (variant === "striped") {
      res.push(classes.stripedCard);
    } else {
      res.push(classes.containedCard);
      if (clickable) {
        res.push(classes.containerCardClickable);
      }
    }

    return res;
  };

  return (
    <div
      className={classnames(getCardClassNames())}
      onClick={onClick}
      role={onClick ? "button" : "banner"}
      onKeyUp={() => { }}
    >
      {children}
    </div>
  );
};

Card.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  color: PropTypes.oneOf(["", "primary", "secondary", "warning", "neutral", "default", "info", "success"]),
  variant: PropTypes.oneOf(["contained", "striped"]),
  clickable: PropTypes.bool,
};

Card.defaultProps = {
  className: "",
  onClick: () => { },
  color: "",
  variant: "contained",
  clickable: false,
};

export default Card;
