/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Card from "./index";

export default {
  title: "Card",
  component: Card,
};

const Template = args => <Card {...args} />;

export const SimpleCard = Template.bind({});
SimpleCard.args = { children: "This is a default card" };

export const ClickableCard = Template.bind({});
ClickableCard.args = {
  children: "This is a clickable card",
  clickable: "true",
  color: "primary",
};

export const StripedCard = Template.bind({});
StripedCard.args = {
  children: "This is a striped card",
  variant: "striped",
};
