import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import MaterialButton from "@material-ui/core/Button";

import { getColorsLabels } from "../styles/colors";
import useMediaQuery from "../styles/useMediaQuery";
import { screenSm, screenXs } from "../styles/breakpoints";

import ArrowRight from "../Icon/ArrowRight";
import Grid from "../Grid";
import getClasses from "./styles";

const colorsList = getColorsLabels();

const RoundedButton = props => {
  const { children, className, size, onClick } = props;
  const { href, disabled, target, ripple, disabledClickable } = props;
  const { height, hover } = props;

  let { color } = props;
  const white = color === "white";

  if (white) color = "primary";

  const classes = getClasses({ color });

  const isScreenSm = useMediaQuery(screenSm);
  const isScreenXs = useMediaQuery(screenXs);

  const getStyles = () => {
    const classNames = [];

    if (height === "narrow") {
      classNames.push(classes.narrow);
    } else {
      classNames.push(classes.normal);
    }

    if (size === "large") {
      classNames.push(classes.baseStyles);
      classNames.push(classes.large);
      classNames.push(classes.contained);

      if (disabled || disabledClickable) classNames.push(classes.containedDisabled);
      if (isScreenSm || isScreenXs) classNames.push(classes.mobileBaseStyles);
    } else {
      classNames.push(classes.baseStyles);
      classNames.push(classes.regular);
      classNames.push(classes.contained);

      if (disabled || disabledClickable) classNames.push(classes.containedDisabled);
      if (isScreenSm || isScreenXs) classNames.push(classes.mobileBaseStyles);
    }

    if (hover) classNames.push(classes.hover);

    if (white) classNames.push(classes.white);
    else classNames.push(classes.color);

    classNames.push(className);
    return classnames(classNames);
  };

  return (
    <MaterialButton
      className={getStyles()}
      size={size}
      onClick={onClick}
      href={href}
      disabled={disabled}
      target={target}
      disableRipple={!ripple}
    >
      {size === "medium" && (
        <div className={classes.text}>{children}</div>
      )}

      {size === "large" && (
        <Grid container alignItems="center">
          <Grid item xs className={classes.text}>{children}</Grid>

          <Grid item className={classes.iconContainer}>
            <ArrowRight
              color={disabled || disabledClickable ? color : "white"}
              shade={disabled || disabledClickable ? 300 : 500}
              size={10}
            />
          </Grid>
        </Grid>
      )}
    </MaterialButton>
  );
};

RoundedButton.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(["medium", "large"]),
  href: PropTypes.string,
  disabled: PropTypes.bool,
  disabledClickable: PropTypes.bool,
  target: PropTypes.string,
  onClick: PropTypes.func,
  hover: PropTypes.bool,
  ripple: PropTypes.bool,
  color: PropTypes.oneOf(colorsList),
};

RoundedButton.defaultProps = {
  className: "",
  size: "medium",
  href: "",
  disabled: false,
  disabledClickable: false,
  target: "",
  onClick: () => { },
  hover: true,
  ripple: true,
  color: "primary",
};

export default RoundedButton;
