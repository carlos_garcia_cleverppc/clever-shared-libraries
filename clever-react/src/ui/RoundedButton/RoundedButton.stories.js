/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import { getColorsLabels } from "../styles/colors";

import RoundedButton from "./index";

export default {
  title: "RoundedButton",
  component: RoundedButton,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => <RoundedButton {...args} />;

export const SimpleRoundedButton = Template.bind({});
SimpleRoundedButton.args = { children: "Rounded button" };

export const NoHoverRoundedButton = Template.bind({});
NoHoverRoundedButton.args = { children: "No hover", color: "secondary", hover: false };

export const LargeNarrowRoundedButton = Template.bind({});
LargeNarrowRoundedButton.args = { children: "Large and narrow", size: "large", height: "narrow" };
