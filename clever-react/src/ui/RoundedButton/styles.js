import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, typography, spacing, shadows } }) => {
  const { sizing } = typography;

  return ({
    baseStyles: {
      height: "48px",
      fontSize: typography.fontSizeBase.constant + typography.fontSizeBase.unit,
      fontFamily: typography.font2,
      fontWeight: "900",
      borderRadius: "8px",
      boxShadow: "none",
      "& svg": {
        height: "14px",
        width: "14px",
      },
      "& p": { fontWeight: "900" },
    },
    mobileBaseStyles: { width: "100%" },
    contained: ({ color }) => ({
      backgroundColor: colors[color][500],
      color: colors.neutral[100],
    }),
    containedDisabled: ({ color }) => ({
      color: `${colors[color][300]} !important`,
      backgroundColor: colors[color][100],
    }),
    outlined: ({ color }) => ({
      color: colors[color][500],
      border: "solid",
    }),
    outlinedDisabled: ({ color }) => ({
      color: `${colors[color][300]} !important`,
      borderColor: colors[color][100],
    }),
    regular: { minWidth: "192px" },
    large: { minWidth: "256px" },
    iconContainer: { ...spacing["pr-3"], display: "flex" },
    normal: {
      height: "48px",
      borderRadius: "24px",
    },
    narrow: {
      height: "34px",
      borderRadius: "17px",
      fontFamily: typography.fontFamilyBody,
      fontSize: sizing.body0,
    },
    color: ({ color }) => ({
      backgroundColor: color,
      "&:hover": { backgroundColor: colors[color][500] },
      "& p": { color: `${colors.neutral[100]} !important` },
    }),
    hover: ({ color }) => ({
      "&:hover": {
        backgroundColor: colors[color][700],
        boxShadow: shadows.hover,
      },
    }),
    white: {
      color: colors.neutral[500],
      backgroundColor: "white !important",
      "&:hover": { backgroundColor: "white" },
    },
    text: { ...spacing["px-4"] },
  });
};

export default makeStyles(styles);
