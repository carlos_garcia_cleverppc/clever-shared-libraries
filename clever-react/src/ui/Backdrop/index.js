/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";

import MaterialBackdrop from "@material-ui/core/Backdrop";

const Backdrop = ({ children, className, open, onClick }) => (
  <MaterialBackdrop
    className={className}
    open={open}
    onClick={onClick}
  >
    {children}
  </MaterialBackdrop>
);

Backdrop.propTypes = {
  className: PropTypes.string,
  open: PropTypes.bool,
  onClick: PropTypes.func,
};

Backdrop.defaultProps = {
  className: "",
  open: false,
  onClick: () => { },
};

export default Backdrop;
