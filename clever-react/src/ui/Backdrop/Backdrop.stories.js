import React from "react";

import Backdrop from "./index";
import Button from "../Button";
import Loading from "../Loading";
import makeStyles from "../styles/makeStyles";

export default {
  title: "Backdrop",
  component: Backdrop,
};

const Template = () => {
  const getClasses = makeStyles(() => ({ backdrop: { color: "#fff", zIndex: 10 } }));
  const classes = getClasses();

  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };
  const handleToggle = () => {
    setOpen(!open);
  };

  return (
    <div>
      <Button onClick={handleToggle}>
        Open
      </Button>

      <Backdrop open={open} onClick={handleClose} className={classes.backdrop}>
        <Loading color="white" />
      </Backdrop>
    </div>
  );
};

/* En este caso, si añadía el loading como prop children no lo reconoce */
export const SimpleBackdrop = Template.bind({});
SimpleBackdrop.args = {};
