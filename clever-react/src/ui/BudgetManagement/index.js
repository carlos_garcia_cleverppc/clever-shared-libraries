import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import nextId from "react-id-generator";

import AlertCard from "../AlertCard";
import BrandIcon from "../BrandIcon";
import Button from "../Button";
import Grid from "../Grid";
import RoundedProgressBar from "../RoundedProgressBar";
import Slider from "../Slider";
import TextField from "../TextField";
import Typography from "../Typography";

import getClasses from "./styles";
import useTheme from "../styles/useTheme";

const BudgetManagement = props => {
  const classes = getClasses();
  const { cleverUI: { colors } } = useTheme();

  const { budgetCopy, currency, channels, textButton, onClickButton, minAlert, maxAlert } = props;

  const [finalChannels, setFinalChannels] = useState(channels);
  const [totalQuantity, setTotalQuantity] = useState(Object.values(finalChannels).reduce((a, b) => a + b, 0));
  const [progresses, setProgresses] = useState(Object.keys(channels).map(channel => (
    { color: channel, progress: (channels[channel] * 100) / totalQuantity })));

  const [showMinAlert, setShowMinAlert] = useState(false);
  const [showMaxAlert, setShowMaxAlert] = useState(false);

  useEffect(() => {
    const values = Object.values(finalChannels);
    if (values.includes(0)) {
      setShowMinAlert(true);
    } else {
      setShowMinAlert(false);
    }

    if (values.includes(200)) {
      setShowMaxAlert(true);
    } else {
      setShowMaxAlert(false);
    }

    setTotalQuantity(values.reduce((a, b) => a + b, 0));
  }, [finalChannels]);

  useEffect(() => {
    setProgresses(Object.keys(finalChannels).map(channel => (
      { color: channel, progress: (finalChannels[channel] * 100) / totalQuantity })));
  }, [totalQuantity]);

  const getNameIcon = name => {
    const auxName = name.toLowerCase();
    let result = "";

    if (auxName.includes("googleads")) {
      result = "GoogleAdsCircle";
    } else if (auxName.includes("analytics")) {
      result = "GoogleAnalyticsCircle";
    } else if (auxName.includes("merchant")) {
      result = "GoogleMerchantCircle";
    } else if (auxName.includes("facebook")) {
      result = "FacebookCircle";
    } else if (auxName.includes("twitter")) {
      result = "TwitterCircle";
    } else if (auxName.includes("microsoft")) {
      result = "MicrosoftCircle";
    } else if (auxName.includes("linkedin")) {
      result = "LinkedinCircle";
    } else if (auxName.includes("instagram")) {
      result = "InstagramCircle";
    }
    return result;
  };

  const handleOnChangeTextfield = (newValue, channel) => {
    if (newValue < 0) {
      setFinalChannels({ ...finalChannels, [channel]: 0 });
    } else if (newValue > 200) {
      setFinalChannels({ ...finalChannels, [channel]: 200 });
    } else {
      setFinalChannels({ ...finalChannels, [channel]: Number(newValue) });
    }
  };

  return (
    <>
      <RoundedProgressBar
        size={145}
        strokeWidth={4}
        progressesArray={progresses}
        className={classes.progress}
      >
        <div className={classes.alignCenter}>
          <div className={classnames(classes.sameLine, classes.alignBottom)}>
            <Typography variant="f1-42" weight="bold" shade={700}>{totalQuantity}</Typography>
            <Typography variant="f1-20" weight="bold" shade={700}>{currency}</Typography>
          </div>

          <Typography variant="f1-12" shade={700}>{budgetCopy}</Typography>
        </div>
      </RoundedProgressBar>

      {showMinAlert && (
        <AlertCard
          color="warning"
          text={minAlert.copyAlert}
          linkText={minAlert.copyOnClick}
          action={minAlert.onClick}
          className={classes.bottomSpace}
        />
      )}

      {showMaxAlert && (
        <AlertCard
          color="warning"
          text={maxAlert.copyAlert}
          linkText={maxAlert.copyOnClick}
          action={maxAlert.onClick}
          className={classes.bottomSpace}
        />
      )}

      <Grid container>
        {Object.keys(finalChannels).map(channel => (
          <Grid item xs={12} className={classnames(classes.sameLine, classes.align)} key={nextId("slider-")}>
            <div className={classes.brandIcon}>
              <BrandIcon icon={getNameIcon(channel)} size={32} />
            </div>

            <div className={classes.sliderWrapper}>
              <div
                style={{
                  marginLeft: "-9px",
                  position: "relative",
                  left: `${((channels[channel] - 0) * 100) / (200 - 0)}%`,
                  bottom: "-12px",
                  opacity: "50%",
                }}
              >
                <Typography variant="f1-10" color={channel}>
                  {channels[channel]}
                </Typography>
              </div>

              <div
                style={{
                  background: colors[channel][500],
                  width: 8,
                  height: 8,
                  marginLeft: "-7px",
                  borderRadius: "4px",
                  position: "relative",
                  left: `${((channels[channel] - 0) * 100) / (200 - 0)}%`,
                  bottom: "-18px",
                  opacity: "50%",
                }}
              />

              <Slider
                value={finalChannels[channel]}
                onChange={(event, newValue) => setFinalChannels({ ...finalChannels, [channel]: Number(newValue) })}
                color={channel}
                valueLabelDisplay="on"
                min={0}
                max={200}
                step={5}
                valueLabelClass={classnames(classes.valueLabel, classes[channel])}
                thumbClass={classes.thumb}
                className={classes.slider}
              />
            </div>

            <TextField
              value={finalChannels[channel]}
              onChange={event => handleOnChangeTextfield(event.target.value, channel)}
              className={classes.textField}
              id={`budget-management-${channel}`}
            />
          </Grid>
        ))}

        <Grid item xs={12} className={classes.button}>
          <Button onClick={() => onClickButton(finalChannels)}>
            {textButton}
          </Button>
        </Grid>
      </Grid>
    </>
  );
};

BudgetManagement.propTypes = {
  budgetCopy: PropTypes.string,
  currency: PropTypes.string,
  channels: PropTypes.instanceOf(Object),
  textButton: PropTypes.string,
  onClickButton: PropTypes.func,
  minAlert: PropTypes.instanceOf(Object),
  maxAlert: PropTypes.instanceOf(Object),
};

BudgetManagement.defaultProps = {
  budgetCopy: "",
  currency: "",
  channels: {},
  textButton: "",
  onClickButton: () => { },
  minAlert: {},
  maxAlert: {},
};

export default BudgetManagement;
