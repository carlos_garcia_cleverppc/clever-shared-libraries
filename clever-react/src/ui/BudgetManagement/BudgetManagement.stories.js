/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import BudgetManagement from "./index";

export default {
  title: "BudgetManagement",
  component: BudgetManagement,
};

const Template = args => <BudgetManagement {...args} />;

export const SimpleBudgetManagement = Template.bind({});
SimpleBudgetManagement.args = {
  budgetCopy: "daily budget",
  currency: "USD",
  channels: {
    googleAds: 10,
    facebookAds: 50,
    twitterAds: 100,
  },
  textButton: "Apply new budget",
  minAlert: {
    copyAlert: "This is an alert",
    copyOnClick: "Click here",
    onClick: () => { },
  },
  maxAlert: {
    copyAlert: "This is an alert",
    copyOnClick: "Click here",
    onClick: () => { },
  },
};
