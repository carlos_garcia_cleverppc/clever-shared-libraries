import makeStyles from "../styles/makeStyles";

const styles = (({ cleverUI: { colors, spacing, typography } }) => ({
  sameLine: { display: "inline-flex" },
  bottomSpace: { ...spacing["mb-3"] },
  alignBottom: { alignItems: "baseline" },
  alignCenter: { textAlign: "center" },
  branIcon: { ...spacing["pr-2"], height: "32px", width: "32px" },
  progress: { ...spacing["mb-6"] },
  valueLabel: {
    top: "-20px",
    left: "-15px",
    boxShadow: "none",
    "& *": {
      fontSize: "10px",
      fontFamily: typography.f1,
      backgroundColor: "transparent",
    },
  },
  googleAds: { "& *": { color: colors.googleAds[500] } },
  facebookAds: { "& *": { color: colors.facebookAds[500] } },
  twitterAds: { "& *": { color: colors.twitterAds[500] } },
  linkedinAds: { "& *": { color: colors.linkedinAds[500] } },
  instagramAds: { "& *": { color: colors.instagramAds[500] } },
  microsoftAds: { "& *": { color: colors.microsoftAds[500] } },
  thumb: { height: "12px", width: "12px", marginTop: "-5px" },
  slider: {},
  textField: {
    height: "26px",
    maxWidth: "63px",
    "& div": { height: "26px", maxWidth: "63px" },
    "& input": { fontSize: "12px !important" },
  },
  sliderWrapper: {
    ...spacing["mx-4"],
    ...spacing["pb-3"],
    display: "block",
    width: "100%",
  },
  align: { alignItems: "center" },
  button: { ...spacing["mt-5"], textAlign: "center" },
}));

export default makeStyles(styles);
