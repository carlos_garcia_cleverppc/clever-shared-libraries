import React from "react";
import PropTypes from "prop-types";
import User from "../Icon/User";

import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors } }) => ({
  defaultAvatar: ({ size }) => ({
    backgroundColor: colors.neutral[100],
    width: size,
    height: size,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: "50%",
    "& svg": {
      width: "62%",
      height: "62%",
    },
  }),
});
const getClasses = makeStyles(styles);

const DefaultAvatar = ({ size }) => {
  const classes = getClasses(({ size }));
  return (<div className={classes.defaultAvatar} aria-hidden="true"><User /></div>);
};

DefaultAvatar.propTypes = { size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]) };

DefaultAvatar.defaultProps = { size: 42 };
export default DefaultAvatar;
