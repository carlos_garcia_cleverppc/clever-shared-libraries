/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import { getColorsLabels } from "../styles/colors";

import Avatar from "./index";

export default {
  title: "Avatar",
  component: Avatar,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => <Avatar {...args} />;

export const SquareImage = Template.bind({});
SquareImage.args = { src: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTafhaU5d1HmEJ3wmi-ZYtu-XsZX6UJiq1WAg&usqp=CAU" };

export const NotSquareImage = Template.bind({});
NotSquareImage.args = { src: "https://geekzilla.tech/home/wp-content/uploads/2020/03/Amazon-1.jpg" };

export const EmptyImage = Template.bind({});
EmptyImage.args = { src: "" };
