import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  root: ({ size }) => ({
    width: size,
    height: size,
    minWidth: size,
    position: "relative",
  }),
  avatarWrapper: {
    width: "100%",
    height: "100%",
    display: "grid",
    borderRadius: "50%",
    "& > *": {
      gridArea: "1/-1",
      borderRadius: "inherit",
    },
  },
  circle: ({ color }) => ({ boxShadow: `inset 0 0 0 3px ${colors[color][500]}` }),
  labelCircle: ({ color }) => ({
    borderRadius: "50%",
    border: `3px solid ${colors[color][500]}`,
    boxSizing: "border-box",
    overflow: "hidden",
  }),
  labelTopRight: ({ color }) => ({
    position: "absolute",
    top: 0,
    right: 0,
    transform: "translateX(50%)",
    whiteSpace: "nowrap",
    ...spacing["p-1"],
    backgroundColor: colors[color][100],
    borderRadius: "8px",
  }),
  labelBottomCenter: ({ color, size }) => ({
    position: "absolute",
    bottom: "-9px", // Middle of label height
    right: size / 2,
    transform: "translateX(50%)",
    whiteSpace: "nowrap",
    ...spacing["p-1"],
    backgroundColor: colors[color][100],
    borderRadius: "8px",
  }),
  image: {
    width: "100%",
    height: "100%",
    objectFit: "contain",
  },
});

export default makeStyles(styles);
