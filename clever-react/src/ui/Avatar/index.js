/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getColorsLabels } from "../styles/colors";

import Image from "../Image";
import Typography from "../Typography";

import getClasses from "./styles";
import DefaultAvatar from "./DefaultAvatar";

const colorsList = getColorsLabels(true);

const Avatar = props => {
  const { className, size, src, color, label, colorCircle, positionLabel } = props;
  const classes = getClasses({ size, color, positionLabel });

  return (
    <div className={classnames(classes.root, className)}>
      <div className={classes.avatarWrapper}>
        {src ? (
          <Image
            className={classes.image}
            src={src}
            size="responsive"
            variant="circle"
            center={false}
          />
        ) : <DefaultAvatar size="100%" />}

        {colorCircle && <div className={classes.circle} />}
      </div>

      {label && (
        <Typography
          color={color}
          weight="bold"
          variant="f1-8"
          className={positionLabel === "top-right" ? classes.labelTopRight : classes.labelBottomCenter}
        >
          {label}
        </Typography>
      )}
    </div>
  );
};

Avatar.propTypes = {
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  src: PropTypes.string,
  label: PropTypes.string,
  colorCircle: PropTypes.bool,
  color: PropTypes.oneOf(colorsList),
  positionLabel: PropTypes.oneOf(["top-right", "bottom-center"]),
};

Avatar.defaultProps = {
  size: 42,
  src: "",
  label: "",
  colorCircle: false,
  color: "primary",
  positionLabel: "top-right",
};

export default Avatar;
