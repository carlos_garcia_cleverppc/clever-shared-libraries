import React from "react";
import PropTypes from "prop-types";

import TextField from "../TextField";

import SelectItem from "./Item";
import getClasses from "./styles";

// eslint-disable-next-line no-unused-vars
function printMenuItemDefault(label, value, disabled, index) {
  return label;
}

const Select = props => {
  const { label, value, name, options, printMenuItem, size, placeholder, native, ...selectProps } = props;

  const classes = getClasses();

  return (
    <TextField
      value={value}
      variant="outlined"
      SelectProps={{
        classes: {
          icon: classes.selectIcon,
        },
        MenuProps: {
          classes: { paper: classes.scroll },
        },
        native,
      }}
      select
      label={(!label) ? "" : label.toString()}
      size={size}
      {...selectProps}
    >
      {!!placeholder && value === "none" && (
        <SelectItem native={native} className={classes.menuItem} value="none" disabled>
          {placeholder}
        </SelectItem>
      )}

      {
        options?.map((option, index) => (
          <SelectItem
            native={native}
            className={classes.menuItem}
            key={`${name}-${option.label}`}
            disabled={option.disabled || false}
            value={option.value}
          >
            {printMenuItem(option.label, option.value, option.disabled || false, index)}
          </SelectItem>
        ))
      }
    </TextField>
  );
};

Select.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
  options: PropTypes.instanceOf(Array).isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  printMenuItem: PropTypes.func,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  size: PropTypes.oneOf(["small", "standard"]),
  startAdornment: PropTypes.instanceOf(Object),
  native: PropTypes.bool,
};

Select.defaultProps = {
  className: "",
  label: "",
  placeholder: "",
  printMenuItem: printMenuItemDefault,
  disabled: false,
  size: "standard",
  startAdornment: undefined,
  native: false,
};

export default Select;
