/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import PropTypes from "prop-types";

import MenuItem from "@material-ui/core/MenuItem";

const SelectItem = React.forwardRef(({ native, ...props }, ref) => {
  if (native) {
    return (
      <option {...props} />
    );
  }

  return (
    <MenuItem {...props} ref={ref} />
  );
});

SelectItem.propTypes = { native: PropTypes.bool };

SelectItem.defaultProps = { native: false };

export default SelectItem;
