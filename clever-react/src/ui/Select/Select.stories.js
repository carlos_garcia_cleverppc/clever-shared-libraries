/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Select from "./index";

export default {
  title: "Select",
  component: Select,
};

const Template = args => {
  const [selected, setSelected] = React.useState("");
  const options = [{ label: "Today", value: 1 }, { label: "Last 7 days", value: 7 }];

  const handleChange = event => {
    setSelected(event.target.value);
  };

  return <Select {...args} value={selected} options={options} onChange={handleChange} />;
};

export const SimpleSelect = Template.bind({});
SimpleSelect.args = {
  name: "select",
  label: "Select label",
};
