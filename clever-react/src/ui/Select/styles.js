import makeStyles from "../styles/makeStyles";

const styles = (({ cleverUI: { spacing, colors, typography } }) => {
  const { sizing } = typography;

  return ({
    scroll: {
      "&::-webkit-scrollbar": {
        width: "12px",
        backgroundColor: "#fff",
      },
      "&::-webkit-scrollbar-track": { backgroundColor: "#fff" },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: colors.neutral[300],
        borderRadius: "16px",
        border: "4px solid #fff",
      },
    },
    selectIcon: {
      ...spacing["mr-1"],
      fill: colors.neutral[900],
    },
    menuItem: ({ size }) => ({
      fontSize: size === "small" ? sizing.body0 : sizing.body1,
      fontFamily: typography.font1,
      color: colors.neutral[900],
    }),
  });
});

export default makeStyles(styles);
