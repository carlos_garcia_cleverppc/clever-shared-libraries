import makeStyles from "../styles/makeStyles";
import getContrastText from "../styles/contrastText";

const styles = ({ cleverUI: { colors } }) => ({
  badge: ({ color }) => ({
    backgroundColor: colors[color][500],
    color: getContrastText(colors[color][500]),
  }),
});

export default makeStyles(styles);
