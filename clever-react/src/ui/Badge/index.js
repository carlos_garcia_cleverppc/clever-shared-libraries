/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import MaterialBadge from "@material-ui/core/Badge";

import getClasses from "./styles";

const Badge = props => {
  const { variant, badgeContent, className, children } = props;
  const { invisible, showZero, color } = props;

  const classes = getClasses({ color });
  const componentClasses = getClasses({ color });

  return (
    <MaterialBadge
      variant={variant}
      badgeContent={badgeContent}
      invisible={invisible}
      showZero={showZero}
      className={className}
      classes={{ ...classes, badge: classnames(componentClasses.badge, classes.badge) }}
    >
      {children}
    </MaterialBadge>
  );
};

Badge.propTypes = {
  color: PropTypes.oneOf(["primary", "secondary", "success", "danger", "warning", "info", "orange"]),
  className: PropTypes.string,
  variant: PropTypes.oneOf(["standard", "dot"]),
  badgeContent: PropTypes.node,
  showZero: PropTypes.bool,
  invisible: PropTypes.bool,
};

Badge.defaultProps = {
  color: "primary",
  className: "",
  variant: "standard",
  badgeContent: "",
  showZero: false,
  invisible: false,
};

export default Badge;
