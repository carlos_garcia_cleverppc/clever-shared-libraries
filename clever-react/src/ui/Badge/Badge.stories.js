/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Bell from "../Icon/Bell";
import Badge from "./index";

export default {
  title: "Badge",
  component: Badge,
};

const Template = args => <Badge {...args} />;

export const SimpleBadge = Template.bind({});
SimpleBadge.args = {
  variant: "dot",
  color: "danger",
  children: <Bell color="primary" />,
};

export const ContentBadge = Template.bind({});
ContentBadge.args = {
  variant: "standard",
  color: "secondary",
  badgeContent: 4,
  children: <Bell color="primary" />,
};
