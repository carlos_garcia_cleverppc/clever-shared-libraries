import React from "react";
import CheckboxTreeComponent from "react-checkbox-tree";
import PropTypes from "prop-types";
import classnames from "classnames";

import Checkbox from "../Checkbox";
import ChevronRight from "../Icon/ChevronRight";
import ChevronDown from "../Icon/ChevronDown";
import makeStyles from "../styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { colors, typography } }) => ({
  node: {
    fontFamily: typography.font2,
    color: colors.neutral[700],
    "& ol": {
      "& li": {
        "& span": {
          "& label": {
            display: "flex",
            alignItems: "center",
            "&:hover": {
              backgroundColor: colors.primary[100],
              borderRadius: "8px",
              transition: "all .3s ease-in-out",
            },
          },
        },
      },
    },
  },
}));

const CheckboxTree = props => {
  const classes = getClasses();

  const { nodes, className, checked, onCheck, expanded } = props;
  const { onExpand, onClick } = props;

  const newNodes = nodes;
  newNodes.forEach(node => {
    node.className = classnames(classes.node, className);
  });

  return (
    <CheckboxTreeComponent
      nodes={nodes}
      checked={checked}
      expanded={expanded}
      onCheck={checkedAux => onCheck(checkedAux)}
      onExpand={expandedAux => onExpand(expandedAux)}
      onClick={onClick}
      onlyLeafCheckboxes
      className={classnames(classes.node, className)}
      showNodeIcon={false}
      expandOnClick
      icons={{
        check: <Checkbox size="small" checked />,
        uncheck: <Checkbox size="small" checked={false} />,
        expandClose: <ChevronRight size={24} />,
        expandOpen: <ChevronDown size={24} />,
      }}
    />
  );
};

CheckboxTree.propTypes = {
  nodes: PropTypes.instanceOf(Array),
  checked: PropTypes.instanceOf(Array),
  expanded: PropTypes.instanceOf(Array),
  className: PropTypes.string,
  onCheck: PropTypes.func,
  onExpand: PropTypes.func,
  onClick: PropTypes.func,
};

CheckboxTree.defaultProps = {
  nodes: [],
  className: "",
  expanded: [],
  checked: [],
  onCheck: () => { },
  onExpand: () => { },
  onClick: () => { },
};

export default CheckboxTree;
