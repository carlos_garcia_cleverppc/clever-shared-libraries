/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import ChevronRight from "../Icon/ChevronRight";
import ChevronDown from "../Icon/ChevronDown";
import CheckboxTree from "./index";
import Checkbox from "../Checkbox";

import "./styles.scss";

export default {
  title: "CheckboxTree",
  component: CheckboxTree,
};

const Template = args => {
  const nodes = [
    {
      value: "mars",
      label: "Mars",
      children: [
        { value: "phobos", label: "Phobos" },
        { value: "deimos", label: "Deimos" },
      ],
    },
  ];

  const [checked, setChecked] = React.useState([]);
  const [expanded, setExpanded] = React.useState([]);

  return (
    <CheckboxTree
      {...args}
      nodes={nodes}
      checked={checked}
      expanded={expanded}
      onCheck={setChecked}
      onExpand={setExpanded}
      onClick={
        clickedNode => {
          if (clickedNode.showCheckbox && !clickedNode.disabled) {
            let newCheckedList;

            if (clickedNode.checked) {
              newCheckedList = checked.filter(currentValue => (currentValue !== clickedNode.value));
            } else {
              newCheckedList = [...checked, clickedNode.value];
            }

            setChecked(newCheckedList);
          }
        }
      }
      icons={{
        check: <Checkbox size="small" checked />,
        uncheck: <Checkbox size="small" checked={false} />,
        expandClose: <ChevronRight size={24} />,
        expandOpen: <ChevronDown size={24} />,
      }}
    />
  );
};

export const SimpleCheckboxTree = Template.bind({});
SimpleCheckboxTree.args = {};
