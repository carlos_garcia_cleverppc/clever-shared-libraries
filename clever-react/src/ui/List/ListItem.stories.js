/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import ListItem from "./index";
import Item from "./Item/index";
import ItemIcon from "./Item/Icon/index";
import Typography from "../Typography";
import Alert from "../Icon/Alert";

export default {
  title: "ListItem",
  component: ListItem,
};

const Template = args => <ListItem {...args} />;

export const SimpleListItem = Template.bind({});
SimpleListItem.args = {
  children: (
    <>
      <Item button disabled onClick={() => { }}>
        <Typography>This is an item</Typography>
      </Item>

      <Item>
        <Typography>This is an item 2</Typography>
      </Item>
    </>
  ),
};

export const IconListItem = Template.bind({});
IconListItem.args = {
  children: (
    <>
      <Item>
        <Typography>This is an item</Typography>

        <ItemIcon icon={<Alert />} />
      </Item>

      <Item>
        <Typography>This is an item 2</Typography>

        <ItemIcon icon={<Alert />} />
      </Item>
    </>
  ),
};

export const CustomComponent = Template.bind({});
CustomComponent.args = {
  children: (
    <>
      <Item component="a" href="http://cleverecommerce.com">
        <Typography>This is an item</Typography>
      </Item>

      <Item component="h2">
        <Typography>This is an item 2</Typography>
      </Item>
    </>
  ),
};
