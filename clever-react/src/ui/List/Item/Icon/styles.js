import makeStyles from "../../../styles/makeStyles";

const styles = ({ cleverUI: { colors, shadows } }) => ({
  root: {
    marginLeft: "auto",
    borderRadius: 50,
    "&:hover": {
      boxShadow: shadows.hover,
    },
  },
});

export default makeStyles(styles);
