import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import IconButton from "../../../IconButton";

import getClasses from "./styles";

const ListItemIcon = ({ className, icon, onClick }) => {
  const classes = getClasses();

  return (
    <IconButton
      icon={icon}
      size="small"
      onClick={onClick}
      className={classnames(className, classes.root)}
      color="primary"
      outlined={false}
    />
  );
};

ListItemIcon.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.instanceOf(Object).isRequired,
  onClick: PropTypes.func,
};

ListItemIcon.defaultProps = {
  className: "",
  onClick: () => { },
};

export default ListItemIcon;
