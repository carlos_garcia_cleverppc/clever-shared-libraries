import makeStyles from "../../styles/makeStyles";

const styles = ({ cleverUI: { spacing } }) => ({
  root: ({ onClick, hoverSelect, padding }) => {
    let res = { display: "flex", backgroundColor: "transparent", alignItems: "center" };

    if (!!onClick && !!hoverSelect) {
      res = {
        ...res,
        cursor: "pointer",

        "&:hover,&:focus": { backgroundColor: "rgba(0, 0, 0, 0.08)" },
      };
    }

    if (padding) {
      res = {
        ...res,
        ...spacing["p-1"],
      };
    }

    return res;
  },
});

export default makeStyles(styles);
