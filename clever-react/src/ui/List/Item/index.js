import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

import getClasses from "./styles";

const ListItem = React.forwardRef((props, ref) => {
  const { component, className, onClick, children, selected, padding, classes, hoverSelect, ...otherProps } = props;
  const componentClasses = getClasses({ padding, onClick, hoverSelect });
  const tagName = component;

  return React.createElement(tagName, {
    className: classnames(className, componentClasses.root, selected && classes?.selected ? classes.selected : ""),
    onClick,
    ref,
    ...otherProps,
  }, children);
});

ListItem.propTypes = {
  // eslint-disable-next-line react/no-unused-prop-types
  component: PropTypes.node,
  padding: PropTypes.bool,
  className: PropTypes.string,
  onClick: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  selected: PropTypes.bool,
  hoverSelect: PropTypes.bool,
};

ListItem.defaultProps = { className: "",
  component: "li",
  padding: true,
  selected: false,
  hoverSelect: false };

export default ListItem;
