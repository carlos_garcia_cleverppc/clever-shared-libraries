import makeStyles from "../styles/makeStyles";

const styles = { root: { listStyle: "none", padding: 0 } };

export default makeStyles(styles);
