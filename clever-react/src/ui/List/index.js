import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

import getClasses from "./styles";

const List = ({ className, children }) => {
  const classes = getClasses();

  return (
    <ul className={classnames(className, classes.root)}>
      {children}
    </ul>
  );
};

List.propTypes = { className: PropTypes.string };

List.defaultProps = { className: "" };

export default List;
