import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";

import Analytics from "../../utils/tracking/analytics";

const PageView = ({ name }) => {
  useEffect(() => {
    Analytics.pageView(name);
  }, [name]);

  return (<></>);
};

PageView.propTypes = { name: PropTypes.string.isRequired };

export default PageView;
