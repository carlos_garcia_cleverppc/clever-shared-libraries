/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import { getColorsLabels } from "../styles/colors";

import Link from "./index";

export default {
  title: "Link",
  component: Link,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => <Link {...args} />;

export const SimpleLink = Template.bind({});
SimpleLink.args = { children: "Simple link" };

export const CustomLink = Template.bind({});
CustomLink.args = {
  children: "Custom link",
  color: "pink",
};
