/* eslint-disable react/no-unused-prop-types */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getColorsLabels } from "../styles/colors";

import getClasses from "./styles";

const colorsList = getColorsLabels(true);

const CleverLink = props => {
  const { children, className, to } = props;
  const { target, onClick, disabled } = props;

  const classes = getClasses(props);

  if (to) {
    return (
      <a
        href={to}
        className={classnames(classes.root, className, disabled ? classes.disabled : "")}
        target={target}
        disabled={disabled}
      >
        {children}
      </a>
    );
  }

  return (
    <button
      type="button"
      className={classnames(classes.root, className, disabled ? classes.disabled : "")}
      onClick={onClick}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

CleverLink.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string,
  target: PropTypes.string,
  onClick: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  disabled: PropTypes.bool,
  color: PropTypes.oneOf(colorsList),
};

CleverLink.defaultProps = {
  to: "",
  className: "",
  target: "",
  onClick: false,
  disabled: false,
  color: "blue",
};

export default CleverLink;
