import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, typography } }) => ({
  root: ({ color }) => ({
    fontFamily: typography.font2,
    lineHeight: typography.lineHeight,
    fontWeight: "bold",
    fontSize: "1rem",
    color: colors[color][500],
    textDecoration: "none",
    cursor: "pointer",
    // override button styles
    border: "none",
    backgroundColor: "transparent",
    textAlign: "left",
    // display: "block",
    padding: 0,
    "&:hover": { color: colors[color][700] },
    "&:focus": { outline: "none", color: colors[color][700] },
  }),
  disabled: ({ color }) => ({
    color: colors[color][500],
    cursor: "default",
    opacity: "0.5",
    textDecoration: "none",
    pointerEvents: "none",
    "&:hover": { color: colors[color][500] },
  }),
});

export default makeStyles(styles);
