import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  positionHorizontal: {
    display: "inline-flex",
  },
  container: ({ size }) => ({
    ...spacing["mb-5"],
    ...spacing["mx-2"],
    width: size,
    height: size,
    minWidth: size,
    position: "relative",
  }),
  circle: ({ color }) => ({ border: "3px solid" }),
  wrapper: {
    width: "100%",
    height: "100%",
    display: "grid",
    borderRadius: "50%",
    "& > *": {
      gridArea: "1/-1",
      borderRadius: "inherit",
    },
  },
  dotContainer: {
    height: "10px",
    width: "100%",
    textAlign: "center",
    position: "absolute",
    bottom: "-4px",
  },
  linkSize: ({ size }) => ({
    width: size,
    height: size,
    minWidth: size,
  }),
  defaultBrand: ({ size }) => ({
    backgroundColor: colors.neutral[300],
    width: size,
    height: size,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: "50%",
    "& svg": {
      width: "62%",
      height: "62%",
    },
  }),
  activeAll: ({ size }) => ({
    width: size - 6,
    height: size - 6,
  }),
  allCopySize: ({ allCopySize }) => ({
    fontSize: `${allCopySize}px`,
  }),
});

export default makeStyles(styles);
