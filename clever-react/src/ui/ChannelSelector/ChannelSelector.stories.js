/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import ChannelSelector from "./index";

export default {
  title: "ChannelSelector",
  component: ChannelSelector,
};

const Template = args => <ChannelSelector {...args} />;

export const SimpleChannelSelector = Template.bind({});
SimpleChannelSelector.args = {
  allCopy: "All",
  channels:
  {
    all: {
      connected: true,
      active: false,
    },
    googleAds: {
      connected: true,
      active: true,
    },
    facebookAds: {
      connected: false,
      active: false,
    },
    twitterAds: {
      connected: true,
      active: false,
    },
    microsoftAds: {
      connected: false,
      active: false,
    },
  },
};
