import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import Circle from "../Icon/Circle";
import BrandIcon from "../BrandIcon";
import Link from "../Link";
import Typography from "../Typography";

import getClasses from "./styles";

const ChannelSelector = props => {
  const { className, color, size, channels, allCopy, position } = props;
  const allCopySize = 40 / allCopy.length;

  const classes = getClasses({ color, size, allCopySize });

  const getNameIcon = (name, connected) => {
    const auxName = name.toLowerCase();
    let result = "";

    if (auxName.includes("googleads")) {
      result = "GoogleAdsCircle";
    } else if (auxName.includes("analytics")) {
      result = "GoogleAnalyticsCircle";
    } else if (auxName.includes("merchant")) {
      result = "GoogleMerchantCircle";
    } else if (auxName.includes("facebook")) {
      result = "FacebookCircle";
    } else if (auxName.includes("twitter")) {
      result = "TwitterCircle";
    } else if (auxName.includes("microsoft")) {
      result = "MicrosoftCircle";
    } else if (auxName.includes("linkedin")) {
      result = "LinkedinCircle";
    } else if (auxName.includes("instagram")) {
      result = "InstagramCircle";
    }
    if (!connected) result = `${result}Unlinked`;
    return result;
  };

  const getAllCircle = active => (
    <div className={classnames(classes.defaultBrand, active ? classes.activeAll : "")}>
      <Typography
        variant="f1-12"
        weight="bold"
        shade={900}
        className={classes.allCopySize}
      >
        {allCopy}
      </Typography>
    </div>
  );

  const printIcon = (name, active, connected) => (
    <div className={classes.container} key={`channel-${name}`}>
      <div className={classes.wrapper}>
        {name === "all" ? (
          <Link
            onClick={() => channels[name].onClick()}
            className={classnames(classes.linkSize, active ? classes.circle : "")}
          >
            {getAllCircle(active)}
          </Link>
        )
          : (
            <Link
              onClick={() => channels[name].onClick()}
              className={classnames(classes.linkSize, active ? classes.circle : "")}
            >
              <BrandIcon icon={getNameIcon(name, connected)} size={active ? 36 : 42} />
            </Link>
          )}

        {active && (
          <div className={classes.dotContainer}>
            <Circle color={color} size={6} />
          </div>
        )}
      </div>
    </div>
  );

  return (
    <div className={classnames(position === "horizontal" ? classes.positionHorizontal : "", className)}>
      {Object.keys(channels).map(brandName => (
        printIcon(brandName, channels[brandName].active, channels[brandName].connected)
      ))}
    </div>
  );
};

ChannelSelector.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  channels: PropTypes.instanceOf(Object),
  allCopy: PropTypes.string,
  position: PropTypes.oneOf(["vertical", "horizontal"]),
};

ChannelSelector.defaultProps = {
  color: "primary",
  size: 42,
  channels: [],
  allCopy: "All",
  position: "vertical",
};

export default ChannelSelector;
