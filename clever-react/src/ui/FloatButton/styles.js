import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  button: ({ buttonColor, onClick }) => ({
    position: "fixed",
    bottom: "20px",
    right: "20px",
    backgroundColor: colors[buttonColor][500],
    "&:hover": { backgroundColor: onClick ? colors[buttonColor][500] : colors[buttonColor][900] },
    zIndex: "1",
  }),
  popper: { zIndex: "2" },
  paper: {
    ...spacing["mb-4"],
    borderRadius: "8px",
  },
});

export default makeStyles(styles);
