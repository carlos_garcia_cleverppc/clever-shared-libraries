/* eslint-disable no-console */
/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import { CSSTransition } from "react-transition-group";

import MenuList from "../Menu/MenuList";
import MenuItem from "../Menu/Item";

import FloatButton from "./index";
import Typography from "../Typography";

import Grid from "../Grid";

import ShoppingCart from "../Icon/ShoppingCart";
import Calendar from "../Icon/Calendar";
import ChevronRight from "../Icon/ChevronRight";
import ArrowRight from "../Icon/ArrowRight";
import Plus from "../Icon/Plus";

import makeStyles from "../styles/makeStyles";
import useTheme from "../styles/useTheme";

const getClasses = makeStyles(({ cleverUI: { colors, spacing } }) => ({
  button: { bottom: "20px" },
  paper: { ...spacing["mb-4"], borderRadius: "8px", maxWidth: "350px", backgroundColor: colors.secondary[500] },
  paper2: {
    ...spacing["p-4"],
    borderRadius: "8px",
    backgroundColor: colors.neutral[500],
  },
  content: {
    width: "fit-content",
    "&:hover": { borderRadius: "8px" },
  },
  inline: {
    display: "inline-flex",
    justifyContent: "center",
    alignItems: "center",
  },
  item: {
    ...spacing["py-2"],
    "& span": { backgroundColor: "white" },
  },
  title: {
    ...spacing["py-2"],
    display: "flex",
  },
  textTitle: {
    ...spacing["px-2"],
    fontWeight: "bold",
  },
  chevron: { ...spacing["mt-2"] },
  iconChevronOpen: { transform: "rotate(90deg)", marginTop: "10px" },
  iconPlusOpen: { transform: "rotate(45deg)", verticalAlign: "middle" },
  iconChevronClose: { transform: "rotate(-90deg)", marginTop: "10px" },
  iconPlusClose: { transform: "rotate(-90deg)", verticalAlign: "middle" },
  rotateEnter: { transform: "rotate(-90deg)" },
  rotateEnterActive: {
    transform: "rotate(90deg)",
    transition: "all 300ms cubic-bezier(.4,1.39,.93,1.01)",
  },
  rotateExit: { transform: "rotate(90deg)" },
  rotateExitActive: {
    transform: "rotate(-90deg)",
    transition: "all 300ms cubic-bezier(.4,1.39,.93,1.01)",
  },
  rotatePlusEnter: { transform: "rotate(-90deg)" },
  rotatePlusEnterActive: {
    transform: "rotate(45deg)",
    transition: "all 300ms cubic-bezier(.4,1.39,.93,1.01)",
  },
  rotatePlusExit: { transform: "rotate(45deg)" },
  rotatePlusExitActive: {
    transform: "rotate(-90deg)",
    transition: "all 300ms cubic-bezier(.4,1.39,.93,1.01)",
  },
  plusContainer: { height: "26px", width: "26px" },
  icons: { marginTop: "-2px", display: "flex", alignItems: "center" },
  iconRight: { marginLeft: "-3px" },
  iconLeft: { marginRight: "-3px" },
  icon: { ...spacing["pl-2"] },
  textButton: {
    display: "flex",
    textTransform: "capitalize",
  },
}));

export default {
  title: "FloatButton",
  component: FloatButton,
};

const Template = args => {
  const classes = getClasses();
  const { variant } = args;
  const { cleverUI: { colors } } = useTheme();

  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const actions = [
    {
      icon: <ShoppingCart size={24} color="neutral" shade={100} />,
      name: "Title 1",
      description: "Description 1",
      onClick: () => { console.log("Go to section 1"); },
    },
    {
      icon: <Calendar size={24} color="neutral" shade={100} />,
      name: "Ttile 2",
      description: "Description 2",
      onClick: () => { console.log("Go to section 2"); },
    },
  ];

  const handleClick = action => event => {
    action.onClick();
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  const renderMenu = () => (
    actions.map(action => (
      <MenuItem
        onClick={handleClick(action)}
        className={classes.content}
        key={action.name}
      >
        <Grid container className={classes.item}>
          <div className={classes.inline}>
            <Grid item className={classes.title}>
              {action.icon}
            </Grid>

            <Grid item>
              <Typography color="neutral" shade={100} className={classes.textTitle}>
                {action.name}
              </Typography>
            </Grid>

            <Grid item>
              <ChevronRight size={24} color={colors.neutral[100]} className={classes.chevron} />
            </Grid>
          </div>

          <Grid item xs={12}>
            <Typography color="secondary" shade={100} variant="body0">
              {action.description}
            </Typography>
          </Grid>
        </Grid>
      </MenuItem>
    ))
  );

  if (variant === "circular") {
    return (
      <FloatButton
        open={open}
        anchorRef={anchorRef}
        setOpen={setOpen}
        buttonLabel={(
          <div className={classes.icons}>
            <div className={classes.iconLeft}>
              <CSSTransition
                in={open}
                exit={!open}
                timeout={300}
                classNames={{
                  enter: classes.rotateEnter,
                  enterActive: classes.rotateEnterActive,
                  exit: classes.rotateExit,
                  exitActive: classes.rotateExitActive,
                }}
              >
                <ChevronRight
                  size={26}
                  color={colors.neutral[100]}
                  className={open ? classes.iconChevronOpen : classes.iconChevronClose}
                />
              </CSSTransition>
            </div>

            <div className={classes.iconRight}>
              <div className={classes.plusContainer}>
                <CSSTransition
                  in={open}
                  exit={!open}
                  timeout={300}
                  classNames={{
                    enter: classes.rotatePlusEnter,
                    enterActive: classes.rotatePlusEnterActive,
                    exit: classes.rotatePlusExit,
                    exitActive: classes.rotatePlusExitActive,
                  }}
                >
                  <Plus
                    size={15}
                    color={colors.neutral[100]}
                    className={open ? classes.iconPlusOpen : classes.iconPlusClose}
                  />
                </CSSTransition>
              </div>
            </div>
          </div>
        )}
        classes={{
          paper: classes.paper,
          button: classes.button,
        }}
        content={(
          <MenuList autoFocusItem={open} id="menu-list-grow">
            {renderMenu()}
          </MenuList>
        )}
      // {...args}
      />
    );
  }
  return (
    <>
      <FloatButton
        setOpen={setOpen}
        anchorRef={anchorRef}
        buttonLabel={(
          <div className={classes.textButton}>
            <Typography variant="body0" color="white" shade={900}>
              Feedback
            </Typography>

            <ArrowRight color="white" size={12} className={classes.icon} />
          </div>
        )}
        buttonColor="navyBlue"
        classes={{
          paper: classes.paper2,
          button: classes.button,
        }}
        content={(
          <>
            <Typography variant="body0" weight="bold" color="white">
              Tu opinión es muy importante para nosotros.
            </Typography>
          </>
        )}
        onClick={() => console.log("Feedback button pressed")}
        variant="extended"
      />
    </>
  );
};

export const RoundFloatButton = Template.bind({});
RoundFloatButton.args = { buttonColor: "primary" };

export const ExtendedFloatButton = Template.bind({});
ExtendedFloatButton.args = { variant: "extended" };
