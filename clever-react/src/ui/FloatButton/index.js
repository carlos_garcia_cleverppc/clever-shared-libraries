/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import Fab from "@material-ui/core/Fab";

import getClasses from "./styles";

const FloatButton = props => {
  const style = getClasses(props);

  const { buttonLabel, content, classes, onClick } = props;
  const { setOpen, open, anchorRef, variant } = props;

  const handleClick = () => {
    if (onClick) {
      onClick();
    } else {
      setOpen(!open);
    }
  };

  const handleHover = () => {
    if (onClick) setOpen(true);
  };

  const handleLeave = () => {
    if (onClick) setOpen(false);
  };

  const handleClose = event => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      setOpen(false);
    }
    if (event.key === "Escape") {
      event.preventDefault();
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  return (
    <>
      <Fab
        ref={anchorRef}
        aria-controls={open ? "menu-list-grow" : undefined}
        aria-haspopup="true"
        onClick={handleClick}
        onMouseEnter={handleHover}
        onMouseLeave={handleLeave}
        className={classnames(style.button, classes.button)}
        onKeyDown={handleListKeyDown}
        variant={variant}
      >
        {buttonLabel}
      </Fab>

      <ClickAwayListener onClickAway={handleClose}>
        <Popper
          open={open}
          placement="top-end"
          anchorEl={anchorRef.current}
          role={undefined}
          transition
          disablePortal
          onKeyDown={handleListKeyDown}
          className={classnames(style.popper, classes.popper)}
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{ transformOrigin: placement === "bottom" ? "center top" : "center bottom" }}
            >
              <Paper className={classnames(style.paper, classes.paper)}>
                {content}
              </Paper>
            </Grow>
          )}
        </Popper>
      </ClickAwayListener>
    </>
  );
};

FloatButton.propTypes = {
  buttonLabel: PropTypes.instanceOf(Object),
  buttonColor: PropTypes.oneOf(["primary", "secondary", "success", "danger", "info", "warning", "blue", "green", "magenta", "yellow", "orange", "pink", "purple", "navyBlue", "christmas", "spring", "neutral", "white", "mobileApp", "audit", "promo", "campaignCreator", "keywordPlanner", "bannerCreator", "adsAssistant", "alt1", "alt2", "alt3"]),
  onClick: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.bool,
  ]),
  variant: PropTypes.oneOf(["extended", "circular"]),
  // content: PropTypes.instanceOf(Object),
  open: PropTypes.bool,
  setOpen: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.bool,
  ]),
  anchorEl: PropTypes.oneOfType([PropTypes.instanceOf(Object), PropTypes.func, PropTypes.string]),
  classes: PropTypes.instanceOf(Object),
};

FloatButton.defaultProps = {
  buttonLabel: <div />,
  buttonColor: "secondary",
  // content: <div />,
  variant: "circular",
  setOpen: false,
  open: false,
  onClick: false,
  anchorEl: "",
  classes: {
    button: "",
    paper: "",
    popper: "",
  },
};

export default FloatButton;

/*
  const { setOpen, open, anchorRef, variant } = props;
*/
