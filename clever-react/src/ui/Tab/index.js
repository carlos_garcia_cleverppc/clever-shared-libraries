/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";

import MaterialTab from "@material-ui/core/Tab";

import makeStyles from "../styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { colors, typography } }) => {
  const { sizing } = typography;

  return ({
    wrapper: {
      fontFamily: typography.font1,
      color: colors.neutral[900],
      textTransform: "none",
      fontWeight: "bold",
      fontSize: sizing.body1,
      display: "flex",
      flexDirection: "row",
    },
  });
});

const Tab = props => {
  const classes = getClasses();

  const { value, label, icon } = props;

  return (
    <MaterialTab
      {...props}
      value={value}
      label={label}
      icon={icon}
      classes={{ wrapper: classes.wrapper }}
      disableRipple
    />
  );
};

Tab.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  label: PropTypes.string.isRequired,
};

Tab.defaultProps = {};

export default Tab;
