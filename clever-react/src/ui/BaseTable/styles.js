/* eslint-disable no-tabs */
import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing, typography } }) => {
  const { sizing } = typography;

  return ({
    paper: {
      overflow: "hidden",
      overflowX: "auto",
      boxShadow: "none",

      "& table": { ...spacing["w-100"] },
      "&::-webkit-scrollbar": {
        width: "12px",
        backgroundColor: "#fff",
      },
      "&::-webkit-scrollbar-track": { backgroundColor: "#fff" },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#babac0",
        borderRadius: "16px",
        border: "4px solid #fff",
      },
    },
    fillAvailable: {
      fallbacks: [
        { width: "-moz-available" },
        { width: "-webkit-fill-available" },
      ],
    },
    results: { ...spacing["mt-6"], fontSize: "1rem" },
    entriesSelect: { ...spacing["mx-3"] },
    entriesWrapper: {
      ...spacing["mb-4"],
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
    },
    selectRoot: {
      ...spacing["py-0"],
      fontFamily: typography.font1,
      color: typography.color,
      fontSize: sizing.body0,
      "& p": { fontWeight: "bold" },
      "&:focus": { backgroundColor: "transparent" },
    },
    selectIcon: {
      top: "auto",
      height: "10px",
      transform: "rotate(90deg)",
      "& path": { fill: `${colors.neutral[900]} !important` },
    },
    inputBase: { height: "30px" },
    labelRoot: {
      fontFamily: typography.font1,
      fontWeight: "bold",
      fontSize: sizing.body0,
      color: colors.neutral[700],
      marginBottom: "-10px",
      lineHeight: 1,
    },
    arrow: ({ sortingIconsColors: { defaultColor } }) => ({ "& path": { fill: colors[defaultColor.color][defaultColor.shade] } }),
    activeArrow: ({ sortingIconsColors: { activeArrow } }) => ({
      "& path": { fill: colors[activeArrow.color][activeArrow.shade] },
    }),
    sortIcons: { ...spacing["ml-2"], width: "20px" },
    bodyCell: {
      paddingRight: "2.85rem",
      ...spacing["pl-5"],
      "&:first-child": { ...spacing["pl-7"] },
      "&:last-child": { ...spacing["pr-7"] },
    },
    headerRow: { "& th": { height: "45px" } },
    headerCell: {
      whiteSpace: "nowrap",
      textAlign: "left",
      ...spacing["px-2"],
      "&:first-child": { ...spacing["pl-7"] },
      "&:last-child": { ...spacing["pr-5"] },
    },
    headerSelectionIcon: { color: colors.primary[300] },
    selectionIcon: { color: colors.primary[500] },
    scrollBar: {
      background: `
      /* Shadow covers */
		  linear-gradient(90deg, white 30%, rgba(255,255,255,0)),
		  linear-gradient(90deg, rgba(255,255,255,0), white 70%) 100% 0,
		
		  /* Shadows */
		  radial-gradient(farthest-side at 0 50%, rgba(0,0,0,.2), rgba(0,0,0,0)),
      radial-gradient(farthest-side at 100% 50%, rgba(0,0,0,.2), rgba(0,0,0,0)) 100% 0
    `,
      backgroundRepeat: "no-repeat",
      backgroundColor: "white",
      backgroundSize: "40px 100%, 40px 100%, 14px 100%, 14px 100%",
      backgroundAttachment: "local, local, scroll, scroll",
    },
    newRowIcon: {
      ...spacing["m-0"],
      ...spacing["ml-1"],
      ...spacing["mr-2"],
      marginBottom: "-0.1rem",
    },
    loading: { display: "flex" },
  });
};

export default makeStyles(styles);
