import makeStyles from "../../styles/makeStyles";

const styles = ({ cleverUI: { spacing } }) => ({
  tablePagination: {
    ...spacing["mt-5"],
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  prevButtonWrapper: { ...spacing["mr-4"] },
  nextButtonWrapper: { ...spacing["ml-4"] },
  pageButton: { ...spacing["mx-2"] },
  selectedPage: { fontSize: "1.25rem" },
});

export default makeStyles(styles);
