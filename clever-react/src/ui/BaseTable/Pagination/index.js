/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/no-multi-comp */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { usePagination } from "@material-ui/lab/Pagination";

import Link from "../../Link";

import getClasses from "./styles";

// const PageButtonComponent = props => <button {...props}>{props.children}</button>;

const Pagination = props => {
  const classes = getClasses();

  const { pages, onPageChange, previousText, nextText, page: pageProp } = props;

  const { items } = usePagination({
    count: pages,
    onChange: (event, page) => onPageChange(page - 1),
    page: pageProp + 1,
  });

  // const visiblePages = getVisiblePages(null, pages);
  // const activePage = page + 1;

  return (
    <>
      <div className={classes.tablePagination}>
        {items.map(({ page, type, selected, ...item }, index) => {
          let children = null;

          if (type === "start-ellipsis" || type === "end-ellipsis") {
            children = "…";
          } else if (type === "page") {
            children = (
              <Link
                className={selected
                  ? classnames(classes.pageButton, classes.selectedPage)
                  : classes.pageButton}
                {...item}
              >
                {page}
              </Link>
            );
          } else {
            children = (
              <>
                {type === "next" && (
                  <Link {...item} className={classes.nextButtonWrapper}>
                    {nextText}
                  </Link>
                )}

                {type === "previous" && (
                  <Link {...item} className={classes.prevButtonWrapper}>
                    {previousText}
                  </Link>
                )}
              </>
            );
          }

          return <span key={index}>{children}</span>;
        })}
      </div>
    </>

  );

  /*
    return (
      <div className={classes.tablePagination}>
        <div className={classes.prevButtonWrapper}>
          <Link
            className="Table__pageButton"
            onClick={() => {
              if (activePage === 1) return;
              changePage(page - 1);
            }}
            disabled={activePage === 1}
          >
            {previousText}
          </Link>
        </div>
        <div className="Table__visiblePagesWrapper">
          {visiblePages.map((p, index, array) => (
            <Link
              key={p}
              disabled={activePage !== p}
              className={classes.pageButton}
              onClick={() => onPageChange(p - 1)}
            >
              {array[index - 1] + 2 < p ? `...${p}` : p}
            </Link>
          ))}
        </div>

        <div className={classes.nextButtonWrapper}>
          <Link
            className="Table__pageButton"
            onClick={() => {
              if (activePage === pages) return;
              changePage(page + 1);
            }}
            disabled={activePage === pages}>
            {nextText}
          </Link>
        </div>
      </div>
    ); */
};

Pagination.propTypes = {
  pages: PropTypes.number,
  page: PropTypes.number,
  onPageChange: PropTypes.func,
  previousText: PropTypes.string,
  nextText: PropTypes.string,
};

Pagination.defaultProps = {
  pages: 1,
  page: 0,
  onPageChange: () => { },
  previousText: "Previous",
  nextText: "Next",
};

export default Pagination;
