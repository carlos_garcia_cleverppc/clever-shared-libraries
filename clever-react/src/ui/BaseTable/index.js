/* eslint-disable max-len */
import React, { useEffect, useRef, forwardRef, useImperativeHandle } from "react";
import { useTable, usePagination, useSortBy, useRowSelect, useGlobalFilter } from "react-table";
import map from "lodash-es/map";
import split from "lodash-es/split";
import replace from "lodash-es/replace";
import merge from "lodash-es/merge";
import classnames from "classnames";
import PropTypes from "prop-types";
import CircleIcon from "@material-ui/icons/RadioButtonUnchecked";
import CheckCircleIcon from "@material-ui/icons/RadioButtonChecked";
import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";

import Link from "../Link";
import Loading from "../Loading";
import Typography from "../Typography";
import MenuItem from "../Menu/Item";
import UpArrow from "../Icon/UpArrow";
import Plus from "../Icon/Plus";
import DownArrow from "../Icon/DownArrow";
import ChevronRight from "../Icon/ChevronRight";

import Pagination from "./Pagination";

import getClasses from "./styles";

function printHeaderDefault(value) {
  return value;
}

function printBodyDefault(value) {
  return value;
}

function rowFilterDefault(rows) {
  return rows;
}

// When the useGlobalFilter hook is used, the rows, rowsById and flatRows properties of instance (the object we obtain with the useTable hook)
// are replaced with the filtered data.

// With this hook those properties remains unchanged so, for example, it is possible to deselect all rows
// no matter what filter is applied (For instance, toggleAllRowsSelected is applied to the rows property, that's why this change is needed)
function useCorrectRowDataInstance(hooks) {
  function useInstance(instance) {
    Object.assign(instance, {
      rows: instance.preGlobalFilteredRows,
      rowsById: instance.preGlobalFilteredRowsById,
      flatRows: instance.preGlobalFilteredFlatRows,
    });
  }

  hooks.useInstance.push(useInstance);
}

const TableBase = forwardRef((props, ref) => {
  const { headers, data, sortingIconsColors, showEntries } = props;
  const { pagination, entriesNumber, entriesOptions, entriesText } = props;
  const { previousText, nextText, resultsText, printBody } = props;
  const { selection, selectionCallback, sortCallback, fullWidth, disableSortRemove } = props;
  const { autoResetSortBy, defaultSorting, rowFilterCallback, autoResetPage, filterDataCallback } = props;
  const { enableSelectAllRows, createRow } = props;

  const classes = getClasses({ sortingIconsColors });

  let customColumns = {};

  headers.forEach(header => {
    customColumns = { ...customColumns, [header.accessor]: !!header.custom };
  });
  headers.forEach((header, idx) => {
    headers[idx] = { sortType: "basic", ...headers[idx] };
  });

  const newClasses = props.classes;

  const didMount = useRef(false);

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    pageCount,
    gotoPage,
    setPageSize,
    selectedFlatRows,
    toggleAllRowsSelected,
    toggleAllPageRowsSelected,
    state: { pageIndex, pageSize, sortBy },
    globalFilteredRows,
    sortedRows,
    setGlobalFilter,
  } = useTable(
    {
      columns: headers,
      data,
      PaginationComponent: Pagination,
      initialState: {
        pageIndex: 0,
        pageSize: entriesNumber,
        sortBy: defaultSorting,
        // If state.globalFilter is undefined, the function rowFilterCallback will never be executed
        globalFilter: {},
      },
      autoResetSelectedRows: false,
      disableSortRemove,
      autoResetSortBy,
      globalFilter: rowFilterCallback,
      autoResetPage,
      // manualPagination: true,
      // manualSortBy: !!sortCallback,
    },
    useGlobalFilter,
    useSortBy,
    usePagination,
    useCorrectRowDataInstance,
    selection && useRowSelect,
    hooks => {
      if (selection) {
        hooks.visibleColumns.push(columns => [
          // Let's make a column for selection
          {
            id: "selection",
            // The header can use the table's getToggleAllRowsSelectedProps method
            // to render a checkbox
            Header: ({ getToggleAllPageRowsSelectedProps, getToggleAllRowsSelectedProps }) => {
              const selectFunctionProps = enableSelectAllRows
                ? getToggleAllRowsSelectedProps
                : getToggleAllPageRowsSelectedProps;

              return (
                <div>
                  <Checkbox
                    icon={<CircleIcon className={newClasses.headerSelectionIcon} />}
                    checkedIcon={<CheckCircleIcon className={newClasses.headerSelectionIcon} />}
                    {...selectFunctionProps()}
                  />
                </div>
              );
            },
            // The cell can use the individual row's getToggleRowSelectedProps method
            // to the render a checkbox
            Cell: ({ row }) => (
              <div>
                <Checkbox
                  icon={<CircleIcon className={classes.selectionIcon} />}
                  checkedIcon={<CheckCircleIcon className={classes.selectionIcon} />}
                  {...row.getToggleRowSelectedProps()}
                />
              </div>
            ),
          },
          ...columns,
        ]);
      }
    },
  );

  // Force an update when the filterCallback changes to put reset the pagination
  useEffect(() => {
    setGlobalFilter([]);
  }, [setGlobalFilter, rowFilterCallback]);

  useEffect(() => {
    filterDataCallback([...globalFilteredRows]);
  }, [filterDataCallback, globalFilteredRows]);

  useEffect(() => {
    if (didMount.current) selectionCallback(selectedFlatRows);
    else didMount.current = true;
  }, [selectedFlatRows, selectionCallback]);

  useEffect(() => {
    if (!!sortCallback && !!sortBy[0]) sortCallback(sortBy[0].id, sortBy[0].desc);
  }, [sortCallback, sortBy]);

  const getSortIcon = column => {
    if (column.isSorted) {
      if (column.isSortedDesc) {
        return (
          <span className={classes.sortIcons}>
            <UpArrow className={classes.arrow} size={10} />

            <DownArrow className={classnames(classes.arrow, classes.activeArrow)} size={10} />
          </span>
        );
      }
      return (
        <span className={classes.sortIcons}>
          <UpArrow className={classnames(classes.arrow, classes.activeArrow)} size={10} />

          <DownArrow className={classes.arrow} size={10} />
        </span>
      );
    }
    return (
      <span className={classes.sortIcons}>
        <UpArrow className={classes.arrow} size={10} />

        <DownArrow className={classes.arrow} size={10} />
      </span>
    );
  };

  const entries = pagination ? page : sortedRows;

  useImperativeHandle(ref, () => ({ // methods that can be used in parent
    checkAllRows(mode) { // true => checkAll, false => uncheckAll
      toggleAllRowsSelected(mode);
    },
    checkAllPageRows(mode) { // true => checkAll, false => uncheckAll
      toggleAllPageRowsSelected(mode);
    },
  }));

  const isClickable = target => ["BUTTON", "A", "INPUT", "TEXTAREA", "SUMMARY", "SELECT", "OPTION"].includes(target.tagName);
  const hasTextSelected = () => {
    const cellText = document.getSelection();
    return cellText.type === "Range";
  };

  return (
    <div style={{ display: fullWidth ? "block" : "flex" }}>
      <div>
        {((pagination && showEntries) || showEntries) && (
          <div className={classes.entriesWrapper}>
            <Typography>{split(entriesText, "--entries--")[0]}</Typography>

            <TextField
              value={pageSize}
              onChange={e => {
                setPageSize(Number(e.target.value));
              }}
              InputProps={{ classes: { root: classes.inputBase } }}
              InputLabelProps={{ classes: { root: classes.labelRoot } }}
              variant="outlined"
              SelectProps={{
                classes: { icon: classes.selectIcon, root: classes.selectRoot },
                MenuProps: { classes: { paper: classes.scroll } },
                IconComponent: ChevronRight,
              }}
              className={classes.entriesSelect}
              select
            >
              {map(entriesOptions, option => (
                <MenuItem className={classes.menuItem} key={`entries-${option}`} value={option}>
                  {option}
                </MenuItem>
              ))}
            </TextField>

            <Typography>{split(entriesText, "--entries--")[1]}</Typography>
          </div>
        )}

        <Paper className={!!newClasses && newClasses.paper ? classnames(classes.paper, newClasses.paper, classes.scrollBar) : classnames(classes.paper, classes.scrollBar)} elevation={0}>
          <table {...getTableProps()} className={!!newClasses && newClasses.table ? newClasses.table : ""}>
            <thead className={!!newClasses && newClasses.header ? newClasses.header : ""}>
              {headerGroups.map(headerGroup => (
                <tr {...headerGroup.getHeaderGroupProps()} className={classes.headerRow}>
                  {headerGroup.headers.map(column => (
                    <th
                      {...merge(column.getHeaderProps(column.getSortByToggleProps()),
                        {
                          style: {
                            width: column.fixedWidth ?? "",
                            textAlign: column.align ?? "",
                          },
                          className: classes.headerCell,
                        })}
                    >
                      {column.render("Header")}

                      {(column.id !== "selection" && !column.hideSort) && getSortIcon(column)}
                    </th>
                  ))}
                </tr>
              ))}
            </thead>

            <tbody {...getTableBodyProps()} className={!!newClasses && !!newClasses.tableBody ? newClasses.tableBody : ""}>
              {createRow.callback && (
                <tr className={!!newClasses && !!newClasses.row ? newClasses.row : ""}>
                  <td
                    colSpan="20"
                    style={{
                      width: "",
                      overflowWrap: "",
                      textAlign: "",
                    }}
                    className={classnames({ [classes.bodyCell]: true, [newClasses.cell]: !!newClasses?.cell })}
                  >
                    <Link onClick={createRow.callback} disabled={createRow.loading} className={classes.loading}>
                      {createRow.loading && <Loading size={14} color="primary" className={classes.newRowIcon} />}

                      <Typography color="primary" variant="f1-14">
                        {!createRow.loading && <Plus size={14} color="primary" className={classes.newRowIcon} />}

                        {createRow.label}
                      </Typography>
                    </Link>
                  </td>
                </tr>
              )}

              {entries.map(row => {
                prepareRow(row);

                const getCellValue = cell => {
                  if (customColumns[cell.column.id]) {
                    return printBody(cell.value, cell.column.id, cell.row.id);
                  } if (typeof (cell.value) === "number") {
                    return (<Typography align="right">{cell.render("Cell")}</Typography>);
                  } if (typeof (cell.value) === "string") {
                    return (<Typography align="left">{cell.render("Cell")}</Typography>);
                  }
                  return cell.render("Cell");
                };

                const handleClick = ev => {
                  if (isClickable(ev.target)) return;
                  if (hasTextSelected()) return;

                  row.toggleRowSelected();
                };

                return (
                  <tr
                    {...row.getRowProps()}
                    className={!!newClasses && !!newClasses.row ? newClasses.row : ""}
                    {...(selection && { onClick: handleClick })}
                  >

                    {row.cells.map(cell => (
                      <td
                        {...cell.getCellProps({
                          style: {
                            width: cell.column.fixedWidth ?? "",
                            overflowWrap: cell.column.fixedWidth ? "anywhere" : "",
                            textAlign: cell.column.align ?? "",
                          },
                          className: classnames({ [classes.bodyCell]: true, [newClasses.cell]: !!newClasses?.cell }),
                        })}
                      >
                        {getCellValue(cell)}
                      </td>
                    ))}
                  </tr>
                );
              })}
            </tbody>
          </table>
        </Paper>

        {pagination && (
          <>
            <Typography className={classes.results} align="center">
              {replace(replace(resultsText, "--total--", globalFilteredRows.length), "--number--", entries.length)}
            </Typography>

            <Pagination
              pages={pageCount}
              page={pageIndex}
              onPageChange={gotoPage}
              previousText={previousText}
              nextText={nextText}
            />
          </>
        )}
      </div>
    </div>
  );
});

TableBase.propTypes = {
  headers: PropTypes.instanceOf(Array).isRequired,
  data: PropTypes.instanceOf(Array).isRequired,
  customColumns: PropTypes.instanceOf(Array),
  sortingIconsColors: PropTypes.instanceOf(Object),
  pagination: PropTypes.bool,
  fullWidth: PropTypes.bool,
  showEntries: PropTypes.bool,
  entriesNumber: PropTypes.number,
  entriesOptions: PropTypes.instanceOf(Array),
  entriesText: PropTypes.string, // Text in select
  resultsText: PropTypes.string, // Text below the table
  previousText: PropTypes.string,
  nextText: PropTypes.string,
  printHeader: PropTypes.func,
  printBody: PropTypes.func,
  selectionCallback: PropTypes.func,
  selectAllRows: PropTypes.func,
  selectAllPages: PropTypes.func,
  sortCallback: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  disableSortRemove: PropTypes.bool,
  autoResetSortBy: PropTypes.bool,
  defaultSorting: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string.isRequired,
    desc: PropTypes.bool,
  })),
  rowFilterCallback: PropTypes.func,
  autoResetPage: PropTypes.bool,
  filterDataCallback: PropTypes.func,
  enableSelectAllRows: PropTypes.bool,
  createRow: PropTypes.shape({
    callback: PropTypes.func,
    label: PropTypes.string,
    loading: PropTypes.bool,
  }),
};

TableBase.defaultProps = {
  customColumns: [],
  sortingIconsColors: {
    defaultColor: { color: "neutral", shade: 500 },
    activeArrow: { color: "primary", shade: 900 },
  },
  pagination: true,
  showEntries: true,
  entriesNumber: 10,
  entriesOptions: [10, 20, 30, 40, 50],
  previousText: "Previous",
  nextText: "Next",
  entriesText: "Show --entries-- entries",
  resultsText: "Showing --number-- of --total-- results",
  printHeader: printHeaderDefault,
  printBody: printBodyDefault,
  selectionCallback: () => { },
  selectAllRows: () => { },
  selectAllPages: () => { },
  sortCallback: false,
  fullWidth: false,
  disableSortRemove: false,
  autoResetSortBy: true,
  defaultSorting: [],
  rowFilterCallback: rowFilterDefault,
  autoResetPage: true,
  filterDataCallback: () => { },
  enableSelectAllRows: false,
  createRow: {
    callback: undefined,
    label: "New row",
    loading: false,
  },
};

export default TableBase;
