import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  root: {
    width: "100%",
    display: "inline-flex",
  },
  barWrapper: {
    ...spacing["mr-4"],
    ...spacing["px-2"],
    height: "40px",
    width: "100%",
    borderRadius: "20px",
    backgroundColor: colors.neutral[100],
    display: "inline-flex",
    justifyContent: "space-between",
    alignItems: "center",
    position: "relative",
  },
  progressWrapper: ({ percentage }) => ({
    height: "100%",
    width: `${percentage}%`,
    backgroundColor: colors.success[100],
    borderRadius: "20px",
    position: "absolute",
    left: "0px",
  }),
  statusWrapper: { display: "flex", zIndex: 1 },
  roundStatusWrapper: {
    height: "32px",
    width: "32px",
    borderRadius: "100px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  finalState: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  warning: { backgroundColor: colors.warning[500] },
  warningFocused: { backgroundColor: colors.warning[700] },
  pending: { backgroundColor: colors.neutral[300] },
  pendingFocused: { backgroundColor: colors.neutral[500] },
  starWrapper: {
    height: "40px",
    width: "40px",
    borderRadius: "100px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  star: { backgroundColor: colors.yellow[100] },
  click: { cursor: "pointer" },
});

export default makeStyles(styles);
