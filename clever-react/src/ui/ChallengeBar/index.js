/* eslint-disable max-len */
import React from "react";
import PropTypes from "prop-types";
import findLastIndex from "lodash-es/findLastIndex";
import classnames from "classnames";

import Check from "../Icon/Check";
import Warning from "../Icon/Warning";
import Star from "../Icon/Star";
import Typography from "../Typography";

import getClasses from "./styles";

const ChallengeBar = props => {
  const { className, statuses, onClickStatus, positionFocused } = props;

  const numberStatuses = statuses.length;
  const lastCompleted = findLastIndex(statuses, val => val.toUpperCase() === "COMPLETED");

  const percentage = lastCompleted >= 0 ? ((lastCompleted + 1) / numberStatuses) * 100 : 0;

  const onClickTask = index => {
    onClickStatus(index);
  };

  const classes = getClasses({ percentage });

  return (
    <>
      {numberStatuses > 0 && (
        <div className={classnames(className, classes.root)}>
          <div className={classes.barWrapper}>
            <div className={classes.progressWrapper} />

            {statuses.map((status, index) => (
              <div
                className={classes.statusWrapper}
                key={`${index}-${status}`}
              >
                <div
                  className={classes.click}
                  onClick={() => onClickTask(index)}
                  onKeyDown={() => onClickTask(index)}
                  role="button"
                  tabIndex={0}
                >
                  {status.toUpperCase() === "COMPLETED" && (
                    <Check size={32} color="success" shade={positionFocused === index ? 700 : 500} />
                  )}

                  {status.toUpperCase() === "FAILED" && (
                    <div
                      className={
                        classnames(classes.roundStatusWrapper, positionFocused === index ? classes.warningFocused : classes.warning)
                      }
                    >
                      <Warning size={24} color="white" />
                    </div>
                  )}

                  {status.toUpperCase() === "PENDING" && (
                    <div className={
                      classnames(classes.roundStatusWrapper, positionFocused === index ? classes.pendingFocused : classes.pending)
                    }
                    >
                      <Typography variant="f1-14" weight="bold" color="white">{index + 1}</Typography>
                    </div>
                  )}
                </div>
              </div>
            ))}
          </div>

          <div className={classes.finalState}>
            <div className={classnames(classes.starWrapper, percentage === 100 ? classes.star : classes.pending)}>
              <Star size={20} color={percentage === 100 ? "yellow" : "white"} shade={300} />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

ChallengeBar.propTypes = {
  className: PropTypes.string,
  statuses: PropTypes.arrayOf(PropTypes.string),
};

ChallengeBar.defaultProps = {
  className: "",
  statuses: [],
};

export default ChallengeBar;
