/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Radio(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M4.591 4.644a.6.6 0 1 0-.849-.848 5.72 5.72 0 0 0 0 8.088.6.6 0 1 0 .85-.848 4.52 4.52 0 0 1 0-6.392zM11.83 3.796a.6.6 0 1 0-.848.848 4.52 4.52 0 0 1 0 6.392.6.6 0 1 0 .849.848 5.72 5.72 0 0 0 0-8.088z"
        fill={colorIcon}
      />
      <path
        d="M6.04 6.088a.6.6 0 1 0-.848-.849 3.672 3.672 0 0 0 0 5.196.6.6 0 1 0 .848-.849 2.472 2.472 0 0 1 0-3.498zM10.382 5.245a.6.6 0 0 0-.848.849 2.472 2.472 0 0 1 0 3.497.6.6 0 1 0 .848.85 3.672 3.672 0 0 0 0-5.196z"
        fill={colorIcon}
      />
      <path
        d="M7.787 6.216a1.624 1.624 0 1 0 0 3.248 1.624 1.624 0 0 0 0-3.248zM7.363 7.84a.424.424 0 1 1 .848 0 .424.424 0 0 1-.848 0z"
        fill={colorIcon}
        fillRule="evenodd"
        clipRule="evenodd"
      />
    </svg>
  );
}

Radio.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Radio.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Radio;
