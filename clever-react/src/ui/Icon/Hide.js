/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Hide(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M7.152 3.266c.28-.043.564-.065.847-.064 2.543 0 4.934 1.83 6.325 4.797-.213.452-.453.89-.72 1.312a.8.8 0 1 0 1.36.84c.372-.586.696-1.201.967-1.84a.8.8 0 0 0 0-.632C14.316 3.93 11.278 1.602 8 1.602a6.22 6.22 0 0 0-1.12.096.811.811 0 1 0 .273 1.6v-.032zM1.37.236A.803.803 0 1 0 .235 1.37l2.479 2.47A11.69 11.69 0 0 0 .067 7.68a.8.8 0 0 0 0 .64C1.682 12.07 4.721 14.396 8 14.396a7.405 7.405 0 0 0 4.038-1.231l2.591 2.599a.8.8 0 0 0 1.136 0 .8.8 0 0 0 0-1.136L1.37.235zm5.085 7.348 1.96 1.959a1.6 1.6 0 0 1-1.959-1.959zM8 12.797c-2.542 0-4.933-1.831-6.317-4.798a9.667 9.667 0 0 1 2.16-3.03L5.256 6.4a3.198 3.198 0 0 0 4.342 4.342l1.27 1.255c-.87.513-1.86.789-2.87.8z"
        fill={colorIcon}
      />
    </svg>
  );
}

Hide.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Hide.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Hide;
