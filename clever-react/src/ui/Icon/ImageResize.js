/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function ImageResize(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 15"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M8.727 6.545h-8A.727.727 0 0 0 0 7.272v6.546c0 .402.326.727.727.727h8a.727.727 0 0 0 .728-.727V7.272a.727.727 0 0 0-.728-.727zm-4.043 6.546 1.403-1.404a.218.218 0 0 1 .364 0l1.396 1.404H4.684zM8 11.185l-.524-.516a1.753 1.753 0 0 0-2.421 0l-2.43 2.422h-1.17V8H8v3.185zM.727 1.535a.727.727 0 0 0 .626-.357.764.764 0 0 0-.088-.979.727.727 0 0 0-.538-.2A.727.727 0 0 0 0 .728V.8a.727.727 0 0 0 .727.734zm5.753-.08h.138a.727.727 0 1 0 0-1.455H6.48a.727.727 0 1 0 0 1.454zM.727 4.93a.727.727 0 0 0 .728-.728v-.16a.727.727 0 1 0-1.455 0v.16c0 .402.326.728.727.728zM9.52 0h-.138a.727.727 0 0 0 0 1.454h.138A.727.727 0 0 0 9.52 0zM3.57 1.454h.14A.727.727 0 1 0 3.71 0h-.14a.727.727 0 0 0 0 1.454zM15.274 3.2a.727.727 0 0 0-.728.727v.153a.727.727 0 0 0 1.455 0v-.153a.727.727 0 0 0-.727-.727zm-3.55 9.89h-.174a.727.727 0 1 0 0 1.455h.175a.727.727 0 1 0 0-1.454zM15.2 0a.727.727 0 0 0-.655.727c.002.106.027.21.073.305A.727.727 0 0 0 16 .807v-.08A.793.793 0 0 0 15.2 0zm.073 6.472a.727.727 0 0 0-.728.728v.16a.727.727 0 0 0 1.455 0V7.2a.727.727 0 0 0-.727-.728zM12.429 0h-.138a.727.727 0 1 0 0 1.454h.138a.727.727 0 0 0 0-1.454zm2.844 13.09a.675.675 0 0 0-.32.08.727.727 0 0 0-.408.648c0 .402.326.727.728.727a.793.793 0 0 0 .727-.8.727.727 0 0 0-.727-.654zm0-3.316a.727.727 0 0 0-.728.728v.16a.727.727 0 0 0 1.455 0v-.19a.727.727 0 0 0-.727-.727v.03z"
        fill={colorIcon}
      />
    </svg>
  );
}

ImageResize.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

ImageResize.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default ImageResize;
