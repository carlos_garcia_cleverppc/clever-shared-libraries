/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Alert(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 24 24"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M11.148 16.452A1.2 1.2 0 0 0 13.2 15.6a1.26 1.26 0 0 0-.348-.852 1.2 1.2 0 0 0-1.308-.252 1.2 1.2 0 0 0-.396.252 1.26 1.26 0 0 0-.348.852 1.2 1.2 0 0 0 .348.852zm10.344-.24h-5.436a1.2 1.2 0 0 0 0 2.4h2.88A9.6 9.6 0 0 1 2.4 12 1.2 1.2 0 0 0 0 12a12 12 0 0 0 20.256 8.676V22.8a1.2 1.2 0 0 0 2.4 0v-5.4a1.2 1.2 0 0 0-1.164-1.188zM12 0a12 12 0 0 0-8.256 3.324V1.2a1.2 1.2 0 0 0-2.4 0v5.4a1.2 1.2 0 0 0 1.2 1.2h5.4a1.2 1.2 0 0 0 0-2.4h-2.88A9.6 9.6 0 0 1 21.6 12a1.2 1.2 0 0 0 2.4 0A12 12 0 0 0 12 0zm0 13.2a1.2 1.2 0 0 0 1.2-1.2V8.4a1.2 1.2 0 0 0-2.4 0V12a1.2 1.2 0 0 0 1.2 1.2z"
        fill={colorIcon}
      />
    </svg>
  );
}

Alert.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Alert.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Alert;
