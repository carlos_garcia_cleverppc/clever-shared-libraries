/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Trophy(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M7.333 13.333v-1.386a4 4 0 0 1-2.826-2 2.68 2.68 0 0 1-3.174-2.614V4c0-.733.6-1.333 1.334-1.333H4c0-.734.6-1.334 1.333-1.334h5.334c.736 0 1.333.597 1.333 1.334h1.333c.737 0 1.334.597 1.334 1.333v3.333a2.666 2.666 0 0 1-3.174 2.62 4 4 0 0 1-2.826 1.994v1.386h2.666a.667.667 0 0 1 0 1.334H4.667a.667.667 0 0 1 0-1.334h2.666zm4.614-4.666H12c.736 0 1.333-.597 1.333-1.334V4H12v4c0 .227-.02.447-.053.667zm-7.894 0A4.027 4.027 0 0 1 4 8V4H2.667v3.333a1.333 1.333 0 0 0 1.386 1.334zm1.28-6V8a2.667 2.667 0 1 0 5.334 0V2.667H5.333z"
        fill={colorIcon}
      />
    </svg>
  );
}

Trophy.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Trophy.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Trophy;
