/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Plug(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 13 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M11.74 3.2H9.39V.8c0-.442-.35-.8-.782-.8a.791.791 0 0 0-.783.8v2.4h-3.13V.8c0-.442-.35-.8-.783-.8a.791.791 0 0 0-.783.8v2.4H.783A.791.791 0 0 0 0 4c0 .442.35.8.783.8h.782v4a.81.81 0 0 0 .227.568l2.121 2.16V15.2c0 .442.35.8.783.8a.791.791 0 0 0 .782-.8V12h1.565v3.2c0 .442.35.8.783.8a.791.791 0 0 0 .783-.8v-3.672l2.12-2.16a.81.81 0 0 0 .228-.568v-4h.782a.791.791 0 0 0 .783-.8c0-.442-.35-.8-.783-.8zM9.39 8.472 7.505 10.4H5.017L3.13 8.472V4.8h6.261v3.672zM5.478 8.8h1.565a.791.791 0 0 0 .783-.8c0-.442-.35-.8-.783-.8H5.478a.791.791 0 0 0-.782.8c0 .442.35.8.782.8z"
        fill={colorIcon}
      />
    </svg>
  );
}

Plug.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Plug.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Plug;
