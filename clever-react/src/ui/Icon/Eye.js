/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Eye(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M7.983 5.41a2.586 2.586 0 1 0 0 5.173 2.586 2.586 0 0 0 0-5.173zM6.596 7.997a1.386 1.386 0 1 1 2.773 0 1.386 1.386 0 0 1-2.773 0z"
        fill={colorIcon}
        fillRule="evenodd"
        clipRule="evenodd"
      />
      <path
        d="m15.891 7.683-.626.314.626.313v.002l-.003.005-.008.015a5.147 5.147 0 0 1-.127.235 13.725 13.725 0 0 1-1.88 2.556c-1.3 1.387-3.294 2.87-5.89 2.87-2.597 0-4.59-1.482-5.89-2.87a13.73 13.73 0 0 1-1.88-2.556 8.235 8.235 0 0 1-.128-.235l-.008-.015-.002-.005v-.001S.073 8.31.7 7.997l-.626-.314.001-.002.002-.005.008-.015a4.97 4.97 0 0 1 .128-.235 13.725 13.725 0 0 1 1.88-2.556C3.393 3.483 5.386 2 7.983 2c2.596 0 4.59 1.483 5.89 2.87a13.721 13.721 0 0 1 1.88 2.556 7.962 7.962 0 0 1 .127.235l.008.015.002.005.001.002zm-14.117.761a11.323 11.323 0 0 1-.275-.447 12.327 12.327 0 0 1 1.615-2.17C4.297 4.566 5.945 3.4 7.983 3.4c2.037 0 3.686 1.166 4.868 2.427a12.332 12.332 0 0 1 1.615 2.17 12.333 12.333 0 0 1-1.615 2.17c-1.182 1.26-2.83 2.426-4.868 2.426-2.038 0-3.686-1.165-4.869-2.427a12.328 12.328 0 0 1-1.34-1.722z"
        fill={colorIcon}
        fillRule="evenodd"
        clipRule="evenodd"
      />
      <path
        d="m15.265 7.997.626-.314a.702.702 0 0 1 0 .627l-.626-.313zM.074 7.683l.626.314-.626.313a.701.701 0 0 1 0-.627z"
        fill={colorIcon}
      />
    </svg>
  );
}

Eye.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Eye.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Eye;
