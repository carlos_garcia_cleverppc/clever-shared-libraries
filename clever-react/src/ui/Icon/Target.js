/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Target(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M8 5.923a2.077 2.077 0 1 0 0 4.154 2.077 2.077 0 0 0 0-4.154zM7.123 8a.877.877 0 1 1 1.754 0 .877.877 0 0 1-1.754 0z"
        fill={colorIcon}
        fillRule="evenodd"
        clipRule="evenodd"
      />
      <path
        d="M2.97 8a5.03 5.03 0 1 1 10.06 0A5.03 5.03 0 0 1 2.97 8zM8 4.17a3.83 3.83 0 1 0 0 7.66 3.83 3.83 0 0 0 0-7.66z"
        fill={colorIcon}
        fillRule="evenodd"
        clipRule="evenodd"
      />
      <path
        d="M.015 8a7.985 7.985 0 1 1 15.97 0A7.985 7.985 0 0 1 .015 8zM8 1.215a6.785 6.785 0 1 0 0 13.57 6.785 6.785 0 0 0 0-13.57z"
        fill={colorIcon}
        fillRule="evenodd"
        clipRule="evenodd"
      />
    </svg>
  );
}

Target.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Target.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Target;
