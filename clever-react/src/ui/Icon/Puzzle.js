/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Puzzle(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M11.333 14.667A1.333 1.333 0 0 1 10 13.333v-.666a.667.667 0 1 0-1.333 0v.666c0 .737-.597 1.334-1.334 1.334h-2A1.333 1.333 0 0 1 4 13.333v-2h-.667a2 2 0 0 1 0-4H4v-2C4 4.593 4.6 4 5.333 4h2v-.667a2 2 0 1 1 4 0V4h2c.737 0 1.334.597 1.334 1.333v2c0 .737-.597 1.334-1.334 1.334h-.666a.667.667 0 0 0 0 1.333h.666c.737 0 1.334.597 1.334 1.333v2c0 .737-.597 1.334-1.334 1.334h-2zm2-1.334v-2h-.666a2 2 0 0 1 0-4h.666v-2h-2A1.333 1.333 0 0 1 10 4v-.667a.667.667 0 0 0-1.333 0V4c0 .736-.597 1.333-1.334 1.333h-2v2c0 .737-.597 1.334-1.333 1.334h-.667a.667.667 0 0 0 0 1.333H4c.736 0 1.333.597 1.333 1.333v2h2v-.666a2 2 0 1 1 4 0v.666h2z"
        fill={colorIcon}
      />
    </svg>
  );
}

Puzzle.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Puzzle.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Puzzle;
