/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Budget(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M15.978 8.356c-.058-.43-.485-.772-1.284-1.023-.647-.202-1.466-.324-2.358-.343V3.468a.315.315 0 0 0-.067-.198c-.105-.382-.532-.692-1.271-.923C10.281 2.122 9.344 2 8.36 2c-.981 0-1.908.122-2.61.347-.576.184-1.267.527-1.267 1.118 0 .036.003.073.01.11l.034 1.504C2.46 4.666.462 6.075.067 8.22.022 8.461 0 8.71 0 8.956c0 2.178 1.71 3.95 3.814 3.95.876 0 1.723-.313 2.405-.884.599.145 1.3.224 2.042.231.163.914 2.074 1.333 3.859 1.333.98 0 1.905-.125 2.612-.353.58-.185 1.268-.531 1.268-1.122V8.484a.353.353 0 0 0-.022-.128zM5.942 2.976c.643-.204 1.504-.32 2.421-.32.911 0 1.803.116 2.453.32.692.219.841.443.841.49 0 .045-.15.273-.844.488-.647.2-1.539.32-2.45.32-.88 0-1.714-.106-2.348-.297-.685-.205-.876-.433-.892-.505 0-.007 0-.01-.003-.01.003-.046.15-.27.822-.485zm5.757 1.314v.845a.274.274 0 0 0-.041.158c0 .043-.15.271-.845.489-.647.201-1.542.32-2.453.32-.787 0-1.558-.086-2.17-.248l-.025-.006a3.73 3.73 0 0 0-.997-.584l-.023-.93c.22.115.45.207.689.273.69.208 1.586.32 2.526.32.984 0 1.921-.122 2.635-.346.245-.07.48-.169.704-.29zM5.95 11.388a3.098 3.098 0 0 1-4.485-.217c-1.18-1.343-1.087-3.426.21-4.647a3.121 3.121 0 0 1 2.138-.858c.318 0 .637.05.943.145 1.102.36 1.934 1.307 2.173 2.479.04.218.063.442.063.663 0 .93-.379 1.815-1.042 2.435zm.81.073a4.02 4.02 0 0 0 .611-1.086c.271.023.558.04.851.043v.06l.016 1.118a9.696 9.696 0 0 1-1.478-.135zm.847-2.897c.188.013.382.02.58.023l.003.023.02 1.145a10.558 10.558 0 0 1-.66-.03 4.197 4.197 0 0 0 .057-1.161zm.784-.634h-.029c-.306 0-.602-.013-.876-.033a3.974 3.974 0 0 0-.57-1.228c.478.063.959.096 1.443.096.984 0 1.921-.122 2.638-.346.245-.08.478-.175.7-.297v.874c-1.369.04-2.784.337-3.306.934zm.427.508c.032-.092.264-.307.917-.498.622-.182 1.43-.28 2.281-.28h.042c.911 0 1.803.108 2.453.313.672.211.832.422.841.478v.004c-.003.05-.162.277-.87.495-.653.201-1.513.31-2.424.31-.86 0-1.682-.102-2.313-.284-.609-.178-.854-.38-.914-.482a.288.288 0 0 0-.013-.056zm6.544 3.673c0 .046-.144.274-.819.492-.647.208-1.507.323-2.425.323-.92 0-1.78-.112-2.418-.313-.675-.215-.812-.436-.812-.485v-.007l-.016-.954c.758.4 2.023.581 3.183.581.984 0 1.915-.122 2.628-.346a3.64 3.64 0 0 0 .679-.29v1zm-.029-1.828c0 .046-.146.27-.835.488-.653.205-1.52.317-2.443.317-1.75 0-2.935-.406-3.196-.726l-.006-.284-.013-.755c.233.122.475.217.727.287.684.198 1.567.31 2.485.31.968 0 1.892-.119 2.602-.337.246-.072.482-.168.705-.29v.848h.003c0 .043-.029.092-.029.142z"
        fill={colorIcon}
      />
    </svg>
  );
}

Budget.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Budget.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Budget;
