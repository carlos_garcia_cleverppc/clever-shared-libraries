/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Exit(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M2.667 8a.667.667 0 0 0 .666.667h5.06L6.86 10.193a.667.667 0 0 0 .217 1.093.666.666 0 0 0 .73-.146l2.666-2.667a.666.666 0 0 0 .14-.22.666.666 0 0 0 0-.506.666.666 0 0 0-.14-.22L7.807 4.86a.67.67 0 0 0-.947.947l1.533 1.526h-5.06A.667.667 0 0 0 2.667 8zm8.666-6.667H4.667a2 2 0 0 0-2 2v2a.667.667 0 1 0 1.333 0v-2a.667.667 0 0 1 .667-.666h6.666a.667.667 0 0 1 .667.666v9.334a.667.667 0 0 1-.667.666H4.667A.667.667 0 0 1 4 12.667v-2a.667.667 0 1 0-1.333 0v2a2 2 0 0 0 2 2h6.666a2 2 0 0 0 2-2V3.333a2 2 0 0 0-2-2z"
        fill={colorIcon}
      />
    </svg>
  );
}

Exit.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Exit.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Exit;
