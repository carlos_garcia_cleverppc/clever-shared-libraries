/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function EditImage(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M12.663 7.305a.666.666 0 0 0-.666.667v1.593l-1-.987c-.73-.7-1.883-.7-2.612 0l-.467.467-1.66-1.66a1.853 1.853 0 0 0-2.612 0l-.98.993V4.64c0-.368.299-.666.667-.666h3.999a.666.666 0 1 0 0-1.333h-4a2 2 0 0 0-1.999 2v7.997a2 2 0 0 0 2 2h7.997a2 2 0 0 0 2-2V7.972a.666.666 0 0 0-.667-.667zm-9.33 5.998a.666.666 0 0 1-.667-.666v-2.373l1.92-1.946a.52.52 0 0 1 .746 0l2.113 2.113 2.839 2.84-6.951.032zm8.664-.666a.666.666 0 0 1-.114.36L8.858 9.984l.466-.46a.52.52 0 0 1 .733 0l1.94 1.927v1.186zm2.48-10.164L12.862.86a.666.666 0 0 0-.94 0L8.872 3.913a.666.666 0 0 0-.207.473V6c0 .368.299.666.667.666h1.613c.175 0 .342-.07.466-.193l3.053-3.052a.666.666 0 0 0 .013-.947zm-3.813 2.86h-.667v-.667l2.4-2.393.666.667-2.4 2.392z"
        fill={colorIcon}
      />
    </svg>
  );
}

EditImage.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

EditImage.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default EditImage;
