/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Score(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M8 16A8 8 0 1 1 8 0a8 8 0 0 1 0 16zM2.944 4.08A6.368 6.368 0 0 0 1.648 7.2H2.4a.8.8 0 1 1 0 1.6h-.752a6.36 6.36 0 0 0 1.056 2.8A7.968 7.968 0 0 1 7.2 9.64V5.6a.8.8 0 1 1 1.6 0v4.04c1.667.164 3.24.85 4.496 1.96a6.36 6.36 0 0 0 1.056-2.8H13.6a.8.8 0 0 1 0-1.6h.752a6.368 6.368 0 0 0-1.296-3.12l-.528.528a.803.803 0 1 1-1.136-1.136l.536-.528A6.368 6.368 0 0 0 8.8 1.648V2.4a.8.8 0 1 1-1.6 0v-.752a6.322 6.322 0 0 0-3.12 1.296l.528.528a.803.803 0 1 1-1.136 1.136l-.528-.536v.008zm.824 8.72a6.376 6.376 0 0 0 8.464 0 6.376 6.376 0 0 0-8.464 0z"
        fill={colorIcon}
      />
    </svg>
  );
}

Score.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Score.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Score;
