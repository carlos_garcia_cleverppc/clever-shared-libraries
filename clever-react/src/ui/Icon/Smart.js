/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Smart(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M8 7.333a.667.667 0 1 0 0 1.334.667.667 0 0 0 0-1.334zM13.08 8l.073-.093c.9-1.24 1.114-2.38.62-3.24-.493-.86-1.6-1.24-3.106-1.087h-.12C9.94 2.2 9.04 1.333 8 1.333c-1.04 0-1.94.867-2.54 2.267l-.127-.02c-1.506-.153-2.613.227-3.106 1.087-.494.86-.28 2 .62 3.24L2.92 8l-.073.093c-.9 1.24-1.114 2.38-.62 3.24.44.734 1.286 1.12 2.44 1.12.206 0 .42 0 .666-.033h.12C6.06 13.8 6.96 14.667 8 14.667c1.04 0 1.94-.867 2.54-2.267h.12c.227 0 .44.033.667.033 1.18 0 2.046-.386 2.466-1.12.494-.86.28-2-.62-3.24L13.08 8zm-9.7-2.667c.167-.293.667-.453 1.333-.453h.327c-.105.41-.182.827-.233 1.247a10 10 0 0 0-.967.833c-.507-.667-.653-1.293-.46-1.627zm0 5.334c-.193-.334 0-.96.447-1.647a10 10 0 0 0 .966.833c.051.423.13.841.234 1.254-.827.053-1.454-.107-1.647-.44zm4.62-8c.373 0 .82.44 1.2 1.22-.407.118-.808.258-1.2.42a11.734 11.734 0 0 0-1.2-.42c.38-.78.827-1.22 1.2-1.22zm0 10.666c-.373 0-.82-.44-1.2-1.22.407-.118.808-.258 1.2-.42.392.162.793.302 1.2.42-.38.78-.827 1.22-1.2 1.22zm1.953-4.206c-.306.213-.62.413-.953.606-.333.194-.667.367-1 .52a13.407 13.407 0 0 1-1-.52 10.155 10.155 0 0 1-.953-.606C6 8.767 6 8.393 6 8s0-.767.047-1.127c.306-.213.62-.413.953-.606.333-.194.667-.367 1-.52.333.155.667.329 1 .52.333.193.667.393.953.606 0 .36.047.734.047 1.127s0 .767-.047 1.127zm2.667 1.54c-.193.333-.82.5-1.647.44.105-.413.183-.831.234-1.254.34-.257.662-.535.966-.833.494.687.64 1.313.447 1.647zm-.447-3.687a9.999 9.999 0 0 0-.966-.833 9.844 9.844 0 0 0-.234-1.247h.327c.667 0 1.153.16 1.333.453.18.294.034.94-.46 1.627z"
        fill={colorIcon}
      />
    </svg>
  );
}

Smart.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Smart.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Smart;
