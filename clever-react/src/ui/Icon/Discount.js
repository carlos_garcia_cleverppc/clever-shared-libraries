/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Discount(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="m5.426 4.587.78.767 2.578-2.578a.6.6 0 0 1 .84.852L7.056 6.206l2.578 2.578a.6.6 0 0 1-.851.84L6.206 7.056l-.78.78a1.799 1.799 0 1 1-.839-.852l.767-.78-.779-.779a1.799 1.799 0 1 1 .851-.839zm7.974 1.02a.6.6 0 0 1 .6.599v.6a.6.6 0 1 1-1.2 0 .6.6 0 0 1 0-1.2h.6zM3.208 12.8a.6.6 0 1 1 0 1.199h-.6a.6.6 0 0 1-.6-.6v-.6a.6.6 0 0 1 1.2 0zM13.4 14h-.6a.6.6 0 1 1 0-1.2.6.6 0 1 1 1.2 0v.6a.6.6 0 0 1-.6.6zM9.204 5.606h1.199a.6.6 0 0 1 0 1.2h-1.2a.6.6 0 1 1 0-1.2zM13.4 8.604a.6.6 0 0 1 .6.6v1.199a.6.6 0 0 1-1.2 0v-1.2a.6.6 0 0 1 .6-.599zm-4.196 4.197h1.199a.6.6 0 1 1 0 1.199h-1.2a.6.6 0 1 1 0-1.2zm-3.598 0h1.2a.6.6 0 1 1 0 1.199h-1.2a.6.6 0 1 1 0-1.2zM4.227 9.024a.6.6 0 1 0-.72-.96.6.6 0 0 0 .72.96zm-.42-4.617a.6.6 0 1 0 0-1.199.6.6 0 0 0 0 1.2z"
        fill={colorIcon}
      />
    </svg>
  );
}

Discount.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Discount.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Discount;
