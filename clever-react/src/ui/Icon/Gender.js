/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Gender(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M10.315 8.207a4.683 4.683 0 0 0 1.559-3.501C11.874 2.106 9.82 0 7.289 0S2.705 2.107 2.705 4.706a4.68 4.68 0 0 0 1.604 3.501C1.688 9.426.003 12.105 0 15.06c0 .52.41.941.917.941a.93.93 0 0 0 .917-.941c0-3.119 2.463-5.647 5.501-5.647 3.039 0 5.502 2.528 5.502 5.647 0 .52.41.941.917.941a.93.93 0 0 0 .917-.941c-.011-2.965-1.716-5.647-4.356-6.852zM7.29 7.53c-1.519 0-2.75-1.264-2.75-2.823 0-1.56 1.231-2.824 2.75-2.824 1.52 0 2.751 1.264 2.751 2.824 0 .749-.29 1.467-.806 1.996a2.715 2.715 0 0 1-1.945.827zm7.794-4.705a.93.93 0 0 0-.917.94v1.883c0 .52.41.941.917.941a.93.93 0 0 0 .917-.94V3.764a.93.93 0 0 0-.917-.941zm-.688 4.978c-.167.18-.262.42-.265.669-.002.25.094.49.265.668.09.083.192.15.303.197.222.101.475.101.697 0a.836.836 0 0 0 .495-.508.809.809 0 0 0 .073-.357.942.942 0 0 0-.565-.875.9.9 0 0 0-1.003.206z"
        fill={colorIcon}
      />
    </svg>
  );
}

Gender.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Gender.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Gender;
