/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Cpa(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M5.6 6a.8.8 0 0 0-.8.8v1.6a.8.8 0 1 0 1.6 0V6.8a.8.8 0 0 0-.8-.8zm9.6.8A.8.8 0 0 0 16 6V2.8a.8.8 0 0 0-.8-.8H.8a.8.8 0 0 0-.8.8V6a.8.8 0 0 0 .8.8.8.8 0 1 1 0 1.6.8.8 0 0 0-.8.8v3.2a.8.8 0 0 0 .8.8h14.4a.8.8 0 0 0 .8-.8V9.2a.8.8 0 0 0-.8-.8.8.8 0 0 1 0-1.6zm-.8-1.456a2.4 2.4 0 0 0 0 4.512V11.6h-8a.8.8 0 0 0-1.6 0H1.6V9.856a2.4 2.4 0 0 0 0-4.512V3.6h3.2a.8.8 0 1 0 1.6 0h8v1.744z"
        fill={colorIcon}
      />
    </svg>
  );
}

Cpa.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Cpa.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Cpa;
