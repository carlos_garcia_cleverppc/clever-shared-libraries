/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Image(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M13.6 1.6H2.4A2.4 2.4 0 0 0 0 4v8a2.4 2.4 0 0 0 2.4 2.4h11.2A2.4 2.4 0 0 0 16 12V4a2.4 2.4 0 0 0-2.4-2.4zM2.4 12.8a.8.8 0 0 1-.8-.8v-1.936l2.64-2.632a.8.8 0 0 1 1.12 0l5.368 5.368H2.4zm12-.8a.8.8 0 0 1-.8.8h-.616L9.936 9.736l.704-.704a.8.8 0 0 1 1.12 0l2.64 2.632V12zm0-2.592-1.504-1.496a2.448 2.448 0 0 0-3.392 0l-.704.704-2.304-2.304a2.448 2.448 0 0 0-3.392 0L1.6 7.808V4a.8.8 0 0 1 .8-.8h11.2a.8.8 0 0 1 .8.8v5.408z"
        fill={colorIcon}
      />
    </svg>
  );
}

Image.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Image.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Image;
