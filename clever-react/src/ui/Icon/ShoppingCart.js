/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function ShoppingCart(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M11 10.4a1.802 1.802 0 0 1 1.036 3.27 1.795 1.795 0 0 1-2.121-.035 1.801 1.801 0 0 1-.61-2.035h-2.6a1.802 1.802 0 0 1-.644 2.06 1.795 1.795 0 0 1-2.154-.039 1.802 1.802 0 0 1-.57-2.081A1.8 1.8 0 0 1 3.811 8V3.2H2.613a.6.6 0 0 1 0-1.2H4.41a.6.6 0 0 1 .599.6v.6h8.388a.599.599 0 0 1 .539.87l-2.397 4.8A.599.599 0 0 1 11 9.2H3.81a.6.6 0 0 0 0 1.2H11zM5.01 8h5.62l1.797-3.6H5.01V8zm0 4.8a.6.6 0 1 0 0-1.2.6.6 0 0 0 0 1.2zm5.991 0a.6.6 0 1 0 0-1.199.6.6 0 0 0 0 1.199z"
        fill={colorIcon}
      />
    </svg>
  );
}

ShoppingCart.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

ShoppingCart.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default ShoppingCart;
