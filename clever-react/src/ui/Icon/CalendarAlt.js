/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function CalendarAlt(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M8.455 11.818a.545.545 0 1 0 0-1.09.545.545 0 0 0 0 1.09zm2.727 0a.545.545 0 1 0 0-1.09.545.545 0 0 0 0 1.09zm0-2.182a.545.545 0 1 0 0-1.09.545.545 0 0 0 0 1.09zm-2.727 0a.545.545 0 1 0 0-1.09.545.545 0 0 0 0 1.09zm3.818-6.545h-.546v-.546a.545.545 0 0 0-1.09 0v.546H6.272v-.546a.545.545 0 0 0-1.091 0v.546h-.546C3.733 3.09 3 3.824 3 4.727v7.637C3 13.267 3.733 14 4.636 14h7.637c.904 0 1.636-.733 1.636-1.636V4.727c0-.903-.732-1.636-1.636-1.636zm.545 9.273a.545.545 0 0 1-.545.545H4.636a.545.545 0 0 1-.545-.545v-4.91h8.727v4.91zm0-6H4.091V4.727c0-.301.244-.545.545-.545h.546v.545a.545.545 0 1 0 1.09 0v-.545h4.364v.545a.545.545 0 1 0 1.091 0v-.545h.546c.301 0 .545.244.545.545v1.637zm-7.09 3.272a.545.545 0 1 0 0-1.09.545.545 0 0 0 0 1.09zm0 2.182a.545.545 0 1 0 0-1.09.545.545 0 0 0 0 1.09z"
        fill={colorIcon}
      />
    </svg>
  );
}

CalendarAlt.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

CalendarAlt.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default CalendarAlt;
