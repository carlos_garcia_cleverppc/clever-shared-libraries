/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Support(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M10.513 3.247c.319.18.724.07.907-.247a.667.667 0 0 1 1.247.333A.667.667 0 0 1 12 4a.667.667 0 1 0 0 1.333 2 2 0 1 0-1.733-3 .666.666 0 0 0 .246.914zM13.38 8a.667.667 0 0 0-.747.573A4.667 4.667 0 0 1 8 12.667H4.273l.434-.434a.667.667 0 0 0 0-.94A4.667 4.667 0 0 1 8 3.333.667.667 0 0 0 8 2a6 6 0 0 0-4.667 9.74l-1.14 1.12a.667.667 0 0 0-.14.727.667.667 0 0 0 .614.413H8a6 6 0 0 0 5.953-5.247A.666.666 0 0 0 13.38 8zm-1.127-1.953A.666.666 0 0 0 11.867 6l-.12.04-.12.06-.1.087a.666.666 0 0 0-.14.213.56.56 0 0 0-.054.267.668.668 0 0 0 .047.26c.034.08.084.152.147.213a.667.667 0 0 0 .473.193.667.667 0 0 0 .667-.666.56.56 0 0 0-.054-.254.713.713 0 0 0-.36-.36v-.006z"
        fill={colorIcon}
      />
    </svg>
  );
}

Support.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Support.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Support;
