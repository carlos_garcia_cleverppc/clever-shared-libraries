/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Tablet(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="m8.241 11.432-.121-.096a.617.617 0 0 0-.146-.072L7.83 11.2a.816.816 0 0 0-.737.216.92.92 0 0 0-.17.264.793.793 0 0 0 .173.87c.23.23.577.3.88.178.096-.046.186-.106.266-.176a.793.793 0 0 0 0-1.12zM10.905 0H4.429C3.087 0 2 1.075 2 2.4v11.2C2 14.925 3.087 16 4.429 16h6.476c1.341 0 2.428-1.075 2.428-2.4V2.4c0-1.325-1.087-2.4-2.428-2.4zm.81 13.6c0 .442-.363.8-.81.8H4.429a.805.805 0 0 1-.81-.8V2.4c0-.442.362-.8.81-.8h6.476c.447 0 .81.358.81.8v11.2z"
        fill={colorIcon}
      />
    </svg>
  );
}

Tablet.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Tablet.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Tablet;
