/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Medal(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 14 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="m13.52 13.981-2.444-4.233a5.775 5.775 0 0 0 1.53-3.932 5.788 5.788 0 0 0-1.707-4.11A5.788 5.788 0 0 0 6.789 0a5.788 5.788 0 0 0-4.11 1.707 5.788 5.788 0 0 0-1.706 4.11c0 1.515.573 2.894 1.529 3.931L.058 13.981a.333.333 0 0 0-.027.26c.054.15.218.218.368.163l1.953-.778.3 2.089a.269.269 0 0 0 .137.205c.136.082.314.027.395-.11l2.472-4.273c.355.068.737.11 1.106.11.382 0 .75-.042 1.106-.11l2.47 4.274c.083.136.26.177.397.109a.269.269 0 0 0 .136-.205l.3-2.089 1.953.778a.282.282 0 0 0 .369-.164c.082-.095.068-.19.027-.259zm-10.39.82-.246-1.626a.302.302 0 0 0-.328-.245c-.027 0-.054.013-.082.027l-1.515.6 1.966-3.399c.614.546 1.352.97 2.17 1.215L3.13 14.8zm3.659-3.742a5.221 5.221 0 0 1-5.23-5.229c0-1.447.588-2.758 1.53-3.7a5.133 5.133 0 0 1 3.7-1.557 5.221 5.221 0 0 1 5.23 5.23 5.22 5.22 0 0 1-1.53 3.7 5.133 5.133 0 0 1-3.7 1.556zm4.314 1.898c-.027-.014-.054-.027-.082-.027-.15-.028-.3.082-.327.245l-.246 1.625-1.966-3.427a5.764 5.764 0 0 0 2.17-1.215l1.967 3.4-1.516-.601z"
        fill={colorIcon}
      />
      <path
        d="M9.465 4.806a.614.614 0 0 0-.56-.26H7.772l-.355-1.119a.729.729 0 0 0-.314-.382.744.744 0 0 0-.3-.069.744.744 0 0 0-.3.069.632.632 0 0 0-.315.382l-.355 1.12H4.7a.614.614 0 0 0-.56.259.613.613 0 0 0-.109.478.64.64 0 0 0 .26.423l.955.683-.369 1.12c0 .013 0 .027-.013.027a.606.606 0 0 0-.014.273.55.55 0 0 0 .123.273c.11.15.26.232.423.26a.61.61 0 0 0 .478-.124l.956-.696.956.696c.15.11.314.137.478.123a.64.64 0 0 0 .423-.26.704.704 0 0 0 .123-.272.617.617 0 0 0-.014-.273c0-.014 0-.028-.014-.028l-.368-1.12.955-.682a.64.64 0 0 0 .26-.423.956.956 0 0 0-.164-.478zm-.478.437-.014.014-1.105.805c-.096.068-.15.205-.11.314l.424 1.297v.054c0 .014 0 .028-.014.028-.014.014-.027.027-.055.027-.013 0-.04 0-.054-.013l-1.12-.82a.274.274 0 0 0-.177-.054.3.3 0 0 0-.178.055l-1.12.819c-.013.013-.04.013-.054.013a.104.104 0 0 1-.055-.027c-.013-.014-.013-.027-.013-.027v-.055l.423-1.297a.26.26 0 0 0-.11-.314l-1.05-.805-.014-.014c-.014-.014-.028-.027-.028-.041 0-.014 0-.041.014-.055 0 0 0-.013.014-.013.013-.014.013-.014.027-.014s.027-.014.041-.014h1.379a.29.29 0 0 0 .273-.204l.423-1.311c0-.014.014-.041.041-.041.014 0 .028-.014.028-.014.013 0 .027 0 .027.014l.04.04.424 1.312c.041.109.15.204.273.204h1.379c.014 0 .027 0 .041.014.014 0 .014.014.027.014 0 0 0 .013.014.013.014.014.014.028.014.055-.028.014-.041.027-.055.04z"
        fill={colorIcon}
      />
      <path
        d="M9.997 2.608A4.519 4.519 0 0 0 6.79 1.283a4.519 4.519 0 0 0-4.533 4.533 4.519 4.519 0 0 0 4.533 4.533 4.519 4.519 0 0 0 4.533-4.533c0-1.256-.505-2.389-1.325-3.208zm-.395 6.007a3.963 3.963 0 0 1-2.813 1.16 3.964 3.964 0 0 1-3.96-3.959c0-1.092.451-2.089 1.161-2.799a3.919 3.919 0 0 1 2.799-1.174 3.964 3.964 0 0 1 3.96 3.96 3.984 3.984 0 0 1-1.147 2.812z"
        fill={colorIcon}
      />
    </svg>
  );
}

Medal.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Medal.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Medal;
