/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Balance(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M15.31 9.215a.642.642 0 0 0-.02-.116l-1.536-4.091c.568-.368.912-.998.913-1.675a.667.667 0 0 0-1.334 0 .667.667 0 0 1-1.254.315 1.929 1.929 0 0 0-1.719-.981H8.667V2a.667.667 0 0 0-1.334 0v.667H5.64a1.929 1.929 0 0 0-1.719.981.667.667 0 0 1-1.254-.315.667.667 0 0 0-1.334 0c.001.677.345 1.307.913 1.675L.709 9.099a.632.632 0 0 0-.018.116.632.632 0 0 0-.024.118l.001.018c0 .01.002.017.003.026a2.663 2.663 0 0 0 5.325 0l.002-.026L6 9.333a.632.632 0 0 0-.024-.118.632.632 0 0 0-.019-.116L4.42 5.003c.283-.18.515-.43.676-.723A.611.611 0 0 1 5.64 4h1.693v9.333h-2a.667.667 0 0 0 0 1.334h5.334a.667.667 0 0 0 0-1.334h-2V4h1.693a.611.611 0 0 1 .545.28c.16.294.393.543.676.723l-1.538 4.096a.625.625 0 0 0-.02.116.636.636 0 0 0-.023.118l.002.018.002.026a2.663 2.663 0 0 0 5.325 0l.002-.026.002-.018a.627.627 0 0 0-.024-.118zM3.332 5.904l1.038 2.763H2.296l1.037-2.763zM4.482 10c-.24.408-.675.661-1.149.667-.476 0-.916-.255-1.154-.667h2.303zm8.185-4.096 1.037 2.763H11.63l1.038-2.763zm0 4.763c-.476 0-.916-.255-1.155-.667h2.303c-.24.408-.675.661-1.148.667z"
        fill={colorIcon}
      />
    </svg>
  );
}

Balance.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Balance.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Balance;
