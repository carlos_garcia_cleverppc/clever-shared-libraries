/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Light(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M11.92 1.324A5.818 5.818 0 0 0 7.062.116a5.818 5.818 0 0 0-4.56 4.597 5.87 5.87 0 0 0 1.25 4.836c.513.578.806 1.316.83 2.087v2.182c0 1.205.977 2.182 2.182 2.182h2.909a2.182 2.182 0 0 0 2.181-2.182v-2.043a3.76 3.76 0 0 1 .888-2.32 5.818 5.818 0 0 0-.822-8.146v.015zM10.4 13.818a.727.727 0 0 1-.727.728h-2.91a.727.727 0 0 1-.727-.728v-.727H10.4v.727zm1.214-5.265a5.186 5.186 0 0 0-1.214 3.083H8.945V9.455a.727.727 0 0 0-1.454 0v2.181H6.036a4.727 4.727 0 0 0-1.163-3.025 4.364 4.364 0 0 1 2.465-7.07 4.364 4.364 0 0 1 5.244 4.277 4.284 4.284 0 0 1-.968 2.735z"
        fill={colorIcon}
      />
    </svg>
  );
}

Light.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Light.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Light;
