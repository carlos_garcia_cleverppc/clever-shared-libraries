/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Telephone(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M12.96 8.667c-.147 0-.3-.047-.447-.08a6.294 6.294 0 0 1-.873-.26 1.333 1.333 0 0 0-1.653.666l-.147.3A8.12 8.12 0 0 1 8.067 7.96a8.12 8.12 0 0 1-1.334-1.773L7.013 6c.612-.3.9-1.013.667-1.653a6.886 6.886 0 0 1-.26-.874c-.033-.146-.06-.3-.08-.453a2 2 0 0 0-2-1.66h-2a2 2 0 0 0-2 2.273 12.667 12.667 0 0 0 11.013 10.974h.254a2 2 0 0 0 1.333-.507 2 2 0 0 0 .667-1.5v-2a2 2 0 0 0-1.647-1.933zm.333 4a.667.667 0 0 1-.226.5c-.15.13-.35.19-.547.166A11.333 11.333 0 0 1 2.713 3.48a.727.727 0 0 1 .167-.547.667.667 0 0 1 .5-.226h2c.32-.007.6.214.667.526a7.413 7.413 0 0 0 .407 1.573l-.934.434a.667.667 0 0 0-.327.887 9.66 9.66 0 0 0 4.667 4.666.667.667 0 0 0 .507 0 .667.667 0 0 0 .38-.346l.413-.934c.345.124.696.226 1.053.307.178.04.358.073.54.1a.667.667 0 0 1 .527.667l.013 2.08z"
        fill={colorIcon}
      />
    </svg>
  );
}

Telephone.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Telephone.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Telephone;
