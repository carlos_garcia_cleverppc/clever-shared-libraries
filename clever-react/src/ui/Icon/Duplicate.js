/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Duplicate(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M2.058 1.116a.922.922 0 0 0-.912.93v6.698c0 .529.424.93.912.93h.743c.316 0 .573.25.573.559a.566.566 0 0 1-.573.558h-.743A2.052 2.052 0 0 1 0 8.744V2.047C0 .93.906 0 2.058 0h6.685C9.895 0 10.8.931 10.8 2.047v.744a.566.566 0 0 1-.573.558.566.566 0 0 1-.573-.558v-.744a.922.922 0 0 0-.912-.93H2.058zm5.2 5.21a.922.922 0 0 0-.913.93v6.697c0 .53.423.93.912.93h6.685a.922.922 0 0 0 .912-.93V7.256a.922.922 0 0 0-.912-.93H7.257zm-2.06.93c0-1.116.907-2.047 2.06-2.047h6.684C15.094 5.21 16 6.14 16 7.256v6.697A2.052 2.052 0 0 1 13.942 16H7.257A2.052 2.052 0 0 1 5.2 13.954V7.255z"
        fill={colorIcon}
        fillRule="evenodd"
        clipRule="evenodd"
      />
    </svg>
  );
}

Duplicate.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Duplicate.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Duplicate;
