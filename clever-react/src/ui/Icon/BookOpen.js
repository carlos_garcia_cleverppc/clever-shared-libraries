/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function BookOpen(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M15.176.15A10.365 10.365 0 0 0 13.459 0 10.237 10.237 0 0 0 7.92 1.622 10.237 10.237 0 0 0 2.384.04C1.808.042 1.234.092.667.19A.791.791 0 0 0 .01.98v9.493a.792.792 0 0 0 .926.791 8.622 8.622 0 0 1 6.527 1.511l.094.055h.087a.72.72 0 0 0 .554 0h.087l.095-.055a8.623 8.623 0 0 1 6.527-1.598.79.79 0 0 0 .925-.791V.894a.791.791 0 0 0-.656-.744zM7.13 10.664a10.182 10.182 0 0 0-4.746-1.171h-.791v-7.91c.263-.016.527-.016.79 0A8.591 8.591 0 0 1 7.13 3.005v7.658zm7.12-1.14h-.791a10.18 10.18 0 0 0-4.747 1.171V3.006a8.591 8.591 0 0 1 4.747-1.424c.263-.015.527-.015.791 0v7.943zm.926 3.284a10.355 10.355 0 0 0-1.717-.15 10.237 10.237 0 0 0-5.538 1.621 10.237 10.237 0 0 0-5.537-1.622c-.576.003-1.15.053-1.717.15a.79.79 0 0 0-.657.91.791.791 0 0 0 .926.625 8.623 8.623 0 0 1 6.527 1.511.79.79 0 0 0 .917 0 8.623 8.623 0 0 1 6.527-1.51.791.791 0 0 0 .925-.626.791.791 0 0 0-.656-.91z"
        fill={colorIcon}
      />
    </svg>
  );
}

BookOpen.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

BookOpen.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default BookOpen;
