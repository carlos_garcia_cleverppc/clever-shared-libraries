/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Trash(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M6.645 6.698c.272 0 .494.22.494.494v4.465a.494.494 0 1 1-.989 0V7.192c0-.273.222-.494.495-.494zM10.115 7.192a.494.494 0 1 0-.988 0v4.465a.494.494 0 1 0 .988 0V7.192z"
        fill={colorIcon}
      />
      <path
        d="M11.604 1.982v.995H14.83a.494.494 0 1 1 0 .988h-.995v9.925a1.983 1.983 0 0 1-1.982 1.982H4.412A1.982 1.982 0 0 1 2.43 13.89V3.964h-.995a.494.494 0 1 1 0-.988H4.662v-.995C4.662.888 5.55 0 6.645 0H9.62c1.095 0 1.983.888 1.983 1.982zm-5.954 0c0-.549.445-.994.995-.994H9.62c.55 0 .995.445.995.994v.995H5.65v-.995zM3.418 3.965v9.925c0 .549.445.994.994.994h7.442c.549 0 .994-.445.994-.995V3.966h-9.43z"
        fill={colorIcon}
        fillRule="evenodd"
        clipRule="evenodd"
      />
    </svg>
  );
}

Trash.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Trash.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Trash;
