/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Location(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M8.222 3.436c-1.758 0-3.341 1.265-3.341 3.008 0 1.744 1.583 3.009 3.341 3.009 1.758 0 3.342-1.265 3.342-3.009 0-1.743-1.584-3.008-3.342-3.008zM6.231 6.444c0-.834.804-1.658 1.991-1.658 1.188 0 1.992.824 1.992 1.658S9.41 8.103 8.222 8.103c-1.187 0-1.991-.825-1.991-1.659z"
        fill={colorIcon}
        fillRule="evenodd"
        clipRule="evenodd"
      />
      <path
        d="m7.94 15.46.282-.46-.283.46a.54.54 0 0 0 .566 0L8.222 15l.283.46.002-.001.004-.003.015-.01a8.381 8.381 0 0 0 .263-.17 21.05 21.05 0 0 0 2.921-2.355c1.572-1.528 3.274-3.732 3.274-6.194 0-3.502-3.07-6.267-6.762-6.267C4.531.46 1.46 3.225 1.46 6.727c0 2.462 1.702 4.666 3.275 6.194a21.05 21.05 0 0 0 3.183 2.526l.015.01.005.002.001.001zm-5.4-8.733c0-2.823 2.501-5.187 5.682-5.187 3.181 0 5.682 2.364 5.682 5.187 0 1.993-1.409 3.925-2.947 5.42a19.984 19.984 0 0 1-2.735 2.21 19.985 19.985 0 0 1-2.735-2.21C3.95 10.652 2.54 8.72 2.54 6.727z"
        fill={colorIcon}
        fillRule="evenodd"
        clipRule="evenodd"
      />
    </svg>
  );
}

Location.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Location.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Location;
