/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Images(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M12.8 10.4v-8A2.4 2.4 0 0 0 10.4 0h-8A2.4 2.4 0 0 0 0 2.4v8a2.4 2.4 0 0 0 2.4 2.4h8a2.4 2.4 0 0 0 2.4-2.4zm-11.2-8a.8.8 0 0 1 .8-.8h8a.8.8 0 0 1 .8.8v3.488l-.864-.872a2.048 2.048 0 0 0-2.896 0l-.728.728-.648-.648a2.344 2.344 0 0 0-3.288 0L1.6 6.28V2.4zm.096 8.36a.752.752 0 0 1-.096-.36V8.536l2.304-2.304a.728.728 0 0 1 1.032 0l.664.648-3.904 3.88zm6.88-4.608a.416.416 0 0 1 .312-.136.416.416 0 0 1 .312.136l2 1.992V10.4a.8.8 0 0 1-.8.8H3.52l5.056-5.048zM15.2 3.2a.8.8 0 0 0-.8.8v8a2.4 2.4 0 0 1-2.4 2.4H4A.8.8 0 0 0 4 16h8a4 4 0 0 0 4-4V4a.8.8 0 0 0-.8-.8z"
        fill={colorIcon}
      />
    </svg>
  );
}

Images.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Images.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Images;
