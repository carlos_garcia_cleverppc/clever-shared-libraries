/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function StarEmpty(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M16 6.136a.8.8 0 0 0-.688-.536l-4.552-.664L8.72.8a.8.8 0 0 0-1.44 0L5.24 4.928.688 5.6a.8.8 0 0 0-.648.544.8.8 0 0 0 .2.8l3.304 3.2-.8 4.544a.8.8 0 0 0 1.16.856L8 13.408l4.08 2.144c.112.063.24.096.368.096a.8.8 0 0 0 .792-.952l-.8-4.544 3.304-3.2A.8.8 0 0 0 16 6.136zm-4.92 3.2a.8.8 0 0 0-.232.712l.576 3.352-3.008-1.6a.8.8 0 0 0-.752 0l-3.008 1.6.576-3.352A.8.8 0 0 0 5 9.336l-2.4-2.4 3.368-.488a.8.8 0 0 0 .608-.44L8 2.96l1.504 3.056a.8.8 0 0 0 .608.44l3.368.488-2.4 2.392z"
        fill={colorIcon}
      />
    </svg>
  );
}

StarEmpty.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

StarEmpty.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default StarEmpty;
