/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Cog(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M8 5.333a2.667 2.667 0 1 0 0 5.334 2.667 2.667 0 0 0 0-5.334zm0 4a1.333 1.333 0 1 1 0-2.666 1.333 1.333 0 0 1 0 2.666zm6.473-1.806L12.907 6V3.76a.667.667 0 0 0-.667-.667h-2.207l-1.56-1.566a.667.667 0 0 0-.946 0L6 3.093H3.76a.667.667 0 0 0-.667.667V6L1.527 7.527a.667.667 0 0 0 0 .946l1.566 1.56v2.207c0 .368.299.667.667.667H6l1.56 1.566a.667.667 0 0 0 .947 0l1.56-1.566h2.206a.667.667 0 0 0 .667-.667v-2.207l1.567-1.56a.667.667 0 0 0-.034-.946zm-2.7 1.76a.667.667 0 0 0-.2.473v1.813H9.76a.667.667 0 0 0-.473.2L8 13.06l-1.287-1.287a.667.667 0 0 0-.473-.2H4.427V9.76a.667.667 0 0 0-.2-.473L2.94 8l1.287-1.287a.667.667 0 0 0 .2-.473V4.427H6.24a.667.667 0 0 0 .473-.2L8 2.94l1.287 1.287c.124.127.295.199.473.2h1.813V6.24c.001.178.073.349.2.473L13.06 8l-1.287 1.287z"
        fill={colorIcon}
      />
    </svg>
  );
}

Cog.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Cog.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Cog;
