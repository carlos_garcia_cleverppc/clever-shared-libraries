/* eslint-disable max-len */
/* eslint-disable comma-dangle */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";

const { colors } = getCurrentConfig().cleverUI;

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  }
}));

const colorsList = getColorsLabels();
const shadesList = getShades();

function Bell(props) {
  const classes = getClasses(props);
  const { size, color, className, onMouseEnter, onMouseLeave } = props;
  const { padding, shade } = props;

  let sizeIcon = size;
  const colorIcon = colors[color] ? colors[color][shade] : color;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M12 8.787v-2.12a4 4 0 0 0-3.333-3.94V2a.667.667 0 0 0-1.334 0v.727A4 4 0 0 0 4 6.667v2.12a2 2 0 0 0-1.333 1.88V12c0 .368.298.667.666.667h2.094a2.667 2.667 0 0 0 5.146 0h2.094a.667.667 0 0 0 .666-.667v-1.333A2 2 0 0 0 12 8.787zm-6.667-2.12a2.667 2.667 0 0 1 5.334 0v2H5.333v-2zM8 13.333a1.333 1.333 0 0 1-1.147-.666h2.294c-.237.41-.674.664-1.147.666zm4-2H4v-.666c0-.368.298-.667.667-.667h6.666c.368 0 .667.299.667.667v.666z"
        fill={colorIcon}
      />
    </svg>
  );
}

Bell.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([
    6,
    8,
    10,
    12,
    14,
    15,
    16,
    18,
    20,
    22,
    24,
    26,
    30,
    32,
    42,
    64,
    96
  ]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};

Bell.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: () => {},
  onMouseLeave: () => {}
};

export default Bell;
