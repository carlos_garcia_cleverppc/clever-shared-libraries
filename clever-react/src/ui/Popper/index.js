import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

import MaterialPopper from "@material-ui/core/Popper";
import Fade from "@material-ui/core/Fade";

import { getColorsLabels, getShades } from "../styles/colors";

import getClasses from "./styles";

const colorsList = getColorsLabels();
const shadesList = getShades();

const Popper = props => {
  const classes = getClasses(props);

  const { children, open, id, anchorEl, transition } = props;
  const { placement, className, arrow, offset } = props;
  const { preventOverflow, flip, handleClose, disablePortal } = props;

  const [arrowRef, setArrowRef] = useState(null);

  const handleArrowRef = node => {
    setArrowRef(node);
  };

  return (
    <MaterialPopper
      className={classes.popper}
      placement={placement}
      id={id}
      open={open}
      anchorEl={anchorEl}
      transition={transition}
      modifiers={{
        offset: {
          enabled: !!offset,
          offset,
        },
        flip: { enabled: flip },
        preventOverflow: {
          enabled: preventOverflow,
          boundariesElement: preventOverflow,
        },
        arrow: {
          enabled: arrow,
          element: arrowRef,
        },
      }}
      disablePortal={disablePortal}
    >
      {({ TransitionProps }) => (
        <ClickAwayListener onClickAway={handleClose}>
          <Fade {...TransitionProps} disableStrictModeCompat timeout={350}>
            <div className={classnames(classes.root, className)}>
              {arrow && (
                <span className={classes.arrow} ref={handleArrowRef} />
              )}

              {children}
            </div>
          </Fade>
        </ClickAwayListener>
      )}
    </MaterialPopper>
  );
};

Popper.propTypes = {
  className: PropTypes.string,
  color: PropTypes.oneOf(colorsList),
  shade: PropTypes.oneOf(shadesList),
  placement: PropTypes.oneOf([
    "bottom-end",
    "bottom-start",
    "bottom",
    "left-end",
    "left-start",
    "left",
    "right-end",
    "right-start",
    "right",
    "top-end",
    "top-start",
    "top",
  ]),
  arrow: PropTypes.bool,
  preventOverflow: PropTypes.string,
  flip: PropTypes.bool,
  handleClose: PropTypes.func,
  offset: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  disablePortal: PropTypes.bool,
};

Popper.defaultProps = {
  className: "",
  color: "white",
  shade: 500,
  placement: "bottom",
  arrow: false,
  preventOverflow: "scrollParent",
  flip: true,
  handleClose: () => { },
  offset: false,
  disablePortal: false,
};

export default Popper;
