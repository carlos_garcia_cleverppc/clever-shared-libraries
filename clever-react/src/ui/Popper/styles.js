import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing, shadows } }) => ({
  root: ({ color, shade }) => ({
    padding: "0.1px",
    backgroundColor: `${colors[color][shade]}`,
    boxShadow: shadows.hover,
  }),
  popper: ({ color, shade }) => ({
    zIndex: 900,
    "&[x-placement*=\"bottom\"] $arrow": {
      width: 0,
      height: 0,
      borderLeft: "1em solid transparent",
      borderRight: "1em solid transparent",
      borderBottom: `1em solid ${colors[color][shade]}`,
      marginTop: "-0.9em",

      "&:before": {
        borderWidth: "0 1em 1em 1em",
        borderColor: "transparent transparent white transparent",
      },
    },
    "&[x-placement*=\"top\"] $arrow": {
      bottom: 0,
      width: 0,
      height: 0,
      borderLeft: "1em solid transparent",
      borderRight: "1em solid transparent",
      borderTop: `1em solid ${colors[color][shade]}`,
      marginBottom: "-0.9em",

      "&:before": {
        borderWidth: "1em 1em 0 1em",
        borderColor: "white transparent transparent transparent",
      },
    },
    "&[x-placement*=\"right\"] $arrow": {
      left: 0,
      width: 0,
      height: 0,
      borderTop: "1em solid transparent",
      borderBottom: "1em solid transparent",
      borderRight: `1em solid ${colors[color][shade]}`,
      marginLeft: "-0.9em",

      "&:before": {
        borderWidth: "1em 1em 1em 0",
        borderColor: "transparent white transparent transparent",
      },
    },
    "&[x-placement*=\"left\"] $arrow": {
      right: 0,
      width: 0,
      height: 0,
      borderTop: "1em solid transparent",
      borderBottom: "1em solid transparent",
      borderLeft: `1em solid ${colors[color][shade]}`,
      marginRight: "-0.9em",
      "&:before": {
        borderWidth: "1em 0 1em 1em",
        borderColor: "transparent transparent transparent white",
      },
    },
  }),
  arrow: () => ({
    ...spacing["mt-2"],
    position: "absolute",
    fontSize: "8px",
    width: "3em",
    height: "3em",
  }),
});

export default makeStyles(styles);
