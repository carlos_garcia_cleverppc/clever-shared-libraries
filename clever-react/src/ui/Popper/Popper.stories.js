/* eslint-disable react/no-multi-comp */
import React, { useState } from "react";

import { getColorsLabels, getShades } from "../styles/colors";

import Popper from "./index";
import Button from "../Button";

export default {
  title: "Popper",
  component: Popper,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
    shade: {
      options: getShades(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => {
  function Parent({ children }) {
    const [anchorEl, setAnchorEl] = useState(null);

    const open = Boolean(anchorEl);
    const id = open ? "simple-popover" : undefined;

    const handleOpen = event => {
      setAnchorEl(event.currentTarget);
    };

    return (
      <div>
        <Button onClick={handleOpen} aria-describedby={id}>
          Open
        </Button>

        {children(anchorEl, setAnchorEl, open, id)}
      </div>
    );
  }

  return (
    <Parent>
      {(anchorEl, setAnchorEl, open, id) => (
        <Popper
          {...args}
          id={id}
          open={open}
          anchorEl={anchorEl}
          handleClose={() => setAnchorEl(null)}
        />
      )}
    </Parent>
  );
};

export const SimplePopper = Template.bind({});
SimplePopper.args = {
  placement: "bottom",
  arrow: true,
  children: <h1>Simple popover</h1>,
  transition: true,
};
