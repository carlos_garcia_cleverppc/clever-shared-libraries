/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";

import Typography from "../Typography";
import Checkbox from "./index";

export default {
  title: "Checkbox",
  component: Checkbox,
};

const Template = args => {
  const [checked, setChecked] = React.useState(false);

  return <Checkbox {...args} checked={checked} onChange={() => setChecked(!checked)} />;
};

export const SimpleCheckbox = Template.bind({});
SimpleCheckbox.args = {
  value: "Single line",
  children: <Typography variant="f1-14">Single line</Typography>,
  icon: <CheckBoxOutlineBlankIcon />,
  checkedIcon: <CheckBoxIcon />,
};

export const CustomCheckbox = Template.bind({});
CustomCheckbox.args = {
  value: "Single line",
  children: <Typography variant="f1-14">Single line</Typography>,
  icon: <CheckBoxOutlineBlankIcon />,
  checkedIcon: <CheckBoxIcon />,
  color: "secondary",
  size: "small",
};
