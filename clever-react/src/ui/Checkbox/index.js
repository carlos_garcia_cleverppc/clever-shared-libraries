import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import MaterialCheckbox from "@material-ui/core/Checkbox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBox from "@material-ui/icons/CheckBox";

import getClasses from "./styles";

const Checkbox = props => {
  const { className, children, disabled, ...checkboxProps } = props;

  const classes = getClasses({ disabled });

  return (
    <label className={cx(classes.root, className)}>
      <MaterialCheckbox
        className={classes.checkbox}
        disabled={disabled}
        {...checkboxProps}
      />

      <div>
        {children}
      </div>
    </label>
  );
};

Checkbox.propTypes = {
  className: PropTypes.string,
  checked: PropTypes.bool.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func,
  color: PropTypes.oneOf(["primary", "secondary"]),
  disabled: PropTypes.bool,
  icon: PropTypes.instanceOf(Object),
  checkedIcon: PropTypes.instanceOf(Object),
  size: PropTypes.oneOf(["medium", "small"]),
  required: PropTypes.bool,
};

Checkbox.defaultProps = {
  className: "",
  onChange: () => { },
  value: "",
  color: "primary",
  disabled: false,
  icon: <CheckBoxOutlineBlankIcon />,
  checkedIcon: <CheckBox />,
  size: "medium",
  required: false,
};

export default Checkbox;
