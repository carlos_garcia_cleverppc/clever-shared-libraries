import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { spacing } }) => ({
  root: ({ disabled }) => ({
    display: "inline-flex",
    "align-items": "center",
    outline: "none",
    cursor: disabled ? "arrow" : "pointer",
  }),
  checkbox: { ...spacing["p-0"], ...spacing["mr-3"] },
});

export default makeStyles(styles);
