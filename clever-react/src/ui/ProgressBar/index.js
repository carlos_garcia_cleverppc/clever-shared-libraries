import React, { useEffect } from "react";
import PropTypes from "prop-types";

import LinearProgress from "@material-ui/core/LinearProgress";

import getClasses from "./styles";

const ProgressBar = props => {
  const classes = getClasses(props);

  const { value, variant, color, className } = props;

  const [progress, setProgress] = React.useState(0);

  useEffect(() => {
    const timer = setInterval(() => {
      setProgress(oldProgress => {
        if (oldProgress === value) {
          return value;
        }
        return Math.min(oldProgress + 1, value);
      });
    }, 10);

    return () => {
      clearInterval(timer);
    };
  }, [value]);

  return (
    <LinearProgress
      className={className}
      classes={{
        root: classes.root,
        bar: classes.bar,
        barColorPrimary: classes.barColorPrimary,
      }}
      color={color}
      variant={variant}
      value={progress}
    />
  );
};

ProgressBar.propTypes = {
  value: PropTypes.number,
  variant: PropTypes.string,
  color: PropTypes.oneOf(["primary", "secondary"]),
  className: PropTypes.string,
};

ProgressBar.defaultProps = {
  value: 25,
  variant: "determinate",
  color: "primary",
  className: "",
};

export default ProgressBar;
