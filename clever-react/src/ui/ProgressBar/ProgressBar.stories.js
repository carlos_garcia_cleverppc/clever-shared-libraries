/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
import React from "react";

import ProgressBar from "./index";

export default {
  title: "ProgressBar",
  component: ProgressBar,
};

const Template = args => <ProgressBar {...args} />;

export const SimpleProgressBar = Template.bind({});
SimpleProgressBar.args = {};

export const CustomProgressBar = Template.bind({});
CustomProgressBar.args = { value: 70, color: "secondary" };

const DynamicTemplate = args => {
  const [value, setValue] = React.useState(args.value);

  setTimeout(() => {
    setValue(Math.min(value + 40, 100));
  }, 1000);

  return <ProgressBar {...args} value={value} />;
};

export const DynamicValueProgressBar = DynamicTemplate.bind({});
DynamicValueProgressBar.args = { value: 50, color: "secondary" };
