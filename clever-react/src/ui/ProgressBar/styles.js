import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors } }) => ({
  root: {
    height: "10px",
    borderRadius: "5px",
    backgroundColor: colors.neutral[100],
  },
  bar: { borderRadius: "5px" },
  barColorPrimary: ({ color }) => ({ backgroundColor: color }),
  inputEnter: { right: 100 },
  inputEnterActive: {
    right: 0,
    transition: "500ms ease-in all",
  },
});

export default makeStyles(styles);
