/* eslint-disable import/prefer-default-export */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import getClasses from "./styles";
import { Square } from ".";

export const SquareGrid = props => {
  const { children, gap, dense, rowHeight, columnWidth, columns, className } = props;
  const classes = getClasses({ gap, dense, rowHeight, columnWidth, columns });

  return (<div className={classnames([className, classes.grid])}>{children}</div>);
};

SquareGrid.propTypes = {
  /**
   * Use the empty cell even if the order has to be changed
   */
  dense: PropTypes.bool,
  rowHeight: PropTypes.string,
  columnWidth: PropTypes.string,
  columns: PropTypes.number,
  gap: PropTypes.string,
};

SquareGrid.defaultProps = {
  dense: true,
  rowHeight: "160px",
  gap: "24px",
  columnWidth: "310px",
  columns: 2,
};
