/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Card from "../Card";
import Button from "../Button";
import SquareGrid, { Square } from ".";

export default {
  title: "Square",
  component: Square,
  decorators: [Story => <SquareGrid><Story /></SquareGrid>],
};

const Template = args => (
  <>
    <Square {...args}>
      <Card color="mobileApp" />
    </Square>
    <Square>
      <Card color="promo" />
    </Square>
    <Square>
      <Card color="primary" />
    </Square>
  </>
);

export const simpleSquare = Template.bind({});
simpleSquare.args = {};

export const fullWidthSquare = Template.bind({});
fullWidthSquare.args = {
  fullWidth: true,
};

export const customHeightSquare = Template.bind({});
customHeightSquare.args = {
  height: 2,
};

const TemplateSquareWithAs = args => (
  <>
    <Square {...args} color="primary" />
    <Square {...args} color="secondary" />
  </>
);

export const asSquare = TemplateSquareWithAs.bind({});
asSquare.args = {
  as: Button,
  children: "As button",
};
