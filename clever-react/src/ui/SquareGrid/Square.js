/* eslint-disable import/prefer-default-export */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import getClasses from "./styles";

export const Square = ({ children, fullWidth, height, as, className, ...props }) => {
  const classes = getClasses({ height });
  const additionalClasses = {
    [classes.withHeight]: height > 1,
    [classes.fullWidth]: fullWidth,
  };

  return React.createElement(as, {
    className: classnames(className, classes.item, additionalClasses),
    ...props,
  }, children);
};

Square.propTypes = {
  fullWidth: PropTypes.bool,
  /**
  Number of rows
  */
  height: PropTypes.number,
  /**
  This component will be used to render the cell.
  */
  as: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.elementType,
  ]),
};

Square.defaultProps = {
  fullWidth: false,
  height: 1,
  as: "div",
};
