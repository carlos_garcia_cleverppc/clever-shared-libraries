import React from "react";

import Block from "../Block";
import SquareGrid, { Square } from ".";

export default {
  title: "SquareGrid",
  component: SquareGrid,
};

const Template = args => (
  <SquareGrid {...args}>
    <Square fullWidth>
      <Block
        title="Title of the block"
        description="Description of the block"
        color="primary"
        image="/assets/img/block-example.svg"
        size="1x4"
      />
    </Square>
    <Square height={2}>
      <Block
        title="Title of the block"
        description="Description of the block"
        color="primary"
        image="/assets/img/block-example.svg"
        size="2x2"
      />
    </Square>
    <Square>
      <Block
        title="Title of the block"
        description="Description of the block"
        color="primary"
        image="/assets/img/block-example.svg"
        size="1x4"
      />
    </Square>
    <Square>
      <Block
        title="Title of the block"
        description="Description of the block"
        color="primary"
        image="/assets/img/block-example.svg"
        size="1x4"
      />
    </Square>
  </SquareGrid>
);

export const SimpleSquareGrid = Template.bind({});
SimpleSquareGrid.args = {};
