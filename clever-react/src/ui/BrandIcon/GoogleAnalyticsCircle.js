import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import makeStyles from "../styles/makeStyles";
import { iconSizes } from "../styles/icons";

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  },
}));

function GoogleAnalyticsCircle(props) {
  const classes = getClasses(props);
  const { size, padding, className, onMouseEnter, onMouseLeave } = props;

  let sizeIcon = size;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave,
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path d="M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16Z" fill="#E7E8E9" />
      <path d="M11.3531 4H9.89612C9.54372 4 9.25444 4.28947 9.25444 4.64211V6.52632H7.1453C6.80342 6.52632 6.52465 6.80526 6.52465 7.15263V9.26316H4.6259C4.27876 9.26316 4 9.54211 4 9.88947V11.3632C4 11.7105 4.27876 11.9895 4.6259 12H11.3583C11.7107 12 12 11.7105 12 11.3579V4.64211C11.9947 4.28947 11.7055 4 11.3531 4Z" fill="url(#paint0_linear)" />
      <path d="M11.3531 4H9.89612C9.54372 4 9.25444 4.28947 9.25444 4.64211V6.52632H7.1453C6.80342 6.52632 6.52465 6.80526 6.52465 7.15263V9.26316H4.6259C4.27876 9.26316 4 9.54211 4 9.88947V11.3632C4 11.7105 4.27876 11.9895 4.6259 12H11.3583C11.7107 12 12 11.7105 12 11.3579V4.64211C11.9947 4.28947 11.7055 4 11.3531 4Z" fill="url(#paint1_linear)" />
      <path d="M11.3579 4H9.91054C9.55791 4 9.26843 4.28947 9.26843 4.64211V12H11.3579C11.7105 12 12 11.7105 12 11.3579V4.64211C12 4.28947 11.7105 4 11.3579 4Z" fill="#F57C00" />
      <path d="M6.52632 7.1579V9.26316H4.63158C4.28421 9.26316 4 9.54737 4 9.89474V11.3684C4 11.7158 4.28421 12 4.63158 12H9.26316V6.52632H7.15789C6.81053 6.52632 6.52632 6.81053 6.52632 7.1579Z" fill="#FFC107" />
      <path d="M9.23077 6.46154V12H11.349C11.7065 12 12 11.7071 12 11.3503V9.23077L9.23077 6.46154Z" fill="url(#paint2_linear)" />
      <path opacity="0.2" d="M7.1006 6.50888H9.23078V6.46154H7.1006C6.74912 6.46154 6.46155 6.71716 6.46155 7.02959V7.07692C6.46155 6.7645 6.74912 6.50888 7.1006 6.50888Z" fill="white" />
      <path opacity="0.2" d="M4.61538 9.2781H6.46154V9.23077H4.61538C4.27692 9.23077 4 9.48639 4 9.79881V9.84615C4 9.53372 4.27692 9.2781 4.61538 9.2781Z" fill="white" />
      <path opacity="0.2" d="M11.349 4H9.88173C9.52424 4 9.23077 4.25641 9.23077 4.56876V4.61538C9.23077 4.30303 9.52424 4.04662 9.88173 4.04662H11.349C11.7065 4.04662 12 4.30303 12 4.61538V4.56876C12 4.25641 11.7065 4 11.349 4Z" fill="white" />
      <path opacity="0.2" d="M11.3627 11.9534H4.63199C4.2844 11.9534 4 11.7016 4 11.3939V11.4406C4 11.7482 4.2844 12 4.63199 12H11.3574C11.7103 12 12 11.7436 12 11.4312V11.3846C12.0052 11.697 11.7155 11.9534 11.3627 11.9534Z" fill="#BF360C" />
      <path d="M11.3579 4H9.91053C9.55789 4 9.26842 4.28947 9.26842 4.64211V6.52632H7.16316C6.81579 6.52632 6.53158 6.81053 6.53158 7.15789V9.26316H4.63158C4.28421 9.26316 4 9.54737 4 9.89474V11.3684C4 11.7158 4.28421 12 4.63158 12H11.3579C11.7105 12 12 11.7105 12 11.3579V4.64211C12 4.28947 11.7105 4 11.3579 4Z" fill="url(#paint3_linear)" />
      <defs>
        <linearGradient id="paint0_linear" x1="4" y1="8" x2="12" y2="8" gradientUnits="userSpaceOnUse">
          <stop stopColor="white" stopOpacity="0.1" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient id="paint1_linear" x1="4" y1="8" x2="12" y2="8" gradientUnits="userSpaceOnUse">
          <stop stopColor="white" stopOpacity="0.1" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
        <linearGradient id="paint2_linear" x1="9.92575" y1="7.85947" x2="12.3943" y2="10.3233" gradientUnits="userSpaceOnUse">
          <stop stopColor="#BF360C" stopOpacity="0.2" />
          <stop offset="1" stopColor="#BF360C" stopOpacity="0.02" />
        </linearGradient>
        <linearGradient id="paint3_linear" x1="6.68258" y1="6.68584" x2="11.8576" y2="11.8609" gradientUnits="userSpaceOnUse">
          <stop stopColor="white" stopOpacity="0.1" />
          <stop offset="1" stopColor="white" stopOpacity="0" />
        </linearGradient>
      </defs>
    </svg>
  );
}

GoogleAnalyticsCircle.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(iconSizes),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  padding: PropTypes.number,
};

GoogleAnalyticsCircle.defaultProps = {
  size: 24,
  className: "",
  rotation: 0,
  padding: 0,
  onMouseEnter: () => { },
  onMouseLeave: () => { },
};

export default GoogleAnalyticsCircle;
