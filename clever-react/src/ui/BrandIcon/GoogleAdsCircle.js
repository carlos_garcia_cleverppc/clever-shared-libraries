import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import makeStyles from "../styles/makeStyles";
import { iconSizes } from "../styles/icons";

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  },
}));

function GoogleAdsCircle(props) {
  const classes = getClasses(props);
  const { size, padding, className, onMouseEnter, onMouseLeave } = props;

  let sizeIcon = size;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave,
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path d="M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16Z" fill="#E7E8E9" />
      <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="3" y="3" width="10" height="10">
        <path d="M3.52002 3.85249H12.6122V12.16H3.52002V3.85249Z" fill="white" />
      </mask>
      <g mask="url(#mask0)">
        <path fillRule="evenodd" clipRule="evenodd" d="M6.61407 4.89851C6.70195 4.66784 6.82276 4.45546 7.00222 4.28335C7.7199 3.58398 8.9063 3.75974 9.3933 4.63855C9.75947 5.30497 10.1476 5.95674 10.5248 6.61587C11.1546 7.71072 11.7917 8.80557 12.4142 9.90409C12.9378 10.8231 12.3703 11.9839 11.3303 12.1414C10.6932 12.2366 10.0963 11.9437 9.76677 11.3724C9.21387 10.4094 8.65727 9.44636 8.10437 8.487C8.09338 8.46503 8.07874 8.44672 8.06409 8.42843C8.0055 8.3808 7.97987 8.31123 7.94325 8.24898C7.69793 7.81691 7.44527 7.38848 7.19993 6.96008C7.04247 6.68178 6.87769 6.40715 6.72026 6.12885C6.57745 5.87985 6.51154 5.6089 6.51886 5.32328C6.52984 5.17681 6.54815 5.03034 6.61407 4.89851Z" fill="#477EB3" />
        <path fillRule="evenodd" clipRule="evenodd" d="M6.6141 4.89851C6.58114 5.03034 6.55182 5.16216 6.54452 5.30131C6.53354 5.6089 6.61044 5.8945 6.76421 6.16181C7.167 6.85389 7.5698 7.5496 7.96891 8.24532C8.00553 8.30757 8.03482 8.36982 8.07144 8.42844C7.85176 8.80924 7.63205 9.18638 7.40868 9.5672C7.10109 10.0982 6.79352 10.6328 6.48227 11.1637C6.46762 11.1637 6.46396 11.1564 6.4603 11.1454C6.45664 11.1161 6.46763 11.0905 6.47495 11.0612C6.62508 10.5119 6.50058 10.0249 6.12342 9.60748C5.89272 9.35482 5.5998 9.21203 5.26292 9.16441C4.82353 9.10216 4.43538 9.21567 4.08751 9.49031C4.02526 9.53791 3.985 9.60748 3.91177 9.6441C3.89709 9.6441 3.88977 9.63678 3.88611 9.62579C4.06187 9.32186 4.23398 9.01796 4.40975 8.71401C5.13476 7.4544 5.85977 6.19476 6.58846 4.93882C6.59579 4.92415 6.60677 4.91319 6.6141 4.89851Z" fill="#F0B628" />
        <path fillRule="evenodd" clipRule="evenodd" d="M3.90075 9.63677C3.97033 9.57452 4.03624 9.50861 4.10948 9.45002C4.99927 8.74698 6.3358 9.25595 6.52987 10.3691C6.57748 10.6364 6.55182 10.8927 6.47128 11.149C6.46762 11.171 6.46396 11.1893 6.45664 11.2113C6.42368 11.2699 6.39439 11.3321 6.35777 11.3907C6.03187 11.929 5.5522 12.1963 4.92237 12.156C4.20102 12.1047 3.63347 11.5628 3.53457 10.8451C3.487 10.4973 3.55657 10.1714 3.73597 9.87113C3.77262 9.80521 3.81653 9.7466 3.85681 9.68071C3.87512 9.66607 3.8678 9.63677 3.90075 9.63677Z" fill="#309F4E" />
      </g>
      <path fillRule="evenodd" clipRule="evenodd" d="M3.90992 9.63677C3.892 9.65142 3.892 9.67705 3.86062 9.68071C3.85614 9.65508 3.87407 9.64043 3.892 9.6221L3.90992 9.63677Z" fill="#F0B628" />
      <path fillRule="evenodd" clipRule="evenodd" d="M6.45914 11.2113C6.43843 11.1857 6.45914 11.1674 6.47985 11.149C6.48503 11.1527 6.49538 11.1601 6.50056 11.1637L6.45914 11.2113Z" fill="#D8BA2F" />
    </svg>
  );
}

GoogleAdsCircle.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(iconSizes),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  padding: PropTypes.number,
};

GoogleAdsCircle.defaultProps = {
  size: 24,
  className: "",
  rotation: 0,
  padding: 0,
  onMouseEnter: () => { },
  onMouseLeave: () => { },
};

export default GoogleAdsCircle;
