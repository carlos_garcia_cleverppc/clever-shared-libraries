import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import makeStyles from "../styles/makeStyles";
import { iconSizes } from "../styles/icons";

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  },
}));

function MicrosoftCircle(props) {
  const classes = getClasses(props);
  const { size, padding, className, onMouseEnter, onMouseLeave } = props;

  let sizeIcon = size;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave,
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path
        d="M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16Z"
        fill="#DCF5FF"
      />
      <path d="M8.32001 7.68H12.16V4.096C12.16 3.95468 12.0453 3.84 11.904 3.84H8.32001V7.68Z" fill="#4CAF50" />
      <path d="M7.68003 7.68V3.84H4.09603C3.95471 3.84 3.84003 3.95468 3.84003 4.096V7.68H7.68003Z" fill="#F44336" />
      <path d="M7.68003 8.32H3.84003V11.904C3.84003 12.0453 3.95471 12.16 4.09603 12.16H7.68003V8.32Z" fill="#2196F3" />
      <path d="M8.32001 8.32V12.16H11.904C12.0453 12.16 12.16 12.0453 12.16 11.904V8.32H8.32001Z" fill="#FFC107" />
    </svg>
  );
}

MicrosoftCircle.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(iconSizes),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  padding: PropTypes.number,
};

MicrosoftCircle.defaultProps = {
  size: 24,
  className: "",
  rotation: 0,
  padding: 0,
  onMouseEnter: () => { },
  onMouseLeave: () => { },
};

export default MicrosoftCircle;
