import React from "react";
import PropTypes from "prop-types";

import isString from "lodash-es/isString";

import { iconSizes } from "../styles/icons";
import TwitterCircle from "./TwitterCircle";
import GoogleMerchantCircle from "./GoogleMerchantCircle";
import GoogleAdsCircle from "./GoogleAdsCircle";
import FacebookCircle from "./FacebookCircle";
import GoogleAnalyticsCircle from "./GoogleAnalyticsCircle";
import MicrosoftCircle from "./MicrosoftCircle";
import LinkedinCircle from "./LinkedinCircle";
import InstagramCircle from "./InstagramCircle";
import TwitterCircleUnlinked from "./TwitterCircleUnlinked";
import GoogleAdsCircleUnlinked from "./GoogleAdsCircleUnlinked";
import FacebookCircleUnlinked from "./FacebookCircleUnlinked";
import MicrosoftCircleUnlinked from "./MicrosoftCircleUnlinked";
import LinkedinCircleUnlinked from "./LinkedinCircleUnlinked";
import InstagramCircleUnlinked from "./InstagramCircleUnlinked";
import WhatsAppIcon from "./WhatsAppIcon";
import TelegramIcon from "./TelegramIcon";
import FacebookMessengerIcon from "./FacebookMessengerIcon";

const ICONS_MAP = {
  twittercircle: TwitterCircle,
  googlemerchantcircle: GoogleMerchantCircle,
  googleadscircle: GoogleAdsCircle,
  facebookcircle: FacebookCircle,
  googleanalyticscircle: GoogleAnalyticsCircle,
  microsoftcircle: MicrosoftCircle,
  linkedincircle: LinkedinCircle,
  instagramcircle: InstagramCircle,
  twittercircleunlinked: TwitterCircleUnlinked,
  googleadscircleunlinked: GoogleAdsCircleUnlinked,
  facebookcircleunlinked: FacebookCircleUnlinked,
  microsoftcircleunlinked: MicrosoftCircleUnlinked,
  linkedincircleunlinked: LinkedinCircleUnlinked,
  instagramcircleunlinked: InstagramCircleUnlinked,
  whatsappicon: WhatsAppIcon,
  telegramicon: TelegramIcon,
  facebookmessengericon: FacebookMessengerIcon,
};

const BrandIcon = ({ icon, ...props }) => {
  const name = icon.trim().toLowerCase();
  const IconSelected = ICONS_MAP[name];

  if (!IconSelected) {
    return null;
  }

  return <IconSelected {...props} />;
};

BrandIcon.propTypes = {
  icon: ({ icon }) => {
    if (!isString(icon)) {
      return new Error(
        "Invalid prop icon supplied to Icon Component. Validation failed.",
      );
    }

    if (!ICONS_MAP[icon.trim().toLowerCase()]) {
      return new Error(
        "Invalid prop icon supplied to Icon Component. Validation failed. INVALID ICON",
      );
    }

    return null;
  },
  className: PropTypes.string,
  size: PropTypes.oneOf(iconSizes),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  padding: PropTypes.number,
};

BrandIcon.defaultProps = {
  icon: "",
  size: 24,
  className: "",
  rotation: 0,
  padding: 0,
  onMouseEnter: () => { },
  onMouseLeave: () => { },
};

export default BrandIcon;
