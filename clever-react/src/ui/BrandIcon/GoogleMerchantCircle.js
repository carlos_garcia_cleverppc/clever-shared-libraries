import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import makeStyles from "../styles/makeStyles";
import { iconSizes } from "../styles/icons";

const getClasses = makeStyles(() => ({
  root: ({ rotation, padding, size }) => {
    const res = {};

    if (rotation > 0) {
      res.transform = `rotate(${rotation}deg)`;
    }

    if (padding > 0) {
      res.padding = `${(size * padding) / 200}px`;
    }

    return res;
  },
}));

function GoogleMerchantCircle(props) {
  const classes = getClasses(props);
  const { size, padding, className, onMouseEnter, onMouseLeave } = props;

  let sizeIcon = size;
  const otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter,
    onMouseLeave,
  };

  if (padding) {
    sizeIcon = (sizeIcon * (100 - padding)) / 100;
  }

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <svg
      viewBox="0 0 16 16"
      fill="none"
      height={sizeIcon}
      width={sizeIcon}
      {...otherProps}
    >
      <path d="M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16Z" fill="#E7E8E9" />
      <path d="M3.5 5.63043H12.5V11.7472C12.5 12.163 12.1643 12.5 11.7501 12.5H4.24993C3.83574 12.5 3.5 12.1629 3.5 11.7472V5.63043Z" fill="#518FF5" />
      <path d="M6.67389 9.97368L9.00995 7.84782L12.5 11.316L12.4984 11.7539C12.4968 12.166 12.16 12.5 11.746 12.5H9.2235L6.67389 9.97368Z" fill="url(#paint0_linear)" />
      <path d="M3.5 4.24102C3.5 3.83177 3.8357 3.5 4.24993 3.5H11.7501C12.1643 3.5 12.5 3.83172 12.5 4.24102V5.63043H3.5V4.24102Z" fill="#4758B8" />
      <path d="M7.62657 4.53622H6.73961C6.55922 4.53622 6.41302 4.68384 6.41302 4.86595C6.41302 5.04808 6.55924 5.19565 6.73961 5.19565H9.26035C9.44074 5.19565 9.58694 5.04803 9.58694 4.86595C9.58694 4.6838 9.44072 4.53622 9.26035 4.53622H8.37337V4.44203C8.37337 4.23383 8.20621 4.06522 7.99998 4.06522C7.79371 4.06522 7.62657 4.23392 7.62657 4.44203V4.53622Z" fill="white" />
      <path d="M6.56198 7.90217C6.3741 7.90217 6.22178 7.74985 6.22178 7.56198C6.22178 7.3741 6.3741 7.22178 6.56198 7.22178C6.74985 7.22178 6.90217 7.3741 6.90217 7.56198C6.90217 7.74985 6.74985 7.90217 6.56198 7.90217ZM10.1914 8.94761L8.13707 6.89326C8.05489 6.81109 7.94076 6.76087 7.81522 6.76087H6.21739C6.09619 6.76045 5.97982 6.80841 5.89412 6.89412C5.80841 6.97982 5.76045 7.09619 5.76087 7.21739V8.81522C5.76087 8.94076 5.81109 9.05489 5.89554 9.13707L7.94761 11.1914C8.03207 11.2736 8.1462 11.3261 8.27174 11.3261C8.39728 11.3261 8.51141 11.2736 8.59359 11.1914L10.1914 9.59359C10.2759 9.51141 10.3261 9.39728 10.3261 9.27174C10.3261 9.14391 10.2736 9.02978 10.1914 8.94761Z" fill="black" />
      <path fillRule="evenodd" clipRule="evenodd" d="M6.56198 7.90217C6.3741 7.90217 6.22178 7.74985 6.22178 7.56198C6.22178 7.3741 6.3741 7.22178 6.56198 7.22178C6.74985 7.22178 6.90217 7.3741 6.90217 7.56198C6.90217 7.74985 6.74985 7.90217 6.56198 7.90217ZM10.1914 8.94761L8.13707 6.89326C8.05489 6.81109 7.94076 6.76087 7.81522 6.76087H6.21739C6.09619 6.76045 5.97982 6.80841 5.89412 6.89412C5.80841 6.97982 5.76045 7.09619 5.76087 7.21739V8.81522C5.76087 8.94076 5.81109 9.05489 5.89554 9.13707L7.94761 11.1914C8.03207 11.2736 8.1462 11.3261 8.27174 11.3261C8.39728 11.3261 8.51141 11.2736 8.59359 11.1914L10.1914 9.59359C10.2759 9.51141 10.3261 9.39728 10.3261 9.27174C10.3261 9.14391 10.2736 9.02978 10.1914 8.94761Z" fill="white" />
      <defs>
        <linearGradient id="paint0_linear" x1="12.5" y1="12.4778" x2="7.95351" y2="6.83837" gradientUnits="userSpaceOnUse">
          <stop stopColor="#4385F5" />
          <stop offset="1" stopColor="#3569D3" />
        </linearGradient>
      </defs>
    </svg>
  );
}

GoogleMerchantCircle.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(iconSizes),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  padding: PropTypes.number,
};

GoogleMerchantCircle.defaultProps = {
  size: 24,
  className: "",
  rotation: 0,
  padding: 0,
  onMouseEnter: () => { },
  onMouseLeave: () => { },
};

export default GoogleMerchantCircle;
