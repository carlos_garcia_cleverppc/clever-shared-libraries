/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import RoundedProgressBar from "./index";

export default {
  title: "RoundedProgressBar",
  component: RoundedProgressBar,
};

const Template = args => <RoundedProgressBar {...args} />;

export const SimpleRoundedProgressBar = Template.bind({});
SimpleRoundedProgressBar.args = {
  size: 145,
  progress: 40,
  strokeWidth: 4,
  progressesArray:
    [
      { color: "blue", progress: 20 },
      { color: "danger", progress: 5 },
      { color: "green", progress: 75 },
    ],
};
