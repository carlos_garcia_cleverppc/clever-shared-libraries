import makeStyles from "../styles/makeStyles";

const styles = (({ cleverUI: { colors } }) => ({
  roundedProgressContainer: {
    display: "grid",
    alignItems: "center",
    justifyItems: "center",
  },
  children: { gridArea: "1/1" },
  svg: { transform: "rotate(-90deg)", gridArea: "1/1" },
  svgCircleBg: { fill: "none", stroke: colors.neutral[300] },
  svgCircle: ({ color, shade }) => ({ fill: "none", stroke: colors[color][shade], strokeLinecap: "round" }),
  svgCircleMultiple: { fill: "none", strokeLinecap: "round" },
  svgCircleText: {
    fontSize: "2rem",
    textAnchor: "middle",
    fill: "#fff",
    fontWeight: "bold",
  },
}));

export default makeStyles(styles);
