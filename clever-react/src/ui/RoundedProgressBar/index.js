/* eslint-disable no-extra-boolean-cast */
import React, { useEffect, useState, useRef, createRef } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { getColorsLabels, getShades } from "../styles/colors";
import getClasses from "./styles";
import useTheme from "../styles/useTheme";

const colorsList = getColorsLabels(true);
const shadesList = getShades(true);

const BudgetManagement = props => {
  const { size, strokeWidth, circleOneStroke, circleTwoStroke, children } = props;
  const { progress, color, shade, progressesArray, className, id } = props;

  const classes = getClasses({ color, shade });
  const { cleverUI: { colors } } = useTheme();

  const circleRef = useRef(null);
  const circleRefs = useRef([]);

  if (circleRefs.current.length !== progressesArray.length) {
    circleRefs.current = Array(progressesArray.length).fill().map((_, i) => circleRefs.current[i] || createRef());
  }

  const center = size / 2;
  const radius = size / 2 - strokeWidth / 2;
  const circumference = 2 * Math.PI * radius;

  const [offset, setOffset] = useState(1 * circumference);

  const [offsetValues, setOffsetValues] = useState(progressesArray.map(() => 1 * circumference));

  useEffect(() => {
    if (progressesArray.length === 0) {
      const progressOffset = ((100 - progress) / 100) * circumference;
      setOffset(progressOffset);
      circleRef.current.style = "transition: stroke-dashoffset 1250ms ease-in-out;";
    } else {
      const progresses = progressesArray.reverse().map(prog => prog.progress).reverse();
      let index = 0;
      const arr = [...offsetValues];

      while (index < progressesArray.length) {
        const progressOffset = ((100 - progresses[index]) / 100) * circumference;

        arr[index] = progressOffset;

        if (index < progresses.length - 1) progresses[index + 1] += progresses[index];

        circleRefs.current[index].current.style = "transition: stroke-dashoffset 1250ms ease-in-out;";

        index += 1;
      }

      setOffsetValues(arr.reverse());
    }
  }, [setOffset, circumference, progress, progressesArray, offset]);

  return (
    <div className={classnames(classes.roundedProgressContainer, className)} id={id}>
      <div className={classes.children}>
        {children}
      </div>

      {progressesArray.length === 0 && (
        <svg className={classes.svg} width={size} height={size}>
          <circle
            className={classes.svgCircleBg}
            stroke={circleOneStroke}
            cx={center}
            cy={center}
            r={radius}
            strokeWidth={strokeWidth}
          />

          <circle
            className={classes.svgCircle}
            ref={circleRef}
            stroke={circleTwoStroke}
            cx={center}
            cy={center}
            r={radius}
            strokeWidth={strokeWidth}
            strokeDasharray={circumference}
            strokeDashoffset={offset}
          />
        </svg>
      )}

      {progressesArray.length > 0 && (
        <svg className={classes.svg} width={size} height={size}>
          <circle
            className={classes.svgCircleBg}
            stroke={circleOneStroke}
            cx={center}
            cy={center}
            r={radius}
            strokeWidth={strokeWidth}
          />

          {progressesArray.map((prog, index) => (
            <circle
              className={classes.svgCircleMultiple}
              stroke={colors[prog.color][!!prog.shade ? prog.shade : 500]}
              cx={center}
              cy={center}
              r={radius}
              strokeWidth={strokeWidth}
              strokeDasharray={circumference}
              strokeDashoffset={offsetValues[index]}
              ref={circleRefs.current[index]}
              key={`circle-${index}`}
            />
          ))}
        </svg>
      )}
    </div>
  );
};

BudgetManagement.propTypes = {
  size: PropTypes.number.isRequired,
  color: PropTypes.oneOf(colorsList),
  shade: PropTypes.oneOf(shadesList),
  progress: PropTypes.number,
  strokeWidth: PropTypes.number.isRequired,
  circleOneStroke: PropTypes.string,
  circleTwoStroke: PropTypes.string,
  progressesArray: PropTypes.instanceOf(Object),
};

BudgetManagement.defaultProps = {
  color: "green",
  shade: 500,
  progress: 0,
  circleOneStroke: "",
  circleTwoStroke: "",
  progressesArray: [],
};

export default BudgetManagement;
