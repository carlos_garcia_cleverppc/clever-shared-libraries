/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Divider from "./index";

export default {
  title: "Divider",
  component: Divider,
};

const Template = args => <Divider {...args} />;

export const SimpleDivider = Template.bind({});
SimpleDivider.args = {};
