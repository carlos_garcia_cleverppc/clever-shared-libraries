import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  horizontal: {
    ...spacing["w-100"],
    minWidth: "100px",
    border: 0,
    height: "1px",
    backgroundColor: colors.neutral[300],
  },
  vertical: {
    ...spacing["h-100"],
    minHeight: "20px",
    border: 0,
    width: "1px",
    backgroundColor: colors.neutral[300],
  },
  dashed: {
    borderTop: `2px dashed ${colors.neutral[300]}`,
    backgroundColor: "unset",
    height: "0px",
  },
});

export default makeStyles(styles);
