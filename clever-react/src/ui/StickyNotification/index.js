import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import SnackbarMaterial from "@material-ui/core/Snackbar";

import { getColorsLabels } from "../styles/colors";

import Grid from "../Grid";
import IconButton from "../IconButton";
import Image from "../Image";
import Times from "../Icon/Times";
import Typography from "../Typography";
import Link from "../Link";

import getClasses from "./styles";

const colorsList = getColorsLabels();

const StickyNotification = React.forwardRef((props, ref) => {
  const { className, onClose, open, title, description } = props;
  const { color, image, anchorEl, id, footer, textLink, onClickLink, ...otherProps } = props;

  const classes = getClasses({ color });

  return (
    <SnackbarMaterial
      ref={ref}
      className={classnames([className, classes.root])}
      onClose={onClose}
      open={open}
      {...otherProps}
      ClickAwayListenerProps={{ onClickAway: () => { } }}
    >
      <div className={classes.content}>
        <Grid container className={classes.container}>
          <Grid item xs={1} className={classnames(classes.marginX, classes.marginY, classes.timesIcon)}>
            <IconButton icon={<Times />} color="white" onClick={onClose} iconSize={16} />
          </Grid>

          <Grid item xs={9} className={classnames(classes.marginY)}>
            <Typography variant="f1-14" weight="bold" color="white">{title}</Typography>

            <Typography variant="f2-12" color="white">
              {description}

              {textLink && (
                <Link
                  className={classes.link}
                  onClick={onClickLink}
                  color="white"
                >
                  {textLink}
                </Link>
              )}
            </Typography>
          </Grid>

          <Grid item xs={2}>
            <Image src={image} className={classes.image} />
          </Grid>
        </Grid>

        {footer}
      </div>
    </SnackbarMaterial>
  );
});

StickyNotification.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  open: PropTypes.bool,
  onClose: PropTypes.func,
  textLink: PropTypes.string,
  onClickLink: PropTypes.func,
  color: PropTypes.oneOf(colorsList),
  anchorOrigin: PropTypes.exact({
    vertical: PropTypes.oneOf(["top", "bottom"]),
    horizontal: PropTypes.oneOf(["left", "right", "center"]),
  }),
  footer: PropTypes.element,
};

StickyNotification.defaultProps = {
  title: "",
  description: "",
  open: false,
  onClose: () => { },
  textLink: "",
  onClickLink: () => { },
  color: "primary",
  anchorOrigin: {
    vertical: "top",
    horizontal: "right",
  },
};

export default StickyNotification;
