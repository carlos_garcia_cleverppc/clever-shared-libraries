import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing, typography } }) => ({
  root: {
    width: "384px",
    minHeight: "82px",
    position: "relative",
  },
  content: ({ color }) => ({
    height: "100%",
    color: "white",
    backgroundColor: colors[color]?.[500] || colors.primary[500],
    borderRadius: "8px",
  }),
  container: {
    flexWrap: "nowrap",
    "& > :last-child": {
      marginRight: 0, // remove negative margin of Material ui
    },
  },
  marginY: { ...spacing["my-3"] },
  marginX: { ...spacing["mx-2"] },
  timesIcon: {
    "& button": { paddingTop: "4px" },
    "& path": { fill: "white !important" },
  },
  link: { ...spacing["ml-1"], fontFamily: typography.font2, fontSize: "12px" },
  image: { ...spacing["mb-2"], height: "76px", width: "auto", borderRadius: "8px", marginRight: "0px" },
});

export default makeStyles(styles);
