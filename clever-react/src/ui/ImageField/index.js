/* eslint-disable no-unused-expressions */
/* eslint-disable no-restricted-properties */
import React, { useRef, useState } from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import Loader from "../Loading";
import getClasses from "./styles";
import Preview from "./Preview";
import useIsDragover from "./useIsDragover";
import { getBase64Image, bytesToMb } from "../../utils/files";
import mergeRefs from "./mergeRefs";

export const TYPES_ERRORS = {
  limitSize: "Limit size",
  invalidFormat: "Invalid format",
  parse: "Parse image",
};

const ImageField = React.forwardRef(({ value, onChange, className, mbLimit, onError, size, label, accept }, ref) => {
  const [loading, setLoading] = useState(false);
  const wrapperRef = useRef(null);
  const inputRef = useRef(null);
  const classes = getClasses({ size });
  const isDragover = useIsDragover(wrapperRef);

  const isValidSize = bytes => bytesToMb(bytes) <= mbLimit;

  const isValidFormat = format => format && format.startsWith("image/");

  const clearInputFile = () => {
    if (inputRef.current) {
      inputRef.current.value = "";
    }
  };

  const handleChange = ev => {
    const file = ev.target.files[0];
    if (!file) return;

    if (!isValidFormat(file.type)) {
      onError(TYPES_ERRORS.invalidFormat);
      return;
    }

    if (!isValidSize(file.size)) {
      onError(TYPES_ERRORS.limitSize);
      return;
    }

    setLoading(true);

    getBase64Image(file)
      .then(onChange)
      .catch(() => onError(TYPES_ERRORS.parse))
      .finally(() => {
        clearInputFile();
        setLoading(false);
      });
  };

  const classNameInputArea = cx(
    classes.inputArea,
    className,
    { [classes.dragging]: isDragover },
  );

  const availableFormats = Array.isArray(accept)
    ? accept.map(format => `image/${format}`).join(", ")
    : `image/${accept}`;

  return (
    <div ref={wrapperRef} className={classNameInputArea} aria-busy={loading}>
      <Preview src={value} size={size} />

      <input
        aria-label={label}
        disabled={loading}
        ref={mergeRefs(inputRef, ref)}
        type="file"
        onChange={handleChange}
        className={classes.file}
        accept={availableFormats}
      />

      <div className={classes.dragNotice} />

      { loading
              && <div className={classes.loading}><Loader /></div>}
    </div>
  );
});

ImageField.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.string,
  size: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  mbLimit: PropTypes.number,
  onError: PropTypes.func,
  accept: PropTypes.oneOfType([
    PropTypes.oneOf(["*", "jpg", "jpeg", "png"]),
    PropTypes.arrayOf(["*", "jpg", "jpeg", "png"]),
  ]),
  label: PropTypes.string,
};

ImageField.defaultProps = {
  onChange: () => { },
  value: "",
  size: 120,
  mbLimit: 10,
  accept: "*",
  onError: () => {},
  label: "Upload image",
};

export default ImageField;
