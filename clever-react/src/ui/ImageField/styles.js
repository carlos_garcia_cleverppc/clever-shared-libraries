import makeStyles from "../styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { colors } }) => ({
  inputArea: ({ size }) => ({
    position: "relative",
    display: "grid",
    width: size,
    height: size,
    "& > *": {
      maxWidth: size, // fix ios input file overflow area
      gridArea: "1/ -1", // fill space with each component
    },
  }),
  file: {
    opacity: 0,
    cursor: "pointer",
  },
  dragNotice: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    backgroundColor: colors.neutral[600],
    borderRadius: 12,
    transform: "scale(.8)",
    opacity: 0,
    transition: "all .15s",
    pointerEvents: "none",
  },
  dragging: {
    "& $dragNotice": {
      transform: "scale(1)",
      opacity: 0.12,
    },
  },
  loading: {
    display: "grid",
    placeItems: "center",
  },
}));

export default getClasses;
