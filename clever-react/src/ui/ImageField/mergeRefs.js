const isDefined = value => value != null;

const mergeRefs = (...refs) => value => {
  refs.forEach(ref => {
    if (typeof ref === "function") {
      ref(value);
    } else if (isDefined(ref)) {
      ref.current = value;
    }
  });
};

export default mergeRefs;
