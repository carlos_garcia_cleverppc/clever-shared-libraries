/* eslint-disable consistent-return */
import { useEffect, useState } from "react";

const useIsDragover = ref => {
  const [isDragover, setIsDragover] = useState(false);

  useEffect(() => {
    const el = ref.current;
    if (!el) return;

    const handleDrag = () => setIsDragover(true);
    const handleDragOut = () => setIsDragover(false);

    el.addEventListener("dragleave", handleDragOut);
    el.addEventListener("drop", handleDragOut);
    el.addEventListener("dragover", handleDrag);

    return () => {
      el.removeEventListener("dragleave", handleDragOut);
      el.removeEventListener("drop", handleDragOut);
      el.removeEventListener("dragover", handleDrag);
    };
  }, [ref]);

  return isDragover;
};

export default useIsDragover;
