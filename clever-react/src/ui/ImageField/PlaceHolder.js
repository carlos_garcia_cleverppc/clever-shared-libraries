import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import makeStyles from "../styles/makeStyles";
import ImageIcon from "../Icon/Image";
import Card from "../Card";

const getClasses = makeStyles(() => ({
  root: ({ size }) => ({
    width: size,
    height: size,
    display: "grid",
    placeItems: "center",
    boxSizing: "border-box",
    marginBottom: 0, // override Card styles
  }),
}));

const PlaceHolder = ({ icon, className, size }) => {
  const classes = getClasses({ size });
  return (
    <Card variant="striped" className={cx(classes.root, className)}>{icon}</Card>
  );
};

PlaceHolder.propTypes = {
  icon: PropTypes.element,
  size: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

PlaceHolder.defaultProps = {
  icon: <ImageIcon size={32} />,
  size: 120,
};

export default PlaceHolder;
