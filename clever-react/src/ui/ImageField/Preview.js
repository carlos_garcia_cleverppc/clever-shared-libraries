import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import makeStyles from "../styles/makeStyles";
import PlaceHolder from "./PlaceHolder";

const getClasses = makeStyles(({ cleverUI: { colors } }) => ({
  outline: {
    boxShadow: "inset 0 0 2px rgba(0,0,0, .2)",
    borderRadius: 12,
  },
  image: ({ size }) => ({
    borderRadius: 12,
    objectFit: "contain",
    width: size,
    height: size,
  }),
  background: {
    borderRadius: 12,
    backgroundColor: colors.gray.lighter,
    opacity: 0.05,
  },
}));

const Preview = ({ src, className, size }) => {
  const classes = getClasses({ size });

  if (!src) {
    return <PlaceHolder />;
  }

  return (
    <>
      <div className={classes.background} />
      <img className={cx(classes.image, className)} src={src} alt="" />
      <div className={classes.outline} />
    </>
  );
};

Preview.propTypes = {
  src: PropTypes.string,
  size: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

Preview.defaultProps = {
  src: "",
  size: 120,
};

export default Preview;
