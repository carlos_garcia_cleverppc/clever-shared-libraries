import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ThemeProvider from "../../Provider";
import ImageField, { TYPES_ERRORS } from "../index";

const fileImage = new File(["12345"], "image.jpg", { type: "image/jpg" });
const base64FileImage = "data:image/jpg;base64,MTIzNDU=";

describe("ImageField", () => {
  test("should upload image", () => {
    render(
      <ThemeProvider>
        <ImageField />
      </ThemeProvider>,
    );
    const input = screen.getByLabelText(/Upload image/i);
    userEvent.upload(input, fileImage);

    expect(input.files[0]).toStrictEqual(fileImage);
    expect(input.files).toHaveLength(1);
  });

  test("should render a preview when has image value", () => {
    render(
      <ThemeProvider>
        <ImageField value={base64FileImage} />
      </ThemeProvider>,
    );
    const image = screen.getByRole("img");

    expect(image.src).toBe(base64FileImage);
  });

  test("should transform file when change file input", async () => {
    const onChange = jest.fn();

    render(
      <ThemeProvider>
        <ImageField onChange={onChange} />
      </ThemeProvider>,
    );

    const input = screen.getByLabelText(/Upload image/i);
    fireEvent.change(input, { target: { files: [fileImage] } });

    await waitFor(() => {
      expect(onChange).toBeCalledWith(base64FileImage);
    });
  });

  test("should call onError with invalid file format", async () => {
    const pdf = new File(["hello"], "hello.pdf", { type: "application/pdf" });
    const onChange = jest.fn();
    const onError = jest.fn();

    render(
      <ThemeProvider>
        <ImageField onError={onError} onChange={onChange} />
      </ThemeProvider>,
    );

    const input = screen.getByLabelText(/Upload image/i);
    fireEvent.change(input, { target: { files: [pdf] } });

    await waitFor(() => {
      expect(onError).toBeCalledWith(TYPES_ERRORS.invalidFormat);
    });
  });

  test("should call onError when exceeds the max size", async () => {
    // eslint-disable-next-line no-restricted-properties
    const SIZE_12MB = 12 * Math.pow(1024, 2);

    const bigImage = {
      type: "image/png",
      size: SIZE_12MB,
    };

    const onChange = jest.fn();
    const onError = jest.fn();

    render(
      <ThemeProvider>
        <ImageField mbLimit={10} onError={onError} onChange={onChange} />
      </ThemeProvider>,
    );

    const input = screen.getByLabelText(/Upload image/i);
    fireEvent.change(input, { target: { files: [bigImage] } });

    await waitFor(() => {
      expect(onError).toBeCalledWith(TYPES_ERRORS.limitSize);
    });
  });
});
