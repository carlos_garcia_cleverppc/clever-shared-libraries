/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import CheckAccountItem from "./index";

export default {
  title: "CheckAccountItem",
  component: CheckAccountItem,
};

const Template = args => {
  const [checked, setChecked] = React.useState(false);

  return <CheckAccountItem {...args} isSelected={checked} onClick={() => setChecked(!checked)} />;
};

export const SimpleCheckAccountItem = Template.bind({});
SimpleCheckAccountItem.args = {
  name: "Account name",
  currency: "Currency",
  timezone: "Timezone",
  id: "id",
};
