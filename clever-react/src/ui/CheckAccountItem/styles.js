import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { spacing } }) => ({
  root: {
    ...spacing["py-3"],
    ...spacing["px-4"],
    ...spacing["my-2"],

  },
  longText: { wordBreak: "break-word" },
  label: { ...spacing["ml-1"] },
  choosable: { cursor: "pointer" },
});

export default makeStyles(styles);
