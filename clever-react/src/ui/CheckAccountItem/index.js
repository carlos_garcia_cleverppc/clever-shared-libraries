/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import Card from "../Card";
import Checkbox from "../Checkbox";
import Typography from "../Typography";

import getClasses from "./styles";

const CheckAccountItem = props => {
  const classes = getClasses();

  const { name, id, currency, timezone } = props;
  const { isSelected, disabled, onClick, color } = props;

  return (
    <Card
      className={classnames(classes.root, disabled ? "" : classes.choosable)}
      clickable={!disabled}
      onClick={disabled ? () => { } : onClick}
    >
      <Checkbox
        checked={isSelected}
        onChange={onClick}
        value="privacy"
        disabled={disabled}
        color={color}
      >
        <div className={classes.label}>
          {!disabled && <Typography variant="body1" shade={900} weight="bold" className={classes.longText}>{name}</Typography>}

          {disabled && <Typography variant="body1" weight="bold" className={classes.longText}>{name}</Typography>}

          {!!id && <Typography variant="body1" className={classes.longText}>{id}</Typography>}

          {(!!currency || !!timezone) && (
            <Typography variant="body0" className={classes.longText}>
              {currency}
              {!!currency && !!timezone ? " - " : ""}
              {timezone}
            </Typography>
          )}
        </div>
      </Checkbox>
    </Card>
  );
};

CheckAccountItem.propTypes = {
  isSelected: PropTypes.bool,
  name: PropTypes.string,
  id: PropTypes.string,
  currency: PropTypes.string,
  timezone: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  color: PropTypes.oneOf(["primary", "secondary"]),
};

CheckAccountItem.defaultProps = {
  isSelected: false,
  name: "",
  id: "",
  currency: "",
  timezone: "",
  disabled: false,
  onClick: () => { },
  color: "primary",
};

export default CheckAccountItem;
