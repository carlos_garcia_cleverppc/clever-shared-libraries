import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import makeStyles from "../../styles/makeStyles";

const getClasses = makeStyles(() => ({
  root: ({ columns, rows }) => ({
    display: "block",
    gridColumn: `span ${columns}`,
    gridRow: `span ${rows}`,
  }),
  children: { display: "block" },
}));

const Item = ({ columns, rows, children, layoutConfig, className }) => {
  const classes = getClasses({ columns, rows });

  if (!layoutConfig || Object.keys(layoutConfig).length === 0) return new Error("\"Item\": Empty layout config");

  return (
    <div className={classnames(classes.root, className)}>
      {children}
    </div>
  );
};

Item.propTypes = {
  columns: PropTypes.oneOf([1, 2, 3, 4]),
  rows: PropTypes.number,
  className: PropTypes.string,
};

Item.defaultProps = { columns: 1, rows: 1, className: "" };

export default Item;
