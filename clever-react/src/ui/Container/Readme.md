

# CSS-GRID React
Sistema de Grid basado en css-grid de css3 adaptado a componentes para react. La finalidad de la implementación es resolver el problema de distribuir los bloques desarrollados por Fran (160x160) dentro de la interfaces aportando una solución que podes usar facilmente dentro de cada proyecto. Hay 2 modos de usar el sistema de grids, por un lado modo de bloques que ajusta el espacio a los tamaños de bloques (160x160) y por otro lado el grid automático que realiza el ajuste del grid al contenido que tenga disponible. Este sistema se divide en:
  - Contenedor (Container): Es donde defininimos como tiene que ser el layout, columnas, filas, alineado, ...
  - Bloque (Item): Es donde definimos cuando espacio de nuestro grid se le asigna para contruir dentro un nuevo elemento o bloque.

La documentación base para construir estos componentes se ha basado en https://css-tricks.com/snippets/css/complete-guide-grid/

## Propiedades del contenedor (Container)
### ClassName
Propiedad usada para añadir una clase de css al container
**Value**: String
**Default**: ""

### columns*
Se refiere al número de columnas que queremos colocar en nuestro grid.

**Value**: [1,2,3,4]

### rows*
Se refiere al número de filas que queremos colocar en nuestro grid. Por defecto ese valor es orientativo por que si la interface necesitase mas filas, el sistema automaticamente las añade con el tamaño necesario, esto será muy interesante sobre todo en responsive donde el grid puede ser diferente o sea necesario fijarlo.

**Value**: Number

### columnGap
Se utiliza para definir el espacio en pixeles que tenemos entre cada columna del grid

**Value**: Number
**Default**: 24 --> GAP_SIZE

### rowGap
Se utiliza para definir el espacio en pixeles que tenemos entre cada fila del grid

**Value**: Number
**Default**: 24 --> GAP_SIZE

### alignColumns
Define como se posiciona el bloque dentro de cada columna.

**Value**: String -- ["start", "end", "center", "stretch"]
**Default**: "stretch"

### alignRows
Define como se posiciona el bloque dentro de cada fila.

**Value**: String -- ["start", "end", "center", "stretch"]
**Default**: "stretch"

### maxWidth
Es la propiedad usada para definir el ancho máximo que se destina al container. Para el caso de querer utilizar el 100% del ancho disponible utilizaremos el valor "auto" y para el caso del sistema de bloques usaremos "blocks".

**Value**: String | undefined -- ["auto", "blocks", undefined]
**Default**: undefined

  - Import a HTML file and watch it magically convert to Markdown
  - Drag and drop images (requires your Dropbox account be linked)

### layout
Esta propiedad es la que define como actua el contenedor a nivel generar (es como el valor por defecto para la configuración) lo utilizaremos como base para contruir por ejemplo un grid donde queremos el 100% de width pero que los bloques tengan el tamaño estandar de los bloques (160x160).

**Value**: String -- ["auto", "blocks"]
**Default**: "blocks"

### columnSize
Lo usaremos para definir el ancho de cada una de las columnas, en caso de querer que el ancho de las columnas se reparta equitativamente en todo el espacio disponible usaremos "auto" para el caso de que el ancho de la columna sea como el del sistema de bloques usaremos "blocks"

**Value**: String -- ["auto", "blocks"]
**Default**: "blocks"

### rowSize
Lo usaremos para definir el alto de cada una de las filas, en caso de querer que el alto de las filas se reparta equitativamente en todo el espacio disponible usaremos "auto" para el caso de que el alto de la filas sea como el del sistema de bloques usaremos "blocks"

**Value**: String -- ["auto", "blocks"]
**Default**: "blocks"

## Propiedades del Bloque (Item)

### ClassName
Propiedad usada para añadir una clase de css al item
**Value**: String
**Default**: ""

### columns*
Se refiere al número de columnas que necesitamos que ocupe nuestro bloque.

**Value**: [1,2,3,4]
**Default**: 1

### rows*
Se refiere al número de filas que necesitamos asignar a nuestro bloque

**Value**: Number
**Default**: 1
