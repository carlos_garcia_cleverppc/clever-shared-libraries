/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable object-curly-newline */
import React from "react";
import PropTypes from "prop-types";
import isArray from "lodash-es/isArray";
import map from "lodash-es/map";
import classnames from "classnames";

import makeStyles from "../styles/makeStyles";

const GAP_SIZE = 24;
const ROW_SIZE = 160;
const COLUMN_SIZE = 160;

function getLayoutValue(layout, value) {
  return value || layout;
}

function isAutoProp(layout, value) {
  return getLayoutValue(layout, value).toLowerCase() === "auto";
}

const getClasses = makeStyles(() => ({
  root: ({
    children, columns, rows, maxWidth, columnSize, rowGap,
    columnGap, rowSize, layout, alignRows, alignColumns,
  }) => {
    const nodes = isArray(children) ? children : [children];

    const gridTemplateColumns = isAutoProp(layout, columnSize) ? `repeat(${!columns ? nodes.length : columns}, auto)` : `repeat(${!columns ? nodes.length : columns}, ${(COLUMN_SIZE * (!columns ? nodes.length : columns)) / (!columns ? nodes.length : columns)}px)`;
    const width = isAutoProp(layout, maxWidth) ? "100%" : `${(COLUMN_SIZE * (!columns ? nodes.length : columns)) + (columnGap * ((!columns ? nodes.length : columns) - 1))}px`;
    const gridTemplateRows = isAutoProp(layout, rowSize) ? `repeat(${rows}, auto)` : `repeat(${rows}, ${ROW_SIZE}px)`;
    const gridAutoRows = isAutoProp(layout, rowSize) ? "auto" : `${ROW_SIZE}px`;

    return {
      display: "grid",
      maxWidth: width,
      width,
      gridTemplateColumns,
      gridTemplateRows,
      gridAutoRows,
      gridAutoFlow: "row",
      columnGap: `${columnGap}px`,
      rowGap: `${rowGap}px`,
      justifyItems: alignColumns,
      alignItems: alignRows,
    };
  },
}));

const Container = props => {
  const classes = getClasses(props);

  const getChildren = () => {
    const { children } = props;

    const nodes = isArray(children) ? children : [children];

    return map(nodes, n => {
      if (n.type) return { ...n, props: { ...n.props, layoutConfig: props } };
      return "";
    });
  };

  return (
    <div className={classnames(classes.root, props.className)}>
      {getChildren()}
    </div>
  );
};

Container.propTypes = {
  columns: PropTypes.oneOf([1, 2, 3, 4, 5, 6, ""]),
  rows: PropTypes.number.isRequired,
  rowGap: PropTypes.number,
  columnGap: PropTypes.number,
  alignColumns: PropTypes.oneOf(["start", "end", "center", "stretch"]),
  alignRows: PropTypes.oneOf(["start", "end", "center", "stretch"]),
  maxWidth: PropTypes.oneOf(["auto", "blocks", undefined]),
  layout: PropTypes.oneOf(["auto", "blocks"]),
  columnSize: PropTypes.oneOf(["auto", "blocks", undefined]),
  rowSize: PropTypes.oneOf(["auto", "blocks", undefined]),
  className: PropTypes.string,
};

Container.defaultProps = {
  maxWidth: undefined,
  rowSize: undefined,
  rowGap: GAP_SIZE,
  columnGap: GAP_SIZE,
  columnSize: undefined,
  columns: "",
  layout: "blocks",
  alignColumns: "stretch",
  alignRows: "stretch",
  className: "",
};

export default Container;
