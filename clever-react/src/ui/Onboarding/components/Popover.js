import React, { useState } from "react";
import PropTypes from "prop-types";
import Popper from "@material-ui/core/Popper";

import makeStyles from "../../styles/makeStyles";

const useStyles = makeStyles(({ cleverUI: { colors, spacing }, zIndex }) => ({
  paper: {
    ...spacing["p-6"],
    backgroundColor: "white",
    maxWidth: "90vw",
    width: "400px",
    borderRadius: 8,
    boxSizing: "border-box",
  },
  popper: {
    zIndex: zIndex.tooltip,
    "&[x-placement*=\"bottom\"] $arrow": {
      top: 0,
      left: 0,
      marginTop: "-10px",
      "&::before": {
        borderWidth: "0 10px 10px 10px",
        borderColor: "transparent transparent white transparent",
      },
    },
    "&[x-placement*=\"top\"] $arrow": {
      bottom: 0,
      left: 0,
      marginBottom: "-10px",
      "&::before": {
        borderWidth: "10px 10px 0 10px",
        borderColor: "white transparent transparent transparent",
      },
    },
  },
  arrow: {
    position: "absolute",
    "&::before": {
      content: "\"\"",
      margin: "auto",
      display: "block",
      width: 0,
      height: 0,
      borderStyle: "solid",
    },
  },
}));

const Popover = React.forwardRef(({ anchorEl, children }, ref) => {
  const classes = useStyles();
  const [arrowRef, setArrowRef] = useState(null);

  return (
    <Popper
      ref={ref}
      open={Boolean(anchorEl)}
      anchorEl={anchorEl}
      className={classes.popper}
      placement="bottom-start"
      modifiers={{
        flip: { behavior: ["bottom", "top"] },
        offset: { offset: "-40,36" },
        preventOverflow: {
          enabled: true,
          boundariesElement: "scrollParent",
        },
        arrow: {
          enabled: true,
          element: arrowRef,
        },
      }}
    >
      <div className={classes.paper}>{children}</div>
      <span className={classes.arrow} ref={setArrowRef} />
    </Popper>
  );
});

Popover.propTypes = {
  anchorEl: PropTypes.exact({
    clientWidth: PropTypes.number.isRequired,
    clientHeight: PropTypes.number.isRequired,
    getBoundingClientRect: PropTypes.func.isRequired,
  }).isRequired,
};

export default Popover;
