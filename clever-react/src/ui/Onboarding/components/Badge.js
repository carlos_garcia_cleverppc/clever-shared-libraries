/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";

import makeStyles from "../../styles/makeStyles";
import { getColorsLabels } from "../../styles/colors";

const duration = 1600;
const colorsList = getColorsLabels();

const useStyles = makeStyles(({ cleverUI: { colors } }) => ({
  "@keyframes ripple": {
    "0%": { boxShadow: "inset 0 0 0 14px currentColor" },
    "100%": {
      transform: "scale(3)",
      boxShadow: "inset 0 0 0 0 currentColor",
    },
  },
  wrapper: {
    height: props => props.size,
    width: props => props.size,
    transform: "translate(-50%, -50%)",
    position: "absolute",
  },
  dot: {
    height: "100%",
    width: "100%",
    backgroundColor: props => colors[props.color][500],
    borderRadius: "50%",
    position: "relative",
    zIndex: 1,
  },
  ripple: {
    position: "absolute",
    top: 0,
    left: 0,
    color: props => colors[props.color][300],
    height: "100%",
    width: "100%",
    borderRadius: "50%",
    animation: `$ripple ${duration}ms infinite ${duration / 2}ms cubic-bezier(0.64, 0.42, 0.54, 1)`,
    "&:last-child": { animationDelay: `-${duration}ms` },
    "@media (prefers-reduced-motion)": {
      animationPlayState: "paused",
      animationDelay: `-${duration - 200}ms`,

      "&:last-child": { animationDelay: `-${duration / 2}ms` },
    },
  },
}));

const Badge = React.forwardRef((props, ref) => {
  const classes = useStyles(props);
  return (
    <div ref={ref} className={classes.wrapper}>
      <div className={classes.dot} />
      <div className={classes.ripple} />
      <div className={classes.ripple} />
    </div>
  );
});

Badge.propTypes = {
  size: PropTypes.number,
  color: PropTypes.oneOf(colorsList),
};

Badge.defaultProps = {
  size: 14,
  color: "primary",
};

export default Badge;
