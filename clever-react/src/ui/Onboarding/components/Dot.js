import React from "react";
import PropTypes from "prop-types";

import makeStyles from "../../styles/makeStyles";
import { getColorsLabels } from "../../styles/colors";

const colorsList = getColorsLabels(true);

const useStyles = makeStyles(({ cleverUI: { colors } }) => ({
  wrapper: {
    width: 11,
    padding: 0,
    height: 24,
    flex: "0 1 11px",
    display: "inline-flex",
    border: 0,
    backgroundColor: "transparent",
    color: props => (props.actived ? colors[props.color][700] : colors[props.color][300]),
  },
  dot: {
    backgroundColor: "currentColor",
    height: 4,
    width: 4,
    margin: "auto",
    borderRadius: 9999,
  },
}

));

const Dot = ({ actived, color }) => {
  const classes = useStyles({ actived, color });
  return <div className={classes.wrapper}><span className={classes.dot} /></div>;
};

Dot.propTypes = {
  color: PropTypes.oneOf(colorsList),
  actived: PropTypes.bool.isRequired,
};

Dot.defaultProps = { color: "primary" };

export default Dot;
