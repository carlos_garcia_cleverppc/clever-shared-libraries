import React, { useState, useCallback, useRef, useEffect, useLayoutEffect } from "react";
import PropTypes from "prop-types";
import FocusLock, { MoveFocusInside } from "react-focus-lock";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

import { getColorsLabels } from "../styles/colors";

import Button from "../Link";
import Typography from "../Typography";
import Backdrop from "../Backdrop";

import Portal from "./components/Portal";
import Badge from "./components/Badge";
import Dot from "./components/Dot";
import Popover from "./components/Popover";
import useStyles from "./styles";
import { getCoords } from "./utilsDom";
import { enableScroll, disableScroll } from "./scrollLock";

const createAnchorEl = ({ x, y }) => ({
  clientWidth: 0,
  clientHeight: 0,
  getBoundingClientRect() {
    return {
      left: x,
      right: x,
      top: y,
      bottom: y,
    };
  },
});

const colorsList = getColorsLabels(true);

const Onboarding = props => {
  const { color, open, steps, onClose, FocusLockDisabled } = props;
  const { nextText, previousText, finishText } = props;

  const classes = useStyles();
  const [current, _setCurrent] = useState(0);
  const [position, setPosition] = useState(null);
  const [isMoving, setMoving] = useState(false);
  const popperRef = useRef();

  const isCompleted = (arr, currentPosition) => !arr[currentPosition];

  const setCurrent = useCallback(newPosition => _setCurrent(lastPosition => {
    const step = steps[lastPosition];
    if (step.onAfter) {
      setMoving(true);
      step.onAfter().then(() => setMoving(false));
    }
    return newPosition;
  }), [steps]);

  useEffect(() => {
    if (open) {
      disableScroll();
    } else {
      enableScroll();
    }
  }, [open]);

  useLayoutEffect(() => {
    async function loadNext() {
      setMoving(true);
      const step = steps[current];
      if (step.onBefore) {
        await step.onBefore();
      }
      const coords = getCoords(step.selector, { placement: step.placement });
      setPosition(coords);

      setMoving(false);
    }
    if (open) loadNext();
  }, [current, steps, open]);

  const handleClose = () => {
    onClose();
    setCurrent(0);
  };

  const next = stepNumber => {
    if (isCompleted(steps, stepNumber)) {
      handleClose();
    } else {
      setCurrent(stepNumber);
    }
  };

  if (!open) {
    return null;
  }

  return (
    <Portal>
      <Backdrop className={classes.backdrop} open>
        {!isMoving && position && (
          <FocusLock shards={[popperRef]} disabled={FocusLockDisabled}>
            <div style={{ position: "absolute", top: position.y, left: position.x }}>
              <Badge color={color} />

              <ClickAwayListener onClickAway={handleClose}>
                <Popover
                  open
                  ref={popperRef}
                  anchorEl={createAnchorEl(position)}
                >
                  <div className={classes.header}>
                    <Typography
                      component="div"
                      variant="f1-14"
                      shade={900}
                      weight="bold"
                      gutterBottom
                    >
                      {steps[current].title}
                    </Typography>
                  </div>

                  <Typography component="div" variant="f2-14">{steps[current].content}</Typography>

                  <MoveFocusInside>
                    <div className={classes.navigation}>
                      <Button
                        disabled={current === 0}
                        onClick={() => next(current - 1)}
                      >
                        {previousText}
                      </Button>

                      <div className={classes.dots}>
                        {steps.map((_, index) => (
                          <Dot
                            key={index}
                            color={color}
                            actived={index === current}
                          />
                        ))}
                      </div>

                      <Button
                        data-autofocus
                        onClick={() => next(current + 1)}
                      >
                        {current === steps.length - 1 ? finishText : nextText}
                      </Button>
                    </div>
                  </MoveFocusInside>
                </Popover>
              </ClickAwayListener>

            </div>
          </FocusLock>
        )}
      </Backdrop>
    </Portal>
  );
};

Onboarding.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  color: PropTypes.oneOf(colorsList),
  steps: PropTypes.arrayOf(PropTypes.exact({
    selector: PropTypes.string.isRequired,
    title: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.node,
    ]).isRequired,
    content: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.node,
    ]).isRequired,
    placement: PropTypes.oneOf(["left", "right", "top", "bottom"]),
    onAfter: PropTypes.func,
    onBefore: PropTypes.func,
  })).isRequired,
  nextText: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  finishText: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  previousText: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]),
  FocusLockDisabled: PropTypes.bool,
};

Onboarding.defaultProps = {
  open: false,
  color: "primary",
  onClose: () => { },
  finishText: "Finish",
  previousText: "Previous",
  nextText: "Next",
  FocusLockDisabled: false,
};

export default Onboarding;
