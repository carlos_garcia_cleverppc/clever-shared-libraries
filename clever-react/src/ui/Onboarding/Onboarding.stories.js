/* eslint-disable no-param-reassign */
import React from "react";

import { getColorsLabels } from "../styles/colors";

import Onboarding from "./index";
import Typography from "../Typography";
import Button from "../Button";
import Dialog from "../Dialog";

export default {
  title: "Onboarding",
  component: Onboarding,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = ({ steps, ...args }) => {
  const [onboardingOpen, setOnboardingOpen] = React.useState(false);
  const [dialogOpen, setDialogOpen] = React.useState(false);

  steps[2].onBefore = () => new Promise(resolve => {
    setDialogOpen(true);
    setTimeout(() => {
      resolve();
    }, 300);
  });

  steps[2].onAfter = async () => setDialogOpen(false);

  return (
    <>
      <Onboarding
        steps={steps}
        {...args}
        open={onboardingOpen}
        onClose={() => setOnboardingOpen(false)}
      />
      <div style={{ padding: 20, maxWidth: 600 }}>
        <div style={{ paddingBottom: 20 }}>
          <Button color="primary" data-onboarding-step="1" onClick={() => setOnboardingOpen(true)}>Start onboarding</Button>
        </div>

        <Typography gutterBottom>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
          ut labore et dolore magna aliqua. Rhoncus dolor purus non enim praesent elementum
          facilisis leo vel. Risus at ultrices mi tempus imperdiet. Semper risus in hendrerit
          gravida rutrum quisque non tellus. Convallis convallis tellus id interdum velit laoreet id
          donec ultrices. Odio morbi quis commodo odio aenean sed adipiscing.
        </Typography>

        <Typography gutterBottom>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
          ut labore et dolore magna aliqua. Rhoncus dolor purus non enim praesent elementum
          facilisis leo vel. Risus at ultrices mi tempus imperdiet.
        </Typography>

        <div style={{ textAlign: "right" }}>
          <Button color="primary" data-onboarding-step="2">Another action</Button>
        </div>

        <Dialog
          buttons={<><Button data-onboarding-step="3">Create new account</Button></>}
          description="This is the description placeholder for any modal dialog. Any component would be optional and depends on its functional purpose."
          showImageMobile
          open={dialogOpen}
          title="This is a title for a modal dialog"
        />
      </div>
    </>
  );
};

export const SimpleOnboarding = Template.bind({});

const steps = [{
  selector: "[data-onboarding-step=\"1\"]",
  title: "Title step 1",
  content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
}, {
  selector: "[data-onboarding-step=\"2\"]",
  title: "Title step 2",
  content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  placement: "left",
}, {
  selector: "[data-onboarding-step=\"3\"]",
  title: "Title step 3",
  content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  onBefore: () => new Promise(resolve => {
    setTimeout(() => {
      resolve();
    }, 300);
  }),
  onAfter: async () => { },
},
];

SimpleOnboarding.args = {
  steps,
  open: false,
  FocusLockDisabled: true,
};
