/* eslint-disable import/prefer-default-export */
export function getCoords(selector, { gap = 32, placement = "right" } = {}) {
  const element = document.querySelector(selector);
  const coordinates = element.getBoundingClientRect();

  switch (placement) {
    case "left":
    case "right":
      return {
        y: coordinates.top + (coordinates.height / 2),
        x: placement === "right"
          ? coordinates.right + gap
          : coordinates.left - gap,
      };
    case "top":
    case "bottom":
      return {
        x: coordinates.left + (coordinates.width / 2),
        y: placement === "bottom"
          ? coordinates.bottom + gap
          : coordinates.top - gap,
      };

    default:
      return {
        x: 0,
        y: 0,
      };
  }
}
