import React from "react";
import { screen, render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import Onboarding from "../index";
import * as utilsDom from "../utilsDom";
import * as scrollLock from "../scrollLock";
import ThemeProvider from "../../Provider";

const renderWithProviders = ui => {
  const tools = render(<ThemeProvider>{ui}</ThemeProvider>);

  return {
    ...tools,
    rerender: Component => tools.rerender(
      <ThemeProvider>
        {Component}
      </ThemeProvider>,
    ),
  };
};

describe("onBoarding", () => {
  beforeEach(() => {
    jest.restoreAllMocks();

    jest.spyOn(scrollLock, "enableScroll").mockReturnValue({ x: 0, y: 0 });
    jest.spyOn(scrollLock, "disableScroll").mockReturnValue({ x: 0, y: 0 });

    jest.spyOn(utilsDom, "getCoords").mockReturnValue({ x: 0, y: 0 });
  });

  test("Translation ready", async () => {
    const steps = [{
      selector: ".selector",
      title: "Title 1",
      content: "content 1",
    }, {
      selector: ".selector",
      title: "Title 2",
      content: "content 2",
    }];

    renderWithProviders(<Onboarding nextText="Siguiente" previousText="Anterior" finishText="Finalizar" open steps={steps} onClose={jest.fn()} />);

    expect(await screen.findByText("Anterior")).toBeInTheDocument();
    userEvent.click(screen.getByRole("button", { name: "Siguiente" }));

    expect(await screen.findByText("Finalizar")).toBeInTheDocument();
  });

  test("Should disable scroll when it is opened", async () => {
    const steps = [{
      selector: ".selector",
      title: "Title 1",
      content: "content 1",
    }];
    const { rerender } = renderWithProviders(
      <Onboarding open steps={steps} onClose={jest.fn()} />,
    );
    expect(await screen.findByText("Title 1")).toBeInTheDocument();
    expect(scrollLock.disableScroll).toHaveBeenCalled();

    rerender(<Onboarding open={false} steps={steps} onClose={jest.fn()} />);
    expect(scrollLock.enableScroll).toHaveBeenCalled();
  });

  test("Should show the steps and be able to navigate them", async () => {
    const mockOnBefore = jest.fn(() => Promise.resolve());
    const mockOnAfter = jest.fn(() => Promise.resolve());
    const mockLastOnBefore = jest.fn(() => Promise.resolve());
    const mockLastOnAfter = jest.fn(() => Promise.resolve());
    const steps = [{
      selector: ".selector",
      title: "Title 1",
      content: "content 1",
      onBefore: mockOnBefore,
      onAfter: mockOnAfter,
    }, {
      selector: ".selector-2",
      title: "Title 2",
      content: "content 2",
      placement: "left",
      onBefore: mockLastOnBefore,
      onAfter: mockLastOnAfter,
    }];

    const onClose = jest.fn();
    const { rerender } = renderWithProviders(
      <Onboarding open steps={steps} onClose={onClose} />,
    );

    expect(await screen.findByText("Title 1")).toBeInTheDocument();
    expect(await screen.findByText("content 1")).toBeInTheDocument();

    expect(mockOnBefore).toHaveBeenCalled();
    expect(utilsDom.getCoords).toHaveBeenCalledWith(".selector", { placement: undefined });

    userEvent.click(screen.getByRole("button", { name: "Next" }));

    expect(await screen.findByText("Title 2")).toBeInTheDocument();
    expect(await screen.findByText("content 2")).toBeInTheDocument();

    expect(mockOnAfter).toHaveBeenCalled();
    expect(mockLastOnBefore).toHaveBeenCalled();
    expect(utilsDom.getCoords).toHaveBeenCalledWith(".selector-2", { placement: "left" });

    userEvent.click(screen.getByRole("button", { name: "Finish" }));

    expect(await screen.findByText("Title 1")).toBeInTheDocument();

    expect(mockLastOnAfter).toHaveBeenCalled();
    expect(onClose).toHaveBeenCalled();

    rerender(<Onboarding open={false} steps={steps} onClose={onClose} />);

    expect(screen.queryByText("Title 1")).not.toBeInTheDocument();
  });
});
