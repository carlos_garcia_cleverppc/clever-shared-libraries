import makeStyles from "../styles/makeStyles";

const useStyles = makeStyles(({ cleverUI: { spacing, typography }, zIndex }) => {
  const { sizing } = typography;

  return ({
    backdrop: { zIndex: zIndex.modal + 1 },
    header: spacing["pt-3"],
    navigation: {
      ...spacing["pt-4"],
      display: "grid",
      gridTemplateColumns: "1fr auto 1fr",
      alignItems: "center",
      justifyItems: "center",
      "& > :first-child": { justifySelf: "start" },
      "& > :last-child": { justifySelf: "end" },
      "& > a": { fontSize: sizing.body0 },
    },
    dots: {
      display: "flex",
    },
  });
});

export default useStyles;
