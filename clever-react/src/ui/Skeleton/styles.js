/* eslint-disable object-curly-newline */
import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors } }) => ({
  root: {
    backgroundColor: colors.neutral[300],
  },
});

export default makeStyles(styles);
