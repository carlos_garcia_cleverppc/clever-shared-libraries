import React from "react";
import PropTypes from "prop-types";

import SkeletonMaterial from "@material-ui/lab/Skeleton";

import getClasses from "./styles";

const Skeleton = props => {
  const classes = getClasses();

  const { variant, className, width, height, children } = props;

  return (
    <SkeletonMaterial
      variant={variant}
      width={width}
      height={height}
      className={className}
      classes={{ root: classes.root }}
    >
      {children}
    </SkeletonMaterial>
  );
};

Skeleton.propTypes = {
  variant: PropTypes.oneOf(["text", "circle", "rect"]),
  className: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

Skeleton.defaultProps = {
  variant: "text",
  className: "",
  width: undefined,
  height: undefined,
};

export default Skeleton;
