/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Skeleton from "./index";

export default {
  title: "Skeleton",
  component: Skeleton,
};

const Template = args => <Skeleton {...args} />;

export const SimpleSkeleton = Template.bind({});
SimpleSkeleton.args = {};

export const CustomSkeleton = Template.bind({});
CustomSkeleton.args = { variant: "circle", width: 25, height: 25 };
