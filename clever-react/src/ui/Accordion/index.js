import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import MaterialExpansionPanel from "@material-ui/core/Accordion";
import ExpansionPanelSummary from "@material-ui/core/AccordionSummary";
import ExpansionPanelDetails from "@material-ui/core/AccordionActions";

import { getColorsLabels } from "../styles/colors";

import Select from "../Icon/Select";
import ChevronDown from "../Icon/ChevronDown";
import Grid from "../Grid";
import Typography from "../Typography";

import getClasses from "./styles";

const colorsList = getColorsLabels(true);

const Accordion = props => {
  const { className, onChange, expanded, expandIcon, classes } = props;
  const { title, children, unmountOnExit, variant, color } = props;
  const { header, subtitle, badge, badgeProps } = props;

  const classesCustom = getClasses({ color, badgeProps });

  return (
    <MaterialExpansionPanel
      onChange={onChange}
      expanded={expanded}
      className={
        classnames(
          className,
          variant === "custom" && !expanded ? classesCustom.accordion : "",
          variant === "custom" && expanded ? classesCustom.accordionSelected : "",
        )
      }
      TransitionProps={{ unmountOnExit }}
    >
      <ExpansionPanelSummary
        expandIcon={variant === "custom" ? <ChevronDown color={expanded ? color : "neutral"} /> : expandIcon}
        classes={{
          ...classes,
          expanded: variant === "custom" ? classesCustom.accordionExpanded : "",
          expandIcon: variant === "custom" ? classesCustom.accordionChevron : "",
        }}
      >
        {variant === "custom" && (
          <Grid container className={classesCustom.title} alignItems="center" justify="space-between">
            <Grid item className={classesCustom.title}>
              <Typography variant="f1-14" weight="bold" shade={900}>{title}</Typography>
            </Grid>

            {badge && (
              <Grid item>
                <Typography
                  color={badgeProps.color}
                  weight="bold"
                  variant="f1-8"
                  className={classesCustom.badge}
                >
                  {badgeProps.label}
                </Typography>
              </Grid>
            )}

            {!!subtitle && (
              <Grid item xs={12}>
                {typeof subtitle === "string" && (
                  <Typography variant="f1-12" shade={700} className={classesCustom.subtitle}>
                    {subtitle}
                  </Typography>
                )}

                {typeof subtitle !== "string" && subtitle}
              </Grid>
            )}
          </Grid>
        )}

        {variant === "standard" && header}
      </ExpansionPanelSummary>

      <ExpansionPanelDetails>
        {children}
      </ExpansionPanelDetails>
    </MaterialExpansionPanel>
  );
};

Accordion.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  expandIcon: PropTypes.instanceOf(Object),
  header: PropTypes.node,
  title: PropTypes.string,
  subtitle: PropTypes.node,
  expanded: PropTypes.bool,
  badge: PropTypes.bool,
  badgeProps: PropTypes.exact({
    label: PropTypes.string,
    color: PropTypes.oneOf(colorsList),
  }),
  unmountOnExit: PropTypes.bool,
  variant: PropTypes.oneOf(["standard", "custom"]),
  color: PropTypes.oneOf(colorsList),
};

Accordion.defaultProps = {
  className: "",
  onChange: () => { },
  expandIcon: <Select size={10} />,
  header: "",
  title: "",
  subtitle: "",
  expanded: true,
  badge: false,
  badgeProps: {
    label: "Completed",
    color: "primary",
  },
  unmountOnExit: false,
  variant: "standard",
  color: "primary",
};

export default Accordion;
