import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  accordion: {
    ...spacing["mb-4"],
    border: "2px solid",
    borderColor: colors.neutral[300],
    boxShadow: "none",
    borderRadius: "8px !important",
  },
  accordionSelected: ({ color }) => ({
    ...spacing["mb-4"],
    border: "2px solid",
    borderColor: colors[color][500],
    boxShadow: "none",
    borderRadius: "8px !important",
  }),
  accordionExpanded: {
    minHeight: "48px !important",
    "& div": { minHeight: "fit-content !important" },
    "& div:first-child": { margin: "12px 0px" },
  },
  accordionChevron: {
    marginTop: "-3px",
    alignSelf: "flex-start",
  },
  title: { margin: "0px !important" },
  badge: ({ badgeProps }) => ({
    ...spacing["p-1"],
    ...spacing["ml-2"],
    backgroundColor: colors[badgeProps.color][100],
    width: "fit-content",
    height: "fit-content",
    borderRadius: "8px",
  }),
  subtitle: {
    ...spacing["mt-1"],
    fontStyle: "italic",
  },
});

export default makeStyles(styles);
