/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Accordion from "./index";
import Typography from "../Typography/index";
import Select from "../Icon/Select";

import { getColorsLabels } from "../styles/colors";

export default {
  title: "Accordion",
  component: Accordion,
  argTypes: {
    color: {
      control: {
        type: "select",
        options: getColorsLabels(),
      },
    },
  },
};

const Template = args => {
  const [expanded, setExpanded] = React.useState(false);

  return (
    <div>
      <Accordion {...args} expanded={expanded} onChange={() => setExpanded(!expanded)} />
    </div>
  );
};

export const StandardAccordion = Template.bind({});
StandardAccordion.args = {
  header: <Typography>Header</Typography>,
  children: <Typography>This is the children</Typography>,
  expandIcon: <Select size={10} />,
  badgeProps: {
    label: "Completed",
    color: "primary",
  },
};

export const CustomAccordion = Template.bind({});
CustomAccordion.args = {
  title: "Title",
  subtitle: "Subtitle",
  children: <Typography>This is the children</Typography>,
  variant: "custom",
  badgeProps: {
    label: "Completed",
    color: "primary",
  },
};
