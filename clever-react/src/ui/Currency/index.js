/* eslint-disable max-len */
/* eslint-disable no-plusplus */
import PropTypes from "prop-types";

import locales from "./locales";
import defaultLocales from "./default-locales";
import symbols from "./symbols";

const ReactCurrencyFormatter = props => {
  const getFormatter = options => {
    let locale;
    let symbol;

    // Helper Functions
    const isUndefined = o => typeof o === "undefined";

    const toFixed = (n, precision) => (+(`${Math.round(+(`${n}e${precision}`))}e${-precision}`)).toFixed(precision);

    // Perform checks on inputs and set up defaults as needed (defaults to en, USD)
    if (isUndefined(options)) {
      // eslint-disable-next-line no-param-reassign
      options = {};
    }

    const currency = isUndefined(options.currency) ? "USD" : options.currency.toUpperCase();
    locale = isUndefined(options.locale) ? locales[defaultLocales[currency]] : locales[options.locale];

    if (!isUndefined(locale.h)) locale = locales[locale.h]; // Locale inheritance

    symbol = isUndefined(options.symbol) ? symbols[currency] : options.symbol;

    if (isUndefined(symbol)) symbol = currency; // In case we don"t have the symbol, just use the ccy code

    const pattern = isUndefined(options.pattern) ? locale.p : options.pattern;
    const decimal = isUndefined(options.decimal) ? locale.d : options.decimal;
    const group = isUndefined(options.group) ? locale.g : options.group;

    // encodePattern Function - returns a few simple characteristics of the pattern provided
    const encodePattern = auxPattern => {
      let decimalPlaces = 0;
      let frontPadding = "";
      let backPadding = "";
      const groupLengths = [];

      let patternStarted = false;
      let decimalsStarted = false;
      let patternEnded = false;

      let currentGroupLength = 0;
      let zeroLength = 0;

      for (let i = 0; i < auxPattern.length; ++i) {
        const c = auxPattern[i];

        if (!patternStarted && ["#", "0", ",", "."].indexOf(c) > -1) {
          patternStarted = true;
        }

        if (!patternStarted) { frontPadding += c; }

        switch (c) {
          case "#":
            ++currentGroupLength;

            break;
          case "0":
            if (decimalsStarted) ++decimalPlaces;
            else {
              ++currentGroupLength;
              ++zeroLength;
            }

            break;
          case ",":
            groupLengths.push(currentGroupLength);
            currentGroupLength = 0;

            break;
          case ".":
            groupLengths.push(currentGroupLength);
            decimalsStarted = true;

            break;
          default:
            break;
        }

        if (patternStarted && !(["#", "0", ",", "."].indexOf(c) > -1)) {
          patternEnded = true;

          if (!decimalsStarted) {
            groupLengths.push(currentGroupLength);
          }
        }

        if (patternEnded) { backPadding += c; }
      }

      const encodedPattern = {
        decimalPlaces,
        frontPadding,
        backPadding,
        groupLengths,
        zeroLength,
      };

      return encodedPattern;
    };

    // Zero Padding helper function
    const pad = (n, width) => {
      // eslint-disable-next-line no-param-reassign
      n += "";

      return n.length >= width ? n : new Array(width - n.length + 1).join("0") + n;
    };

    // Format function
    const format = (n, f) => {
      let formattedNumber = toFixed(Math.abs(n), f.decimalPlaces);
      const splitNumber = formattedNumber.split(".");
      let segment = "";

      // i.e. we actually have some sort of grouping in the values
      if (f.groupLengths.length > 1) {
        let cursor = splitNumber[0].length;
        let groupIndex = f.groupLengths.length - 1;

        while (cursor > 0) {
          if (groupIndex <= 0) { groupIndex = 1; } // Always reset to the first group length if the number is big
          const currentGroupLength = f.groupLengths[groupIndex];
          const start = cursor - currentGroupLength;

          segment = splitNumber[0].substring(start, cursor) + f.group + segment;
          cursor -= currentGroupLength;
          --groupIndex;
        }

        segment = segment.substring(0, segment.length - 1);
      }

      if (segment.length < f.zeroLength) { segment = pad(segment, f.zeroLength); }

      formattedNumber = f.frontPadding + segment + (isUndefined(splitNumber[1]) ? "" : (f.decimal + splitNumber[1])) + f.backPadding;

      return formattedNumber.replace("!", symbol);
    };

    // Use encode function to work out pattern
    const patternArray = pattern.split(";");
    const positiveFormat = encodePattern(patternArray[0]);

    positiveFormat.symbol = symbol;
    positiveFormat.decimal = decimal;
    positiveFormat.group = group;

    const negativeFormat = isUndefined(patternArray[1]) ? encodePattern(`-${patternArray[0]}`) : encodePattern(patternArray[1]);

    negativeFormat.symbol = symbol;
    negativeFormat.decimal = decimal;
    negativeFormat.group = group;

    const zero = isUndefined(patternArray[2]) ? format(0, positiveFormat) : patternArray[2];

    return n => {
      let formattedNumber;
      // eslint-disable-next-line no-param-reassign
      n = Number(n);

      if (n > 0) formattedNumber = format(n, positiveFormat);
      else if (n === 0) formattedNumber = zero.replace("!", symbol);
      else formattedNumber = format(n, negativeFormat);

      return formattedNumber;
    };
  };

  const format = (number, options) => {
    const formatterFunction = getFormatter(options);

    return formatterFunction(number);
  };

  const { currency, symbol, locale, decimal } = props;
  const { group, pattern, symbolOnly } = props;

  if (symbolOnly) {
    return symbol || symbols[currency];
  }
  const value = (format(props.quantity, {
    currency,
    symbol,
    locale,
    decimal,
    group,
    pattern,
  }));

  if (value.indexOf(".") < value.indexOf(",")) {
    return value.replace(",00", "");
  }
  return value.replace(".00", "");
};

ReactCurrencyFormatter.defaultProps = { currency: "USD", symbolOnly: false, quantity: 1 };

ReactCurrencyFormatter.propTypes = {
  symbolOnly: PropTypes.bool,
  quantity: PropTypes.number,
  currency: PropTypes.string,
  symbol: PropTypes.string,
  locale: PropTypes.string,
  decimal: PropTypes.string,
  group: PropTypes.string,
  pattern: PropTypes.string,
};

export default ReactCurrencyFormatter;
