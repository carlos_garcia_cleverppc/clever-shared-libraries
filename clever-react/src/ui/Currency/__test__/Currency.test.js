/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
import React from "react";
import { render } from "@testing-library/react";
import Currency from "../index";

const quantity = 4556.04;
const expected = {
  EUR: "4.556,04 €",
  QAR: "﷼ 4,556.04",
  XAF: "4 556,04 FCFA",
  TTD: "TT$4,556.04",
  RSD: "4.556,04 Дин.",
  JPY: "¥4,556.04", // @TODO: algunas monedas no adminten decimales por ejemplo: "JPY"
  TOP: "T$ 4,556.04",
  SEK: "4 556,04 kr",
  MZN: "MT4.556,04",
  IQD: "ع.د 4,556.04",
};

describe("Currency", () => {
  test("should format long numbers", () => {
    const { container } = render(<Currency quantity={4500000056.04} />);
    expect(container.innerHTML).toBe("$4,500,000,056.04");
  });

  test("should format currency", () => {
    for (const currency in expected) {
      const { container } = render(
        <Currency quantity={quantity} currency={currency} />,
      );
      expect(container.innerHTML).toBe(expected[currency]);
    }
  });

  test("should get only currency symbol", () => {
    const { container } = render(
      <Currency currency="EUR" quantity={1000} symbolOnly />,
    );

    expect(container.innerHTML).toBe("€");
  });

  test("should use dolar as default currency", () => {
    const { container } = render(<Currency quantity={9010760.1} />);

    expect(container.innerHTML).toBe("$9,010,760.10");
  });

  describe("Options currency", () => {
    test("should custom symbol", () => {
      const { container } = render(
        <Currency quantity={90100} currency="EUR" symbol="%%" />,
      );

      expect(container.innerHTML).toBe("90.100 %%");
    });

    test("should custom symbol with symbolOnly prop", () => {
      const { container } = render(
        <Currency quantity={1000} symbolOnly symbol="%%" />,
      );

      expect(container.innerHTML).toBe("%%");
    });

    test("should set locale", () => {
      const { container } = render(
        <Currency locale="en_FI" quantity={9010760.1} />,
      );

      expect(container.innerHTML).toBe("$9 010 760.10");
    });

    test("should set locale 2", () => {
      const { container } = render(
        <Currency locale="agq" quantity={9010760.1} currency="BIF" />,
      );

      expect(container.innerHTML).toBe("9 010 760,10FBu");
    });

    test("should custom decimal mark", () => {
      const { container } = render(
        <Currency
          locale="agq"
          quantity={9010760.1}
          currency="BIF"
          decimal="**"
        />,
      );

      expect(container.innerHTML).toBe("9 010 760**10FBu");
    });

    test("should custom group mark", () => {
      const { container } = render(
        <Currency quantity={9010760.1} group="__" />,
      );

      expect(container.innerHTML).toBe("$9__010__760_.10");
    });

    test("should custom pattern", () => {
      const { container } = render(
        <Currency quantity={9010760.1} pattern="!  #,##0.0" />,
      );

      expect(container.innerHTML).toBe("$  9,010,760.1");
    });
    test("should custom pattern 2", () => {
      const { container } = render(
        <Currency quantity={9010760.1} pattern="###,#,##,0.00-!" />,
      );

      expect(container.innerHTML).toBe("9,0,1,0,76,0.10-$");
    });

    test("should mix options", () => {
      const { container } = render(
        <Currency
          quantity={9010760.1}
          group="__"
          decimal="'"
          pattern="! #,#,#,0.00"
        />,
      );

      expect(container.innerHTML).toBe("$ 9__0__1__0__7__6__0_'10");
    });
  });
});
