/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Currency from "./index";

export default {
  title: "Currency",
  component: Currency,
};

const Template = args => <Currency {...args} />;

export const SimpleCurrency = Template.bind({});
SimpleCurrency.args = {
  quantity: 4556.04,
  currency: "EUR",
};
