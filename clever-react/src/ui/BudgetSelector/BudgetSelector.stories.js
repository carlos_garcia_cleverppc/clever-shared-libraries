/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import BudgetSelector from "./index";

export default {
  title: "BudgetSelector",
  component: BudgetSelector,
};

const Template = args => <BudgetSelector {...args} />;

export const SimpleBudgetSelector = Template.bind({});
SimpleBudgetSelector.args = {
  selectedBudget: 20,
  recommendedBudget: 20,
  mainCurrency: "USD",
  adwordsCurrencyName: "EUR",
  adwordsCurrencyRate: 0.86,
  description: "This is the description",
};
