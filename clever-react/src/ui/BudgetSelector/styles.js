import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { spacing, colors } }) => ({
  rootBudgetCard: {
    ...spacing["p-5"],
    boxShadow: "none",
    textAlign: "center",
    ...spacing["pt-5"],
    ...spacing["pb-4"],
  },
  budgetCardWarning: { backgroundColor: colors.warning[500] },
  budgetCardSuccess: { backgroundColor: colors.green[500] },
  budgetConversion: { ...spacing["pb-4"] },
  slider: {
    ...spacing["mt-5"],
    ...spacing["w-100"],
    display: "inline-flex",
    alignItems: "center",
    justifyContent: "center",
  },
  sliderLimitL: {
    ...spacing["pr-4"],
    whiteSpace: "nowrap",
  },
  sliderLimitR: {
    ...spacing["pl-4"],
    whiteSpace: "nowrap",
  },
  colorSuccess: { color: colors.green[500] },
  colorWarning: { color: colors.warning[500] },
});

export default makeStyles(styles);
