import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import upperCase from "lodash-es/upperCase";
import round from "lodash-es/round";

import Slider from "../Slider";
import Card from "../Card";
import Grid from "../Grid";
import Typography from "../Typography";
import useMediaQuery from "../styles/useMediaQuery";

import { screenXs } from "../styles/breakpoints";

import getClasses from "./styles";

const BudgetSelector = props => {
  const classes = getClasses();

  const { selectedBudget, recommendedBudget, minLimit, maxLimit, step, sliderBubble } = props;
  const {
    description, mainCurrency, adwordsCurrencyName,
    adwordsCurrencyRate, onChange, disabled,
  } = props;

  const isScreenXs = useMediaQuery(screenXs);

  const budgetColor = selectedBudget < recommendedBudget ? "Warning" : "Success";

  const handleOnChange = (ev, value) => {
    onChange(value);
  };

  return (
    <div>
      <Card className={classnames(classes.rootBudgetCard, classes[`budgetCard${budgetColor}`])}>
        <Grid container spacing={1} justify="center" alignItems="baseline">
          <Grid item>
            <Typography variant="h1" color="white">{selectedBudget}</Typography>
          </Grid>

          <Grid item>
            <Typography variant="body2" weight="bold" color="white">
              {" "}
              {upperCase(mainCurrency)}
            </Typography>
          </Grid>
        </Grid>

        {adwordsCurrencyRate !== 0
        && upperCase(adwordsCurrencyName) !== upperCase(mainCurrency)
        && (
          <Typography variant="body1" weight="bold" color="white" className={classes.budgetConversion}>
            (
            {round(adwordsCurrencyRate * selectedBudget, 2)}
            {" "}
            {upperCase(adwordsCurrencyName)}
            )
          </Typography>
        )}

        {description !== "" && (
          <Typography variant="body0" color="white">
            {description}
          </Typography>
        )}
      </Card>

      <div className={classes.slider}>
        <Typography variant="body0" className={classes.sliderLimitL}>
          {" "}
          {minLimit}
          {" "}
          {upperCase(mainCurrency)}
          {" "}
        </Typography>

        <Slider
          value={selectedBudget}
          min={minLimit}
          max={maxLimit}
          step={step}
          onChange={handleOnChange}
          className={selectedBudget >= recommendedBudget
            ? classes.colorSuccess
            : classes.colorWarning}
          mobile={isScreenXs}
          valueLabelDisplay={sliderBubble ? "auto" : "off"}
          disabled={disabled}
        />

        <Typography variant="body0" className={classes.sliderLimitR}>
          {" "}
          {maxLimit}
          {" "}
          {upperCase(mainCurrency)}
          {" "}
        </Typography>
      </div>
    </div>
  );
};

BudgetSelector.propTypes = {
  selectedBudget: PropTypes.number,
  recommendedBudget: PropTypes.number,
  minLimit: PropTypes.number,
  maxLimit: PropTypes.number,
  step: PropTypes.number,
  sliderBubble: PropTypes.bool,
  description: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Object)]),
  mainCurrency: PropTypes.string,
  adwordsCurrencyName: PropTypes.string,
  adwordsCurrencyRate: PropTypes.number,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
};

BudgetSelector.defaultProps = {
  selectedBudget: 5,
  recommendedBudget: 0,
  minLimit: 5,
  maxLimit: 200,
  step: 5,
  sliderBubble: true,
  description: "",
  mainCurrency: "USD",
  adwordsCurrencyName: "",
  adwordsCurrencyRate: 0,
  onChange: () => { },
  disabled: false,
};

export default BudgetSelector;
