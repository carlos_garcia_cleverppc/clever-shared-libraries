import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  positionHorizontal: {
    display: "inline-flex",
  },
  card: ({ borderColor }) => ({
    ...spacing["px-4"],
    ...spacing["py-5"],
    backgroundColor: colors.neutral[100],
    borderWidth: "2px",
    borderColor: borderColor === "neutral" ? colors.neutral[100] : colors[borderColor][500],
    width: "264px",
  }),
  pointer: { cursor: "pointer" },
  time: {
    ...spacing["mb-2"],
    display: "flex",
    alignItems: "baseline",
  },
  badge: {
    ...spacing["ml-2"],
    height: "8px",
    width: "8px",
    borderRadius: "50%",
    backgroundColor: colors.danger[500],
  },
  thumbs: { width: "300px" },
});

export default makeStyles(styles);
