/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import FeedbackCard from "./index";

export default {
  title: "FeedbackCard",
  component: FeedbackCard,
};

const Template = args => <FeedbackCard {...args} />;

export const SimpleFeedbackCard = Template.bind({});
SimpleFeedbackCard.args = {
  timeCopy: "Hoy a las 16:32",
  showBadge: true,
  comment: "Hola! Acabo de recibir tu solicitud. En cuanto lo tenga, te lo envio por email.",
  thumbDownSelected: true,
  cardSelected: false,
};
