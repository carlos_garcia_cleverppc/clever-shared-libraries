import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import Card from "../Card";
import FeedbackThumbs from "../FeedbackThumb";
import Grid from "../Grid";
import Typography from "../Typography";

import getClasses from "./styles";

const FeedbackCard = props => {
  const { timeCopy, showBadge, comment, thumbUpSelected, thumbDownSelected } = props;
  const { onClickThumbUp, onClickThumbDown, onClick, cardSelected, className } = props;

  const [isSelectedThumbUp, setIsSelectedThumbUp] = useState(thumbUpSelected ? "up" : undefined);
  const [isSelectedThumbDown, setIsSelectedThumbDown] = useState(thumbDownSelected ? "down" : undefined);

  const getBorderColor = () => {
    if (cardSelected) {
      return "green";
    }

    return "neutral";
  };

  const classes = getClasses({ borderColor: getBorderColor() });

  const handleThumbClick = feedback => {
    if (feedback === "up") {
      setIsSelectedThumbUp("up");
      setIsSelectedThumbDown(undefined);
      onClickThumbUp();
    } else {
      setIsSelectedThumbUp(undefined);
      setIsSelectedThumbDown("down");
      onClickThumbDown();
    }
  };

  return (
    <div className={className}>
      <div className={classes.time}>
        <Typography variant="f2-11">{timeCopy}</Typography>

        {showBadge && (
          <div className={classes.badge} />
        )}
      </div>

      <Card className={classnames(classes.card, onClick !== undefined ? classes.pointer : "")} onClick={onClick}>
        <Typography variant="f2-14" shade={900}>
          {comment}
        </Typography>
      </Card>

      <Grid container justify="flex-end" alignItems="center" className={classes.thumbs}>
        <Grid item>
          <FeedbackThumbs
            handleThumbClick={() => handleThumbClick("up")}
            isSelected={isSelectedThumbUp}
            thumbUp
            size={50}
          />
        </Grid>

        <Grid item>
          <FeedbackThumbs
            handleThumbClick={() => handleThumbClick("down")}
            isSelected={isSelectedThumbDown}
            size={50}
          />
        </Grid>
      </Grid>
    </div>
  );
};

FeedbackCard.propTypes = {
  timeCopy: PropTypes.string,
  showBadge: PropTypes.bool,
  comment: PropTypes.string,
  cardSelected: PropTypes.bool,
  thumbDownSelected: PropTypes.bool,
  thumbUpSelected: PropTypes.bool,
  onClickThumbDown: PropTypes.instanceOf(Object),
  onClickThumbUp: PropTypes.instanceOf(Object),
  onClick: PropTypes.instanceOf(Object),
};

FeedbackCard.defaultProps = {
  timeCopy: "",
  showBadge: false,
  comment: "",
  cardSelected: false,
  thumbDownSelected: false,
  thumbUpSelected: false,
  onClickThumbDown: () => { },
  onClickThumbUp: () => { },
  onClick: undefined,
};

export default FeedbackCard;
