/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import BorderedBlock from "./index";
import Typography from "../Typography";
import Link from "../Link";
import Button from "../Button";

export default {
  title: "BorderedBlock",
  component: BorderedBlock,
};

const Template = args => <BorderedBlock {...args} />;

export const SimpleBorderedBlock = Template.bind({});
SimpleBorderedBlock.args = {
  image: "/assets/img/block-example.svg",
  title: "¡Buen trabajo! Continúa así…",
  description: "Desde que comenzaste tus campañas no para de mejorar la calidad del tráfico que llega a tu tienda. Tener potenciales clientes es la clave para conseguir más ventas.",
  footerContent: (
    <>
      <Typography variant="body0">
        Si la tendencia del tráfico es decreciente puedes probar a incrementar la inversion y explorar nuevas nuevas plataformas que no estas trabajando como Google Ads.
      </Typography>

      <Link onClick={() => console.log("click_link")}>
        <Typography variant="body0" color="primary" weight="bold">
          Crear Campañas...
        </Typography>
      </Link>
    </>
  ),
  borderColor: "secondary",
  actionOnClick: () => console.log("click_link"),
  actionText: "Go to shopping campaigns",
  quantityImage: 1876,
  textImage: "CONVERSIONS",
  textBadge: "STILL LEARNING...",
  content: (
    <Button
      onClick={() => console.log("click_button")}
    >
      button
    </Button>
  ),
};
