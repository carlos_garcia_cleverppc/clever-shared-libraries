import makeStyles from "../styles/makeStyles";

const styles = makeStyles(({ cleverUI: { colors, spacing } }) => ({
  card: ({ borderColor, borderShade, width }) => ({
    borderColor: colors[borderColor][borderShade],
    borderWidth: "2px",
    width: `${160 * width + (width - 1) * 24}px`,
  }),
  container: { ...spacing["py-5"], ...spacing["px-4"] },
  firstItem: {
    textAlign: "center",
    ...spacing["pr-4"],
  },
  imageInfo: { ...spacing["mnt-4"] },
  title: { ...spacing["mb-2"] },
  link: {
    fontSize: "14px !important",
    textDecoration: "underline !important",
    display: "flex",
    alignItems: "center",
  },
  badge: {
    textAlign: "right",
    display: "flex",
    justifyContent: "flex-end",
  },
  textBadge: { ...spacing["pr-6"], marginTop: "-5px", letterSpacing: "2px" },
  "@keyframes ripple": {
    "0%": { boxShadow: "inset 0 0 0 12px currentColor" },
    "100%": {
      transform: "scale(3)",
      boxShadow: "inset 0 0 0 0 currentColor",
    },
  },
  wrapper: {
    height: "12px",
    width: "12px",
    transform: "translate(-50%, -50%)",
    position: "absolute",
  },
  dot: {
    height: "100%",
    width: "100%",
    backgroundColor: colors.secondary[500],
    borderRadius: "50%",
    position: "relative",
    zIndex: 1,
  },
  ripple: {
    position: "absolute",
    top: 0,
    right: 0,
    color: colors.secondary[100],
    height: "100%",
    width: "100%",
    borderRadius: "50%",
    animation: "$ripple 1600ms infinite 800ms cubic-bezier(0.64, 0.42, 0.54, 1)",
    "&:last-child": { animationDelay: "-1600ms" },
    "@media (prefers-reduced-motion)": {
      animationPlayState: "paused",
      animationDelay: "-1400ms",
      "&:last-child": { animationDelay: "-800ms" },
    },
  },
  iconMargin: { ...spacing["ml-2"] },
  copyContent: {
    backgroundColor: colors.neutral[100],
    borderRadius: "5%",
    ...spacing["p-5"],
    ...spacing["my-4"],
  },
  copyButton: {
    position: "absolute",
    top: "68px",
    right: "35px",
  },
  contentDiv: {
    ...spacing["my-2"],
  },
  description: ({ descriptionLines }) => ({
    ...spacing["mb-2"],
    ...spacing["pr-1"],
    overflow: "hidden",
    textOverflow: "ellipsis",
    display: "-webkit-box",
    "-webkit-line-clamp": descriptionLines,
    "-webkit-box-orient": "vertical",
  }),
  divider: ({ width }) => ({
    marginLeft: "-14px",
    width: `${160 * width + (width - 1) * 24}px`,
  }),
  image: ({ isMobile }) => ({
    display: isMobile ? "flex" : "",
    maxWidth: "270px",
    marginTop: isMobile ? "" : "auto",
    maxHeight: "120px",
    marginBottom: isMobile ? "" : "auto",
  }),
  metricContainer: { minHeight: "140px" },
  imageItem: { margin: "auto", textAlign: "center" },
  metricItem: { display: "grid", textAlign: "center" },
}));

export default styles;
