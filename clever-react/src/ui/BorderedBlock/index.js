/* eslint-disable no-extra-boolean-cast */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import Card from "../Card";
import Grid from "../Grid";
import Link from "../Link";
import Typography from "../Typography";
import Divider from "../Divider";

import Image from "../Image";

import ArrowRight from "../Icon/ArrowRight";

import { getColorsLabels, getShades } from "../styles/colors";
import { screenSm, screenXs } from "../styles/breakpoints";
import useMediaQuery from "../styles/useMediaQuery";

import getClasses from "./styles";

const colorsList = getColorsLabels(true);
const shadesList = getShades();

const BorderedBlock = props => {
  const { image, title, description, quantityImage, textImage, content } = props;
  const { actionText, actionOnClick, borderColor, borderShade, textBadge, children } = props;
  const { footerContent, titleLines, descriptionLines, footerLines, className } = props;

  const isScreenSm = useMediaQuery(screenSm);
  const isScreenXs = useMediaQuery(screenXs);
  const isMobile = isScreenSm || isScreenXs;

  const width = isMobile ? 2 : 4;

  const classes = getClasses({ borderColor, borderShade, width, titleLines, descriptionLines, footerLines, isMobile });

  return (
    <Card className={classnames(classes.card, className)}>
      <Grid container alignItems="center" className={classes.container}>
        {textBadge !== "" && (
          <>
            {width > 2 && (
              <Grid item xs={12} className={classes.badge}>
                <Typography variant="f1-10" className={classes.textBadge}>{textBadge}</Typography>

                <div className={classes.wrapper}>
                  <div className={classes.dot} />

                  <div className={classes.ripple} />
                </div>
              </Grid>
            )}

            {width <= 2 && (
              <Grid item xs={12}>
                <Grid container spacing={1} justify="center" alignItems="stretch" className={classes.metricContainer}>
                  <Grid item xs={6} className={classes.imageItem}>
                    <Image src={image} center className={classes.image} />
                  </Grid>

                  <Grid item xs={6} className={classes.metricItem}>
                    <div className={classes.badge}>
                      <Typography variant="f1-10" className={classes.textBadge}>{textBadge}</Typography>

                      <div className={classes.wrapper}>
                        <div className={classes.dot} />

                        <div className={classes.ripple} />
                      </div>
                    </div>

                    {(!!textImage || !!quantityImage) && (
                      <div className={classes.imageInfo}>
                        {quantityImage !== undefined && (
                          <Typography variant="f1-40" weight="bold" shade="darker">
                            {quantityImage}
                          </Typography>
                        )}

                        {!!textImage && <Typography variant="f2-10" shade="darker">{textImage}</Typography>}
                      </div>
                    )}
                  </Grid>
                </Grid>

              </Grid>
            )}
          </>
        )}

        {width > 2 && (
          <Grid item xs={4} className={classes.firstItem}>
            <Image src={image} center className={classes.image} />

            {(!!textImage || !!quantityImage) && (
              <div className={classes.imageInfo}>
                {quantityImage !== undefined && (
                  <Typography variant="f1-40" weight="bold" shade="darker">
                    {quantityImage}
                  </Typography>
                )}

                {!!textImage && <Typography variant="f2-10" shade="darker">{textImage}</Typography>}
              </div>
            )}
          </Grid>
        )}

        <Grid item xs={width <= 2 ? 12 : 8}>
          <Typography variant="h4" shade="darker" className={classes.title}>{title}</Typography>

          {!!content && (
            <div className={classes.contentDiv}>
              {content}
            </div>
          )}

          <Typography
            variant="body0"
            gutterBottom
            className={classes.description}
          >
            {description}
          </Typography>

          {!!children ? children : null}

          {actionText !== "" && (
            <Link
              onClick={actionOnClick}
              color="neutral"
              className={classes.link}
            >
              {actionText}

              <ArrowRight
                size={12}
                className={classes.iconMargin}
              />
            </Link>
          )}
        </Grid>

        {!!footerContent && (
          <>
            <Grid item xs={12}>
              <Divider dashed className={classes.divider} />
            </Grid>

            <Grid item variant="body0" xs={12}>
              {footerContent}
            </Grid>
          </>
        )}

      </Grid>
    </Card>
  );
};

BorderedBlock.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  quantityImage: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  textImage: PropTypes.string,
  actionText: PropTypes.string,
  actionOnClick: PropTypes.instanceOf(Object),
  borderColor: PropTypes.oneOf(colorsList),
  borderShade: PropTypes.oneOf(shadesList),
  textBadge: PropTypes.string,
  titleLines: PropTypes.number,
  descriptionLines: PropTypes.number,
};

BorderedBlock.defaultProps = {
  image: "",
  title: "",
  description: "",
  quantityImage: undefined,
  textImage: "",
  actionText: "",
  actionOnClick: () => { },
  borderColor: "neutral",
  borderShade: 500,
  textBadge: "",
  titleLines: 3,
  descriptionLines: 4,
};

export default BorderedBlock;
