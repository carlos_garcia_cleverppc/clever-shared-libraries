import React from "react";
import PropTypes from "prop-types";

import MenuItem from "@material-ui/core/MenuItem";

// import styles from "./styles";

const CleverMenuItem = React.forwardRef((props, ref) => {
  const {
    variant,
    onClick,
    className,
    children,
    value,
  } = props;

  if (variant === "header") {
    return (
      <div className={className} ref={ref}>
        {children}
      </div>
    );
  }
  return (
    <MenuItem
      {...props}
      ref={ref}
      className={className}
      onClick={onClick}
      value={value}
    >
      {children}
    </MenuItem>
  );
});

CleverMenuItem.propTypes = {
  className: PropTypes.string,
  variant: PropTypes.oneOf(["default", "header"]),
  onClick: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.bool]),
};

CleverMenuItem.defaultProps = {
  className: "",
  variant: "default",
  onClick: () => { },
  value: "",
};

export default CleverMenuItem;
