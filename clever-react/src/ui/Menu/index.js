import React from "react";
import PropTypes from "prop-types";

import Menu from "@material-ui/core/Menu";

const CleverMenu = props => {
  const { className, children, anchorEl, open, onClose } = props;

  return (
    <Menu
      className={className}
      anchorEl={anchorEl}
      open={open}
      onClose={onClose}
    >
      {children}
    </Menu>
  );
};

CleverMenu.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool,
  anchorEl: PropTypes.oneOfType([PropTypes.element, PropTypes.func, PropTypes.instanceOf(Object)]),
};

CleverMenu.defaultProps = {
  className: "",
  onClose: () => { },
  open: false,
  anchorEl: null,
};

export default CleverMenu;
