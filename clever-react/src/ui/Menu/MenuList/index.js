import React from "react";
import PropTypes from "prop-types";

import MenuList from "@material-ui/core/MenuList";

// import styles from "./styles";

const CleverMenuList = props => {
  const { onClick, className, children, onKeyDown, autoFocusItem, id } = props;

  return (
    <MenuList
      className={className}
      onClick={onClick}
      onKeyDown={onKeyDown}
      id={id}
      autoFocusItem={autoFocusItem}
    >
      {children}
    </MenuList>
  );
};

CleverMenuList.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  onKeyDown: PropTypes.func,
};

CleverMenuList.defaultProps = {
  className: "",
  onClick: () => { },
  onKeyDown: () => { },
};

export default CleverMenuList;
