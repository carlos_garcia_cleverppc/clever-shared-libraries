/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Menu from "./index";
import MenuItem from "./Item";

import Button from "../Button";

export default {
  title: "Menu",
  component: Menu,
};

const Template = args => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div>
      <Button onClick={handleClick}>
        Open Menu
      </Button>

      <Menu
        {...args}
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={handleClose}
        keepMounted
      />
    </div>
  );
};

export const SimpleMenu = Template.bind({});
SimpleMenu.args = {
  children: [
    <MenuItem key="1">Profile</MenuItem>,
    <MenuItem key="2">My account</MenuItem>,
    <MenuItem key="3">Logout</MenuItem>]
  ,
};
