import React from "react";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ThemeProvider from "../../Provider";
import TextFieldCollapsed from "../index";

describe("TextFieldCollapsed", () => {
  test("should submit input content", () => {
    const onSubmit = jest.fn();

    render(
      <ThemeProvider>
        <TextFieldCollapsed onSubmit={onSubmit} />
      </ThemeProvider>,
    );

    const [openHandle, submit] = screen.getAllByRole("button");
    userEvent.click(openHandle);

    const input = screen.getByRole("textbox");
    userEvent.type(input, "https://google.com");
    userEvent.click(submit);

    expect(onSubmit).toBeCalledWith("https://google.com");
  });

  test("should submit on keypress enter", () => {
    const onSubmit = jest.fn();

    render(
      <ThemeProvider>
        <TextFieldCollapsed onSubmit={onSubmit} />
      </ThemeProvider>,
    );

    const [openHandle] = screen.getAllByRole("button");
    userEvent.click(openHandle);

    const input = screen.getByRole("textbox");
    userEvent.type(input, "https://google.com{enter}");

    expect(onSubmit).toBeCalledWith("https://google.com");
  });

  test("should focus on open textfield", async () => {
    const onSubmit = jest.fn();

    render(
      <ThemeProvider>
        <TextFieldCollapsed onSubmit={onSubmit} />
      </ThemeProvider>,
    );

    const [openHandle] = screen.getAllByRole("button");
    userEvent.click(openHandle);

    const input = screen.getByRole("textbox");
    expect(input).toHaveFocus();
  });
});
