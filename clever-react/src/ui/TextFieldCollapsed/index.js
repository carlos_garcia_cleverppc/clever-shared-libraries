/* eslint-disable no-unused-expressions */
import React, { useRef, useState, useEffect } from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import IconButton from "../IconButton";
import Link from "../Icon/Link";
import Enter from "../Icon/Enter";
import ClickAwayListener from "../ClickAwayListener";

import useTransitionEnd from "./useTransitionEnd";
import getClasses from "./styles";

const TextFieldCollapsed = ({ onSubmit, className, type, icon }) => {
  const [open, setOpen] = useState(false);
  const [submitWidth, setSubmitWidth] = useState(0);
  const [value, setValue] = useState("");
  const inputRef = useRef();
  const submitRef = useRef();
  const classes = getClasses({ submitWidth });

  useTransitionEnd(() => {
    !open && setValue("");
  }, inputRef);

  useEffect(() => {
    open && inputRef.current.focus();
  }, [open]);

  useEffect(() => {
    const el = submitRef.current;
    const width = el
      ? el.getBoundingClientRect().width
      : 0;

    setSubmitWidth(width);
  }, [submitRef]);

  const toggleOpen = () => setOpen(state => !state);

  const handleChange = ev => setValue(ev.target.value);

  const submit = ev => {
    ev.preventDefault();
    onSubmit(value);
    setOpen(false);
  };

  const handleClickAway = () => open && setOpen(false);

  return (
    <ClickAwayListener onClickAway={handleClickAway}>
      <div className={cx(classes.root, className, { [classes.open]: open })}>
        <IconButton
          className={classes.handle}
          outlined={false}
          onClick={toggleOpen}
          color="primary"
          active={open}
          icon={icon}
        />

        <form className={classes.form} onSubmit={submit}>
          <input
            ref={inputRef}
            className={classes.fieldInput}
            value={value}
            onChange={handleChange}
            type={type}
          />

          <IconButton
            ref={submitRef}
            type="submit"
            outlined={false}
            color="primary"
            onClick={submit}
            icon={<Enter />}
            className={classes.submit}
          />
        </form>
      </div>
    </ClickAwayListener>
  );
};

export default TextFieldCollapsed;

TextFieldCollapsed.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  type: PropTypes.string,
  icon: PropTypes.element,
};

TextFieldCollapsed.defaultProps = {
  type: "url",
  icon: <Link />,
};
