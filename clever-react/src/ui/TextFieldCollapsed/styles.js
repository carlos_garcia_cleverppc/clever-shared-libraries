import makeStyles from "../styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { shadows, typography } }) => ({
  root: {
    display: "flex",
    borderRadius: 99999,
    backgroundColor: "#fff",
    width: "min-content",
    boxShadow: "0 0 0 rgba(0,0,0, .2)",
    transition: "box-shadow .2s",
  },
  form: {
    display: "flex",
  },
  handle: {
    position: "relative",
    zIndex: 1,
  },
  fieldInput: {
    width: 0,
    padding: 0,
    border: 0,
    fontFamily: typography.font1,
    color: typography.color,
    fontSize: "16px", // warning ios zoom when is smaller
    backgroundColor: "transparent",
    shadow: shadows.hover,
    boxSizing: "border-box",
    willChange: "width, padding",
    transition: "width .3s, padding .3s",
    "&:focus": {
      outline: 0,
    },
  },
  submit: ({ submitWidth }) => ({
    transition: "margin .1s",
    marginLeft: submitWidth * -1,
  }),
  open: {
    "& $fieldInput": {
      width: "calc(100vw - 200px)",
      padding: "0 10px",
      "@media(min-width: 520px)": {
        width: 200,
      },
    },
    "& $submit": {
      marginLeft: 0,
    },
    "&$root": {
      boxShadow: shadows.hover,
    },
  },
}));

export default getClasses;
