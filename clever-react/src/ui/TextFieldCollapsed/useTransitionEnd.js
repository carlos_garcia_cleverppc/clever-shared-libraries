/* eslint-disable consistent-return */
import { useEffect } from "react";

const useTransitionEnd = (callback, ref) => {
  useEffect(() => {
    const el = ref.current;
    if (!el) return;

    el.addEventListener("transitionend", callback);

    return () => {
      el.removeEventListener("transitionend", callback);
    };
  }, [callback, ref]);
};

export default useTransitionEnd;
