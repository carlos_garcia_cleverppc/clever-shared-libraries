import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { spacing, typography, colors } }) => ({
  root: { ...spacing["w-100"] },
  endAdorment: { ...spacing["pr-2"] },
  listbox: {
    "&::-webkit-scrollbar": {
      width: "12px",
      backgroundColor: "#fff",
    },
    "&::-webkit-scrollbar-track": { backgroundColor: "#fff" },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: colors.neutral[300],
      borderRadius: "16px",
      border: "4px solid #fff",
    },
  },
  input: { padding: "0.444rem !important", maxHeight: "22px" },
  noOptions: {
    fontSize: typography.fontSizeBase.constant + typography.fontSizeBase.unit,
    fontFamily: typography.font2,
  },
});

export default makeStyles(styles);
