import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import MaterialAutocomplete from "@material-ui/lab/Autocomplete";

import getClasses from "./styles";

function Autocomplete(props) {
  const {
    options, getOptionLabel, renderInput, className,
    onChange, inputValue, closeIcon, renderOption,
    onInputChange, getOptionDisabled, ListboxComponent,
    noOptionsText,
  } = props;

  const classes = getClasses();

  return (
    <MaterialAutocomplete
      classes={{
        endAdornment: classes.endAdornment,
        listbox: classes.listbox,
        noOptions: classes.noOptions,
        input: classes.input }}
      className={classnames(classes.root, className)}
      options={options}
      getOptionLabel={getOptionLabel}
      renderInput={renderInput}
      onChange={onChange}
      inputValue={inputValue}
      closeIcon={closeIcon}
      forcePopupIcon={false}
      renderOption={renderOption}
      onInputChange={onInputChange}
      getOptionDisabled={getOptionDisabled}
      ListboxComponent={ListboxComponent}
      noOptionsText={noOptionsText}
    />
  );
}

Autocomplete.propTypes = {
  className: PropTypes.string,
  inputValue: PropTypes.string.isRequired,
  onInputChange: PropTypes.func,
  onChange: PropTypes.func,
  getOptionLabel: PropTypes.func,
  getOptionDisabled: PropTypes.func,
  options: PropTypes.instanceOf(Object).isRequired,
  renderInput: PropTypes.instanceOf(Object).isRequired,
  closeIcon: PropTypes.instanceOf(Object),
  renderOption: PropTypes.instanceOf(Object),
  noOptionsText: PropTypes.string,
};

Autocomplete.defaultProps = {
  className: "",
  getOptionLabel: () => { },
  getOptionDisabled: () => { },
  closeIcon: undefined,
  renderOption: undefined,
  onInputChange: () => { },
  onChange: () => { },
  noOptionsText: "No options",
};

export default Autocomplete;
