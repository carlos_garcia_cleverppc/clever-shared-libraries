import React from "react";

import Autocomplete from "./index";
import TextField from "../TextField";
import Typography from "../Typography";

export default {
  title: "Autocomplete",
  component: Autocomplete,
};

const Template = args => {
  const top10Films = [
    { title: "The Shawshank Redemption", year: 1994 },
    { title: "The Godfather", year: 1972 },
    { title: "The Godfather: Part II", year: 1974 },
    { title: "The Dark Knight", year: 2008 },
    { title: "12 Angry Men", year: 1957 },
  ];

  const [value, setValue] = React.useState("");

  const handleChange = newValue => {
    if (newValue) setValue(newValue.title);
    else setValue("");
  };

  return (
    <Autocomplete
      {...args}
      inputValue={value}
      options={top10Films}
      onChange={(event, newValue) => handleChange(newValue)}
      getOptionLabel={option => (option ? option.title : "")}
      onInputChange={ev => (ev ? setValue(ev.target.value) : setValue(""))}
      getOptionDisabled={option => option.title === "count"}
      ListboxComponent={listboxProps => (
        <ul {...listboxProps}>
          {listboxProps.children}

          <div>
            <Typography variant="body1" weight="bold">5 suggestions</Typography>
          </div>
        </ul>
      )}
      renderInput={params => (
        <TextField {...params} label="Combo box" variant="outlined" fullWidth />
      )}
      renderOption={option => (
        <div>
          <Typography variant="body1" weight="bold">{option.title}</Typography>

          <Typography variant="body1">{option.year}</Typography>
        </div>
      )}
    />
  );
};

export const SimpleAutocomplete = Template.bind({});
SimpleAutocomplete.args = {};
