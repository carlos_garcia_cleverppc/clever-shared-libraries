import React from "react";
import PropTypes from "prop-types";

import { ThemeProvider, StylesProvider } from "@material-ui/styles";
import { createTheme } from "@material-ui/core/styles";

import themeConfig from "./themeConfig";
import GlobalStyles from "./GlobalStyles";

const Provider = ({ children, theme }) => {
  const themeBuilder = createTheme(themeConfig(theme));

  return (
    <StylesProvider injectFirst>
      <ThemeProvider theme={themeBuilder}>
        <GlobalStyles />
        {children}
      </ThemeProvider>
    </StylesProvider>
  );
};

Provider.propTypes = { theme: PropTypes.instanceOf(Object) };

Provider.defaultProps = { theme: {} };

export default Provider;
