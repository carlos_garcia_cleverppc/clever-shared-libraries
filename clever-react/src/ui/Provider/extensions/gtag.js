/* eslint-disable prefer-rest-params */
/* eslint-disable no-inner-declarations */
import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";

const GoogleTagManager = props => {
  const { dataLayerName, id, sandbox, children } = props;

  useEffect(() => {
    if (!!id && id.trim().length > 0 && !window.gtag && window[dataLayerName]) {
      const script = document.createElement("script");

      script.src = `https://www.googletagmanager.com/gtag/js?id=${id}`;

      script.async = true;
      script.defer = false;

      document.body.appendChild(script);

      if (!sandbox) {
        function gtag() { window[dataLayerName].push(arguments); }

        gtag("js", new Date());
        gtag("config", id);

        window.gtag = gtag;
        window.gtagConfig = { id, dataLayerName, sandbox };
      }
    }
  }, [dataLayerName, id, sandbox]);

  return (
    <>{children}</>
  );
};

GoogleTagManager.propTypes = {
  id: PropTypes.string.isRequired,
  dataLayerName: PropTypes.string,
  sandbox: PropTypes.bool,
};

GoogleTagManager.defaultProps = {
  dataLayerName: "dataLayer",
  sandbox: false,
};

export default GoogleTagManager;
