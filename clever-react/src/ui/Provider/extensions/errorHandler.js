/* eslint-disable no-param-reassign */

import React from "react";
import { connect } from "react-redux";
import produce from "immer";
import PropTypes from "prop-types";
import ModuleLoader from "../../ModuleLoader";

import { actions } from "../../../utils/redux";

const reducer = (state, action) => (
  produce(state || { hasError: false, code: false }, possibleState => {
    switch (action.type) {
      case actions.LAUNCH_ERROR: {
        possibleState.code = action.payload.code;
        possibleState.hasError = action.payload.hasError;

        break;
      }
      default: {
        // Do nothing
      }
    }
  })
);

const Handler = ({ children, pageErrors, hasError, code }) => {
  if (hasError) {
    if (!pageErrors || !pageErrors[code]) {
      return (<>{children}</>);
    }

    const PageError = pageErrors[code];

    return (<PageError>{children}</PageError>);
  }
  return (<>{children}</>);
};

export const moduleConfig = {
  id: "errorHandler",
  reducerMap: { errorHandler: reducer },
  initialActions: [],
  finalActions: [],
};

Handler.propTypes = { pageErrors: PropTypes.instanceOf(Object) };
Handler.defaultProps = { pageErrors: {} };

const mapStateToProps = ({ errorHandler }) => ({
  code: errorHandler ? errorHandler.code : false,
  hasError: errorHandler ? errorHandler.hasError : false,
});

export default props => (
  <ModuleLoader
    Component={connect(mapStateToProps)(Handler)}
    config={moduleConfig}
    componentProps={props}
  />
);
