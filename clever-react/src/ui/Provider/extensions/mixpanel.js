/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-multi-comp */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import mixpanel from "mixpanel-browser";
import produce from "immer";

import ModuleLoader from "../../ModuleLoader";

import { launchAction } from "../../../utils/redux";
import { init } from "../../../utils/tracking/mixpanel";

const INIT_MIXPANEL = "INIT_MIXPANEL";
const defaultState = { isInit: false, isSandbox: true, options: {}, id: false };

const Mixpanel = ({ mixpanelId, sandbox, dispatch, options, children }) => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (!sandbox || !mixpanelId || mixpanelId.trim().length > 0) {
      mixpanel.init(mixpanelId);
    }

    init({ options, id: mixpanelId, isSandbox: sandbox });
    launchAction(dispatch, INIT_MIXPANEL, { options, id: mixpanelId, isSandbox: sandbox });

    setIsLoading(false);
  }, [mixpanelId, sandbox, options]);

  if (isLoading) return (<></>);

  return (<>{children}</>);
};

Mixpanel.propTypes = {
  mixpanelId: PropTypes.string,
  sandbox: PropTypes.bool,
  options: PropTypes.instanceOf(Object),
};

Mixpanel.defaultProps = { mixpanelId: "", sandbox: false, options: {} };

const Reducer = (state, action) => produce(state || defaultState, possibleState => {
  switch (action.type) {
    case INIT_MIXPANEL: {
      possibleState.options = action.payload.options;
      possibleState.id = action.payload.id;
      possibleState.isSandbox = action.payload.isSandbox;
      possibleState.isInit = true;

      break;
    }
    default: {
      // Do nothing
    }
  }
});

const moduleConfig = {
  id: "MixpanelExtension",
  reducerMap: { mixpanelExtension: Reducer },
  initialActions: [],
  finalActions: [],
};

export default props => (
  <ModuleLoader
    Component={connect()(Mixpanel)}
    config={moduleConfig}
    componentProps={props}
  />
);
