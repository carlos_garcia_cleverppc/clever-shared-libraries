/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-multi-comp */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ReactGA from "react-ga";
import produce from "immer";

import ModuleLoader from "../../ModuleLoader";

import { launchAction } from "../../../utils/redux";
import { init } from "../../../utils/tracking/analytics";

const INIT_ANALYTICS = "INIT_ANALYTICS";
const defaultState = { isInit: false, isSandbox: true, options: {}, id: false };

const Analytics = ({ analyticsId, sandbox, dispatch, options, children }) => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (!sandbox || !analyticsId || analyticsId.trim().length > 0) {
      ReactGA.initialize(analyticsId, options);
    }

    window.analyticsConfig = { options, id: analyticsId, isSandbox: sandbox };

    init({ options, id: analyticsId, isSandbox: sandbox });
    launchAction(dispatch, INIT_ANALYTICS, { options, id: analyticsId, isSandbox: sandbox });

    setIsLoading(false);
  }, [analyticsId, sandbox, options]);

  if (isLoading) return (<></>);

  return (<>{children}</>);
};

Analytics.propTypes = {
  analyticsId: PropTypes.string,
  sandbox: PropTypes.bool,
  options: PropTypes.instanceOf(Object),
};

Analytics.defaultProps = { analyticsId: "", sandbox: false, options: {} };

const Reducer = (state, action) => produce(state || defaultState, possibleState => {
  switch (action.type) {
    case INIT_ANALYTICS: {
      possibleState.options = action.payload.options;
      possibleState.id = action.payload.id;
      possibleState.isSandbox = action.payload.isSandbox;
      possibleState.isInit = true;

      break;
    }
    default: {
      // Do nothing
    }
  }
});

const moduleConfig = {
  id: "AnalyticsExtension",
  reducerMap: { analyticsExtension: Reducer },
  initialActions: [],
  finalActions: [],
};

export default props => (
  <ModuleLoader
    Component={connect()(Analytics)}
    config={moduleConfig}
    componentProps={props}
  />
);
