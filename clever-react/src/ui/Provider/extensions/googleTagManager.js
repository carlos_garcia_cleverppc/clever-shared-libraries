/* eslint-disable prefer-rest-params */
/* eslint-disable no-inner-declarations */
import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import gtmParts from "react-google-tag-manager";

const GoogleTagManager = props => {
  const { dataLayerName, scriptId, id, children } = props;
  const { additionalEvents, previewVariables, sandbox } = props;

  useEffect(() => {
    if (!!id && id.trim().length > 0) {
      if (!window[dataLayerName]) {
        const gtmScriptNode = document.getElementById(scriptId);

        // eslint-disable-next-line no-eval
        eval(gtmScriptNode.textContent);
      }
    }
  }, [dataLayerName, id, scriptId, sandbox]);

  if (!!id && id.trim().length > 0) {
    const gtm = gtmParts({
      id,
      dataLayerName,
      additionalEvents,
      previewVariables: previewVariables.trim().length === 0 ? false : previewVariables,
    });

    return (
      <>
        {gtm.noScriptAsReact()}

        <div id={scriptId || ""}>
          {gtm.scriptAsReact()}
        </div>

        {children}
      </>
    );
  }
  return (
    <>{children}</>
  );
};

GoogleTagManager.propTypes = {
  id: PropTypes.string.isRequired,
  dataLayerName: PropTypes.string,
  additionalEvents: PropTypes.instanceOf(Object),
  previewVariables: PropTypes.string,
  scriptId: PropTypes.string,
  sandbox: PropTypes.bool,
};

GoogleTagManager.defaultProps = {
  dataLayerName: "dataLayer",
  additionalEvents: {},
  previewVariables: "",
  scriptId: "react-google-tag-manager-gtm",
  sandbox: false,
};

export default GoogleTagManager;
