/* eslint-disable react/no-multi-comp */
/* eslint-disable react-hooks/exhaustive-deps */

import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import produce from "immer";

import ModuleLoader from "../../ModuleLoader";

import { launchAction } from "../../../utils/redux";
import { init } from "../../../utils/tracking/smartlook";

const INIT_SMARTLOOK = "INIT_SMARTLOOK";
const defaultState = { isInit: false, isSandbox: true, id: false };

const Smartlook = ({ smartlookId, sandbox, dispatch, children }) => {
  useEffect(() => {
    init(smartlookId, sandbox);
    launchAction(dispatch, INIT_SMARTLOOK, { id: smartlookId, isSandbox: sandbox });
  }, [smartlookId, sandbox]);

  return (<>{children}</>);
};

Smartlook.propTypes = {
  smartlookId: PropTypes.string.isRequired,
  sandbox: PropTypes.bool,
};

Smartlook.defaultProps = { sandbox: false };

const Reducer = (state, action) => produce(state || defaultState, possibleState => {
  switch (action.type) {
    case INIT_SMARTLOOK: {
      possibleState.id = action.payload.id;
      possibleState.isSandbox = action.payload.isSandbox;
      possibleState.isInit = true;

      break;
    }
    default: {
      // Do nothing
    }
  }
});

const moduleConfig = {
  id: "SmartlookExtension",
  reducerMap: { smartlookExtension: Reducer },
  initialActions: [],
  finalActions: [],
};

export default props => (
  <ModuleLoader
    Component={connect()(Smartlook)}
    config={moduleConfig}
    componentProps={props}
  />
);
