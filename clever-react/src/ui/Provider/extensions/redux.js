import React from "react";
import PropTypes from "prop-types";

import { Provider as ReduxProvider } from "react-redux";
import { createStore } from "redux-dynamic-modules-core";
import { getThunkExtension } from "redux-dynamic-modules-thunk";

const Provider = ({ modules, children }) => {
  const store = createStore({
    enhancements: [],
    extensions: [getThunkExtension()],
    ...modules,
  });

  return (
    <ReduxProvider store={store}>
      {children}
    </ReduxProvider>
  );
};

Provider.propTypes = { modules: PropTypes.arrayOf(PropTypes.instanceOf(Object)) };
Provider.defaultProps = { modules: [] };

export default Provider;
