import themeConfig from "./themeConfig";

describe("themeConfig", () => {
  test("should get a valid theme", () => {
    const theme = themeConfig();

    const themeExpected = expect.objectContaining({
      cleverUI: expect.objectContaining({
        spacing: expect.any(Object),
        colors: expect.any(Object),
        typography: expect.objectContaining({
          font1: expect.any(String),
          font2: expect.any(String),
          fontWeight: expect.any(Number),
          fontSizeBase: { constant: expect.any(Number), unit: expect.any(String) },
          lineHeight: expect.any(Number),
          color: expect.any(String),
          sizing: {
            h1: expect.any(Number),
            h2: expect.any(Number),
            h3: expect.any(Number),
            h4: expect.any(Number),
            h5: expect.any(Number),
            h6: expect.any(Number),
            body0: expect.any(Number),
            body1: expect.any(Number),
            body2: expect.any(Number),
            body3: expect.any(Number),
            span: expect.any(Number),
          },
        }),
        icons: expect.any(Object),
        shadows: expect.any(Object),
      }),
      overrides: expect.any(Object),
    });

    expect(theme).toEqual(themeExpected);
  });

  test("should override Material UI theme", () => {
    const theme = themeConfig({
      materialUI: {
        typography: {
          fontFamily: "Arial",
        },
      },
    });

    const themeExpected = expect.objectContaining({
      cleverUI: expect.any(Object),
      typography: expect.objectContaining({
        fontFamily: "Arial",
      }),
    });

    expect(theme).toEqual(themeExpected);
  });

  test("should override clever theme", () => {
    const shadowHover = "0 20px 120px rgba(255,0,0,0.3)";

    const theme = themeConfig({
      shadows: {
        hover: shadowHover,
      },
    });

    const themeExpected = expect.objectContaining({
      cleverUI: expect.objectContaining({
        shadows: expect.objectContaining({
          hover: shadowHover,
        }),
      }),
    });

    expect(theme).toEqual(themeExpected);
  });

  test("should override clever theme using value cleverUI property", () => {
    const shadowHover = "90px 0px 120px rgba(0,255,0,0.3)";

    const theme = themeConfig({
      cleverUI: {
        shadows: {
          hover: shadowHover,
        },
      },
    });

    const themeExpected = expect.objectContaining({
      cleverUI: expect.objectContaining({
        shadows: expect.objectContaining({
          hover: shadowHover,
        }),
      }),
    });

    expect(theme).toEqual(themeExpected);
  });

  test("should override both theme", () => {
    const color = "#006d00";

    const theme = themeConfig({
      colors: {
        primary: {
          main: color,
        },
      },
    });

    const themeExpected = expect.objectContaining({
      cleverUI: expect.objectContaining({
        colors: expect.objectContaining({
          primary: expect.objectContaining({
            main: color,
          }),
        }),
      }),
      palette: expect.objectContaining({
        primary: expect.objectContaining({
          main: color,
        }),
      }),
    });

    expect(theme).toEqual(themeExpected);
  });
});
