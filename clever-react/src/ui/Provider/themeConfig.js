/* eslint-disable no-bitwise */
import forEach from "lodash-es/forEach";
import merge from "lodash-es/merge";

import getContrastText from "../styles/contrastText";

import generateSpacing from "../../utils/spacing";
import { getColors } from "../styles/colors";
import createMaterialTheme from "./createMaterialTheme";

function LightenDarkenColor(col, amt) {
  const usePound = col[0] === "#";
  const auxCol = usePound ? col.slice(1) : col;
  const num = parseInt(auxCol, 16);
  let r = (num >> 16) + amt;
  let b = ((num >> 8) & 0x00ff) + amt;
  let g = (num & 0x0000ff) + amt;

  if (r > 255) r = 255;
  else if (r < 0) r = 0;

  if (b > 255) b = 255;
  else if (b < 0) b = 0;

  if (g > 255) g = 255;
  else if (g < 0) g = 0;

  return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
}

export const lighter = (col, amt) => LightenDarkenColor(col, amt);
export const darken = (col, amt) => LightenDarkenColor(col, amt * -1);

function resolveColor(palette, defaultPalette, isGray = false) {
  let colorPalette = { ...palette };

  if (Object.keys(palette).length === 0) {
    colorPalette = defaultPalette;
  } else if (!palette.main && palette.lighter) {
    colorPalette.lighter = palette.lighter;
    colorPalette.light = !palette.light
      ? darken(colorPalette.lighter, 15)
      : palette.light;
    colorPalette.main = darken(palette.light, 15);
    colorPalette.dark = !palette.dark
      ? darken(colorPalette.main, 15)
      : palette.dark;
    colorPalette.darker = !palette.darker
      ? darken(colorPalette.dark, 15)
      : palette.darker;
  } else if (!palette.main && palette.light) {
    colorPalette.lighter = !palette.lighter
      ? lighter(colorPalette.light, 15)
      : palette.lighter;
    colorPalette.light = palette.light;
    colorPalette.main = darken(palette.light, 15);
    colorPalette.dark = !palette.dark
      ? darken(colorPalette.main, 15)
      : palette.dark;
    colorPalette.darker = !palette.darker
      ? darken(colorPalette.dark, 15)
      : palette.darker;
  } else if (!palette.main && palette.dark) {
    colorPalette.darker = !palette.darker
      ? darken(colorPalette.dark, 15)
      : palette.darker;
    colorPalette.dark = palette.dark;
    colorPalette.main = lighter(palette.dark, 15);
    colorPalette.light = !palette.light
      ? lighter(colorPalette.main, 15)
      : palette.light;
    colorPalette.lighter = !palette.lighter
      ? lighter(colorPalette.light, 15)
      : palette.lighter;
  } else if (!palette.main && palette.darker) {
    colorPalette.darker = palette.darker;
    colorPalette.dark = !palette.dark
      ? lighter(colorPalette.darker, 15)
      : palette.dark;
    colorPalette.main = lighter(palette.dark, 15);
    colorPalette.light = !palette.light
      ? lighter(palette.main, 15)
      : palette.light;
    colorPalette.lighter = !palette.lighter
      ? lighter(palette.light, 15)
      : palette.lighter;
  } else {
    colorPalette.main = palette.main;
    colorPalette.dark = !palette.dark
      ? darken(colorPalette.main, 15)
      : palette.dark;
    colorPalette.darker = !palette.darker
      ? darken(colorPalette.dark, 15)
      : palette.darker;
    colorPalette.light = !palette.light
      ? lighter(colorPalette.main, 15)
      : palette.light;
    colorPalette.lighter = !palette.lighter
      ? lighter(colorPalette.light, 15)
      : palette.lighter;
  }

  colorPalette.contrastText = getContrastText(colorPalette.main);

  if (isGray) {
    if (!colorPalette.lighter) {
      colorPalette.lighter = lighter(colorPalette.light, 20);
    }

    if (!colorPalette.darker) {
      colorPalette.darker = darken(colorPalette.dark, 20);
    }
  }

  return colorPalette;
}

const defaultConfig = {
  cleverUI: {
    spacers: {
      values: [0, 4, 8, 12, 16, 24, 32, 48, 64, 96, 128, 192, 256, 384, 512],
      calculateSpace: () => {},
      unit: "px",
    },
    colors: getColors(),
    typography: {
      font1: ["Montserrat", "sans-serif"].join(","),
      font2: ["Nunito", "sans-serif"].join(","),
      fontWeight: 400,
      fontSizeBase: { constant: 18, unit: "px" },
      lineHeight: 1.25,
      color: "#4a4a4a",
      sizing: {
        h1: 36,
        h2: 24,
        h3: 21,
        h4: 18,
        h5: 18,
        h6: 18,
        body0: 14,
        body1: 18,
        body2: 21,
        body3: 24,
        span: 18,
      },
      variants: {
        title: {},
        subtitle: {},
        title2: {},
        paragraph: {},
        inputText: {},
        small: {},
        caption: {},
      },
    },
    icons: { sizeBase: 24 },
    shadows: {
      none: "none",
      hover: "0 2px 20px rgba(0,0,0,0.1)",
      normal: "0 2px 20px rgba(0,0,0,0.1)",
    },
    spaces: {
      xs: 8,
      sm: 16,
      md: 24,
      lg: 48,
      xl: 72,
      xxl: 96,
    },
    breakpoints: {
      values: {
        sm: 800,
        md: 1400,
        lg: 1800,
      },
      up: () => {},
      down: () => {},
    },
  },
};

let currentConfig = defaultConfig;

// TODO: deprecated when remove from styles/sizing
export const getCurrentConfig = () => currentConfig;

export default themeConfig => {
  let cleverConfig = {};

  if (!!themeConfig && !!themeConfig.cleverUI) {
    cleverConfig = themeConfig.cleverUI;
  } else if (!!themeConfig && !themeConfig.materialUI) {
    cleverConfig = themeConfig;
  }

  if (cleverConfig.colors) {
    forEach(Object.keys(cleverConfig.colors), color => {
      cleverConfig.colors[color] = resolveColor(
        cleverConfig.colors[color],
        defaultConfig.cleverUI.colors[color],
        color === "gray"
      );
    });
  }

  cleverConfig = merge(defaultConfig.cleverUI, cleverConfig);

  const materialTheme = createMaterialTheme(cleverConfig);
  const materialThemeCustomized = merge(materialTheme, themeConfig?.materialUI);

  cleverConfig.spacers = {
    ...cleverConfig.spacers,
    calculateSpace: size => cleverConfig.spacers.values[size],
    unit: "px",
  };

  cleverConfig.spacing = generateSpacing(
    cleverConfig.spacers.calculateSpace,
    cleverConfig.spacers.unit,
    cleverConfig.spacers.values.length
  );

  cleverConfig.breakpoints = {
    ...cleverConfig.breakpoints,
    up: breakpoint =>
      `@media(min-width: ${cleverConfig.breakpoints.values[breakpoint]}px)`,
    down: breakpoint =>
      `@media(max-width: ${cleverConfig.breakpoints.values[breakpoint] - 1}px)`,
  };

  cleverConfig.typography.variants = {
    ...cleverConfig.typography.variants,
    title: {
      fontFamily: cleverConfig.typography.font1,
      fontSize: 21,
      fontWeight: 700,
      lineHeight: 1,
      [cleverConfig.breakpoints.up("md")]: {
        fontSize: 24,
      },
      [cleverConfig.breakpoints.up("lg")]: {
        fontSize: 28,
      },
    },
    subtitle: {
      fontFamily: cleverConfig.typography.font2,
      fontSize: 18,
      fontWeight: 400,
      lineHeight: 1.25,
      [cleverConfig.breakpoints.up("sm")]: {
        fontSize: 21,
      },
      [cleverConfig.breakpoints.up("md")]: {
        fontSize: 24,
      },
      [cleverConfig.breakpoints.up("lg")]: {
        fontSize: 28,
      },
    },
    title2: {
      fontFamily: cleverConfig.typography.font1,
      fontSize: 14,
      fontWeight: 600,
      lineHeight: 1.5,
      [cleverConfig.breakpoints.up("md")]: {
        fontSize: 18,
      },
      [cleverConfig.breakpoints.up("lg")]: {
        fontSize: 21,
      },
    },
    paragraph: {
      fontFamily: cleverConfig.typography.font2,
      fontSize: 14,
      fontWeight: 400,
      lineHeight: 1.5,
      [cleverConfig.breakpoints.up("md")]: {
        fontSize: 18,
      },
      [cleverConfig.breakpoints.up("lg")]: {
        fontSize: 21,
      },
    },
    inputText: {
      fontFamily: cleverConfig.typography.font1,
      fontSize: 14,
      fontWeight: 600,
      lineHeight: 1.5,
      [cleverConfig.breakpoints.up("md")]: {
        fontSize: 16,
      },
      [cleverConfig.breakpoints.up("lg")]: {
        fontSize: 18,
      },
    },
    small: {
      fontFamily: cleverConfig.typography.font1,
      fontSize: 12,
      fontWeight: 500,
      lineHeight: 1.75,
      [cleverConfig.breakpoints.up("lg")]: {
        fontSize: 14,
      },
    },
    caption: {
      fontFamily: cleverConfig.typography.font1,
      fontSize: 12,
      fontWeight: 400,
      lineHeight: 1.75,
      letterSpacing: ".1em",
      textTransform: "uppercase",
      [cleverConfig.breakpoints.up("lg")]: {
        fontSize: 14,
      },
    },
  };

  currentConfig = {
    cleverUI: cleverConfig,
    ...materialThemeCustomized,
  };

  return currentConfig;
};

const currentEnvConfig = {
  analytics: { id: "", isSandBox: true, options: {} },
  mixpanel: { id: "", isSandBox: true, options: {} },
  tagManager: { id: "", isSandBox: true },
  gtag: { id: "", isSandBox: true },
  smartlook: { id: "", isSandBox: true },
};

const deprecationWarning = message => {
  if (process.env.NODE_ENV === "development") {
    // eslint-disable-next-line no-console
    console.warn(`[Clever-react]: ${message}`);
  }
};

export const getEnvConfig = () => {
  // deprecation warning
  deprecationWarning("The function 'getEnvConfig' is deprecated");

  return currentEnvConfig;
};

export const initEnvConfig = ({
  analytics,
  gtag,
  tagManager,
  mixpanel,
  smartlook,
}) => {
  // deprecation warning
  deprecationWarning(
    "[Clever-react]: The function 'initEnvConfig' is deprecated"
  );

  if (analytics) {
    currentEnvConfig.analytics = analytics;
  }

  if (
    !currentEnvConfig.analytics.options &&
    currentEnvConfig.analytics.options === undefined
  ) {
    currentEnvConfig.analytics = {
      ...currentEnvConfig.analytics,
      options: {},
    };
  }

  if (mixpanel) {
    currentEnvConfig.mixpanel = mixpanel;
  }

  if (
    !currentEnvConfig.mixpanel.options &&
    currentEnvConfig.mixpanel.options === undefined
  ) {
    currentEnvConfig.mixpanel = {
      ...currentEnvConfig.mixpanel,
      options: {},
    };
  }

  if (gtag) {
    currentEnvConfig.gtag = gtag;
  }

  if (tagManager) {
    currentEnvConfig.tagManager = tagManager;
  }

  if (smartlook) {
    currentEnvConfig.smartlook = smartlook;
  }

  return currentEnvConfig;
};
