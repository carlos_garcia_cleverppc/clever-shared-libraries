import makeStyles from "../styles/makeStyles";

const useStyles = makeStyles(({ cleverUI: { colors, typography } }) => ({
  "@global": {
    html: { lineHeight: 1.15, WebkitTextSizeAdjust: "100%" },
    main: { display: "block" },
    hr: { boxSizing: "content-box", height: "0", overflow: "visible" },
    pre: { fontFamily: "monospace, monospace", fontSize: "1em" },
    "abbr[title]": {
      borderBottom: "none",
      textDecoration: ["underline", "underline dotted"],
    },
    "b, strong": { fontWeight: "bolder" },
    "code, kbd, samp": { fontFamily: "monospace, monospace", fontSize: "1em" },
    "sub, sup": {
      fontSize: "75%",
      lineHeight: 0,
      position: "relative",
      verticalAlign: "baseline",
    },
    sub: { bottom: "-0.25em" },
    sup: { top: "-0.5em" },
    img: { borderStyle: "none" },
    "button, input, optgroup, select, textarea": {
      fontFamily: "inherit",
      fontSize: "100%",
      lineHeight: 1.15,
      margin: "0",
    },
    "button, input": { overflow: "visible" },
    "button, select": { textTransform: "none" },
    "button, [type=\"button\"], [type=\"reset\"], [type=\"submit\"]": { WebkitAppearance: "button" },
    "button::-moz-focus-inner, [type=\"button\"]::-moz-focus-inner, [type=\"reset\"]::-moz-focus-inner, [type=\"submit\"]::-moz-focus-inner": {
      borderStyle: "none",
      padding: "0",
    },
    "button:-moz-focusring, [type=\"button\"]:-moz-focusring, [type=\"reset\"]:-moz-focusring, [type=\"submit\"]:-moz-focusring": {
      outline: "1px dotted ButtonText",
    },
    fieldset: { padding: "0.35em 0.75em 0.625em" },
    legend: {
      boxSizing: "border-box",
      color: "inherit",
      display: "table",
      maxWidth: "100%",
      padding: "0",
      whiteSpace: "normal",
    },
    progress: { verticalAlign: "baseline" },
    textarea: { overflow: "auto" },
    "[type=\"checkbox\"], [type=\"radio\"]": {
      boxSizing: "border-box",
      padding: "0",
    },
    "[type=\"number\"]::-webkit-inner-spin-button, [type=\"number\"]::-webkit-outer-spin-button": { height: "auto" },
    "[type=\"search\"]": { WebkitAppearance: "textfield", outlineOffset: "-2px" },
    "[type=\"search\"]::-webkit-search-decoration": { WebkitAppearance: "none" },
    "::-webkit-file-upload-button": {
      WebkitAppearance: "button",
      font: "inherit",
    },
    details: { display: "block" },
    summary: { display: "list-item" },
    template: { display: "none" },
    "[hidden]": { display: "none" },
    body: {
      fontFamily: typography.font2,
      lineHeight: typography.lineHeight,
      fontSize: typography.sizing.body1,
      color: colors.neutral[500],
      margin: "0",
    },
    small: { fontSize: typography.sizing.body0 },
    "h1, h2, h3, h4, h5, h6": {
      fontFamily: typography.font1,
      color: colors.neutral[900],
    },
    a: {
      backgroundColor: "transparent",
      color: colors.primary[500],
      textDecoration: "none",
      cursor: "pointer",
    },
  },
}));

const GlobalStyles = () => {
  useStyles();

  return null;
};

export default GlobalStyles;
