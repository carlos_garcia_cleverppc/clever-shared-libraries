import React from "react";
import { LazyMotion, domAnimation, m } from "framer-motion";
import PropTypes from "prop-types";

import IconButton from "../IconButton";
import makeStyles from "../styles/makeStyles";
import Icon from "../Icon";
import Grid from "../Grid";

import FeedbackThumbDone from "./FeedbackThumbDone";

const getClasses = makeStyles(() => ({
  thumbWrapper: ({ size }) => ({ height: `${size}px` }),
  thumb: ({ c, variant, size }) => ({
    height: `${variant !== "compact" ? 24 * c : (size / 1.6)}px !important`,
    width: `${variant !== "compact" ? 24 * c : (size / 1.6)}px !important`,
  }),
}));

const FeedbackBlock = ({ variant, handleThumbClick, isSelected, thumbUp, size }) => {
  const c = size / 80;

  const classes = getClasses({ size, c, variant });

  const opinion = isSelected === "up";

  return (
    <LazyMotion features={domAnimation} strict>
      {isSelected && opinion === thumbUp && (
        <FeedbackThumbDone variant={variant} opinion={thumbUp} size={size} />
      )}

      {!isSelected && (
        <m.div>
          <Grid container alignItems="center" className={classes.thumbWrapper}>
            <Grid item>
              <IconButton
                icon={<Icon icon={thumbUp ? "ThumbUp" : "ThumbDown"} className={classes.thumb} />}
                onClick={() => handleThumbClick(thumbUp ? "up" : "down")}
              />
            </Grid>
          </Grid>
        </m.div>
      )}
    </LazyMotion>
  );
};

FeedbackBlock.propTypes = {
  isSelected: PropTypes.oneOf(["up", "down"]),
  handleThumbClick: PropTypes.func,
  thumbUp: PropTypes.bool,
  size: PropTypes.number,
  variant: PropTypes.oneOf(["default", "compact"]),
};

FeedbackBlock.defaultProps = {
  isSelected: undefined,
  handleThumbClick: () => { },
  thumbUp: false,
  size: 80,
  variant: "default",
};

export default FeedbackBlock;
