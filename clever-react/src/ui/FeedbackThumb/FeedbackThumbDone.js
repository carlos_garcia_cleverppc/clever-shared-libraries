import React from "react";
import { m } from "framer-motion";
import PropTypes from "prop-types";

import useTheme from "../styles/useTheme";
import makeStyles from "../styles/makeStyles";
import Icon from "../Icon";

const getClasses = makeStyles(() => ({
  container: { display: "flex" },
  thumb: ({ c, variant, size }) => ({
    position: "absolute",
    top: `${variant !== "compact" ? 24 * c : (size - (size / 1.2))}px`,
    left: `${variant !== "compact" ? 24 * c : (size - (size / 1.2))}px`,
    height: `${variant !== "compact" ? 24 * c : (size / 1.6)}px`,
    width: `${variant !== "compact" ? 24 * c : (size / 1.6)}px`,
  }),
}));

const FeedbackThumbDone = ({ variant, opinion, size }) => {
  const { cleverUI: { colors } } = useTheme();
  const c = size / 80;

  const classes = getClasses({ c, variant, size });

  const isPositive = opinion;
  const color = isPositive ? "yellow" : "primary";

  return (
    <m.div
      initial={{ scale: 0 }}
      animate={{ rotate: 360, scale: 1 }}
      transition={{ ease: [0.4, 1.39, 0.93, 1.01] }}
    >
      {variant !== "compact" && (
        <div className={classes.container}>
          <Icon icon={isPositive ? "ThumbUp" : "ThumbDown"} color={color} className={classes.thumb} />

          <svg width={`${size}px`} size={`${size}px`} viewBox={`0 0 ${size} ${size}`} version="1.1">
            <g id="Final" stroke="none" strokeWidth={`${1 * c}`} fill="none" fillRule="evenodd">
              <g id="Group-2">
                <circle id="Oval" fill={colors[color][100]} cx={`${36 * c}`} cy={`${36 * c}`} r={`${23 * c}`} />
              </g>

              <>
                <circle id="Oval-1" fill={colors[color][100]} cx={`${67 * c}`} cy={`${52 * c}`} r={`${2 * c}`} />
                <circle id="Oval-2" fill={colors[color][100]} cx={`${57 * c}`} cy={`${72 * c}`} r={`${2 * c}`} />
                <circle id="Oval-3" fill={colors[color][100]} cx={`${7 * c}`} cy={`${62 * c}`} r={`${2 * c}`} />
                <circle id="Oval-4" fill={colors[color][100]} cx={`${3 * c}`} cy={`${12 * c}`} r={`${2 * c}`} />
                <circle id="Oval-5" fill={colors[color][100]} cx={`${12 * c}`} cy={`${32 * c}`} r={`${2 * c}`} />
                <circle id="Oval-6" fill={colors[color][100]} cx={`${21 * c}`} cy={`${14 * c}`} r={`${3 * c}`} />
                <circle id="Oval-7" fill={colors[color][100]} cx={`${28 * c}`} cy={`${74 * c}`} r={`${3 * c}`} />
                <circle id="Oval-8" fill={colors[color][100]} cx={`${61 * c}`} cy={`${3 * c}`} r={`${3 * c}`} />
                <circle id="Oval-9" fill={colors[color][100]} cx={`${62 * c}`} cy={`${14 * c}`} r={`${1 * c}`} />
                <circle id="Oval-10" fill={colors[color][100]} cx={`${41 * c}`} cy={`${6 * c}`} r={`${1 * c}`} />
                <circle id="Oval-11" fill={colors[color][100]} cx={`${1 * c}`} cy={`${46 * c}`} r={`${1 * c}`} />
                <circle id="Oval-12" fill={colors[color][100]} cx={`${26 * c}`} cy={`${64 * c}`} r={`${1 * c}`} />
                <circle id="Oval-13" fill={colors[color][100]} cx={`${66 * c}`} cy={`${65 * c}`} r={`${1 * c}`} />
                <circle id="Oval-14" fill={colors[color][100]} cx={`${79 * c}`} cy={`${40 * c}`} r={`${1 * c}`} />
                <circle id="Oval-15" fill={colors[color][100]} cx={`${73 * c}`} cy={`${27 * c}`} r={`${2 * c}`} />
              </>
            </g>
          </svg>
        </div>
      )}

      {variant === "compact" && (
        <div className={classes.container}>
          <Icon icon={isPositive ? "ThumbUp" : "ThumbDown"} color={color} className={classes.thumb} />

          <svg width={`${size}px`} size={`${size}px`} viewBox={`0 0 ${size} ${size}`} version="1.1">
            <g id="Final" stroke="none" strokeWidth={`${1 * c}`} fill="none" fillRule="evenodd">
              <g id="Group-2">
                <circle id="Oval" fill={colors[color][100]} cx={size / 2} cy={size / 2} r={size / 2} />
              </g>
            </g>
          </svg>
        </div>
      )}
    </m.div>
  );
};

FeedbackThumbDone.propTypes = {
  opinion: PropTypes.bool,
  size: PropTypes.number,
};

FeedbackThumbDone.defaultProps = {
  opinion: true,
  size: 80,
};

export default FeedbackThumbDone;
