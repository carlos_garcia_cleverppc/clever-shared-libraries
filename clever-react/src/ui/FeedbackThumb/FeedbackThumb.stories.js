/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import FeedbackThumb from "./index";

export default {
  title: "FeedbackThumb",
  component: FeedbackThumb,
};

const Template = args => {
  const [isSelectedThumbUp, setIsSelectedThumbUp] = React.useState(args.isSelected);

  return (
    <div>
      <FeedbackThumb
        handleThumbClick={() => setIsSelectedThumbUp("up")}
        isSelected={isSelectedThumbUp}
        thumbUp={args.thumbUp}
        size={args.size}
      />
    </div>
  );
};

export const FeedbackThumbSimple = Template.bind({});
FeedbackThumbSimple.args = {
  isSelected: undefined,
  thumbUp: true,
  size: 40,
};
