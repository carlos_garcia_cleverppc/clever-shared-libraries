/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from "react";

import StarsRating from "./index";

export default {
  title: "StarsRating",
  component: StarsRating,
};

const Template = args => {
  const [starsSelected, setStarsSelected] = useState(undefined);

  return (
    <>
      <StarsRating selected={starsSelected} setSelected={setStarsSelected} {...args} />
    </>
  );
};

export const SimpleStarsRating = Template.bind({});
SimpleStarsRating.args = {};
