import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import StarRatingComponent from "react-star-rating-component";

import makeStyles from "../styles/makeStyles";

const getClasses = makeStyles(() => ({
  stars: ({ size }) => ({ fontSize: `${size}px` }),
}));

const StarsRating = props => {
  const { className, defaultStars, selected, setSelected, size, disabled } = props;
  const classes = getClasses({ size });
  const [stars, setStars] = useState(selected || defaultStars);

  const selectStar = value => {
    setStars(value);
    setSelected(value);
  };

  const selectHoldStars = value => {
    setStars(value);
  };

  const defaultHoldStars = () => {
    const holdStars = selected || defaultStars;

    setStars(holdStars);
  };

  return (
    <StarRatingComponent
      name="rating"
      value={stars}
      onStarClick={ev => selectStar(ev)}
      onStarHover={ev => selectHoldStars(ev)}
      onStarHoverOut={ev => defaultHoldStars(ev)}
      className={classnames(classes.stars, className)}
      editing={!disabled}
    />
  );
};

StarsRating.propTypes = {
  className: PropTypes.string,
  selected: PropTypes.oneOf([1, 2, 3, 4, 5]),
  defaultStars: PropTypes.oneOf([1, 2, 3, 4, 5]),
  size: PropTypes.number,
  setSelected: PropTypes.func,
  disabled: PropTypes.bool,
};

StarsRating.defaultProps = {
  className: "",
  selected: undefined,
  defaultStars: 5,
  size: 50,
  setSelected: () => { },
  disabled: false,
};

export default StarsRating;
