import makeStyles from "../../styles/makeStyles";

const styles = ({ cleverUI: { colors, typography, spaces } }) => ({
  root: ({ color, shade, align, variant }) => ({
    color: colors[color] ? colors[color][shade] : color,
    lineHeight: typography.lineHeight,
    textAlign: align,
    ...typography.variants[variant],
  }),
  mb: {
    marginBottom: spaces.sm,
  },
  upperCase: { textTransform: "uppercase!important" },
  downCase: { textTransform: "lowercase!important" },
  capitalize: { textTransform: "capitalize!important" },
  light: { fontWeight: "300!important" },
  normal: { fontWeight: "400!important" },
  bold: { fontWeight: "700!important" },
});

export default makeStyles(styles);
