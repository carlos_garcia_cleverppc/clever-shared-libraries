import makeStyles from "../../../styles/makeStyles";

const styles = ({ cleverUI: { spacing, spaces } }) => ({
  root: {
    backgroundColor: "white",
    height: "100%",
    width: "100%",
    boxSizing: "border-box",
    paddingLeft: spaces.md,
    paddingRight: spaces.md,
  },
  rootCollapsed: { width: "160px" },
  platformLogo: {
    height: "200px",
    display: "flex",
    alignItems: "center",
  },
  body: ({ bodyHeight }) => ({
    height: bodyHeight,
    "&::-webkit-scrollbar": { width: "5px" },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#babac0",
      borderRadius: "16px",
      borderLeft: "1px solid #fff",
    },
  }),
  bodyDesktop: {},
  footer: {
    ...spacing["pb-5"],
    ...spacing["pt-3"],
  },
  footerCollapsed: {
    ...spacing["pb-5"],
    ...spacing["pt-3"],
  },
});

export default makeStyles(styles);
