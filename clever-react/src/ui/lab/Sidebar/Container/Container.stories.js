/* eslint-disable react/no-multi-comp */
import React from "react";

import Container from "./index";

import PlatformLogo from "../PlatformLogo/index";
import SidebarItem from "../Item/index";
import Footer from "../Footer/index";
import FooterActions from "../FooterActions/index";
import UserAccount from "../UserAccount/index";

export default {
  title: "SidebarContainer",
  component: Container,
};

const Template = args => <Container {...args} />;

export const SimpleContainer = Template.bind({});
SimpleContainer.args = {
  isSidebarCollapsed: false,
  platformLogo: (
    <PlatformLogo
      src="https://www.cleverecommerce.com/wp-content/uploads/2016/06/logoCleverEcommerce01.png"
    />
  ),
  body: (
    <>
      <SidebarItem
        name="Home"
        icon="Home"
      />

      <SidebarItem
        name="Settings"
        icon="Home"
        label="PRO+"
      />

      <SidebarItem
        name="Metrics"
        icon="Home"
        childrenItems={[
          <SidebarItem
            name="Child1"
            icon="Home"
            isChild
          />, <SidebarItem
            name="Child2"
            icon="Home"
            isChild
          />]}
      />
    </>
  ),
  footer: (
    <Footer
      actionsPosition="right"
      userAccount={
        <UserAccount accountSelected={{ name: "Test name", accountId: "123456789" }} label="PRO" />
      }
      footerActions={(
        <FooterActions
          items={[
            {
              icon: "Support",
              name: "Contact",
            },
            {
              icon: "Ruler",
              name: "Metrics",
            },
          ]}
        />
      )}
    />
  ),
};
