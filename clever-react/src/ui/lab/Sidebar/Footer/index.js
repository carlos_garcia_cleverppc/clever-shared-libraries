import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import Grid from "../../../Grid";
import Divider from "../../../Divider";

import getClasses from "./styles";

const Footer = props => {
  const classes = getClasses();

  const {
    userAccount,
    footerActions,
    actionsPosition,
    divider,
    isSidebarCollapsed,
  } = props;
  const { className } = props;

  const [itemAccountCols, setItemAccountCols] = useState(8);
  const [itemActionsCols, setItemActionsCols] = useState(4);

  useEffect(() => {
    const actionsElem = document.querySelector("#footer-actions");
    const actionsCount = actionsElem
      ? actionsElem.children[0].children.length
      : 0;
    if (actionsPosition === "right" && !isSidebarCollapsed) {
      if (actionsCount === 1) {
        setItemAccountCols(10);
        setItemActionsCols(2);
      }
    }
  }, [actionsPosition, isSidebarCollapsed]);

  return (
    <>
      {/* isSidebarCollapsed OR actions top */}
      {(isSidebarCollapsed || actionsPosition === "top") && (
        <Grid
          container
          alignItems="center"
          className={classnames(
            className,
            isSidebarCollapsed ? classes.center : ""
          )}
        >
          <Grid
            item
            className={!isSidebarCollapsed ? classes.marginActions : ""}
          >
            {footerActions}
          </Grid>

          {divider && !isSidebarCollapsed && (
            <Grid item xs={12}>
              <div id="divider-footer" className={classes.divider}>
                <Divider />
              </div>
            </Grid>
          )}

          <Grid item>{userAccount}</Grid>
        </Grid>
      )}

      {/* !isSidebarCollapsed AND actions right */}
      {!isSidebarCollapsed && actionsPosition === "right" && (
        <div id="footer-actions-right">
          <Grid
            container
            alignItems="center"
            justify="space-between"
            className={className}
          >
            <Grid item xs={itemAccountCols}>
              {userAccount}
            </Grid>

            <Grid item xs={itemActionsCols}>
              {footerActions}
            </Grid>
          </Grid>
        </div>
      )}
    </>
  );
};

Footer.propTypes = {
  userAccount: PropTypes.instanceOf(Object),
  footerActions: PropTypes.instanceOf(Object),
  actionsPosition: PropTypes.oneOf(["top", "right"]),
  divider: PropTypes.bool,
  isSidebarCollapsed: PropTypes.bool,
  className: PropTypes.string,
};

Footer.defaultProps = {
  userAccount: null,
  footerActions: null,
  actionsPosition: "right",
  divider: false,
  isSidebarCollapsed: false,
  className: "",
};

export default Footer;
