import makeStyles from "../../../styles/makeStyles";

const styles = ({ cleverUI: { spacing } }) => ({
  divider: {
    ...spacing["pb-3"],
    ...spacing["px-4"],
    ...spacing["mr-4"],
  },
  center: {
    width: "fit-content",
    textAlign: "center",
    display: "block",
  },
  marginActions: { ...spacing["ml-3"] },
});

export default makeStyles(styles);
