import makeStyles from "../../../styles/makeStyles";

const styles = ({ cleverUI: { spacing, shadows } }) => ({
  mainContainer: {
    ...spacing["p-6"],
    ...spacing["pb-4"],
    backgroundColor: "white",
    borderRadius: "8px",
    boxShadow: shadows.normal,
    maxWidth: "375px",
  },
  noAccounts: { ...spacing["my-4"] },
  newAccount: {
    ...spacing["mt-2"],
    ...spacing["mb-4"],
  },
  title: { ...spacing["my-2"] },
  accountList: {
    overflowY: "auto",
    maxHeight: "50vh",
    "&::-webkit-scrollbar": { width: "5px" },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#babac0",
      borderRadius: "16px",
      borderLeft: "1px solid #fff",
    },
  },
  listElement: {
    ...spacing["mt-1"],
    cursor: "pointer",
  },
  addAccountContainer: {
    cursor: "pointer",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  addImage: { ...spacing["mr-2"] },
  divider: {
    ...spacing["pl-2"],
    ...spacing["pr-2"],
  },
  signOut: {
    textAlign: "right",
    ...spacing["pl-2"],
    ...spacing["pr-2"],
    ...spacing["my-2"],
  },
  accountInfo: { ...spacing["ml-3"] },
  name: {
    ...spacing["mr-2"],
    maxWidth: "150px",
    overflow: "hidden",
    textOverflow: "ellipsis",
    whiteSpace: "nowrap",
  },
});

export default makeStyles(styles);
