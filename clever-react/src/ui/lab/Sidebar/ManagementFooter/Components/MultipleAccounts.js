/* eslint-disable react/no-unused-prop-types */
import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import BrandIcon from "../../../../BrandIcon";
import IconButton from "../../../../IconButton";
import Grid from "../../../../Grid";
import Switch from "../../../../Switch";
import Typography from "../../../../Typography";

import ChevronRight from "../../../../Icon/ChevronRight";
import PlusRound from "../../../../Icon/PlusRound";

import makeStyles from "../../../../styles/makeStyles";

const styles = ({ cleverUI: { spacing } }) => ({
  root: ({ cursor }) => ({
    ...spacing["p-1"],
    ...spacing["m-1"],
    display: "inline-flex",
    alignItems: "center",
    outline: "none",
    cursor: `${cursor}`,
    marginLeft: "auto",
  }),
  platformLogo: { height: "32px", width: "32px" },
  name: {
    display: "inline-flex",
    alignItems: "center",
  },
  accountName: {
    ...spacing["mb-1"],
    wordBreak: "break-all",
    overflow: "hidden",
    textOverflow: "ellipsis",
    display: "-webkit-box",
    "-webkit-line-clamp": 1,
    "-webkit-box-orient": "vertical",
  },
  chevron: { padding: 0 },
  sameLine: { display: "inline-flex" },
  alert: { ...spacing["mt-3"] },
  arrowPosition: { transform: "rotate(90deg)" },
  switch: { ...spacing["ml-2"] },
  bottomSpace: { ...spacing["mb-2"] },
});

const getClasses = makeStyles(styles);

const MultipleAccounts = props => {
  const classes = getClasses();

  const { data } = props;
  const { name, accounts, addAccount } = data;

  const addAccountColor =
    addAccount && addAccount.color ? addAccount.color : "primary";

  const hasAccounts = accounts.length > 0;
  const [openDropdown, setOpenDropdown] = useState(false);

  const getChannelIcon = () => {
    const auxName = name.toLowerCase();
    let result = "";

    if (auxName.includes("google ads")) {
      result = "GoogleAdsCircle";
    } else if (auxName.includes("google analytics")) {
      result = "GoogleAnalyticsCircle";
    } else if (auxName.includes("google merchant")) {
      result = "GoogleMerchantCircle";
    } else if (auxName.includes("facebook")) {
      result = "FacebookCircle";
    } else if (auxName.includes("twitter")) {
      result = "TwitterCircle";
    } else if (auxName.includes("microsoft")) {
      result = "MicrosoftCircle";
    } else if (auxName.includes("linkedin")) {
      result = "LinkedinCircle";
    } else if (auxName.includes("instagram")) {
      result = "InstagramCircle";
    }

    return result;
  };

  const channelIcon = getChannelIcon();

  return (
    <Grid container alignItems="center" className={classes.root}>
      <Grid item xs={2} className={classes.platformLogo}>
        <BrandIcon icon={channelIcon} size={32} />
      </Grid>

      {hasAccounts && (
        <>
          <Grid item xs={10}>
            <Grid container alignItems="center">
              <Grid item xs={9} className={classes.name}>
                <Typography
                  weight="bold"
                  variant="f1-12"
                  shade={900}
                  className={classes.accountName}
                >
                  {name} ({accounts.length})
                </Typography>
              </Grid>

              <Grid item xs={3} className={classes.sameLine}>
                <IconButton
                  icon={<PlusRound />}
                  active
                  color={addAccountColor}
                  hover="zoomIn"
                  onClick={addAccount.onClick}
                />

                <IconButton
                  icon={<ChevronRight />}
                  active
                  color="neutral"
                  shade={900}
                  onClick={() => setOpenDropdown(!openDropdown)}
                  className={classnames(
                    classes.chevron,
                    openDropdown ? classes.arrowPosition : ""
                  )}
                />
              </Grid>
            </Grid>
          </Grid>

          {openDropdown &&
            accounts.map(account => (
              <Grid item xs={12} className={classes.bottomSpace}>
                <Grid container>
                  <Grid item xs={2} />

                  <Grid item xs={6}>
                    <Grid container>
                      <Grid item xs={12}>
                        <Typography
                          weight="bold"
                          variant="f1-12"
                          shade={900}
                          className={classes.accountName}
                        >
                          {account.name}
                        </Typography>
                      </Grid>

                      {!!account.id && (
                        <Grid item xs={12}>
                          <Typography variant="f2-10">
                            ({account.id})
                          </Typography>
                        </Grid>
                      )}
                    </Grid>
                  </Grid>

                  <Grid item xs={4}>
                    <Switch
                      checked={account.active}
                      onChange={account.setActive}
                      size="small"
                      className={classes.switch}
                    />
                  </Grid>
                </Grid>
              </Grid>
            ))}
        </>
      )}

      {!hasAccounts && (
        <Grid item xs={10}>
          <Grid container alignItems="center">
            <Grid item xs={9}>
              <Typography variant="body0" shade={900}>
                {name} ({accounts.length})
              </Typography>
            </Grid>

            <Grid item xs={3}>
              <IconButton
                icon={<PlusRound />}
                active
                color={addAccountColor}
                hover="zoomIn"
                onClick={addAccount.onClick}
              />
            </Grid>
          </Grid>
        </Grid>
      )}
    </Grid>
  );
};

MultipleAccounts.propTypes = {
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

MultipleAccounts.defaultProps = {
  size: 42,
};

export default MultipleAccounts;
