import split from "lodash-es/split";
import makeStyles from "../../../../../styles/makeStyles";

const dimensions = anchorEl => anchorEl.getBoundingClientRect();

/**
 * Paper Styles
 */

/** Function to place the paper in X axis controlling it doesn't go outside screen */
const getPaperLeftPosition = (anchorEl, paperWidth) => {
  const result =
    dimensions(anchorEl).left + dimensions(anchorEl).width / 2 - paperWidth / 2;

  if (result > 16 && result + paperWidth < window.innerWidth - 16) {
    return result;
  }
  if (result < 16) return 16;
  if (result + paperWidth > window.innerWidth - 16)
    return window.innerWidth - paperWidth - 16;

  return result;
};

/** Function to move arrow up or down anchorEl element */
const getMaxHeight = (anchorEl, placement) => {
  if (split(placement, "-")[0] === "top")
    return dimensions(anchorEl).top - 16 - 32;

  if (split(placement, "-")[0] === "center")
    return window.innerHeight - dimensions(anchorEl).top;

  return window.innerHeight - dimensions(anchorEl).top - 32 - 64;
};

/** Function to place popover paper */
const getPaperPosition = (placement, anchorEl, lockBodyScroll, paperWidth) => {
  const place = { top: "", bottom: "", right: "", left: "" };

  if (split(placement, "-")[0] === "top") {
    return {
      ...place,
      bottom:
        window.innerHeight -
        dimensions(anchorEl).bottom +
        dimensions(anchorEl).height +
        33,
      left: getPaperLeftPosition(anchorEl, paperWidth),
    };
  }

  if (split(placement, "-")[0] === "bottom") {
    return {
      ...place,
      top: dimensions(anchorEl).top + dimensions(anchorEl).height + 16 + 16,
      left: getPaperLeftPosition(anchorEl, paperWidth),
    };
  }

  if (split(placement, "-")[0] === "center") {
    const quantityTop =
      dimensions(anchorEl).top -
      getMaxHeight(anchorEl, placement) / 2 +
      dimensions(anchorEl).height / 2;
    const centerPlacement = {
      ...place,
      top: quantityTop > 16 ? quantityTop : 16,
    };

    if (split(placement, "-")[1] === "left") {
      const lockPos = !lockBodyScroll ? -16 : 0;
      /**
       * If popover cannot be painted left next to anchorEl
       * it will be right next to anchorEl
       */
      if (dimensions(anchorEl).left - 32 - paperWidth < 16) {
        return {
          ...centerPlacement,
          left: dimensions(anchorEl).left + dimensions(anchorEl).width + 4 + 32,
        };
      }

      return {
        ...centerPlacement,
        right:
          window.innerWidth -
          dimensions(anchorEl).right -
          4 +
          dimensions(anchorEl).width +
          10 +
          32 +
          lockPos,
      };
    }

    if (split(placement, "-")[1] === "right") {
      /**
       * If popover cannot be painted right next to anchorEl
       * it will be left next to anchorEl
       */
      if (
        dimensions(anchorEl).left +
          dimensions(anchorEl).width +
          4 +
          32 +
          paperWidth >
        window.innerWidth - 16
      ) {
        const lockPos = !lockBodyScroll ? -16 : 0;

        return {
          ...centerPlacement,
          right:
            window.innerWidth -
            dimensions(anchorEl).right -
            4 +
            dimensions(anchorEl).width +
            10 +
            32 +
            lockPos,
        };
      }
      return {
        ...centerPlacement,
        left: dimensions(anchorEl).left + dimensions(anchorEl).width + 4 + 32,
      };
    }
  }

  return place;
};

/**
 * Arrow Styles
 */
/** Function to get border to paint */
const getArrowColor = (
  placement,
  arrowColor,
  arrowShade,
  colors,
  anchorEl,
  paperWidth
) => {
  const color = colors[arrowColor][arrowShade];
  const borders = {
    borderTop: "",
    borderBottom: "",
    borderRight: "",
    borderLeft: "",
  };

  if (split(placement, "-")[0] === "top")
    return { ...borders, borderTop: color };
  if (split(placement, "-")[0] === "bottom")
    return { ...borders, borderBottom: color };
  /**
   * If popover cannot be painted right next to anchorEl
   * it will be left next to anchorEl
   */
  if (
    dimensions(anchorEl).left +
      dimensions(anchorEl).width +
      4 +
      32 +
      paperWidth >
    window.innerWidth - 16
  ) {
    return { ...borders, borderLeft: color };
  }

  /**
   * If popover cannot be painted left next to anchorEl
   * it will be right next to anchorEl
   */

  if (dimensions(anchorEl).left - 32 - paperWidth < 16) {
    return { ...borders, borderRight: color };
  }
  if (split(placement, "-")[1] === "left")
    return { ...borders, borderLeft: color };
  if (split(placement, "-")[1] === "right")
    return { ...borders, borderRight: color };
};

/** Function to move arrow up or down anchorEl element */
const arrowTopPlacement = (anchorEl, placement) => {
  let res = window.innerHeight - dimensions(anchorEl).bottom;

  const top = split(placement, "-")[0] === "top";
  const bottom = split(placement, "-")[0] === "bottom";

  if (top) res += dimensions(anchorEl).height + 1;
  if (bottom) res += -32;

  return res;
};

/** Function to place the arrow in X axis */
const getArrowLeftPlacement = (placement, anchorEl) => {
  const result = -16;
  const axisX = split(placement, "-")[1];

  if (axisX === "left") return result + 8;
  if (axisX === "right") return result + dimensions(anchorEl).width - 8;
  return result + dimensions(anchorEl).width / 2;
};

/** Function to place Arrow paper */
const getArrowPosition = (placement, anchorEl, lockBodyScroll, paperWidth) => {
  const place = { top: "", bottom: "", right: "", left: "" };

  if (split(placement, "-")[0] !== "center") {
    return {
      ...place,
      bottom: arrowTopPlacement(anchorEl, placement, lockBodyScroll),
      left:
        dimensions(anchorEl).left + getArrowLeftPlacement(placement, anchorEl),
    };
  }
  if (split(placement, "-")[0] === "center") {
    if (split(placement, "-")[1] === "left") {
      const lockPos = !lockBodyScroll ? -16 : 0;

      /**
       * If popover cannot be painted left next to anchorEl
       * it will be right next to anchorEl
       */
      if (dimensions(anchorEl).left - 32 - paperWidth < 16) {
        return {
          ...place,
          top: dimensions(anchorEl).top - 16 + dimensions(anchorEl).height / 2,
          left: dimensions(anchorEl).left + dimensions(anchorEl).width + 4,
        };
      }

      return {
        ...place,
        top: dimensions(anchorEl).top - 16 + dimensions(anchorEl).height / 2,
        right:
          window.innerWidth -
          dimensions(anchorEl).right -
          4 +
          dimensions(anchorEl).width +
          10 +
          lockPos,
      };
    }
    if (split(placement, "-")[1] === "right") {
      /**
       * If popover cannot be painted right next to anchorEl
       * it will be left next to anchorEl
       */
      if (
        dimensions(anchorEl).left +
          dimensions(anchorEl).width +
          4 +
          32 +
          paperWidth >
        window.innerWidth - 16
      ) {
        const lockPos = !lockBodyScroll ? -16 : 0;

        return {
          ...place,
          top: dimensions(anchorEl).top - 16 + dimensions(anchorEl).height / 2,
          right:
            window.innerWidth -
            dimensions(anchorEl).right -
            4 +
            dimensions(anchorEl).width +
            10 +
            lockPos,
        };
      }
      return {
        ...place,
        top: dimensions(anchorEl).top - 16 + dimensions(anchorEl).height / 2,
        left: dimensions(anchorEl).left + dimensions(anchorEl).width + 4,
      };
    }
  }

  return place;
};

const styles = ({ cleverUI: { colors, shadows } }) => ({
  popover: ({
    anchorEl,
    open,
    paperWidth,
    placement,
    lockBodyScroll,
    zIndex,
  }) => ({
    backgroundColor: "white",
    borderRadius: "8px",
    position: "fixed",
    boxShadow: shadows.normal,
    overflow: "auto",
    zIndex,
    width: paperWidth,

    left: anchorEl
      ? getPaperPosition(placement, anchorEl, lockBodyScroll, paperWidth).left
      : "",
    bottom: anchorEl
      ? getPaperPosition(placement, anchorEl, lockBodyScroll, paperWidth).bottom
      : "",
    top: anchorEl
      ? getPaperPosition(placement, anchorEl, lockBodyScroll, paperWidth).top
      : "",
    right: anchorEl
      ? getPaperPosition(placement, anchorEl, lockBodyScroll, paperWidth).right
      : "",

    maxHeight: anchorEl ? getMaxHeight(anchorEl, placement) : "",

    opacity: `${open ? "1" : "0"}`,
    transition: "opacity 300ms cubic-bezier(.4,1.39,.93,1.01)",

    "&::-webkit-scrollbar": {
      width: "12px",
      backgroundColor: "#fff",
    },
    "&::-webkit-scrollbar-track": { backgroundColor: "#fff" },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#babac0",
      borderRadius: "16px",
      border: "4px solid #fff",
    },
  }),
  portal: ({ open, zIndex }) => ({
    position: "fixed",
    top: "0px",
    left: "0",
    width: "100%",
    height: "100%",
    zIndex: zIndex - 1,
    display: `${!open ? "none" : ""}`,
  }),
  popoverContainer: ({
    anchorEl,
    open,
    placement,
    arrowColor,
    arrowShade,
    lockBodyScroll,
    paperWidth,
    zIndex,
  }) => ({
    "&:before": {
      content: "''",
      position: "fixed",
      border: "solid 16px transparent",
      zIndex: zIndex + 1,

      left: anchorEl
        ? getArrowPosition(
            placement,
            anchorEl,
            lockBodyScroll,
            paperWidth,
            lockBodyScroll
          ).left
        : "",
      right: anchorEl
        ? getArrowPosition(
            placement,
            anchorEl,
            lockBodyScroll,
            paperWidth,
            lockBodyScroll
          ).right
        : "",
      bottom: anchorEl
        ? getArrowPosition(
            placement,
            anchorEl,
            lockBodyScroll,
            paperWidth,
            lockBodyScroll
          ).bottom
        : "",
      top: anchorEl
        ? getArrowPosition(
            placement,
            anchorEl,
            lockBodyScroll,
            paperWidth,
            lockBodyScroll
          ).top
        : "",

      borderTopColor: anchorEl
        ? getArrowColor(
            placement,
            arrowColor,
            arrowShade,
            colors,
            anchorEl,
            paperWidth
          ).borderTop
        : "",
      borderBottomColor: anchorEl
        ? getArrowColor(
            placement,
            arrowColor,
            arrowShade,
            colors,
            anchorEl,
            paperWidth
          ).borderBottom
        : "",
      borderRightColor: anchorEl
        ? getArrowColor(
            placement,
            arrowColor,
            arrowShade,
            colors,
            anchorEl,
            paperWidth
          ).borderRight
        : "",
      borderLeftColor: anchorEl
        ? getArrowColor(
            placement,
            arrowColor,
            arrowShade,
            colors,
            anchorEl,
            paperWidth
          ).borderLeft
        : "",

      opacity: `${open ? "1" : "0"}`,
      transition: "opacity 300ms cubic-bezier(.4,1.39,.93,1.01)",
    },
  }),
});

export default makeStyles(styles);
