import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import Icon from "../../../Icon";
import IconButton from "../../../IconButton";
import Tooltip from "../../../Tooltip";
import Typography from "../../Typography";
import ListItem from "../../../List/Item";
import ChevronRight from "../../../Icon/ChevronRight";
import Dropdown from "../Dropdown";

import getClasses from "./styles";

const SidebarItem = props => {
  const { name, icon, color, className, onClick, isSelected } = props;
  const { childrenItems, isChild, isSidebarCollapsed, label, openList } = props;

  /* DropdownState: openList can come from the project or if isSelected */
  const [open, setOpen] = useState(isSelected || openList);

  const hasChildren = childrenItems.length > 0;

  const classes = getClasses({ color });

  const handleOnClick = () => {
    if (hasChildren) setOpen(!open);
    onClick();
  };

  return (
    <>
      {/* Open dropdown is controlled from here if hasChildren */}
      <ListItem
        component={isSidebarCollapsed ? "li" : "button"}
        key={name}
        onClick={handleOnClick}
        hoverSelect={!isSidebarCollapsed}
        className={classnames({
          [classes.selectItemCollapsed]: isSidebarCollapsed,
          [classes.selectItemHover]: !isSidebarCollapsed,
          [classes.selectedSection]: isSelected && !isChild,
          [classes.selectItem]: true,
          [className]: true,
        })}
      >
        {/* Tooltip displayed only if isSidebarCollapsed because only appears the icon */}
        {!!isSidebarCollapsed && (
          <Tooltip title={name}>
            <IconButton
              icon={<Icon icon={icon} />}
              color={isSelected ? color : "neutral"}
              className={classes.iconButton}
            />
          </Tooltip>
        )}

        {/* Icon when !isSidebarCollapsed */}
        {!isSidebarCollapsed && (
          <div className={isChild ? classes.iconOpenChild : classes.iconOpen}>
            <Icon
              className={classes.iconSize}
              icon={icon}
              color={isSelected ? color : "neutral"}
              shade={isSelected ? 500 : 400}
            />
          </div>
        )}

        {/* Item name displayed when !isSidebarCollapsed */}
        {!isSidebarCollapsed && (
          <Typography
            variant={isChild ? "paragraph" : "title2"}
            shade={isSelected ? 700 : 400}
          >
            {name}
          </Typography>
        )}

        {/* Color label displayed when label has a string */}
        {label !== "" && (
          <Typography
            color={color}
            weight="bold"
            variant="small"
            className={classes.label}
          >
            {label}
          </Typography>
        )}

        {/* Chevrons displayed in the correct position when hasChildren */}
        {hasChildren && (
          <ChevronRight
            className={
              open
                ? classnames(classes.arrow, classes.arrowPosition)
                : classes.arrow
            }
          />
        )}
      </ListItem>

      {/* Children list */}
      {hasChildren && open && (
        <Dropdown
          itemsList={childrenItems}
          isSidebarCollapsed={isSidebarCollapsed}
        />
      )}
    </>
  );
};

SidebarItem.propTypes = {
  name: PropTypes.string,
  className: PropTypes.string,
  icon: PropTypes.string,
  isSelected: PropTypes.bool,
  color: PropTypes.string,
  childrenItems: PropTypes.instanceOf(Array),
  isChild: PropTypes.bool,
  onClick: PropTypes.func,
  isSidebarCollapsed: PropTypes.bool,
  label: PropTypes.string,
  openList: PropTypes.bool,
};

SidebarItem.defaultProps = {
  name: "",
  className: "",
  icon: "",
  color: "primary",
  isSelected: false,
  childrenItems: [],
  isChild: false,
  onClick: () => {},
  isSidebarCollapsed: false,
  label: "",
  openList: false,
};

export default SidebarItem;
