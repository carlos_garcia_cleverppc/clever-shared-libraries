import makeStyles from "../../../styles/makeStyles";

const styles = ({ cleverUI: { spacing, colors } }) => ({
  /* Vertical color bar */
  selectedSection: ({ color }) => ({
    "&::before": {
      content: '"  "',
      backgroundColor: colors[color][500],
      height: "20px",
      width: "3px",
      top: "10px",
      left: "-4px",
      position: "absolute",
    },
  }),
  /* Hover shadow in item */
  selectItem: {
    ...spacing["p-0"],
    maxWidth: "100%",
    borderRadius: "8px",

    position: "relative",
    width: "100%",
    appeareance: "none",
    border: "0",
    ...spacing["mb-1"],

    textAlign: "left",
    lineHeight: "inherit",
  },
  selectItemHover: {
    "&:hover, &:focus": {
      backgroundColor: `${colors.neutral[100]} !important`,
      outline: "none",
    },
  },
  selectItemCollapsed: {
    "&:hover, &:focus": { backgroundColor: "transparent" },
  },
  iconButton: {
    "&:hover, &:focus": {
      backgroundColor: `${colors.neutral[100]} !important`,
    },
  },
  /* Item component not collapsed, more space for parents */
  iconOpen: {
    ...spacing["my-2"],
    ...spacing["mt-2"],
    paddingLeft: "9px",
    paddingRight: "9px",
  },
  /* Item component not collapsed, less space for children */
  iconOpenChild: {
    ...spacing["my-1"],
    ...spacing["mt-1"],
    paddingLeft: "9px",
    paddingRight: "9px",
  },
  arrow: {
    "& path": { fill: colors.neutral[800] },
    transform: "rotate(0deg)",

    transition: "transform .05s ease-in-out",
  },
  /* Chevron position if opened or closed */
  arrowPosition: {
    transform: "rotate(90deg)",
  },
  /* Label chip styles */
  label: ({ color }) => ({
    ...spacing["p-1"],
    ...spacing["mx-2"],
    backgroundColor: colors[color][100],
    width: "fit-content",
    borderRadius: "8px",
  }),
});

export default makeStyles(styles);
