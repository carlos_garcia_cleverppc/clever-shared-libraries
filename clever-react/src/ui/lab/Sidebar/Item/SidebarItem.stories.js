/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import SidebarItem from "./index";

export default {
  title: "SidebarItem",
  component: SidebarItem,
};

const Template = args => <SidebarItem {...args} />;

export const SimpleItem = Template.bind({});
SimpleItem.args = { name: "Home", icon: "Home" };

export const ItemWithChildren = Template.bind({});
ItemWithChildren.args = {
  name: "Home",
  icon: "Home",
  childrenItems: [
    <SidebarItem
      name="Child1"
      icon="Cog"
      isChild
    />, <SidebarItem
      name="Child2"
      icon="Ruler"
      isChild
      label="PRO"
    />,
  ],
};
