import makeStyles from "../../../styles/makeStyles";

const styles = () => ({
  image: { maxWidth: "200px", height: "auto", outline: "none" },
  imageCollapsed: { maxWidth: "40px", height: "auto", outline: "none" },
  cursor: { cursor: "pointer" },
});

export default makeStyles(styles);
