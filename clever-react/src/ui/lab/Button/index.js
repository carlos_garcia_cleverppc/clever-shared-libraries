/* eslint-disable react/require-default-props */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import Handle from "./Handle";
import getClasses from "./styles";

const Button = React.forwardRef((props, ref) => {
  const { children, className, variant, size, ...buttonProps } = props;
  const classes = getClasses(props);

  const buttonClassName = classnames(className, classes.root, {
    [classes.outlined]: variant === "secondary",
    [classes.contained]: variant === "primary",
    [classes.medium]: size === "medium",
    [classes.large]: size === "large",
    [classes.small]: size === "small",
  });

  return (
    <Handle className={buttonClassName} ref={ref} {...buttonProps}>
      {children}
    </Handle>
  );
});

Button.propTypes = {
  variant: PropTypes.oneOf(["primary", "secondary"]),
  size: PropTypes.oneOf(["medium", "large", "small"]),
  disabled: PropTypes.bool,
  disabledClickable: PropTypes.bool,
  isLoading: PropTypes.bool,
  href: PropTypes.string,
  target: PropTypes.string,
  onClick: PropTypes.func,
  type: PropTypes.oneOf(["button", "reset", "submit"]),
};

Button.defaultProps = {
  variant: "primary",
  size: "medium",
  type: "button",
};

export default Button;
