import makeStyles from "../../styles/makeStyles";

const styles = ({ cleverUI: { colors, typography, spaces } }) => ({
  root: {
    ...typography.variants.inputText,
    display: "inline-flex",
    placeItems: "center",
    minHeight: 48,
    padding: `${spaces.xs}px ${spaces.sm}px`,
    borderRadius: 8,
  },
  large: {},
  medium: {},
  small: {},
  contained: {
    backgroundColor: colors.primary[500],
    color: colors.primary[100],
    "&:hover": {
      color: colors.white[500],
      backgroundColor: colors.primary[700],
    },
    "&:focus": {
      color: colors.white[500],
      backgroundColor: colors.primary[700],
    },
    "&:disabled": {
      color: colors.primary[300],
      backgroundColor: colors.primary[100],
    },
  },
  outlined: {
    color: colors.primary[500],
    border: "2px solid",
    borderColor: colors.primary[700],

    "&:hover": {
      color: colors.primary[700],
    },
    "&:focus": {
      color: colors.primary[700],
    },
    "&:disabled": {
      color: colors.primary[300],
      backgroundColor: colors.primary[100],
      borderColor: colors.primary[100],
    },
  },
});

export default makeStyles(styles);
