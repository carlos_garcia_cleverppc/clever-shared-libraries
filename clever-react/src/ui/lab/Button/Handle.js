import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import makeStyles from "../../styles/makeStyles";

const getClasses = makeStyles(() => ({
  link: { textDecoration: "none", color: "currentColor" },
  button: { border: 0, "&:not(:disabled)": { cursor: "pointer" } },
}));

const Handle = ({ href, children, target, className, ...attrs }) => {
  const defaultRel = target === "_blank" ? "noopener" : undefined;
  const rel = attrs.rel || defaultRel;

  const classes = getClasses();

  if (href) {
    return (
      <a
        {...attrs}
        className={classnames([className, classes.link])}
        href={href}
        target={target}
        rel={rel}
      >
        {children}
      </a>
    );
  }
  return (
    <button
      {...attrs}
      type="button"
      className={classnames([className, classes.button])}
    >
      {children}
    </button>
  );
};

Handle.propTypes = {
  href: PropTypes.string,
  target: PropTypes.string,
  rel: PropTypes.string,
};

export default Handle;
