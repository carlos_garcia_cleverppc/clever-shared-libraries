import React, { useRef } from "react";
import PropTypes from "prop-types";

import Dialog from "../Dialog";

import Cropper from "./Cropper";
import getClasses from "./styles";

const DialogCropper = props => {
  const {
    title, description, buttonText,
    open, onClose, value, onSubmit,
    maxHeight, maxWidth,
    ...cropperProps
  } = props;

  const cropperRef = useRef();
  const classes = getClasses();

  const cropImage = () => {
    const cropperCanvasOptions = {
      maxWidth,
      maxHeight,
    };
    const canvas = cropperRef.current.cropper.getCroppedCanvas(cropperCanvasOptions);

    onSubmit(canvas.toDataURL());
  };

  return (
    <Dialog
      open={open}
      onClose={onClose}
      description={description}
      title={title}
      buttonPrimary={{
        label: buttonText,
        onClick: cropImage,
      }}
    >
      <Cropper
        ref={cropperRef}
        src={value}
        className={classes.cropper}
        minCropBoxWidth={100}
        minCropBoxHeight={100}
        guides
        autoCropArea={1}
        viewMode={1}
        {...cropperProps}
      />
    </Dialog>
  );
};

DialogCropper.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  maxHeight: PropTypes.number,
  maxWidth: PropTypes.number,
};

DialogCropper.defaultProps = {
  maxHeight: 500,
  maxWidth: 500,
};

export default DialogCropper;
