/* eslint-disable max-len */
/* eslint-disable no-console */
import React, { useState } from "react";

import Button from "../Button";
import DialogCropper from "./index";
import exampleImage from "../ImageUploader/exampleImage";

export default {
  title: "DialogCropper",
  component: DialogCropper,
};

const Template = args => {
  const [open, setOpen] = useState(false);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <>
      <Button color="primary" onClick={handleOpen}>
        Open
      </Button>
      <DialogCropper
        {...args}
        open={open}
        onClose={handleClose}
        onSubmit={handleClose}
      />
    </>
  );
};

export const SimpleDialogCopper = Template.bind({});
SimpleDialogCopper.args = {
  title: "This is a title for a modal dialog",
  description: "This is the description placeholder for any modal dialog. Any component would be optional and depends on its functional purpose.",
  buttonText: "Aceptar",
  value: exampleImage,
};
