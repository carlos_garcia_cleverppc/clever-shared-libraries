import makeStyles from "../styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  cropper: {
    ...spacing["mx-auto"],
    ...spacing["mb-2"],
    width: "100%",
    maxWidth: 600,
    height: 400,
    minHeight: 144,
    maxHeight: "min(400px, 28vh)",
  },
}));

export default getClasses;
