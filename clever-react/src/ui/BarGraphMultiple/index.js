import React from "react";
import PropTypes from "prop-types";

import { BarStack } from "@vx/shape";
import { Group } from "@vx/group";
import { AxisLeft } from "@vx/axis";
import { scaleBand, scaleLinear, scaleOrdinal } from "@vx/scale";
import { useTooltip, TooltipWithBounds } from "@vx/tooltip";
import { localPoint } from "@vx/event";
import { GridRows } from "@vx/grid";

import Typography from "../Typography";

import getClasses from "./styles";
import useTheme from "../styles/useTheme";

const BarGraphMultiple = props => {
  const { graphData, colorArray, colorShade, type, width, height, legendVisualizer } = props;
  const classes = getClasses(props);

  const categoryAPI = Object.keys(graphData[0]).shift();

  const keysToInclude = Object.keys(graphData[0]).slice(1);

  const { cleverUI: { colors, spacing } } = useTheme();

  const axisNum = 3;

  const {
    tooltipData,
    tooltipLeft,
    tooltipTop,
    tooltipOpen,
    showTooltip,
    hideTooltip,
  } = useTooltip();

  // returns a number array with totals for each and every date
  const totals = graphData.reduce((ret, cur) => {
    const t = keysToInclude.reduce((dailyTotal, k) => {
      dailyTotal += +cur[k];
      return dailyTotal;
    }, 0);
    ret.push(t);
    return ret;
  }, []);

  // set maximum width and height for X-Axis and Y-Axis
  const xMax = width - 40;
  const yMax = height - 20;

  // getter function to get the category value for each
  // date - categoryAPI is obtained from props
  const getCategory = d => d[categoryAPI];

  // scales for X-Axis - each date
  const xScale = scaleBand({
    domain: graphData.map(getCategory),
    padding: 0.2,
  });

  // scales for Y-Axis - 0 to maximum of totals in each dates
  const yScale = scaleLinear({
    domain: [0, Math.max(...totals)],
    nice: true,
  });

  // scales for colorings for each key in bar chart - one to one mapping
  const color = scaleOrdinal({
    domain: keysToInclude,
    range: colorArray,
  });

  xScale.rangeRound([0, xMax]);
  yScale.range([yMax, 0]);

  const myChartRef = React.createRef();

  const handleMouseOver = (event, value) => {
    const position = myChartRef.current.getBoundingClientRect();
    const coords = localPoint(event.target.ownerSVGElement, event);

    showTooltip({
      tooltipLeft: coords.x,
      tooltipTop: coords.y - position.height - 60,
      tooltipData: value,
    });
  };

  const tooltipStyles = {
    zIndex: 100000,
    backgroundColor: colors.navyBlue[500],
    minWidth: "110px",
    borderRadius: "8px",
    display: "inline-block",
    whiteSpace: "pre-line",
    ...spacing["pl-3"],
    ...spacing["pr-5"],
    ...spacing["pt-3"],
    ...spacing["pb-4"],
    textAlign: "left",
  };

  const sumArray = arr => arr.reduce((a, b) => a + b, 0);
  const maxValue = Math.max.apply(Math, graphData.map(d => sumArray(Object.keys(d).splice(1).map(k => d[k]))));

  const yScaleGrid = scaleLinear({
    domain: [0, maxValue],
    nice: true,
  });

  yScaleGrid.range([yMax, 0]);
  yScaleGrid.ticks(axisNum);

  const ticks = yScaleGrid.ticks(axisNum);
  const tickFormats = yScaleGrid.tickFormat(axisNum, "~s");
  ticks.map(tickFormats);

  return (
    <div className={classes.root}>
      <svg width={width} height={height} ref={myChartRef} className={classes.chartContainer}>
        <Group left={22} top={7} width={width} height={height}>
          <AxisLeft
            scale={yScaleGrid}
            stroke="transparent"
            tickStroke="transparent"
            tickValues={ticks}
            tickFormat={tickFormats}
            left={-15}
            top={3}
            tickLabelProps={() => ({
              fill: colors.neutral[300],
              fontSize: 12,
            })}
          />

          <GridRows
            scale={yScaleGrid}
            width={xMax}
            numTicks={axisNum}
          />

          <BarStack
            data={graphData}
            keys={keysToInclude}
            x={getCategory}
            xScale={xScale}
            yScale={yScale}
            color={color}
          >
            {barStacks => (barStacks.reverse().map(barStack => barStack.bars.map(bar => (
              <rect
                className={classes.bar}
                key={`bar-stack-${barStack.index}-${bar.index}`}
                x={bar.x}
                y={bar.y}
                ry={2}
                height={bar.height + 3 + bar.y > yMax ? bar.height : bar.height + 3}
                width={6}
                fill={colors[bar.color][colorShade[barStack.index] || 500]}
                onMouseOver={ev => handleMouseOver(ev, bar)}
                onMouseOut={hideTooltip}
              >
                <animate
                  attributeName="height"
                  from={0}
                  to={bar.height + 3 + bar.y > yMax ? bar.height : bar.height + 3}
                  dur="0.3"
                  fill="freeze"
                />

                <animate
                  attributeName="y"
                  from={yMax}
                  to={bar.y}
                  dur="0.3"
                  fill="freeze"
                />

                <animate
                  attributeName="opacity"
                  from={0}
                  to={1}
                  dur="0.3"
                  fill="freeze"
                />
              </rect>
            ))))}
          </BarStack>
        </Group>
      </svg>

      <div className={classes.legend}>
        {keysToInclude.map((key, index) => (
          <div className={classes.legendOption} key={`legeng-${index}`}>
            {legendVisualizer[index] && (
              <div className={classes.legendOptionIcon}>
                {legendVisualizer[index]}
              </div>
            )}

            {!legendVisualizer[index] && (
              <Typography variant="f2-12" weight="bold" className={classes.legendOptionText}>
                {key}
              </Typography>
            )}

            <svg width={4} height={20} className={classes.legendOptionColor}>
              <rect
                ry={2}
                height={20}
                width={4}
                fill={colors[colorArray[index]][colorShade[index] || 500]}
              />
            </svg>
          </div>
        ))}
      </div>

      {tooltipOpen && (
        <TooltipWithBounds
          // set this to random so it correctly updates with parent bounds
          key={Math.random()}
          top={tooltipTop}
          left={tooltipLeft}
          style={tooltipStyles}
        >
          <div className={classes.tooltipContainer}>
            <Typography variant="f2-12" gutterBottom>
              {tooltipData.bar.data[categoryAPI]}
            </Typography>

            {keysToInclude.map((key, index) => (
              <div className={classes.tooltipMetric}>
                {legendVisualizer[index] && (
                  <div className={classes.tooltipIcon}>
                    {legendVisualizer[index]}
                  </div>
                )}

                {!legendVisualizer[index] && (
                  <Typography variant="f2-12" weight="bold" className={classes.tooltipIcon}>
                    {key}
                  </Typography>
                )}

                <Typography variant="f1-12" weight="bold" color="white" className={classes.tooltipText}>
                  {tooltipData.bar.data[key]}
                  {" "}
                  {type}
                </Typography>
              </div>
            ))}
          </div>
        </TooltipWithBounds>
      )}
    </div>
  );
};

BarGraphMultiple.propTypes = {
  height: PropTypes.number,
  width: PropTypes.number,
  type: PropTypes.string,
  graphData: PropTypes.instanceOf(Array).isRequired,
  colorArray: PropTypes.instanceOf(Array).isRequired,
  colorShade: PropTypes.instanceOf(Array),
  legendVisualizer: PropTypes.instanceOf(Array),
};

BarGraphMultiple.defaultProps = {
  height: 300,
  width: 300,
  type: "impressions",
};

export default BarGraphMultiple;
