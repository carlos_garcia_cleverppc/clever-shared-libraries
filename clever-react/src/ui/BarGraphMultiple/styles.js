import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { spacing } }) => ({
  root: ({ width, height }) => ({
    width,
    height,
  }),
  chartContainer: ({ width, height }) => ({
    width,
    height,
  }),
  legend: ({ width }) => ({
    ...spacing["mx-2"],
    maxWidth: width - 16,
    textAlign: "center",
  }),
  legendOption: {
    ...spacing["mx-2"],
    display: "inline-block",
  },
  legendOptionIcon: {
    display: "inline-block",
    height: "20px",
  },
  legendOptionText: {
    display: "inline-block",
    height: "20px",
    verticalAlign: "super",
  },
  legendOptionColor: {
    ...spacing["mx-2"],
    display: "inline-block",
  },
  tooltipContainer: {
    display: "inline-block",
  },
  tooltipMetric: {
    alignItems: "center",
  },
  tooltipIcon: {
    ...spacing["mr-2"],
    display: "inline-block",
  },
  tooltipText: {
    display: "inline-block",
    verticalAlign: "text-top",
    marginTop: "2px",
  },
});

export default makeStyles(styles);
