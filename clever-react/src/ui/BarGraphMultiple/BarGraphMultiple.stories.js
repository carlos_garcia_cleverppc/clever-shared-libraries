/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import BarGraphMultiple from "./index";
import BrandIcon from "../BrandIcon";

export default {
  title: "BarGraphMultiple",
  component: BarGraphMultiple,
};

const Template = args => <BarGraphMultiple {...args} />;

export const SimpleBarGraphMultiple = Template.bind({});
SimpleBarGraphMultiple.args = {
  graphData: [
    {
      date: "02/04/21",
      google: Math.floor(Math.random() * 11),
      facebook: Math.floor(Math.random() * 11),
      twitter: Math.floor(Math.random() * 11),
      microsoft: Math.floor(Math.random() * 11),
    },
    {
      date: "03/04/21",
      google: Math.floor(Math.random() * 11),
      facebook: Math.floor(Math.random() * 11),
      twitter: Math.floor(Math.random() * 11),
      microsoft: Math.floor(Math.random() * 11),
    },
    {
      date: "04/04/21",
      google: Math.floor(Math.random() * 11),
      facebook: Math.floor(Math.random() * 11),
      twitter: Math.floor(Math.random() * 11),
      microsoft: Math.floor(Math.random() * 11),
    },
    {
      date: "05/04/21",
      google: Math.floor(Math.random() * 11),
      facebook: Math.floor(Math.random() * 11),
      twitter: Math.floor(Math.random() * 11),
      microsoft: Math.floor(Math.random() * 11),
    },
    {
      date: "06/04/21",
      google: Math.floor(Math.random() * 11),
      facebook: Math.floor(Math.random() * 11),
      twitter: Math.floor(Math.random() * 11),
      microsoft: Math.floor(Math.random() * 11),
    },
    {
      date: "07/04/21",
      google: Math.floor(Math.random() * 11),
      facebook: Math.floor(Math.random() * 11),
      twitter: Math.floor(Math.random() * 11),
      microsoft: Math.floor(Math.random() * 11),
    },
    {
      date: "08/04/21",
      google: Math.floor(Math.random() * 11),
      facebook: Math.floor(Math.random() * 11),
      twitter: Math.floor(Math.random() * 11),
      microsoft: Math.floor(Math.random() * 11),
    },
    {
      date: "09/04/21",
      google: Math.floor(Math.random() * 11),
      facebook: Math.floor(Math.random() * 11),
      twitter: Math.floor(Math.random() * 11),
      microsoft: Math.floor(Math.random() * 11),
    },
    {
      date: "10/04/21",
      google: Math.floor(Math.random() * 11),
      facebook: Math.floor(Math.random() * 11),
      twitter: Math.floor(Math.random() * 11),
      microsoft: Math.floor(Math.random() * 11),
    },
  ],
  colorArray: ["primary", "primary", "primary", "warning"],
  colorShade: [500, 700, 300],
  legendVisualizer: [
    <BrandIcon icon="GoogleAdsCircle" size={20} />,
    <BrandIcon icon="FacebookCircle" size={20} />,
    <BrandIcon icon="TwitterCircle" size={20} />,
    <BrandIcon icon="MicrosoftCircle" size={20} />,
  ],
};
