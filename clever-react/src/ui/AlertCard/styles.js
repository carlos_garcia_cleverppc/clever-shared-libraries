import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  card: ({ color }) => ({
    backgroundColor: colors[color][100],
    borderRadius: "8px",
    ...spacing["p-4"],
  }),
  icon: { alignSelf: "center" },
  text: {
    alignSelf: "center",
    display: "flex",
  },
  link: ({ color }) => ({
    ...spacing["ml-1"],
    color: colors[color][900],
    textDecoration: "underline",
    fontSize: "14px",
  }),
});

export default makeStyles(styles);
