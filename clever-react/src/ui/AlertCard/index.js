/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import Grid from "../Grid";
import Typography from "../Typography";
import Link from "../Link";
import Warning from "../Icon/Warning";

import getClasses from "./styles";
import useTheme from "../styles/useTheme";
import { getColorsLabels } from "../styles/colors";

const colorsList = getColorsLabels(true);

const AlertCard = props => {
  const { color, text, action, linkText, className } = props;

  const classes = getClasses({ color });

  const { cleverUI: { colors } } = useTheme();

  return (
    <Grid container className={classnames(classes.card, className)}>
      <Grid item xs={1} className={classes.icon}>
        <Warning size={20} color={color === "danger" ? "white" : color} shade={colors[color][700] ? 700 : 500} />
      </Grid>

      <Grid item xs={11} className={classes.text}>
        <Typography
          color={color === "danger" ? "white" : color}
          shade={colors[color][700] ? 700 : 500}
          variant="body0"
          align="left"
        >
          {text}

          {!!linkText && (
            <Link onClick={action} className={classes.link} color={color}>
              {linkText}
            </Link>
          )}
        </Typography>
      </Grid>
    </Grid>
  );
};

AlertCard.propTypes = {
  color: PropTypes.oneOf(colorsList),
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Object)]),
  className: PropTypes.string,
  action: PropTypes.func,
  linkText: PropTypes.string,
};

AlertCard.defaultProps = {
  color: "primary",
  text: "",
  className: "",
  action: () => { },
  linkText: "",
};

export default AlertCard;
