/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import AlertCard from "./index";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels } from "../styles/colors";

export default {
  title: "AlertCard",
  component: AlertCard,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => {
  const getClasses = makeStyles(() => ({ width: { width: "450px" } }));
  const classes = getClasses();
  return (
    <div className={classes.width}>
      <AlertCard {...args} />
    </div>
  );
};

export const SimpleAlertCard = Template.bind({});
SimpleAlertCard.args = { text: "This is the text in the alert card" };

export const LinkAlertCard = Template.bind({});
LinkAlertCard.args = { text: "This is a loooong text in the alert card", color: "warning", linkText: "LinkText" };
