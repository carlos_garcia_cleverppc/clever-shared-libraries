import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  card: {
    width: "250px",
    textAlign: "center",
    backgroundColor: "white",
    borderRadius: "8px",
    overflow: "overlay",
    marginTop: "-164px",
    marginLeft: "40px",
    zIndex: "2",
    boxShadow: "0 2px 21px rgba(0,0,0,.1)",
  },
  header: {
    display: "flex",
    height: "45px",
    backgroundColor: colors.neutral[900],
    alignItems: "center",
    justifyContent: "center",
  },
  content: {
    ...spacing["py-3"],
    height: "75%",
  },
});

export default makeStyles(styles);
