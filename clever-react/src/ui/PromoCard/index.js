/* eslint-disable max-len */
import React from "react";
import PropTypes from "prop-types";

import Typography from "../Typography";
import Currency from "../Currency";

import getClasses from "./styles";

const PromoCard = props => {
  const classes = getClasses();

  const { promoQuantity, promoCurrency, promoText } = props;

  return (
    <>
      <svg width="230" height="200" viewBox="0 0 100 73" xmlns="http://www.w3.org/2000/svg">
        <g fillRule="nonzero" fill="none">
          <path d="M24.165 57.902c14.84 0 25.048-13.441 25.048-28.554C49.213 14.234 33.35 0 18.51 0S0 14.615 0 29.728C0 44.84 9.325 57.902 24.165 57.902Z" fill="#2ECC71" />
          <path d="M55.554 72.774c7.664 0 10.785-10.071 10.785-17.847 0-7.775-6.214-14.078-13.878-14.078-7.665 0-13.988 11.527-13.988 19.303 0 7.776 9.416 12.622 17.08 12.622Z" fill="#F1C40F" /><path d="M82.839 49.943c7.012 0 16.964-8.335 16.964-15.454 0-7.118-9.621-12.082-16.634-12.082-7.012 0-12.697 5.77-12.697 12.89 0 7.118 5.354 14.646 12.367 14.646Z" fill="#3498DB" />
        </g>
      </svg>

      <div className={classes.card}>
        <div className={classes.header}>
          <Typography variant="f1-16" color="white">
            Google Ads
          </Typography>
        </div>

        <div className={classes.content}>
          <Typography variant="f2-28" color="primary" weight="bold">
            <Currency
              quantity={promoQuantity}
              currency={promoCurrency}
            />
          </Typography>

          <Typography variant="f2-12" shade={900}>
            {promoText}
          </Typography>
        </div>
      </div>
    </>
  );
};

PromoCard.propTypes = {
  promoQuantity: PropTypes.number.isRequired,
  promoCurrency: PropTypes.string.isRequired,
  promoText: PropTypes.string.isRequired,
};

export default PromoCard;
