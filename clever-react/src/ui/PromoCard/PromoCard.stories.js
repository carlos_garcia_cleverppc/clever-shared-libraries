import React from "react";

import PromoCard from "./index";

export default {
  title: "PromoCard",
  component: PromoCard,
};

const Template = args => <PromoCard {...args} />;

export const SamplePromoCard = Template.bind({});
SamplePromoCard.args = { promoQuantity: 120.00, promoCurrency: "EUR", promoText: "promotional discount" };
