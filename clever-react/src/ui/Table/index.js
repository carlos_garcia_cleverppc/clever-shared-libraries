/* eslint-disable react/no-multi-comp */
import React, { useRef, useImperativeHandle, forwardRef } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import BaseTable from "../BaseTable";

import getClasses from "./styles";

function printHeaderDefault(value) {
  return value;
}

function printBodyDefault(value) {
  return value;
}

const Table = forwardRef((props, ref) => {
  const { data, headers, rowHover, className } = props;
  const { pagination, showEntries, entriesNumber, entriesOptions } = props;
  const { entriesText, resultsText, previousText, nextText } = props;
  const { printHeader, printBody, selectionCallback, selection } = props;
  const { sortCallback, fullWidth, createRow } = props;

  const classes = getClasses();
  const childRef = useRef();

  useImperativeHandle(ref, () => ({
    checkAllRows(mode) {
      childRef.current.checkAllRows(mode);
    },
    checkAllPageRows(mode) {
      childRef.current.checkAllPageRows(mode);
    },
  }));

  return (
    <BaseTable
      headers={headers}
      data={data}
      classes={{
        paper: classnames(classes.paper, className),
        table: classes.table,
        header: classes.header,
        cell: classes.cell,
        row: rowHover ? classnames(classes.row, classes.rowHover) : classes.row,
        tableBody: classes.tableBody,
        headerSelectionIcon: classes.headerSelectionIcon,
        selectionIcon: classes.selectionIcon,
      }}
      pagination={pagination}
      showEntries={showEntries}
      entriesNumber={entriesNumber}
      entriesOptions={entriesOptions}
      entriesText={entriesText}
      resultsText={resultsText}
      previousText={previousText}
      nextText={nextText}
      printHeader={printHeader}
      printBody={printBody}
      selection={selection}
      selectionCallback={selectionCallback}
      sortCallback={sortCallback}
      fullWidth={fullWidth}
      createRow={createRow}
    />
  );
});

Table.propTypes = {
  className: PropTypes.string,
  headers: PropTypes.instanceOf(Array).isRequired,
  data: PropTypes.instanceOf(Array).isRequired,
  rowHover: PropTypes.bool,
  pagination: PropTypes.bool,
  fullWidth: PropTypes.bool,
  showEntries: PropTypes.bool,
  entriesNumber: PropTypes.number,
  entriesOptions: PropTypes.instanceOf(Array),
  entriesText: PropTypes.string, // Text in select
  resultsText: PropTypes.string, // Text below the table
  previousText: PropTypes.string,
  nextText: PropTypes.string,
  printHeader: PropTypes.func,
  printBody: PropTypes.func,
  selection: PropTypes.bool,
  selectionCallback: PropTypes.func,
  sortCallback: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  createRow: PropTypes.shape({
    callback: PropTypes.func,
    label: PropTypes.string,
    loading: PropTypes.bool,
  }),
};

Table.defaultProps = {
  className: "",
  rowHover: true,
  pagination: true,
  fullWidth: false,
  showEntries: true,
  entriesNumber: 10,
  entriesOptions: [10, 20, 30, 40, 50],
  previousText: "Previous",
  nextText: "Next",
  entriesText: "Show --entries-- entries",
  resultsText: "Showing --number-- of --total-- results",
  printHeader: printHeaderDefault,
  printBody: printBodyDefault,
  selection: false,
  selectionCallback: () => { },
  sortCallback: false,
  createRow: {
    callback: undefined,
    label: "New row",
    loading: false,
  },
};

export default Table;
