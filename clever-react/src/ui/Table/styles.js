import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing, typography } }) => {
  const { sizing } = typography;

  return ({
    paper: { border: `1px solid ${colors.neutral[300]}` },
    table: {
      borderRadius: "8px",
      borderCollapse: "collapse",
      backgroundColor: "white",
    },
    row: { height: "auto" },
    rowHover: { "&:hover": { backgroundColor: colors.neutral[100] } },
    header: {
      ...spacing["px-5"],
      ...spacing["pb-2"],
      ...spacing["pt-3"],
      borderBottom: `1px solid ${colors.neutral[300]}`,
      fontWeight: "bold",
      textTransform: "uppercase",
      backgroundColor: colors.primary[100],
      fontFamily: typography.font1,
      fontSize: sizing.body0,
      color: colors.primary[500],
      "& tr": {
        "& th": {
          ...spacing["pt-4"],
          ...spacing["pb-3"],
          ...spacing["px-5"],
        },
      },
    },
    cell: {
      ...spacing["pt-4"],
      ...spacing["pb-3"],
      fontSize: typography.fontSizeBase.constant + typography.fontSizeBase.unit,
      fontFamily: typography.font2,
      borderBottom: `2px solid ${colors.primary[100]}`,
      // "& p": { color: colors.neutral[700] },
    },
    headerSelectionIcon: { color: colors.primary[500] },
    selectionIcon: { color: colors.primary[500] },
  });
};

export default makeStyles(styles);
