import React from "react";
import PropTypes from "prop-types";

import StepBackward from "../Icon/StepBackward";
import ChevronLeft from "../Icon/ChevronLeft";
import ChevronRight from "../Icon/ChevronRight";
import StepForward from "../Icon/StepForward";

import IconButton from "../IconButton";

import getClasses from "./styles";

const TablePaginationActions = props => {
  const classes = getClasses();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = event => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = event => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = event => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = event => {
    onChangePage(
      event,
      Math.max(0, Math.ceil(count / rowsPerPage) - 1),
    );
  };

  return (
    <div className={classes.pagination}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        icon={<StepBackward />}
        size="small"
      />

      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        icon={<ChevronLeft />}
        size="small"
      />

      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        icon={<ChevronRight />}
        size="small"
      />
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        icon={<StepForward />}
        size="small"
      />
    </div>
  );
};

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

export default TablePaginationActions;
