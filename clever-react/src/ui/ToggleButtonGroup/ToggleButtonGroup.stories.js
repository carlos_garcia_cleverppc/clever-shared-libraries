import React from "react";

import ToggleButtonGoup from "./index";

import AlignRight from "../Icon/AlignRight";
import AlignLeft from "../Icon/AlignLeft";
import AlignCenter from "../Icon/AlignCenter";
import AlignJustify from "../Icon/AlignJustify";

import Bold from "../Icon/Bold";
import Underline from "../Icon/Underline";
import Italic from "../Icon/Italic";

export default {
  title: "ToggleButtonGoup",
  component: ToggleButtonGoup,
};

const TemplateAligment = args => {
  const [alignment, setAlignment] = React.useState("left");

  const handleAlignment = (event, newAlignment) => {
    setAlignment(newAlignment);
  };

  return <ToggleButtonGoup {...args} value={alignment} onChange={handleAlignment} />;
};

export const AlignmentToggleButtonGoup = TemplateAligment.bind({});
AlignmentToggleButtonGoup.args = {
  exclusive: true,
  ariaLabel: "text alignment",
  options: [
    { value: "left", label: <AlignLeft /> },
    { value: "center", label: <AlignCenter /> },
    { value: "right", label: <AlignRight /> },
    { value: "justify", label: <AlignJustify /> },
  ],
};

const TemplateFormat = args => {
  const [formats, setFormats] = React.useState(() => ["bold"]);

  const handleFormat = (event, newFormats) => {
    setFormats(newFormats);
  };

  return <ToggleButtonGoup {...args} value={formats} onChange={handleFormat} />;
};

export const FormatToggleButtonGoup = TemplateFormat.bind({});
FormatToggleButtonGoup.args = {
  ariaLabel: "text format",
  options: [
    { value: "bold", label: <Bold /> },
    { value: "italic", label: <Italic /> },
    { value: "underlined", label: <Underline /> },
  ],
};
