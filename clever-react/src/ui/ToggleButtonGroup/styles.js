import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  root: {
    display: "inline-flex",
    overflow: "hidden",
  },
  option: {
    cursor: "pointer",
    ...spacing["p-3"],
    display: "flex",
    "& svg": {
      width: "14px",
      height: "14px",
    },
    "&:hover": {
      backgroundColor: colors.neutral[100],
    },
  },
  selected: {
    backgroundColor: colors.neutral[100],
    "& path": {
      fill: colors.neutral[900],
    },
  },
});

export default makeStyles(styles);
