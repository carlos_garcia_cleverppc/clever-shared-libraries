/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable-next-line jsx-a11y/click-events-have-key-events */

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import findIndex from "lodash-es/findIndex";
import isArray from "lodash-es/isArray";
import isString from "lodash-es/isString";
import remove from "lodash-es/remove";

import Typography from "../Typography";

import getClasses from "./styles";

const ToggleButtonGroup = ({
  value, options, exclusive, onChange,
}) => {
  const classes = getClasses();

  const isSelected = auxValue => {
    if (exclusive) return auxValue === value;
    if (isArray(value)) return findIndex(value, val => val === auxValue) >= 0;
    return value === auxValue;
  };

  const handleChange = newValue => event => {
    if (exclusive) onChange(event, newValue);
    else if (isArray(value)) {
      if (isSelected(newValue)) {
        const auxList = [...value];

        remove(auxList, obj => obj === newValue);
        onChange(event, auxList);
      } else {
        onChange(event, [...value, newValue]);
      }
    } else onChange(event, [newValue]);
  };

  return (
    <div className={classes.root}>
      {options.map((option, index) => (
        <div
          key={`option-${index}-${option.value}`}
          onClick={handleChange(option.value)}
          className={classnames(classes.option, isSelected(option.value) ? classes.selected : "")}
        >
          {isString(option.label) && (
            <Typography variant="h3" align="center">{option.label}</Typography>
          )}
          {!isString(option.label) && option.label}
        </div>
      ))}
    </div>
  );
};

ToggleButtonGroup.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string, PropTypes.number,
    PropTypes.bool,
    PropTypes.instanceOf(Array),
  ]),
  options: PropTypes.instanceOf(Array).isRequired,
  exclusive: PropTypes.bool,
  onChange: PropTypes.func,
};

ToggleButtonGroup.defaultProps = {
  value: "",
  exclusive: false,
  onChange: () => { },
};

export default ToggleButtonGroup;
