import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, typography, spacing }, breakpoints }) => {
  const { sizing } = typography;

  return {
    link: { textDecoration: "none" },
    autocomplete: { ...spacing["mb-3"] },
    selected: { ...spacing["pl-5"] },
    tools: { ...spacing["mb-4"] },
    chip: { ...spacing["mr-2"], ...spacing["mb-2"] },
    icon: { ...spacing["mr-2"], ...spacing["mb-0"] },
    alertText: { ...spacing["mt-2"], display: "inline-flex", flexWrap: "nowrap" },
    charCount: { marginLeft: "-25px" },
    iconTooltip: { ...spacing["mt-1"] },
    inputEnter: { opacity: 0.01 },
    inputEnterActive: {
      opacity: 1,
      transition: "opacity 500ms ease-in",
    },
    inputLeave: { opacity: 1 },
    inputLeaveActive: {
      opacity: 0.01,
      transition: "opacity 300ms ease-in",
    },
    inputTransition: { zIndex: 1 },
    error: { "& fieldset": { borderColor: colors.danger.main } },
    alignRight: {
      textAlign: "right",
      fontFamily: typography.font1,
      [breakpoints.down("sm")]: { fontSize: "16px" },
      [breakpoints.up("md")]: { fontSize: sizing.body1 },
    },
  };
};

export default makeStyles(styles);
