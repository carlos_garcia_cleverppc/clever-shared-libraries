/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import { getColorsLabels } from "../styles/colors";

import ChipTextField from "./index";
import { urlRegExp, dots } from "./validations";

export default {
  title: "ChipTextField",
  component: ChipTextField,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => {
  const [values, setValues] = React.useState([]);

  const addValue = newValue => {
    setValues([...values, { text: newValue }]);
  };

  const deleteValue = index => {
    const auxValues = [...values];

    auxValues.splice(index, 1);
    setValues(auxValues);
  };

  const deleteAllValues = () => {
    setValues([]);
  };

  const validation = (input, maxLength) => input.length <= maxLength && !urlRegExp.test(input) && !dots.test(input);

  return (
    <ChipTextField
      {...args}
      chips={values}
      addChip={addValue}
      deleteChip={deleteValue}
      clearAll={deleteAllValues}
      validate={validation}
    />
  );
};

export const SimpleChipTextField = Template.bind({});
SimpleChipTextField.args = {
  type: "Headlines",
  maxChars: 35,
  update: () => { },
  placeholder: "Introduce un item",
  maxChipsPlaceholderText: "Ya has introducido el maximo de chips posibles",
  errorTexts: {
    minLengthErrorText: "El texto no contiene el minimo de caracteres",
    reapetedErrorText: "El texto ya esta insertado",
    maxLengthErrorText: "El texto excede el maximo de caracteres",
    inputErrorText: "El texto contiene un caracter no permitido",
  },
  clearAllText: "Clear all",
  warningSaveText: "Recuerda pulsar el intro para introducir el elemento",
  chipsAddedText: "chips añadidos",
};
