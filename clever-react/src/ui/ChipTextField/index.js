import React, { useState } from "react";
import PropTypes from "prop-types";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";

import isEmpty from "lodash-es/isEmpty";

import Grid from "../Grid";
import TextField from "../TextField";
import Chip from "../Chip";
import Link from "../Link";
import InputAdornment from "../InputAdornment";
import IconButton from "../IconButton";
import Alert from "../Icon/Alert";
import Tooltip from "../Tooltip";
import Circle from "../Icon/Circle";
import Enter from "../Icon/Enter";

import Typography from "../Typography";

import getClasses from "./styles";

const ChipTextField = props => {
  const classes = getClasses();

  const { type, chips, addChip, deleteChip, clearAll } = props;
  const { loadingChips, update, validate, maxChips, copies } = props;
  const { autoFocus, scores, chipsColor, maxChars, minChars } = props;
  const { className } = props;

  const [selected, setSelected] = useState(chips.map(aux => aux.text));
  const [text, setText] = useState("");
  const [showAlertSave, setShowAlertSave] = useState(false);
  const [showError, setShowError] = useState(false);
  // const [error, setError] = useState(false);  Para el scoring

  const defaultCopies = {
    placeholder: "Enter an item",
    maxChipsAlert: `You can add up to ${maxChips} items`,
    minLengthAlert: `String must contain at least ${minChars} characters`,
    duplicatedItemAlert: "You already added this string",
    maxLengthAlert: `String must not exceed ${maxChars} characters`,
    inputError: "There are illegal characters in the string",
    clearAll: "Clear all",
    saveWarning: "Please, press Enter to submit",
    chipsAdded: "chips added",
    ...copies,
  };

  const handleChange = event => {
    const textValue = event.target.value;
    setText(textValue);
    if (!!validate && textValue.length > 0 && (selected.indexOf(textValue) > -1
      || textValue.length < minChars || !validate(textValue, maxChars))) {
      setShowAlertSave(false);
      setShowError(true);
    } else {
      if (!showAlertSave) setShowAlertSave(true);
      setShowError(false);
    }
  };

  const addNew = async () => {
    setShowAlertSave(false);
    const isValid = validate(text, maxChars);

    if (selected.length >= maxChips || text.length < minChars || selected.indexOf(text) > -1) {
      // setError(true);
    } else if (!!text && isValid) {
      setSelected(selected.concat(text));
      addChip(text);
      setText("");
      await update(text, type.substring(0, type.length - 1).toLowerCase());
    }
  };

  const onKeyPress = event => {
    const isValid = validate(text, maxChars);

    if (selected.length >= maxChips || text.length < minChars || selected.indexOf(text) > -1) {
      // setError(true);
    } else if (event.key === "Enter" && !!text && isValid) {
      setShowAlertSave(false);
      addNew();
    }
  };

  const handleDelete = async (id, nText) => {
    const selectedDelete = [...selected];
    const index = selectedDelete.indexOf(nText);
    selectedDelete.splice(index, 1);
    setSelected(selectedDelete);
    deleteChip(index);
    await update(id, type.substring(0, type.length - 1).toLowerCase(), 0);
  };

  const deleteAll = () => {
    clearAll();
    setSelected([]);
  };

  const handleOnBlur = event => {
    if (!(!!event.target && event.target.value.length > 0 && !showError)) {
      setShowAlertSave(false);
    }
  };

  const handleOnFocus = () => {
    if (!showError && !showAlertSave) setShowAlertSave(true);
  };

  const errorType = () => {
    if (!!maxChars && text.length > maxChars) {
      return defaultCopies.maxLengthAlert;
    } if (!!minChars && text.length < minChars) {
      return defaultCopies.minLengthAlert;
    } if (selected.indexOf(text) > -1) {
      return defaultCopies.duplicatedItemAlert;
    } if (!validate(text, maxChars)) {
      return defaultCopies.inputError;
    }

    return "";
  };

  return (
    <Grid container className={className}>
      <Grid item xs={12} className={classes.autocomplete}>
        <Grid container justify="center" alignItems="center" spacing>
          <Grid item xs={12}>
            <Grid container justify="center" alignItems="center" spacing>
              <Grid item xs={10}>
                <TextField
                  className={selected.length > 0 ? "" : classes.error}
                  autoFocus={autoFocus}
                  value={text}
                  placeholder={
                    selected.length >= maxChips
                      ? defaultCopies.maxChipsAlert
                      : defaultCopies.placeholder
                  }
                  onChange={ev => handleChange(ev)}
                  onKeyPress={onKeyPress}
                  error={showError}
                  color="secondary"
                  disabled={selected.length >= maxChips}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton color="primary" outlined={false} disabled={!text} icon={<Enter />} onClick={addNew} />
                      </InputAdornment>
                    ),
                  }}
                  onBlur={ev => handleOnBlur(ev)}
                  onFocus={() => handleOnFocus()}
                />
              </Grid>

              <Grid item xs={2}>
                <Typography
                  className={classes.alignRight}
                >
                  {`${text.length} / ${maxChars}`}
                </Typography>
              </Grid>
            </Grid>

            <ReactCSSTransitionGroup
              transitionName={{
                enter: classes.inputEnter,
                enterActive: classes.inputEnterActive,
                leave: classes.inputLeave,
                leaveActive: classes.inputLeaveActive,
              }}
              className={classes.inputTransition}
              transitionEnterTimeout={500}
              transitionLeaveTimeout={300}
            >
              {showError && errorType() ? (
                <Grid container className={classes.alertText}>
                  <Grid item>
                    <Alert color="danger" className={classes.icon} />
                  </Grid>

                  <Grid item>
                    <Typography variant="body1" weight="bold">
                      {errorType()}
                    </Typography>
                  </Grid>
                </Grid>
              ) : null}
            </ReactCSSTransitionGroup>

            <ReactCSSTransitionGroup
              transitionName={{
                enter: classes.inputEnter,
                enterActive: classes.inputEnterActive,
                leave: classes.inputLeave,
                leaveActive: classes.inputLeaveActive,
              }}
              className={classes.inputTransition}
              transitionEnterTimeout={500}
              transitionLeaveTimeout={300}
            >
              {showAlertSave ? (
                <Grid container className={classes.alertText}>
                  <Grid item>
                    <Alert color="warning" className={classes.icon} />
                  </Grid>

                  <Grid item>
                    <Typography color="warning" variant="body1" weight="bold">
                      {defaultCopies.saveWarning}
                    </Typography>
                  </Grid>
                </Grid>
              ) : null}
            </ReactCSSTransitionGroup>
          </Grid>
        </Grid>
      </Grid>

      <Grid item xs={10}>
        <Grid container justify="space-between" className={classes.tools}>
          <Grid item xs={6}>
            <Typography
              variant="f2-16"
              gutterBottom
            >
              {`${chips.length} ${defaultCopies.chipsAdded}`}
            </Typography>
          </Grid>

          <Grid item>
            <Link disabled={isEmpty(selected)} onClick={deleteAll} color="danger">
              {defaultCopies.clearAll}
            </Link>
          </Grid>
        </Grid>

        {chips.length > 0 && (
          <Grid item xs={12} className={classes.chips}>
            <Grid container>
              {chips.map(chip => {
                if (!scores[chip.text] || scores[chip.text] < 0) {
                  return (
                    <Chip
                      key={chip.text}
                      label={chip.text}
                      onDelete={() => handleDelete(chip.id, chip.text)}
                      className={classes.chip}
                      loading={!!loadingChips && loadingChips[chip.text]}
                      color={chipsColor[chip.text]}
                      clickable={false}
                    />
                  );
                }
                return (
                  <Tooltip
                    key={chip.text}
                    title={loadingChips[chip.text] ? "Waiting" : "Score"}
                    description={(scores[chip.text] >= 80 || loadingChips[chip.text]) ? "" : "Score"}
                    icon={<Circle size={24} color={chipsColor[chip.text]} className={classes.iconTooltip} />}
                    type="complete"
                  >
                    <Chip
                      label={chip.text}
                      onDelete={() => handleDelete(chip.id, chip.text)}
                      className={classes.chip}
                      loading={!!loadingChips && loadingChips[chip.text]}
                      color={chipsColor[chip.text]}
                      clickable={false}
                    />
                  </Tooltip>
                );
              })}
            </Grid>
          </Grid>
        )}
      </Grid>
    </Grid>
  );
};

ChipTextField.propTypes = {
  type: PropTypes.string.isRequired,
  className: PropTypes.string,
  chips: PropTypes.instanceOf(Object).isRequired,
  addChip: PropTypes.func.isRequired,
  deleteChip: PropTypes.func.isRequired,
  clearAll: PropTypes.func.isRequired,
  update: PropTypes.func.isRequired,
  validate: PropTypes.func.isRequired,
  loadingChips: PropTypes.instanceOf(Object),
  scores: PropTypes.instanceOf(Object),
  chipsColor: PropTypes.instanceOf(Object),
  maxChars: PropTypes.number,
  minChars: PropTypes.number,
  maxChips: PropTypes.number,
  autoFocus: PropTypes.bool,
  copies: PropTypes.instanceOf(Object),
};

ChipTextField.defaultProps = {
  className: "",
  loadingChips: {},
  scores: {},
  chipsColor: {},
  maxChars: 90,
  minChars: 1,
  maxChips: 10,
  autoFocus: false,
  copies: {},
};

export default ChipTextField;
