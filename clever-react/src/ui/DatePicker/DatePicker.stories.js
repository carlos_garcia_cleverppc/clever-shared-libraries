/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import DatePicker from "./index";

export default {
  title: "DatePicker",
  component: DatePicker,
};

const Template = args => {
  const [date, setDate] = React.useState(new Date());

  return (<DatePicker {...args} onChange={setDate} value={date} />);
};

export const SimpleDatePicker = Template.bind({});
SimpleDatePicker.args = {
  label: "Datepicker",
  disablePast: true,
  type: "standard",
};
