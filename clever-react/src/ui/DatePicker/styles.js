import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { typography, colors } }) => {
  const { sizing } = typography;

  return ({
    notchedOutline: ({ color }) => ({ borderColor: `${colors[color][500]} !important` }),
    notchedOutlineLabel: ({ color }) => ({ color: `${colors[color][500]} !important` }),
    labelRoot: {
      fontFamily: typography.font1,
      fontWeight: "bold",
      fontSize: sizing.body0,
      color: colors.neutral[700],
      marginBottom: "-10px",
      lineHeight: 1,
    },
    shrink: {
      fontFamily: typography.font2,
      transform: "translate(14px, -6px) scale(0.95) !important",
    },
    inputBase: ({ size }) => ({
      height: size === "small" ? "36px" : "56px",
      padding: 0,
      color: typography.color,
      fontSize: sizing.body0,
      fontWeight: "bold",
    }),
    input: () => ({
      "&::placeholder": {
        color: colors.neutral[700],
        fontSize: sizing.body0,
        fontWeight: "bold",
        opacity: 1,
      },
    }),
    singleLine: { height: 30 + typography.fontSizeBase.constant + typography.fontSizeBase.unit },
    standardInput: { height: "auto" },
    labelOutlined: { transform: "translate(14px, 22px) scale(1)" },
    selectInputBase: ({ size }) => ({
      height: size === "small" ? "36px" : "56px",
      padding: "0 10px",
      color: typography.color,
      fontFamily: typography.font1,
      fontSize: "12px",
      fontWeight: "bold",
      borderRadius: 4,
      cursor: "pointer",
    }),
    selectInput: { cursor: "pointer", paddingLeft: "10px", boxSizing: "border-box", width: "auto" },
  });
};

export default makeStyles(styles);
