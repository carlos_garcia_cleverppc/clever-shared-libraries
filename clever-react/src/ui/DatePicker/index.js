import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import DateFnsUtils from "@date-io/date-fns";
import enLocale from "date-fns/locale/en-US";
import esLocale from "date-fns/locale/es";
import frLocale from "date-fns/locale/fr";

import { MuiPickersUtilsProvider, KeyboardDatePicker, DatePicker } from "@material-ui/pickers";

import ChevronDown from "../Icon/ChevronDown";
import getClasses from "./styles";

const CleverDatePicker = props => {
  const { label, value, onChange, className, inputClass, size,
    disablePast, disableFuture, InputLabelProps, InputProps,
    warning, error, format, type, clearable,
    keyboardIcon, positionIcon, KeyboardButtonProps, disabled, locale } = props;

  const getDefaultFormat = () => {
    switch (locale) {
      case "es": return "dd/MM/yyyy";
      case "fr": return "dd/MM/yyyy";
      default: return "MM/dd/yyyy";
    }
  };

  const formatAux = format || getDefaultFormat();

  const getColor = () => {
    if (warning) {
      return "warning";
    } if (error) {
      return "danger";
    }
    return "neutral";
  };

  const getLocale = () => {
    switch (locale) {
      case "es": return esLocale;
      case "fr": return frLocale;
      default: return enLocale;
    }
  };

  const getErrorMessages = () => {
    switch (locale) {
      case "es": return {
        invalidDateMessage: "Formato de fecha inválido",
        maxDateMessage: "No se puede exceder la fecha máxima",
        minDateMessage: "No se puede preceder la fecha mínima",
      };
      case "fr": return {
        invalidDateMessage: "Format de date non valide",
        maxDateMessage: "La date maximale ne peut être dépassée",
        minDateMessage: "La date minimale ne peut être précédée",
      };
      default: return {
        invalidDateMessage: "Invalid date format",
        maxDateMessage: "Date shouldn’t exceed maximum date",
        minDateMessage: "Date shouldn’t be before minimum date",
      };
    }
  };

  const color = getColor();
  const classes = getClasses({ color, size });

  if (type === "keyboard") { // Type: keyboard
    return (
      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={getLocale()}>
        <KeyboardDatePicker
          autoOk
          variant="inline"
          label={label}
          disablePast={disablePast}
          disableFuture={disableFuture}
          value={value}
          onChange={onChange}
          inputVariant="outlined"
          className={className}
          disableToolbar
          keyboardIcon={keyboardIcon}
          format={formatAux}
          disabled={disabled}
          clearable={clearable}
          InputAdornmentProps={{ position: positionIcon }}
          KeyboardButtonProps={{ ...KeyboardButtonProps }}
          InputProps={{
            ...InputProps,
            classes: {
              input: classnames(classes.input, inputClass),
              root: classes.inputBase,
              notchedOutline: (warning || error) ? classes.notchedOutline : "",
            },
          }}
          InputLabelProps={{
            ...InputLabelProps,
            classes: {
              root: (warning || error)
                ? classnames(classes.labelRoot, classes.notchedOutlineLabel)
                : classes.labelRoot,
              outlined: classes.labelOutlined,
              shrink: classes.shrink,
            },
          }}
          {...getErrorMessages()}
        />
      </MuiPickersUtilsProvider>
    );
  }

  return ( // Type: standard or select
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={getLocale()}>
      <DatePicker
        autoOk
        variant="inline"
        label={label}
        disablePast={disablePast}
        disableFuture={disableFuture}
        value={value}
        onChange={onChange}
        inputVariant="outlined"
        className={className}
        disableToolbar
        format={formatAux}
        disabled={disabled}
        clearable={clearable}
        InputProps={{
          ...InputProps,
          ...(type === "select" && {
            startAdornment: keyboardIcon,
            endAdornment: <ChevronDown />,
          }),
          classes: {
            input: classnames(classes.input, { [classes.selectInput]: type === "select" }, inputClass),
            root: type === "select" ? classes.selectInputBase : classes.inputBase,
            notchedOutline: (warning || error) ? classes.notchedOutline : "",
          },
        }}
        InputLabelProps={{
          ...InputLabelProps,
          classes: {
            root: (warning || error)
              ? classnames(classes.labelRoot, classes.notchedOutlineLabel)
              : classes.labelRoot,
            outlined: classes.labelOutlined,
            shrink: classes.shrink,
          },
        }}
        {...getErrorMessages()}
      />
    </MuiPickersUtilsProvider>
  );
};

CleverDatePicker.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.instanceOf(Date)]),
  error: PropTypes.bool,
  warning: PropTypes.bool,
  disablePast: PropTypes.bool,
  disableFuture: PropTypes.bool,
  keyboardIcon: PropTypes.instanceOf(Object),
  positionIcon: PropTypes.oneOf(["start", "end"]),
  size: PropTypes.oneOf(["small", "standard"]),
  type: PropTypes.oneOf(["keyboard", "standard", "select"]),
  format: PropTypes.string,
  disabled: PropTypes.bool,
  locale: PropTypes.oneOf(["en", "es", "fr"]),
};

CleverDatePicker.defaultProps = {
  className: "",
  label: "",
  onChange: () => { },
  value: "",
  error: false,
  warning: false,
  disablePast: false,
  disableFuture: false,
  keyboardIcon: undefined,
  positionIcon: "start",
  size: "standard",
  type: "standard",
  format: undefined,
  disabled: false,
  locale: "en",
};

export default CleverDatePicker;
