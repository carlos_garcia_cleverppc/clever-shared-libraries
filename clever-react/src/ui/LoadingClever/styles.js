import makeStyles from "../styles/makeStyles";

const getClasses = makeStyles(() => ({
  "@keyframes move": {
    "0%": { transform: "translate(-225%,-50%) scale(0)" },
    "25%": { transform: "translate(-225%,-50%) scale(0)" },
    "50%": { transform: "translate(-225%,-50%) scale(1)" },
    "75%": { transform: "translate(-50%,-50%) scale(1)" },
    "100%": { transform: "translate(125%,-50%) scale(1)" },
  },
  "@keyframes resize": {
    "0%": { transform: "translate(125%,-50%) scale(1)" },
    "100%": { transform: "translate(125%,-50%) scale(0)" },
  },
  "@keyframes color": {
    "0%": { background: "#3498d8" },
    "25%": { background: "#ffd427" },
    "50%": { background: "#2ecc71" },
    "75%": { background: "#d9d9d9" },
    "100%": { background: "#3498d8" },
  },
  inner: {
    width: "100%",
    paddingTop: "100%", // aspect ratio
    position: "relative",
    transform: "translateZ(0) scale(1)",
    backfaceVisibility: "hidden",
    transformOrigin: "0 0",

    "& div": {
      position: "absolute",
      width: "20%",
      paddingBottom: "20%",
      borderRadius: "50%",
      top: "50%",
      left: "50%",
      transform: "translate(-200%, -50%) scale(1)",
      background: "#3498d8",
      animation: "$move 1.1764705882352942s infinite cubic-bezier(0,0.5,0.5,1)",
    },
    "& div:nth-child(1)": {
      background: "#d9d9d9",
      transform: "translate(148px,80px) scale(1)",
      animation:
            "$resize 0.29411764705882354s infinite cubic-bezier(0,0.5,0.5,1), $color 1.1764705882352942s infinite step-start",
    },
    "& div:nth-child(2)": {
      animationDelay: "-0.29411764705882354s",
      background: "#3498d8",
    },
    "& div:nth-child(3)": {
      animationDelay: "-0.5882352941176471s",
      background: "#d9d9d9",
    },
    "& div:nth-child(4)": {
      animationDelay: "-0.8823529411764706s",
      background: "#2ecc71",
    },
    "& div:nth-child(5)": {
      animationDelay: "-1.1764705882352942s",
      background: "#ffd427",
    },
  },
  root: {
    maxWidth: "100%",
    display: "inline-block",
    width: ({ size }) => size,
  },
}));

export default getClasses;
