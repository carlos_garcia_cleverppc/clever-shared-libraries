/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import LoadingClever from "./index";

export default {
  title: "LoadingClever",
  component: LoadingClever,
};

const Template = args => <LoadingClever {...args} />;

export const SimpleLoadingClever = Template.bind({});
SimpleLoadingClever.args = {};
