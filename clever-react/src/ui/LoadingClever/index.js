import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import getClasses from "./styles";

const LoadingClever = React.forwardRef(({ size, className }, ref) => {
  const classes = getClasses({ size });
  return (
    <div ref={ref} className={cx(classes.root, className)} role="alert" aria-label="Loading">
      <div className={classes.inner}>
        <div />
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  );
});

export default LoadingClever;

LoadingClever.propTypes = {
  size: PropTypes.number,
};

LoadingClever.defaultProps = {
  size: 200,
};
