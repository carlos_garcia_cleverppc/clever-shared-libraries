import React from "react";
import { screen, render } from "@testing-library/react";
import LoadingClever from ".";
import ThemeProvider from "../Provider";
import { getCurrentConfig } from "../Provider/themeConfig";

const renderWithProviders = ui => render(
  <ThemeProvider theme={getCurrentConfig}>{ui}</ThemeProvider>,
);

describe("<LoadingClever />", () => {
  test("should have accessibility attributes", () => {
    renderWithProviders(<LoadingClever />);

    const alert = screen.getByRole("alert", { name: /loading/i });
    expect(alert).toBeInTheDocument();
  });
});
