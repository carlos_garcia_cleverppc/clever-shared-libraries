/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import RoundedSwitch from "./index";

import { getColorsLabels } from "../styles/colors";

export default {
  title: "RoundedSwitch",
  component: RoundedSwitch,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => {
  const [checked, setChecked] = React.useState(false);

  return <RoundedSwitch {...args} checked={checked} onChange={() => setChecked(!checked)} />;
};

export const SimpleRoundedSwitch = Template.bind({});
SimpleRoundedSwitch.args = {
  options: ["All", "Clever"],
  color: "secondary",
  buttonWidth: "200px",
};
