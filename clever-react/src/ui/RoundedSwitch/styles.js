import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors } }) => ({
  root: {
    height: "32px",
    width: "256px",
    borderRadius: "16px",
    backgroundColor: "white",
    position: "relative",
    "&:focus": { outline: "none" },
  },
  desktopCursor: { cursor: "pointer" },
  button: ({ buttonWidth, color }) => ({
    height: "32px",
    width: buttonWidth,
    borderRadius: "16px",
    background: colors[color][500],
    position: "absolute",
    zIndex: 1,
    transition: "all 700ms cubic-bezier(.01, .81, .44, 1)",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }),
  checkedButton: ({ buttonWidth }) => ({
    transform: `translate(${buttonWidth}, 0)`,
    transition: "all 700ms cubic-bezier(.01, .81, .44, 1)",
  }),
  option0: ({ buttonWidth }) => ({
    height: "32px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: buttonWidth,
    position: "absolute",
  }),
  option1: ({ buttonWidth }) => ({
    height: "32px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: buttonWidth,
    left: buttonWidth,
    position: "absolute",
  }),
});

export default makeStyles(styles);
