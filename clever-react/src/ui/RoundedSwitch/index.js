/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import { onKeyDown } from "../../utils/helpers";
import useMediaQuery from "../styles/useMediaQuery";
import { screenSm, screenXs } from "../styles/breakpoints";
import { getColorsLabels } from "../styles/colors";

import Typography from "../Typography";

import getClasses from "./styles";

const colorsList = getColorsLabels();

const RoundedSwitch = props => {
  const classes = getClasses(props);

  const { checked, onChange, options } = props;
  const { className } = props;

  const isScreenSm = useMediaQuery(screenSm);
  const isScreenXs = useMediaQuery(screenXs);

  return (
    <div onClick={onChange} onKeyDown={onKeyDown(onChange)} className={classnames(classes.root, className, isScreenXs || isScreenSm ? "" : classes.desktopCursor)} role="button" tabIndex={0}>
      <div className={classes.option0}>
        <Typography weight="bold" align="center" variant="f2-14">
          {options[0]}
        </Typography>
      </div>

      <div className={classnames(classes.button, checked ? classes.checkedButton : "")}>
        <Typography color="white" weight="bold" align="center" variant="f2-14">
          {!checked ? options[0] : ""}

          {!checked ? "" : options[1]}
        </Typography>
      </div>

      <div className={classes.option1}>
        <Typography weight="bold" align="center" variant="f2-14">
          {options[1]}
        </Typography>
      </div>
    </div>
  );
};

RoundedSwitch.propTypes = {
  checked: PropTypes.bool,
  options: PropTypes.instanceOf(Array).isRequired,
  color: PropTypes.oneOf(colorsList), // no borrar
  onChange: PropTypes.func,
  className: PropTypes.string,
  buttonWidth: PropTypes.string, // no borrar
};

RoundedSwitch.defaultProps = {
  checked: false,
  color: "secondary",
  onChange: () => { },
  className: "",
  buttonWidth: "128px",
};

export default RoundedSwitch;
