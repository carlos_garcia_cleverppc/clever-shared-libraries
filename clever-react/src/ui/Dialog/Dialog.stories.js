/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Dialog from "./index";

import Button from "../Button";
import Link from "../Link";

export default {
  title: "Dialog",
  component: Dialog,
};

const Template = args => {
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(!open);
  };
  return (
    <div>
      <Button color="primary" onClick={handleOpen}>
        Open
      </Button>

      <Dialog {...args} open={open} onClose={() => setOpen(false)} />
    </div>
  );
};

export const SimpleDialog = Template.bind({});
SimpleDialog.args = {
  title: "This is a title for a modal dialog",
  description: "This is the description placeholder for any modal dialog. Any component would be optional and depends on its functional purpose.",
  showImageMobile: true,
  buttons: (
    <>
      <Link to="https://cleverecommerce.com" target="_blank" external>Links and terciary buttons</Link>

      <Button>
        Create new account
      </Button>
    </>
  ),
};
