import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import MaterialDialog from "@material-ui/core/Dialog";

import Grid from "../Grid";
import IconButton from "../IconButton";
import Button from "../Button";
import Link from "../Link";
import Times from "../Icon/Times";
import Typography from "../Typography";
import Hidden from "../Hidden";
import Drawer from "../Drawer";

import DialogActions from "./Actions";

import getClasses from "./styles";

const Dialog = props => {
  const { children, open, onClose, className, maxWidth, fullWidth } = props;
  const { title, description, image, buttons, showImageMobile, fullWidthContent } = props;
  const { contentSpacing, buttonPrimary, buttonSecondary, darkMode } = props;
  const { variant } = props;

  const classes = getClasses({ darkMode });

  if (variant === "empty") {
    return (
      <>
        <Hidden smDown>
          <MaterialDialog
            open={open}
            onClose={onClose || (() => { })}
            maxWidth={maxWidth}
            fullWidth={fullWidth}
            classes={{ paper: classes.root }}
          >
            <div className={className}>
              {children}
            </div>

            {(buttonPrimary || buttonSecondary || buttons) && (
              <DialogActions
                buttons={buttons}
                buttonPrimary={buttonPrimary}
                buttonSecondary={buttonSecondary}
                darkMode={darkMode}
              />
            )}
          </MaterialDialog>
        </Hidden>

        <Hidden mdUp>
          <Drawer anchor="bottom" open={open} onClose={onClose || (() => { })}>
            {children}
          </Drawer>
        </Hidden>
      </>
    );
  }

  return (
    <>
      <Hidden smDown>
        <MaterialDialog
          open={open}
          onClose={onClose || (() => { })}
          maxWidth={maxWidth}
          fullWidth={fullWidth}
          classes={{ paper: classnames(className, classes.root) }}
        >
          <div className={classes.stripe} />

          <div className={classes.closeIconContainer}>
            {!!onClose && (
              <IconButton
                onClick={onClose}
                size="small"
                icon={<Times />}
                className={classes.iconButton}
                color="neutral"
                shade={100}
                outlined={false}
              />
            )}
          </div>

          <Grid container justify="center" className={classes.container}>
            <Grid
              item
              xs={fullWidthContent ? 12 : 10}
              md={fullWidthContent ? 12 : 8}
              className={fullWidthContent ? classes.fullWidthDialog : ""}
            >
              <Typography variant="body2" shade={darkMode ? 300 : 900} align="center" gutterBottom>
                {title}
              </Typography>

              <Typography variant="body1" align="center">
                {description}
              </Typography>

              <Grid container justify="center" className={classes.content} spacing={2}>
                {!!image && !!children && (
                  <Grid item xs={12} md={4} className={classes.imageContainer}>
                    <Hidden xsDown>
                      <img className={classes.image} src={image} alt="Dialog ilustration" />
                    </Hidden>
                  </Grid>
                )}

                {!!image && !children && (
                  <Grid item xs={12} md={4} className={classes.imageContainer}>
                    <img className={classes.image} src={image} alt="Dialog ilustration" />
                  </Grid>
                )}

                {!!children && (
                  <Grid item xs={12} md={image ? 8 : 12 - contentSpacing}>
                    {children}
                  </Grid>
                )}
              </Grid>
            </Grid>
          </Grid>

          <DialogActions
            buttons={buttons}
            buttonPrimary={buttonPrimary}
            buttonSecondary={buttonSecondary}
            darkMode={darkMode}
          />
        </MaterialDialog>
      </Hidden>

      <Hidden mdUp>
        <Drawer anchor="bottom" open={open} onClose={onClose || (() => { })}>
          <div className={classes.stripe} />

          <div className={classes.closeIconContainer}>
            {!!onClose && (
              <IconButton onClick={onClose} size="small" icon={<Times />} className={classes.iconButton} />
            )}
          </div>

          <Grid container justify="center" className={classes.container}>
            <Grid item xs={10} md={8}>
              <Typography variant="body2" shade={darkMode ? 300 : 900} align="center" gutterBottom>
                {title}
              </Typography>

              <Typography variant="body1" align="center">
                {description}
              </Typography>

              <Grid container justify="center" className={classes.content} spacing={2}>
                {!!image && !!showImageMobile && (
                  <Grid item xs={12} md={4} className={classes.imageContainer}>
                    <img className={classes.image} src={image} alt="Dialog ilustration" />
                  </Grid>
                )}

                {!!children && (
                  <Grid item xs={12} md={8}>
                    {children}
                  </Grid>
                )}
              </Grid>
            </Grid>
          </Grid>

          <Grid container className={classnames(classes.drawerActions, className)} alignItems="center" justify="center">
            {(!!buttonPrimary || !!buttonSecondary) && (
              <>
                {!!buttonSecondary && (
                  <Link
                    to={buttonSecondary.to}
                    target="_blank"
                    onClick={buttonSecondary.onClick}
                    className={classes.secondaryButton}
                    disabled={buttonSecondary?.disabled}
                  >
                    {buttonSecondary.label}
                  </Link>
                )}

                {!!buttonPrimary && (
                  <Button
                    onClick={buttonPrimary.onClick}
                    disabled={buttonPrimary?.disabled}
                    isLoading={buttonPrimary?.isLoading}
                    className={classes.primaryButton}
                  >
                    {buttonPrimary.label}
                  </Button>
                )}
              </>
            )}

            {(!buttonPrimary && !buttonSecondary) && buttons}
          </Grid>
        </Drawer>
      </Hidden>
    </>
  );
};

Dialog.propTypes = {
  open: PropTypes.bool,
  variant: PropTypes.oneOf(["default", "empty"]),
  onClose: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  className: PropTypes.string,
  maxWidth: PropTypes.oneOf(["xs", "sm", "md", "lg", "xl"]),
  fullWidth: PropTypes.bool,
  title: PropTypes.string,
  description: PropTypes.oneOfType([PropTypes.instanceOf(Object), PropTypes.string]),
  image: PropTypes.string,
  buttons: PropTypes.instanceOf(Object),
  showImageMobile: PropTypes.bool,
  contentSpacing: PropTypes.number,
  fullWidthContent: PropTypes.bool,
  buttonPrimary: PropTypes.exact({
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
    isLoading: PropTypes.bool,
  }),
  buttonSecondary: PropTypes.exact({
    label: PropTypes.string.isRequired,
    to: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
  }),
  darkMode: PropTypes.bool,
};

Dialog.defaultProps = {
  open: false,
  variant: "default",
  onClose: false,
  className: "",
  maxWidth: "md",
  fullWidth: true,
  title: "",
  description: "",
  image: "",
  buttons: undefined,
  showImageMobile: false,
  contentSpacing: 4,
  fullWidthContent: false,
  buttonPrimary: undefined,
  buttonSecondary: undefined,
  darkMode: false,
};

export default Dialog;
