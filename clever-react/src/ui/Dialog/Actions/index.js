import React from "react";
import PropTypes from "prop-types";

import classnames from "classnames";
import Grid from "../../Grid";
import Link from "../../Link";
import Button from "../../Button";

import makeStyles from "../../styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { colors, spacing } }) => ({
  root: ({ darkMode }) => ({
    ...spacing["pb-3"],
    ...spacing["pt-4"],
    ...spacing["px-5"],
    backgroundColor: darkMode ? "#333333" : colors.neutral[100],
    display: "flex",
    justifyContent: "space-between",
    order: 2,
    "& button": { ...spacing["mb-1"] },
  }),
  primaryButton: { marginLeft: "auto" },
}));

const DialogActions = ({ className, buttons, buttonPrimary, buttonSecondary, darkMode }) => {
  const classes = getClasses({ darkMode });

  return (
    <Grid container className={classnames(classes.root, className)} alignItems="center">
      {(!!buttonPrimary || !!buttonSecondary) && (
        <>
          {!!buttonSecondary && (
            <Link
              to={buttonSecondary.to}
              target="_blank"
              external={buttonSecondary.external}
              onClick={buttonSecondary.onClick}
              disabled={buttonSecondary?.disabled}
            >
              {buttonSecondary.label}
            </Link>
          )}

          {!!buttonPrimary && (
            <Button
              onClick={buttonPrimary.onClick}
              className={classes.primaryButton}
              disabled={buttonPrimary.disabled}
              isLoading={buttonPrimary.isLoading}
            >
              {buttonPrimary.label}
            </Button>
          )}
        </>
      )}

      {(!buttonPrimary && !buttonSecondary) && buttons}
    </Grid>
  );
};

DialogActions.propTypes = { className: PropTypes.string };

DialogActions.defaultProps = { className: "" };

export default DialogActions;
