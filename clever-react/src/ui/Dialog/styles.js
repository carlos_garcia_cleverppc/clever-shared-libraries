import makeStyles from "../styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing, colors }, breakpoints }) => ({
  root: {
    ...spacing["py-0"],
    borderRadius: "8px",
    // maxHeight: "unset",
  },
  stripe: {
    height: "4px",
    width: "100%",
    backgroundImage: `linear-gradient(90deg, ${colors.primary[500]} 33.33%, ${colors.warning[500]} 33.33%, ${colors.warning[500]} 66.66%, ${colors.secondary[500]} 66.66%)`,
    backgroundSize: "100% 100%",
    backgroundRepeat: "no-repeat",
    margin: "0px",
  },
  container: ({ darkMode }) => ({
    overflowY: "auto",
    ...spacing["pt-7"],
    ...spacing["pb-6"],
    ...(darkMode && { background: "#424242", color: colors.neutral[300] }),
    [breakpoints.down("sm")]: {
      ...spacing["pb-6"],
      maxHeight: "55vh",
      overflow: "auto",
    },
    "&::-webkit-scrollbar": {
      width: "12px",
      backgroundColor: "#fff",
    },
    "&::-webkit-scrollbar-track": { backgroundColor: "#fff" },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: colors.neutral[300],
      borderRadius: "16px",
      border: "4px solid #fff",
    },
  }),
  content: {
    ...spacing["m-0"],
    ...spacing["mt-5"],
    width: "100%",
  },
  closeIconContainer: {
    ...spacing["mt-5"],
    ...spacing["ml-6"],
    ...spacing["mb-3"],
    position: "absolute",
  },
  iconButton: ({ darkMode }) => ({
    ...(darkMode && { backgroundColor: "#333333 !important" }),
    "& path": { fill: `${darkMode ? colors.neutral[300] : colors.neutral[900]} !important` },
    zIndex: "1",
    "&:hover": {
      backgroundColor: `${darkMode ? "#333333" : colors.neutral[300]} !important`,
    },
  }),
  image: { ...spacing["w-auto"], maxHeight: "200px" },
  imageContainer: { textAlign: "center" },
  drawerActions: ({ darkMode }) => ({
    ...spacing["pb-4"],
    ...spacing["pt-5"],
    ...spacing["px-6"],
    backgroundColor: darkMode ? "#333333" : colors.neutral[100],
    "& button": { ...spacing["mb-3"] },
  }),
  fullWidthDialog: { ...spacing["mx-8"] },
  primaryButton: { marginLeft: "auto" },
  secondaryButton: { ...spacing["mb-5"] },
}));

export default getClasses;
