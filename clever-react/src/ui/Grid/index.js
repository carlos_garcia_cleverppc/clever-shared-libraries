/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable object-property-newline */
import React from "react";
import PropTypes from "prop-types";

import MaterialGrid from "@material-ui/core/Grid";

const Grid = props => {
  const { container, item, align, alignItems } = props;
  const { justify, spacing, direction } = props;
  const { xs, sm, md, lg, xl } = props;
  const { children, className, style } = props;

  const auxProps = {
    container, item, align, alignItems, className,
    direction, justifyContent: justify, xs, sm, md, lg, xl, style,
  };

  if (container && typeof spacing === "number") {
    auxProps.spacing = spacing;
  } else if (container && spacing !== undefined && spacing) {
    auxProps.spacing = 2;
  } else {
    auxProps.spacing = undefined;
  }

  return (
    <MaterialGrid {...auxProps}>
      {children}
    </MaterialGrid>
  );
};

Grid.propTypes = {
  spacing: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.oneOf([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]),
  ]),
};

Grid.defaultProps = { spacing: false };

export default Grid;
