/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import RoundedSelect from "./index";

export default {
  title: "RoundedSelect",
  component: RoundedSelect,
};

const Template = args => {
  const [selected, setSelected] = React.useState("");
  const options = [{ label: "Today", value: 1 }, { label: "Last 7 days", value: 7 }];

  const handleChange = event => {
    setSelected(event.target.value);
  };

  return <RoundedSelect {...args} value={selected} options={options} onChange={handleChange} />;
};

export const SimpleRoundedSelect = Template.bind({});
SimpleRoundedSelect.args = { name: "Rounded select" };
