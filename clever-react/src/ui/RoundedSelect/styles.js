import makeStyles from "../styles/makeStyles";

const styles = (({ cleverUI: { spacing, colors, typography, shadows } }) => {
  const { sizing } = typography;

  return ({
    rounded: {
      borderRadius: "16px",
      backgroundColor: "white",
      borderWidth: "1px",
      borderStyle: "solid",
      borderColor: colors.neutral[100],
      height: "32px",
      "& > div:first-child": { ...spacing["pl-4"] },
    },
    root: {
      height: "inherit",
      padding: "6px 0 7px",
      minWidth: "150px",
      fontFamily: typography.font2,
      fontSize: sizing.body0,
      color: colors.neutral[500],
      "& p": { fontWeight: "bold" },
      "&:focus": { backgroundColor: "transparent" },
    },
    scroll: {
      "&::-webkit-scrollbar": {
        width: "12px",
        backgroundColor: "#fff",
      },
      "&::-webkit-scrollbar-track": { backgroundColor: "#fff" },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#babac0",
        borderRadius: "16px",
        border: "4px solid #fff",
      },
    },
    menu: {
      ...spacing["py-3"],
      ...spacing["px-4"],
      backgroundColor: "white",
      borderRadius: "8px",
      minWidth: "130px !important",
      marginTop: "50px",
      boxShadow: shadows.normal,
      overflow: "unset",
      "&:after": {
        content: "''",
        position: "absolute",
        top: "-28px",
        right: "8px",
        border: "solid 14px transparent",
        borderTopColor: "white",
        transform: "rotate(180deg)",
      },
    },
    selectIcon: {
      ...spacing["mr-2"],
      height: "24px",
      "& path": { fill: `${colors.neutral[900]} !important` },
    },
    iconOpen: { transform: "rotate(90deg)" },
    menuItem: {
      ...spacing["py-1"],
      fontFamily: typography.font2,
      fontSize: sizing.body0,
      color: colors.neutral[600],
      backgroundColor: "transparent",
    },
    menuItemRoot: ({ textAlignRight }) => ({
      justifyContent: textAlignRight ? "flex-end" : "flex-start",
      "&:hover": { backgroundColor: "transparent" },
    }),
    menuItemSelected: {
      fontWeight: "bold",
      backgroundColor: colors.neutral[200],
      borderRadius: "9px",
    },
    menuItemRootPlaceHolder: { display: "none" },
    tooltip: { display: "flex", marginLeft: "auto" },
    tooltipContainer: { maxWidth: "200px", margin: "auto" },
    icon: { ...spacing["mr-2"] },
  });
});

export default makeStyles(styles);
