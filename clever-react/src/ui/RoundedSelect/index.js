import React from "react";
import PropTypes from "prop-types";
import map from "lodash-es/map";
import isArray from "lodash-es/isArray";
import classnames from "classnames";

import MenuItem from "@material-ui/core/MenuItem";

import Icon from "../Icon";
import ChevronRight from "../Icon/ChevronRight";

import TextField from "../TextField";
import Tooltip from "../Tooltip";

import getClasses from "./styles";
import Typography from "../Typography";

// eslint-disable-next-line no-unused-vars
function printMenuItemDefault(label, value, disabled, index) {
  return label;
}

const RoundedSelect = props => {
  const { className, value, onChange, id, label, textAlignRight } = props;
  const { name, options, printMenuItem, disabled, placeholder } = props;

  const classes = getClasses({ textAlignRight });

  const getItem = (option, index) => (
    <MenuItem
      classes={{ root: classes.menuItemRoot, selected: classes.menuItemSelected }}
      className={classes.menuItem}
      key={`${name}-${option.label}`}
      value={option.value}
      disabled={option.disabled || false}
    >
      {option.icon && <Icon icon={option.icon} className={classes.icon} color={option.iconColor ? option.iconColor : "neutral"} />}

      {printMenuItem(option.label, option.value, option.disabled || false, index)}
    </MenuItem>
  );

  const getTooltipItem = (option, index) => (
    <Tooltip
      title={(
        <Typography variant="body0" color="white" className={classes.tooltipContainer}>
          {option.disabledMessage}
        </Typography>
      )}
      key={`rounded-select-item-${index}`}
      type="simple"
      className={classes.tooltip}
    >
      {getItem(option, index)}
    </Tooltip>
  );

  return (
    <TextField
      name={name}
      value={!value ? placeholder : value}
      onChange={onChange}
      SelectProps={{
        classes: { icon: classes.selectIcon, root: classes.root, iconOpen: classes.iconOpen },
        IconComponent: ChevronRight,
        MenuProps: {
          classes: { paper: classnames(classes.scroll, classes.menu) },
          getContentAnchorEl: null,
          anchorOrigin: { vertical: "top", horizontal: "right" },
          transformOrigin: { vertical: "top", horizontal: "right" },
        },
      }}
      className={classnames(classes.rounded, className)}
      disabled={disabled}
      variant="standard"
      select
      id={id}
      label={label}
    >
      {!!placeholder && !value && (
        <MenuItem
          classes={{ root: classes.menuItemRootPlaceHolder }}
          value={placeholder}
        >
          {printMenuItem(placeholder)}
        </MenuItem>
      )}

      {isArray(options) && map(options, (option, index) => {
        if (option.disabled && !!option.disabledMessage) return getTooltipItem(option, index);

        return getItem(option, index);
      })}
    </TextField>
  );
};

RoundedSelect.propTypes = {
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
  options: PropTypes.instanceOf(Array).isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  printMenuItem: PropTypes.func,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  label: PropTypes.string,
  textAlignRight: PropTypes.bool,
};

RoundedSelect.defaultProps = {
  className: "",
  printMenuItem: printMenuItemDefault,
  disabled: false,
  id: "",
  label: "",
  textAlignRight: true,
};

export default RoundedSelect;
