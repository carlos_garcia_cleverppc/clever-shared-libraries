import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import MaterialRadio from "@material-ui/core/Radio";
import CircleIcon from "@material-ui/icons/RadioButtonUnchecked";
import CheckCircleIcon from "@material-ui/icons/RadioButtonChecked";

import { onKeyDown } from "../../utils/helpers";

import getClasses from "./styles";

const Radio = props => {
  const { checked, onChange, value, className } = props;
  const { size, disabled, children, icon, checkedIcon } = props;
  const { color } = props;

  const classes = getClasses({ size });

  return (
    <div
      className={classnames(classes.root, className)}
      onClick={() => onChange({ target: { value } })}
      onKeyDown={onKeyDown(() => onChange({ target: { value } }))}
      role="button"
      tabIndex={0}
    >
      <MaterialRadio
        checked={checked}
        value={value}
        disabled={disabled}
        icon={icon}
        classes={{ root: classes.radio }}
        checkedIcon={checkedIcon}
        color={color}
      />

      <div>
        {children}
      </div>
    </div>
  );
};

Radio.propTypes = {
  className: PropTypes.string,
  checked: PropTypes.bool.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  color: PropTypes.oneOf(["primary", "secondary"]),
  disabled: PropTypes.bool,
  size: PropTypes.number,
  icon: PropTypes.instanceOf(Object),
  checkedIcon: PropTypes.instanceOf(Object),
};

Radio.defaultProps = {
  className: "",
  onChange: () => { },
  color: "primary",
  disabled: false,
  size: 20,
  icon: <CircleIcon />,
  checkedIcon: <CheckCircleIcon />,
};

export default Radio;
