/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import CheckCircleIcon from "@material-ui/icons/RadioButtonChecked";
import CircleIcon from "@material-ui/icons/RadioButtonUnchecked";
import Radio from "./index";

export default {
  title: "Radio",
  component: Radio,
};

const Template = args => {
  const [value, setValue] = React.useState(true);

  return <Radio {...args} checked={value} onChange={() => setValue(!value)} />;
};

export const SimpleRadio = Template.bind({});
SimpleRadio.args = { value: "simpleRadio", children: "simpleRadio", checkedIcon: <CheckCircleIcon />, icon: <CircleIcon /> };
