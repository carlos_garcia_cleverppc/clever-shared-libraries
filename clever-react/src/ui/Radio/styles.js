import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { spacing } }) => ({
  root: {
    display: "inline-flex",
    "align-items": "center",
    outline: "none",
    cursor: "pointer",
  },
  radio: ({ size }) => ({
    ...spacing["p-2"],
    "& span svg": {
      width: `${size}px`,
      height: `${size}px`,
    },
  }),
});

export default makeStyles(styles);
