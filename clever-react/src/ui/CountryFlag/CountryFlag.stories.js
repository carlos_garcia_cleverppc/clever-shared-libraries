/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import CountryFlag from "./index";

export default {
  title: "CountryFlag",
  component: CountryFlag,
};

const Template = args => <CountryFlag {...args} />;

export const SimpleCountryFlag = Template.bind({});
SimpleCountryFlag.args = { countryCode: "ES" };
