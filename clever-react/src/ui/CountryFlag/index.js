import React from "react";

import ReactCountryFlag from "react-country-flag";
import PropTypes from "prop-types";

const CountryFlag = props => {
  const { countryCode, className, svg } = props;

  return (
    <ReactCountryFlag
      countryCode={countryCode}
      className={className}
      svg={svg}
    />
  );
};

CountryFlag.propTypes = {
  className: PropTypes.string,
  countryCode: PropTypes.string,
  svg: PropTypes.bool,
};

CountryFlag.defaultProps = {
  className: "",
  countryCode: "",
  svg: false,
};

export default CountryFlag;
