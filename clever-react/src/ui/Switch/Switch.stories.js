/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import { getColorsLabels } from "../styles/colors";

import Switch from "./index";

const colorsList = getColorsLabels();
colorsList.push("");

export default {
  title: "Switch",
  component: Switch,
  argTypes: {
    color: {
      options: colorsList,
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => {
  const [value, setValue] = React.useState(true);

  return <Switch {...args} checked={value} onChange={() => setValue(!value)} />;
};

export const SimpleSwitch = Template.bind({});
SimpleSwitch.args = {};

export const CustomLoadingSwitch = Template.bind({});
CustomLoadingSwitch.args = { spinner: "/assets/img/switch_spinner.gif", loading: true };

export const LoadingSwitch = Template.bind({});
LoadingSwitch.args = { loading: true };
