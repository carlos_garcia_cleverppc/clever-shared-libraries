import React from "react";
import PropTypes from "prop-types";

import SwitchMaterial from "@material-ui/core/Switch";

import { getColorsLabels } from "../styles/colors";
import Loading from "../Loading";

import getClasses from "./styles";

const colorsList = getColorsLabels();
colorsList.push("");

const Switch = props => {
  const { checked, onChange, disabled, name, className, loading, spinner, color, size } = props;

  const classes = getClasses({ color, size });

  if (loading) {
    return (
      <div className={classes.loadingContainer}>
        {spinner
          ? <img src={spinner} alt="spinner" className={classes.loadingSpinner} />
          : <Loading size={13} color="neutral" center />}
      </div>
    );
  }

  return (
    <SwitchMaterial
      checked={checked}
      onChange={onChange}
      className={className}
      name={name}
      disabled={disabled}
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
    />
  );
};

Switch.propTypes = {
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  checked: PropTypes.bool,
  className: PropTypes.string,
  name: PropTypes.string,
  spinner: PropTypes.string,
  color: PropTypes.oneOf(colorsList),
  size: PropTypes.oneOf(["small", "standard"]),
};

Switch.defaultProps = {
  checked: true,
  onChange: () => { },
  disabled: false,
  name: "",
  loading: false,
  color: "",
  size: "standard",
};

export default Switch;
