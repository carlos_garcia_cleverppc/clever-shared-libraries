import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  root: ({ size }) => ({
    width: size === "standard" ? 46 : 28,
    height: size === "standard" ? 26 : 16,
    padding: 0,
    ...spacing["m-1"],
  }),
  switchBase: ({ size }) => ({
    top: size === "standard" ? 1 : 0,
    left: "2px",
    padding: 0,
    paddingTop: "1px",
  }),
  thumb: ({ size }) => ({
    width: size === "standard" ? 22 : 14,
    height: size === "standard" ? 22 : 14,
    color: "white",
  }),
  track: {
    borderRadius: 26 / 2,
    backgroundColor: `${colors.neutral[500]}`,
    opacity: 0.5,
  },
  checked: ({ color, size }) => ({
    transform: size === "standard" ? "translateX(19px) !important" : "translateX(11px) !important",
    "& + .MuiSwitch-track": {
      backgroundColor: `${!color ? "#52d869" : colors[color][500]} !important`,
      opacity: `${1} !important`,
      border: "none",
    },
  }),
  loadingContainer: ({ size }) => ({
    width: size === "standard" ? 46 : 28,
    height: size === "standard" ? 26 : 16,
    backgroundColor: colors.neutral[300],
    borderRadius: 26 / 2,
    ...spacing["m-1"],
    display: "flex",
  }),
  loadingSpinner: ({ size }) => ({
    width: size === "standard" ? 20 : 12,
    height: size === "standard" ? 20 : 12,
    marginLeft: size === "standard" ? 12 : 8,
    alignSelf: "center",
  }),
});

export default makeStyles(styles);
