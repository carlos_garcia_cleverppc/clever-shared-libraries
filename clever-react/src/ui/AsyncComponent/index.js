/* eslint-disable react/jsx-props-no-spreading */
import React, { Suspense } from "react";

import Loading from "../Loading";

function asyncComponent(Component, loader = <Loading center />) {
  return props => (
    <Suspense fallback={loader}>
      <Component {...props} />
    </Suspense>
  );
}

export default asyncComponent;
