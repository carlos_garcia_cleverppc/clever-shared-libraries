import React from "react";
import PropTypes from "prop-types";

import InputAdornment from "@material-ui/core/InputAdornment";

function CleverInputAdornment(props) {
  const {
    className,
    children,
    position,
  } = props;

  return (
    <InputAdornment
      className={className}
      position={position}
    >
      {children}
    </InputAdornment>
  );
}

CleverInputAdornment.propTypes = {
  className: PropTypes.string,
  position: PropTypes.string,
};

CleverInputAdornment.defaultProps = {
  className: "",
  position: "start",
};

export default CleverInputAdornment;
