import React, { useState } from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import Modal from "@material-ui/core/Modal";

import ShowMore from "../Icon/ShowMore";
import IconButton from "../IconButton";
import Button from "../Button";
import Popper from "../Popper";
import MenuItem from "../Menu/Item";
import TextField from "../TextField";
import Typography from "../Typography";

import getClasses from "./styles";

const ModalInput = props => {
  const classes = getClasses();

  const { open, onSubmit, onClose } = props;
  const { title, icon } = props;
  const { defaultValue, placeholder, maxLength } = props;
  const { menuOptions, footer, disablePortal, className } = props;
  const { buttonText, buttonDisabled, inputProps } = props;

  const [inputValue, setInputValue] = useState(defaultValue);
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClose = () => {
    setAnchorEl(null);
    onClose();
  };

  const isButtonDisabled = () => {
    if (inputProps?.value) return inputProps.value === "" || buttonDisabled;
    return inputValue === "" || buttonDisabled;
  };

  return (
    <Modal
      open={open}
      onClose={handleClose}
      disablePortal={disablePortal}
      className={classes.modal}
    >
      <div className={cx(classes.paper, className)}>
        {menuOptions && (
          <>
            <IconButton
              onClick={ev => setAnchorEl(ev.currentTarget)}
              icon={<ShowMore />}
              iconSize={18}
              shade={400}
              className={classes.optionsIcon}
            />

            <Popper
              className={classes.menu}
              arrow
              placement="bottom-end"
              open={Boolean(anchorEl)}
              handleClose={() => setAnchorEl(null)}
              anchorEl={anchorEl}
              transition
              color="white"
              disablePortal={disablePortal}
            >
              <ul className={classes.menuList}>
                {menuOptions.map(option => (
                  <MenuItem onClick={option.onClick} key={`modal-input-option-${option.label}`}>
                    {option.icon}

                    <Typography color={option.color} variant="f2-16">
                      {option.label}
                    </Typography>
                  </MenuItem>
                ))}
              </ul>
            </Popper>
          </>
        )}

        {(title || icon) && (
          <div className={classes.title}>
            {icon}

            {title && (
              <Typography variant="f2-16" weight="bold" shade={700}>
                {title}
              </Typography>
            )}
          </div>
        )}

        <form
          onSubmit={ev => {
            ev.preventDefault();
            onSubmit(inputValue);
          }}
          className={classes.inputContainer}
        >
          <TextField
            variant="standard"
            value={inputValue}
            onChange={ev => setInputValue(ev.target.value)}
            placeholder={placeholder}
            autoFocus
            inputProps={maxLength ? { maxLength } : undefined}
            inputClass={classes.textField}
            {...inputProps}
          />

          {buttonText && (
            <Button
              type="submit"
              size="small"
              disabled={isButtonDisabled()}
              className={classes.submitButton}
            >
              {buttonText}
            </Button>
          )}
        </form>

        {footer && (
          <div className={classes.footer}>
            {footer}
          </div>
        )}
      </div>
    </Modal>
  );
};

ModalInput.propTypes = {
  open: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  icon: PropTypes.element,
  placeholder: PropTypes.string,
  defaultValue: PropTypes.string,
  maxLength: PropTypes.number,
  buttonText: PropTypes.string,
  buttonDisabled: PropTypes.bool,
  menuOptions: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    color: PropTypes.string,
  })),
  footer: PropTypes.element,
  disablePortal: PropTypes.bool,
  className: PropTypes.string,
};

ModalInput.defaultProps = {
  placeholder: "",
  defaultValue: "",
  disablePortal: false,
  className: "",
  buttonDisabled: false,
};

export default ModalInput;
