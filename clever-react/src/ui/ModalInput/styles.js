import makeStyles from "../styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing, colors } }) => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    zIndex: "900 !important",
  },
  paper: {
    ...spacing["p-6"],
    boxSizing: "border-box",
    minWidth: "min(400px, 90%)",
    maxWidth: "90%",
    position: "relative",
    backgroundColor: colors.white[500],
    borderRadius: "8px",
  },
  optionsIcon: {
    position: "absolute",
    right: 16,
    top: 8,
  },
  menu: {
    borderRadius: "8px",
    "& svg": { ...spacing["mr-3"] },
  },
  menuList: {
    margin: "0",
    padding: "8px",
    listStyle: "none",
    maxHeight: 150,
    overflowY: "auto",

    "& li": { borderRadius: "4px" },
    "&::-webkit-scrollbar": {
      width: "12px",
      backgroundColor: "#fff",
      borderRadius: "8px",
    },
    "&::-webkit-scrollbar-track": {
      backgroundColor: "#fff",
      borderRadius: "8px",
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: colors.neutral[300],
      borderRadius: "16px",
      border: "4px solid #fff",
    },
  },
  title: {
    display: "flex",
    alignItems: "center",
    "& *:first-child": { ...spacing["mr-2"] },
  },
  inputContainer: { ...spacing["mb-4"] },
  textField: {
    ...spacing["px-1"],
    ...spacing["mt-5"],
    ...spacing["mb-2"],
    fontSize: "16px !important",
    "&::placeholder": {
      fontWeight: "normal",
      fontSize: "16px",
      color: colors.neutral[400],
    },
  },
  submitButton: {
    position: "absolute",
    bottom: -50,
    right: 0,
  },
  footer: { width: "100%" },
}));

export default getClasses;
