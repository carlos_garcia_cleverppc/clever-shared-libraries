import React from "react";
import PropTypes from "prop-types";

import HiddenMaterial from "@material-ui/core/Hidden";

const Hidden = props => {
  const {
    only,
    xsUp,
    smUp,
    mdUp,
    lgUp,
    xlUp,
    xsDown,
    smDown,
    mdDown,
    lgDown,
    xlDown,
    children,
  } = props;

  if (only) {
    return (
      <HiddenMaterial
        only={only}
        xsUp={xsUp}
        smUp={smUp}
        mdUp={mdUp}
        lgUp={lgUp}
        xlUp={xlUp}
        xsDown={xsDown}
        smDown={smDown}
        mdDown={mdDown}
        lgDown={lgDown}
        xlDown={xlDown}
      >
        {children}
      </HiddenMaterial>
    );
  }
  return (
    <HiddenMaterial
      xsUp={xsUp}
      smUp={smUp}
      mdUp={mdUp}
      lgUp={lgUp}
      xlUp={xlUp}
      xsDown={xsDown}
      smDown={smDown}
      mdDown={mdDown}
      lgDown={lgDown}
      xlDown={xlDown}
    >
      {children}
    </HiddenMaterial>
  );
};

Hidden.propTypes = {
  only: PropTypes.oneOfType([PropTypes.bool, PropTypes.oneOf(["xs", "sm", "md", "lg", "xl"]), PropTypes.arrayOf(PropTypes.oneOf(["xs", "sm", "md", "lg", "xl"]))]),
  xsUp: PropTypes.bool,
  smUp: PropTypes.bool,
  mdUp: PropTypes.bool,
  lgUp: PropTypes.bool,
  xlUp: PropTypes.bool,
  xsDown: PropTypes.bool,
  smDown: PropTypes.bool,
  mdDown: PropTypes.bool,
  lgDown: PropTypes.bool,
  xlDown: PropTypes.bool,
};

Hidden.defaultProps = {
  only: false,
  xsUp: false,
  smUp: false,
  mdUp: false,
  lgUp: false,
  xlUp: false,
  xsDown: false,
  smDown: false,
  mdDown: false,
  lgDown: false,
  xlDown: false,
};

export default Hidden;
