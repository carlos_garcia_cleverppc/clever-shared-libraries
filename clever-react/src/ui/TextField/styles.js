import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { typography: { sizing }, colors } }) => ({
  warning: {
    "& .MuiInputLabel-root": {
      color: `${colors.warning[500]}!important`,
    },
    "& .MuiOutlinedInput-notchedOutline": {
      borderColor: `${colors.warning[500]}!important`,
    },
  },
  input: {
    fontSize: sizing.body1,
    "&[type=\"search\" i]::-webkit-search-cancel-button": {
      /* Remove default */
      appearance: "none",

      /* Now our custom style */
      height: "20px",
      width: "20px",
      backgroundImage: "url(assets/img/times.svg)",
      backgroundRepeat: "no-repeat",
      transform: "translateX(9px) translateY(3px)",
    },
  },
  smallText: {
    fontSize: sizing.body0,
  },
});

export default makeStyles(styles);
