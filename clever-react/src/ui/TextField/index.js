/* eslint-disable max-len */
/* eslint-disable no-useless-escape */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import toLower from "lodash-es/toLower";

import TextField from "@material-ui/core/TextField";
import InputAdornment from "../InputAdornment";

import getClasses from "./styles";

const CleverTextField = props => {
  const {
    className, InputProps, size, onChange, warning, error, value,
    inputClass, checkErrorType, hasErrorType, startAdornment, ...textFieldProps
  } = props;

  const classes = getClasses();

  const [errorType, setErrorType] = useState(false);

  const urlRegExp = /^((http|https):\/{2})?(([0-9a-zA-Z_-]+\.)+[a-zA-Z]+(\/([~0-9a-zA-Z#+%@./_-]+))?[/]?)(\?((.*(=.*)?)(&?)))*$/;
  const blackList = /^(?:(?!\.youtube\.|\.facebook\.|\.instagram\.|\.google\.|\.mackeeper\.|\.youtu\.be|\.test\.|\.blogspot\.|\.blogger\.|\.twitter\.|\.wordpress\.|\.teespring\.|\.amazon\.).)*$/;
  const emailRegexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const headlinesRegEx = /[-!$%^&*()_+|~=\`{}\[\]:";'<>?\/]/;
  const descriptionsRegEx = /[-!$%^&*()_+|~=\`{}\[\]:"'<>?\/]/;
  const dots = /.+(\w+(\.|,))\S+/;

  const handleCheckErrorType = newValue => {
    let valid = true;
    switch (checkErrorType) {
      case "url":
        valid = urlRegExp.test(newValue);
        break;
      case "url_blacklist":
        valid = urlRegExp.test(newValue) && blackList.test(toLower(newValue));
        break;
      case "email":
        valid = emailRegexp.test(newValue);
        break;
      case "headline":
        valid = newValue.length <= 30
          && !headlinesRegEx.test(newValue)
          && !urlRegExp.test(newValue)
          && !dots.test(newValue);
        break;
      case "description":
        valid = newValue.length <= 90
          && !descriptionsRegEx.test(newValue)
          && !urlRegExp.test(newValue)
          && !dots.test(newValue);
        break;
      default:
        break;
    }
    hasErrorType(!valid);
    setErrorType(!valid);
  };

  useEffect(() => {
    if (checkErrorType !== "") handleCheckErrorType(value);
  }, []);

  const handleOnChange = ev => {
    onChange(ev);
    if (checkErrorType !== "") {
      handleCheckErrorType(ev.target.value);
    }
  };

  const getAdornment = () => {
    if (startAdornment) {
      return (
        <InputAdornment position="start">
          {startAdornment}
        </InputAdornment>
      );
    }
    return InputProps?.startAdornment;
  };

  return (
    <TextField
      {...textFieldProps}
      className={classnames({ [classes.warning]: warning }, className)}
      InputProps={{
        ...InputProps,
        classes: {
          input: classnames(
            classes.input,
            { [classes.smallText]: size === "small" },
            inputClass,
          ),
        },
        startAdornment: getAdornment(),
      }}
      onChange={handleOnChange}
      color="secondary"
      error={error || errorType}
      size={size === "standard" ? "medium" : size}
      value={value}
    />
  );
};

CleverTextField.propTypes = {
  className: PropTypes.string,
  variant: PropTypes.oneOf(["standard", "outlined"]),
  select: PropTypes.bool,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.bool]),
  disabled: PropTypes.bool,
  type: PropTypes.string,
  error: PropTypes.bool,
  warning: PropTypes.bool,
  fullWidth: PropTypes.bool,
  multiline: PropTypes.bool,
  rows: PropTypes.number,
  inputClass: PropTypes.string,
  size: PropTypes.oneOf(["small", "standard"]),
  checkErrorType: PropTypes.oneOf(["", "url", "url_blacklist", "email", "headline", "description"]),
  hasErrorType: PropTypes.func,
  startAdornment: PropTypes.node,
};

CleverTextField.defaultProps = {
  variant: "outlined",
  className: "",
  select: false,
  label: "",
  placeholder: "",
  onChange: () => { },
  disabled: false,
  type: "text",
  error: false,
  warning: false,
  fullWidth: true,
  value: "",
  multiline: false,
  rows: 3,
  inputClass: "",
  size: "standard",
  checkErrorType: "",
  hasErrorType: () => { },
};

export default CleverTextField;
