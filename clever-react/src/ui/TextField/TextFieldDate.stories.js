/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import TextFieldDate from "./index";

export default {
  title: "TextFieldDate",
  component: TextFieldDate,
};

const Template = args => {
  const [date, setDate] = React.useState("");

  const handleChange = event => {
    setDate(event.target.value);
  };

  return (<TextFieldDate {...args} onChange={handleChange} value={date} />);
};

export const SimpleTextFieldDate = Template.bind({});
SimpleTextFieldDate.args = {
  label: "End Date",
  defaultValue: "2017-05-24",
  type: "date",
  InputLabelProps: { shrink: true },
};
