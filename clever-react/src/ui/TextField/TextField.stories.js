import React from "react";

import TextField from "./index";
import IconSearch from "../Icon/Search";
import InputAdornment from "../InputAdornment";

export default {
  title: "TextField",
  component: TextField,
};

const Template = args => {
  const [value, setValue] = React.useState("TextField");

  const handleChange = event => {
    setValue(event.target.value);
  };

  return (
    <TextField {...args} onChange={handleChange} value={value} />
  );
};

export const SimpleTextField = Template.bind({});
SimpleTextField.args = {};

export const LabelTextField = Template.bind({});
LabelTextField.args = { label: "*Shop name" };

const TemplateExp = args => {
  const [value, setValue] = React.useState("email@email.com");

  const handleChange = event => {
    setValue(event.target.value);
  };

  const [, setErrorType] = React.useState(false);

  return (<TextField {...args} onChange={handleChange} value={value} checkErrorType="email" hasErrorType={err => setErrorType(err)} />);
};

export const RegExpTextField = TemplateExp.bind({});
RegExpTextField.args = { label: "E-mail" };

const TemplateEmpty = args => {
  const [value, setValue] = React.useState("");

  const handleChange = event => {
    setValue(event.target.value);
  };

  return (
    <TextField {...args} onChange={handleChange} value={value} />
  );
};

export const AdornmentTextField = TemplateEmpty.bind({});
AdornmentTextField.args = {
  startAdornment: <IconSearch size={18} shade={400} />,
  type: "search",
  name: "product-search",
  placeholder: "Search products ...",
};
