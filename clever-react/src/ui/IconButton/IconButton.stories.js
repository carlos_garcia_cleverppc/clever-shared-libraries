/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import { getColorsLabels } from "../styles/colors";

import IconButton from "./index";
import Ruler from "../Icon/Ruler";

export default {
  title: "IconButton",
  component: IconButton,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => <IconButton {...args} />;

export const SimpleIconButton = Template.bind({});
SimpleIconButton.args = { icon: <Ruler /> };

export const ActiveIconButton = Template.bind({});
ActiveIconButton.args = { icon: <Ruler />, active: true, color: "green" };

export const DisabledIconButton = Template.bind({});
DisabledIconButton.args = { icon: <Ruler />, disabled: true };

export const OutlinedIconButton = Template.bind({});
OutlinedIconButton.args = { icon: <Ruler />, outlined: false };

export const HoverSquareBigIconButton = Template.bind({});
HoverSquareBigIconButton.args = { icon: <Ruler />, hover: "square", outlined: false, iconSize: 96 };

export const HoverZoomIconButton = Template.bind({});
HoverZoomIconButton.args = { icon: <Ruler />, hover: "zoomIn" };
