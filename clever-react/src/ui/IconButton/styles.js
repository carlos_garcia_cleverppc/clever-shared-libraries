import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  outlined: ({ active, color, shade }) => ({
    backgroundColor: "transparent",
    "& path": { fill: `${active ? colors[color][shade] : colors.neutral[shade]} !important` },
    "&:hover": { "& path": { fill: `${colors[color][shade]} !important` }, boxShadow: "none", backgroundColor: "transparent" },
    "&:focus": { "& path": { fill: `${colors[color][shade]} !important` }, boxShadow: "none", backgroundColor: "transparent" },
  }),
  outlinedDisabled: {
    backgroundColor: "transparent",
    "& path": { fill: `${colors.neutral[300]} !important` },
    "&:hover": { "& path": { fill: `${colors.neutral[300]} !important` }, boxShadow: "none", backgroundColor: "transparent" },
    "&:focus": { "& path": { fill: `${colors.neutral[300]} !important` }, boxShadow: "none", backgroundColor: "transparent" },
  },
  notOutlined: ({ color, shade }) => ({
    backgroundColor: `${colors[color][shade]} !important`,
    "& path": { fill: "white" },
    "&:hover": { backgroundColor: `${colors[color][700]} !important`, boxShadow: "none" },
    "&:focus": { backgroundColor: `${colors[color][700]} !important`, boxShadow: "none" },
  }),
  notOutlinedDisabled: ({ color }) => ({
    backgroundColor: `${colors[color][100]} !important`,
    "& path": { fill: "white" },
    "&:hover": { backgroundColor: `${colors[color][100]} !important`, boxShadow: "none" },
    "&:focus": { backgroundColor: `${colors[color][100]} !important`, boxShadow: "none" },
  }),
  zoomIn: {
    transition: "all .3s ease-in-out",
    "&:hover": { boxShadow: "none", transform: "scale(1.25)" },
  },
  squareOutlined: {
    borderRadius: "8px",
  },
  size: ({ iconSize }) => ({
    "& svg": {
      width: iconSize,
      height: iconSize,
    },
  }),
  sizeSmall: {
    ...spacing["p-1"],
    "& svg": {
      width: "0.5rem",
      height: "0.5rem",
    },
  },
  sizeMedium: {
    ...spacing["p-2"],
    "& svg": {
      width: "1.3rem",
      height: "1.3rem",
    },
  },
});

export default makeStyles(styles);
