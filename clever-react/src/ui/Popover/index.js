import React, { useState } from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import split from "lodash-es/split";

import MaterialPopover from "@material-ui/core/Popover";

import getClasses from "./styles";

const Popover = props => {
  const { children, open, id, anchorEl, arrow, arrowColor, arrowColorShade, arrowPosition, arrowSize } = props;
  const { placement, className, classNameRoot, onClose, backgroundColor, disableScrollLock, disablePortal } = props;

  const [paperDimensions, setPaperDimensions] = useState(null);

  const classes = getClasses({
    placement,
    arrow,
    backgroundColor,
    anchorEl,
    paperDimensions,
    arrowColor,
    arrowColorShade,
    arrowPosition,
    arrowSize,
  });

  const positionAnchor = split(placement, "-")[0];
  const alignAnchor = split(placement, "-")[1];

  const getInversePosition = position => {
    if (position === "top") return "bottom";
    if (position === "bottom") return "top";
    return "center";
  };

  const getInverseAlign = align => {
    if (align === "left") return "right";
    if (align === "right") return "left";
    return "center";
  };

  if (!anchorEl) return <div />;

  return (
    <MaterialPopover
      id={id}
      open={open}
      anchorEl={anchorEl}
      onClose={onClose}
      TransitionProps={{
        onEntering: () => setPaperDimensions(document.querySelector(`#${id}`).children[2]),
      }}
      anchorOrigin={{
        vertical: positionAnchor,
        horizontal: alignAnchor,
      }}
      transformOrigin={{
        vertical: getInversePosition(positionAnchor),
        horizontal: positionAnchor === "center" ? getInverseAlign(alignAnchor) : alignAnchor,
      }}
      classes={{
        paper: classnames(anchorEl ? classes.paper : "", className),
        root: classnames(classes.root, classNameRoot),
      }}
      transitionDuration={250}
      disableScrollLock={disableScrollLock}
      disablePortal={disablePortal}
    >
      {children}
    </MaterialPopover>
  );
};

Popover.propTypes = {
  arrow: PropTypes.bool,
  placement: PropTypes.oneOf([
    "top-left",
    "top-center",
    "top-right",
    "bottom-left",
    "bottom-center",
    "bottom-right",
    "center-left",
    "center-right",
  ]),
  anchorEl: PropTypes.instanceOf(Node),
  children: PropTypes.instanceOf(Object).isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  className: PropTypes.string,
  classNameRoot: PropTypes.string,
  id: PropTypes.string,
  backgroundColor: PropTypes.string,
  arrowColor: PropTypes.string,
  arrowColorShade: PropTypes.number,
  arrowPosition: PropTypes.oneOf(["left", "center", "right"]),
  arrowSize: PropTypes.number,
  disablePortal: PropTypes.bool,
  disableScrollLock: PropTypes.bool,
};

Popover.defaultProps = {
  arrow: false,
  placement: "bottom-center",
  className: "",
  classNameRoot: "",
  id: "",
  backgroundColor: "white",
  arrowColor: "white",
  anchorEl: null,
  arrowColorShade: 500,
  arrowPosition: "center",
  arrowSize: 32,
  disablePortal: false,
  disableScrollLock: false,
};

export default Popover;
