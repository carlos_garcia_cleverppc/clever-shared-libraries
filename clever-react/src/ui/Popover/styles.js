/* eslint-disable max-len */
import split from "lodash-es/split";
import makeStyles from "../styles/makeStyles";

const getLeftRoot = (placement, anchorEl, arrowSize) => {
  const dimensions = anchorEl ? anchorEl.getBoundingClientRect() : { x: 0 };
  if (split(placement, "-")[0] === "center") return split(placement, "-")[1] === "left" ? `-${arrowSize / 2}px !important` : `${arrowSize / 2}px !important`;
  if (split(placement, "-")[1] === "left") return dimensions.x >= arrowSize ? `-${arrowSize}px !important` : `${dimensions.x}px !important`;
  if (split(placement, "-")[1] === "center") return dimensions.x >= arrowSize ? "" : `${dimensions.x}px !important`;
  if (split(placement, "-")[1] === "right") return dimensions.x >= arrowSize ? `${arrowSize}px !important` : `${dimensions.x}px !important`;
  return "";
};

const getTopSpacing = (placement, arrowSize) => {
  if (split(placement, "-")[0] === "top") return `-${arrowSize / 2}px !important`;
  if (split(placement, "-")[0] === "bottom") return `${arrowSize / 2}px !important`;
  return "";
};

const getPositionX = (placement, anchorEl, paperDimensions, arrowPosition, arrowSize) => {
  /** Function to move arrow among X axis */
  if (split(placement, "-")[0] === "center") {
    if (split(placement, "-")[1] === "right") return `-${arrowSize / 2}px`;
    if (split(placement, "-")[1] === "left") return `calc(100% + ${arrowSize / 2}px)`;
  } else {
    const dimensions = anchorEl ? anchorEl.getBoundingClientRect() : { x: 0 };
    const dimensionsPaper = paperDimensions ? paperDimensions.getBoundingClientRect() : { x: 0 };
    // Distance in X axis from the upper left corner of paper to the upper left corner of button
    const moveX = dimensions.x - dimensionsPaper.x;

    if (arrowPosition === "left") return `${moveX + 8 + (arrowSize / 2)}px`; // Plus 8 for borderRadius, 16 for the middle of the arrow
    if (arrowPosition === "right") return `${moveX + dimensions.width - 8 - (arrowSize / 2)}px`; // Minus 8 for borderRadius
    return `${moveX + dimensions.width / 2}px`;
  }
  return "";
};

const getPositionY = (placement, arrowSize) => {
  if (split(placement, "-")[0] === "center") return "50%";
  if (split(placement, "-")[0] === "top") return "100%";
  return split(placement, "-")[0] === "bottom" ? `-${arrowSize}px` : "0%";
};

function getColor(colors, color, shade) {
  switch (color) {
    case "inherit":
      return "inherit";
    case "white":
      return "white";
    default:
      return colors[color][shade];
  }
}

const styles = ({ cleverUI: { colors, shadows } }) => ({
  root: ({ placement, arrow, anchorEl, arrowSize }) => ({
    zIndex: "900 !important",
    top: !arrow ? "" : `${getTopSpacing(placement, arrowSize)}`,
    left: !arrow ? "" : `${getLeftRoot(placement, anchorEl, arrowSize)}`,
  }),
  paper: ({
    placement, arrow, backgroundColor, anchorEl, arrowSize,
    paperDimensions, arrowColor, arrowColorShade, arrowPosition,
  }) => ({
    backgroundColor: getColor(colors, backgroundColor, 500),
    boxShadow: shadows.normal,
    overflow: "unset",
    borderRadius: "8px",
    transform: "none !important",
    "&:after": {
      content: !arrow ? "" : "''",
      position: !arrow ? "" : "absolute",
      top: !arrow ? "" : `${getPositionY(placement, arrowSize)}`,
      left: !arrow ? "" : `${getPositionX(placement, anchorEl, paperDimensions, arrowPosition, arrowSize)}`,
      zIndex: !arrow ? "" : "1",
      border: !arrow ? "" : `solid ${arrowSize / 2}px transparent`,
      borderTopColor: arrow && `${split(placement, "-")[0]}` === "top" ? getColor(colors, arrowColor, arrowColorShade) : "",
      borderRightColor: arrow && `${split(placement, "-")[0]}` === "center" && `${split(placement, "-")[1]}` === "right" ? getColor(colors, arrowColor, arrowColorShade) : "",
      borderLeftColor: arrow && `${split(placement, "-")[0]}` === "center" && `${split(placement, "-")[1]}` === "left" ? getColor(colors, arrowColor, arrowColorShade) : "",
      borderBottomColor: arrow && `${split(placement, "-")[0]}` === "bottom" ? getColor(colors, arrowColor, arrowColorShade) : "",
      marginLeft: !arrow ? "" : `-${arrowSize / 2}px`,
      marginTop: arrow && `${split(placement, "-")[0]}` === "center" ? `-${arrowSize / 2}px` : "",
    },
  }),
});

export default makeStyles(styles);
