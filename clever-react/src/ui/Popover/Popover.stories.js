/* eslint-disable no-console */
import React, { useState } from "react";

import Popover from "./index";

import Typography from "../Typography";
import Button from "../Button";
import IconButton from "../IconButton";
import Grid from "../Grid";

import makeStyles from "../styles/makeStyles";

import Menu from "../Icon/Menu";

export default {
  title: "Popover",
  component: Popover,
};

const getClasses = makeStyles(() => ({ button: { top: "300px", left: "50%" } }));

const Template = args => {
  const classes = getClasses();

  function Parent({ children }) {
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const id = open ? "simple-popover" : undefined;

    const handleClick = event => {
      setAnchorEl(event.currentTarget);
    };

    return (
      <div>
        <IconButton icon={<Menu />} onClick={handleClick} className={classes.button} />

        {children(anchorEl, setAnchorEl, open, id)}
      </div>
    );
  }

  return (
    <Parent>
      {(anchorEl, setAnchorEl, open, id) => (
        <Popover
          {...args}
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={() => setAnchorEl(null)}
        />
      )}
    </Parent>
  );
};

export const SimplePopover = Template.bind({});
SimplePopover.args = {
  arrow: true,
  children: (
    <>
      <Typography variant="h2" shade={900} gutterBottom>
        Buttons and Links
      </Typography>

      <Grid container spacing>
        <Grid item xs>
          <Button color="warning" onClick={() => console.log("You click the link")}>
            Next
          </Button>
        </Grid>

        <Grid item xs>
          <Button onClick={() => console.log("You click the link")}>
            Next
          </Button>
        </Grid>
      </Grid>
    </>
  ),
  backgroundColor: "neutral",
};
