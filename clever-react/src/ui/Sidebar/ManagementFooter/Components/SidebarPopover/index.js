import React from "react";
import ReactDOM from "react-dom";
import classnames from "classnames";

import PropTypes from "prop-types";

import { getColorsLabels, getShades } from "../../../../styles/colors";

import getClasses from "./styles";

const colorsList = getColorsLabels(true);
const shadesList = getShades();

const Popover = props => {
  const { children, onClose, anchorEl, open, arrow, width, zIndex } = props;
  const { placement, arrowColor, arrowShade, className } = props;

  const classes = getClasses({ anchorEl, open, placement, paperWidth: width, arrowColor, arrowShade, zIndex });

  return open && (ReactDOM.createPortal(
    <>
      {open && (
        <div
          className={classes.portal}
          role="presentation"
          onClick={onClose}
        />
      )}

      { open && (
        <div className={arrow ? classnames(classes.popoverContainer, className.arrow ? className.arrow : "") : ""}>
          <div className={classnames(classes.popover, className.popover ? className.popover : "")}>
            {children}
          </div>
        </div>
      )}
    </>,
    document.body,
  ));
};

Popover.propTypes = {
  onClose: PropTypes.func.isRequired,
  arrow: PropTypes.bool,
  anchorEl: PropTypes.instanceOf(Object),
  open: PropTypes.bool.isRequired,
  children: PropTypes.instanceOf(Object).isRequired,
  width: PropTypes.number,
  zIndex: PropTypes.number,
  arrowColor: PropTypes.oneOf(colorsList),
  arrowShade: PropTypes.oneOf(shadesList),
  lockBodyScroll: PropTypes.bool,
  className: PropTypes.instanceOf(Object),
  placement: PropTypes.oneOf([
    "top-left",
    "top-center",
    "top-right",
    "bottom-left",
    "bottom-center",
    "bottom-right",
    "center-left",
    "center-right",
  ]),
};

Popover.defaultProps = {
  arrow: true,
  width: 375,
  placement: "bottom-center",
  arrowColor: "white",
  arrowShade: 500,
  lockBodyScroll: false,
  className: {},
  zIndex: 1000,
};

export default Popover;
