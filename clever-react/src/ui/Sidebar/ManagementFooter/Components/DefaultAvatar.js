/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import Typography from "../../../Typography";

import makeStyles from "../../../styles/makeStyles";
import { getColorsLabels } from "../../../styles/colors";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  defaultAvatar: ({ size, color }) => ({
    backgroundColor: colors[color][500],
    width: size,
    height: size,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: "50%",
    "& svg": {
      width: "62%",
      height: "62%",
    },
  }),
  root: ({ size }) => ({
    width: size,
    height: size,
    minWidth: size,
    position: "relative",
  }),
  avatarWrapper: {
    width: "100%",
    height: "100%",
    display: "grid",
    borderRadius: "50%",
    "& > *": {
      gridArea: "1/-1",
      borderRadius: "inherit",
    },
  },
  circle: ({ color }) => ({ boxShadow: `inset 0 0 0 3px ${colors[color][500]}` }),
  label: ({ color, size }) => ({
    position: "absolute",
    bottom: "-9px", // Middle of label height
    right: size / 2,
    transform: "translateX(50%)",
    whiteSpace: "nowrap",
    ...spacing["p-1"],
    backgroundColor: colors[color][100],
    borderRadius: "8px",
  }),
});

const getClasses = makeStyles(styles);
const colorsList = getColorsLabels(true);

const DefaultAvatar = props => {
  const { className, size, name, color, label, colorCircle } = props;
  const classes = getClasses({ size, color });

  return (
    <div className={classnames(classes.root, className)}>
      <div className={classes.avatarWrapper}>
        <div className={classes.defaultAvatar}>
          <Typography color="white" weight="bold" variant="h3">{name}</Typography>
        </div>

        {colorCircle && <div className={classes.circle} />}
      </div>

      {label && <Typography color={color} weight="bold" variant="f1-8" className={classes.label}>{label}</Typography>}
    </div>
  );
};

DefaultAvatar.propTypes = {
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  name: PropTypes.string,
  label: PropTypes.string,
  colorCircle: PropTypes.bool,
  color: PropTypes.oneOf(colorsList),
};

DefaultAvatar.defaultProps = {
  size: 42,
  name: "CL",
  label: "",
  colorCircle: false,
  color: "primary",
};

export default DefaultAvatar;
