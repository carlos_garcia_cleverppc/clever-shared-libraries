import React, { useEffect } from "react";
import classnames from "classnames";

import PropTypes from "prop-types";

import IconButton from "../../../IconButton";
import Popover from "../../../Popover";
import RoundedProgressBar from "../../../RoundedProgressBar";
import Typography from "../../../Typography";
import Link from "../../../Link";

import ArrowRight from "../../../Icon/ArrowRight";
import Times from "../../../Icon/Times";
import Warning from "../../../Icon/Warning";

import { getColorsLabels } from "../../../styles/colors";

import makeStyles from "../../../styles/makeStyles";

const colorsList = getColorsLabels(true);

const styles = ({ cleverUI: { spacing, colors } }) => ({
  spaceRight: { ...spacing["mr-2"] },
  avatarWrapper: { height: "42px", width: "42px" },
  opacity: { opacity: "30%" },
  popover: {
    zIndex: "1400 !important",
    width: "410px",
    height: "90px",
  },
  link: { ...spacing["py-3"], ...spacing["px-3"], display: "flex" },
  copyLink: { display: "inline-flex", alignItems: "center" },
  icon: { ...spacing["ml-2"] },
  clickable: { cursor: "pointer" },
  warning: {
    top: "4px",
    zIndex: 2,
    position: "absolute",
    backgroundColor: colors.secondary[500],
    borderRadius: "7px",
  },
  popoverTransition: { transition: "all 300ms cubic-bezier(.4,1.39,.93,1) !important" },
});

const getClasses = makeStyles(styles);

const ProgressAvatar = props => {
  const { children, onClickProgress, color, percentageDone, percentageCopy } = props;
  const classes = getClasses();

  const [open, setOpen] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [manuallyClosed, setManuallyClosed] = React.useState(false);

  useEffect(() => {
    if (open) {
      document.getElementById("popover-progress-avatar").firstChild.style.display = "none";
    }
  }, [open]);

  const openPopover = () => {
    setManuallyClosed(false);
    if (percentageDone < 100) {
      setAnchorEl(document.getElementById("progress-avatar"));
      setOpen(!!anchorEl);
    }
  };

  const closePopover = () => {
    setManuallyClosed(true);
    setAnchorEl(null);
    setOpen(false);
  };

  React.useEffect(() => {
    if (!manuallyClosed) {
      openPopover();
    }
  });

  return (
    <>
      {!open && percentageDone < 100 && <Warning color="white" className={classes.warning} size={14} />}

      <div
        onClick={openPopover}
        onKeyDown={openPopover}
        role="button"
        tabIndex={0}
        className={percentageDone < 100 ? classes.clickable : ""}
      >
        <RoundedProgressBar
          size={42}
          progress={percentageDone}
          strokeWidth={4}
          color={color}
          className={classes.spaceRight}
          id="progress-avatar"
          aria-describedby="progress-avatar"
        >
          <div className={classnames(classes.avatarWrapper, percentageDone < 100 ? classes.opacity : "")}>
            {children}
          </div>
        </RoundedProgressBar>
      </div>

      {percentageDone < 100 && (
        <Popover
          arrow
          arrowPosition="center"
          id="popover-progress-avatar"
          open={open}
          anchorEl={anchorEl}
          placement="top-center"
          classNameRoot={classes.popover}
          className={classes.popoverTransition}
          backgroundColor="secondary"
          arrowColor="secondary"
          arrowSize={8}
          disableScrollLock
          disablePortal
          onClose={closePopover}
        >
          <div className={classes.link}>
            <IconButton
              className={classes.iconTimes}
              color="white"
              icon={<Times />}
              iconSize={12}
              onClick={closePopover}
              active
            />

            <Link color="white" gutterBottom onClick={onClickProgress}>
              <div className={classes.copyLink}>
                <Typography variant="f2-12" color="white">{percentageCopy}</Typography>

                <ArrowRight color="white" size="12" className={classes.icon} />
              </div>
            </Link>
          </div>
        </Popover>
      )}
    </>
  );
};

ProgressAvatar.propTypes = {
  onClickProgress: PropTypes.func,
  color: PropTypes.oneOf(colorsList),
  percentageDone: PropTypes.number,
  percentageCopy: PropTypes.string,
};

ProgressAvatar.defaultProps = {
  onClickProgress: () => { },
  color: "secondary",
  percentageDone: 0,
  percentageCopy: "",
};

export default ProgressAvatar;
