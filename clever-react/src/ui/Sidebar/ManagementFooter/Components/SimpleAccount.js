/* eslint-disable react/no-unused-prop-types */
import React, { useState } from "react";
import PropTypes from "prop-types";

import AlertCard from "../../../AlertCard";
import BrandIcon from "../../../BrandIcon";
import IconButton from "../../../IconButton";
import Grid from "../../../Grid";
import Tooltip from "../../../Tooltip";
import Typography from "../../../Typography";

import CirclePlus from "../../../Icon/CirclePlus";
import Exit from "../../../Icon/Exit";
import Refresh from "../../../Icon/Refresh";
import Warning from "../../../Icon/Warning";

import makeStyles from "../../../styles/makeStyles";

const styles = ({ cleverUI: { spacing } }) => ({
  root: ({ cursor }) => ({
    ...spacing["p-1"],
    ...spacing["m-1"],
    display: "inline-flex",
    alignItems: "center",
    outline: "none",
    cursor: `${cursor}`,
    marginLeft: "auto",
  }),
  platformLogo: { height: "32px", width: "32px" },
  opacity: { opacity: "50%" },
  name: {
    display: "inline-flex",
    alignItems: "center",
  },
  accountName: {
    wordBreak: "break-all",
    overflow: "hidden",
    textOverflow: "ellipsis",
    display: "-webkit-box",
    "-webkit-line-clamp": 1,
    "-webkit-box-orient": "vertical",
  },
  sameLine: { display: "inline-flex" },
  alert: { ...spacing["mt-3"] },
});

const getClasses = makeStyles(styles);

const SimpleAccount = props => {
  const classes = getClasses();

  const { data } = props;
  const { name, accounts, addAccount } = data;
  const account = accounts.length === 0 ? null : accounts[0];

  const alert = account && account.alert ? account.alert : null;
  const refresh = account && account.refresh ? account.refresh : null;
  const logout = account && account.logout ? account.logout : null;

  const alertColor = alert && alert.color ? alert.color : "orange";
  const refreshColor = refresh && refresh.color ? refresh.color : "secondary";
  const logoutColor = logout && logout.color ? logout.color : "danger";
  const addAccountColor = addAccount && addAccount.color ? addAccount.color : "secondary";

  const [showAlert, setShowAlert] = useState(false);

  const getChannelIcon = () => {
    const auxName = name.toLowerCase();
    let result = "";

    if (auxName.includes("google ads")) {
      result = "GoogleAdsCircle";
    } else if (auxName.includes("google analytics")) {
      result = "GoogleAnalyticsCircle";
    } else if (auxName.includes("google merchant")) {
      result = "GoogleMerchantCircle";
    } else if (auxName.includes("facebook")) {
      result = "FacebookCircle";
    } else if (auxName.includes("twitter")) {
      result = "TwitterCircle";
    } else if (auxName.includes("microsoft")) {
      result = "MicrosoftCircle";
    } else if (auxName.includes("linkedin")) {
      result = "LinkedinCircle";
    } else if (auxName.includes("instagram")) {
      result = "InstagramCircle";
    }

    return result;
  };

  const channelIcon = getChannelIcon();

  return (
    <Grid container alignItems="center" className={classes.root}>
      <Grid item xs={2} className={classes.platformLogo}>
        <BrandIcon icon={channelIcon} size={32} className={!account ? classes.opacity : ""} />
      </Grid>

      {account && (
        <>
          <Grid item xs={10} className={classes.sameLine}>
            <Grid container alignItems="center">
              <Grid item xs={12} className={classes.name}>
                <Typography weight="bold" variant="f1-12" shade={900} className={classes.accountName}>
                  {account.name}
                </Typography>
              </Grid>

              {!!account.id && (
                <Grid item xs={12}>
                  <Typography variant="f2-10">{account.id}</Typography>
                </Grid>
              )}
            </Grid>

            {!!alert && (
              <IconButton icon={<Warning />} color={alertColor} active onClick={() => setShowAlert(!showAlert)} hover="zoomIn" />
            )}

            {!!refresh && (
              <Tooltip title={refresh.copy}>
                <IconButton icon={<Refresh />} color={refreshColor} onClick={() => refresh.onClick()} hover="zoomIn" />
              </Tooltip>
            )}

            {!!logout && (
              <Tooltip title={logout.copy}>
                <IconButton icon={<Exit />} color={logoutColor} onClick={() => logout.onClick()} hover="zoomIn" />
              </Tooltip>
            )}
          </Grid>

          {!!alert && showAlert && (
            <Grid item xs={12} className={classes.alert}>
              <AlertCard
                text={alert.copyAlert}
                color={alertColor}
                linkText={alert.copyOnClick}
                action={alert.onClick}
              />
            </Grid>
          )}
        </>
      )}

      {!account && (
        <Grid item xs={10}>
          <Grid container alignItems="center">
            <Grid item xs={10}>
              <Typography variant="body0" shade={900}>{addAccount.copy}</Typography>
            </Grid>

            <Grid item xs={2}>
              <IconButton icon={<CirclePlus />} active color={addAccountColor} hover="zoomIn" onClick={addAccount.onClick} />
            </Grid>
          </Grid>
        </Grid>
      )}
    </Grid>
  );
};

SimpleAccount.propTypes = {
  data: PropTypes.instanceOf(Object).isRequired,
};

export default SimpleAccount;
