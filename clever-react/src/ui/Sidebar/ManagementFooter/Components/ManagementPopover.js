/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable import/no-cycle */
import React from "react";
import PropTypes from "prop-types";

import AlertCard from "../../../AlertCard";
import Divider from "../../../Divider";
import Grid from "../../../Grid";
import Link from "../../../Link";
import Popover from "./SidebarPopover";

import Icon from "../../../Icon";
import Exit from "../../../Icon/Exit";
import Megaphone from "../../../Icon/Megaphone";

import MultipleAccounts from "./MultipleAccounts";
import SimpleAccount from "./SimpleAccount";

import Typography from "../../../Typography";

import makeStyles from "../../../styles/makeStyles";

const styles = ({ cleverUI: { spacing, shadows } }) => ({
  mainContainer: {
    ...spacing["p-6"],
    ...spacing["pb-4"],
    backgroundColor: "white",
    borderRadius: "8px",
    boxShadow: shadows.normal,
    maxWidth: "375px",
  },
  divider: { ...spacing["px-2"], ...spacing["pb-3"] },
  bottomSpace: { ...spacing["mb-1"] },
  icon: { ...spacing["mr-2"] },
  sameLine: { display: "inline-flex", alignItems: "center" },
  alert: { ...spacing["mb-5"] },
  signOut: {
    textAlign: "right",
    ...spacing["px-2"],
    ...spacing["my-2"],
  },
  accounts: {
    overflowY: "auto",
    overflowX: "hidden",
    maxHeight: "40vh",
    "&::-webkit-scrollbar": {
      width: "12px",
      backgroundColor: "#fff",
    },
    "&::-webkit-scrollbar-track": { backgroundColor: "#fff" },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#babac0",
      borderRadius: "16px",
      border: "4px solid #fff",
    },
  },
  accountsContainer: {
    maxWidth: "98%",
  },
});

const getClasses = makeStyles(styles);

const ManagementPopover = props => {
  const classes = getClasses();

  const { allAccounts, allowMultipleAccounts, accountsTitleCopy, actions } = props;
  const { logOutCopy, handleOnClickLogOut, color, alert } = props;
  const { anchorEl, setAnchorEl, open, setOpen, popoverClassName } = props;

  const alertColor = Object.keys(alert).length > 0 && alert.color ? alert.color : "orange";

  return (
    <Popover
      placement="top-center"
      open={open}
      anchorEl={anchorEl}
      onClose={() => { setAnchorEl(null); setOpen(false); }}
      arrow
      arrowColor="white"
      className={popoverClassName}
    >
      <Grid container className={classes.mainContainer} justify="center">
        {Object.keys(alert).length > 0 && (
          <Grid item xs={12} className={classes.alert}>
            <AlertCard text={alert.copyAlert} color={alertColor} linkText={alert.copyOnClick} action={alert.onClick} />
          </Grid>
        )}

        <Grid item xs={12}>
          <div className={classes.sameLine}>
            <Megaphone color={color} className={classes.icon} />

            <Typography variant="body0" weight="bold" shade={900}>{accountsTitleCopy}</Typography>
          </div>

          <Grid item xs={12} className={classes.accounts}>
            <div className={classes.accountsContainer}>
              {allAccounts.map(platform => {
                const htmlId = Object.keys(platform)[0];

                return (
                  allowMultipleAccounts
                    ? <MultipleAccounts data={Object.values(platform)[0]} key={`management-popover-${htmlId}`} />
                    : <SimpleAccount data={Object.values(platform)[0]} key={`management-popover-simple-${htmlId}`} />
                );
              })}
            </div>
          </Grid>
        </Grid>

        <Grid item className={classes.divider} xs={12}>
          <Divider />
        </Grid>

        <Grid item xs={12}>
          <Grid container>
            {actions.map(action => (
              <Grid item className={classes.bottomSpace} key={action.name} xs={12}>
                <Link onClick={action.onClick} color="neutral" className={classes.sameLine}>
                  <Icon icon={action.icon} color={action.color ? action.color : color} className={classes.icon} />

                  <Typography variant="body0" weight="bold" shade={900}>{action.name}</Typography>
                </Link>
              </Grid>
            ))}
          </Grid>
        </Grid>

        {logOutCopy !== "" && (
          <>
            <Grid item className={classes.divider} xs={12}>
              <Divider />
            </Grid>

            <Grid item className={classes.signOut} xs={12}>
              <Link onClick={handleOnClickLogOut} color="neutral" className={classes.sameLine}>
                <Exit color="danger" className={classes.icon} />

                <Typography variant="body0" weight="bold" color="neutral" shade={900}>{logOutCopy}</Typography>
              </Link>
            </Grid>
          </>
        )}
      </Grid>
    </Popover>
  );
};

ManagementPopover.propTypes = {
  allAccounts: PropTypes.instanceOf(Array),
  allowMultipleAccounts: PropTypes.bool,
  accountsTitleCopy: PropTypes.string,
  actions: PropTypes.instanceOf(Object),
  logOutCopy: PropTypes.string,
  handleOnClickLogOut: PropTypes.instanceOf(Object),
  color: PropTypes.string,
  alert: PropTypes.instanceOf(Object),
  anchorEl: PropTypes.instanceOf(Object),
  setAnchorEl: PropTypes.instanceOf(Object),
  popoverClassName: PropTypes.instanceOf(Object),
};

ManagementPopover.defaultProps = {
  allAccounts: {},
  allowMultipleAccounts: true,
  accountsTitleCopy: "",
  actions: [],
  logOutCopy: "",
  handleOnClickLogOut: () => { },
  color: "primary",
  alert: {},
  anchorEl: null,
  setAnchorEl: () => { },
  popoverClassName: {},
};

export default ManagementPopover;
