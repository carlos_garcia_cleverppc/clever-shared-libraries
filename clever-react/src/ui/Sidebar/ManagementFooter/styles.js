import makeStyles from "../../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  root: {
    ...spacing["p-1"],
    ...spacing["m-1"],
    display: "inline-flex",
    alignItems: "center",
    marginLeft: "auto",
    position: "relative",
  },
  rootClickable: {
    display: "inline-flex",
    alignItems: "center",
    outline: "none",
    cursor: "pointer",
    "& path": { fill: colors.neutral[700] },
  },
  /* Label chip styles */
  label: ({ color }) => ({
    ...spacing["p-1"],
    ...spacing["ml-2"],
    backgroundColor: colors[color][100],
    width: "fit-content",
    height: "fit-content",
    borderRadius: "8px",
  }),
  /* Spacing between image and info */
  accountInfo: { ...spacing["mr-3"] },
  /* Name and label styles */
  name: {
    display: "inline-flex",
    alignItems: "center",
  },
  accountName: {
    wordBreak: "break-all",
    overflow: "hidden",
    textOverflow: "ellipsis",
    display: "-webkit-box",
    "-webkit-line-clamp": 1,
    "-webkit-box-orient": "vertical",
  },
  /* Chevron position if opened or closed */
  arrowPosition: { transform: "rotate(270deg)" },
  arrow: { ...spacing["ml-2"] },
  badge: ({ isSidebarCollapsed }) => ({
    "& .MuiBadge-anchorOriginTopRightRectangle": {
      right: isSidebarCollapsed ? "8px" : "18px", top: "4px", width: "12px", height: "12px", borderRadius: "50%",
    },
  }),
  popoverArrow: {
    "&:before": { zIndex: 1002 },
  },
  popoverZIndex: {
    zIndex: 1001,
  },
});

export default makeStyles(styles);
