/* eslint-disable no-extra-boolean-cast */
import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import last from "lodash-es/last";

import Avatar from "../../Avatar";
import Badge from "../../Badge";
import Grid from "../../Grid";
import Typography from "../../Typography";
import ChevronRight from "../../Icon/ChevronRight";

import DefaultAvatar from "./Components/DefaultAvatar";
import ManagementPopper from "./Components/ManagementPopover";
import ProgressAvatar from "./Components/ProgressAvatar";

import { onKeyDown } from "../../../utils/helpers";

import getClasses from "./styles";

const ManagementFooter = props => {
  // IF label !== "" THEN isPremium
  const { avatarSrc, avatarDefaultName, accountName, color, label } = props;
  const { isSidebarCollapsed, actions, logoutCopy, handleOnClickLogOut } = props;
  const { allAccounts, allowMultipleAccounts, accountsTitleCopy, alert, popoverClassName } = props;
  const { showProgress, onClickProgress, percentageDone, percentageCopy } = props;

  const classes = getClasses({ color, isSidebarCollapsed });

  const [openSelector, setOpenSelector] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = event => {
    setAnchorEl(last(event.currentTarget.getElementsByTagName("svg")));
    setOpenSelector(!openSelector);
  };

  const printAvatar = () => {
    /* If label !== "" image will be displayed with a circle */
    /* If label !== "" it will be displayed here when isSidebarCollapsed */
    let result = null;
    if (avatarSrc !== "") {
      result = (
        <Avatar
          src={avatarSrc}
          size={42}
          label={isSidebarCollapsed ? label : ""}
          colorCircle={!showProgress && label !== ""}
          className={!isSidebarCollapsed ? classes.accountInfo : ""}
          color={color}
          positionLabel="bottom-center"
        />
      );
    } else {
      result = (
        <DefaultAvatar
          name={avatarDefaultName}
          size={42}
          label={isSidebarCollapsed ? label : ""}
          colorCircle={!showProgress && label !== ""}
          className={!isSidebarCollapsed ? classes.accountInfo : ""}
          color={color}
        />
      );
    }

    if (showProgress) {
      result = (
        <ProgressAvatar
          onClickProgress={onClickProgress}
          percentageDone={percentageDone}
          percentageCopy={percentageCopy}
        >
          {result}
        </ProgressAvatar>
      );
    }

    if (Object.keys(alert).length > 0) {
      result = (
        <Badge color={alert.color} variant="dot" className={classes.badge}>{result}</Badge>
      );
    }

    return result;
  };

  return (
    <div
      className={classes.root}
      id="management-footer"
      aria-describedby="management-popover"
    >
      {printAvatar()}

      <div
        className={classes.rootClickable}
        onClick={handleClick}
        onKeyDown={onKeyDown(handleClick)}
        role="button"
        tabIndex={0}
      >
        {/* Name and accountId will be displayed if !isSidebarCollapsed */}
        {!isSidebarCollapsed && (
          <Grid container alignItems="center">
            <Grid item xs={12} className={classes.name}>
              <Typography weight="bold" variant="f1-14" className={classes.accountName}>{accountName}</Typography>

              {/* If label !== "" will be displayed here if !isSidebarCollapsed */}
              {label !== "" && (
                <Typography color={color} weight="bold" variant="f1-8" className={classes.label}>{label}</Typography>
              )}
            </Grid>
          </Grid>
        )}

        <ChevronRight className={classnames(classes.arrow, !!anchorEl ? classes.arrowPosition : "")} size={24} />
      </div>

      <ManagementPopper
        color={color}
        accountsTitleCopy={accountsTitleCopy}
        allAccounts={allAccounts}
        logOutCopy={logoutCopy}
        handleOnClickLogOut={handleOnClickLogOut}
        actions={actions}
        alert={alert}
        open={!!anchorEl}
        setOpen={setOpenSelector}
        anchorEl={anchorEl}
        setAnchorEl={setAnchorEl}
        allowMultipleAccounts={allowMultipleAccounts}
        popoverClassName={popoverClassName}
      />
    </div>
  );
};

ManagementFooter.propTypes = {
  avatarSrc: PropTypes.string,
  avatarDefaultName: PropTypes.string,
  accountName: PropTypes.string,
  color: PropTypes.string,
  label: PropTypes.string,
  isSidebarCollapsed: PropTypes.bool,
  actions: PropTypes.instanceOf(Object),
  logoutCopy: PropTypes.string,
  handleOnClickLogOut: PropTypes.instanceOf(Object),
  allAccounts: PropTypes.instanceOf(Object),
  alert: PropTypes.instanceOf(Object),
  allowMultipleAccounts: PropTypes.bool,
  popoverClassName: PropTypes.instanceOf(Object),
  showProgress: PropTypes.bool,
};

ManagementFooter.defaultProps = {
  avatarSrc: "",
  avatarDefaultName: "CL",
  accountName: "My clever account",
  color: "primary",
  label: "",
  isSidebarCollapsed: false,
  actions: null,
  logoutCopy: "",
  handleOnClickLogOut: () => { },
  allAccounts: {},
  alert: {},
  allowMultipleAccounts: true,
  popoverClassName: {},
  showProgress: false,
};

export default ManagementFooter;
