import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import Image from "../../Image";
import { onKeyDown } from "../../../utils/helpers";

import getClasses from "./styles";

const PlatformLogo = props => {
  const classes = getClasses({});

  const { src, isSidebarCollapsed, onClick, className } = props;

  return (
    <>
      <div
        className={classnames(className, isSidebarCollapsed ? classes.imageCollapsed : classes.image, onClick ? classes.cursor : "")}
        onClick={onClick}
        onKeyDown={onKeyDown(onClick)}
        role="button"
        tabIndex={0}
      >
        <Image
          src={src}
          size="responsive"
        />
      </div>
    </>
  );
};

PlatformLogo.propTypes = {
  src: PropTypes.string,
  isSidebarCollapsed: PropTypes.bool,
  onClick: PropTypes.instanceOf(Object),
  className: PropTypes.string,
};

PlatformLogo.defaultProps = {
  src: "",
  isSidebarCollapsed: false,
  onClick: null,
  className: "",
};

export default PlatformLogo;
