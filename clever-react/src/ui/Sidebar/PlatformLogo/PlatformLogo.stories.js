/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import PlatformLogo from "./index";

export default {
  title: "SidebarPlatformLogo",
  component: PlatformLogo,
};

const Template = args => <PlatformLogo {...args} />;

export const NormalPlatformLogo = Template.bind({});
NormalPlatformLogo.args = { src: "https://www.cleverecommerce.com/wp-content/uploads/2016/06/logoCleverEcommerce01.png" };

export const CollapsedPlatformLogo = Template.bind({});
CollapsedPlatformLogo.args = {
  src: "https://www.cleverecommerce.com/wp-content/uploads/2018/10/Icon_clever_ecommerce_no_background.png",
  isSidebarCollapsed: true,
};
