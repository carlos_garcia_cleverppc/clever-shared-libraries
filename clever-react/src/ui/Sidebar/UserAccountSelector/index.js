/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable import/no-cycle */
import React from "react";
import PropTypes from "prop-types";

import Link from "../../Link";
import PlusIcon from "../../Icon/PlusRound";
import Divider from "../../Divider";
import Grid from "../../Grid";
import List from "../../List";
import ListItem from "../../List/Item";
import Popover from "../../Popover";
import Avatar from "../../Avatar";

import Typography from "../../Typography";

import getClasses from "./styles";

const UserAccountsSelector = props => {
  const { allAccounts, handleOnClickItem, handleOnClickLogOut, handleAddAccount } = props;
  const { titleCopy, noAccountsCopy, addAccountCopy, logOutCopy } = props;
  const { open, setOpen, anchorEl, setAnchorEl } = props;

  const classes = getClasses();

  const printAccount = item => (
    <>
      <Avatar src={item.src} size={42} />

      <div className={classes.accountInfo}>
        <Grid container alignItems="center">
          <Grid item>
            <Typography weight="bold" variant="f1-14" className={classes.name}>{item.name}</Typography>
          </Grid>

          {!!item.accountId && (
            <Grid item>
              <Typography variant="body0">
                (
                {item.accountId}
                )
              </Typography>
            </Grid>
          )}
        </Grid>
      </div>
    </>
  );

  return (
    <Popover
      placement="top-center"
      open={open}
      anchorEl={anchorEl}
      className={classes.accountsPopper}
      onClose={() => {
        setAnchorEl(null);
        setOpen(false);
      }}
      id="accounts-popper"
      arrow
      backgroundColor="white"
    >
      <Grid container className={classes.mainContainer} justify="center">
        <Grid item xs={12} className={classes.title}>
          <Typography variant="body0" weight="bold" shade={900}>{titleCopy}</Typography>
        </Grid>

        <Grid item xs={12}>
          {allAccounts.length > 1 && (
            <List className={classes.accountList}>
              {allAccounts.filter(item => !item.isSelected).map(account => (
                <ListItem
                  onClick={() => handleOnClickItem(account.accountId)}
                  className={classes.listElement}
                  key={account.name}
                >
                  {printAccount(account)}
                </ListItem>
              ))}
            </List>
          )}

          {allAccounts.length === 1 && (
            <Typography variant="body0" align="center" color="neutral" className={classes.noAccounts}>
              {noAccountsCopy}
            </Typography>
          )}
        </Grid>

        {addAccountCopy !== "" && (
          <Grid item xs={12} className={classes.newAccount}>
            <div
              onClick={handleAddAccount}
              onKeyDown={handleAddAccount}
              role="button"
              tabIndex={0}
              className={classes.addAccountContainer}
            >
              <PlusIcon className={classes.addImage} color="blue" size={32} />

              <Typography variant="body0" weight="bold" align="center" color="blue">{addAccountCopy}</Typography>
            </div>
          </Grid>
        )}

        <Grid item className={classes.divider} xs={12}>
          <Divider />
        </Grid>

        <Grid item className={classes.signOut} xs={12}>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="#" onClick={handleOnClickLogOut} color="danger">
                <Typography variant="body0" weight="bold" color="danger">{logOutCopy}</Typography>
              </Link>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Popover>
  );
};

UserAccountsSelector.propTypes = {
  allAccounts: PropTypes.instanceOf(Array),
  handleOnClickItem: PropTypes.instanceOf(Object),
  handleOnClickLogOut: PropTypes.instanceOf(Object),
  handleAddAccount: PropTypes.instanceOf(Object),
  titleCopy: PropTypes.string,
  noAccountsCopy: PropTypes.string,
  addAccountCopy: PropTypes.string,
  logOutCopy: PropTypes.string,
  open: PropTypes.bool,
  setOpen: PropTypes.instanceOf(Object),
  anchorEl: PropTypes.instanceOf(Object),
  setAnchorEl: PropTypes.instanceOf(Object),
};

UserAccountsSelector.defaultProps = {
  allAccounts: [],
  handleOnClickItem: () => { },
  handleOnClickLogOut: () => { },
  handleAddAccount: () => { },
  titleCopy: "",
  noAccountsCopy: "",
  addAccountCopy: "",
  logOutCopy: "",
  open: false,
  setOpen: () => { },
  anchorEl: null,
  setAnchorEl: () => { },
};

export default UserAccountsSelector;
