/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Footer from "./index";

import UserAccount from "../UserAccount";
import FooterActions from "../FooterActions";

export default {
  title: "SidebarFooter",
  component: Footer,
};

const Template = args => <Footer {...args} />;

export const SimpleFooter = Template.bind({});
SimpleFooter.args = {
  actionsPosition: "right",
  userAccount: (
    <UserAccount accountSelected={{ name: "Test name", accountId: "123456789" }} label="PRO" />
  ),
  footerActions: (
    <FooterActions
      items={[
        {
          icon: "Support",
          name: "Contact",
        },
        {
          icon: "Ruler",
          name: "Metrics",
        },
      ]}
    />
  ),
};
