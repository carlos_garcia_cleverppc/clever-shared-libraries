import React from "react";
import PropTypes from "prop-types";

import List from "../../List";
import getClasses from "./styles";

const SidebarDropdown = props => {
  const classes = getClasses();

  const { itemsList, isSidebarCollapsed } = props;

  return (
    <>
      <List component="div" disablePadding className={isSidebarCollapsed ? classes.dropdownListCollapsed : classes.dropdownList}>
        {itemsList}
      </List>
    </>
  );
};

SidebarDropdown.propTypes = {
  itemsList: PropTypes.instanceOf(Array),
  isSidebarCollapsed: PropTypes.bool,
};

SidebarDropdown.defaultProps = {
  itemsList: [],
  isSidebarCollapsed: false,
};

export default SidebarDropdown;
