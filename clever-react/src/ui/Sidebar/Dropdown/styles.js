import makeStyles from "../../styles/makeStyles";

const styles = ({ cleverUI: { spacing } }) => ({
  dropdownList: {
    marginTop: "0",
    marginBottom: "0",
    ...spacing["pb-2"],
    ...spacing["pl-5"],
  },
  dropdownListCollapsed: {
    marginTop: "0",
    marginBottom: "0",
    ...spacing["pb-2"],
    ...spacing["pl-4"],
  },
});

export default makeStyles(styles);
