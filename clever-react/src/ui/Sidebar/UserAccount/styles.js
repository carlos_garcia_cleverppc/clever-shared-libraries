import makeStyles from "../../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  root: ({ cursor }) => ({
    ...spacing["p-1"],
    ...spacing["m-1"],
    display: "inline-flex",
    alignItems: "center",
    outline: "none",
    cursor: `${cursor}`,
    marginLeft: "auto",
    "& path": { fill: colors.neutral[900] },
  }),
  /* Label chip styles */
  label: ({ color }) => ({
    ...spacing["p-1"],
    ...spacing["ml-2"],
    backgroundColor: colors[color][100],
    width: "fit-content",
    height: "fit-content",
    borderRadius: "8px",
  }),
  /* Spacing between image and info */
  accountInfo: { ...spacing["mr-3"] },
  /* Name and label styles */
  name: {
    display: "inline-flex",
    alignItems: "center",
  },
  accountName: {
    wordBreak: "break-all",
    overflow: "hidden",
    textOverflow: "ellipsis",
    display: "-webkit-box",
    "-webkit-line-clamp": 1,
    "-webkit-box-orient": "vertical",
  },
  /* Chevron position if opened or closed */
  arrowPosition: { transform: "rotate(270deg)" },
  arrow: { ...spacing["ml-2"] },
});

export default makeStyles(styles);
