/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import UserAccount from "./index";

export default {
  title: "SidebarUserAccount",
  component: UserAccount,
};

const Template = args => (
  <div style={{ marginTop: "350px" }}>
    <UserAccount {...args} />
  </div>
);

export const SimpleUserAccount = Template.bind({});
SimpleUserAccount.args = {
  avatarSrc: "",
  accountSelected: { name: "Test name", accountId: "12345678" },
};

export const ItemWithChildren = Template.bind({});
ItemWithChildren.args = {
  avatarSrc: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTafhaU5d1HmEJ3wmi-ZYtu-XsZX6UJiq1WAg&usqp=CAU",
  accountSelected: { name: "Test name", accountId: "123456789" },
  label: "PRO",
  allAccounts: [
    { name: "Test name", accountId: "123456789", isSelected: true },
    { name: "Test name2", accountId: "223456789", isSelected: false },
    { name: "Test name3", accountId: "323456789", isSelected: false },
  ],
  titleCopy: "titleCopy",
  noAccountsCopy: "noAccountsCopy",
  addAccountCopy: "addAccountCopy",
  logOutCopy: "logOutCopy",
};
