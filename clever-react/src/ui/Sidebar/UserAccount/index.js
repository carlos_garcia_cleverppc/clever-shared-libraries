/* eslint-disable import/no-cycle */
import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import last from "lodash-es/last";

import UserAccountSelector from "../UserAccountSelector";
import Avatar from "../../Avatar";
import Grid from "../../Grid";
import Typography from "../../Typography";
import ChevronRight from "../../Icon/ChevronRight";

import { onKeyDown } from "../../../utils/helpers";

import getClasses from "./styles";

const UserAccount = props => {
  const { avatarSrc, accountSelected, allAccounts, isSidebarCollapsed, label, color } = props;
  const { handleOnClickItem, handleOnClickLogOut, handleAddAccount } = props;
  const { titleCopy, noAccountsCopy, addAccountCopy, logOutCopy } = props;

  const [openSelector, setOpenSelector] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);

  /* Cursor will be different if there are more accounts or not */
  const cursor = allAccounts.length > 0 ? "pointer" : "default";

  const handleClick = event => {
    /* This function only happen there are more accounts */
    if (allAccounts.length >= 1) {
      setAnchorEl(last(event.currentTarget.getElementsByTagName("svg")));
      setOpenSelector(!openSelector);
    }
  };

  const classes = getClasses({ color, cursor });

  return (
    <div className={classes.root} onClick={handleClick} onKeyDown={onKeyDown(handleClick)} role="button" tabIndex={0}>
      {/* If label !== "" image will be displayed with a circle */}
      {/* If label !== "" it will be displayed here when isSidebarCollapsed */}
      <Avatar
        src={avatarSrc}
        size={42}
        label={isSidebarCollapsed ? label : ""}
        colorCircle={label !== ""}
        className={!isSidebarCollapsed ? classes.accountInfo : ""}
        color={color}
      />

      {/* Name and accountId will be displayed if !isSidebarCollapsed */}
      {!isSidebarCollapsed && (
        <Grid container alignItems="center">
          <Grid item xs={12} className={classes.name}>
            <Typography weight="bold" variant="f1-14" className={classes.accountName}>{accountSelected.name}</Typography>

            {/* If label !== "" will be displayed here if !isSidebarCollapsed */}
            {label !== "" && (
              <Typography color={color} weight="bold" variant="f1-8" className={classes.label}>{label}</Typography>
            )}
          </Grid>

          {!!accountSelected.accountId && (
            <Grid item xs={12}>
              <Typography variant="body0">{accountSelected.accountId}</Typography>
            </Grid>
          )}
        </Grid>
      )}

      {allAccounts.length > 0 && (
        <div>
          <ChevronRight className={classnames(classes.arrow, openSelector ? classes.arrowPosition : "")} size={20} />

          <UserAccountSelector
            allAccounts={allAccounts}
            src={avatarSrc}
            handleOnClickItem={handleOnClickItem}
            handleOnClickLogOut={handleOnClickLogOut}
            handleAddAccount={handleAddAccount}
            titleCopy={titleCopy}
            noAccountsCopy={noAccountsCopy}
            addAccountCopy={addAccountCopy}
            logOutCopy={logOutCopy}
            open={openSelector}
            setOpen={setOpenSelector}
            anchorEl={anchorEl}
            setAnchorEl={setAnchorEl}
          />
        </div>
      )}
    </div>
  );
};

UserAccount.propTypes = {
  avatarSrc: PropTypes.string,
  accountSelected: PropTypes.instanceOf(Object),
  allAccounts: PropTypes.instanceOf(Array),
  isSidebarCollapsed: PropTypes.bool,
  label: PropTypes.string,
  color: PropTypes.string,
  handleOnClickItem: PropTypes.instanceOf(Object),
  handleOnClickLogOut: PropTypes.instanceOf(Object),
  handleAddAccount: PropTypes.instanceOf(Object),
  titleCopy: PropTypes.string,
  noAccountsCopy: PropTypes.string,
  addAccountCopy: PropTypes.string,
  logOutCopy: PropTypes.string,
};

UserAccount.defaultProps = {
  avatarSrc: "",
  accountSelected: { name: "", accountId: "" },
  allAccounts: [],
  isSidebarCollapsed: false,
  label: "",
  color: "primary",
  handleOnClickItem: () => { },
  handleOnClickLogOut: () => { },
  handleAddAccount: () => { },
  titleCopy: "",
  noAccountsCopy: "",
  addAccountCopy: "",
  logOutCopy: "",
};

export default UserAccount;
