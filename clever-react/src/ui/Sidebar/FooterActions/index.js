import React from "react";
import PropTypes from "prop-types";

import Grid from "../../Grid";
import Icon from "../../Icon";
import IconButton from "../../IconButton";
import Badge from "../../Badge";
import Tooltip from "../../Tooltip";

import getClasses from "./styles";

const FooterActions = props => {
  const { items, color, isSidebarCollapsed } = props;

  const classes = getClasses();

  const printItem = item => {
    /* Each item can has: name, icon, isSelected, badgeActive and onClick */
    const name = item.name ? item.name : "";
    const icon = item.icon ? item.icon : "";
    const { isSelected } = item; // item.isSelected
    const badgeActive = !!item.badgeActive;
    const clickFunction = item.onClick ? item.onClick : () => { };
    const showTooltip = typeof (item.tooltip) !== "undefined" ? item.tooltip : true;
    const itemColor = item.color ? item.color : color;

    return (
      <div>
        {!badgeActive && showTooltip && (
          <Tooltip title={name}>
            <IconButton
              onClick={clickFunction}
              color={itemColor}
              icon={<Icon icon={icon} />}
              active={isSelected}
            />
          </Tooltip>
        )}

        {!badgeActive && !showTooltip && (
          <IconButton
            onClick={clickFunction}
            color={itemColor}
            icon={<Icon icon={icon} />}
            active={isSelected}
          />
        )}

        {badgeActive && showTooltip && (
          <Tooltip title={name}>
            <IconButton
              onClick={clickFunction}
              color={itemColor}
              active={isSelected}
              icon={(
                <Badge variant="dot" invisible={false} color="danger">
                  <Icon icon={icon} />
                </Badge>
              )}
            />
          </Tooltip>
        )}

        {badgeActive && !showTooltip && (
          <IconButton
            onClick={clickFunction}
            color={itemColor}
            active={isSelected}
            icon={(
              <Badge variant="dot" invisible={false} color="danger">
                <Icon icon={icon} />
              </Badge>
            )}
          />
        )}
      </div>
    );
  };

  return (
    <>
      {/* Each item will be printed */}
      {items.length > 0 && (
        <div id="footer-actions" className={classes.root}>
          <Grid container className={isSidebarCollapsed ? classes.actionsList : ""}>
            {items.map(action => (
              <Grid item className={isSidebarCollapsed ? classes.iconButtonVertical : ""} key={action.name}>
                {printItem(action)}
              </Grid>
            ))}
          </Grid>
        </div>
      )}
    </>
  );
};

FooterActions.propTypes = {
  items: PropTypes.instanceOf(Array),
  color: PropTypes.string,
  isSidebarCollapsed: PropTypes.bool,
};

FooterActions.defaultProps = {
  items: [],
  color: "primary",
  isSidebarCollapsed: false,
};

export default FooterActions;
