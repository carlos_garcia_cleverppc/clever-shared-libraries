import makeStyles from "../../styles/makeStyles";

const styles = ({ cleverUI: { spacing } }) => ({
  root: { minHeight: "39.39px" },
  actionsList: {
    display: "block",
    width: "50px",
  },
  iconButtonVertical: {
    ...spacing["mx-auto"],
    width: "fit-content",
  },
});

export default makeStyles(styles);
