/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import FooterActions from "./index";

export default {
  title: "SidebarFooterActions",
  component: FooterActions,
};

const Template = args => <FooterActions {...args} />;

export const SimpleFooterActionsAndBadge = Template.bind({});
SimpleFooterActionsAndBadge.args = {
  items: [
    {
      icon: "Cog",
      name: "Settings",
    },
    {
      icon: "BellOutlined",
      name: "Notifications",
      badgeActive: true,
    },
  ],
};
