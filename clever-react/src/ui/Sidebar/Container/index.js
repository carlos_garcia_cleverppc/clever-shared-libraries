/* eslint-disable import/no-cycle */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import getClasses from "./styles";

const Container = props => {
  const { isSidebarCollapsed, platformLogo, body, footer, className } = props;
  const [bodyHeight, setBodyHeight] = useState("");
  const classes = getClasses({ isSidebarCollapsed, bodyHeight });

  useEffect(() => {
    const logoElem = document.querySelector("#sidebar-logo");
    const rectLogo = logoElem ? logoElem.getBoundingClientRect().height : 0;

    const dividerElem = document.querySelector("#divider-footer");
    const dividerHeight = dividerElem ? 31 : 0;

    // Footer actions height
    const actionsRightPosition = document.querySelector("#footer-actions-right");
    const actionsElem = document.querySelector("#footer-actions");
    const actionsCount = actionsElem ? actionsElem.children[0].children.length : 0;

    // 39.39: Action height
    let actionsHeight = isSidebarCollapsed ? actionsCount * 39.39 : 39.39;

    // New management footer
    const managementFooter = document.querySelector("#management-footer");

    // If actions are at right, don't sum anything to height
    actionsHeight = actionsRightPosition ? 0 : actionsHeight;

    // 94: height is always the same when !!managementFooter
    // 36: padding top + bottom
    // 58: Avatar height
    const calcHeightFooter = managementFooter ? 94 : 36 + 58 + actionsHeight + dividerHeight;

    setBodyHeight(`calc(100% - ${rectLogo}px - ${calcHeightFooter}px )`);
  }, [props]);

  return (
    <div className={classnames(className, classes.root, isSidebarCollapsed ? classes.rootCollapsed : "")}>
      {!!platformLogo && (
        <div id="sidebar-logo" className={classes.platformLogo}>
          {platformLogo}
        </div>
      )}

      <div id="sidebar-body" className={classnames(classes.body, isSidebarCollapsed ? "" : classes.bodyDesktop)}>
        {body}
      </div>

      <div id="sidebar-footer" className={isSidebarCollapsed ? classes.footerCollapsed : classes.footer}>
        {footer}
      </div>
    </div>
  );
};

Container.propTypes = {
  isSidebarCollapsed: PropTypes.bool,
  platformLogo: PropTypes.instanceOf(Object),
  body: PropTypes.instanceOf(Object),
  footer: PropTypes.instanceOf(Object),
  className: PropTypes.string,
};

Container.defaultProps = {
  isSidebarCollapsed: false,
  platformLogo: null,
  body: null,
  footer: null,
  className: "",
};

export default Container;
