import makeStyles from "../../styles/makeStyles";

const styles = ({ cleverUI: { spacing } }) => ({
  root: {
    backgroundColor: "white",
    height: "100%",
    width: "100%",
  },
  rootCollapsed: { width: "160px" },
  platformLogo: {
    ...spacing["pl-7"],
    height: "200px",
    display: "flex",
    alignItems: "center",
  },
  body: ({ bodyHeight }) => ({
    ...spacing["pl-7"],
    height: bodyHeight,
    "&::-webkit-scrollbar": { width: "5px" },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "#babac0",
      borderRadius: "16px",
      borderLeft: "1px solid #fff",
    },
  }),
  bodyDesktop: { ...spacing["pr-7"] },
  footer: {
    ...spacing["px-7"],
    ...spacing["pb-5"],
    ...spacing["pt-3"],
  },
  footerCollapsed: {
    ...spacing["pl-7"],
    ...spacing["pb-5"],
    ...spacing["pt-3"],
  },
});

export default makeStyles(styles);
