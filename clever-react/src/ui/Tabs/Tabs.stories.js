/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import { getColorsLabels } from "../styles/colors";

import Tabs from "./index";
import Tab from "../Tab";
import Image from "../Image";

export default {
  title: "Tabs",
  component: Tabs,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div>
      <Tabs {...args} value={value} onChange={handleChange}>
        <Tab label="Item One" value={0} icon={<Image size="50x50" center={false} src="/assets/img/avatar-google.svg" />} />
        <Tab label="Item Two" value={1} icon={<Image size="50x50" center={false} src="/assets/img/avatar-ms.svg" />} />
      </Tabs>

      {value === 0 && <h1>Item1</h1>}
      {value === 1 && <h1>Item2</h1>}
    </div>
  );
};

export const SimpleTabs = Template.bind({});
SimpleTabs.args = {};
