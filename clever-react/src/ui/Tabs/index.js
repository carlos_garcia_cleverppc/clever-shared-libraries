/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";

import MaterialTabs from "@material-ui/core/Tabs";

import makeStyles from "../styles/makeStyles";
import { getColorsLabels } from "../styles/colors";

const getClasses = makeStyles(({ cleverUI: { colors } }) => ({
  flexContainer: { borderBottom: `solid 1px ${colors.neutral[300]}` },
  indicator: ({ color }) => ({ height: "3px", backgroundColor: colors[color][500] }),
}));

const colorsList = getColorsLabels(true);

const Tabs = props => {
  const { children, value, onChange, color, variant, scrollButtons } = props;

  const classes = getClasses({ color });

  return (
    <MaterialTabs
      value={value}
      onChange={onChange}
      classes={{
        flexContainer: classes.flexContainer,
        indicator: classes.indicator,
      }}
      variant={variant}
      scrollButtons={scrollButtons}
    >
      {children}
    </MaterialTabs>
  );
};

Tabs.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onChange: PropTypes.func,
  color: PropTypes.oneOf(colorsList),
  variant: PropTypes.oneOf(["standard", "scrollable"]),
  scrollButtons: PropTypes.oneOf(["auto", "desktop", "on", "off"]),
};

Tabs.defaultProps = {
  onChange: () => { },
  color: "primary",
  variant: "standard",
  scrollButtons: "auto",
};

export default Tabs;
