/* eslint-disable no-console */
import React, { useState } from "react";

import ImageUploader from "./index";

import exampleImage from "./exampleImage";

export default {
  title: "ImageUploader",
  component: ImageUploader,
};

const Template = args => {
  const [open, setOpen] = useState(false);
  const [logo, setLogo] = useState();

  const closeLogoModal = () => setOpen(false);

  const uploadLogo = img => {
    setLogo(img);

    // send logo to backend

    return img;
  };

  const getBase64FromUrlApi = async img => (
    /**
     * Lo que devolvemos aqui es una imagen de ejemplo, esta conversion se lleva a cabo en el back
     */
    { base64: exampleImage }
  );

  return (
    <ImageUploader
      {...args}
      open={open}
      logo={logo}
      closeModal={closeLogoModal}
      onSubmit={uploadLogo}
      getBase64FromUrlApi={getBase64FromUrlApi}
    />
  );
};

export const SimpleImageUploader = Template.bind({});
SimpleImageUploader.args = {
  images: {
    warning: "/assets/img/error.svg",
  },
  copies: {
    errors: {
      invalidUrl: "This image URL is not valid or not working at the moment. Remember you can only use .PNG, .JPEG or .JPG images.",
      invalidFileFormat: "Please upload a valid PNG, JPEG or JPG file. The file must be 10MB or less.",
      errorUploadingImage: "Oops! Something went wrong, please upload your logo again. If this error persists, contact us.",
    },
    cropper: {
      title: "Crop your image",
      howTo: "Tip: You can zoom in and out of the image.",
      chooseImageButton: "Done",
    },
    closeButton: "OK",
  },
};
