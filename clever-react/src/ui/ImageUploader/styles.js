import makeStyles from "../styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { colors, spacing, shadows } }) => ({
  root: {
    width: "fit-content",
    display: "inline-block",
  },
  panel: { ...spacing["mb-4"] },
  cropper: {
    ...spacing["mx-auto"],
    ...spacing["mb-2"],
    minHeight: 144,
    maxHeight: 400,
    maxWidth: 600,
    width: "100%",
  },
  loader: { ...spacing["h-100"] },
  logoPlaceholder: {
    width: "9rem",
    height: "9rem",
    alignItems: "center",
    justifyContent: "center",
  },
  newLogo: {
    ...spacing["p-2"],
    display: "flex",
    background: "transparent",
    border: `solid 1px ${colors.gray.main}`,
    width: "9rem",
    height: "9rem",
    borderRadius: "12px",
    boxSizing: "border-box",
    textAlign: "center",
  },
  imagePreview: {
    maxWidth: "100%",
    maxHeight: "100%",
    ...spacing["w-auto"],
    ...spacing["h-auto"],
    ...spacing["m-auto"],
  },
  actions: {
    ...spacing["mnt-5"],
    ...spacing["mb-4"],
    marginLeft: "5rem",
    display: "flex",
    position: "absolute",
  },
  actionsMobile: { marginLeft: 0, position: "absolute" },
  urlInputContainer: {
    ...spacing["px-6"],
    ...spacing["mnx-3"],
    ...spacing["pb-1"],
    ...spacing["pt-3"],
    backgroundColor: "white",
    boxShadow: shadows.hover,
  },
  input: { maxWidth: "175px", width: "100%", minWidth: "16px", "& input": { padding: "0px !important" } },
  inputEnter: { width: 0 },
  inputEnterActive: {
    width: "175px",
    transition: "width 500ms ease-in-out",
  },
  inputLeave: { width: "200px" },
  inputLeaveActive: {
    width: 0,
    transition: "width 300ms ease-in-out",
  },
  buttonEnter: { transform: "translate(-10px, 0)" },
  buttonEnterActive: {
    transform: "translate(0, 0)",
    transition: "all 500ms ease-in-out",
  },
  buttonLeave: {},
  buttonLeaveActive: {
    transform: "translate(0, 0)",
    transition: "all 800ms ease-in-out",
  },
  uploadLogoBtn: { ...spacing["mr-3"] },
  inputTransition: { zIndex: 1 },
  iconButton: { zIndex: 2 },
  hiddeInput: { display: "none" },
}));

export default getClasses;
