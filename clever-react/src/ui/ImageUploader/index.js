import React, { useEffect, useRef, useState } from "react";
import PropTypes from "prop-types";
import FileReaderInput from "react-file-reader-input";
import Cropper from "react-cropper";
import ceil from "lodash-es/ceil";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import classnames from "classnames";

import "cropperjs/dist/cropper.css";

import Card from "../Card";
import Dialog from "../Dialog";
import TextField from "../TextField";
import Typography from "../Typography";
import IconButton from "../IconButton";
import Upload from "../Icon/Upload";
import ImageIcon from "../Icon/Image";
import Link from "../Icon/Link";
import Enter from "../Icon/Enter";
import Loader from "../Loading";

import getClasses from "./styles";
import { resizeBase64ForMaxWidth, resizeBase64ForMaxHeight } from "./resizer";

import { isValidUrl, getImageDimensions } from "./image";

const availableMimetypes = ["image/jpg", "image/png", "image/jpeg"];

const ImageUpload = props => {
  const classes = getClasses();

  const { minHeight, minWidth, aspect, getBase64FromUrlApi, className } = props;
  const { image, logo, mobile, onSubmit, copies, mbLimit, images } = props;

  const [uploadDialog, setUploadDialog] = useState({ logoUrl: "" });
  const [croppedImage, setCroppedImage] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [openUrl, setOpenUrl] = useState(false);
  const [openFile, setOpenFile] = useState(false);
  const [openInvalidUrl, setOpenInvalidUrl] = useState(false);
  const [openUploadError, setOpenUploadError] = useState(false);
  const [openInvalidFileFormat, setOpenInvalidFileFormat] = useState(false);
  const [showUrl, setShowUrl] = useState(false);
  const [base64Img, setBase64Img] = useState(undefined);
  const refLink = useRef(null);
  const refFile = useRef(null);

  useEffect(() => {
    if (image) {
      setUploadDialog({
        logoUrl: "",
        hasError: false,
      });

      setBase64Img(image);
    }
  }, []);

  const hideErrorInUpdateDialog = () => {
    setOpenInvalidFileFormat(false);
    setUploadDialog({ ...uploadDialog, hasError: false });
  };

  const updateLogo = async () => {
    let finalImage = "";

    setOpenFile(false);
    setOpenUrl(false);
    setIsLoading(true);

    hideErrorInUpdateDialog();

    const dimensions = await getImageDimensions(croppedImage.toDataURL());

    if (dimensions.height > 300 || dimensions.width > 300) {
      const successCallback = resizedImage => {
        finalImage = resizedImage;
      };

      const errorCallback = () => {
        setOpenUploadError(true);
      };

      if (dimensions.height > dimensions.width) {
        await resizeBase64ForMaxHeight(croppedImage.toDataURL(), 300, 300, successCallback, errorCallback);
      } else {
        await resizeBase64ForMaxWidth(croppedImage.toDataURL(), 300, 300, successCallback, errorCallback);
      }
    } else {
      finalImage = croppedImage.toDataURL();
    }

    const res = await onSubmit(finalImage);

    setOpenFile(false);
    setOpenUrl(false);
    setIsLoading(false);

    if (res.error) { // error during upload
      setOpenUploadError(true);
    } else { // successful upload
      setOpenUploadError(false);
    }
  };

  const handleChangeFile = (event, results) => {
    const [e, file] = results[0];

    const base64 = e.target.result;
    const fileSize = 4 * ceil(base64.length / 3);

    if (!!file && !!file.type) {
      if (availableMimetypes.indexOf(file.type) !== -1 && fileSize < mbLimit * 1000000) {
        setUploadDialog({ logoUrl: "" });
        setShowUrl(false);
        setBase64Img(base64);
        setOpenFile(true);
      } else {
        setUploadDialog({ ...uploadDialog, logoUrl: "", hasError: true });
        setOpenInvalidFileFormat(true);
      }
    }

    return false;
  };

  const handleShowURL = () => {
    setShowUrl(!showUrl);
  };

  const handleUrlChange = evt => {
    const logoUrl = evt.target.value;

    setUploadDialog({
      ...uploadDialog,
      logoUrl,
      hasError: false,
    });
  };

  const updateLogoUrlValue = async () => {
    const { logoUrl } = uploadDialog;

    if (isValidUrl(logoUrl)) {
      setIsLoading(true);

      const { base64 } = await getBase64FromUrlApi(logoUrl);

      setIsLoading(false);

      setUploadDialog({ ...uploadDialog, logoUrl, hasError: false });
      setBase64Img(base64);
      setOpenUrl(true);
    } else {
      setUploadDialog({ ...uploadDialog, logoUrl, hasError: true });
      setOpenInvalidUrl(true);
    }
  };

  const updateLogoUrlValueEnter = event => {
    if (event.key === "Enter") {
      updateLogoUrlValue();
    }
  };

  const crop = () => {
    // image in dataUrl
    if (openFile) {
      setCroppedImage(refFile.current.cropper.getCroppedCanvas());
    } else if (openUrl) {
      setCroppedImage(refLink.current.cropper.getCroppedCanvas());
    }
  };

  return (
    <div className={classnames(classes.root, className)}>
      {!!logo && (
        <div className={classes.newLogo}>
          {!isLoading && <img src={logo} className={classes.imagePreview} alt="logo" />}

          {isLoading && <Loader size={60} center className={classes.loader} />}
        </div>
      )}

      {!logo && (
        <Card variant="striped" className={classes.logoPlaceholder}>
          {!isLoading && <ImageIcon size={32} />}

          {isLoading && <Loader size={60} center className={classes.loader} />}
        </Card>
      )}

      <div className={mobile ? classnames(classes.actions, classes.actionsMobile) : classes.actions}>
        <FileReaderInput
          id="my-file-input"
          onChange={handleChangeFile}
          className={classes.hiddeInput}
        >
          <IconButton outlined={false} color="primary" className={classes.uploadLogoBtn} icon={<Upload />} />
        </FileReaderInput>

        <IconButton
          outlined={false}
          onClick={() => handleShowURL(showUrl)}
          color="primary"
          active={showUrl}
          icon={<Link />}
          className={classes.iconButton}
        />

        <ReactCSSTransitionGroup
          transitionName={{
            enter: classes.inputEnter,
            enterActive: classes.inputEnterActive,
            leave: classes.inputLeave,
            leaveActive: classes.inputLeaveActive,
          }}
          className={classes.inputTransition}
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}
        >
          {showUrl ? (
            <div className={classes.urlInputContainer}>
              <TextField
                variant="standard"
                className={classes.input}
                value={uploadDialog.logoUrl}
                onChange={evt => handleUrlChange(evt)}
                onKeyPress={updateLogoUrlValueEnter}
                size="small"
              />
            </div>
          ) : null}
        </ReactCSSTransitionGroup>

        <ReactCSSTransitionGroup
          transitionName={{
            enter: classes.buttonEnter,
            enterActive: classes.buttonEnterActive,
            leave: classes.buttonLeave,
            leaveActive: classes.buttonLeaveActive,
          }}
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}
        >
          {showUrl
            ? (
              <IconButton
                outlined={false}
                color="primary"
                onClick={updateLogoUrlValue}
                icon={<Enter />}
                className={classes.iconButton}
              />
            )
            : null}
        </ReactCSSTransitionGroup>
      </div>

      <Dialog
        open={openUrl}
        onClose={() => setOpenUrl(false)}
        title={copies.cropper.title}
        buttonPrimary={{
          label: copies.cropper.chooseImageButton,
          onClick: updateLogo,
          isLoading,
        }}
      >
        <>
          <Typography variant="body1" align="center" className={classes.panel}>
            {copies.cropper.howTo}
          </Typography>

          <Cropper
            ref={refLink}
            src={base64Img}
            className={classes.cropper}
            aspectRatio={aspect}
            ready={crop}
            cropend={crop}
            zoom={crop}
            minCropBoxWidth={minWidth}
            minCropBoxHeight={minHeight}
          />
        </>
      </Dialog>

      <Dialog
        open={openInvalidUrl}
        onClose={() => setOpenInvalidUrl(false)}
        title={copies.errors.invalidUrl}
        image={images.warning}
        buttonPrimary={{
          label: copies.closeButton,
          onClick: () => setOpenInvalidUrl(false),
        }}
      />

      <Dialog
        open={openFile}
        onClose={() => setOpenFile(false)}
        title={copies.cropper.title}
        buttonPrimary={{
          label: copies.cropper.chooseImageButton,
          onClick: updateLogo,
          isLoading,
        }}
      >
        <>
          <Typography variant="body1" align="center" className={classes.panel}>
            {copies.cropper.howTo}
          </Typography>

          <Cropper
            ref={refFile}
            src={base64Img}
            className={classes.cropper}
            aspectRatio={aspect}
            ready={crop}
            cropend={crop}
            minCropBoxWidth={minWidth}
            minCropBoxHeight={minHeight}
          />
        </>
      </Dialog>

      <Dialog
        open={openInvalidFileFormat}
        onClose={() => setOpenInvalidFileFormat(false)}
        title={copies.errors.invalidFileFormat}
        image={images.warning}
        buttonPrimary={{
          label: copies.closeButton,
          onClick: () => setOpenInvalidFileFormat(false),
        }}
      />

      <Dialog
        open={openUploadError}
        onClose={() => setOpenUploadError(false)}
        title={copies.errors.errorUploadingImage}
        image={images.warning}
        buttonPrimary={{
          label: copies.closeButton,
          onClick: () => setOpenUploadError(false),
        }}
      />
    </div>
  );
};

ImageUpload.propTypes = {
  onSubmit: PropTypes.func,
  minWidth: PropTypes.number,
  minHeight: PropTypes.number,
  aspect: PropTypes.number,
  mbLimit: PropTypes.number,
  image: PropTypes.string,
  images: PropTypes.instanceOf(Object),
  getBase64FromUrlApi: PropTypes.func.isRequired,
};

ImageUpload.defaultProps = {
  onSubmit: () => { },
  minHeight: 144,
  minWidth: 144,
  aspect: null,
  image: "",
  mbLimit: 10,
  images: {},
};

export default ImageUpload;
