import take from "lodash-es/take";
import sortBy from "lodash-es/sortBy";

/* eslint-disable no-multi-assign */

export const componentToHex = c => {
  const hex = c.toString(16);

  return hex.length === 1 ? `0${hex}` : hex;
};

export const getAverageHex = imgSrc => new Promise(resolve => {
  const imgEl = new Image();

  imgEl.onload = () => {
    const blockSize = 5; // only visit every 5 pixels
    const defaultHexs = ["#000", "#111", "#222", "#555", "#777", "#999", "#aaa", "#ccc", "#ddd", "#fff"]; // for non-supporting envs

    const canvas = document.createElement("canvas");
    const context = canvas.getContext && canvas.getContext("2d");
    let data = null;

    let i = -4;

    if (!context) {
      resolve(defaultHexs);
    }

    const height = canvas.height = imgEl.naturalHeight || imgEl.offsetHeight || imgEl.height;
    const width = canvas.width = imgEl.naturalWidth || imgEl.offsetWidth || imgEl.width;

    context.drawImage(imgEl, 0, 0);

    try {
      data = context.getImageData(0, 0, width, height);
    } catch (e) {
      /* security error, img on diff domain */
      resolve(defaultHexs);
    }

    const { data: { length } } = data;

    const colors = {};

    try {
      while ((i += blockSize * 4) < length) {
        const r = data.data[i];
        const g = data.data[i + 1];
        const b = data.data[i + 2];
        const a = data.data[i + 3];

        // Clean transparent pixels
        if (a > 125 && (r < 250 && g < 250 && b < 250)) {
          const hex = `#${componentToHex(r)}${componentToHex(g)}${componentToHex(b)}`;

          if (colors[hex] !== undefined) {
            colors[hex]++;
          } else {
            colors[hex] = 1;
          }
        }
      }

      resolve(take(sortBy(Object.keys(colors), hex => (-1) * colors[hex]), 10));
    } catch (e) {
      resolve(defaultHexs);
    }
  };

  imgEl.src = imgSrc;
});

export const isValidUrl = str => {
  const pattern = /^((http[s]*)?:\/\/)?((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|((\d{1,3}\.){3}\d{1,3}))(:\d+)?(\/[-a-z\d%_:.~+]*)*(\?[;&a-z\d%_.:~+=-]*)?(#[-a-z:\d_]*)?$/i;

  return pattern.test(str);
};

export const getImageDimensions = file => new Promise((resolved, rejected) => {
  const i = new Image();

  i.onload = () => {
    resolved({ width: i.width, height: i.height });
  };
  i.src = file;
});
