import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  root: {
    color: colors.primary[500],
    maxHeight: "400px",
    overflow: "auto",
    border: `solid 1px ${colors.neutral[300]}`,
    borderRadius: "5px",
    "&::-webkit-scrollbar": {
      width: "12px",
      backgroundColor: "#fff",
    },
    "&::-webkit-scrollbar-track": { backgroundColor: "#fff" },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: colors.neutral[300],
      borderRadius: "16px",
      border: "4px solid #fff",
    },
  },
  colorBlue: { backgroundColor: colors.primary[100] },
  iconTimes: { "& path": { fill: colors.primary[100] } },
  iconPlus: { "& path": { fill: colors.primary[500] } },
  buttonPlus: { backgroundColor: colors.primary[300] },
  buttonTimes: { backgroundColor: colors.primary[500] },
  checkBox: { ...spacing["pl-4"] },
  firstCheckBox: { ...spacing["pt-3"], ...spacing["pl-4"] },
  divider: { ...spacing["px-3"] },
});

export default makeStyles(styles);
