/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Multiselect from "./index";

export default {
  title: "Multiselect",
  component: Multiselect,
};

const Template = args => {
  const films = [
    { id: "0", label: "The Shawshank Redemption" },
    { id: "1", label: "The Godfather" },
    { id: "2", label: "The Godfather: Part II" },
    { id: "3", label: "The Dark Knight" },
    { id: "4", label: "12 Angry Men" },
    { id: "5", label: "Schindler's List" },
    { id: "6", label: "Pulp Fiction" },
    { id: "7", label: "The Lord of the Rings: The Return of the King" },
    { id: "8", label: "The Good, the Bad and the Ugly" },
  ];

  const [checked, setState] = React.useState(films.map(() => (false)));

  const handleChange = id => (setState(prevState => ({ ...prevState, [id]: !checked[id] })));

  return (
    <Multiselect {...args} options={films} onChange={handleChange} checked={checked} />
  );
};

export const SimpleMultiselect = Template.bind({});
SimpleMultiselect.args = {};
