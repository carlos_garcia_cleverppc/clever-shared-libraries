/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import List from "../List";
import ListItem from "../List/Item";
import Divider from "../Divider";
import Grid from "../Grid";
import Checkbox from "../Checkbox";
import Typography from "../Typography";

import getClasses from "./styles";

const MultiSelect = ({ className, options, onChange, checked, disabled }) => {
  const classes = getClasses();

  return (
    <List component="nav" aria-label="main mailbox folders" className={classes.root}>
      {options.map((item, index) => (
        <ListItem key={item.id} className={classnames(classes.list, className)}>
          <Grid container>
            <Grid item className={index === 0 ? classes.firstCheckBox : classes.checkBox}>
              <Checkbox
                checked={checked[item.id]}
                onChange={() => onChange(item.id)}
                color="primary"
                disabled={disabled}
              >
                <Typography shade={900}>{item.label}</Typography>
              </Checkbox>
            </Grid>

            <Grid item xs={12} className={classes.divider}>
              <Divider />
            </Grid>
          </Grid>
        </ListItem>
      ))}
    </List>
  );
};

MultiSelect.propTypes = {
  className: PropTypes.string,
  options: PropTypes.instanceOf(Object).isRequired,
  checked: PropTypes.instanceOf(Object).isRequired,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
};

MultiSelect.defaultProps = {
  className: "",
  onChange: () => { },
  disabled: false,
};

export default MultiSelect;
