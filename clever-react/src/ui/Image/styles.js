import isString from "lodash-es/isString";

import makeStyles from "../styles/makeStyles";

function buildImgSize(size) {
  if (isString(size) && size.indexOf("x") !== -1) {
    const [width, height] = size.split("x");

    return {
      width: `${width}px`,
      height: `${height}px`,
    };
  } if (size === "responsive") {
    return {
      height: "auto",
      width: "100%",
    };
  } if (size === "auto") {
    return {
      height: "auto",
      width: "auto",
    };
  }
  return {
    height: `${size}px`,
    width: `${size}px`,
  };
}

export default makeStyles(({ cleverUI: { spacing } }) => ({
  root: ({ size }) => ({ ...buildImgSize(size) }),
  circle: {
    display: "block",
    borderRadius: "50%",
    backgroundColor: "#fff",
    verticalAlign: "baseline",
  },
  center: { ...spacing["mx-auto"], display: "block" },
}));
