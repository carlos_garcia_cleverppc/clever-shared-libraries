/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import Image from "./index";

export default {
  title: "Image",
  component: Image,
};

const Template = args => <Image {...args} />;

export const SimpleImage = Template.bind({});
SimpleImage.args = {
  src: "https://cdn.jpegmini.com/user/images/slider_puffin_jpegmini_mobile.jpg",
  variant: "circle",
  size: "100x100",
};
