/* eslint-disable react/destructuring-assignment */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import getClasses from "./styles";

const Image = ({ className, src, variant, alt, center, size }) => {
  const classes = getClasses({ size });

  return (
    <img
      className={classnames(
        className,
        classes.root,
        classes[variant],
        center ? classes.center : "",
      )}
      src={src}
      alt={alt}
    />
  );
};

Image.propTypes = {
  className: PropTypes.string,
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
  center: PropTypes.bool,
  variant: PropTypes.oneOf(["default", "circle"]),
  size: (props, propName, componentName) => {
    if (props[propName] !== "responsive" && props[propName] !== "auto" && !/^\d+$/.test(props[propName]) && !/^\d+x\d+/.test(props[propName])) {
      return new Error(`Invalid prop \`${propName}\` supplied to ${componentName}. Validation failed.`);
    }
    return null;
  },
};

Image.defaultProps = {
  className: "",
  variant: "default",
  size: "responsive",
  alt: "",
  center: true,
};

export default Image;
