/* eslint-disable react/require-default-props */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import MaterialButton from "@material-ui/core/Button";

import { getColorsLabels } from "../styles/colors";

import ArrowRight from "../Icon/ArrowRight";
import Grid from "../Grid";
import Loading from "./Loading";
import getClasses from "./styles";

const colorsList = getColorsLabels(true);

const Button = React.forwardRef((props, ref) => {
  const {
    children, className, color, variant, size, disabledClickable, disabled, isLoading, ...buttonProps
  } = props;
  const classes = getClasses(props);

  const buttonClassName = classnames(className, classes.baseStyles, {
    [classes.outlined]: variant === "secondary",
    [classes.contained]: variant === "primary",
    [classes.medium]: size === "medium",
    [classes.large]: size === "large",
    [classes.small]: size === "small",
    [classes.disabled]: disabled || disabledClickable,
  });

  const hasShortContent = typeof children === "string" && children.length <= 10;

  return (
    <MaterialButton
      className={buttonClassName}
      disabled={disabled}
      ref={ref}
      {...buttonProps}
    >
      {size === "large" && (
        <Grid container alignItems="center">
          <Grid item xs>{children}</Grid>

          {isLoading ? (
            <Loading />
          ) : (
            <Grid item className={classes.iconContainer}>
              <ArrowRight color="currentColor" size={20} />
            </Grid>
          )}
        </Grid>
      )}

      {size !== "large" && (
        <>
          {children}

          {isLoading && <Loading absolute={hasShortContent} />}
        </>
      )}
    </MaterialButton>
  );
});

Button.propTypes = {
  variant: PropTypes.oneOf(["primary", "secondary"]),
  size: PropTypes.oneOf(["medium", "large", "small"]),
  disabled: PropTypes.bool,
  disabledClickable: PropTypes.bool,
  isLoading: PropTypes.bool,
  href: PropTypes.string,
  target: PropTypes.string,
  onClick: PropTypes.func,
  color: PropTypes.oneOf(colorsList),
  type: PropTypes.oneOf(["button", "reset", "submit"]),
};

Button.defaultProps = {
  variant: "primary",
  size: "medium",
  disabled: false,
  isLoading: false,
  color: "blue",
  type: "button",
};

export default Button;
