import React from "react";
import classnames from "classnames";

import LoadingIcon from "../Loading";
import makeStyles from "../styles/makeStyles";

const getClasses = makeStyles(({ cleverUI: { spacing } }) => ({
  container: {
    ...spacing["ml-3"],
  },
  absolute: {
    marginRight: "-25px",
  },
  loading: {
    margin: "0!important", // override default loading margin
  },
}));

const Loading = ({ absolute }) => {
  const classes = getClasses();

  const containerClassName = classnames(classes.container, {
    [classes.absolute]: absolute,
  });

  return (
    <div className={containerClassName}>
      <LoadingIcon size={14} className={classes.loading} color="currentColor" />
    </div>
  );
};

export default Loading;
