import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, typography, shadows }, breakpoints }) => ({
  baseStyles: ({ size, color }) => ({
    fontWeight: 700,
    fontFamily: typography.font1,
    borderRadius: "8px",
    boxShadow: "none",
    textAlign: "center",
    minWidth: size === "large" ? "256px" : "192px",
    "&:disabled": { color: colors[color][300] },
    [breakpoints.down("sm")]: { width: "100%" },
  }),
  large: {
    fontSize: typography.fontSizeBase.constant + typography.fontSizeBase.unit,
    minHeight: "48px",
    minWidth: "256px",
  },
  medium: {
    fontSize: typography.fontSizeBase.constant + typography.fontSizeBase.unit,
    minHeight: "48px",
    minWidth: "192px",
  },
  small: {
    fontSize: "14px",
    minHeight: "36px",
    minWidth: "fit-content",
    textTransform: "none",
  },
  contained: ({ color }) => ({
    backgroundColor: colors[color][500],
    color: colors[color][100],
    "&:hover": {
      color: "white",
      backgroundColor: colors[color][700],
      boxShadow: shadows.hover,
    },
    "&:focus": {
      color: "white",
      backgroundColor: colors[color][700],
      boxShadow: shadows.hover,
    },
  }),
  outlined: ({ color }) => ({
    color: colors[color][500],
    border: "solid",
    "&:hover": {
      color: colors[color][700],
      borderColor: colors[color][700],
      boxShadow: shadows.hover,
    },
    "&:focus": {
      color: colors[color][700],
      borderColor: colors[color][700],
      boxShadow: shadows.hover,
    },
  }),
  disabled: ({ color, variant }) => ({
    color: colors[color][300],
    backgroundColor: variant === "primary" ? colors[color][100] : null,
    borderColor: variant === "secondary" ? colors[color][100] : null,
  }),
  iconContainer: { display: "flex" },
});

export default makeStyles(styles);
