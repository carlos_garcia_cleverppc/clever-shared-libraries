/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import { getColorsLabels } from "../styles/colors";

import Button from "./index";

export default {
  title: "Button",
  component: Button,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => <Button {...args} />;

export const SimpleButton = Template.bind({});
SimpleButton.args = { children: "Primary" };

export const SecondaryButton = Template.bind({});
SecondaryButton.args = {
  children: "Secondary",
  variant: "secondary",
  color: "danger",
};

export const LoadingButton = Template.bind({});
LoadingButton.args = {
  children: "Loading",
  isLoading: true,
  disabled: true,
};
