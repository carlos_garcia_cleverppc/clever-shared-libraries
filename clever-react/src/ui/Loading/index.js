import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import MaterialCircular from "@material-ui/core/CircularProgress";

import { getColorsLabels } from "../styles/colors";

import Grid from "../Grid";
import getClasses from "./styles";

const colorsList = getColorsLabels(true);

const LoadingIcon = ({ color, className, ...props }) => {
  const classes = getClasses({ color });

  return <MaterialCircular className={classnames(className, classes.root)} {...props} />;
};

const Loading = ({ center, className, ...loadingIconProps }) => {
  if (center) {
    return (
      <Grid container justify="center" align="center" alignItems="center" className={className} style={{ height: "100%" }}>
        <Grid item xs={12}>
          <LoadingIcon {...loadingIconProps} />
        </Grid>
      </Grid>
    );
  }
  return (
    <LoadingIcon className={className} {...loadingIconProps} />
  );
};

Loading.propTypes = {
  size: PropTypes.number,
  color: PropTypes.oneOf([...colorsList, "currentColor"]),
  center: PropTypes.bool,
};

Loading.defaultProps = {
  size: 50,
  color: "primary",
  center: false,
};

export default Loading;
