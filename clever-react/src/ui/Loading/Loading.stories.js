/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import { getColorsLabels } from "../styles/colors";

import Loading from "./index";

export default {
  title: "Loading",
  component: Loading,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => <Loading {...args} />;

export const SimpleLoading = Template.bind({});
SimpleLoading.args = {};

export const CustomLoading = Template.bind({});
CustomLoading.args = { color: "danger", center: true, size: 100 };
