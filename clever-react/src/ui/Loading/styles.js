import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing } }) => ({
  root: {
    ...spacing["m-2"],
    color: ({ color }) => (colors[color] ? colors[color][500] : color),
  },
});

export default makeStyles(styles);
