import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import MaterialButton from "@material-ui/core/Button";

import getClasses from "./styles";

const ConnectButton = props => {
  const { children, className, size, onClick } = props;
  const { disabled, label, image } = props;

  const classes = getClasses();

  return (
    <MaterialButton
      className={classnames(classes.root, className)}
      size={size}
      onClick={onClick}
      disabled={disabled}
    >
      {size === "medium" && children}

      <div className={classes.logoContainer}>
        <img src={image} alt="Google Logo" className={classes.logo} />
      </div>

      <span className={classes.label}>{label}</span>
    </MaterialButton>
  );
};

ConnectButton.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(["medium", "large"]),
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

ConnectButton.defaultProps = {
  className: "",
  size: "medium",
  disabled: false,
  onClick: () => { },
};

export default ConnectButton;
