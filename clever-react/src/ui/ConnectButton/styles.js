import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors: { primary }, typography, spacing, shadows } }) => ({
  root: {
    height: "48px",
    width: "335px",
    fontSize: typography.fontSizeBase.constant + typography.fontSizeBase.unit,
    fontFamily: typography.font2,
    borderRadius: "8px",
    boxShadow: "none",
    textTransform: "none",
    color: "white",
    backgroundColor: "#4185f4",
    fontWeight: "700",
    "&:hover": { backgroundColor: primary[700], boxShadow: shadows.hover },
    "&:focus": { backgroundColor: primary[700], boxShadow: shadows.hover },
  },
  logo: {
    height: "20px",
    width: "20px",
  },
  logoContainer: {
    ...spacing["p-2"],
    backgroundColor: "white",
    height: "20px",
  },
  label: { ...spacing["mx-6"] },
});

export default makeStyles(styles);
