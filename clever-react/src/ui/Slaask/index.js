/* eslint-disable react/no-this-in-sfc */
/* eslint-disable no-empty */
/* eslint-disable no-undef */
/* eslint-disable react/no-multi-comp */

import React, { useEffect } from "react";
import PropTypes from "prop-types";

const Slaask = ({ params, token, identify }) => {
  useEffect(() => {
    const x = document.createElement("script");

    x.src = "https://cdn.slaask.com/chat.js";
    x.type = "text/javascript";
    x.async = "true";
    x.id = "slaask-script";

    x.onreadystatechange = () => {
      const x1 = document.readyState;

      if (!x1 || x1 === "complete" || x1 === "loaded") {
        try {
          _slaask.identify(identify, {
            ...params,
            path: window.location.pathname,
          });

          _slaask.init(token);
        } catch (e) { }
      }
    };
    x.onload = x.onreadystatechange;

    setTimeout(() => {
      const t = document.getElementById("slaask-insert");

      try {
        if (t && t.parentNode && t.parentNode.insertBefore && !document.getElementById("slaask-script")) {
          t.parentNode.insertBefore(x, t);
        }
      } catch (e) { }
    }, 10000);
  }, [params, token, identify]);

  return (<div id="slaask-insert" />);
};

Slaask.propTypes = {
  identify: PropTypes.string.isRequired,
  token: PropTypes.string.isRequired,
  params: PropTypes.instanceOf(Object),
};

Slaask.defaultProps = { params: {} };

export default Slaask;
