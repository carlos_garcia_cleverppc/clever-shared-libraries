import React from "react";
import PropTypes from "prop-types";
import { DynamicModuleLoader } from "redux-dynamic-modules-react";

const ModuleLoader = ({ Component, config, componentProps }) => (
  <DynamicModuleLoader modules={[config]}>
    <Component {...componentProps} />
  </DynamicModuleLoader>
);

ModuleLoader.propTypes = {
  config: PropTypes.instanceOf(Object).isRequired,
  Component: PropTypes.oneOfType([
    PropTypes.instanceOf(Object),
    PropTypes.node, PropTypes.element,
  ]).isRequired,
  componentProps: PropTypes.instanceOf(Object),
};

ModuleLoader.defaultProps = { componentProps: {} };

export default ModuleLoader;
