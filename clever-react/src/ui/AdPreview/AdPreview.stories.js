import React from "react";

import AdPreview from "./index";
import Typography from "../Typography/index";

export default {
  title: "AdPreview",
  component: AdPreview,
};

const Template = args => (
  <>
    <AdPreview {...args} />

    <Typography>
      WARNING! Storybook does not load the roboto font, but it should work fine in your project
    </Typography>
  </>
);

export const StandardAdPreview = Template.bind({});
StandardAdPreview.args = {
  url: "www.mitienda.com",
  name: "Mi tienda",
  headlines: ["Headline1", "Headline2", "Headline3", "Headline4"],
  descriptions: ["Description1", "Description2", "Description3", "Description4"],
  services: {
    customerService: { name: "24-7 customer service", value: "", selected: true },
    easyReturns: { name: "Easy returns", value: "", selected: true },
    freeShipping: { name: "Free shipping over", value: "50", selected: true },
  },
  usePhone: true,
  phoneText: "Call +34 952845778",
  currencySymbol: "$",
};

export const EmptyAdPreview = Template.bind({});
EmptyAdPreview.args = {
  url: "www.mitienda.com",
  name: "Mi tienda",
  headlinesDefault: "Your default headline",
  descriptionsDefault: "Your default description",
  servicesDefault: "Your default service",
};
