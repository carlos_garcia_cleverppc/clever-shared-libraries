/* eslint-disable react/jsx-one-expression-per-line */
/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import capitalize from "lodash-es/capitalize";
import sampleSize from "lodash-es/sampleSize";

import Card from "../Card";
import Divider from "../Divider";
import Grid from "../Grid";
import Typography from "../Typography";

import getClasses from "./styles";

const AdPreview = props => {
  const classes = getClasses();

  const { adWord, url, name, usePhone, phoneText, currencySymbol } = props;
  const { headlines, descriptions, services } = props;
  const { headlinesDefault, descriptionsDefault, servicesDefault } = props;

  const selectedServices = Object.keys(services);
  const endService = " · ";

  return (
    <Card>
      <Grid container className={classes.root}>
        <Grid item xs={12} className={classnames(classes.sameLine, classes.adDomain)}>
          <Typography shade={900} variant="body1" weight="bold" className={classes.roboto}>
            {adWord} ·<span>&nbsp;</span>
          </Typography>

          <Typography shade={900} variant="body1" className={classnames(classes.roboto, classes.break, classes.adDomain)}>
            {url}
          </Typography>
        </Grid>

        <Grid item xs={12}>
          {(headlines.length > 0) && (
            <Typography variant="body3" className={classnames(classes.roboto, classes.headlinesMargin, classes.blueGoogle)}>
              {name}

              {(headlines.length > 0) && headlines.slice(0, 2).map((headline, index) => {
                const aux = index < Math.max(headlines.length - 1, 2) ? " | " : "";
                return `${aux}${headline}`;
              })}
            </Typography>
          )}

          {(headlines.length === 0) && (
            <Typography variant="body3" className={classnames(classes.roboto, classes.headlinesMargin, classes.blueGoogle)}>
              {`${name} | ${headlinesDefault}`}
            </Typography>
          )}
        </Grid>

        <Grid item xs={12}>
          {(descriptions.length > 0) && (
            <Typography variant="body1" shade={900} className={classes.roboto}>
              {descriptions.slice(0, 3).map(description => `${description}. `)}
            </Typography>
          )}

          {(descriptions.length === 0) && (
            <Typography variant="body1" shade={900} className={classes.roboto}>{descriptionsDefault}</Typography>
          )}
        </Grid>

        <Grid item>
          {selectedServices.length !== 0 && (
            <Grid container className={classes.services}>
              {sampleSize(selectedServices, 3).map((service, index) => (
                <Grid item key={`service-${service}-view-xs`}>
                  <Typography className={classes.servicesSpacing}>
                    {capitalize(services[service].name)}
                    {service === "freeShipping" ? ` ${services[service].value}${currencySymbol}` : ""}
                    {(index !== 2 && index !== selectedServices.length - 1) ? endService : ""}
                  </Typography>
                </Grid>
              ))}
            </Grid>
          )}

          {selectedServices.length === 0 && !!servicesDefault && (
            <Grid container className={classnames(classes.emptyServices, classes.roboto)}>
              <Grid item>
                <Typography variant="body1" className={classnames(classes.roboto, classes.blueGoogle)}>
                  {servicesDefault}
                </Typography>
              </Grid>
            </Grid>
          )}
        </Grid>

        {usePhone && (
          <>
            <Divider />

            <Grid item xs={12}>
              <Typography variant="body1" shade={900} className={classes.roboto}>{phoneText}</Typography>
            </Grid>
          </>
        )}
      </Grid>
    </Card>
  );
};

AdPreview.propTypes = {
  adWord: PropTypes.string,
  url: PropTypes.string,
  name: PropTypes.string,
  headlines: PropTypes.instanceOf(Object),
  headlinesDefault: PropTypes.string,
  descriptions: PropTypes.instanceOf(Object),
  descriptionsDefault: PropTypes.string,
  services: PropTypes.instanceOf(Object),
  servicesDefault: PropTypes.string,
  usePhone: PropTypes.bool,
  phoneText: PropTypes.string,
  currencySymbol: PropTypes.string,
};

AdPreview.defaultProps = {
  adWord: "Ad",
  url: "",
  name: "",
  headlines: [],
  headlinesDefault: "",
  descriptions: [],
  descriptionsDefault: "",
  services: [],
  servicesDefault: "",
  usePhone: false,
  phoneText: "",
  currencySymbol: "",
};

export default AdPreview;
