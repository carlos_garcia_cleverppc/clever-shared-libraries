import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { spacing, colors } }) => ({
  root: { ...spacing["p-5"] },
  adDomain: {
    overflow: "hidden",
    textOverflow: "ellipsis",
    wrap: "nowrap",
    whiteSpace: "nowrap",
    "& p:first-child": { minWidth: "fit-content" },
    "& p": { display: "inline" },
  },
  break: {
    wordBreak: "break-all",
    overflow: "hidden",
    textOverflow: "ellipsis",
    display: "-webkit-box",
    "-webkit-line-clamp": 1,
    "-webkit-box-orient": "vertical",
  },
  sameLine: { display: "inline-flex" },
  headlinesMargin: { ...spacing["my-2"] },
  services: { ...spacing["mt-3"] },
  customServices: { display: "flex" },
  servicesSpacing: { ...spacing["mr-1"] },
  emptyServices: {
    border: `0.5px solid ${colors.neutral[300]}`,
    ...spacing["p-2"],
    ...spacing["mt-3"],
  },
  roboto: {
    fontFamily: "Roboto",
    overflow: "hidden",
    textOverflow: "ellipsis",
  },
  blueGoogle: { color: "#1a0dab" },
  fadeEnter: { opacity: 0.01 },
  fadEnterFadeEnterActive: {
    opacity: 1,
    transition: "opacity 1300ms ease-in",
  },
});

export default makeStyles(styles);
