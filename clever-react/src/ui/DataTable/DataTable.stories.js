/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/no-multi-comp */
import React from "react";

import DataTable from "./index";

export default {
  title: "DataTable",
  component: DataTable,
};

const headers = [
  {
    Header: "First Name",
    accessor: "firstName",
    hideSort: true, // hides de sorting icon
    fixedWidth: "30vw",
  },
  {
    Header: "Age",
    accessor: "age",
  },
  {
    Header: "Gender",
    accessor: "gender",
  },
  {
    Header: "Grade",
    accessor: "grade",
  },
];

const data = [
  {
    firstName: "Alice",
    age: 0.3,
    gender: "F",
    grade: 4,
  },
  {
    firstName: "Mike",
    age: 0.04,
    gender: "M",
    grade: 1,
  },
  {
    firstName: "John",
    age: 1,
    gender: "M",
    grade: 4,
  },
  {
    firstName: "Joe",
    age: 0.5,
    gender: "M",
    grade: 6,
  },
  {
    firstName: "Linda",
    age: 0.1,
    gender: "F",
    grade: 5,
  },
  {
    firstName: "Noah",
    age: 0.03,
    gender: "M",
    grade: 3,
  },
  {
    firstName: "Emma",
    age: 7,
    gender: "F",
    grade: 3,
  },
  {
    firstName: "James",
    age: 10,
    gender: "M",
    grade: 5,
  },
  {
    firstName: "Mia",
    age: 8,
    gender: "F",
    grade: 5,
  },
  {
    firstName: "William",
    age: 11,
    gender: "M",
    grade: 6,
  },
  {
    firstName: "Alice",
    age: 9,
    gender: "F",
    grade: 4,
  },
  {
    firstName: "Mike",
    age: 5,
    gender: "M",
    grade: 1,
  },
  {
    firstName: "aaaaaaJohn",
    age: 8,
    gender: "M",
    grade: 4,
  },
  {
    firstName: "aaaaaaJoe",
    age: 11,
    gender: "M",
    grade: 6,
  },
  {
    firstName: "aaaaaaLinda",
    age: 8,
    gender: "F",
    grade: 5,
  },
  {
    firstName: "aaaaaaNoah",
    age: 9,
    gender: "M",
    grade: 3,
  },
  {
    firstName: "aaaaaaEmma",
    age: 7,
    gender: "F",
    grade: 3,
  },
  {
    firstName: "aaaaaaJames",
    age: 10,
    gender: "M",
    grade: 5,
  },
];

const Template = args => <DataTable {...args} />;

export const BasicTable = Template.bind({});
BasicTable.args = {
  headers,
  data,
  createRow: { callback: () => { }, label: "Add person" },
};

export const WithSelection = Template.bind({});
WithSelection.args = {
  headers,
  data,
  selection: true,
};

const TemplateSort = args => {
  const [dataSplice, setDataSplice] = React.useState(false);

  const dataMemo = React.useMemo(() => (
    dataSplice ? args.data : args.data.slice(0, 5)
  ), [args.data, dataSplice]);

  return (
    <>
      <button type="button" onClick={() => setDataSplice(state => !state)}>ChangeData</button>
      <DataTable {...args} data={dataMemo} />
    </>
  );
};

export const AutoResetSortByFalse = TemplateSort.bind({});
AutoResetSortByFalse.args = {
  headers,
  data,
  resetAutoSortBy: false,
};

const filterFunction = rows => rows.filter(row => row.values.grade > 4);

const TemplateFilter = args => {
  const [filter, setFilter] = React.useState(false);

  return (
    <>
      <button type="button" onClick={() => { setFilter(!filter); }}>
        Filter: Grade &gt; 4
        {" "}
        {filter ? "ON" : "OFF"}
      </button>
      <DataTable {...args} data={args.data} rowFilterCallback={filter ? filterFunction : null} />
    </>
  );
};

export const WithSimpleFilter = TemplateFilter.bind({});
WithSimpleFilter.args = {
  headers,
  data,
  rowFilterCallback: filterFunction,
};

export const WithSimpleFilterAndPaginationOff = TemplateFilter.bind({});
WithSimpleFilterAndPaginationOff.args = {
  headers,
  data,
  rowFilterCallback: filterFunction,
  pagination: false,
};

// Interactive elements

const interactiveHeaders = [
  {
    Header: "First Name",
    accessor: "firstName",
  },
  {
    Header: "button",
    accessor: "button",
    custom: true,
  },
  {
    Header: "a",
    accessor: "a",
    custom: true,
  },
  {
    Header: "input",
    accessor: "input",
    custom: true,
  },
];

const interactiveData = [
  {
    firstname: "first",
    button: "button",
    a: "a",
    input: "input",
  },
];

const printInteractiveBody = (_value, key) => {
  switch (key) {
    case "button":
      return <button type="button" onClick={() => alert("button clicked")}>Button</button>;
    case "a":
      return <a href="http://cleverads.com" target="_blank" rel="noreferrer">Link</a>;
    case "input":
      return <input type="text" />;
    default:
      // do thing
  }
};

export const SelectionWithInteractiveElements = Template.bind({});
SelectionWithInteractiveElements.args = {
  headers: interactiveHeaders,
  data: interactiveData,
  selection: true,
  printBody: printInteractiveBody,
};
