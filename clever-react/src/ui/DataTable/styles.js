import makeStyles from "../styles/makeStyles";
import { cssStringHex2rgba } from "../styles/colors";

const styles = ({ cleverUI: { colors, spacing, typography } }) => ({
  paper: { borderRadius: "0", borderBottom: `1px solid ${colors.neutral[300]}` },
  table: {
    borderCollapse: "collapse",
    // backgroundColor: "white",
  },
  row: {
    "&:nth-of-type(odd)": { backgroundColor: cssStringHex2rgba("#F0F7F7") },
    height: "36px",
  },
  rowHover: { "&:hover": { backgroundColor: cssStringHex2rgba(colors.neutral[100]) } },
  header: {
    fontWeight: "bold",
    textTransform: "uppercase",
    backgroundColor: colors.primary[500],
    fontFamily: typography.font1,
    fontSize: "12px",
    color: colors.primary[100],
    "& tr": {
      "& th": {
        ...spacing["pt-2"],
        ...spacing["pb-2"],
        ...spacing["px-5"],
      },
    },
  },
  label: { color: `${colors.primary[500]} !important` },
  cell: {
    "& p": {
      // color: colors.neutral[700],
      fontFamily: typography.font1,
      fontSize: "12px",
      fontWeight: "600",
    },
  },
  headerSelectionIcon: { color: colors.primary[100] },
  selectionIcon: { color: colors.primary[500] },
});

export default makeStyles(styles);
