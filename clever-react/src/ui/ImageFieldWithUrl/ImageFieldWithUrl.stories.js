/* eslint-disable no-console */
import React, { useState } from "react";

import ImageFieldWithUrl from "./index";

export default {
  title: "ImageFieldWithUrl",
  component: ImageFieldWithUrl,
};

const Template = args => {
  const [value, setValue] = useState("");
  const onChange = image => setValue(image);

  return (
    <ImageFieldWithUrl
      {...args}
      value={value}
      onChange={onChange}
    />
  );
};

export const BaseImageFieldWithUrl = Template.bind({});
BaseImageFieldWithUrl.args = {
  onSubmitUrl: console.log,
  onError: console.log,
};
