import React, { useRef } from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import IconButton from "../IconButton";
import Upload from "../Icon/Upload";

import ImageField from "../ImageField";
import TextFieldCollapsed from "../TextFieldCollapsed";
import getClasses from "./styles";

const ImageFieldWithUrl = ({ onSubmitUrl, className, ...props }) => {
  const classes = getClasses();
  const inputRef = useRef();

  const openImageField = () => inputRef.current.click();

  return (
    <div className={cx(classes.root, className)}>
      <ImageField ref={inputRef} {...props} />

      <div className={classes.actions}>
        <IconButton
          outlined={false}
          color="primary"
          onClick={openImageField}
          icon={<Upload />}
        />

        <TextFieldCollapsed onSubmit={onSubmitUrl} />
      </div>
    </div>
  );
};

export default ImageFieldWithUrl;

ImageFieldWithUrl.propTypes = {
  onSubmitUrl: PropTypes.func.isRequired,
  onChange: PropTypes.func,
  value: PropTypes.string,
  size: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  mbLimit: PropTypes.number,
  onError: PropTypes.func,
  /**
   * You can insert one of de values
   * or an Array of some of de values showed before Array[]
   * @default *
   */
  accept: PropTypes.oneOfType([
    PropTypes.oneOf(["*", "jpg", "jpeg", "png"]),
    PropTypes.arrayOf(["*", "jpg", "jpeg", "png"]),
  ]),
  label: PropTypes.string,
};
