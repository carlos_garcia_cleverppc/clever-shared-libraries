import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import MaterialTooltip from "@material-ui/core/Tooltip";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

import getClasses from "./styles";
import Grid from "../Grid";
import Typography from "../Typography";
import Circle from "../Icon/Circle";

import { onKeyDown } from "../../utils/helpers";
import useMediaQuery from "../styles/useMediaQuery";
import { screenSm, screenXs } from "../styles/breakpoints";
import { getColorsLabels } from "../styles/colors";

const colorsList = getColorsLabels();

const Tooltip = props => {
  const [openState, setOpenState] = useState(false);

  const { children, title, placement, type, color } = props;
  const { icon, description, className, spacing, clickable, textAlign } = props;

  const classes = getClasses({ color, textAlign });

  const isScreenSm = useMediaQuery(screenSm);
  const isScreenXs = useMediaQuery(screenXs);

  const aux = (
    <Grid container className={classes.card}>
      <Grid item xs={1}>
        {icon}
      </Grid>

      <Grid item xs={11}>
        <Typography weight="bold" variant="body2" className={classes.text}>{title}</Typography>
      </Grid>

      <Grid item xs={12}>
        <Typography variant="body1">{description}</Typography>
      </Grid>
    </Grid>
  );

  const handleTooltipClose = () => {
    setOpenState(false);
  };

  const handleTooltipOpen = () => {
    setOpenState(true);
  };

  const getSpacing = () => {
    switch (spacing) {
      case 1:
        return classes.tooltipSpacingSmall;
      case 2:
        return classes.tooltipSpacingMed;
      case 3:
        return classes.tooltipSpacingLarge;
      default:
        return "";
    }
  };

  if (isScreenSm || isScreenXs || clickable) {
    return (
      <ClickAwayListener onClickAway={handleTooltipClose}>
        <MaterialTooltip
          title={type === "simple" ? title : aux}
          classes={{ tooltip: classnames(classes.tooltip, getSpacing(), color ? classes.tooltipDiv : ""), arrow: classes.arrow }}
          placement={placement}
          arrow
          className={className}
          onClose={handleTooltipClose}
          open={openState}
          interactive
          disableFocusListener
          disableHoverListener
          disableTouchListener
        >
          <span className={classes.root} onClick={handleTooltipOpen} role="button" tabIndex={0} onKeyDown={onKeyDown(handleTooltipOpen)}>
            {children}
          </span>
        </MaterialTooltip>
      </ClickAwayListener>
    );
  }
  return (
    <MaterialTooltip
      title={type === "simple" ? title : aux}
      classes={{ tooltip: classnames(classes.tooltip, getSpacing()), arrow: classes.arrow }}
      placement={placement}
      arrow
      className={className}
      interactive
    >
      <span className={classes.root}>
        {children}
      </span>
    </MaterialTooltip>
  );
};

Tooltip.propTypes = {
  children: PropTypes.instanceOf(Object).isRequired,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Object)]).isRequired,
  placement: PropTypes.string,
  type: PropTypes.oneOf(["simple", "complete"]),
  icon: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Object)]),
  description: PropTypes.string,
  className: PropTypes.string,
  spacing: PropTypes.oneOf([0, 1, 2, 3]),
  clickable: PropTypes.bool,
  color: PropTypes.oneOf(colorsList),
  textAlign: PropTypes.oneOf(["left", "center", "right"]),
};

Tooltip.defaultProps = {
  placement: "top",
  type: "simple",
  icon: <Circle />,
  description: "",
  className: "",
  spacing: 1,
  clickable: false,
  color: "navyBlue",
  textAlign: "center",
};

export default Tooltip;
