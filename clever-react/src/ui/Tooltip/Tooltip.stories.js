/* eslint-disable react/jsx-props-no-spreading */
import React from "react";

import { getColorsLabels } from "../styles/colors";

import Tooltip from "./index";

import Typography from "../Typography";
import Button from "../Button";

export default {
  title: "Tooltip",
  component: Tooltip,
  argTypes: {
    color: {
      options: getColorsLabels(),
      control: {
        type: "select",
      },
    },
  },
};

const Template = args => <Tooltip {...args} />;

export const SimpleTooltip = Template.bind({});
SimpleTooltip.args = {
  title: "Simple tooltip",
  children: <div>Simple tooltip</div>,
};

export const InteractuableTooltip = Template.bind({});
InteractuableTooltip.args = {
  title: (
    <>
      <Typography>
        Interactuable tooltip
      </Typography>

      <Button onClick={() => { }}>Click</Button>
    </>
  ),
  color: "neutral",
  interactive: true,
  children: <div>Interactuable tooltip</div>,
};
