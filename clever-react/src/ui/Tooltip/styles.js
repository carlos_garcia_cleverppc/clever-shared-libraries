import makeStyles from "../styles/makeStyles";

const styles = ({ cleverUI: { colors, spacing, typography } }) => ({
  tooltip: ({ color, textAlign, space }) => ({
    ...spacing[`p-${space + 1}`],
    fontSize: "12px",
    fontFamily: typography.font1,
    backgroundColor: colors[color][500],
    maxWidth: "350px",
    borderRadius: "8px",
    textAlign,
    width: "100%",
    whiteSpace: "pre-line",
    display: "inline-block",
    zIndex: "100000",
  }),
  root: { display: "inline-block", width: "fit-content", outline: "none" },
  arrow: ({ color }) => ({ color: colors[color][500] }),
  list: {
    backgroundColor: colors.neutral[300],
    padding: "1rem",
  },
  text: { color: "white", ...spacing["ml-3"], fontFamily: typography.font1 },
  card: { display: "flex" },
  icon: { ...spacing["mt-1"] },
  inline: { display: "inline" },
  tooltipSpacingSmall: { ...spacing["p-2"] },
  tooltipSpacingMed: { ...spacing["p-3"] },
  tooltipSpacingLarge: { ...spacing["p-4"] },
  tooltipDiv: ({ color }) => ({ backgroundColor: colors[color][500] }),
});

export default makeStyles(styles);
