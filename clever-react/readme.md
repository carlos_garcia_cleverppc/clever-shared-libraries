# How to release a new version

1. Merge develop branch into master
2. Run deploy script depending on the version type
	- **Minor:**  `npm run deploy:minor` 
	- **Patch:** `npm run deploy:patch`
	- **Major:** `npm run deploy:major` 
3. To publish in NPM run `npm publish`  (Remember: your user must have permissions)
