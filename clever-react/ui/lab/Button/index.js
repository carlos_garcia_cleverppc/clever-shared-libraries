import _extends from "@babel/runtime/helpers/extends";
import _defineProperty from "@babel/runtime/helpers/defineProperty";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["children", "className", "variant", "size"];

/* eslint-disable react/require-default-props */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Handle from "./Handle";
import getClasses from "./styles";
var Button = /*#__PURE__*/React.forwardRef(function (props, ref) {
  var _classnames;

  var children = props.children,
      className = props.className,
      variant = props.variant,
      size = props.size,
      buttonProps = _objectWithoutProperties(props, _excluded);

  var classes = getClasses(props);
  var buttonClassName = classnames(className, classes.root, (_classnames = {}, _defineProperty(_classnames, classes.outlined, variant === "secondary"), _defineProperty(_classnames, classes.contained, variant === "primary"), _defineProperty(_classnames, classes.medium, size === "medium"), _defineProperty(_classnames, classes.large, size === "large"), _defineProperty(_classnames, classes.small, size === "small"), _classnames));
  return /*#__PURE__*/React.createElement(Handle, _extends({
    className: buttonClassName,
    ref: ref
  }, buttonProps), children);
});
Button.propTypes = {
  variant: PropTypes.oneOf(["primary", "secondary"]),
  size: PropTypes.oneOf(["medium", "large", "small"]),
  disabled: PropTypes.bool,
  disabledClickable: PropTypes.bool,
  isLoading: PropTypes.bool,
  href: PropTypes.string,
  target: PropTypes.string,
  onClick: PropTypes.func,
  type: PropTypes.oneOf(["button", "reset", "submit"])
};
Button.defaultProps = {
  variant: "primary",
  size: "medium",
  type: "button"
};
export default Button;