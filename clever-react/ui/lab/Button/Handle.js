import _extends from "@babel/runtime/helpers/extends";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["href", "children", "target", "className"];
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import makeStyles from "../../styles/makeStyles";
var getClasses = makeStyles(function () {
  return {
    link: {
      textDecoration: "none",
      color: "currentColor"
    },
    button: {
      border: 0,
      "&:not(:disabled)": {
        cursor: "pointer"
      }
    }
  };
});

var Handle = function Handle(_ref) {
  var href = _ref.href,
      children = _ref.children,
      target = _ref.target,
      className = _ref.className,
      attrs = _objectWithoutProperties(_ref, _excluded);

  var defaultRel = target === "_blank" ? "noopener" : undefined;
  var rel = attrs.rel || defaultRel;
  var classes = getClasses();

  if (href) {
    return /*#__PURE__*/React.createElement("a", _extends({}, attrs, {
      className: classnames([className, classes.link]),
      href: href,
      target: target,
      rel: rel
    }), children);
  }

  return /*#__PURE__*/React.createElement("button", _extends({}, attrs, {
    type: "button",
    className: classnames([className, classes.button])
  }), children);
};

Handle.propTypes = {
  href: PropTypes.string,
  target: PropTypes.string,
  rel: PropTypes.string
};
export default Handle;