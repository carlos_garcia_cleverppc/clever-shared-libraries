import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      typography = _ref$cleverUI.typography,
      spaces = _ref$cleverUI.spaces;
  return {
    root: _objectSpread(_objectSpread({}, typography.variants.inputText), {}, {
      display: "inline-flex",
      placeItems: "center",
      minHeight: 48,
      padding: "".concat(spaces.xs, "px ").concat(spaces.sm, "px"),
      borderRadius: 8
    }),
    large: {},
    medium: {},
    small: {},
    contained: {
      backgroundColor: colors.primary[500],
      color: colors.primary[100],
      "&:hover": {
        color: colors.white[500],
        backgroundColor: colors.primary[700]
      },
      "&:focus": {
        color: colors.white[500],
        backgroundColor: colors.primary[700]
      },
      "&:disabled": {
        color: colors.primary[300],
        backgroundColor: colors.primary[100]
      }
    },
    outlined: {
      color: colors.primary[500],
      border: "2px solid",
      borderColor: colors.primary[700],
      "&:hover": {
        color: colors.primary[700]
      },
      "&:focus": {
        color: colors.primary[700]
      },
      "&:disabled": {
        color: colors.primary[300],
        backgroundColor: colors.primary[100],
        borderColor: colors.primary[100]
      }
    }
  };
};

export default makeStyles(styles);