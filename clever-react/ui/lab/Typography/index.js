import _defineProperty from "@babel/runtime/helpers/defineProperty";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["upperCase", "downCase", "capitalize", "align", "color", "shade", "variant", "children", "className", "weight", "gutterBottom", "component"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import { getColorsLabels, getShades } from "../../styles/colors";
import getClasses from "./styles";
var colorsList = getColorsLabels(true);
var shadesList = getShades();
var availableVariantTags = {
  h1: "h1",
  h2: "h2",
  h3: "h3",
  h4: "h4",
  h5: "h5",
  h6: "h6",
  span: "span"
};

var getTagFromVariant = function getTagFromVariant(variant) {
  var name = variant.toLocaleLowerCase();
  return availableVariantTags[name] || "p";
};

var Typography = /*#__PURE__*/React.forwardRef(function (props, ref) {
  var _utilities;

  var upperCase = props.upperCase,
      downCase = props.downCase,
      capitalize = props.capitalize,
      align = props.align,
      color = props.color,
      shade = props.shade,
      variant = props.variant,
      children = props.children,
      className = props.className,
      weight = props.weight,
      gutterBottom = props.gutterBottom,
      component = props.component,
      otherProps = _objectWithoutProperties(props, _excluded);

  var classes = getClasses({
    variant: variant,
    align: align,
    color: color,
    shade: shade
  });
  var utilities = (_utilities = {}, _defineProperty(_utilities, classes.upperCase, upperCase), _defineProperty(_utilities, classes.downCase, downCase), _defineProperty(_utilities, classes.capitalize, capitalize), _defineProperty(_utilities, classes.mb, gutterBottom), _defineProperty(_utilities, classes.light, weight === "thin"), _defineProperty(_utilities, classes.normal, weight === "normal"), _defineProperty(_utilities, classes.bold, weight === "bold"), _utilities);
  var classNames = cx(className, classes.root, utilities);
  var tagName = component || getTagFromVariant(variant);
  return /*#__PURE__*/React.createElement(tagName, _objectSpread({
    className: classNames,
    ref: ref
  }, otherProps), children);
});
Typography.propTypes = {
  variant: PropTypes.string,
  className: PropTypes.string,
  gutterBottom: PropTypes.bool,
  weight: PropTypes.oneOf(["thin", "normal", "bold"]),
  color: PropTypes.oneOf(colorsList),
  shade: PropTypes.oneOf(shadesList),
  align: PropTypes.oneOf(["left", "right", "center", "justify"]),
  upperCase: PropTypes.bool,
  downCase: PropTypes.bool,
  capitalize: PropTypes.bool,
  component: PropTypes.node
};
Typography.defaultProps = {
  variant: "paragraph",
  gutterBottom: false,
  color: "neutral",
  shade: 500,
  upperCase: false,
  downCase: false,
  capitalize: false
};
export default Typography;