import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      typography = _ref$cleverUI.typography,
      spaces = _ref$cleverUI.spaces;
  return {
    root: function root(_ref2) {
      var color = _ref2.color,
          shade = _ref2.shade,
          align = _ref2.align,
          variant = _ref2.variant;
      return _objectSpread({
        color: colors[color] ? colors[color][shade] : color,
        lineHeight: typography.lineHeight,
        textAlign: align
      }, typography.variants[variant]);
    },
    mb: {
      marginBottom: spaces.sm
    },
    upperCase: {
      textTransform: "uppercase!important"
    },
    downCase: {
      textTransform: "lowercase!important"
    },
    capitalize: {
      textTransform: "capitalize!important"
    },
    light: {
      fontWeight: "300!important"
    },
    normal: {
      fontWeight: "400!important"
    },
    bold: {
      fontWeight: "700!important"
    }
  };
};

export default makeStyles(styles);