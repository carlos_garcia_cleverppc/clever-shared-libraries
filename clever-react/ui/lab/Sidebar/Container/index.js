import _slicedToArray from "@babel/runtime/helpers/slicedToArray";

/* eslint-disable import/no-cycle */

/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import getClasses from "./styles";

var Container = function Container(props) {
  var isSidebarCollapsed = props.isSidebarCollapsed,
      platformLogo = props.platformLogo,
      body = props.body,
      footer = props.footer,
      className = props.className;

  var _useState = useState(""),
      _useState2 = _slicedToArray(_useState, 2),
      bodyHeight = _useState2[0],
      setBodyHeight = _useState2[1];

  var classes = getClasses({
    isSidebarCollapsed: isSidebarCollapsed,
    bodyHeight: bodyHeight
  });
  useEffect(function () {
    var logoElem = document.querySelector("#sidebar-logo");
    var rectLogo = logoElem ? logoElem.getBoundingClientRect().height : 0;
    var dividerElem = document.querySelector("#divider-footer");
    var dividerHeight = dividerElem ? 31 : 0; // Footer actions height

    var actionsRightPosition = document.querySelector("#footer-actions-right");
    var actionsElem = document.querySelector("#footer-actions");
    var actionsCount = actionsElem ? actionsElem.children[0].children.length : 0; // 39.39: Action height

    var actionsHeight = isSidebarCollapsed ? actionsCount * 39.39 : 39.39; // New management footer

    var managementFooter = document.querySelector("#management-footer"); // If actions are at right, don't sum anything to height

    actionsHeight = actionsRightPosition ? 0 : actionsHeight; // 94: height is always the same when !!managementFooter
    // 36: padding top + bottom
    // 58: Avatar height

    var calcHeightFooter = managementFooter ? 94 : 36 + 58 + actionsHeight + dividerHeight;
    setBodyHeight("calc(100% - ".concat(rectLogo, "px - ").concat(calcHeightFooter, "px )"));
  }, [props]);
  return /*#__PURE__*/React.createElement("div", {
    className: classnames(className, classes.root, isSidebarCollapsed ? classes.rootCollapsed : "")
  }, !!platformLogo && /*#__PURE__*/React.createElement("div", {
    id: "sidebar-logo",
    className: classes.platformLogo
  }, platformLogo), /*#__PURE__*/React.createElement("div", {
    id: "sidebar-body",
    className: classnames(classes.body, isSidebarCollapsed ? "" : classes.bodyDesktop)
  }, body), /*#__PURE__*/React.createElement("div", {
    id: "sidebar-footer",
    className: isSidebarCollapsed ? classes.footerCollapsed : classes.footer
  }, footer));
};

Container.propTypes = {
  isSidebarCollapsed: PropTypes.bool,
  platformLogo: PropTypes.instanceOf(Object),
  body: PropTypes.instanceOf(Object),
  footer: PropTypes.instanceOf(Object),
  className: PropTypes.string
};
Container.defaultProps = {
  isSidebarCollapsed: false,
  platformLogo: null,
  body: null,
  footer: null,
  className: ""
};
export default Container;