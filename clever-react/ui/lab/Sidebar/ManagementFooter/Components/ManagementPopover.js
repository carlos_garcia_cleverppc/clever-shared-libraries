import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable jsx-a11y/anchor-is-valid */

/* eslint-disable import/no-cycle */
import React from "react";
import PropTypes from "prop-types";
import nextId from "react-id-generator";
import AlertCard from "../../../../AlertCard";
import Divider from "../../../../Divider";
import Grid from "../../../../Grid";
import Link from "../../../../Link";
import Popover from "./SidebarPopover";
import Icon from "../../../../Icon";
import Exit from "../../../../Icon/Exit";
import Megaphone from "../../../../Icon/Megaphone";
import MultipleAccounts from "./MultipleAccounts";
import SimpleAccount from "./SimpleAccount";
import Typography from "../../../../Typography";
import makeStyles from "../../../../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      spacing = _ref$cleverUI.spacing,
      shadows = _ref$cleverUI.shadows;
  return {
    mainContainer: _objectSpread(_objectSpread(_objectSpread({}, spacing["p-6"]), spacing["pb-4"]), {}, {
      backgroundColor: "white",
      borderRadius: "8px",
      boxShadow: shadows.normal,
      maxWidth: "375px"
    }),
    divider: _objectSpread(_objectSpread({}, spacing["px-2"]), spacing["pb-3"]),
    bottomSpace: _objectSpread({}, spacing["mb-1"]),
    icon: _objectSpread({}, spacing["mr-2"]),
    sameLine: {
      display: "inline-flex",
      alignItems: "center"
    },
    alert: _objectSpread({}, spacing["mb-5"]),
    signOut: _objectSpread(_objectSpread({
      textAlign: "right"
    }, spacing["px-2"]), spacing["my-2"]),
    accounts: {
      overflowY: "auto",
      overflowX: "hidden",
      maxHeight: "40vh",
      "&::-webkit-scrollbar": {
        width: "12px",
        backgroundColor: "#fff"
      },
      "&::-webkit-scrollbar-track": {
        backgroundColor: "#fff"
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#babac0",
        borderRadius: "16px",
        border: "4px solid #fff"
      }
    },
    accountsContainer: {
      maxWidth: "98%"
    }
  };
};

var getClasses = makeStyles(styles);

var ManagementPopover = function ManagementPopover(props) {
  var classes = getClasses();
  var allAccounts = props.allAccounts,
      allowMultipleAccounts = props.allowMultipleAccounts,
      accountsTitleCopy = props.accountsTitleCopy,
      actions = props.actions;
  var logOutCopy = props.logOutCopy,
      handleOnClickLogOut = props.handleOnClickLogOut,
      color = props.color,
      alert = props.alert;
  var anchorEl = props.anchorEl,
      setAnchorEl = props.setAnchorEl,
      open = props.open,
      setOpen = props.setOpen,
      popoverClassName = props.popoverClassName;
  var alertColor = Object.keys(alert).length > 0 && alert.color ? alert.color : "orange";
  return /*#__PURE__*/React.createElement(Popover, {
    placement: "top-center",
    open: open,
    anchorEl: anchorEl,
    onClose: function onClose() {
      setAnchorEl(null);
      setOpen(false);
    },
    arrow: true,
    arrowColor: "white",
    className: popoverClassName
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    className: classes.mainContainer,
    justify: "center"
  }, Object.keys(alert).length > 0 && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    className: classes.alert
  }, /*#__PURE__*/React.createElement(AlertCard, {
    text: alert.copyAlert,
    color: alertColor,
    linkText: alert.copyOnClick,
    action: alert.onClick
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.sameLine
  }, /*#__PURE__*/React.createElement(Megaphone, {
    color: color,
    className: classes.icon
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body0",
    weight: "bold",
    shade: 900
  }, accountsTitleCopy)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    className: classes.accounts
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.accountsContainer
  }, allAccounts.map(function (platform) {
    var htmlId = nextId();
    return allowMultipleAccounts ? /*#__PURE__*/React.createElement(MultipleAccounts, {
      data: Object.values(platform)[0],
      key: "management-popover-".concat(htmlId)
    }) : /*#__PURE__*/React.createElement(SimpleAccount, {
      data: Object.values(platform)[0],
      key: "management-popover-simple-".concat(htmlId)
    });
  })))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    className: classes.divider,
    xs: 12
  }, /*#__PURE__*/React.createElement(Divider, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true
  }, actions.map(function (action) {
    return /*#__PURE__*/React.createElement(Grid, {
      item: true,
      className: classes.bottomSpace,
      key: action.name,
      xs: 12
    }, /*#__PURE__*/React.createElement(Link, {
      onClick: action.onClick,
      color: "neutral",
      className: classes.sameLine
    }, /*#__PURE__*/React.createElement(Icon, {
      icon: action.icon,
      color: action.color ? action.color : color,
      className: classes.icon
    }), /*#__PURE__*/React.createElement(Typography, {
      variant: "body0",
      weight: "bold",
      shade: 900
    }, action.name)));
  }))), logOutCopy !== "" && /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    className: classes.divider,
    xs: 12
  }, /*#__PURE__*/React.createElement(Divider, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    className: classes.signOut,
    xs: 12
  }, /*#__PURE__*/React.createElement(Link, {
    onClick: handleOnClickLogOut,
    color: "neutral",
    className: classes.sameLine
  }, /*#__PURE__*/React.createElement(Exit, {
    color: "danger",
    className: classes.icon
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body0",
    weight: "bold",
    color: "neutral",
    shade: 900
  }, logOutCopy))))));
};

ManagementPopover.propTypes = {
  allAccounts: PropTypes.instanceOf(Array),
  allowMultipleAccounts: PropTypes.bool,
  accountsTitleCopy: PropTypes.string,
  actions: PropTypes.instanceOf(Object),
  logOutCopy: PropTypes.string,
  handleOnClickLogOut: PropTypes.instanceOf(Object),
  color: PropTypes.string,
  alert: PropTypes.instanceOf(Object),
  anchorEl: PropTypes.instanceOf(Object),
  setAnchorEl: PropTypes.instanceOf(Object),
  popoverClassName: PropTypes.instanceOf(Object)
};
ManagementPopover.defaultProps = {
  allAccounts: {},
  allowMultipleAccounts: true,
  accountsTitleCopy: "",
  actions: [],
  logOutCopy: "",
  handleOnClickLogOut: function handleOnClickLogOut() {},
  color: "primary",
  alert: {},
  anchorEl: null,
  setAnchorEl: function setAnchorEl() {},
  popoverClassName: {}
};
export default ManagementPopover;