import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import split from "lodash-es/split";
import makeStyles from "../../../../../styles/makeStyles";

var dimensions = function dimensions(anchorEl) {
  return anchorEl.getBoundingClientRect();
};
/**
 * Paper Styles
 */

/** Function to place the paper in X axis controlling it doesn't go outside screen */


var getPaperLeftPosition = function getPaperLeftPosition(anchorEl, paperWidth) {
  var result = dimensions(anchorEl).left + dimensions(anchorEl).width / 2 - paperWidth / 2;

  if (result > 16 && result + paperWidth < window.innerWidth - 16) {
    return result;
  }

  if (result < 16) return 16;
  if (result + paperWidth > window.innerWidth - 16) return window.innerWidth - paperWidth - 16;
  return result;
};
/** Function to move arrow up or down anchorEl element */


var getMaxHeight = function getMaxHeight(anchorEl, placement) {
  if (split(placement, "-")[0] === "top") return dimensions(anchorEl).top - 16 - 32;
  if (split(placement, "-")[0] === "center") return window.innerHeight - dimensions(anchorEl).top;
  return window.innerHeight - dimensions(anchorEl).top - 32 - 64;
};
/** Function to place popover paper */


var getPaperPosition = function getPaperPosition(placement, anchorEl, lockBodyScroll, paperWidth) {
  var place = {
    top: "",
    bottom: "",
    right: "",
    left: ""
  };

  if (split(placement, "-")[0] === "top") {
    return _objectSpread(_objectSpread({}, place), {}, {
      bottom: window.innerHeight - dimensions(anchorEl).bottom + dimensions(anchorEl).height + 33,
      left: getPaperLeftPosition(anchorEl, paperWidth)
    });
  }

  if (split(placement, "-")[0] === "bottom") {
    return _objectSpread(_objectSpread({}, place), {}, {
      top: dimensions(anchorEl).top + dimensions(anchorEl).height + 16 + 16,
      left: getPaperLeftPosition(anchorEl, paperWidth)
    });
  }

  if (split(placement, "-")[0] === "center") {
    var quantityTop = dimensions(anchorEl).top - getMaxHeight(anchorEl, placement) / 2 + dimensions(anchorEl).height / 2;

    var centerPlacement = _objectSpread(_objectSpread({}, place), {}, {
      top: quantityTop > 16 ? quantityTop : 16
    });

    if (split(placement, "-")[1] === "left") {
      var lockPos = !lockBodyScroll ? -16 : 0;
      /**
       * If popover cannot be painted left next to anchorEl
       * it will be right next to anchorEl
       */

      if (dimensions(anchorEl).left - 32 - paperWidth < 16) {
        return _objectSpread(_objectSpread({}, centerPlacement), {}, {
          left: dimensions(anchorEl).left + dimensions(anchorEl).width + 4 + 32
        });
      }

      return _objectSpread(_objectSpread({}, centerPlacement), {}, {
        right: window.innerWidth - dimensions(anchorEl).right - 4 + dimensions(anchorEl).width + 10 + 32 + lockPos
      });
    }

    if (split(placement, "-")[1] === "right") {
      /**
       * If popover cannot be painted right next to anchorEl
       * it will be left next to anchorEl
       */
      if (dimensions(anchorEl).left + dimensions(anchorEl).width + 4 + 32 + paperWidth > window.innerWidth - 16) {
        var _lockPos = !lockBodyScroll ? -16 : 0;

        return _objectSpread(_objectSpread({}, centerPlacement), {}, {
          right: window.innerWidth - dimensions(anchorEl).right - 4 + dimensions(anchorEl).width + 10 + 32 + _lockPos
        });
      }

      return _objectSpread(_objectSpread({}, centerPlacement), {}, {
        left: dimensions(anchorEl).left + dimensions(anchorEl).width + 4 + 32
      });
    }
  }

  return place;
};
/**
 * Arrow Styles
 */

/** Function to get border to paint */


var getArrowColor = function getArrowColor(placement, arrowColor, arrowShade, colors, anchorEl, paperWidth) {
  var color = colors[arrowColor][arrowShade];
  var borders = {
    borderTop: "",
    borderBottom: "",
    borderRight: "",
    borderLeft: ""
  };
  if (split(placement, "-")[0] === "top") return _objectSpread(_objectSpread({}, borders), {}, {
    borderTop: color
  });
  if (split(placement, "-")[0] === "bottom") return _objectSpread(_objectSpread({}, borders), {}, {
    borderBottom: color
  });
  /**
   * If popover cannot be painted right next to anchorEl
   * it will be left next to anchorEl
   */

  if (dimensions(anchorEl).left + dimensions(anchorEl).width + 4 + 32 + paperWidth > window.innerWidth - 16) {
    return _objectSpread(_objectSpread({}, borders), {}, {
      borderLeft: color
    });
  }
  /**
   * If popover cannot be painted left next to anchorEl
   * it will be right next to anchorEl
   */


  if (dimensions(anchorEl).left - 32 - paperWidth < 16) {
    return _objectSpread(_objectSpread({}, borders), {}, {
      borderRight: color
    });
  }

  if (split(placement, "-")[1] === "left") return _objectSpread(_objectSpread({}, borders), {}, {
    borderLeft: color
  });
  if (split(placement, "-")[1] === "right") return _objectSpread(_objectSpread({}, borders), {}, {
    borderRight: color
  });
};
/** Function to move arrow up or down anchorEl element */


var arrowTopPlacement = function arrowTopPlacement(anchorEl, placement) {
  var res = window.innerHeight - dimensions(anchorEl).bottom;
  var top = split(placement, "-")[0] === "top";
  var bottom = split(placement, "-")[0] === "bottom";
  if (top) res += dimensions(anchorEl).height + 1;
  if (bottom) res += -32;
  return res;
};
/** Function to place the arrow in X axis */


var getArrowLeftPlacement = function getArrowLeftPlacement(placement, anchorEl) {
  var result = -16;
  var axisX = split(placement, "-")[1];
  if (axisX === "left") return result + 8;
  if (axisX === "right") return result + dimensions(anchorEl).width - 8;
  return result + dimensions(anchorEl).width / 2;
};
/** Function to place Arrow paper */


var getArrowPosition = function getArrowPosition(placement, anchorEl, lockBodyScroll, paperWidth) {
  var place = {
    top: "",
    bottom: "",
    right: "",
    left: ""
  };

  if (split(placement, "-")[0] !== "center") {
    return _objectSpread(_objectSpread({}, place), {}, {
      bottom: arrowTopPlacement(anchorEl, placement, lockBodyScroll),
      left: dimensions(anchorEl).left + getArrowLeftPlacement(placement, anchorEl)
    });
  }

  if (split(placement, "-")[0] === "center") {
    if (split(placement, "-")[1] === "left") {
      var lockPos = !lockBodyScroll ? -16 : 0;
      /**
       * If popover cannot be painted left next to anchorEl
       * it will be right next to anchorEl
       */

      if (dimensions(anchorEl).left - 32 - paperWidth < 16) {
        return _objectSpread(_objectSpread({}, place), {}, {
          top: dimensions(anchorEl).top - 16 + dimensions(anchorEl).height / 2,
          left: dimensions(anchorEl).left + dimensions(anchorEl).width + 4
        });
      }

      return _objectSpread(_objectSpread({}, place), {}, {
        top: dimensions(anchorEl).top - 16 + dimensions(anchorEl).height / 2,
        right: window.innerWidth - dimensions(anchorEl).right - 4 + dimensions(anchorEl).width + 10 + lockPos
      });
    }

    if (split(placement, "-")[1] === "right") {
      /**
       * If popover cannot be painted right next to anchorEl
       * it will be left next to anchorEl
       */
      if (dimensions(anchorEl).left + dimensions(anchorEl).width + 4 + 32 + paperWidth > window.innerWidth - 16) {
        var _lockPos2 = !lockBodyScroll ? -16 : 0;

        return _objectSpread(_objectSpread({}, place), {}, {
          top: dimensions(anchorEl).top - 16 + dimensions(anchorEl).height / 2,
          right: window.innerWidth - dimensions(anchorEl).right - 4 + dimensions(anchorEl).width + 10 + _lockPos2
        });
      }

      return _objectSpread(_objectSpread({}, place), {}, {
        top: dimensions(anchorEl).top - 16 + dimensions(anchorEl).height / 2,
        left: dimensions(anchorEl).left + dimensions(anchorEl).width + 4
      });
    }
  }

  return place;
};

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      shadows = _ref$cleverUI.shadows;
  return {
    popover: function popover(_ref2) {
      var anchorEl = _ref2.anchorEl,
          open = _ref2.open,
          paperWidth = _ref2.paperWidth,
          placement = _ref2.placement,
          lockBodyScroll = _ref2.lockBodyScroll,
          zIndex = _ref2.zIndex;
      return {
        backgroundColor: "white",
        borderRadius: "8px",
        position: "fixed",
        boxShadow: shadows.normal,
        overflow: "auto",
        zIndex: zIndex,
        width: paperWidth,
        left: anchorEl ? getPaperPosition(placement, anchorEl, lockBodyScroll, paperWidth).left : "",
        bottom: anchorEl ? getPaperPosition(placement, anchorEl, lockBodyScroll, paperWidth).bottom : "",
        top: anchorEl ? getPaperPosition(placement, anchorEl, lockBodyScroll, paperWidth).top : "",
        right: anchorEl ? getPaperPosition(placement, anchorEl, lockBodyScroll, paperWidth).right : "",
        maxHeight: anchorEl ? getMaxHeight(anchorEl, placement) : "",
        opacity: "".concat(open ? "1" : "0"),
        transition: "opacity 300ms cubic-bezier(.4,1.39,.93,1.01)",
        "&::-webkit-scrollbar": {
          width: "12px",
          backgroundColor: "#fff"
        },
        "&::-webkit-scrollbar-track": {
          backgroundColor: "#fff"
        },
        "&::-webkit-scrollbar-thumb": {
          backgroundColor: "#babac0",
          borderRadius: "16px",
          border: "4px solid #fff"
        }
      };
    },
    portal: function portal(_ref3) {
      var open = _ref3.open,
          zIndex = _ref3.zIndex;
      return {
        position: "fixed",
        top: "0px",
        left: "0",
        width: "100%",
        height: "100%",
        zIndex: zIndex - 1,
        display: "".concat(!open ? "none" : "")
      };
    },
    popoverContainer: function popoverContainer(_ref4) {
      var anchorEl = _ref4.anchorEl,
          open = _ref4.open,
          placement = _ref4.placement,
          arrowColor = _ref4.arrowColor,
          arrowShade = _ref4.arrowShade,
          lockBodyScroll = _ref4.lockBodyScroll,
          paperWidth = _ref4.paperWidth,
          zIndex = _ref4.zIndex;
      return {
        "&:before": {
          content: "''",
          position: "fixed",
          border: "solid 16px transparent",
          zIndex: zIndex + 1,
          left: anchorEl ? getArrowPosition(placement, anchorEl, lockBodyScroll, paperWidth, lockBodyScroll).left : "",
          right: anchorEl ? getArrowPosition(placement, anchorEl, lockBodyScroll, paperWidth, lockBodyScroll).right : "",
          bottom: anchorEl ? getArrowPosition(placement, anchorEl, lockBodyScroll, paperWidth, lockBodyScroll).bottom : "",
          top: anchorEl ? getArrowPosition(placement, anchorEl, lockBodyScroll, paperWidth, lockBodyScroll).top : "",
          borderTopColor: anchorEl ? getArrowColor(placement, arrowColor, arrowShade, colors, anchorEl, paperWidth).borderTop : "",
          borderBottomColor: anchorEl ? getArrowColor(placement, arrowColor, arrowShade, colors, anchorEl, paperWidth).borderBottom : "",
          borderRightColor: anchorEl ? getArrowColor(placement, arrowColor, arrowShade, colors, anchorEl, paperWidth).borderRight : "",
          borderLeftColor: anchorEl ? getArrowColor(placement, arrowColor, arrowShade, colors, anchorEl, paperWidth).borderLeft : "",
          opacity: "".concat(open ? "1" : "0"),
          transition: "opacity 300ms cubic-bezier(.4,1.39,.93,1.01)"
        }
      };
    }
  };
};

export default makeStyles(styles);