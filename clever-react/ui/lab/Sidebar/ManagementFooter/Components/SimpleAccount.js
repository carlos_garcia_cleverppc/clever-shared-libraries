import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable react/no-unused-prop-types */
import React, { useState } from "react";
import PropTypes from "prop-types";
import AlertCard from "../../../../AlertCard";
import BrandIcon from "../../../../BrandIcon";
import IconButton from "../../../../IconButton";
import Grid from "../../../../Grid";
import Tooltip from "../../../../Tooltip";
import Typography from "../../../../Typography";
import CirclePlus from "../../../../Icon/CirclePlus";
import Exit from "../../../../Icon/Exit";
import Refresh from "../../../../Icon/Refresh";
import Warning from "../../../../Icon/Warning";
import makeStyles from "../../../../styles/makeStyles";

var styles = function styles(_ref) {
  var spacing = _ref.cleverUI.spacing;
  return {
    root: function root(_ref2) {
      var cursor = _ref2.cursor;
      return _objectSpread(_objectSpread(_objectSpread({}, spacing["p-1"]), spacing["m-1"]), {}, {
        display: "inline-flex",
        alignItems: "center",
        outline: "none",
        cursor: "".concat(cursor),
        marginLeft: "auto"
      });
    },
    platformLogo: {
      height: "32px",
      width: "32px"
    },
    opacity: {
      opacity: "50%"
    },
    name: {
      display: "inline-flex",
      alignItems: "center"
    },
    accountName: {
      wordBreak: "break-all",
      overflow: "hidden",
      textOverflow: "ellipsis",
      display: "-webkit-box",
      "-webkit-line-clamp": 1,
      "-webkit-box-orient": "vertical"
    },
    sameLine: {
      display: "inline-flex"
    },
    alert: _objectSpread({}, spacing["mt-3"])
  };
};

var getClasses = makeStyles(styles);

var SimpleAccount = function SimpleAccount(props) {
  var classes = getClasses();
  var data = props.data;
  var name = data.name,
      accounts = data.accounts,
      addAccount = data.addAccount;
  var account = accounts.length === 0 ? null : accounts[0];
  var alert = account && account.alert ? account.alert : null;
  var refresh = account && account.refresh ? account.refresh : null;
  var logout = account && account.logout ? account.logout : null;
  var alertColor = alert && alert.color ? alert.color : "orange";
  var refreshColor = refresh && refresh.color ? refresh.color : "secondary";
  var logoutColor = logout && logout.color ? logout.color : "danger";
  var addAccountColor = addAccount && addAccount.color ? addAccount.color : "secondary";

  var _useState = useState(false),
      _useState2 = _slicedToArray(_useState, 2),
      showAlert = _useState2[0],
      setShowAlert = _useState2[1];

  var getChannelIcon = function getChannelIcon() {
    var auxName = name.toLowerCase();
    var result = "";

    if (auxName.includes("google ads")) {
      result = "GoogleAdsCircle";
    } else if (auxName.includes("google analytics")) {
      result = "GoogleAnalyticsCircle";
    } else if (auxName.includes("google merchant")) {
      result = "GoogleMerchantCircle";
    } else if (auxName.includes("facebook")) {
      result = "FacebookCircle";
    } else if (auxName.includes("twitter")) {
      result = "TwitterCircle";
    } else if (auxName.includes("microsoft")) {
      result = "MicrosoftCircle";
    } else if (auxName.includes("linkedin")) {
      result = "LinkedinCircle";
    } else if (auxName.includes("instagram")) {
      result = "InstagramCircle";
    }

    return result;
  };

  var channelIcon = getChannelIcon();
  return /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center",
    className: classes.root
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 2,
    className: classes.platformLogo
  }, /*#__PURE__*/React.createElement(BrandIcon, {
    icon: channelIcon,
    size: 32,
    className: !account ? classes.opacity : ""
  })), account && /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 10,
    className: classes.sameLine
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    className: classes.name
  }, /*#__PURE__*/React.createElement(Typography, {
    weight: "bold",
    variant: "f1-12",
    shade: 900,
    className: classes.accountName
  }, account.name)), !!account.id && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "f2-10"
  }, account.id))), !!alert && /*#__PURE__*/React.createElement(IconButton, {
    icon: /*#__PURE__*/React.createElement(Warning, null),
    color: alertColor,
    active: true,
    onClick: function onClick() {
      return setShowAlert(!showAlert);
    },
    hover: "zoomIn"
  }), !!refresh && /*#__PURE__*/React.createElement(Tooltip, {
    title: refresh.copy
  }, /*#__PURE__*/React.createElement(IconButton, {
    icon: /*#__PURE__*/React.createElement(Refresh, null),
    color: refreshColor,
    onClick: function onClick() {
      return refresh.onClick();
    },
    hover: "zoomIn"
  })), !!logout && /*#__PURE__*/React.createElement(Tooltip, {
    title: logout.copy
  }, /*#__PURE__*/React.createElement(IconButton, {
    icon: /*#__PURE__*/React.createElement(Exit, null),
    color: logoutColor,
    onClick: function onClick() {
      return logout.onClick();
    },
    hover: "zoomIn"
  }))), !!alert && showAlert && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    className: classes.alert
  }, /*#__PURE__*/React.createElement(AlertCard, {
    text: alert.copyAlert,
    color: alertColor,
    linkText: alert.copyOnClick,
    action: alert.onClick
  }))), !account && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 10
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 10
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body0",
    shade: 900
  }, addAccount.copy)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 2
  }, /*#__PURE__*/React.createElement(IconButton, {
    icon: /*#__PURE__*/React.createElement(CirclePlus, null),
    active: true,
    color: addAccountColor,
    hover: "zoomIn",
    onClick: addAccount.onClick
  })))));
};

SimpleAccount.propTypes = {
  data: PropTypes.instanceOf(Object).isRequired
};
export default SimpleAccount;