import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Typography from "../../../Typography";
import makeStyles from "../../../../styles/makeStyles";
import { getColorsLabels } from "../../../../styles/colors";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    defaultAvatar: function defaultAvatar(_ref2) {
      var size = _ref2.size,
          color = _ref2.color;
      return {
        backgroundColor: colors[color][500],
        width: size,
        height: size,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: "50%",
        "& svg": {
          width: "62%",
          height: "62%"
        }
      };
    },
    root: function root(_ref3) {
      var size = _ref3.size;
      return {
        width: size,
        height: size,
        minWidth: size,
        position: "relative"
      };
    },
    avatarWrapper: {
      width: "100%",
      height: "100%",
      display: "grid",
      borderRadius: "50%",
      "& > *": {
        gridArea: "1/-1",
        borderRadius: "inherit"
      }
    },
    circle: function circle(_ref4) {
      var color = _ref4.color;
      return {
        boxShadow: "inset 0 0 0 3px ".concat(colors[color][500])
      };
    },
    label: function label(_ref5) {
      var color = _ref5.color,
          size = _ref5.size;
      return _objectSpread(_objectSpread({
        position: "absolute",
        bottom: "-9px",
        // Middle of label height
        right: size / 2,
        transform: "translateX(50%)",
        whiteSpace: "nowrap"
      }, spacing["p-1"]), {}, {
        backgroundColor: colors[color][100],
        borderRadius: "8px"
      });
    }
  };
};

var getClasses = makeStyles(styles);
var colorsList = getColorsLabels(true);

var DefaultAvatar = function DefaultAvatar(props) {
  var className = props.className,
      size = props.size,
      name = props.name,
      color = props.color,
      label = props.label,
      colorCircle = props.colorCircle;
  var classes = getClasses({
    size: size,
    color: color
  });
  return /*#__PURE__*/React.createElement("div", {
    className: classnames(classes.root, className)
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.avatarWrapper
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.defaultAvatar
  }, /*#__PURE__*/React.createElement(Typography, {
    color: "white",
    weight: "bold",
    variant: "h3"
  }, name)), colorCircle && /*#__PURE__*/React.createElement("div", {
    className: classes.circle
  })), label && /*#__PURE__*/React.createElement(Typography, {
    color: color,
    weight: "bold",
    variant: "f1-8",
    className: classes.label
  }, label));
};

DefaultAvatar.propTypes = {
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  name: PropTypes.string,
  label: PropTypes.string,
  colorCircle: PropTypes.bool,
  color: PropTypes.oneOf(colorsList)
};
DefaultAvatar.defaultProps = {
  size: 42,
  name: "CL",
  label: "",
  colorCircle: false,
  color: "primary"
};
export default DefaultAvatar;