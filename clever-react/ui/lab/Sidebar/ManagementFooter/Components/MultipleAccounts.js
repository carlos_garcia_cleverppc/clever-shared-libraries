import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable react/no-unused-prop-types */
import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import BrandIcon from "../../../../BrandIcon";
import IconButton from "../../../../IconButton";
import Grid from "../../../../Grid";
import Switch from "../../../../Switch";
import Typography from "../../../../Typography";
import ChevronRight from "../../../../Icon/ChevronRight";
import PlusRound from "../../../../Icon/PlusRound";
import makeStyles from "../../../../styles/makeStyles";

var styles = function styles(_ref) {
  var spacing = _ref.cleverUI.spacing;
  return {
    root: function root(_ref2) {
      var cursor = _ref2.cursor;
      return _objectSpread(_objectSpread(_objectSpread({}, spacing["p-1"]), spacing["m-1"]), {}, {
        display: "inline-flex",
        alignItems: "center",
        outline: "none",
        cursor: "".concat(cursor),
        marginLeft: "auto"
      });
    },
    platformLogo: {
      height: "32px",
      width: "32px"
    },
    name: {
      display: "inline-flex",
      alignItems: "center"
    },
    accountName: _objectSpread(_objectSpread({}, spacing["mb-1"]), {}, {
      wordBreak: "break-all",
      overflow: "hidden",
      textOverflow: "ellipsis",
      display: "-webkit-box",
      "-webkit-line-clamp": 1,
      "-webkit-box-orient": "vertical"
    }),
    chevron: {
      padding: 0
    },
    sameLine: {
      display: "inline-flex"
    },
    alert: _objectSpread({}, spacing["mt-3"]),
    arrowPosition: {
      transform: "rotate(90deg)"
    },
    "switch": _objectSpread({}, spacing["ml-2"]),
    bottomSpace: _objectSpread({}, spacing["mb-2"])
  };
};

var getClasses = makeStyles(styles);

var MultipleAccounts = function MultipleAccounts(props) {
  var classes = getClasses();
  var data = props.data;
  var name = data.name,
      accounts = data.accounts,
      addAccount = data.addAccount;
  var addAccountColor = addAccount && addAccount.color ? addAccount.color : "primary";
  var hasAccounts = accounts.length > 0;

  var _useState = useState(false),
      _useState2 = _slicedToArray(_useState, 2),
      openDropdown = _useState2[0],
      setOpenDropdown = _useState2[1];

  var getChannelIcon = function getChannelIcon() {
    var auxName = name.toLowerCase();
    var result = "";

    if (auxName.includes("google ads")) {
      result = "GoogleAdsCircle";
    } else if (auxName.includes("google analytics")) {
      result = "GoogleAnalyticsCircle";
    } else if (auxName.includes("google merchant")) {
      result = "GoogleMerchantCircle";
    } else if (auxName.includes("facebook")) {
      result = "FacebookCircle";
    } else if (auxName.includes("twitter")) {
      result = "TwitterCircle";
    } else if (auxName.includes("microsoft")) {
      result = "MicrosoftCircle";
    } else if (auxName.includes("linkedin")) {
      result = "LinkedinCircle";
    } else if (auxName.includes("instagram")) {
      result = "InstagramCircle";
    }

    return result;
  };

  var channelIcon = getChannelIcon();
  return /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center",
    className: classes.root
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 2,
    className: classes.platformLogo
  }, /*#__PURE__*/React.createElement(BrandIcon, {
    icon: channelIcon,
    size: 32
  })), hasAccounts && /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 10
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 9,
    className: classes.name
  }, /*#__PURE__*/React.createElement(Typography, {
    weight: "bold",
    variant: "f1-12",
    shade: 900,
    className: classes.accountName
  }, name, " (", accounts.length, ")")), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 3,
    className: classes.sameLine
  }, /*#__PURE__*/React.createElement(IconButton, {
    icon: /*#__PURE__*/React.createElement(PlusRound, null),
    active: true,
    color: addAccountColor,
    hover: "zoomIn",
    onClick: addAccount.onClick
  }), /*#__PURE__*/React.createElement(IconButton, {
    icon: /*#__PURE__*/React.createElement(ChevronRight, null),
    active: true,
    color: "neutral",
    shade: 900,
    onClick: function onClick() {
      return setOpenDropdown(!openDropdown);
    },
    className: classnames(classes.chevron, openDropdown ? classes.arrowPosition : "")
  })))), openDropdown && accounts.map(function (account) {
    return /*#__PURE__*/React.createElement(Grid, {
      item: true,
      xs: 12,
      className: classes.bottomSpace
    }, /*#__PURE__*/React.createElement(Grid, {
      container: true
    }, /*#__PURE__*/React.createElement(Grid, {
      item: true,
      xs: 2
    }), /*#__PURE__*/React.createElement(Grid, {
      item: true,
      xs: 6
    }, /*#__PURE__*/React.createElement(Grid, {
      container: true
    }, /*#__PURE__*/React.createElement(Grid, {
      item: true,
      xs: 12
    }, /*#__PURE__*/React.createElement(Typography, {
      weight: "bold",
      variant: "f1-12",
      shade: 900,
      className: classes.accountName
    }, account.name)), !!account.id && /*#__PURE__*/React.createElement(Grid, {
      item: true,
      xs: 12
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "f2-10"
    }, "(", account.id, ")")))), /*#__PURE__*/React.createElement(Grid, {
      item: true,
      xs: 4
    }, /*#__PURE__*/React.createElement(Switch, {
      checked: account.active,
      onChange: account.setActive,
      size: "small",
      className: classes["switch"]
    }))));
  })), !hasAccounts && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 10
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 9
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body0",
    shade: 900
  }, name, " (", accounts.length, ")")), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 3
  }, /*#__PURE__*/React.createElement(IconButton, {
    icon: /*#__PURE__*/React.createElement(PlusRound, null),
    active: true,
    color: addAccountColor,
    hover: "zoomIn",
    onClick: addAccount.onClick
  })))));
};

MultipleAccounts.propTypes = {
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};
MultipleAccounts.defaultProps = {
  size: 42
};
export default MultipleAccounts;