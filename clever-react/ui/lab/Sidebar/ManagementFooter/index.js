import _slicedToArray from "@babel/runtime/helpers/slicedToArray";

/* eslint-disable no-extra-boolean-cast */
import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import last from "lodash-es/last";
import Avatar from "../../../Avatar";
import Badge from "../../../Badge";
import Grid from "../../../Grid";
import Typography from "../../Typography";
import ChevronRight from "../../../Icon/ChevronRight";
import DefaultAvatar from "./Components/DefaultAvatar";
import ManagementPopper from "./Components/ManagementPopover";
import ProgressAvatar from "./Components/ProgressAvatar";
import { onKeyDown } from "../../../../utils/helpers";
import getClasses from "./styles";

var ManagementFooter = function ManagementFooter(props) {
  // IF label !== "" THEN isPremium
  var avatarSrc = props.avatarSrc,
      avatarDefaultName = props.avatarDefaultName,
      accountName = props.accountName,
      color = props.color,
      label = props.label;
  var isSidebarCollapsed = props.isSidebarCollapsed,
      actions = props.actions,
      logoutCopy = props.logoutCopy,
      handleOnClickLogOut = props.handleOnClickLogOut;
  var allAccounts = props.allAccounts,
      allowMultipleAccounts = props.allowMultipleAccounts,
      accountsTitleCopy = props.accountsTitleCopy,
      alert = props.alert,
      popoverClassName = props.popoverClassName;
  var showProgress = props.showProgress,
      onClickProgress = props.onClickProgress,
      percentageDone = props.percentageDone,
      percentageCopy = props.percentageCopy;
  var classes = getClasses({
    color: color,
    isSidebarCollapsed: isSidebarCollapsed
  });

  var _useState = useState(false),
      _useState2 = _slicedToArray(_useState, 2),
      openSelector = _useState2[0],
      setOpenSelector = _useState2[1];

  var _useState3 = useState(null),
      _useState4 = _slicedToArray(_useState3, 2),
      anchorEl = _useState4[0],
      setAnchorEl = _useState4[1];

  var handleClick = function handleClick(event) {
    setAnchorEl(last(event.currentTarget.getElementsByTagName("svg")));
    setOpenSelector(!openSelector);
  };

  var printAvatar = function printAvatar() {
    /* If label !== "" image will be displayed with a circle */

    /* If label !== "" it will be displayed here when isSidebarCollapsed */
    var result = null;

    if (avatarSrc !== "") {
      result = /*#__PURE__*/React.createElement(Avatar, {
        src: avatarSrc,
        size: 42,
        label: isSidebarCollapsed ? label : "",
        colorCircle: !showProgress && label !== "",
        className: !isSidebarCollapsed ? classes.accountInfo : "",
        color: color,
        positionLabel: "bottom-center"
      });
    } else {
      result = /*#__PURE__*/React.createElement(DefaultAvatar, {
        name: avatarDefaultName,
        size: 42,
        label: isSidebarCollapsed ? label : "",
        colorCircle: !showProgress && label !== "",
        className: !isSidebarCollapsed ? classes.accountInfo : "",
        color: color
      });
    }

    if (showProgress) {
      result = /*#__PURE__*/React.createElement(ProgressAvatar, {
        onClickProgress: onClickProgress,
        percentageDone: percentageDone,
        percentageCopy: percentageCopy
      }, result);
    }

    if (Object.keys(alert).length > 0) {
      result = /*#__PURE__*/React.createElement(Badge, {
        color: alert.color,
        variant: "dot",
        className: classes.badge
      }, result);
    }

    return result;
  };

  return /*#__PURE__*/React.createElement("div", {
    className: classes.root,
    id: "management-footer",
    "aria-describedby": "management-popover"
  }, printAvatar(), /*#__PURE__*/React.createElement("div", {
    className: classes.rootClickable,
    onClick: handleClick,
    onKeyDown: onKeyDown(handleClick),
    role: "button",
    tabIndex: 0
  }, !isSidebarCollapsed && /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    className: classes.name
  }, /*#__PURE__*/React.createElement(Typography, {
    weight: "bold",
    variant: "f1-14",
    className: classes.accountName
  }, accountName), label !== "" && /*#__PURE__*/React.createElement(Typography, {
    color: color,
    weight: "bold",
    variant: "f1-8",
    className: classes.label
  }, label))), /*#__PURE__*/React.createElement(ChevronRight, {
    className: classnames(classes.arrow, !!anchorEl ? classes.arrowPosition : ""),
    size: 24
  })), /*#__PURE__*/React.createElement(ManagementPopper, {
    color: color,
    accountsTitleCopy: accountsTitleCopy,
    allAccounts: allAccounts,
    logOutCopy: logoutCopy,
    handleOnClickLogOut: handleOnClickLogOut,
    actions: actions,
    alert: alert,
    open: !!anchorEl,
    setOpen: setOpenSelector,
    anchorEl: anchorEl,
    setAnchorEl: setAnchorEl,
    allowMultipleAccounts: allowMultipleAccounts,
    popoverClassName: popoverClassName
  }));
};

ManagementFooter.propTypes = {
  avatarSrc: PropTypes.string,
  avatarDefaultName: PropTypes.string,
  accountName: PropTypes.string,
  color: PropTypes.string,
  label: PropTypes.string,
  isSidebarCollapsed: PropTypes.bool,
  actions: PropTypes.instanceOf(Object),
  logoutCopy: PropTypes.string,
  handleOnClickLogOut: PropTypes.instanceOf(Object),
  allAccounts: PropTypes.instanceOf(Object),
  alert: PropTypes.instanceOf(Object),
  allowMultipleAccounts: PropTypes.bool,
  popoverClassName: PropTypes.instanceOf(Object),
  showProgress: PropTypes.bool
};
ManagementFooter.defaultProps = {
  avatarSrc: "",
  avatarDefaultName: "CL",
  accountName: "My clever account",
  color: "primary",
  label: "",
  isSidebarCollapsed: false,
  actions: null,
  logoutCopy: "",
  handleOnClickLogOut: function handleOnClickLogOut() {},
  allAccounts: {},
  alert: {},
  allowMultipleAccounts: true,
  popoverClassName: {},
  showProgress: false
};
export default ManagementFooter;