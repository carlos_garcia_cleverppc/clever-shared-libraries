import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../../../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      spacing = _ref$cleverUI.spacing,
      colors = _ref$cleverUI.colors;
  return {
    /* Vertical color bar */
    selectedSection: function selectedSection(_ref2) {
      var color = _ref2.color;
      return {
        "&::before": {
          content: '"  "',
          backgroundColor: colors[color][500],
          height: "20px",
          width: "3px",
          top: "10px",
          left: "-4px",
          position: "absolute"
        }
      };
    },

    /* Hover shadow in item */
    selectItem: _objectSpread(_objectSpread(_objectSpread({}, spacing["p-0"]), {}, {
      maxWidth: "100%",
      borderRadius: "8px",
      position: "relative",
      width: "100%",
      appeareance: "none",
      border: "0"
    }, spacing["mb-1"]), {}, {
      textAlign: "left",
      lineHeight: "inherit"
    }),
    selectItemHover: {
      "&:hover, &:focus": {
        backgroundColor: "".concat(colors.neutral[100], " !important"),
        outline: "none"
      }
    },
    selectItemCollapsed: {
      "&:hover, &:focus": {
        backgroundColor: "transparent"
      }
    },
    iconButton: {
      "&:hover, &:focus": {
        backgroundColor: "".concat(colors.neutral[100], " !important")
      }
    },

    /* Item component not collapsed, more space for parents */
    iconOpen: _objectSpread(_objectSpread(_objectSpread({}, spacing["my-2"]), spacing["mt-2"]), {}, {
      paddingLeft: "9px",
      paddingRight: "9px"
    }),

    /* Item component not collapsed, less space for children */
    iconOpenChild: _objectSpread(_objectSpread(_objectSpread({}, spacing["my-1"]), spacing["mt-1"]), {}, {
      paddingLeft: "9px",
      paddingRight: "9px"
    }),
    arrow: {
      "& path": {
        fill: colors.neutral[800]
      },
      transform: "rotate(0deg)",
      transition: "transform .05s ease-in-out"
    },

    /* Chevron position if opened or closed */
    arrowPosition: {
      transform: "rotate(90deg)"
    },

    /* Label chip styles */
    label: function label(_ref3) {
      var color = _ref3.color;
      return _objectSpread(_objectSpread(_objectSpread({}, spacing["p-1"]), spacing["mx-2"]), {}, {
        backgroundColor: colors[color][100],
        width: "fit-content",
        borderRadius: "8px"
      });
    }
  };
};

export default makeStyles(styles);