import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Grid from "../../../Grid";
import Divider from "../../../Divider";
import getClasses from "./styles";

var Footer = function Footer(props) {
  var classes = getClasses();
  var userAccount = props.userAccount,
      footerActions = props.footerActions,
      actionsPosition = props.actionsPosition,
      divider = props.divider,
      isSidebarCollapsed = props.isSidebarCollapsed;
  var className = props.className;

  var _useState = useState(8),
      _useState2 = _slicedToArray(_useState, 2),
      itemAccountCols = _useState2[0],
      setItemAccountCols = _useState2[1];

  var _useState3 = useState(4),
      _useState4 = _slicedToArray(_useState3, 2),
      itemActionsCols = _useState4[0],
      setItemActionsCols = _useState4[1];

  useEffect(function () {
    var actionsElem = document.querySelector("#footer-actions");
    var actionsCount = actionsElem ? actionsElem.children[0].children.length : 0;

    if (actionsPosition === "right" && !isSidebarCollapsed) {
      if (actionsCount === 1) {
        setItemAccountCols(10);
        setItemActionsCols(2);
      }
    }
  }, [actionsPosition, isSidebarCollapsed]);
  return /*#__PURE__*/React.createElement(React.Fragment, null, (isSidebarCollapsed || actionsPosition === "top") && /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center",
    className: classnames(className, isSidebarCollapsed ? classes.center : "")
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    className: !isSidebarCollapsed ? classes.marginActions : ""
  }, footerActions), divider && !isSidebarCollapsed && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, /*#__PURE__*/React.createElement("div", {
    id: "divider-footer",
    className: classes.divider
  }, /*#__PURE__*/React.createElement(Divider, null))), /*#__PURE__*/React.createElement(Grid, {
    item: true
  }, userAccount)), !isSidebarCollapsed && actionsPosition === "right" && /*#__PURE__*/React.createElement("div", {
    id: "footer-actions-right"
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center",
    justify: "space-between",
    className: className
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: itemAccountCols
  }, userAccount), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: itemActionsCols
  }, footerActions))));
};

Footer.propTypes = {
  userAccount: PropTypes.instanceOf(Object),
  footerActions: PropTypes.instanceOf(Object),
  actionsPosition: PropTypes.oneOf(["top", "right"]),
  divider: PropTypes.bool,
  isSidebarCollapsed: PropTypes.bool,
  className: PropTypes.string
};
Footer.defaultProps = {
  userAccount: null,
  footerActions: null,
  actionsPosition: "right",
  divider: false,
  isSidebarCollapsed: false,
  className: ""
};
export default Footer;