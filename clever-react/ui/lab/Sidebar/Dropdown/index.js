import React from "react";
import PropTypes from "prop-types";
import List from "../../../List";
import getClasses from "./styles";

var SidebarDropdown = function SidebarDropdown(props) {
  var classes = getClasses();
  var itemsList = props.itemsList,
      isSidebarCollapsed = props.isSidebarCollapsed;
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(List, {
    component: "div",
    disablePadding: true,
    className: isSidebarCollapsed ? classes.dropdownListCollapsed : classes.dropdownList
  }, itemsList));
};

SidebarDropdown.propTypes = {
  itemsList: PropTypes.instanceOf(Array),
  isSidebarCollapsed: PropTypes.bool
};
SidebarDropdown.defaultProps = {
  itemsList: [],
  isSidebarCollapsed: false
};
export default SidebarDropdown;