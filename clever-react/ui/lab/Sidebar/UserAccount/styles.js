import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../../../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    root: function root(_ref2) {
      var cursor = _ref2.cursor;
      return _objectSpread(_objectSpread(_objectSpread({}, spacing["p-1"]), spacing["m-1"]), {}, {
        display: "inline-flex",
        alignItems: "center",
        outline: "none",
        cursor: "".concat(cursor),
        marginLeft: "auto",
        "& path": {
          fill: colors.neutral[900]
        }
      });
    },

    /* Label chip styles */
    label: function label(_ref3) {
      var color = _ref3.color;
      return _objectSpread(_objectSpread(_objectSpread({}, spacing["p-1"]), spacing["ml-2"]), {}, {
        backgroundColor: colors[color][100],
        width: "fit-content",
        height: "fit-content",
        borderRadius: "8px"
      });
    },

    /* Spacing between image and info */
    accountInfo: _objectSpread({}, spacing["mr-3"]),

    /* Name and label styles */
    name: {
      display: "inline-flex",
      alignItems: "center"
    },
    accountName: {
      wordBreak: "break-all",
      overflow: "hidden",
      textOverflow: "ellipsis",
      display: "-webkit-box",
      "-webkit-line-clamp": 1,
      "-webkit-box-orient": "vertical"
    },

    /* Chevron position if opened or closed */
    arrowPosition: {
      transform: "rotate(270deg)"
    },
    arrow: _objectSpread({}, spacing["ml-2"])
  };
};

export default makeStyles(styles);