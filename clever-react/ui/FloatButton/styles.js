import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    button: function button(_ref2) {
      var buttonColor = _ref2.buttonColor,
          onClick = _ref2.onClick;
      return {
        position: "fixed",
        bottom: "20px",
        right: "20px",
        backgroundColor: colors[buttonColor][500],
        "&:hover": {
          backgroundColor: onClick ? colors[buttonColor][500] : colors[buttonColor][900]
        },
        zIndex: "1"
      };
    },
    popper: {
      zIndex: "2"
    },
    paper: _objectSpread(_objectSpread({}, spacing["mb-4"]), {}, {
      borderRadius: "8px"
    })
  };
};

export default makeStyles(styles);