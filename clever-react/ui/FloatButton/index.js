import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import Fab from "@material-ui/core/Fab";
import getClasses from "./styles";

var FloatButton = function FloatButton(props) {
  var style = getClasses(props);
  var buttonLabel = props.buttonLabel,
      content = props.content,
      classes = props.classes,
      onClick = props.onClick;
  var setOpen = props.setOpen,
      open = props.open,
      anchorRef = props.anchorRef,
      variant = props.variant;

  var handleClick = function handleClick() {
    if (onClick) {
      onClick();
    } else {
      setOpen(!open);
    }
  };

  var handleHover = function handleHover() {
    if (onClick) setOpen(true);
  };

  var handleLeave = function handleLeave() {
    if (onClick) setOpen(false);
  };

  var handleClose = function handleClose(event) {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      setOpen(false);
    }

    if (event.key === "Escape") {
      event.preventDefault();
      setOpen(false);
    }
  } // return focus to the button when we transitioned from !open -> open


  var prevOpen = React.useRef(open);
  React.useEffect(function () {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Fab, {
    ref: anchorRef,
    "aria-controls": open ? "menu-list-grow" : undefined,
    "aria-haspopup": "true",
    onClick: handleClick,
    onMouseEnter: handleHover,
    onMouseLeave: handleLeave,
    className: classnames(style.button, classes.button),
    onKeyDown: handleListKeyDown,
    variant: variant
  }, buttonLabel), /*#__PURE__*/React.createElement(ClickAwayListener, {
    onClickAway: handleClose
  }, /*#__PURE__*/React.createElement(Popper, {
    open: open,
    placement: "top-end",
    anchorEl: anchorRef.current,
    role: undefined,
    transition: true,
    disablePortal: true,
    onKeyDown: handleListKeyDown,
    className: classnames(style.popper, classes.popper)
  }, function (_ref) {
    var TransitionProps = _ref.TransitionProps,
        placement = _ref.placement;
    return /*#__PURE__*/React.createElement(Grow, _extends({}, TransitionProps, {
      style: {
        transformOrigin: placement === "bottom" ? "center top" : "center bottom"
      }
    }), /*#__PURE__*/React.createElement(Paper, {
      className: classnames(style.paper, classes.paper)
    }, content));
  })));
};

FloatButton.propTypes = {
  buttonLabel: PropTypes.instanceOf(Object),
  buttonColor: PropTypes.oneOf(["primary", "secondary", "success", "danger", "info", "warning", "blue", "green", "magenta", "yellow", "orange", "pink", "purple", "navyBlue", "christmas", "spring", "neutral", "white", "mobileApp", "audit", "promo", "campaignCreator", "keywordPlanner", "bannerCreator", "adsAssistant", "alt1", "alt2", "alt3"]),
  onClick: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  variant: PropTypes.oneOf(["extended", "circular"]),
  // content: PropTypes.instanceOf(Object),
  open: PropTypes.bool,
  setOpen: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  anchorEl: PropTypes.oneOfType([PropTypes.instanceOf(Object), PropTypes.func, PropTypes.string]),
  classes: PropTypes.instanceOf(Object)
};
FloatButton.defaultProps = {
  buttonLabel: /*#__PURE__*/React.createElement("div", null),
  buttonColor: "secondary",
  // content: <div />,
  variant: "circular",
  setOpen: false,
  open: false,
  onClick: false,
  anchorEl: "",
  classes: {
    button: "",
    paper: "",
    popper: ""
  }
};
export default FloatButton;
/*
  const { setOpen, open, anchorRef, variant } = props;
*/