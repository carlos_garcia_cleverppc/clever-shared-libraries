import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing,
      typography = _ref$cleverUI.typography;
  var sizing = typography.sizing;
  return {
    paper: {
      border: "1px solid ".concat(colors.neutral[300])
    },
    table: {
      borderRadius: "8px",
      borderCollapse: "collapse",
      backgroundColor: "white"
    },
    row: {
      height: "auto"
    },
    rowHover: {
      "&:hover": {
        backgroundColor: colors.neutral[100]
      }
    },
    header: _objectSpread(_objectSpread(_objectSpread(_objectSpread({}, spacing["px-5"]), spacing["pb-2"]), spacing["pt-3"]), {}, {
      borderBottom: "1px solid ".concat(colors.neutral[300]),
      fontWeight: "bold",
      textTransform: "uppercase",
      backgroundColor: colors.primary[100],
      fontFamily: typography.font1,
      fontSize: sizing.body0,
      color: colors.primary[500],
      "& tr": {
        "& th": _objectSpread(_objectSpread(_objectSpread({}, spacing["pt-4"]), spacing["pb-3"]), spacing["px-5"])
      }
    }),
    cell: _objectSpread(_objectSpread(_objectSpread({}, spacing["pt-4"]), spacing["pb-3"]), {}, {
      fontSize: typography.fontSizeBase.constant + typography.fontSizeBase.unit,
      fontFamily: typography.font2,
      borderBottom: "2px solid ".concat(colors.primary[100]) // "& p": { color: colors.neutral[700] },

    }),
    headerSelectionIcon: {
      color: colors.primary[500]
    },
    selectionIcon: {
      color: colors.primary[500]
    }
  };
};

export default makeStyles(styles);