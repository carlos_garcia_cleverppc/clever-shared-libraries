import React from "react";
import PropTypes from "prop-types";
import StepBackward from "../Icon/StepBackward";
import ChevronLeft from "../Icon/ChevronLeft";
import ChevronRight from "../Icon/ChevronRight";
import StepForward from "../Icon/StepForward";
import IconButton from "../IconButton";
import getClasses from "./styles";

var TablePaginationActions = function TablePaginationActions(props) {
  var classes = getClasses();
  var count = props.count,
      page = props.page,
      rowsPerPage = props.rowsPerPage,
      onChangePage = props.onChangePage;

  var handleFirstPageButtonClick = function handleFirstPageButtonClick(event) {
    onChangePage(event, 0);
  };

  var handleBackButtonClick = function handleBackButtonClick(event) {
    onChangePage(event, page - 1);
  };

  var handleNextButtonClick = function handleNextButtonClick(event) {
    onChangePage(event, page + 1);
  };

  var handleLastPageButtonClick = function handleLastPageButtonClick(event) {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return /*#__PURE__*/React.createElement("div", {
    className: classes.pagination
  }, /*#__PURE__*/React.createElement(IconButton, {
    onClick: handleFirstPageButtonClick,
    disabled: page === 0,
    icon: /*#__PURE__*/React.createElement(StepBackward, null),
    size: "small"
  }), /*#__PURE__*/React.createElement(IconButton, {
    onClick: handleBackButtonClick,
    disabled: page === 0,
    icon: /*#__PURE__*/React.createElement(ChevronLeft, null),
    size: "small"
  }), /*#__PURE__*/React.createElement(IconButton, {
    onClick: handleNextButtonClick,
    disabled: page >= Math.ceil(count / rowsPerPage) - 1,
    icon: /*#__PURE__*/React.createElement(ChevronRight, null),
    size: "small"
  }), /*#__PURE__*/React.createElement(IconButton, {
    onClick: handleLastPageButtonClick,
    disabled: page >= Math.ceil(count / rowsPerPage) - 1,
    icon: /*#__PURE__*/React.createElement(StepForward, null),
    size: "small"
  }));
};

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired
};
export default TablePaginationActions;