/* eslint-disable react/jsx-one-expression-per-line */

/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import capitalize from "lodash-es/capitalize";
import sampleSize from "lodash-es/sampleSize";
import Card from "../Card";
import Divider from "../Divider";
import Grid from "../Grid";
import Typography from "../Typography";
import getClasses from "./styles";

var AdPreview = function AdPreview(props) {
  var classes = getClasses();
  var adWord = props.adWord,
      url = props.url,
      name = props.name,
      usePhone = props.usePhone,
      phoneText = props.phoneText,
      currencySymbol = props.currencySymbol;
  var headlines = props.headlines,
      descriptions = props.descriptions,
      services = props.services;
  var headlinesDefault = props.headlinesDefault,
      descriptionsDefault = props.descriptionsDefault,
      servicesDefault = props.servicesDefault;
  var selectedServices = Object.keys(services);
  var endService = " · ";
  return /*#__PURE__*/React.createElement(Card, null, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    className: classes.root
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    className: classnames(classes.sameLine, classes.adDomain)
  }, /*#__PURE__*/React.createElement(Typography, {
    shade: 900,
    variant: "body1",
    weight: "bold",
    className: classes.roboto
  }, adWord, " \xB7", /*#__PURE__*/React.createElement("span", null, "\xA0")), /*#__PURE__*/React.createElement(Typography, {
    shade: 900,
    variant: "body1",
    className: classnames(classes.roboto, classes["break"], classes.adDomain)
  }, url)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, headlines.length > 0 && /*#__PURE__*/React.createElement(Typography, {
    variant: "body3",
    className: classnames(classes.roboto, classes.headlinesMargin, classes.blueGoogle)
  }, name, headlines.length > 0 && headlines.slice(0, 2).map(function (headline, index) {
    var aux = index < Math.max(headlines.length - 1, 2) ? " | " : "";
    return "".concat(aux).concat(headline);
  })), headlines.length === 0 && /*#__PURE__*/React.createElement(Typography, {
    variant: "body3",
    className: classnames(classes.roboto, classes.headlinesMargin, classes.blueGoogle)
  }, "".concat(name, " | ").concat(headlinesDefault))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, descriptions.length > 0 && /*#__PURE__*/React.createElement(Typography, {
    variant: "body1",
    shade: 900,
    className: classes.roboto
  }, descriptions.slice(0, 3).map(function (description) {
    return "".concat(description, ". ");
  })), descriptions.length === 0 && /*#__PURE__*/React.createElement(Typography, {
    variant: "body1",
    shade: 900,
    className: classes.roboto
  }, descriptionsDefault)), /*#__PURE__*/React.createElement(Grid, {
    item: true
  }, selectedServices.length !== 0 && /*#__PURE__*/React.createElement(Grid, {
    container: true,
    className: classes.services
  }, sampleSize(selectedServices, 3).map(function (service, index) {
    return /*#__PURE__*/React.createElement(Grid, {
      item: true,
      key: "service-".concat(service, "-view-xs")
    }, /*#__PURE__*/React.createElement(Typography, {
      className: classes.servicesSpacing
    }, capitalize(services[service].name), service === "freeShipping" ? " ".concat(services[service].value).concat(currencySymbol) : "", index !== 2 && index !== selectedServices.length - 1 ? endService : ""));
  })), selectedServices.length === 0 && !!servicesDefault && /*#__PURE__*/React.createElement(Grid, {
    container: true,
    className: classnames(classes.emptyServices, classes.roboto)
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body1",
    className: classnames(classes.roboto, classes.blueGoogle)
  }, servicesDefault)))), usePhone && /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Divider, null), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body1",
    shade: 900,
    className: classes.roboto
  }, phoneText)))));
};

AdPreview.propTypes = {
  adWord: PropTypes.string,
  url: PropTypes.string,
  name: PropTypes.string,
  headlines: PropTypes.instanceOf(Object),
  headlinesDefault: PropTypes.string,
  descriptions: PropTypes.instanceOf(Object),
  descriptionsDefault: PropTypes.string,
  services: PropTypes.instanceOf(Object),
  servicesDefault: PropTypes.string,
  usePhone: PropTypes.bool,
  phoneText: PropTypes.string,
  currencySymbol: PropTypes.string
};
AdPreview.defaultProps = {
  adWord: "Ad",
  url: "",
  name: "",
  headlines: [],
  headlinesDefault: "",
  descriptions: [],
  descriptionsDefault: "",
  services: [],
  servicesDefault: "",
  usePhone: false,
  phoneText: "",
  currencySymbol: ""
};
export default AdPreview;