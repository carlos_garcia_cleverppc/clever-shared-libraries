import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      spacing = _ref$cleverUI.spacing,
      colors = _ref$cleverUI.colors;
  return {
    root: _objectSpread({}, spacing["p-5"]),
    adDomain: {
      overflow: "hidden",
      textOverflow: "ellipsis",
      wrap: "nowrap",
      whiteSpace: "nowrap",
      "& p:first-child": {
        minWidth: "fit-content"
      },
      "& p": {
        display: "inline"
      }
    },
    "break": {
      wordBreak: "break-all",
      overflow: "hidden",
      textOverflow: "ellipsis",
      display: "-webkit-box",
      "-webkit-line-clamp": 1,
      "-webkit-box-orient": "vertical"
    },
    sameLine: {
      display: "inline-flex"
    },
    headlinesMargin: _objectSpread({}, spacing["my-2"]),
    services: _objectSpread({}, spacing["mt-3"]),
    customServices: {
      display: "flex"
    },
    servicesSpacing: _objectSpread({}, spacing["mr-1"]),
    emptyServices: _objectSpread(_objectSpread({
      border: "0.5px solid ".concat(colors.neutral[300])
    }, spacing["p-2"]), spacing["mt-3"]),
    roboto: {
      fontFamily: "Roboto",
      overflow: "hidden",
      textOverflow: "ellipsis"
    },
    blueGoogle: {
      color: "#1a0dab"
    },
    fadeEnter: {
      opacity: 0.01
    },
    fadEnterFadeEnterActive: {
      opacity: 1,
      transition: "opacity 1300ms ease-in"
    }
  };
};

export default makeStyles(styles);