/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import getClasses from "./styles";

var Card = function Card(props) {
  var children = props.children,
      className = props.className,
      onClick = props.onClick;
  var variant = props.variant,
      color = props.color,
      clickable = props.clickable;
  var classes = getClasses({
    variant: variant,
    color: color
  });

  var getCardClassNames = function getCardClassNames() {
    var res = [className];

    if (variant === "striped") {
      res.push(classes.stripedCard);
    } else {
      res.push(classes.containedCard);

      if (clickable) {
        res.push(classes.containerCardClickable);
      }
    }

    return res;
  };

  return /*#__PURE__*/React.createElement("div", {
    className: classnames(getCardClassNames()),
    onClick: onClick,
    role: onClick ? "button" : "banner",
    onKeyUp: function onKeyUp() {}
  }, children);
};

Card.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  color: PropTypes.oneOf(["", "primary", "secondary", "warning", "neutral", "default", "info", "success"]),
  variant: PropTypes.oneOf(["contained", "striped"]),
  clickable: PropTypes.bool
};
Card.defaultProps = {
  className: "",
  onClick: function onClick() {},
  color: "",
  variant: "contained",
  clickable: false
};
export default Card;