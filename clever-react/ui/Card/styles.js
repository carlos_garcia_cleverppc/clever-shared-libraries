import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    containedCard: function containedCard(_ref2) {
      var variant = _ref2.variant,
          color = _ref2.color;
      return _objectSpread(_objectSpread(_objectSpread({}, spacing["px-1"]), spacing["pt-1"]), {}, {
        position: "relative",
        display: "flex",
        flexDirection: "column",
        border: "1px solid",
        borderColor: colors.neutral[300],
        borderRadius: "8px",
        minWidth: 0,
        backgroundClip: "border-box",
        backgroundColor: !!colors[color] && variant === "contained" ? colors[color][500] : "white",
        color: !!colors[color] && variant === "contained" ? colors[color].contrastText : colors.neutral[900],
        "& .card__header": {
          order: 0,
          display: "flex"
        },
        "& .card__body": {
          order: 1,
          display: "flex"
        }
      });
    },
    containerCardClickable: {
      "&:hover": {
        boxShadow: "1px 4px 16px 0px rgba(0,0,0,0.24)"
      }
    },
    stripedCard: function stripedCard(_ref3) {
      var variant = _ref3.variant,
          color = _ref3.color;
      var strokeWidth = "5";
      /* width's dashes */

      var dashArray = "10 20";
      /* space between dashes should be > strokeWidth */

      var lineCap = "square";
      /* square, round, butt; type of border of dashes */

      var dashOffset = "5";
      /* move clockside dashes */

      var borderRadius = "16";
      var strokeColor = colors.neutral[300]; // codigo hexadecimal

      return _objectSpread(_objectSpread(_objectSpread(_objectSpread({
        backgroundImage: "url(\"data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' rx='".concat(borderRadius, "' ry='").concat(borderRadius, "' stroke='%23").concat(strokeColor.replace("#", ""), "FF' stroke-width='").concat(strokeWidth, "' stroke-dasharray='").concat(dashArray, "' stroke-dashoffset='").concat(dashOffset, "' stroke-linecap='").concat(lineCap, "'/%3e%3c/svg%3e\")"),
        borderRadius: "".concat(borderRadius, "px")
      }, spacing["mb-3"]), spacing["px-1"]), spacing["pt-1"]), {}, {
        position: "relative",
        display: "flex",
        color: !!colors[color] && variant === "contained" ? colors[color].contrastText : colors.neutral[900]
      });
    }
  };
};

export default makeStyles(styles);