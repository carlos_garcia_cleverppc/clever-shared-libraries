/* eslint-disable react/no-unused-prop-types */

/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getColorsLabels } from "../styles/colors";
import getClasses from "./styles";
var colorsList = getColorsLabels(true);

var CleverLink = function CleverLink(props) {
  var children = props.children,
      className = props.className,
      to = props.to;
  var target = props.target,
      onClick = props.onClick,
      disabled = props.disabled;
  var classes = getClasses(props);

  if (to) {
    return /*#__PURE__*/React.createElement("a", {
      href: to,
      className: classnames(classes.root, className, disabled ? classes.disabled : ""),
      target: target,
      disabled: disabled
    }, children);
  }

  return /*#__PURE__*/React.createElement("button", {
    type: "button",
    className: classnames(classes.root, className, disabled ? classes.disabled : ""),
    onClick: onClick,
    disabled: disabled
  }, children);
};

CleverLink.propTypes = {
  className: PropTypes.string,
  to: PropTypes.string,
  target: PropTypes.string,
  onClick: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  disabled: PropTypes.bool,
  color: PropTypes.oneOf(colorsList)
};
CleverLink.defaultProps = {
  to: "",
  className: "",
  target: "",
  onClick: false,
  disabled: false,
  color: "blue"
};
export default CleverLink;