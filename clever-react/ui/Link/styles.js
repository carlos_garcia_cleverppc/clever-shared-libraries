import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      typography = _ref$cleverUI.typography;
  return {
    root: function root(_ref2) {
      var color = _ref2.color;
      return {
        fontFamily: typography.font2,
        lineHeight: typography.lineHeight,
        fontWeight: "bold",
        fontSize: "1rem",
        color: colors[color][500],
        textDecoration: "none",
        cursor: "pointer",
        // override button styles
        border: "none",
        backgroundColor: "transparent",
        textAlign: "left",
        // display: "block",
        padding: 0,
        "&:hover": {
          color: colors[color][700]
        },
        "&:focus": {
          outline: "none",
          color: colors[color][700]
        }
      };
    },
    disabled: function disabled(_ref3) {
      var color = _ref3.color;
      return {
        color: colors[color][500],
        cursor: "default",
        opacity: "0.5",
        textDecoration: "none",
        pointerEvents: "none",
        "&:hover": {
          color: colors[color][500]
        }
      };
    }
  };
};

export default makeStyles(styles);