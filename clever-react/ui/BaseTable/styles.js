import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable no-tabs */
import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing,
      typography = _ref$cleverUI.typography;
  var sizing = typography.sizing;
  return {
    paper: {
      overflow: "hidden",
      overflowX: "auto",
      boxShadow: "none",
      "& table": _objectSpread({}, spacing["w-100"]),
      "&::-webkit-scrollbar": {
        width: "12px",
        backgroundColor: "#fff"
      },
      "&::-webkit-scrollbar-track": {
        backgroundColor: "#fff"
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#babac0",
        borderRadius: "16px",
        border: "4px solid #fff"
      }
    },
    fillAvailable: {
      fallbacks: [{
        width: "-moz-available"
      }, {
        width: "-webkit-fill-available"
      }]
    },
    results: _objectSpread(_objectSpread({}, spacing["mt-6"]), {}, {
      fontSize: "1rem"
    }),
    entriesSelect: _objectSpread({}, spacing["mx-3"]),
    entriesWrapper: _objectSpread(_objectSpread({}, spacing["mb-4"]), {}, {
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    }),
    selectRoot: _objectSpread(_objectSpread({}, spacing["py-0"]), {}, {
      fontFamily: typography.font1,
      color: typography.color,
      fontSize: sizing.body0,
      "& p": {
        fontWeight: "bold"
      },
      "&:focus": {
        backgroundColor: "transparent"
      }
    }),
    selectIcon: {
      top: "auto",
      height: "10px",
      transform: "rotate(90deg)",
      "& path": {
        fill: "".concat(colors.neutral[900], " !important")
      }
    },
    inputBase: {
      height: "30px"
    },
    labelRoot: {
      fontFamily: typography.font1,
      fontWeight: "bold",
      fontSize: sizing.body0,
      color: colors.neutral[700],
      marginBottom: "-10px",
      lineHeight: 1
    },
    arrow: function arrow(_ref2) {
      var defaultColor = _ref2.sortingIconsColors.defaultColor;
      return {
        "& path": {
          fill: colors[defaultColor.color][defaultColor.shade]
        }
      };
    },
    activeArrow: function activeArrow(_ref3) {
      var _activeArrow = _ref3.sortingIconsColors.activeArrow;
      return {
        "& path": {
          fill: colors[_activeArrow.color][_activeArrow.shade]
        }
      };
    },
    sortIcons: _objectSpread(_objectSpread({}, spacing["ml-2"]), {}, {
      width: "20px"
    }),
    bodyCell: _objectSpread(_objectSpread({
      paddingRight: "2.85rem"
    }, spacing["pl-5"]), {}, {
      "&:first-child": _objectSpread({}, spacing["pl-7"]),
      "&:last-child": _objectSpread({}, spacing["pr-7"])
    }),
    headerRow: {
      "& th": {
        height: "45px"
      }
    },
    headerCell: _objectSpread(_objectSpread({
      whiteSpace: "nowrap",
      textAlign: "left"
    }, spacing["px-2"]), {}, {
      "&:first-child": _objectSpread({}, spacing["pl-7"]),
      "&:last-child": _objectSpread({}, spacing["pr-5"])
    }),
    headerSelectionIcon: {
      color: colors.primary[300]
    },
    selectionIcon: {
      color: colors.primary[500]
    },
    scrollBar: {
      background: "\n      /* Shadow covers */\n\t\t  linear-gradient(90deg, white 30%, rgba(255,255,255,0)),\n\t\t  linear-gradient(90deg, rgba(255,255,255,0), white 70%) 100% 0,\n\t\t\n\t\t  /* Shadows */\n\t\t  radial-gradient(farthest-side at 0 50%, rgba(0,0,0,.2), rgba(0,0,0,0)),\n      radial-gradient(farthest-side at 100% 50%, rgba(0,0,0,.2), rgba(0,0,0,0)) 100% 0\n    ",
      backgroundRepeat: "no-repeat",
      backgroundColor: "white",
      backgroundSize: "40px 100%, 40px 100%, 14px 100%, 14px 100%",
      backgroundAttachment: "local, local, scroll, scroll"
    },
    newRowIcon: _objectSpread(_objectSpread(_objectSpread(_objectSpread({}, spacing["m-0"]), spacing["ml-1"]), spacing["mr-2"]), {}, {
      marginBottom: "-0.1rem"
    }),
    loading: {
      display: "flex"
    }
  };
};

export default makeStyles(styles);