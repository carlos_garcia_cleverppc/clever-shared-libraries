import _extends from "@babel/runtime/helpers/extends";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["page", "type", "selected"];

/* eslint-disable react/jsx-props-no-spreading */

/* eslint-disable react/no-array-index-key */

/* eslint-disable react/no-multi-comp */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { usePagination } from "@material-ui/lab/Pagination";
import Link from "../../Link";
import getClasses from "./styles"; // const PageButtonComponent = props => <button {...props}>{props.children}</button>;

var Pagination = function Pagination(props) {
  var classes = getClasses();
  var pages = props.pages,
      onPageChange = props.onPageChange,
      previousText = props.previousText,
      nextText = props.nextText,
      pageProp = props.page;

  var _usePagination = usePagination({
    count: pages,
    onChange: function onChange(event, page) {
      return onPageChange(page - 1);
    },
    page: pageProp + 1
  }),
      items = _usePagination.items; // const visiblePages = getVisiblePages(null, pages);
  // const activePage = page + 1;


  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
    className: classes.tablePagination
  }, items.map(function (_ref, index) {
    var page = _ref.page,
        type = _ref.type,
        selected = _ref.selected,
        item = _objectWithoutProperties(_ref, _excluded);

    var children = null;

    if (type === "start-ellipsis" || type === "end-ellipsis") {
      children = "…";
    } else if (type === "page") {
      children = /*#__PURE__*/React.createElement(Link, _extends({
        className: selected ? classnames(classes.pageButton, classes.selectedPage) : classes.pageButton
      }, item), page);
    } else {
      children = /*#__PURE__*/React.createElement(React.Fragment, null, type === "next" && /*#__PURE__*/React.createElement(Link, _extends({}, item, {
        className: classes.nextButtonWrapper
      }), nextText), type === "previous" && /*#__PURE__*/React.createElement(Link, _extends({}, item, {
        className: classes.prevButtonWrapper
      }), previousText));
    }

    return /*#__PURE__*/React.createElement("span", {
      key: index
    }, children);
  })));
  /*
    return (
      <div className={classes.tablePagination}>
        <div className={classes.prevButtonWrapper}>
          <Link
            className="Table__pageButton"
            onClick={() => {
              if (activePage === 1) return;
              changePage(page - 1);
            }}
            disabled={activePage === 1}
          >
            {previousText}
          </Link>
        </div>
        <div className="Table__visiblePagesWrapper">
          {visiblePages.map((p, index, array) => (
            <Link
              key={p}
              disabled={activePage !== p}
              className={classes.pageButton}
              onClick={() => onPageChange(p - 1)}
            >
              {array[index - 1] + 2 < p ? `...${p}` : p}
            </Link>
          ))}
        </div>
         <div className={classes.nextButtonWrapper}>
          <Link
            className="Table__pageButton"
            onClick={() => {
              if (activePage === pages) return;
              changePage(page + 1);
            }}
            disabled={activePage === pages}>
            {nextText}
          </Link>
        </div>
      </div>
    ); */
};

Pagination.propTypes = {
  pages: PropTypes.number,
  page: PropTypes.number,
  onPageChange: PropTypes.func,
  previousText: PropTypes.string,
  nextText: PropTypes.string
};
Pagination.defaultProps = {
  pages: 1,
  page: 0,
  onPageChange: function onPageChange() {},
  previousText: "Previous",
  nextText: "Next"
};
export default Pagination;