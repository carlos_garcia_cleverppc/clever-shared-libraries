/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";
import MaterialBackdrop from "@material-ui/core/Backdrop";

var Backdrop = function Backdrop(_ref) {
  var children = _ref.children,
      className = _ref.className,
      open = _ref.open,
      onClick = _ref.onClick;
  return /*#__PURE__*/React.createElement(MaterialBackdrop, {
    className: className,
    open: open,
    onClick: onClick
  }, children);
};

Backdrop.propTypes = {
  className: PropTypes.string,
  open: PropTypes.bool,
  onClick: PropTypes.func
};
Backdrop.defaultProps = {
  className: "",
  open: false,
  onClick: function onClick() {}
};
export default Backdrop;