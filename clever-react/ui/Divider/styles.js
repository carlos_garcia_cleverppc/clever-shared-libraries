import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    horizontal: _objectSpread(_objectSpread({}, spacing["w-100"]), {}, {
      minWidth: "100px",
      border: 0,
      height: "1px",
      backgroundColor: colors.neutral[300]
    }),
    vertical: _objectSpread(_objectSpread({}, spacing["h-100"]), {}, {
      minHeight: "20px",
      border: 0,
      width: "1px",
      backgroundColor: colors.neutral[300]
    }),
    dashed: {
      borderTop: "2px dashed ".concat(colors.neutral[300]),
      backgroundColor: "unset",
      height: "0px"
    }
  };
};

export default makeStyles(styles);