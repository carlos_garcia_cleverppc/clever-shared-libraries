/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import getClasses from "./styles";

var Divider = function Divider(props) {
  var classes = getClasses(props);
  var className = props.className,
      position = props.position,
      dashed = props.dashed;
  return /*#__PURE__*/React.createElement("hr", {
    className: classnames(position === "horizontal" ? classes.horizontal : classes.vertical, dashed ? classes.dashed : "", className)
  });
};

Divider.propTypes = {
  className: PropTypes.string,
  position: PropTypes.oneOf(["horizontal", "vertical"]),
  dashed: PropTypes.bool
};
Divider.defaultProps = {
  className: "",
  position: "horizontal",
  dashed: false
};
export default Divider;