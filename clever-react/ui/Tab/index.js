import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable react/jsx-props-no-spreading */

/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";
import MaterialTab from "@material-ui/core/Tab";
import makeStyles from "../styles/makeStyles";
var getClasses = makeStyles(function (_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      typography = _ref$cleverUI.typography;
  var sizing = typography.sizing;
  return {
    wrapper: {
      fontFamily: typography.font1,
      color: colors.neutral[900],
      textTransform: "none",
      fontWeight: "bold",
      fontSize: sizing.body1,
      display: "flex",
      flexDirection: "row"
    }
  };
});

var Tab = function Tab(props) {
  var classes = getClasses();
  var value = props.value,
      label = props.label,
      icon = props.icon;
  return /*#__PURE__*/React.createElement(MaterialTab, _extends({}, props, {
    value: value,
    label: label,
    icon: icon,
    classes: {
      wrapper: classes.wrapper
    },
    disableRipple: true
  }));
};

Tab.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  label: PropTypes.string.isRequired
};
Tab.defaultProps = {};
export default Tab;