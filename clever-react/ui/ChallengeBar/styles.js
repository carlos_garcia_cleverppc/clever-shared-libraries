import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    root: {
      width: "100%",
      display: "inline-flex"
    },
    barWrapper: _objectSpread(_objectSpread(_objectSpread({}, spacing["mr-4"]), spacing["px-2"]), {}, {
      height: "40px",
      width: "100%",
      borderRadius: "20px",
      backgroundColor: colors.neutral[100],
      display: "inline-flex",
      justifyContent: "space-between",
      alignItems: "center",
      position: "relative"
    }),
    progressWrapper: function progressWrapper(_ref2) {
      var percentage = _ref2.percentage;
      return {
        height: "100%",
        width: "".concat(percentage, "%"),
        backgroundColor: colors.success[100],
        borderRadius: "20px",
        position: "absolute",
        left: "0px"
      };
    },
    statusWrapper: {
      display: "flex",
      zIndex: 1
    },
    roundStatusWrapper: {
      height: "32px",
      width: "32px",
      borderRadius: "100px",
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    },
    finalState: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    },
    warning: {
      backgroundColor: colors.warning[500]
    },
    warningFocused: {
      backgroundColor: colors.warning[700]
    },
    pending: {
      backgroundColor: colors.neutral[300]
    },
    pendingFocused: {
      backgroundColor: colors.neutral[500]
    },
    starWrapper: {
      height: "40px",
      width: "40px",
      borderRadius: "100px",
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    },
    star: {
      backgroundColor: colors.yellow[100]
    },
    click: {
      cursor: "pointer"
    }
  };
};

export default makeStyles(styles);