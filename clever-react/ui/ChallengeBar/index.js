/* eslint-disable max-len */
import React from "react";
import PropTypes from "prop-types";
import findLastIndex from "lodash-es/findLastIndex";
import classnames from "classnames";
import Check from "../Icon/Check";
import Warning from "../Icon/Warning";
import Star from "../Icon/Star";
import Typography from "../Typography";
import getClasses from "./styles";

var ChallengeBar = function ChallengeBar(props) {
  var className = props.className,
      statuses = props.statuses,
      onClickStatus = props.onClickStatus,
      positionFocused = props.positionFocused;
  var numberStatuses = statuses.length;
  var lastCompleted = findLastIndex(statuses, function (val) {
    return val.toUpperCase() === "COMPLETED";
  });
  var percentage = lastCompleted >= 0 ? (lastCompleted + 1) / numberStatuses * 100 : 0;

  var onClickTask = function onClickTask(index) {
    onClickStatus(index);
  };

  var classes = getClasses({
    percentage: percentage
  });
  return /*#__PURE__*/React.createElement(React.Fragment, null, numberStatuses > 0 && /*#__PURE__*/React.createElement("div", {
    className: classnames(className, classes.root)
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.barWrapper
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.progressWrapper
  }), statuses.map(function (status, index) {
    return /*#__PURE__*/React.createElement("div", {
      className: classes.statusWrapper,
      key: "".concat(index, "-").concat(status)
    }, /*#__PURE__*/React.createElement("div", {
      className: classes.click,
      onClick: function onClick() {
        return onClickTask(index);
      },
      onKeyDown: function onKeyDown() {
        return onClickTask(index);
      },
      role: "button",
      tabIndex: 0
    }, status.toUpperCase() === "COMPLETED" && /*#__PURE__*/React.createElement(Check, {
      size: 32,
      color: "success",
      shade: positionFocused === index ? 700 : 500
    }), status.toUpperCase() === "FAILED" && /*#__PURE__*/React.createElement("div", {
      className: classnames(classes.roundStatusWrapper, positionFocused === index ? classes.warningFocused : classes.warning)
    }, /*#__PURE__*/React.createElement(Warning, {
      size: 24,
      color: "white"
    })), status.toUpperCase() === "PENDING" && /*#__PURE__*/React.createElement("div", {
      className: classnames(classes.roundStatusWrapper, positionFocused === index ? classes.pendingFocused : classes.pending)
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "f1-14",
      weight: "bold",
      color: "white"
    }, index + 1))));
  })), /*#__PURE__*/React.createElement("div", {
    className: classes.finalState
  }, /*#__PURE__*/React.createElement("div", {
    className: classnames(classes.starWrapper, percentage === 100 ? classes.star : classes.pending)
  }, /*#__PURE__*/React.createElement(Star, {
    size: 20,
    color: percentage === 100 ? "yellow" : "white",
    shade: 300
  })))));
};

ChallengeBar.propTypes = {
  className: PropTypes.string,
  statuses: PropTypes.arrayOf(PropTypes.string)
};
ChallengeBar.defaultProps = {
  className: "",
  statuses: []
};
export default ChallengeBar;