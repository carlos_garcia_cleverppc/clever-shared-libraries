import React from "react";
import PropTypes from "prop-types";
import User from "../Icon/User";
import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var colors = _ref.cleverUI.colors;
  return {
    defaultAvatar: function defaultAvatar(_ref2) {
      var size = _ref2.size;
      return {
        backgroundColor: colors.neutral[100],
        width: size,
        height: size,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: "50%",
        "& svg": {
          width: "62%",
          height: "62%"
        }
      };
    }
  };
};

var getClasses = makeStyles(styles);

var DefaultAvatar = function DefaultAvatar(_ref3) {
  var size = _ref3.size;
  var classes = getClasses({
    size: size
  });
  return /*#__PURE__*/React.createElement("div", {
    className: classes.defaultAvatar,
    "aria-hidden": "true"
  }, /*#__PURE__*/React.createElement(User, null));
};

DefaultAvatar.propTypes = {
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};
DefaultAvatar.defaultProps = {
  size: 42
};
export default DefaultAvatar;