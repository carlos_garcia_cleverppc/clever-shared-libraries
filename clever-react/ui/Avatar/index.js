/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getColorsLabels } from "../styles/colors";
import Image from "../Image";
import Typography from "../Typography";
import getClasses from "./styles";
import DefaultAvatar from "./DefaultAvatar";
var colorsList = getColorsLabels(true);

var Avatar = function Avatar(props) {
  var className = props.className,
      size = props.size,
      src = props.src,
      color = props.color,
      label = props.label,
      colorCircle = props.colorCircle,
      positionLabel = props.positionLabel;
  var classes = getClasses({
    size: size,
    color: color,
    positionLabel: positionLabel
  });
  return /*#__PURE__*/React.createElement("div", {
    className: classnames(classes.root, className)
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.avatarWrapper
  }, src ? /*#__PURE__*/React.createElement(Image, {
    className: classes.image,
    src: src,
    size: "responsive",
    variant: "circle",
    center: false
  }) : /*#__PURE__*/React.createElement(DefaultAvatar, {
    size: "100%"
  }), colorCircle && /*#__PURE__*/React.createElement("div", {
    className: classes.circle
  })), label && /*#__PURE__*/React.createElement(Typography, {
    color: color,
    weight: "bold",
    variant: "f1-8",
    className: positionLabel === "top-right" ? classes.labelTopRight : classes.labelBottomCenter
  }, label));
};

Avatar.propTypes = {
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  src: PropTypes.string,
  label: PropTypes.string,
  colorCircle: PropTypes.bool,
  color: PropTypes.oneOf(colorsList),
  positionLabel: PropTypes.oneOf(["top-right", "bottom-center"])
};
Avatar.defaultProps = {
  size: 42,
  src: "",
  label: "",
  colorCircle: false,
  color: "primary",
  positionLabel: "top-right"
};
export default Avatar;