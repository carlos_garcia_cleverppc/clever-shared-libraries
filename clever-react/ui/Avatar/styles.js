import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    root: function root(_ref2) {
      var size = _ref2.size;
      return {
        width: size,
        height: size,
        minWidth: size,
        position: "relative"
      };
    },
    avatarWrapper: {
      width: "100%",
      height: "100%",
      display: "grid",
      borderRadius: "50%",
      "& > *": {
        gridArea: "1/-1",
        borderRadius: "inherit"
      }
    },
    circle: function circle(_ref3) {
      var color = _ref3.color;
      return {
        boxShadow: "inset 0 0 0 3px ".concat(colors[color][500])
      };
    },
    labelCircle: function labelCircle(_ref4) {
      var color = _ref4.color;
      return {
        borderRadius: "50%",
        border: "3px solid ".concat(colors[color][500]),
        boxSizing: "border-box",
        overflow: "hidden"
      };
    },
    labelTopRight: function labelTopRight(_ref5) {
      var color = _ref5.color;
      return _objectSpread(_objectSpread({
        position: "absolute",
        top: 0,
        right: 0,
        transform: "translateX(50%)",
        whiteSpace: "nowrap"
      }, spacing["p-1"]), {}, {
        backgroundColor: colors[color][100],
        borderRadius: "8px"
      });
    },
    labelBottomCenter: function labelBottomCenter(_ref6) {
      var color = _ref6.color,
          size = _ref6.size;
      return _objectSpread(_objectSpread({
        position: "absolute",
        bottom: "-9px",
        // Middle of label height
        right: size / 2,
        transform: "translateX(50%)",
        whiteSpace: "nowrap"
      }, spacing["p-1"]), {}, {
        backgroundColor: colors[color][100],
        borderRadius: "8px"
      });
    },
    image: {
      width: "100%",
      height: "100%",
      objectFit: "contain"
    }
  };
};

export default makeStyles(styles);