/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Grid from "../Grid";
import Typography from "../Typography";
import Link from "../Link";
import Warning from "../Icon/Warning";
import getClasses from "./styles";
import useTheme from "../styles/useTheme";
import { getColorsLabels } from "../styles/colors";
var colorsList = getColorsLabels(true);

var AlertCard = function AlertCard(props) {
  var color = props.color,
      text = props.text,
      action = props.action,
      linkText = props.linkText,
      className = props.className;
  var classes = getClasses({
    color: color
  });

  var _useTheme = useTheme(),
      colors = _useTheme.cleverUI.colors;

  return /*#__PURE__*/React.createElement(Grid, {
    container: true,
    className: classnames(classes.card, className)
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 1,
    className: classes.icon
  }, /*#__PURE__*/React.createElement(Warning, {
    size: 20,
    color: color === "danger" ? "white" : color,
    shade: colors[color][700] ? 700 : 500
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 11,
    className: classes.text
  }, /*#__PURE__*/React.createElement(Typography, {
    color: color === "danger" ? "white" : color,
    shade: colors[color][700] ? 700 : 500,
    variant: "body0",
    align: "left"
  }, text, !!linkText && /*#__PURE__*/React.createElement(Link, {
    onClick: action,
    className: classes.link,
    color: color
  }, linkText))));
};

AlertCard.propTypes = {
  color: PropTypes.oneOf(colorsList),
  text: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Object)]),
  className: PropTypes.string,
  action: PropTypes.func,
  linkText: PropTypes.string
};
AlertCard.defaultProps = {
  color: "primary",
  text: "",
  className: "",
  action: function action() {},
  linkText: ""
};
export default AlertCard;