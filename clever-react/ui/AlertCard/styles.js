import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    card: function card(_ref2) {
      var color = _ref2.color;
      return _objectSpread({
        backgroundColor: colors[color][100],
        borderRadius: "8px"
      }, spacing["p-4"]);
    },
    icon: {
      alignSelf: "center"
    },
    text: {
      alignSelf: "center",
      display: "flex"
    },
    link: function link(_ref3) {
      var color = _ref3.color;
      return _objectSpread(_objectSpread({}, spacing["ml-1"]), {}, {
        color: colors[color][900],
        textDecoration: "underline",
        fontSize: "14px"
      });
    }
  };
};

export default makeStyles(styles);