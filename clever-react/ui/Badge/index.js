import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import MaterialBadge from "@material-ui/core/Badge";
import getClasses from "./styles";

var Badge = function Badge(props) {
  var variant = props.variant,
      badgeContent = props.badgeContent,
      className = props.className,
      children = props.children;
  var invisible = props.invisible,
      showZero = props.showZero,
      color = props.color;
  var classes = getClasses({
    color: color
  });
  var componentClasses = getClasses({
    color: color
  });
  return /*#__PURE__*/React.createElement(MaterialBadge, {
    variant: variant,
    badgeContent: badgeContent,
    invisible: invisible,
    showZero: showZero,
    className: className,
    classes: _objectSpread(_objectSpread({}, classes), {}, {
      badge: classnames(componentClasses.badge, classes.badge)
    })
  }, children);
};

Badge.propTypes = {
  color: PropTypes.oneOf(["primary", "secondary", "success", "danger", "warning", "info", "orange"]),
  className: PropTypes.string,
  variant: PropTypes.oneOf(["standard", "dot"]),
  badgeContent: PropTypes.node,
  showZero: PropTypes.bool,
  invisible: PropTypes.bool
};
Badge.defaultProps = {
  color: "primary",
  className: "",
  variant: "standard",
  badgeContent: "",
  showZero: false,
  invisible: false
};
export default Badge;