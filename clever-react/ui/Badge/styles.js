import makeStyles from "../styles/makeStyles";
import getContrastText from "../styles/contrastText";

var styles = function styles(_ref) {
  var colors = _ref.cleverUI.colors;
  return {
    badge: function badge(_ref2) {
      var color = _ref2.color;
      return {
        backgroundColor: colors[color][500],
        color: getContrastText(colors[color][500])
      };
    }
  };
};

export default makeStyles(styles);