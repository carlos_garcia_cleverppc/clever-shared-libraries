/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import IconButton from "@material-ui/core/IconButton";
import { getColorsLabels, getShades } from "../styles/colors";
import getClasses from "./styles";
var colorsList = getColorsLabels(true);
var shadesList = getShades(true);
var CleverIconButton = /*#__PURE__*/React.forwardRef(function (props, ref) {
  var className = props.className,
      onClick = props.onClick,
      disabled = props.disabled,
      ariaLabel = props.ariaLabel,
      href = props.href,
      target = props.target,
      type = props.type;
  var size = props.size,
      icon = props.icon,
      iconSize = props.iconSize,
      hover = props.hover,
      active = props.active,
      color = props.color,
      shade = props.shade,
      outlined = props.outlined;
  var classes = getClasses({
    disabled: disabled,
    size: size,
    hover: hover,
    active: active,
    color: color,
    shade: shade,
    iconSize: iconSize,
    outlined: outlined
  });

  var getStyles = function getStyles() {
    var classNames = [];
    classNames.push(className);

    if (outlined && disabled) {
      classNames.push(classes.outlinedDisabled);
    } else if (outlined) {
      classNames.push(classes.outlined);
    } else if (disabled) {
      classNames.push(classes.notOutlinedDisabled);
    } else {
      classNames.push(classes.notOutlined);
    }

    if (hover === "zoomIn") {
      classNames.push(classes.zoomIn);
    } else if (hover === "square" && !outlined) {
      classNames.push(classes.squareOutlined);
    }

    if (size === "small") {
      classNames.push(classes.sizeSmall);
    } else {
      classNames.push(classes.sizeMedium);
    }

    if (iconSize) classNames.push(classes.size);
    return classnames(classNames);
  };

  return /*#__PURE__*/React.createElement(IconButton, {
    className: getStyles(),
    onClick: onClick,
    disabled: disabled,
    "aria-label": ariaLabel,
    href: href,
    target: target,
    size: size,
    ref: ref,
    type: type // {...props}

  }, icon);
});
CleverIconButton.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  icon: PropTypes.element.isRequired,
  href: PropTypes.string,
  target: PropTypes.string,
  size: PropTypes.oneOf(["small", "medium"]),
  color: PropTypes.oneOf(colorsList),
  hover: PropTypes.oneOf(["circle", "square", "zoomIn"]),
  active: PropTypes.bool,
  iconSize: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96, false]),
  ariaLabel: PropTypes.string,
  outlined: PropTypes.bool,
  shade: PropTypes.oneOf(shadesList),
  type: PropTypes.oneOf(["button", "reset", "submit"])
};
CleverIconButton.defaultProps = {
  className: "",
  onClick: function onClick() {},
  disabled: false,
  href: "",
  target: "",
  size: "medium",
  color: "primary",
  hover: "circle",
  active: false,
  iconSize: false,
  ariaLabel: "",
  outlined: true,
  shade: 500,
  type: "button"
};
export default CleverIconButton;