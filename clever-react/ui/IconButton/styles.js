import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    outlined: function outlined(_ref2) {
      var active = _ref2.active,
          color = _ref2.color,
          shade = _ref2.shade;
      return {
        backgroundColor: "transparent",
        "& path": {
          fill: "".concat(active ? colors[color][shade] : colors.neutral[shade], " !important")
        },
        "&:hover": {
          "& path": {
            fill: "".concat(colors[color][shade], " !important")
          },
          boxShadow: "none",
          backgroundColor: "transparent"
        },
        "&:focus": {
          "& path": {
            fill: "".concat(colors[color][shade], " !important")
          },
          boxShadow: "none",
          backgroundColor: "transparent"
        }
      };
    },
    outlinedDisabled: {
      backgroundColor: "transparent",
      "& path": {
        fill: "".concat(colors.neutral[300], " !important")
      },
      "&:hover": {
        "& path": {
          fill: "".concat(colors.neutral[300], " !important")
        },
        boxShadow: "none",
        backgroundColor: "transparent"
      },
      "&:focus": {
        "& path": {
          fill: "".concat(colors.neutral[300], " !important")
        },
        boxShadow: "none",
        backgroundColor: "transparent"
      }
    },
    notOutlined: function notOutlined(_ref3) {
      var color = _ref3.color,
          shade = _ref3.shade;
      return {
        backgroundColor: "".concat(colors[color][shade], " !important"),
        "& path": {
          fill: "white"
        },
        "&:hover": {
          backgroundColor: "".concat(colors[color][700], " !important"),
          boxShadow: "none"
        },
        "&:focus": {
          backgroundColor: "".concat(colors[color][700], " !important"),
          boxShadow: "none"
        }
      };
    },
    notOutlinedDisabled: function notOutlinedDisabled(_ref4) {
      var color = _ref4.color;
      return {
        backgroundColor: "".concat(colors[color][100], " !important"),
        "& path": {
          fill: "white"
        },
        "&:hover": {
          backgroundColor: "".concat(colors[color][100], " !important"),
          boxShadow: "none"
        },
        "&:focus": {
          backgroundColor: "".concat(colors[color][100], " !important"),
          boxShadow: "none"
        }
      };
    },
    zoomIn: {
      transition: "all .3s ease-in-out",
      "&:hover": {
        boxShadow: "none",
        transform: "scale(1.25)"
      }
    },
    squareOutlined: {
      borderRadius: "8px"
    },
    size: function size(_ref5) {
      var iconSize = _ref5.iconSize;
      return {
        "& svg": {
          width: iconSize,
          height: iconSize
        }
      };
    },
    sizeSmall: _objectSpread(_objectSpread({}, spacing["p-1"]), {}, {
      "& svg": {
        width: "0.5rem",
        height: "0.5rem"
      }
    }),
    sizeMedium: _objectSpread(_objectSpread({}, spacing["p-2"]), {}, {
      "& svg": {
        width: "1.3rem",
        height: "1.3rem"
      }
    })
  };
};

export default makeStyles(styles);