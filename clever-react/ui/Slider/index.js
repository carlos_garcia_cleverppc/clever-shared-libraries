import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import SliderMaterial from "@material-ui/core/Slider";
import getClasses from "./styles";
import { getColorsLabels } from "../styles/colors";
var colorsList = getColorsLabels(true);

var Slider = function Slider(props) {
  var color = props.color,
      value = props.value,
      onChange = props.onChange,
      disabled = props.disabled,
      max = props.max,
      min = props.min,
      step = props.step;
  var onDragEnd = props.onDragEnd,
      className = props.className,
      valueLabelDisplay = props.valueLabelDisplay,
      valueLabelFormat = props.valueLabelFormat,
      valueLabelClass = props.valueLabelClass;
  var thumbClass = props.thumbClass,
      trackClass = props.trackClass;
  var classes = getClasses({
    color: color
  });
  return /*#__PURE__*/React.createElement(SliderMaterial, {
    value: value,
    onChange: onChange,
    disabled: disabled,
    max: max,
    min: min,
    step: step,
    onDragEnd: onDragEnd,
    className: className,
    valueLabelDisplay: valueLabelDisplay,
    classes: {
      rail: classes.rail,
      track: classnames(classes.track, trackClass),
      valueLabel: valueLabelClass ? classnames(classes.valueLabel, valueLabelClass) : classes.valueLabel,
      thumb: classnames(classes.thumb, thumbClass)
    },
    valueLabelFormat: valueLabelFormat
  });
};

Slider.propTypes = {
  color: PropTypes.oneOf(colorsList),
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.array]).isRequired,
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  max: PropTypes.number,
  min: PropTypes.number,
  onDragEnd: PropTypes.func,
  step: PropTypes.number,
  className: PropTypes.string,
  valueLabelClass: PropTypes.string,
  thumbClass: PropTypes.string,
  trackClass: PropTypes.string,
  valueLabelDisplay: PropTypes.oneOf(["on", "off", "auto"]),
  valueLabelFormat: PropTypes.func
};
Slider.defaultProps = {
  color: "secondary",
  onChange: function onChange() {},
  disabled: false,
  max: 100,
  min: 0,
  onDragEnd: function onDragEnd() {},
  step: 1,
  className: "",
  valueLabelClass: "",
  thumbClass: "",
  trackClass: "",
  valueLabelDisplay: "auto",
  valueLabelFormat: function valueLabelFormat(v) {
    return v;
  }
};
export default Slider;