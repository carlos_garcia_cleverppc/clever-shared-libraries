import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      typography = _ref$cleverUI.typography,
      shadows = _ref$cleverUI.shadows;
  return {
    rail: {
      backgroundColor: colors.neutral[500],
      height: 3
    },
    track: function track(_ref2) {
      var color = _ref2.color;
      return {
        height: 3,
        color: colors[color][500]
      };
    },
    thumb: function thumb(_ref3) {
      var color = _ref3.color;
      return {
        height: 20,
        width: 20,
        marginTop: -8,
        marginLeft: -9,
        color: colors[color][500]
      };
    },
    valueLabel: {
      fontFamily: typography.font1,
      fontWeight: 700,
      fontSize: "14px",
      width: "42px",
      height: "42px",
      top: "-44px",
      borderRadius: "50%",
      boxShadow: shadows.normal,
      left: "calc(-51% + 0px)",
      "& > :first-child": {
        width: "42px",
        height: "42px"
      }
    }
  };
};

export default makeStyles(styles);