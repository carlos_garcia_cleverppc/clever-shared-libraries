import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";
var getClasses = makeStyles(function (_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      spacing = _ref$cleverUI.spacing,
      colors = _ref$cleverUI.colors;
  return {
    modal: {
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      zIndex: "900 !important"
    },
    paper: _objectSpread(_objectSpread({}, spacing["p-6"]), {}, {
      boxSizing: "border-box",
      minWidth: "min(400px, 90%)",
      maxWidth: "90%",
      position: "relative",
      backgroundColor: colors.white[500],
      borderRadius: "8px"
    }),
    optionsIcon: {
      position: "absolute",
      right: 16,
      top: 8
    },
    menu: {
      borderRadius: "8px",
      "& svg": _objectSpread({}, spacing["mr-3"])
    },
    menuList: {
      margin: "0",
      padding: "8px",
      listStyle: "none",
      maxHeight: 150,
      overflowY: "auto",
      "& li": {
        borderRadius: "4px"
      },
      "&::-webkit-scrollbar": {
        width: "12px",
        backgroundColor: "#fff",
        borderRadius: "8px"
      },
      "&::-webkit-scrollbar-track": {
        backgroundColor: "#fff",
        borderRadius: "8px"
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: colors.neutral[300],
        borderRadius: "16px",
        border: "4px solid #fff"
      }
    },
    title: {
      display: "flex",
      alignItems: "center",
      "& *:first-child": _objectSpread({}, spacing["mr-2"])
    },
    inputContainer: _objectSpread({}, spacing["mb-4"]),
    textField: _objectSpread(_objectSpread(_objectSpread(_objectSpread({}, spacing["px-1"]), spacing["mt-5"]), spacing["mb-2"]), {}, {
      fontSize: "16px !important",
      "&::placeholder": {
        fontWeight: "normal",
        fontSize: "16px",
        color: colors.neutral[400]
      }
    }),
    submitButton: {
      position: "absolute",
      bottom: -50,
      right: 0
    },
    footer: {
      width: "100%"
    }
  };
});
export default getClasses;