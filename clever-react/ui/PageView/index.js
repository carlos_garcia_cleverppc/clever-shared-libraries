import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import Analytics from "../../utils/tracking/analytics";

var PageView = function PageView(_ref) {
  var name = _ref.name;
  useEffect(function () {
    Analytics.pageView(name);
  }, [name]);
  return /*#__PURE__*/React.createElement(React.Fragment, null);
};

PageView.propTypes = {
  name: PropTypes.string.isRequired
};
export default PageView;