import React from "react";
import PropTypes from "prop-types";
import SkeletonMaterial from "@material-ui/lab/Skeleton";
import getClasses from "./styles";

var Skeleton = function Skeleton(props) {
  var classes = getClasses();
  var variant = props.variant,
      className = props.className,
      width = props.width,
      height = props.height,
      children = props.children;
  return /*#__PURE__*/React.createElement(SkeletonMaterial, {
    variant: variant,
    width: width,
    height: height,
    className: className,
    classes: {
      root: classes.root
    }
  }, children);
};

Skeleton.propTypes = {
  variant: PropTypes.oneOf(["text", "circle", "rect"]),
  className: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  height: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};
Skeleton.defaultProps = {
  variant: "text",
  className: "",
  width: undefined,
  height: undefined
};
export default Skeleton;