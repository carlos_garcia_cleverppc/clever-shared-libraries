/* eslint-disable object-curly-newline */
import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var colors = _ref.cleverUI.colors;
  return {
    root: {
      backgroundColor: colors.neutral[300]
    }
  };
};

export default makeStyles(styles);