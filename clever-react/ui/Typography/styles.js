import _typeof from "@babel/runtime/helpers/typeof";
import _inherits from "@babel/runtime/helpers/inherits";
import _setPrototypeOf from "@babel/runtime/helpers/setPrototypeOf";
import _defineProperty from "@babel/runtime/helpers/defineProperty";

function _wrapRegExp() { _wrapRegExp = function _wrapRegExp(re, groups) { return new BabelRegExp(re, undefined, groups); }; var _super = RegExp.prototype; var _groups = new WeakMap(); function BabelRegExp(re, flags, groups) { var _this = new RegExp(re, flags); _groups.set(_this, groups || _groups.get(re)); return _setPrototypeOf(_this, BabelRegExp.prototype); } _inherits(BabelRegExp, RegExp); BabelRegExp.prototype.exec = function (str) { var result = _super.exec.call(this, str); if (result) result.groups = buildGroups(result, this); return result; }; BabelRegExp.prototype[Symbol.replace] = function (str, substitution) { if (typeof substitution === "string") { var groups = _groups.get(this); return _super[Symbol.replace].call(this, str, substitution.replace(/\$<([^>]+)>/g, function (_, name) { return "$" + groups[name]; })); } else if (typeof substitution === "function") { var _this = this; return _super[Symbol.replace].call(this, str, function () { var args = arguments; if (_typeof(args[args.length - 1]) !== "object") { args = [].slice.call(args); args.push(buildGroups(args, _this)); } return substitution.apply(this, args); }); } else { return _super[Symbol.replace].call(this, str, substitution); } }; function buildGroups(result, re) { var g = _groups.get(re); return Object.keys(g).reduce(function (groups, name) { groups[name] = result[g[name]]; return groups; }, Object.create(null)); } return _wrapRegExp.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var isHeading = function isHeading(name) {
  return name.match(/h[1,2,3,4,5,6]/i);
};

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      typography = _ref$cleverUI.typography,
      spacing = _ref$cleverUI.spacing,
      breakpoints = _ref.breakpoints;
  var sizing = typography.sizing,
      font1 = typography.font1,
      font2 = typography.font2;

  var getSpecialStyles = function getSpecialStyles(variant) {
    switch (variant) {
      case "h1":
        return _defineProperty({}, breakpoints.down("sm"), {
          fontSize: "28px"
        });

      case "h3":
        return {
          textTransform: "uppercase"
        };

      default:
        return {};
    }
  };

  var getStylesFromSizing = function getStylesFromSizing(variant) {
    var heading = isHeading(variant);
    var fontSize = sizing[variant];
    var fontFamily = heading ? font1 : font2;
    var specialStyles = getSpecialStyles(variant);
    return _objectSpread(_objectSpread({
      fontSize: fontSize,
      fontFamily: fontFamily
    }, heading && {
      fontWeight: "700"
    }), specialStyles);
  };

  var getStylesFromString = function getStylesFromString(str) {
    var regexpStyles = /*#__PURE__*/_wrapRegExp(/(f[,12])\x2D([0-9]+)/, {
      family: 1,
      size: 2
    });

    var match = regexpStyles.exec(str);

    if (!match) {
      // eslint-disable-next-line no-console
      console.warn("[Typography]: invalid variant");
      return {};
    }

    var FONT_FAMILIES = {
      f1: font1,
      f2: font2
    };
    var _match$groups = match.groups,
        family = _match$groups.family,
        size = _match$groups.size;
    return {
      fontFamily: FONT_FAMILIES[family],
      fontSize: "".concat(size, "px")
    };
  };

  var getVariantStyles = function getVariantStyles(variant) {
    var styleObject = sizing[variant] ? getStylesFromSizing(variant) : getStylesFromString(variant);
    return styleObject;
  };

  return {
    root: function root(_ref3) {
      var color = _ref3.color,
          shade = _ref3.shade,
          align = _ref3.align,
          variant = _ref3.variant;
      return _objectSpread({
        margin: 0,
        color: colors[color] ? colors[color][shade] : color,
        whiteSpace: "pre-line",
        lineHeight: typography.lineHeight,
        textAlign: align
      }, getVariantStyles(variant));
    },
    mb: function mb() {
      return spacing["mb-3"];
    },
    upperCase: {
      textTransform: "uppercase!important"
    },
    downCase: {
      textTransform: "lowercase!important"
    },
    capitalize: {
      textTransform: "capitalize!important"
    },
    light: {
      fontWeight: "300!important"
    },
    normal: {
      fontWeight: "400!important"
    },
    bold: {
      fontWeight: "700!important"
    }
  };
};

export default makeStyles(styles);