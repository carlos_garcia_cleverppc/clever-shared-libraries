import React from "react";
import PropTypes from "prop-types";
import HiddenMaterial from "@material-ui/core/Hidden";

var Hidden = function Hidden(props) {
  var only = props.only,
      xsUp = props.xsUp,
      smUp = props.smUp,
      mdUp = props.mdUp,
      lgUp = props.lgUp,
      xlUp = props.xlUp,
      xsDown = props.xsDown,
      smDown = props.smDown,
      mdDown = props.mdDown,
      lgDown = props.lgDown,
      xlDown = props.xlDown,
      children = props.children;

  if (only) {
    return /*#__PURE__*/React.createElement(HiddenMaterial, {
      only: only,
      xsUp: xsUp,
      smUp: smUp,
      mdUp: mdUp,
      lgUp: lgUp,
      xlUp: xlUp,
      xsDown: xsDown,
      smDown: smDown,
      mdDown: mdDown,
      lgDown: lgDown,
      xlDown: xlDown
    }, children);
  }

  return /*#__PURE__*/React.createElement(HiddenMaterial, {
    xsUp: xsUp,
    smUp: smUp,
    mdUp: mdUp,
    lgUp: lgUp,
    xlUp: xlUp,
    xsDown: xsDown,
    smDown: smDown,
    mdDown: mdDown,
    lgDown: lgDown,
    xlDown: xlDown
  }, children);
};

Hidden.propTypes = {
  only: PropTypes.oneOfType([PropTypes.bool, PropTypes.oneOf(["xs", "sm", "md", "lg", "xl"]), PropTypes.arrayOf(PropTypes.oneOf(["xs", "sm", "md", "lg", "xl"]))]),
  xsUp: PropTypes.bool,
  smUp: PropTypes.bool,
  mdUp: PropTypes.bool,
  lgUp: PropTypes.bool,
  xlUp: PropTypes.bool,
  xsDown: PropTypes.bool,
  smDown: PropTypes.bool,
  mdDown: PropTypes.bool,
  lgDown: PropTypes.bool,
  xlDown: PropTypes.bool
};
Hidden.defaultProps = {
  only: false,
  xsUp: false,
  smUp: false,
  mdUp: false,
  lgUp: false,
  xlUp: false,
  xsDown: false,
  smDown: false,
  mdDown: false,
  lgDown: false,
  xlDown: false
};
export default Hidden;