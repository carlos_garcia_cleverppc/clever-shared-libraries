import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing,
      typography = _ref$cleverUI.typography;
  return {
    root: {
      width: "384px",
      minHeight: "82px",
      position: "relative"
    },
    content: function content(_ref2) {
      var _colors$color;

      var color = _ref2.color;
      return {
        height: "100%",
        color: "white",
        backgroundColor: ((_colors$color = colors[color]) === null || _colors$color === void 0 ? void 0 : _colors$color[500]) || colors.primary[500],
        borderRadius: "8px"
      };
    },
    container: {
      flexWrap: "nowrap",
      "& > :last-child": {
        marginRight: 0 // remove negative margin of Material ui

      }
    },
    marginY: _objectSpread({}, spacing["my-3"]),
    marginX: _objectSpread({}, spacing["mx-2"]),
    timesIcon: {
      "& button": {
        paddingTop: "4px"
      },
      "& path": {
        fill: "white !important"
      }
    },
    link: _objectSpread(_objectSpread({}, spacing["ml-1"]), {}, {
      fontFamily: typography.font2,
      fontSize: "12px"
    }),
    image: _objectSpread(_objectSpread({}, spacing["mb-2"]), {}, {
      height: "76px",
      width: "auto",
      borderRadius: "8px",
      marginRight: "0px"
    })
  };
};

export default makeStyles(styles);