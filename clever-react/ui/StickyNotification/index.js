import _extends from "@babel/runtime/helpers/extends";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["color", "image", "anchorEl", "id", "footer", "textLink", "onClickLink"];
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import SnackbarMaterial from "@material-ui/core/Snackbar";
import { getColorsLabels } from "../styles/colors";
import Grid from "../Grid";
import IconButton from "../IconButton";
import Image from "../Image";
import Times from "../Icon/Times";
import Typography from "../Typography";
import Link from "../Link";
import getClasses from "./styles";
var colorsList = getColorsLabels();
var StickyNotification = /*#__PURE__*/React.forwardRef(function (props, ref) {
  var className = props.className,
      onClose = props.onClose,
      open = props.open,
      title = props.title,
      description = props.description;

  var color = props.color,
      image = props.image,
      anchorEl = props.anchorEl,
      id = props.id,
      footer = props.footer,
      textLink = props.textLink,
      onClickLink = props.onClickLink,
      otherProps = _objectWithoutProperties(props, _excluded);

  var classes = getClasses({
    color: color
  });
  return /*#__PURE__*/React.createElement(SnackbarMaterial, _extends({
    ref: ref,
    className: classnames([className, classes.root]),
    onClose: onClose,
    open: open
  }, otherProps, {
    ClickAwayListenerProps: {
      onClickAway: function onClickAway() {}
    }
  }), /*#__PURE__*/React.createElement("div", {
    className: classes.content
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    className: classes.container
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 1,
    className: classnames(classes.marginX, classes.marginY, classes.timesIcon)
  }, /*#__PURE__*/React.createElement(IconButton, {
    icon: /*#__PURE__*/React.createElement(Times, null),
    color: "white",
    onClick: onClose,
    iconSize: 16
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 9,
    className: classnames(classes.marginY)
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "f1-14",
    weight: "bold",
    color: "white"
  }, title), /*#__PURE__*/React.createElement(Typography, {
    variant: "f2-12",
    color: "white"
  }, description, textLink && /*#__PURE__*/React.createElement(Link, {
    className: classes.link,
    onClick: onClickLink,
    color: "white"
  }, textLink))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 2
  }, /*#__PURE__*/React.createElement(Image, {
    src: image,
    className: classes.image
  }))), footer));
});
StickyNotification.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  open: PropTypes.bool,
  onClose: PropTypes.func,
  textLink: PropTypes.string,
  onClickLink: PropTypes.func,
  color: PropTypes.oneOf(colorsList),
  anchorOrigin: PropTypes.exact({
    vertical: PropTypes.oneOf(["top", "bottom"]),
    horizontal: PropTypes.oneOf(["left", "right", "center"])
  }),
  footer: PropTypes.element
};
StickyNotification.defaultProps = {
  title: "",
  description: "",
  open: false,
  onClose: function onClose() {},
  textLink: "",
  onClickLink: function onClickLink() {},
  color: "primary",
  anchorOrigin: {
    vertical: "top",
    horizontal: "right"
  }
};
export default StickyNotification;