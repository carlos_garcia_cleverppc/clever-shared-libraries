import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import MaterialPaper from "@material-ui/core/Paper";
import { getColorsLabels, getShades } from "../styles/colors";
import getClasses from "./styles";
var colorsList = getColorsLabels();
var shadesList = getShades();

var Paper = function Paper(props) {
  var children = props.children,
      className = props.className,
      color = props.color,
      shade = props.shade;
  var classes = getClasses({
    color: color,
    shade: shade
  });
  return /*#__PURE__*/React.createElement(MaterialPaper, {
    className: classnames(classes.root, className)
  }, children);
};

Paper.propTypes = {
  className: PropTypes.string,
  color: PropTypes.oneOf(colorsList),
  shade: PropTypes.oneOf(shadesList)
};
Paper.defaultProps = {
  className: "",
  color: "primary",
  shade: 500
};
export default Paper;