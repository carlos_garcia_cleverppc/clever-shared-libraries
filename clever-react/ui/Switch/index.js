import React from "react";
import PropTypes from "prop-types";
import SwitchMaterial from "@material-ui/core/Switch";
import { getColorsLabels } from "../styles/colors";
import Loading from "../Loading";
import getClasses from "./styles";
var colorsList = getColorsLabels();
colorsList.push("");

var Switch = function Switch(props) {
  var checked = props.checked,
      onChange = props.onChange,
      disabled = props.disabled,
      name = props.name,
      className = props.className,
      loading = props.loading,
      spinner = props.spinner,
      color = props.color,
      size = props.size;
  var classes = getClasses({
    color: color,
    size: size
  });

  if (loading) {
    return /*#__PURE__*/React.createElement("div", {
      className: classes.loadingContainer
    }, spinner ? /*#__PURE__*/React.createElement("img", {
      src: spinner,
      alt: "spinner",
      className: classes.loadingSpinner
    }) : /*#__PURE__*/React.createElement(Loading, {
      size: 13,
      color: "neutral",
      center: true
    }));
  }

  return /*#__PURE__*/React.createElement(SwitchMaterial, {
    checked: checked,
    onChange: onChange,
    className: className,
    name: name,
    disabled: disabled,
    focusVisibleClassName: classes.focusVisible,
    disableRipple: true,
    classes: {
      root: classes.root,
      switchBase: classes.switchBase,
      thumb: classes.thumb,
      track: classes.track,
      checked: classes.checked
    }
  });
};

Switch.propTypes = {
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  checked: PropTypes.bool,
  className: PropTypes.string,
  name: PropTypes.string,
  spinner: PropTypes.string,
  color: PropTypes.oneOf(colorsList),
  size: PropTypes.oneOf(["small", "standard"])
};
Switch.defaultProps = {
  checked: true,
  onChange: function onChange() {},
  disabled: false,
  name: "",
  loading: false,
  color: "",
  size: "standard"
};
export default Switch;