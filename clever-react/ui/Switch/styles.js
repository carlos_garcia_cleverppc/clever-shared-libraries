import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    root: function root(_ref2) {
      var size = _ref2.size;
      return _objectSpread({
        width: size === "standard" ? 46 : 28,
        height: size === "standard" ? 26 : 16,
        padding: 0
      }, spacing["m-1"]);
    },
    switchBase: function switchBase(_ref3) {
      var size = _ref3.size;
      return {
        top: size === "standard" ? 1 : 0,
        left: "2px",
        padding: 0,
        paddingTop: "1px"
      };
    },
    thumb: function thumb(_ref4) {
      var size = _ref4.size;
      return {
        width: size === "standard" ? 22 : 14,
        height: size === "standard" ? 22 : 14,
        color: "white"
      };
    },
    track: {
      borderRadius: 26 / 2,
      backgroundColor: "".concat(colors.neutral[500]),
      opacity: 0.5
    },
    checked: function checked(_ref5) {
      var color = _ref5.color,
          size = _ref5.size;
      return {
        transform: size === "standard" ? "translateX(19px) !important" : "translateX(11px) !important",
        "& + .MuiSwitch-track": {
          backgroundColor: "".concat(!color ? "#52d869" : colors[color][500], " !important"),
          opacity: "".concat(1, " !important"),
          border: "none"
        }
      };
    },
    loadingContainer: function loadingContainer(_ref6) {
      var size = _ref6.size;
      return _objectSpread(_objectSpread({
        width: size === "standard" ? 46 : 28,
        height: size === "standard" ? 26 : 16,
        backgroundColor: colors.neutral[300],
        borderRadius: 26 / 2
      }, spacing["m-1"]), {}, {
        display: "flex"
      });
    },
    loadingSpinner: function loadingSpinner(_ref7) {
      var size = _ref7.size;
      return {
        width: size === "standard" ? 20 : 12,
        height: size === "standard" ? 20 : 12,
        marginLeft: size === "standard" ? 12 : 8,
        alignSelf: "center"
      };
    }
  };
};

export default makeStyles(styles);