/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Card from "../Card";
import Checkbox from "../Checkbox";
import Typography from "../Typography";
import getClasses from "./styles";

var CheckAccountItem = function CheckAccountItem(props) {
  var classes = getClasses();
  var name = props.name,
      id = props.id,
      currency = props.currency,
      timezone = props.timezone;
  var isSelected = props.isSelected,
      disabled = props.disabled,
      onClick = props.onClick,
      color = props.color;
  return /*#__PURE__*/React.createElement(Card, {
    className: classnames(classes.root, disabled ? "" : classes.choosable),
    clickable: !disabled,
    onClick: disabled ? function () {} : onClick
  }, /*#__PURE__*/React.createElement(Checkbox, {
    checked: isSelected,
    onChange: onClick,
    value: "privacy",
    disabled: disabled,
    color: color
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.label
  }, !disabled && /*#__PURE__*/React.createElement(Typography, {
    variant: "body1",
    shade: 900,
    weight: "bold",
    className: classes.longText
  }, name), disabled && /*#__PURE__*/React.createElement(Typography, {
    variant: "body1",
    weight: "bold",
    className: classes.longText
  }, name), !!id && /*#__PURE__*/React.createElement(Typography, {
    variant: "body1",
    className: classes.longText
  }, id), (!!currency || !!timezone) && /*#__PURE__*/React.createElement(Typography, {
    variant: "body0",
    className: classes.longText
  }, currency, !!currency && !!timezone ? " - " : "", timezone))));
};

CheckAccountItem.propTypes = {
  isSelected: PropTypes.bool,
  name: PropTypes.string,
  id: PropTypes.string,
  currency: PropTypes.string,
  timezone: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  color: PropTypes.oneOf(["primary", "secondary"])
};
CheckAccountItem.defaultProps = {
  isSelected: false,
  name: "",
  id: "",
  currency: "",
  timezone: "",
  disabled: false,
  onClick: function onClick() {},
  color: "primary"
};
export default CheckAccountItem;