import React from "react";
import PropTypes from "prop-types";
import InputAdornment from "@material-ui/core/InputAdornment";

function CleverInputAdornment(props) {
  var className = props.className,
      children = props.children,
      position = props.position;
  return /*#__PURE__*/React.createElement(InputAdornment, {
    className: className,
    position: position
  }, children);
}

CleverInputAdornment.propTypes = {
  className: PropTypes.string,
  position: PropTypes.string
};
CleverInputAdornment.defaultProps = {
  className: "",
  position: "start"
};
export default CleverInputAdornment;