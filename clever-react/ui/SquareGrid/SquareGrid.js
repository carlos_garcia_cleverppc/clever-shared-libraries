/* eslint-disable import/prefer-default-export */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import getClasses from "./styles";
import { Square } from ".";
export var SquareGrid = function SquareGrid(props) {
  var children = props.children,
      gap = props.gap,
      dense = props.dense,
      rowHeight = props.rowHeight,
      columnWidth = props.columnWidth,
      columns = props.columns,
      className = props.className;
  var classes = getClasses({
    gap: gap,
    dense: dense,
    rowHeight: rowHeight,
    columnWidth: columnWidth,
    columns: columns
  });
  return /*#__PURE__*/React.createElement("div", {
    className: classnames([className, classes.grid])
  }, children);
};
SquareGrid.propTypes = {
  /**
   * Use the empty cell even if the order has to be changed
   */
  dense: PropTypes.bool,
  rowHeight: PropTypes.string,
  columnWidth: PropTypes.string,
  columns: PropTypes.number,
  gap: PropTypes.string
};
SquareGrid.defaultProps = {
  dense: true,
  rowHeight: "160px",
  gap: "24px",
  columnWidth: "310px",
  columns: 2
};