import makeStyles from "../styles/makeStyles";
var styles = {
  grid: function grid(props) {
    return {
      "--gap": props.gap,
      "--column-size": props.columnWidth,
      "--columns-amount": props.columns,
      "--column-size-max": "calc(100% / var(--columns-amount) - var(--gap))",
      "--column-size-min": "min(var(--column-size), 100%)",
      // prevent overflow
      display: "grid",
      gridTemplateColumns: "repeat(auto-fit, minmax(max(var(--column-size-min), var(--column-size-max)), 1fr))",
      gridAutoFlow: props.dense ? "row dense" : "row",
      gridAutoRows: "minmax(".concat(props.rowHeight, ", auto)"),
      gap: "var(--gap)"
    };
  },
  fullWidth: {
    gridColumnStart: 1,
    gridColumnEnd: -1
  },
  item: {
    "& > *": {
      height: "100%",
      width: "100%",
      boxSizing: "border-box"
    }
  },
  withHeight: function withHeight(props) {
    return {
      gridRowEnd: "span ".concat(props.height)
    };
  }
};
export default makeStyles(styles);