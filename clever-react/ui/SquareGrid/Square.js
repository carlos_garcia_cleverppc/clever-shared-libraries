import _defineProperty from "@babel/runtime/helpers/defineProperty";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["children", "fullWidth", "height", "as", "className"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable import/prefer-default-export */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import getClasses from "./styles";
export var Square = function Square(_ref) {
  var _additionalClasses;

  var children = _ref.children,
      fullWidth = _ref.fullWidth,
      height = _ref.height,
      as = _ref.as,
      className = _ref.className,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = getClasses({
    height: height
  });
  var additionalClasses = (_additionalClasses = {}, _defineProperty(_additionalClasses, classes.withHeight, height > 1), _defineProperty(_additionalClasses, classes.fullWidth, fullWidth), _additionalClasses);
  return /*#__PURE__*/React.createElement(as, _objectSpread({
    className: classnames(className, classes.item, additionalClasses)
  }, props), children);
};
Square.propTypes = {
  fullWidth: PropTypes.bool,

  /**
  Number of rows
  */
  height: PropTypes.number,

  /**
  This component will be used to render the cell.
  */
  as: PropTypes.oneOfType([PropTypes.string, PropTypes.elementType])
};
Square.defaultProps = {
  fullWidth: false,
  height: 1,
  as: "div"
};