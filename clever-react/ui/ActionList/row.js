/* eslint-disable react-hooks/exhaustive-deps */
import React, { forwardRef } from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import ListItem from "../List/Item";
import IconButton from "../IconButton";
import Typography from "../Typography";
import Times from "../Icon/Times";
import Plus from "../Icon/Plus";
import getClasses from "./styles"; // eslint-disable-next-line import/no-cycle

import { ListContext } from "./index"; // eslint-disable-next-line no-unused-vars

var Row = /*#__PURE__*/forwardRef(function (props, ref) {
  var item = props.item,
      index = props.index,
      variant = props.variant,
      className = props.className,
      _onClick = props.onClick,
      action = props.action;
  var classes = getClasses({
    variant: variant
  });

  var _React$useContext = React.useContext(ListContext),
      setSize = _React$useContext.setSize;

  var root = React.useRef();
  React.useEffect(function () {
    setSize(index, root.current.getBoundingClientRect().height);
  }, []);
  return /*#__PURE__*/React.createElement("div", {
    ref: root
  }, /*#__PURE__*/React.createElement(ListItem, {
    ref: root,
    padding: false,
    className: classnames(index % 2 === 1 ? classes.rowColor : classes.rowColorWhite, classes.list, className)
  }, /*#__PURE__*/React.createElement(Typography, {
    color: variant || "neutral",
    className: classnames(classes.itemText)
  }, item.label), /*#__PURE__*/React.createElement(IconButton, {
    size: "small",
    onClick: function onClick(e) {
      return _onClick(e, item.id);
    },
    icon: action === "remove" ? /*#__PURE__*/React.createElement(Times, null) : /*#__PURE__*/React.createElement(Plus, null),
    color: !variant ? "neutral" : variant,
    shade: 100,
    outlined: false,
    className: classes.icon
  })));
});
Row.propTypes = {
  variant: PropTypes.string
};
Row.defaultProps = {
  variant: "neutral"
};
export default Row;