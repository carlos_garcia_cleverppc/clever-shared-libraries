/* eslint-disable react/no-multi-comp */

/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { CSSTransition, TransitionGroup } from "react-transition-group"; // import ReactCSSTransitionGroup from "react-addons-css-transition-group";

import ListItem from "../List/Item";
import IconButton from "../IconButton";
import Typography from "../Typography";
import Grid from "../Grid";
import Times from "../Icon/Times";
import Plus from "../Icon/Plus";
import getClasses from "./styles";

var ActionList = function ActionList(_ref) {
  var className = _ref.className,
      listInfo = _ref.listInfo,
      _onClick = _ref.onClick,
      variant = _ref.variant,
      action = _ref.action,
      emptyText = _ref.emptyText;
  var classes = getClasses({
    variant: variant
  });

  var Row = function Row(index) {
    return /*#__PURE__*/React.createElement(CSSTransition, {
      key: index,
      timeout: 500,
      classNames: {
        enter: classes.moveEnter,
        enterActive: classes.moveEnterActive,
        exit: classes.moveExit,
        exitActive: classes.moveExitActive
      }
    }, /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(ListItem, {
      className: classnames(index % 2 === 1 ? classes.rowColor : classes.rowColorWhite, classes.list, className)
    }, /*#__PURE__*/React.createElement(Typography, {
      color: variant || "inherit",
      className: classnames(classes.itemText)
    }, listInfo[index].label), /*#__PURE__*/React.createElement(IconButton, {
      size: "small",
      onClick: function onClick(e) {
        return _onClick(e, listInfo[index].id);
      },
      icon: action === "remove" ? /*#__PURE__*/React.createElement(Times, null) : /*#__PURE__*/React.createElement(Plus, null),
      color: !variant ? "neutral" : variant,
      shade: 100,
      outlined: false,
      className: classes.icon
    }))));
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, listInfo.length > 0 && /*#__PURE__*/React.createElement(TransitionGroup, null, listInfo.map(function (item, index) {
    return Row(index);
  })), listInfo.length === 0 && /*#__PURE__*/React.createElement(Grid, {
    container: true,
    justify: "center",
    alignItems: "center",
    className: classnames(classes.root, classes.emptyList)
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body1"
  }, emptyText))));
};

ActionList.propTypes = {
  className: PropTypes.string,
  listInfo: PropTypes.instanceOf(Object).isRequired,
  onClick: PropTypes.func,
  variant: PropTypes.oneOf(["", "primary"]),
  action: PropTypes.oneOf(["add", "remove"]),
  emptyText: PropTypes.string
};
ActionList.defaultProps = {
  className: "",
  onClick: function onClick() {},
  variant: "primary",
  action: "add",
  emptyText: ""
};
export default ActionList;