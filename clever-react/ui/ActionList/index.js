import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import React, { useCallback } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { VariableSizeList as List } from "react-window";
import Typography from "../Typography";
import Grid from "../Grid";
import getClasses from "./styles"; // eslint-disable-next-line import/no-cycle

import Row from "./row";
export var ListContext = /*#__PURE__*/React.createContext({});

var ActionList = function ActionList(_ref) {
  var className = _ref.className,
      listInfo = _ref.listInfo,
      onClick = _ref.onClick,
      variant = _ref.variant,
      action = _ref.action,
      emptyText = _ref.emptyText;
  var classes = getClasses({
    variant: variant
  });
  var sizeMap = React.useRef({});
  var listRef = React.useRef();
  var setSize = useCallback(function (index, size) {
    if (!sizeMap[index]) {
      sizeMap.current = _objectSpread(_objectSpread({}, sizeMap.current), {}, _defineProperty({}, index, size));
      listRef.current.resetAfterIndex(index);
    }
  }, []);
  var getSize = useCallback(function (index) {
    return sizeMap.current[index] || 45;
  }, []);
  return /*#__PURE__*/React.createElement(ListContext.Provider, {
    value: {
      setSize: setSize
    }
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.optimizedList
  }, listInfo.length > 0 && /*#__PURE__*/React.createElement(List, {
    className: classes.root,
    height: 400,
    itemCount: listInfo.length,
    itemSize: getSize,
    width: "100%",
    ref: listRef
  }, function (_ref2) {
    var index = _ref2.index,
        style = _ref2.style;
    return /*#__PURE__*/React.createElement("div", {
      style: style
    }, /*#__PURE__*/React.createElement(Row, {
      variant: variant,
      index: index,
      item: listInfo[index],
      className: className,
      onClick: onClick,
      action: action
    }));
  }), listInfo.length === 0 && /*#__PURE__*/React.createElement(Grid, {
    container: true,
    justify: "center",
    alignItems: "center",
    className: classnames(classes.root, classes.emptyList)
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body1"
  }, emptyText)))));
};

ActionList.propTypes = {
  className: PropTypes.string,
  listInfo: PropTypes.instanceOf(Object).isRequired,
  onClick: PropTypes.func,
  variant: PropTypes.oneOf(["", "primary"]),
  action: PropTypes.oneOf(["add", "remove"]),
  emptyText: PropTypes.string
};
ActionList.defaultProps = {
  className: "",
  onClick: function onClick() {},
  variant: "",
  action: "add",
  emptyText: ""
};
export default ActionList;