import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    root: {
      overflow: "auto",
      border: "solid 1px ".concat(colors.neutral[300]),
      borderRadius: "5px",
      "&::-webkit-scrollbar": {
        width: "12px",
        backgroundColor: "#fff"
      },
      "&::-webkit-scrollbar-track": {
        backgroundColor: "#fff"
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: colors.neutral[300],
        borderRadius: "16px",
        border: "4px solid #fff"
      }
    },
    emptyList: {
      height: "400px"
    },
    rowColor: function rowColor(_ref2) {
      var variant = _ref2.variant;
      return {
        backgroundColor: !variant ? "".concat(colors.neutral[200], " !important") : "".concat(colors[variant][100], " !important")
      };
    },
    rowColorWhite: {
      backgroundColor: "white !important"
    },
    icon: _objectSpread({}, spacing["ml-auto"]),
    list: _objectSpread(_objectSpread({}, spacing["p-3"]), {}, {
      "& p": {
        wordBreak: "break-word"
      }
    }),
    itemText: function itemText(_ref3) {
      var variant = _ref3.variant;
      return {
        color: !variant ? colors.neutral[700] : colors[variant][500]
      };
    },
    enter: {
      opacity: "0.01",
      transform: "translate(-40px, 0)"
    },
    enterActive: {
      opacity: "1",
      transform: "translate(0, 0)",
      transition: "all 500ms ease-in"
    },
    leave: {
      opacity: "1",
      transform: "translate(0, 0)"
    },
    leaveActive: {
      opacity: "0.01",
      transform: "translate(40px, 0)",
      transition: "all 500ms ease-in"
    },
    moveEnter: {
      opacity: "0.01",
      transform: "translate(-40px, 0)"
    },
    moveEnterActive: {
      opacity: "1",
      transform: "translate(0, 0)",
      transition: "all 500ms ease-in"
    },
    moveExit: {
      opacity: "1",
      transform: "translate(0, 0)"
    },
    moveExitActive: {
      opacity: "0.01",
      transform: "translate(-40px, 0)",
      transition: "all 500ms ease-in-out"
    },
    optimizedList: {
      width: "100%",
      height: "400px"
    }
  };
};

export default makeStyles(styles);