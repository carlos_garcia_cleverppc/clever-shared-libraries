import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function CalendarAlt(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M8.455 11.818a.545.545 0 1 0 0-1.09.545.545 0 0 0 0 1.09zm2.727 0a.545.545 0 1 0 0-1.09.545.545 0 0 0 0 1.09zm0-2.182a.545.545 0 1 0 0-1.09.545.545 0 0 0 0 1.09zm-2.727 0a.545.545 0 1 0 0-1.09.545.545 0 0 0 0 1.09zm3.818-6.545h-.546v-.546a.545.545 0 0 0-1.09 0v.546H6.272v-.546a.545.545 0 0 0-1.091 0v.546h-.546C3.733 3.09 3 3.824 3 4.727v7.637C3 13.267 3.733 14 4.636 14h7.637c.904 0 1.636-.733 1.636-1.636V4.727c0-.903-.732-1.636-1.636-1.636zm.545 9.273a.545.545 0 0 1-.545.545H4.636a.545.545 0 0 1-.545-.545v-4.91h8.727v4.91zm0-6H4.091V4.727c0-.301.244-.545.545-.545h.546v.545a.545.545 0 1 0 1.09 0v-.545h4.364v.545a.545.545 0 1 0 1.091 0v-.545h.546c.301 0 .545.244.545.545v1.637zm-7.09 3.272a.545.545 0 1 0 0-1.09.545.545 0 0 0 0 1.09zm0 2.182a.545.545 0 1 0 0-1.09.545.545 0 0 0 0 1.09z",
    fill: colorIcon
  }));
}

CalendarAlt.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
CalendarAlt.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default CalendarAlt;