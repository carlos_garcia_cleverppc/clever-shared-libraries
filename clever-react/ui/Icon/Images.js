import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Images(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M12.8 10.4v-8A2.4 2.4 0 0 0 10.4 0h-8A2.4 2.4 0 0 0 0 2.4v8a2.4 2.4 0 0 0 2.4 2.4h8a2.4 2.4 0 0 0 2.4-2.4zm-11.2-8a.8.8 0 0 1 .8-.8h8a.8.8 0 0 1 .8.8v3.488l-.864-.872a2.048 2.048 0 0 0-2.896 0l-.728.728-.648-.648a2.344 2.344 0 0 0-3.288 0L1.6 6.28V2.4zm.096 8.36a.752.752 0 0 1-.096-.36V8.536l2.304-2.304a.728.728 0 0 1 1.032 0l.664.648-3.904 3.88zm6.88-4.608a.416.416 0 0 1 .312-.136.416.416 0 0 1 .312.136l2 1.992V10.4a.8.8 0 0 1-.8.8H3.52l5.056-5.048zM15.2 3.2a.8.8 0 0 0-.8.8v8a2.4 2.4 0 0 1-2.4 2.4H4A.8.8 0 0 0 4 16h8a4 4 0 0 0 4-4V4a.8.8 0 0 0-.8-.8z",
    fill: colorIcon
  }));
}

Images.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Images.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Images;