import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Support(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M10.513 3.247c.319.18.724.07.907-.247a.667.667 0 0 1 1.247.333A.667.667 0 0 1 12 4a.667.667 0 1 0 0 1.333 2 2 0 1 0-1.733-3 .666.666 0 0 0 .246.914zM13.38 8a.667.667 0 0 0-.747.573A4.667 4.667 0 0 1 8 12.667H4.273l.434-.434a.667.667 0 0 0 0-.94A4.667 4.667 0 0 1 8 3.333.667.667 0 0 0 8 2a6 6 0 0 0-4.667 9.74l-1.14 1.12a.667.667 0 0 0-.14.727.667.667 0 0 0 .614.413H8a6 6 0 0 0 5.953-5.247A.666.666 0 0 0 13.38 8zm-1.127-1.953A.666.666 0 0 0 11.867 6l-.12.04-.12.06-.1.087a.666.666 0 0 0-.14.213.56.56 0 0 0-.054.267.668.668 0 0 0 .047.26c.034.08.084.152.147.213a.667.667 0 0 0 .473.193.667.667 0 0 0 .667-.666.56.56 0 0 0-.054-.254.713.713 0 0 0-.36-.36v-.006z",
    fill: colorIcon
  }));
}

Support.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Support.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Support;