import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Binoculars(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M3.473 7.398A3.477 3.477 0 0 0 0 10.871a3.477 3.477 0 0 0 3.473 3.473 3.477 3.477 0 0 0 3.473-3.473 3.477 3.477 0 0 0-3.473-3.473zm0 5.91a2.44 2.44 0 0 1-2.436-2.437 2.439 2.439 0 0 1 2.436-2.436 2.439 2.439 0 0 1 2.436 2.436 2.439 2.439 0 0 1-2.436 2.437z",
    fill: colorIcon
  }), /*#__PURE__*/React.createElement("path", {
    d: "M5.237 2.032a2.165 2.165 0 0 0-1.623.365 2.155 2.155 0 0 0-.89 1.387L.179 9.824a.519.519 0 0 0 .955.402l2.592-6.151a.519.519 0 0 0 .04-.206 1.142 1.142 0 0 1 1.295-.815c.297.051.557.217.733.467.175.25.244.553.192.852a.524.524 0 0 0-.007.08l-.104 6.825a.518.518 0 0 0 .51.526h.008a.518.518 0 0 0 .518-.51l.103-6.787a2.178 2.178 0 0 0-1.777-2.475z",
    fill: colorIcon
  }), /*#__PURE__*/React.createElement("path", {
    d: "M9.503 7.122H6.48v1.036h3.024V7.122zM9.503 8.919H6.48v1.037h3.024V8.919z",
    fill: colorIcon
  }), /*#__PURE__*/React.createElement("path", {
    d: "M12.527 7.398a3.477 3.477 0 0 0-3.473 3.473 3.477 3.477 0 0 0 3.473 3.473A3.477 3.477 0 0 0 16 10.871a3.477 3.477 0 0 0-3.473-3.473zm0 5.91a2.44 2.44 0 0 1-2.436-2.437 2.44 2.44 0 0 1 2.436-2.436 2.44 2.44 0 0 1 2.436 2.436 2.439 2.439 0 0 1-2.436 2.437z",
    fill: colorIcon
  }), /*#__PURE__*/React.createElement("path", {
    d: "m15.821 9.823-2.544-6.038a2.18 2.18 0 0 0-2.514-1.753 2.155 2.155 0 0 0-1.406.894 2.167 2.167 0 0 0-.371 1.58l.103 6.788a.518.518 0 0 0 1.036-.016l-.103-6.825a.544.544 0 0 0-.008-.08 1.136 1.136 0 0 1 .192-.852c.176-.25.436-.416.733-.467.581-.1 1.14.259 1.294.814 0 .07.012.14.04.207l2.593 6.151a.519.519 0 0 0 .955-.403z",
    fill: colorIcon
  }), /*#__PURE__*/React.createElement("path", {
    d: "M9.52 3.943H6.48v1.036h3.04V3.943z",
    fill: colorIcon
  }));
}

Binoculars.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Binoculars.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Binoculars;