import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function MoneyBill(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M4 7.333a.667.667 0 1 0 0 1.334.667.667 0 0 0 0-1.334zm8 0a.667.667 0 1 0 0 1.334.667.667 0 0 0 0-1.334zm1.333-4H2.667a2 2 0 0 0-2 2v5.334a2 2 0 0 0 2 2h10.666a2 2 0 0 0 2-2V5.333a2 2 0 0 0-2-2zM14 10.667a.667.667 0 0 1-.667.666H2.667A.667.667 0 0 1 2 10.667V5.333c0-.368.298-.666.667-.666h10.666c.368 0 .667.298.667.666v5.334zM8 6a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm0 2.667a.667.667 0 1 1 0-1.334.667.667 0 0 1 0 1.334z",
    fill: colorIcon
  }));
}

MoneyBill.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
MoneyBill.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default MoneyBill;