import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Infinity(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 13",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M14.92 3.318C14.184 2.468 13.148 2 12 2c-1.415 0-2.48.832-3.227 1.754.284.445.505.87.666 1.215.573-.842 1.432-1.694 2.561-1.694 1.594 0 2.75 1.314 2.75 3.125 0 1.81-1.156 3.125-2.75 3.125-2.278 0-3.397-3.295-3.407-3.327l-.003-.01C8.53 6.018 7.11 2 4 2c-1.148 0-2.184.468-2.92 1.318C.384 4.123 0 5.218 0 6.4c0 1.183.384 2.277 1.08 3.082.736.85 1.772 1.318 2.92 1.318 1.427 0 2.486-.81 3.226-1.72-.283-.444-.502-.87-.662-1.221C5.998 8.69 5.146 9.525 4 9.525c-1.593 0-2.75-1.314-2.75-3.125 0-1.81 1.157-3.125 2.75-3.125 2.199 0 3.372 3.23 3.41 3.334.083.25 1.445 4.191 4.59 4.191 1.148 0 2.184-.468 2.92-1.318.696-.805 1.08-1.9 1.08-3.082 0-1.183-.384-2.277-1.08-3.082z",
    fill: colorIcon
  }));
}

Infinity.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Infinity.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Infinity;