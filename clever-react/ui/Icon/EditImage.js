import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function EditImage(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M12.663 7.305a.666.666 0 0 0-.666.667v1.593l-1-.987c-.73-.7-1.883-.7-2.612 0l-.467.467-1.66-1.66a1.853 1.853 0 0 0-2.612 0l-.98.993V4.64c0-.368.299-.666.667-.666h3.999a.666.666 0 1 0 0-1.333h-4a2 2 0 0 0-1.999 2v7.997a2 2 0 0 0 2 2h7.997a2 2 0 0 0 2-2V7.972a.666.666 0 0 0-.667-.667zm-9.33 5.998a.666.666 0 0 1-.667-.666v-2.373l1.92-1.946a.52.52 0 0 1 .746 0l2.113 2.113 2.839 2.84-6.951.032zm8.664-.666a.666.666 0 0 1-.114.36L8.858 9.984l.466-.46a.52.52 0 0 1 .733 0l1.94 1.927v1.186zm2.48-10.164L12.862.86a.666.666 0 0 0-.94 0L8.872 3.913a.666.666 0 0 0-.207.473V6c0 .368.299.666.667.666h1.613c.175 0 .342-.07.466-.193l3.053-3.052a.666.666 0 0 0 .013-.947zm-3.813 2.86h-.667v-.667l2.4-2.393.666.667-2.4 2.392z",
    fill: colorIcon
  }));
}

EditImage.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
EditImage.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default EditImage;