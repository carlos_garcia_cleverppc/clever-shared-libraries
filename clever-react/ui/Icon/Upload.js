import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Upload(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M12.251 8.57a.603.603 0 0 0-.603.603v.23l-.892-.893a1.682 1.682 0 0 0-2.37 0l-.422.422-1.496-1.496a1.718 1.718 0 0 0-2.37 0l-.892.893V4.95c0-.333.27-.603.603-.603H8.03a.603.603 0 0 0 0-1.207H3.81c-1 0-1.809.81-1.809 1.81v7.24c0 1 .81 1.81 1.809 1.81h7.236c1 0 1.81-.81 1.81-1.81V9.173a.603.603 0 0 0-.604-.603zM3.81 12.793a.603.603 0 0 1-.603-.603v-2.154l1.749-1.75a.476.476 0 0 1 .657 0L7.524 10.2l2.593 2.594H3.809zm7.84-.603a.537.537 0 0 1-.11.32L8.82 9.777l.422-.423a.464.464 0 0 1 .663 0l1.743 1.756v1.08zm2.84-8.875-1.81-1.81a.604.604 0 0 0-.199-.126.603.603 0 0 0-.458 0 .604.604 0 0 0-.199.126l-1.809 1.81a.606.606 0 0 0 .856.857l.778-.784V6.76a.603.603 0 1 0 1.206 0V3.388l.778.784a.603.603 0 0 0 .856 0 .603.603 0 0 0 0-.857z",
    fill: colorIcon
  }));
}

Upload.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Upload.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Upload;