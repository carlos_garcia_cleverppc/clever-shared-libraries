import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Age(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M12.364 2.91h.727v.728a.727.727 0 0 0 1.454 0V2.91h.728a.727.727 0 0 0 0-1.455h-.728V.73a.727.727 0 0 0-1.454 0v.727h-.727a.727.727 0 1 0 0 1.455zm-8 1.455v7.272a.727.727 0 0 0 1.454 0V4.365a.727.727 0 0 0-1.454 0zM14.982 5.82a.727.727 0 0 0-.567.859 6.544 6.544 0 0 1-11.04 5.947A6.544 6.544 0 0 1 9.324 1.587a.742.742 0 1 0 .29-1.454A7.855 7.855 0 0 0 8 .002 8 8 0 1 0 8 16a8 8 0 0 0 7.84-9.599.727.727 0 0 0-.858-.582zm-7.71 0v.728c.004.538.206 1.056.568 1.454a2.181 2.181 0 0 0-.567 1.454v.727a2.181 2.181 0 0 0 2.182 2.182h.727a2.182 2.182 0 0 0 2.182-2.182v-.727a2.182 2.182 0 0 0-.568-1.454c.362-.398.564-.916.568-1.454v-.728a2.182 2.182 0 0 0-2.182-2.181h-.727a2.182 2.182 0 0 0-2.182 2.181zm3.637 4.363a.727.727 0 0 1-.727.728h-.727a.727.727 0 0 1-.728-.728v-.727c0-.401.326-.727.728-.727h.727c.401 0 .727.326.727.727v.727zm0-4.363v.728a.727.727 0 0 1-.727.727h-.727a.727.727 0 0 1-.728-.727v-.728c0-.401.326-.727.728-.727h.727c.401 0 .727.326.727.727z",
    fill: colorIcon
  }));
}

Age.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Age.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Age;