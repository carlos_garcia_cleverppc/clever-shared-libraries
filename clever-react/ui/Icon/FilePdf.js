import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function FilePdf(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 12 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M5.684 8.003c-.156-.5-.153-1.466-.062-1.466.262 0 .237 1.154.062 1.466zm-.053 1.475c-.24.631-.54 1.353-.887 1.96.572-.22 1.219-.538 1.965-.685a4.048 4.048 0 0 1-1.078-1.275zm-2.94 3.9c0 .025.412-.169 1.09-1.256-.21.197-.91.765-1.09 1.256zM7.75 5H12v10.25c0 .416-.334.75-.75.75H.75a.748.748 0 0 1-.75-.75V.75C0 .334.334 0 .75 0H7v4.25c0 .412.338.75.75.75zm-.25 5.369c-.625-.381-1.04-.906-1.334-1.681.14-.579.362-1.457.193-2.007-.146-.918-1.325-.828-1.493-.212-.157.572-.013 1.378.253 2.406-.363.863-.897 2.019-1.275 2.681-.003 0-.003.003-.006.003-.847.435-2.3 1.391-1.704 2.125.175.216.5.313.672.313.56 0 1.116-.563 1.91-1.931.806-.266 1.69-.597 2.468-.725.678.368 1.472.609 2 .609.913 0 .975-1 .616-1.356-.434-.425-1.697-.303-2.3-.225zm4.281-7.088L8.72.22A.75.75 0 0 0 8.188 0H8v4h4v-.19a.747.747 0 0 0-.219-.529zM9.466 11.26c.128-.084-.079-.372-1.338-.28 1.16.493 1.338.28 1.338.28z",
    fill: colorIcon
  }));
}

FilePdf.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
FilePdf.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default FilePdf;