import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Radio(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M4.591 4.644a.6.6 0 1 0-.849-.848 5.72 5.72 0 0 0 0 8.088.6.6 0 1 0 .85-.848 4.52 4.52 0 0 1 0-6.392zM11.83 3.796a.6.6 0 1 0-.848.848 4.52 4.52 0 0 1 0 6.392.6.6 0 1 0 .849.848 5.72 5.72 0 0 0 0-8.088z",
    fill: colorIcon
  }), /*#__PURE__*/React.createElement("path", {
    d: "M6.04 6.088a.6.6 0 1 0-.848-.849 3.672 3.672 0 0 0 0 5.196.6.6 0 1 0 .848-.849 2.472 2.472 0 0 1 0-3.498zM10.382 5.245a.6.6 0 0 0-.848.849 2.472 2.472 0 0 1 0 3.497.6.6 0 1 0 .848.85 3.672 3.672 0 0 0 0-5.196z",
    fill: colorIcon
  }), /*#__PURE__*/React.createElement("path", {
    d: "M7.787 6.216a1.624 1.624 0 1 0 0 3.248 1.624 1.624 0 0 0 0-3.248zM7.363 7.84a.424.424 0 1 1 .848 0 .424.424 0 0 1-.848 0z",
    fill: colorIcon,
    fillRule: "evenodd",
    clipRule: "evenodd"
  }));
}

Radio.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Radio.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Radio;