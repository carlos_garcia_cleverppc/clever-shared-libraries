import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Cpa(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M5.6 6a.8.8 0 0 0-.8.8v1.6a.8.8 0 1 0 1.6 0V6.8a.8.8 0 0 0-.8-.8zm9.6.8A.8.8 0 0 0 16 6V2.8a.8.8 0 0 0-.8-.8H.8a.8.8 0 0 0-.8.8V6a.8.8 0 0 0 .8.8.8.8 0 1 1 0 1.6.8.8 0 0 0-.8.8v3.2a.8.8 0 0 0 .8.8h14.4a.8.8 0 0 0 .8-.8V9.2a.8.8 0 0 0-.8-.8.8.8 0 0 1 0-1.6zm-.8-1.456a2.4 2.4 0 0 0 0 4.512V11.6h-8a.8.8 0 0 0-1.6 0H1.6V9.856a2.4 2.4 0 0 0 0-4.512V3.6h3.2a.8.8 0 1 0 1.6 0h8v1.744z",
    fill: colorIcon
  }));
}

Cpa.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Cpa.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Cpa;