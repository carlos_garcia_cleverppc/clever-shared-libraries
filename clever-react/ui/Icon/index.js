import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["icon"];

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import isString from "lodash-es/isString";
import { getColorsLabels, getShades } from "../styles/colors";
import Age from "./Age";
import Alarm from "./Alarm";
import Alert from "./Alert";
import AlignCenter from "./AlignCenter";
import AlignJustify from "./AlignJustify";
import AlignLeft from "./AlignLeft";
import AlignRight from "./AlignRight";
import Analytics from "./Analytics";
import ArrowRight from "./ArrowRight";
import At from "./At";
import Balance from "./Balance";
import Bell from "./Bell";
import BellOutlined from "./BellOutlined";
import Binoculars from "./Binoculars";
import Bold from "./Bold";
import BookOpen from "./BookOpen";
import Budget from "./Budget";
import Calendar from "./Calendar";
import CalendarAlt from "./CalendarAlt";
import Channel from "./Channel";
import Chat from "./Chat";
import Check from "./Check";
import CheckAlt from "./CheckAlt";
import CheckImage from "./CheckImage";
import CheckRound from "./CheckRound";
import ChevronDoubleLeft from "./ChevronDoubleLeft";
import ChevronDoubleRight from "./ChevronDoubleRight";
import ChevronDown from "./ChevronDown";
import ChevronLeft from "./ChevronLeft";
import ChevronRight from "./ChevronRight";
import ChevronUp from "./ChevronUp";
import Circle from "./Circle";
import CircleArrowDown from "./CircleArrowDown";
import CircleArrowUp from "./CircleArrowUp";
import CirclePlus from "./CirclePlus";
import Clipboard from "./Clipboard";
import Clock from "./Clock";
import Codebar from "./Codebar";
import Cog from "./Cog";
import Comment from "./Comment";
import Condition from "./Condition";
import Copy from "./Copy";
import Cpa from "./Cpa";
import Create from "./Create";
import CreditCard from "./CreditCard";
import Cta from "./Cta";
import CtaAlt from "./CtaAlt";
import Desktop from "./Desktop";
import Discount from "./Discount";
import DownArrow from "./DownArrow";
import Download from "./Download";
import Duplicate from "./Duplicate";
import Edit from "./Edit";
import EditImage from "./EditImage";
import Enter from "./Enter";
import Envelope from "./Envelope";
import Exclamation from "./Exclamation";
import Exit from "./Exit";
import ExternalLink from "./ExternalLink";
import Eye from "./Eye";
import Female from "./Female";
import File from "./File";
import FilePdf from "./FilePdf";
import FileSvg from "./FileSvg";
import FilterFunnel from "./FilterFunnel";
import Gender from "./Gender";
import GoogleAds from "./GoogleAds";
import Graph from "./Graph";
import Headline from "./Headline";
import Headlines from "./Headlines";
import Hide from "./Hide";
import Home from "./Home";
import Image from "./Image";
import ImageResize from "./ImageResize";
import Images from "./Images";
import Income from "./Income";
import Infinity from "./Infinity";
import InfoCircle from "./InfoCircle";
import Iphone from "./Iphone";
import Italic from "./Italic";
import Key from "./Key";
import Languages from "./Languages";
import Light from "./Light";
import Link from "./Link";
import Location from "./Location";
import LocationAlt from "./LocationAlt";
import Male from "./Male";
import Medal from "./Medal";
import Megaphone from "./Megaphone";
import Menu from "./Menu";
import Merge from "./Merge";
import MergePlus from "./MergePlus";
import MoneyBill from "./MoneyBill";
import Moon from "./Moon";
import MousePointer from "./MousePointer";
import OpenBlock from "./OpenBlock";
import OpenLock from "./OpenLock";
import PauseCircle from "./PauseCircle";
import Pen from "./Pen";
import Percent from "./Percent";
import PlayCircle from "./PlayCircle";
import Plug from "./Plug";
import Plus from "./Plus";
import PlusRound from "./PlusRound";
import Puzzle from "./Puzzle";
import Radio from "./Radio";
import RadioButtonChecked from "./RadioButtonChecked";
import RadioButtonUnchecked from "./RadioButtonUnchecked";
import Redo from "./Redo";
import Refresh from "./Refresh";
import Rocket from "./Rocket";
import Ruler from "./Ruler";
import Score from "./Score";
import Search from "./Search";
import Select from "./Select";
import Shape from "./Shape";
import ShoppingBag from "./ShoppingBag";
import ShoppingCart from "./ShoppingCart";
import ShowMore from "./ShowMore";
import Smart from "./Smart";
import Spent from "./Spent";
import Star from "./Star";
import StarEmpty from "./StarEmpty";
import StepBackward from "./StepBackward";
import StepForward from "./StepForward";
import Sun from "./Sun";
import Support from "./Support";
import Tablet from "./Tablet";
import Tag from "./Tag";
import Target from "./Target";
import Telephone from "./Telephone";
import Template from "./Template";
import Text from "./Text";
import ThumbDown from "./ThumbDown";
import ThumbUp from "./ThumbUp";
import Times from "./Times";
import TimesRound from "./TimesRound";
import Trash from "./Trash";
import Trophy from "./Trophy";
import Uncheck from "./Uncheck";
import Underline from "./Underline";
import UpArrow from "./UpArrow";
import Upload from "./Upload";
import User from "./User";
import UserCheck from "./UserCheck";
import Warning from "./Warning";
var ICONS_MAP = {
  age: Age,
  alarm: Alarm,
  alert: Alert,
  aligncenter: AlignCenter,
  alignjustify: AlignJustify,
  alignleft: AlignLeft,
  alignright: AlignRight,
  analytics: Analytics,
  arrowright: ArrowRight,
  at: At,
  balance: Balance,
  bell: Bell,
  belloutlined: BellOutlined,
  binoculars: Binoculars,
  bold: Bold,
  bookopen: BookOpen,
  budget: Budget,
  calendar: Calendar,
  calendaralt: CalendarAlt,
  channel: Channel,
  chat: Chat,
  check: Check,
  checkalt: CheckAlt,
  checkimage: CheckImage,
  checkround: CheckRound,
  chevrondoubleleft: ChevronDoubleLeft,
  chevrondoubleright: ChevronDoubleRight,
  chevrondown: ChevronDown,
  chevronleft: ChevronLeft,
  chevronright: ChevronRight,
  chevronup: ChevronUp,
  circle: Circle,
  circlearrowdown: CircleArrowDown,
  circlearrowup: CircleArrowUp,
  circleplus: CirclePlus,
  clipboard: Clipboard,
  clock: Clock,
  codebar: Codebar,
  cog: Cog,
  comment: Comment,
  condition: Condition,
  copy: Copy,
  cpa: Cpa,
  create: Create,
  creditcard: CreditCard,
  cta: Cta,
  ctaalt: CtaAlt,
  desktop: Desktop,
  discount: Discount,
  downarrow: DownArrow,
  download: Download,
  duplicate: Duplicate,
  edit: Edit,
  editimage: EditImage,
  enter: Enter,
  envelope: Envelope,
  exclamation: Exclamation,
  exit: Exit,
  externallink: ExternalLink,
  eye: Eye,
  female: Female,
  file: File,
  filepdf: FilePdf,
  filesvg: FileSvg,
  filterfunnel: FilterFunnel,
  gender: Gender,
  googleads: GoogleAds,
  graph: Graph,
  headline: Headline,
  headlines: Headlines,
  hide: Hide,
  home: Home,
  image: Image,
  imageresize: ImageResize,
  images: Images,
  income: Income,
  infinity: Infinity,
  infocircle: InfoCircle,
  iphone: Iphone,
  italic: Italic,
  key: Key,
  languages: Languages,
  light: Light,
  link: Link,
  location: Location,
  locationalt: LocationAlt,
  male: Male,
  medal: Medal,
  megaphone: Megaphone,
  menu: Menu,
  merge: Merge,
  mergeplus: MergePlus,
  moneybill: MoneyBill,
  moon: Moon,
  mousepointer: MousePointer,
  openblock: OpenBlock,
  openlock: OpenLock,
  pausecircle: PauseCircle,
  pen: Pen,
  percent: Percent,
  playcircle: PlayCircle,
  plug: Plug,
  plus: Plus,
  plusround: PlusRound,
  puzzle: Puzzle,
  radio: Radio,
  radiobuttonchecked: RadioButtonChecked,
  radiobuttonunchecked: RadioButtonUnchecked,
  redo: Redo,
  refresh: Refresh,
  rocket: Rocket,
  ruler: Ruler,
  score: Score,
  search: Search,
  select: Select,
  shape: Shape,
  shoppingbag: ShoppingBag,
  shoppingcart: ShoppingCart,
  showmore: ShowMore,
  smart: Smart,
  spent: Spent,
  star: Star,
  starempty: StarEmpty,
  stepbackward: StepBackward,
  stepforward: StepForward,
  sun: Sun,
  support: Support,
  tablet: Tablet,
  tag: Tag,
  target: Target,
  telephone: Telephone,
  template: Template,
  text: Text,
  thumbdown: ThumbDown,
  thumbup: ThumbUp,
  times: Times,
  timesround: TimesRound,
  trash: Trash,
  trophy: Trophy,
  uncheck: Uncheck,
  underline: Underline,
  uparrow: UpArrow,
  upload: Upload,
  user: User,
  usercheck: UserCheck,
  warning: Warning
};
var colorsList = getColorsLabels();
var shadesList = getShades();

var Icon = function Icon(_ref) {
  var icon = _ref.icon,
      props = _objectWithoutProperties(_ref, _excluded);

  var name = icon.trim().toLowerCase();
  var IconSelected = ICONS_MAP[name];

  if (!IconSelected) {
    return null;
  }

  return /*#__PURE__*/React.createElement(IconSelected, props);
};

Icon.propTypes = {
  icon: function icon(_ref2) {
    var _icon = _ref2.icon;

    if (!isString(_icon)) {
      return new Error("Invalid prop icon supplied to Icon Component. Validation failed.");
    }

    if (!ICONS_MAP[_icon.trim().toLowerCase()]) {
      return new Error("Invalid prop icon supplied to Icon Component. Validation failed. INVALID ICON");
    }

    return null;
  },
  color: PropTypes.oneOf(colorsList),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Icon.defaultProps = {
  icon: "",
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Icon;