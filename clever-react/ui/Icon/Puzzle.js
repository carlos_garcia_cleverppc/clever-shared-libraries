import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Puzzle(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M11.333 14.667A1.333 1.333 0 0 1 10 13.333v-.666a.667.667 0 1 0-1.333 0v.666c0 .737-.597 1.334-1.334 1.334h-2A1.333 1.333 0 0 1 4 13.333v-2h-.667a2 2 0 0 1 0-4H4v-2C4 4.593 4.6 4 5.333 4h2v-.667a2 2 0 1 1 4 0V4h2c.737 0 1.334.597 1.334 1.333v2c0 .737-.597 1.334-1.334 1.334h-.666a.667.667 0 0 0 0 1.333h.666c.737 0 1.334.597 1.334 1.333v2c0 .737-.597 1.334-1.334 1.334h-2zm2-1.334v-2h-.666a2 2 0 0 1 0-4h.666v-2h-2A1.333 1.333 0 0 1 10 4v-.667a.667.667 0 0 0-1.333 0V4c0 .736-.597 1.333-1.334 1.333h-2v2c0 .737-.597 1.334-1.333 1.334h-.667a.667.667 0 0 0 0 1.333H4c.736 0 1.333.597 1.333 1.333v2h2v-.666a2 2 0 1 1 4 0v.666h2z",
    fill: colorIcon
  }));
}

Puzzle.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Puzzle.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Puzzle;