import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function CheckImage(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M15.768.233a.8.8 0 0 0-1.128 0l-2.704 2.641-.976-.96a.8.8 0 1 0-1.12 1.144l1.536 1.505a.8.8 0 0 0 1.12 0L15.76 1.36a.8.8 0 0 0 .008-1.128zM13.6 5.595a.8.8 0 0 0-.8.8V9.91l-1.184-1.184a2.216 2.216 0 0 0-3.144 0l-.56.56-1.984-1.993a2.231 2.231 0 0 0-3.144 0L1.6 8.477V3.994a.8.8 0 0 1 .8-.8h4.8a.8.8 0 0 0 0-1.6H2.4a2.4 2.4 0 0 0-2.4 2.4V13.6A2.4 2.4 0 0 0 2.4 16H12a2.4 2.4 0 0 0 2.4-2.401V6.396a.8.8 0 0 0-.8-.8zM2.4 14.4a.8.8 0 0 1-.8-.8v-2.857L3.92 8.42a.632.632 0 0 1 .872 0l2.536 2.537 3.432 3.441H2.4zm10.4-.8a.713.713 0 0 1-.144.424l-3.608-3.626.56-.56a.624.624 0 0 1 .88 0l2.312 2.337V13.6z",
    fill: colorIcon
  }));
}

CheckImage.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
CheckImage.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default CheckImage;