import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Link(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "m6.346 12.738-1.427 1.298c-.78.73-2.043.73-2.822 0a1.786 1.786 0 0 1 0-2.643l3.66-3.435c.746-.701 1.948-.731 2.733-.068l.097.076a.846.846 0 0 0 1.14-.012.722.722 0 0 0-.012-1.068 2.12 2.12 0 0 0-.145-.159c-1.428-1.163-3.575-1.09-4.91.166L.953 10.33c-1.31 1.335-1.263 3.399.106 4.68 1.368 1.282 3.572 1.326 4.997.1l1.395-1.277a.72.72 0 0 0-.008-1.029.846.846 0 0 0-1.096-.066zm8.618-10.752C13.552.67 11.272.67 9.86 1.986L8.466 3.261a.72.72 0 0 0 .008 1.028.846.846 0 0 0 1.096.066l1.395-1.298c.78-.73 2.043-.73 2.822 0 .779.73.779 1.913 0 2.642l-3.66 3.436c-.747.7-1.949.73-2.733.068l-.097-.076a.846.846 0 0 0-1.14.012.722.722 0 0 0 .011 1.068c.059.056.12.11.186.159 1.43 1.16 3.573 1.087 4.91-.167l3.668-3.435c1.411-1.314 1.426-3.449.032-4.78z",
    fill: colorIcon
  }));
}

Link.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Link.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Link;