import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Target(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M8 5.923a2.077 2.077 0 1 0 0 4.154 2.077 2.077 0 0 0 0-4.154zM7.123 8a.877.877 0 1 1 1.754 0 .877.877 0 0 1-1.754 0z",
    fill: colorIcon,
    fillRule: "evenodd",
    clipRule: "evenodd"
  }), /*#__PURE__*/React.createElement("path", {
    d: "M2.97 8a5.03 5.03 0 1 1 10.06 0A5.03 5.03 0 0 1 2.97 8zM8 4.17a3.83 3.83 0 1 0 0 7.66 3.83 3.83 0 0 0 0-7.66z",
    fill: colorIcon,
    fillRule: "evenodd",
    clipRule: "evenodd"
  }), /*#__PURE__*/React.createElement("path", {
    d: "M.015 8a7.985 7.985 0 1 1 15.97 0A7.985 7.985 0 0 1 .015 8zM8 1.215a6.785 6.785 0 1 0 0 13.57 6.785 6.785 0 0 0 0-13.57z",
    fill: colorIcon,
    fillRule: "evenodd",
    clipRule: "evenodd"
  }));
}

Target.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Target.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Target;