import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Alarm(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M15 5.37a1.361 1.361 0 0 0-1.314-1.342 1.363 1.363 0 0 0-1.415 1.237 1.358 1.358 0 0 0 1.177 1.461 3.02 3.02 0 0 1-.154 2.426 3.063 3.063 0 0 1-1.896 1.54 2.04 2.04 0 0 0-.889-.97 4.716 4.716 0 0 0-.464-6.45 4.814 4.814 0 0 0-6.52 0 4.716 4.716 0 0 0-.466 6.45 2.03 2.03 0 0 0-1.045 1.749v.678c0 1.123.918 2.033 2.05 2.033h5.468c1.133 0 2.05-.91 2.05-2.033v-.13a4.417 4.417 0 0 0 2.897-2.206c.6-1.116.685-2.433.234-3.615.185-.237.286-.528.287-.827zm-4.784 6.779a.68.68 0 0 1-.684.677H4.064a.68.68 0 0 1-.683-.677v-.678a.68.68 0 0 1 .683-.678h.28a4.777 4.777 0 0 0 4.908 0h.28a.68.68 0 0 1 .684.678v.678zm-3.418-2.034c-1.887 0-3.417-1.517-3.417-3.389 0-1.871 1.53-3.389 3.417-3.389 1.888 0 3.418 1.518 3.418 3.39 0 .898-.36 1.76-1.001 2.396-.641.635-1.51.992-2.417.992zm0-4.067a.68.68 0 0 0-.683.678.68.68 0 0 0 .683.678.68.68 0 0 0 .684-.678.68.68 0 0 0-.684-.678z",
    fill: colorIcon
  }));
}

Alarm.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Alarm.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Alarm;