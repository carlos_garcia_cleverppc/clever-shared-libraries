import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Rocket(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M10.99 10.825a3.175 3.175 0 0 1-2.552 3.112l-2.444.488a.635.635 0 0 1-.572-.171l-.45-.444a1.27 1.27 0 0 1 0-1.797l.914-.92a8.365 8.365 0 0 1-.902-.896l-.914.914a1.27 1.27 0 0 1-1.797 0l-.444-.444a.635.635 0 0 1-.178-.572l.489-2.45a3.175 3.175 0 0 1 3.11-2.553h.547a8.298 8.298 0 0 1 8.14-3.473.635.635 0 0 1 .527.527 8.241 8.241 0 0 1-3.48 8.14v.54h.007zm-2.8 1.867c.89-.178 1.53-.96 1.53-1.867v-.888a.635.635 0 0 1 .299-.534 7.01 7.01 0 0 0 3.25-6.59 6.971 6.971 0 0 0-6.59 3.25.635.635 0 0 1-.54.299h-.888c-.905 0-1.686.637-1.867 1.524l-.419 2.12.21.21 1.422-1.422a.635.635 0 0 1 .965.076 7.033 7.033 0 0 0 1.65 1.65.635.635 0 0 1 .077.966l-1.422 1.422.21.21 2.113-.426zm2.032-6.28a.635.635 0 1 1 0-1.27.635.635 0 0 1 0 1.27z",
    fill: colorIcon
  }));
}

Rocket.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Rocket.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Rocket;