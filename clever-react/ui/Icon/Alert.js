import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Alert(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 24 24",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M11.148 16.452A1.2 1.2 0 0 0 13.2 15.6a1.26 1.26 0 0 0-.348-.852 1.2 1.2 0 0 0-1.308-.252 1.2 1.2 0 0 0-.396.252 1.26 1.26 0 0 0-.348.852 1.2 1.2 0 0 0 .348.852zm10.344-.24h-5.436a1.2 1.2 0 0 0 0 2.4h2.88A9.6 9.6 0 0 1 2.4 12 1.2 1.2 0 0 0 0 12a12 12 0 0 0 20.256 8.676V22.8a1.2 1.2 0 0 0 2.4 0v-5.4a1.2 1.2 0 0 0-1.164-1.188zM12 0a12 12 0 0 0-8.256 3.324V1.2a1.2 1.2 0 0 0-2.4 0v5.4a1.2 1.2 0 0 0 1.2 1.2h5.4a1.2 1.2 0 0 0 0-2.4h-2.88A9.6 9.6 0 0 1 21.6 12a1.2 1.2 0 0 0 2.4 0A12 12 0 0 0 12 0zm0 13.2a1.2 1.2 0 0 0 1.2-1.2V8.4a1.2 1.2 0 0 0-2.4 0V12a1.2 1.2 0 0 0 1.2 1.2z",
    fill: colorIcon
  }));
}

Alert.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Alert.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Alert;