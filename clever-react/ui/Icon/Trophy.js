import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Trophy(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M7.333 13.333v-1.386a4 4 0 0 1-2.826-2 2.68 2.68 0 0 1-3.174-2.614V4c0-.733.6-1.333 1.334-1.333H4c0-.734.6-1.334 1.333-1.334h5.334c.736 0 1.333.597 1.333 1.334h1.333c.737 0 1.334.597 1.334 1.333v3.333a2.666 2.666 0 0 1-3.174 2.62 4 4 0 0 1-2.826 1.994v1.386h2.666a.667.667 0 0 1 0 1.334H4.667a.667.667 0 0 1 0-1.334h2.666zm4.614-4.666H12c.736 0 1.333-.597 1.333-1.334V4H12v4c0 .227-.02.447-.053.667zm-7.894 0A4.027 4.027 0 0 1 4 8V4H2.667v3.333a1.333 1.333 0 0 0 1.386 1.334zm1.28-6V8a2.667 2.667 0 1 0 5.334 0V2.667H5.333z",
    fill: colorIcon
  }));
}

Trophy.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Trophy.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Trophy;