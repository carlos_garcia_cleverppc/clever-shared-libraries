import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Condition(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M11.333 4.667a5.16 5.16 0 1 0-6.666 6.666 5.16 5.16 0 1 0 6.666-6.666zm-1.066 1.066c.048.25.072.505.073.76-.001.301-.037.6-.107.894L8.607 5.773c.292-.07.592-.105.893-.106.255 0 .51.025.76.073l.007-.007zM9.64 8.667a4 4 0 0 1-.973.973L6.36 7.333a4 4 0 0 1 .973-.973L9.64 8.667zm-5.333.973A3.833 3.833 0 1 1 9.64 4.307h-.167A5.173 5.173 0 0 0 4.333 9.5c0 .053.007.113.007.167l-.033-.027zm1.4.593a4.079 4.079 0 0 1-.074-.76c.002-.3.038-.6.107-.893l1.62 1.62c-.293.07-.592.105-.893.107a4.082 4.082 0 0 1-.727-.047l-.033-.027zm3.793 3.1a3.847 3.847 0 0 1-3.167-1.673H6.5a5.173 5.173 0 0 0 5.167-5.16v-.167a3.833 3.833 0 0 1-2.167 7z",
    fill: colorIcon
  }));
}

Condition.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Condition.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Condition;