import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Copy(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M15.4 5.552a1.044 1.044 0 0 0-.048-.216v-.072a.857.857 0 0 0-.152-.224L10.4.24a.855.855 0 0 0-.224-.152.256.256 0 0 0-.072 0A.704.704 0 0 0 9.84 0H6.6a2.4 2.4 0 0 0-2.4 2.4v.8h-.8A2.4 2.4 0 0 0 1 5.6v8A2.4 2.4 0 0 0 3.4 16h6.4a2.4 2.4 0 0 0 2.4-2.4v-.8h.8a2.4 2.4 0 0 0 2.4-2.4V5.552zm-4.8-2.824L12.672 4.8H11.4a.8.8 0 0 1-.8-.8V2.728zm0 10.872a.8.8 0 0 1-.8.8H3.4a.8.8 0 0 1-.8-.8v-8a.8.8 0 0 1 .8-.8h.8v5.6a2.4 2.4 0 0 0 2.4 2.4h4v.8zm3.2-3.2a.8.8 0 0 1-.8.8H6.6a.8.8 0 0 1-.8-.8v-8a.8.8 0 0 1 .8-.8H9V4a2.4 2.4 0 0 0 2.4 2.4h2.4v4z",
    fill: colorIcon
  }));
}

Copy.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Copy.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Copy;