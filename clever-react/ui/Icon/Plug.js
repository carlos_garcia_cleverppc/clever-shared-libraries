import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Plug(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 13 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M11.74 3.2H9.39V.8c0-.442-.35-.8-.782-.8a.791.791 0 0 0-.783.8v2.4h-3.13V.8c0-.442-.35-.8-.783-.8a.791.791 0 0 0-.783.8v2.4H.783A.791.791 0 0 0 0 4c0 .442.35.8.783.8h.782v4a.81.81 0 0 0 .227.568l2.121 2.16V15.2c0 .442.35.8.783.8a.791.791 0 0 0 .782-.8V12h1.565v3.2c0 .442.35.8.783.8a.791.791 0 0 0 .783-.8v-3.672l2.12-2.16a.81.81 0 0 0 .228-.568v-4h.782a.791.791 0 0 0 .783-.8c0-.442-.35-.8-.783-.8zM9.39 8.472 7.505 10.4H5.017L3.13 8.472V4.8h6.261v3.672zM5.478 8.8h1.565a.791.791 0 0 0 .783-.8c0-.442-.35-.8-.783-.8H5.478a.791.791 0 0 0-.782.8c0 .442.35.8.782.8z",
    fill: colorIcon
  }));
}

Plug.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Plug.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Plug;