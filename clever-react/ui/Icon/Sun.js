import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */

/* eslint-disable comma-dangle */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getCurrentConfig } from "../Provider/themeConfig";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels, getShades } from "../styles/colors";
var colors = getCurrentConfig().cleverUI.colors;
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});
var colorsList = getColorsLabels();
var shadesList = getShades();

function Sun(props) {
  var classes = getClasses(props);
  var size = props.size,
      color = props.color,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var padding = props.padding,
      shade = props.shade;
  var sizeIcon = size;
  var colorIcon = colors[color] ? colors[color][shade] : color;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return /*#__PURE__*/React.createElement("svg", _extends({
    viewBox: "0 0 16 16",
    fill: "none",
    height: sizeIcon,
    width: sizeIcon
  }, otherProps), /*#__PURE__*/React.createElement("path", {
    d: "M8 .35a.65.65 0 0 1 .65.65v1.273a.65.65 0 0 1-1.3 0V1A.65.65 0 0 1 8 .35z",
    fill: colorIcon
  }), /*#__PURE__*/React.createElement("path", {
    d: "M4.168 8a3.832 3.832 0 1 1 7.664 0 3.832 3.832 0 0 1-7.664 0zM8 5.468a2.532 2.532 0 1 0 0 5.064 2.532 2.532 0 0 0 0-5.064z",
    fill: colorIcon,
    fillRule: "evenodd",
    clipRule: "evenodd"
  }), /*#__PURE__*/React.createElement("path", {
    d: "M8.65 13.727a.65.65 0 1 0-1.3 0V15a.65.65 0 1 0 1.3 0v-1.273zM2.59 2.59a.65.65 0 0 1 .919 0l.903.903a.65.65 0 1 1-.919.92l-.904-.904a.65.65 0 0 1 0-.92zM12.507 11.588a.65.65 0 0 0-.92.919l.904.904a.65.65 0 1 0 .92-.92l-.904-.903zM.35 8A.65.65 0 0 1 1 7.35h1.273a.65.65 0 0 1 0 1.3H1A.65.65 0 0 1 .35 8zM13.727 7.35a.65.65 0 1 0 0 1.3H15a.65.65 0 1 0 0-1.3h-1.273zM4.412 11.588a.65.65 0 0 1 0 .919l-.903.904a.65.65 0 1 1-.92-.92l.904-.903a.65.65 0 0 1 .92 0zM13.41 3.509a.65.65 0 1 0-.919-.92l-.903.904a.65.65 0 1 0 .919.92l.904-.904z",
    fill: colorIcon
  }));
}

Sun.propTypes = {
  color: PropTypes.oneOfType([PropTypes.oneOf(colorsList), PropTypes.string]),
  className: PropTypes.string,
  size: PropTypes.oneOf([6, 8, 10, 12, 14, 15, 16, 18, 20, 22, 24, 26, 30, 32, 42, 64, 96]),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  shade: PropTypes.oneOf(shadesList),
  padding: PropTypes.number
};
Sun.defaultProps = {
  color: "neutral",
  size: 24,
  className: "",
  rotation: 0,
  shade: 500,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default Sun;