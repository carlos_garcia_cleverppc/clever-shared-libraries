import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      spacing = _ref$cleverUI.spacing,
      colors = _ref$cleverUI.colors,
      typography = _ref$cleverUI.typography,
      shadows = _ref$cleverUI.shadows;
  var sizing = typography.sizing;
  return {
    rounded: {
      borderRadius: "16px",
      backgroundColor: "white",
      borderWidth: "1px",
      borderStyle: "solid",
      borderColor: colors.neutral[100],
      height: "32px",
      "& > div:first-child": _objectSpread({}, spacing["pl-4"])
    },
    root: {
      height: "inherit",
      padding: "6px 0 7px",
      minWidth: "150px",
      fontFamily: typography.font2,
      fontSize: sizing.body0,
      color: colors.neutral[500],
      "& p": {
        fontWeight: "bold"
      },
      "&:focus": {
        backgroundColor: "transparent"
      }
    },
    scroll: {
      "&::-webkit-scrollbar": {
        width: "12px",
        backgroundColor: "#fff"
      },
      "&::-webkit-scrollbar-track": {
        backgroundColor: "#fff"
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#babac0",
        borderRadius: "16px",
        border: "4px solid #fff"
      }
    },
    menu: _objectSpread(_objectSpread(_objectSpread({}, spacing["py-3"]), spacing["px-4"]), {}, {
      backgroundColor: "white",
      borderRadius: "8px",
      minWidth: "130px !important",
      marginTop: "50px",
      boxShadow: shadows.normal,
      overflow: "unset",
      "&:after": {
        content: "''",
        position: "absolute",
        top: "-28px",
        right: "8px",
        border: "solid 14px transparent",
        borderTopColor: "white",
        transform: "rotate(180deg)"
      }
    }),
    selectIcon: _objectSpread(_objectSpread({}, spacing["mr-2"]), {}, {
      height: "24px",
      "& path": {
        fill: "".concat(colors.neutral[900], " !important")
      }
    }),
    iconOpen: {
      transform: "rotate(90deg)"
    },
    menuItem: _objectSpread(_objectSpread({}, spacing["py-1"]), {}, {
      fontFamily: typography.font2,
      fontSize: sizing.body0,
      color: colors.neutral[600],
      backgroundColor: "transparent"
    }),
    menuItemRoot: function menuItemRoot(_ref2) {
      var textAlignRight = _ref2.textAlignRight;
      return {
        justifyContent: textAlignRight ? "flex-end" : "flex-start",
        "&:hover": {
          backgroundColor: "transparent"
        }
      };
    },
    menuItemSelected: {
      fontWeight: "bold",
      backgroundColor: colors.neutral[200],
      borderRadius: "9px"
    },
    menuItemRootPlaceHolder: {
      display: "none"
    },
    tooltip: {
      display: "flex",
      marginLeft: "auto"
    },
    tooltipContainer: {
      maxWidth: "200px",
      margin: "auto"
    },
    icon: _objectSpread({}, spacing["mr-2"])
  };
};

export default makeStyles(styles);