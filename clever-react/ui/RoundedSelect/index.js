import React from "react";
import PropTypes from "prop-types";
import map from "lodash-es/map";
import isArray from "lodash-es/isArray";
import classnames from "classnames";
import MenuItem from "@material-ui/core/MenuItem";
import Icon from "../Icon";
import ChevronRight from "../Icon/ChevronRight";
import TextField from "../TextField";
import Tooltip from "../Tooltip";
import getClasses from "./styles";
import Typography from "../Typography"; // eslint-disable-next-line no-unused-vars

function printMenuItemDefault(label, value, disabled, index) {
  return label;
}

var RoundedSelect = function RoundedSelect(props) {
  var className = props.className,
      value = props.value,
      onChange = props.onChange,
      id = props.id,
      label = props.label,
      textAlignRight = props.textAlignRight;
  var name = props.name,
      options = props.options,
      printMenuItem = props.printMenuItem,
      disabled = props.disabled,
      placeholder = props.placeholder;
  var classes = getClasses({
    textAlignRight: textAlignRight
  });

  var getItem = function getItem(option, index) {
    return /*#__PURE__*/React.createElement(MenuItem, {
      classes: {
        root: classes.menuItemRoot,
        selected: classes.menuItemSelected
      },
      className: classes.menuItem,
      key: "".concat(name, "-").concat(option.label),
      value: option.value,
      disabled: option.disabled || false
    }, option.icon && /*#__PURE__*/React.createElement(Icon, {
      icon: option.icon,
      className: classes.icon,
      color: option.iconColor ? option.iconColor : "neutral"
    }), printMenuItem(option.label, option.value, option.disabled || false, index));
  };

  var getTooltipItem = function getTooltipItem(option, index) {
    return /*#__PURE__*/React.createElement(Tooltip, {
      title: /*#__PURE__*/React.createElement(Typography, {
        variant: "body0",
        color: "white",
        className: classes.tooltipContainer
      }, option.disabledMessage),
      key: "rounded-select-item-".concat(index),
      type: "simple",
      className: classes.tooltip
    }, getItem(option, index));
  };

  return /*#__PURE__*/React.createElement(TextField, {
    name: name,
    value: !value ? placeholder : value,
    onChange: onChange,
    SelectProps: {
      classes: {
        icon: classes.selectIcon,
        root: classes.root,
        iconOpen: classes.iconOpen
      },
      IconComponent: ChevronRight,
      MenuProps: {
        classes: {
          paper: classnames(classes.scroll, classes.menu)
        },
        getContentAnchorEl: null,
        anchorOrigin: {
          vertical: "top",
          horizontal: "right"
        },
        transformOrigin: {
          vertical: "top",
          horizontal: "right"
        }
      }
    },
    className: classnames(classes.rounded, className),
    disabled: disabled,
    variant: "standard",
    select: true,
    id: id,
    label: label
  }, !!placeholder && !value && /*#__PURE__*/React.createElement(MenuItem, {
    classes: {
      root: classes.menuItemRootPlaceHolder
    },
    value: placeholder
  }, printMenuItem(placeholder)), isArray(options) && map(options, function (option, index) {
    if (option.disabled && !!option.disabledMessage) return getTooltipItem(option, index);
    return getItem(option, index);
  }));
};

RoundedSelect.propTypes = {
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
  options: PropTypes.instanceOf(Array).isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  printMenuItem: PropTypes.func,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  label: PropTypes.string,
  textAlignRight: PropTypes.bool
};
RoundedSelect.defaultProps = {
  className: "",
  printMenuItem: printMenuItemDefault,
  disabled: false,
  id: "",
  label: "",
  textAlignRight: true
};
export default RoundedSelect;