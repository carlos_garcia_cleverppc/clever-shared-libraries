import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

function getNormalBgColor(colors, color, variant) {
  if (variant === "metric") return "white";
  var shade = color === "neutral" ? 300 : 100;
  return colors[color][shade];
}

function getTextColor(colors, color) {
  var shade = color === "neutral" ? 900 : 500;
  return colors[color][shade];
}

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing,
      shadows = _ref$cleverUI.shadows,
      typography = _ref$cleverUI.typography;
  var sizing = typography.sizing;
  return {
    block: function block(_ref2) {
      var width = _ref2.width,
          height = _ref2.height;
      return _objectSpread(_objectSpread({}, spacing["p-4"]), {}, {
        borderRadius: "8px",
        width: "".concat(160 * width + (width - 1) * 24, "px"),
        height: "".concat(160 * height + (height - 1) * 24, "px"),
        boxSizing: "border-box",
        display: "flex",
        flexDirection: "column",
        "&:focus": {
          outline: "none"
        },
        "& a": {
          fontSize: sizing.body0
        }
      });
    },
    hoverable: {
      cursor: "pointer",
      "&:hover": {
        boxShadow: shadows.hover,
        transition: "all .3s ease-in-out"
      },
      "&:focus": {
        boxShadow: shadows.hover,
        transition: "all .3s ease-in-out"
      }
    },
    normal: function normal(_ref3) {
      var color = _ref3.color,
          variant = _ref3.variant;
      return {
        backgroundColor: getNormalBgColor(colors, color, variant),
        "& h6": {
          color: getTextColor(colors, color)
        },
        "& p": {
          color: getTextColor(colors, color)
        },
        "& a": {
          color: getTextColor(colors, color)
        }
      };
    },
    selected: function selected(_ref4) {
      var color = _ref4.color;
      return {
        border: "3px solid ".concat(getTextColor(colors, color)),
        "& h6": {
          color: getTextColor(colors, color)
        },
        "& p": {
          color: getTextColor(colors, color)
        },
        "& a": {
          color: getTextColor(colors, color)
        }
      };
    },
    active: function active(_ref5) {
      var color = _ref5.color;
      return {
        backgroundColor: color === "yellow" || color === "neutral" ? colors[color][900] : colors[color][500],
        "& h6": {
          color: colors[color][100]
        },
        "& p": {
          color: colors[color][100]
        },
        "& a": {
          color: colors[color][100]
        }
      };
    },
    drawerRoot: {
      maxWidth: "400px"
    },
    drawerHeader: function drawerHeader(_ref6) {
      var color = _ref6.color;
      return _objectSpread(_objectSpread({}, spacing["p-5"]), {}, {
        minWidth: 250,
        backgroundColor: colors[color][700],
        display: "flex",
        flexDirection: "column",
        "& h6": {
          color: "white !important"
        },
        "& p": {
          color: "white !important"
        }
      });
    },
    drawerContent: _objectSpread({}, spacing["p-5"]),
    iconButton: {
      padding: 0
    },
    image: function image(_ref7) {
      var imageWidth = _ref7.imageWidth;
      return _objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread({}, spacing["mx-auto"]), spacing["w-".concat(imageWidth)]), spacing["h-".concat(imageWidth)]), spacing["mt-".concat(imageWidth === "25" ? "8" : "3")]), {}, {
        maxHeight: "147px"
      });
    },
    smallImage: _objectSpread(_objectSpread(_objectSpread({}, spacing["mx-auto"]), spacing["mt-3"]), {}, {
      maxHeight: "115px",
      maxWidth: "110px"
    }),
    icon: function icon(_ref8) {
      var color = _ref8.color;
      return {
        width: "32px !important",
        height: "32px !important",
        "& path": {
          fill: colors[color][900]
        }
      };
    },
    iconActive: function iconActive(_ref9) {
      var color = _ref9.color;
      return {
        "& path": {
          fill: colors[color][100]
        }
      };
    },
    closeIconContainer: {
      height: 0,
      textAlign: "right"
    },
    marginNegative: _objectSpread({}, spacing["mnt-3"]),
    marginAuto: _objectSpread({}, spacing["mt-auto"]),
    bigBlockContainer: _objectSpread({}, spacing["my-auto"]),
    description: function description(_ref10) {
      var descriptionLines = _ref10.descriptionLines,
          link = _ref10.link;
      return _objectSpread(_objectSpread(_objectSpread({}, spacing["mb-".concat(link ? "2" : "0")]), spacing["pr-1"]), {}, {
        overflow: "hidden",
        textOverflow: "ellipsis",
        display: "-webkit-box",
        "-webkit-line-clamp": descriptionLines,
        "-webkit-box-orient": "vertical"
      });
    },
    title: function title(_ref11) {
      var titleLines = _ref11.titleLines;
      return _objectSpread(_objectSpread({}, spacing["mb-2"]), {}, {
        overflow: "hidden",
        textOverflow: "ellipsis",
        display: "-webkit-box",
        "-webkit-line-clamp": titleLines,
        "-webkit-box-orient": "vertical"
      });
    },
    negativeMargin: _objectSpread({}, spacing["mnt-3"]),
    h100: _objectSpread({}, spacing["h-100"]),
    fullHeightImage: _objectSpread(_objectSpread({}, spacing["my-auto"]), {}, {
      maxHeight: "120px",
      maxWidth: "270px"
    }),
    centerImage: {
      display: "flex",
      justifyContent: "center"
    },
    // Metric block
    metricIconContainer: function metricIconContainer(_ref12) {
      var color = _ref12.color;
      return _objectSpread(_objectSpread(_objectSpread({}, spacing["mt-3"]), spacing["mb-4"]), {}, {
        backgroundColor: colors[color][100],
        borderRadius: "8px",
        width: "50px",
        height: "50px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        "& svg": {
          fill: "".concat(colors[color][500], " !important")
        }
      });
    },
    metricIcon: function metricIcon(_ref13) {
      var color = _ref13.color;
      return {
        "& path": {
          fill: colors[color][500]
        }
      };
    },
    iconCheck: function iconCheck(_ref14) {
      var active = _ref14.active,
          color = _ref14.color;
      return {
        "& path": {
          fill: "".concat(active ? colors[color][100] : colors.secondary[700], " !important")
        }
      };
    },
    value: _objectSpread(_objectSpread({}, spacing["pr-1"]), {}, {
      display: "inline-block"
    }),
    currency: {
      display: "inline-block"
    },
    // Icon block
    iconContainer: function iconContainer(_ref15) {
      var color = _ref15.color,
          active = _ref15.active;
      var iconColor = colors[color][500];
      if (color === "neutral" && !active) iconColor = colors[color][900];else if (active) iconColor = colors[color][100];
      return {
        "& svg": {
          height: "26px",
          width: "26px",
          "& path": {
            fill: "".concat(iconColor, " !important")
          }
        }
      };
    },
    applied: function applied(_ref16) {
      var color = _ref16.color;
      return {
        border: "3px solid ".concat(color === "neutral" ? colors[color][900] : colors[color][500]),
        "& h6": {
          color: getTextColor(colors, color)
        },
        "& p": {
          color: getTextColor(colors, color)
        },
        "& a": {
          color: getTextColor(colors, color)
        }
      };
    },
    inactive: function inactive(_ref17) {
      var color = _ref17.color;
      return {
        border: "3px solid ".concat(colors[color][300]),
        "& h6": {
          color: colors[color][500]
        },
        "& p": {
          color: colors[color][500]
        },
        "& a": {
          color: colors[color][500]
        }
      };
    },
    // dashed block
    dashedBlock: function dashedBlock(_ref18) {
      var color = _ref18.color,
          active = _ref18.active;
      return {
        border: "2px dashed ".concat(active ? colors[color][900] : colors[color][500]),
        "& svg": {
          "& path": {
            fill: active ? colors[color][900] : colors[color][500]
          }
        }
      };
    }
  };
};

export default makeStyles(styles);