import _typeof from "@babel/runtime/helpers/typeof";

/* eslint-disable react/no-multi-comp */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import split from "lodash-es/split";
import { onKeyDown } from "../../utils/helpers";
import { getColorsLabels } from "../styles/colors";
import Typography from "../Typography";
import OpenBlock from "../Icon/OpenBlock";
import CheckRound from "../Icon/CheckRound";
import Grid from "../Grid";
import Image from "../Image";
import At from "../Icon/At";
import getClasses from "./styles";
var colorsList = getColorsLabels(true);

var Block = function Block(props) {
  var children = props.children,
      size = props.size,
      title = props.title,
      description = props.description,
      link = props.link,
      color = props.color;
  var active = props.active,
      selected = props.selected,
      applied = props.applied,
      inactive = props.inactive,
      image = props.image;
  var handleOpen = props.handleOpen,
      className = props.className,
      variant = props.variant,
      icon = props.icon,
      value = props.value;
  var onClick = props.onClick,
      titleLines = props.titleLines,
      descriptionLines = props.descriptionLines,
      imageSize = props.imageSize;
  var width = split(size, "x")[1];
  var height = split(size, "x")[0];

  var imageWidth = function imageWidth() {
    switch (imageSize) {
      case "sm":
        return "25";

      case "md":
        return "50";

      case "lg":
        return "75";

      case "xl":
        return "100";

      default:
        return "100";
    }
  };

  var classes = getClasses({
    width: width,
    height: height,
    color: color,
    titleLines: titleLines,
    descriptionLines: descriptionLines,
    active: active,
    variant: variant,
    link: link,
    imageWidth: imageWidth()
  });

  var getStyle = function getStyle() {
    var styles = [classes.block];

    if (variant === "dashed") {
      styles.push(classes.dashedBlock);
    } else if (selected) styles.push(classes.selected);else if (active) styles.push(classes.active);else if (applied) styles.push(classes.applied);else if (inactive) styles.push(classes.inactive);else styles.push(classes.normal);

    if (!!handleOpen || !!onClick) styles.push(classes.hoverable);
    styles.push(className);
    return styles;
  };

  var getOpenIconButton = function getOpenIconButton() {
    if (!!handleOpen && !selected) {
      return /*#__PURE__*/React.createElement(OpenBlock, {
        className: active ? classnames(classes.icon, classes.iconButton, classes.iconActive) : classnames(classes.icon, classes.iconButton)
      });
    }

    return "";
  };

  var getCheckIconButton = function getCheckIconButton() {
    return /*#__PURE__*/React.createElement(CheckRound, {
      className: classnames(classes.icon, classes.iconButton, classes.iconCheck)
    });
  };

  var getInfoBlock = function getInfoBlock() {
    if (height === "1" && width === "1") {
      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
        className: classes.closeIconContainer
      }, getOpenIconButton()), /*#__PURE__*/React.createElement("div", {
        className: classes.marginAuto
      }, /*#__PURE__*/React.createElement(Typography, {
        variant: "h6",
        className: classes.title
      }, title)));
    }

    if (height === "1" && width === "2") {
      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
        className: classes.closeIconContainer
      }, getOpenIconButton()), /*#__PURE__*/React.createElement(Grid, {
        container: true,
        className: classes.bigBlockContainer,
        justify: "space-between"
      }, /*#__PURE__*/React.createElement(Grid, {
        item: true,
        xs: 7
      }, /*#__PURE__*/React.createElement(Typography, {
        variant: "h6",
        className: classes.title
      }, title), !!description && /*#__PURE__*/React.createElement("div", {
        className: classes.description
      }, /*#__PURE__*/React.createElement(Typography, {
        variant: "body0"
      }, description)), !!link && /*#__PURE__*/React.createElement("div", {
        className: classes.link
      }, /*#__PURE__*/React.createElement(Typography, {
        variant: "body0",
        weight: "bold"
      }, link))), !!image && /*#__PURE__*/React.createElement("img", {
        src: image,
        className: classnames(classes.smallImage),
        alt: "block illustration"
      })));
    }

    if (height === "1" && width > "2") {
      return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Grid, {
        container: true,
        justify: "space-between",
        className: classes.h100
      }, /*#__PURE__*/React.createElement(Grid, {
        item: true,
        xs: 7,
        className: classes.bigBlockContainer
      }, /*#__PURE__*/React.createElement(Typography, {
        variant: "h6",
        className: classes.title
      }, title), !!description && /*#__PURE__*/React.createElement("div", {
        className: classes.description
      }, /*#__PURE__*/React.createElement(Typography, {
        variant: "body0"
      }, description)), !!link && /*#__PURE__*/React.createElement("div", {
        className: classes.link
      }, /*#__PURE__*/React.createElement(Typography, {
        variant: "body0",
        weight: "bold"
      }, link))), !!image && /*#__PURE__*/React.createElement(Grid, {
        item: true,
        xs: true,
        className: classes.centerImage
      }, /*#__PURE__*/React.createElement("img", {
        src: image,
        className: classes.fullHeightImage,
        alt: "block illustration"
      })), handleOpen && /*#__PURE__*/React.createElement(Grid, {
        item: true
      }, getOpenIconButton())));
    }

    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: classes.closeIconContainer
    }, getOpenIconButton()), /*#__PURE__*/React.createElement("div", {
      className: classes.bigBlockContainer
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "h6",
      className: classes.title
    }, title), !!description && /*#__PURE__*/React.createElement("div", {
      className: classes.description
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "body0"
    }, description)), !!link && /*#__PURE__*/React.createElement("div", {
      className: classes.link
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "body0",
      weight: "bold"
    }, link)), !!image && /*#__PURE__*/React.createElement(Image, {
      size: "auto",
      src: image,
      className: classes.image,
      alt: "block illustration"
    })));
  };

  var getMetricText = function getMetricText() {
    return _typeof(value) === "object" ? value : {
      value: value,
      currency: ""
    };
  };

  var getMetricBlock = function getMetricBlock() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: classes.closeIconContainer
    }, getOpenIconButton()), /*#__PURE__*/React.createElement("div", {
      className: classes.metricIconContainer
    }, icon), /*#__PURE__*/React.createElement("div", {
      className: classes.marginAuto
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "body0",
      className: classes.description
    }, description), /*#__PURE__*/React.createElement("div", {
      className: classnames.description
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "h2",
      color: color,
      shade: active ? 100 : 500,
      className: classes.value,
      weight: "bold"
    }, getMetricText().value), getMetricText().currency !== "" && /*#__PURE__*/React.createElement(Typography, {
      variant: "f2-12",
      color: color,
      shade: active ? 100 : 500,
      className: classes.currency,
      upperCase: true
    }, getMetricText().currency))));
  };

  var getCustomBlock = function getCustomBlock() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: classes.closeIconContainer
    }, getOpenIconButton()), children);
  };

  var getIconBlock = function getIconBlock() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: classes.closeIconContainer
    }, applied ? getCheckIconButton() : getOpenIconButton()), /*#__PURE__*/React.createElement("div", {
      className: classes.iconContainer
    }, icon), /*#__PURE__*/React.createElement("div", {
      className: classes.marginAuto
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "body0",
      className: classes.description,
      gutterBottom: true
    }, description)));
  };

  var getDashedBlock = function getDashedBlock() {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: classes.closeIconContainer
    }, getOpenIconButton()), icon, /*#__PURE__*/React.createElement("div", {
      className: classes.marginAuto
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "body0",
      shade: active ? 900 : 500,
      className: classes.description,
      color: color
    }, description)));
  };

  var getAction = function getAction() {
    if (onClick) return onClick;
    if (handleOpen) return handleOpen;
    return undefined;
  };

  return /*#__PURE__*/React.createElement("div", {
    className: classnames(getStyle()),
    onClick: getAction(),
    onKeyDown: onKeyDown(getAction()),
    role: "button",
    tabIndex: 0
  }, variant === "info" && getInfoBlock(), variant === "metric" && getMetricBlock(), variant === "icon" && getIconBlock(), variant === "custom" && getCustomBlock(), variant === "dashed" && getDashedBlock());
};

Block.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  link: PropTypes.string,
  size: PropTypes.oneOf(["1x1", "1x2", "2x2", "1x4"]),
  active: PropTypes.bool,
  selected: PropTypes.bool,
  applied: PropTypes.bool,
  inactive: PropTypes.bool,
  handleOpen: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  onClick: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  titleLines: PropTypes.number,
  descriptionLines: PropTypes.number,
  variant: PropTypes.oneOf(["info", "metric", "icon", "dashed", "custom"]),
  icon: PropTypes.instanceOf(Object),
  color: PropTypes.oneOf(colorsList),
  imageSize: PropTypes.oneOf(["sm", "md", "lg", "xl"])
};
Block.defaultProps = {
  className: "",
  title: "",
  description: "",
  link: "",
  size: "1x1",
  active: false,
  selected: false,
  applied: false,
  inactive: false,
  handleOpen: undefined,
  onClick: undefined,
  titleLines: 3,
  descriptionLines: 4,
  variant: "info",
  icon: /*#__PURE__*/React.createElement(At, {
    color: "primary"
  }),
  color: "primary",
  imageSize: "xl"
};
export default Block;