import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      typography = _ref$cleverUI.typography,
      colors = _ref$cleverUI.colors;
  var sizing = typography.sizing;
  return {
    notchedOutline: function notchedOutline(_ref2) {
      var color = _ref2.color;
      return {
        borderColor: "".concat(colors[color][500], " !important")
      };
    },
    notchedOutlineLabel: function notchedOutlineLabel(_ref3) {
      var color = _ref3.color;
      return {
        color: "".concat(colors[color][500], " !important")
      };
    },
    labelRoot: {
      fontFamily: typography.font1,
      fontWeight: "bold",
      fontSize: sizing.body0,
      color: colors.neutral[700],
      marginBottom: "-10px",
      lineHeight: 1
    },
    shrink: {
      fontFamily: typography.font2,
      transform: "translate(14px, -6px) scale(0.95) !important"
    },
    inputBase: function inputBase(_ref4) {
      var size = _ref4.size;
      return {
        height: size === "small" ? "36px" : "56px",
        padding: 0,
        color: typography.color,
        fontSize: sizing.body0,
        fontWeight: "bold"
      };
    },
    input: function input() {
      return {
        "&::placeholder": {
          color: colors.neutral[700],
          fontSize: sizing.body0,
          fontWeight: "bold",
          opacity: 1
        }
      };
    },
    singleLine: {
      height: 30 + typography.fontSizeBase.constant + typography.fontSizeBase.unit
    },
    standardInput: {
      height: "auto"
    },
    labelOutlined: {
      transform: "translate(14px, 22px) scale(1)"
    },
    selectInputBase: function selectInputBase(_ref5) {
      var size = _ref5.size;
      return {
        height: size === "small" ? "36px" : "56px",
        padding: "0 10px",
        color: typography.color,
        fontFamily: typography.font1,
        fontSize: "12px",
        fontWeight: "bold",
        borderRadius: 4,
        cursor: "pointer"
      };
    },
    selectInput: {
      cursor: "pointer",
      paddingLeft: "10px",
      boxSizing: "border-box",
      width: "auto"
    }
  };
};

export default makeStyles(styles);