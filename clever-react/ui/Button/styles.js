import _defineProperty from "@babel/runtime/helpers/defineProperty";
import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      typography = _ref$cleverUI.typography,
      shadows = _ref$cleverUI.shadows,
      breakpoints = _ref.breakpoints;
  return {
    baseStyles: function baseStyles(_ref2) {
      var size = _ref2.size,
          color = _ref2.color;
      return _defineProperty({
        fontWeight: 700,
        fontFamily: typography.font1,
        borderRadius: "8px",
        boxShadow: "none",
        textAlign: "center",
        minWidth: size === "large" ? "256px" : "192px",
        "&:disabled": {
          color: colors[color][300]
        }
      }, breakpoints.down("sm"), {
        width: "100%"
      });
    },
    large: {
      fontSize: typography.fontSizeBase.constant + typography.fontSizeBase.unit,
      minHeight: "48px",
      minWidth: "256px"
    },
    medium: {
      fontSize: typography.fontSizeBase.constant + typography.fontSizeBase.unit,
      minHeight: "48px",
      minWidth: "192px"
    },
    small: {
      fontSize: "14px",
      minHeight: "36px",
      minWidth: "fit-content",
      textTransform: "none"
    },
    contained: function contained(_ref4) {
      var color = _ref4.color;
      return {
        backgroundColor: colors[color][500],
        color: colors[color][100],
        "&:hover": {
          color: "white",
          backgroundColor: colors[color][700],
          boxShadow: shadows.hover
        },
        "&:focus": {
          color: "white",
          backgroundColor: colors[color][700],
          boxShadow: shadows.hover
        }
      };
    },
    outlined: function outlined(_ref5) {
      var color = _ref5.color;
      return {
        color: colors[color][500],
        border: "solid",
        "&:hover": {
          color: colors[color][700],
          borderColor: colors[color][700],
          boxShadow: shadows.hover
        },
        "&:focus": {
          color: colors[color][700],
          borderColor: colors[color][700],
          boxShadow: shadows.hover
        }
      };
    },
    disabled: function disabled(_ref6) {
      var color = _ref6.color,
          variant = _ref6.variant;
      return {
        color: colors[color][300],
        backgroundColor: variant === "primary" ? colors[color][100] : null,
        borderColor: variant === "secondary" ? colors[color][100] : null
      };
    },
    iconContainer: {
      display: "flex"
    }
  };
};

export default makeStyles(styles);