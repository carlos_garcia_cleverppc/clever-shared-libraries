import _extends from "@babel/runtime/helpers/extends";
import _defineProperty from "@babel/runtime/helpers/defineProperty";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["children", "className", "color", "variant", "size", "disabledClickable", "disabled", "isLoading"];

/* eslint-disable react/require-default-props */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import MaterialButton from "@material-ui/core/Button";
import { getColorsLabels } from "../styles/colors";
import ArrowRight from "../Icon/ArrowRight";
import Grid from "../Grid";
import Loading from "./Loading";
import getClasses from "./styles";
var colorsList = getColorsLabels(true);
var Button = /*#__PURE__*/React.forwardRef(function (props, ref) {
  var _classnames;

  var children = props.children,
      className = props.className,
      color = props.color,
      variant = props.variant,
      size = props.size,
      disabledClickable = props.disabledClickable,
      disabled = props.disabled,
      isLoading = props.isLoading,
      buttonProps = _objectWithoutProperties(props, _excluded);

  var classes = getClasses(props);
  var buttonClassName = classnames(className, classes.baseStyles, (_classnames = {}, _defineProperty(_classnames, classes.outlined, variant === "secondary"), _defineProperty(_classnames, classes.contained, variant === "primary"), _defineProperty(_classnames, classes.medium, size === "medium"), _defineProperty(_classnames, classes.large, size === "large"), _defineProperty(_classnames, classes.small, size === "small"), _defineProperty(_classnames, classes.disabled, disabled || disabledClickable), _classnames));
  var hasShortContent = typeof children === "string" && children.length <= 10;
  return /*#__PURE__*/React.createElement(MaterialButton, _extends({
    className: buttonClassName,
    disabled: disabled,
    ref: ref
  }, buttonProps), size === "large" && /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: true
  }, children), isLoading ? /*#__PURE__*/React.createElement(Loading, null) : /*#__PURE__*/React.createElement(Grid, {
    item: true,
    className: classes.iconContainer
  }, /*#__PURE__*/React.createElement(ArrowRight, {
    color: "currentColor",
    size: 20
  }))), size !== "large" && /*#__PURE__*/React.createElement(React.Fragment, null, children, isLoading && /*#__PURE__*/React.createElement(Loading, {
    absolute: hasShortContent
  })));
});
Button.propTypes = {
  variant: PropTypes.oneOf(["primary", "secondary"]),
  size: PropTypes.oneOf(["medium", "large", "small"]),
  disabled: PropTypes.bool,
  disabledClickable: PropTypes.bool,
  isLoading: PropTypes.bool,
  href: PropTypes.string,
  target: PropTypes.string,
  onClick: PropTypes.func,
  color: PropTypes.oneOf(colorsList),
  type: PropTypes.oneOf(["button", "reset", "submit"])
};
Button.defaultProps = {
  variant: "primary",
  size: "medium",
  disabled: false,
  isLoading: false,
  color: "blue",
  type: "button"
};
export default Button;