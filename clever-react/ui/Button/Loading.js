import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import React from "react";
import classnames from "classnames";
import LoadingIcon from "../Loading";
import makeStyles from "../styles/makeStyles";
var getClasses = makeStyles(function (_ref) {
  var spacing = _ref.cleverUI.spacing;
  return {
    container: _objectSpread({}, spacing["ml-3"]),
    absolute: {
      marginRight: "-25px"
    },
    loading: {
      margin: "0!important" // override default loading margin

    }
  };
});

var Loading = function Loading(_ref2) {
  var absolute = _ref2.absolute;
  var classes = getClasses();
  var containerClassName = classnames(classes.container, _defineProperty({}, classes.absolute, absolute));
  return /*#__PURE__*/React.createElement("div", {
    className: containerClassName
  }, /*#__PURE__*/React.createElement(LoadingIcon, {
    size: 14,
    className: classes.loading,
    color: "currentColor"
  }));
};

export default Loading;