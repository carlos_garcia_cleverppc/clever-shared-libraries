import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";
var getClasses = makeStyles(function (_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      spacing = _ref$cleverUI.spacing,
      colors = _ref$cleverUI.colors,
      breakpoints = _ref.breakpoints;
  return {
    root: _objectSpread(_objectSpread({}, spacing["py-0"]), {}, {
      borderRadius: "8px" // maxHeight: "unset",

    }),
    stripe: {
      height: "4px",
      width: "100%",
      backgroundImage: "linear-gradient(90deg, ".concat(colors.primary[500], " 33.33%, ").concat(colors.warning[500], " 33.33%, ").concat(colors.warning[500], " 66.66%, ").concat(colors.secondary[500], " 66.66%)"),
      backgroundSize: "100% 100%",
      backgroundRepeat: "no-repeat",
      margin: "0px"
    },
    container: function container(_ref2) {
      var _objectSpread2;

      var darkMode = _ref2.darkMode;
      return _objectSpread(_objectSpread(_objectSpread(_objectSpread({
        overflowY: "auto"
      }, spacing["pt-7"]), spacing["pb-6"]), darkMode && {
        background: "#424242",
        color: colors.neutral[300]
      }), {}, (_objectSpread2 = {}, _defineProperty(_objectSpread2, breakpoints.down("sm"), _objectSpread(_objectSpread({}, spacing["pb-6"]), {}, {
        maxHeight: "55vh",
        overflow: "auto"
      })), _defineProperty(_objectSpread2, "&::-webkit-scrollbar", {
        width: "12px",
        backgroundColor: "#fff"
      }), _defineProperty(_objectSpread2, "&::-webkit-scrollbar-track", {
        backgroundColor: "#fff"
      }), _defineProperty(_objectSpread2, "&::-webkit-scrollbar-thumb", {
        backgroundColor: colors.neutral[300],
        borderRadius: "16px",
        border: "4px solid #fff"
      }), _objectSpread2));
    },
    content: _objectSpread(_objectSpread(_objectSpread({}, spacing["m-0"]), spacing["mt-5"]), {}, {
      width: "100%"
    }),
    closeIconContainer: _objectSpread(_objectSpread(_objectSpread(_objectSpread({}, spacing["mt-5"]), spacing["ml-6"]), spacing["mb-3"]), {}, {
      position: "absolute"
    }),
    iconButton: function iconButton(_ref3) {
      var darkMode = _ref3.darkMode;
      return _objectSpread(_objectSpread({}, darkMode && {
        backgroundColor: "#333333 !important"
      }), {}, {
        "& path": {
          fill: "".concat(darkMode ? colors.neutral[300] : colors.neutral[900], " !important")
        },
        zIndex: "1",
        "&:hover": {
          backgroundColor: "".concat(darkMode ? "#333333" : colors.neutral[300], " !important")
        }
      });
    },
    image: _objectSpread(_objectSpread({}, spacing["w-auto"]), {}, {
      maxHeight: "200px"
    }),
    imageContainer: {
      textAlign: "center"
    },
    drawerActions: function drawerActions(_ref4) {
      var darkMode = _ref4.darkMode;
      return _objectSpread(_objectSpread(_objectSpread(_objectSpread({}, spacing["pb-4"]), spacing["pt-5"]), spacing["px-6"]), {}, {
        backgroundColor: darkMode ? "#333333" : colors.neutral[100],
        "& button": _objectSpread({}, spacing["mb-3"])
      });
    },
    fullWidthDialog: _objectSpread({}, spacing["mx-8"]),
    primaryButton: {
      marginLeft: "auto"
    },
    secondaryButton: _objectSpread({}, spacing["mb-5"])
  };
});
export default getClasses;