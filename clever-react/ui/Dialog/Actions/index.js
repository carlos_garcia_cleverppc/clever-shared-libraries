import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Grid from "../../Grid";
import Link from "../../Link";
import Button from "../../Button";
import makeStyles from "../../styles/makeStyles";
var getClasses = makeStyles(function (_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    root: function root(_ref2) {
      var darkMode = _ref2.darkMode;
      return _objectSpread(_objectSpread(_objectSpread(_objectSpread({}, spacing["pb-3"]), spacing["pt-4"]), spacing["px-5"]), {}, {
        backgroundColor: darkMode ? "#333333" : colors.neutral[100],
        display: "flex",
        justifyContent: "space-between",
        order: 2,
        "& button": _objectSpread({}, spacing["mb-1"])
      });
    },
    primaryButton: {
      marginLeft: "auto"
    }
  };
});

var DialogActions = function DialogActions(_ref3) {
  var className = _ref3.className,
      buttons = _ref3.buttons,
      buttonPrimary = _ref3.buttonPrimary,
      buttonSecondary = _ref3.buttonSecondary,
      darkMode = _ref3.darkMode;
  var classes = getClasses({
    darkMode: darkMode
  });
  return /*#__PURE__*/React.createElement(Grid, {
    container: true,
    className: classnames(classes.root, className),
    alignItems: "center"
  }, (!!buttonPrimary || !!buttonSecondary) && /*#__PURE__*/React.createElement(React.Fragment, null, !!buttonSecondary && /*#__PURE__*/React.createElement(Link, {
    to: buttonSecondary.to,
    target: "_blank",
    external: buttonSecondary.external,
    onClick: buttonSecondary.onClick,
    disabled: buttonSecondary === null || buttonSecondary === void 0 ? void 0 : buttonSecondary.disabled
  }, buttonSecondary.label), !!buttonPrimary && /*#__PURE__*/React.createElement(Button, {
    onClick: buttonPrimary.onClick,
    className: classes.primaryButton,
    disabled: buttonPrimary.disabled,
    isLoading: buttonPrimary.isLoading
  }, buttonPrimary.label)), !buttonPrimary && !buttonSecondary && buttons);
};

DialogActions.propTypes = {
  className: PropTypes.string
};
DialogActions.defaultProps = {
  className: ""
};
export default DialogActions;