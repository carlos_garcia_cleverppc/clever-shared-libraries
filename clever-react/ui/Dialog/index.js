import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import MaterialDialog from "@material-ui/core/Dialog";
import Grid from "../Grid";
import IconButton from "../IconButton";
import Button from "../Button";
import Link from "../Link";
import Times from "../Icon/Times";
import Typography from "../Typography";
import Hidden from "../Hidden";
import Drawer from "../Drawer";
import DialogActions from "./Actions";
import getClasses from "./styles";

var Dialog = function Dialog(props) {
  var children = props.children,
      open = props.open,
      onClose = props.onClose,
      className = props.className,
      maxWidth = props.maxWidth,
      fullWidth = props.fullWidth;
  var title = props.title,
      description = props.description,
      image = props.image,
      buttons = props.buttons,
      showImageMobile = props.showImageMobile,
      fullWidthContent = props.fullWidthContent;
  var contentSpacing = props.contentSpacing,
      buttonPrimary = props.buttonPrimary,
      buttonSecondary = props.buttonSecondary,
      darkMode = props.darkMode;
  var variant = props.variant;
  var classes = getClasses({
    darkMode: darkMode
  });

  if (variant === "empty") {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Hidden, {
      smDown: true
    }, /*#__PURE__*/React.createElement(MaterialDialog, {
      open: open,
      onClose: onClose || function () {},
      maxWidth: maxWidth,
      fullWidth: fullWidth,
      classes: {
        paper: classes.root
      }
    }, /*#__PURE__*/React.createElement("div", {
      className: className
    }, children), (buttonPrimary || buttonSecondary || buttons) && /*#__PURE__*/React.createElement(DialogActions, {
      buttons: buttons,
      buttonPrimary: buttonPrimary,
      buttonSecondary: buttonSecondary,
      darkMode: darkMode
    }))), /*#__PURE__*/React.createElement(Hidden, {
      mdUp: true
    }, /*#__PURE__*/React.createElement(Drawer, {
      anchor: "bottom",
      open: open,
      onClose: onClose || function () {}
    }, children)));
  }

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Hidden, {
    smDown: true
  }, /*#__PURE__*/React.createElement(MaterialDialog, {
    open: open,
    onClose: onClose || function () {},
    maxWidth: maxWidth,
    fullWidth: fullWidth,
    classes: {
      paper: classnames(className, classes.root)
    }
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.stripe
  }), /*#__PURE__*/React.createElement("div", {
    className: classes.closeIconContainer
  }, !!onClose && /*#__PURE__*/React.createElement(IconButton, {
    onClick: onClose,
    size: "small",
    icon: /*#__PURE__*/React.createElement(Times, null),
    className: classes.iconButton,
    color: "neutral",
    shade: 100,
    outlined: false
  })), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    justify: "center",
    className: classes.container
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: fullWidthContent ? 12 : 10,
    md: fullWidthContent ? 12 : 8,
    className: fullWidthContent ? classes.fullWidthDialog : ""
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    shade: darkMode ? 300 : 900,
    align: "center",
    gutterBottom: true
  }, title), /*#__PURE__*/React.createElement(Typography, {
    variant: "body1",
    align: "center"
  }, description), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    justify: "center",
    className: classes.content,
    spacing: 2
  }, !!image && !!children && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4,
    className: classes.imageContainer
  }, /*#__PURE__*/React.createElement(Hidden, {
    xsDown: true
  }, /*#__PURE__*/React.createElement("img", {
    className: classes.image,
    src: image,
    alt: "Dialog ilustration"
  }))), !!image && !children && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4,
    className: classes.imageContainer
  }, /*#__PURE__*/React.createElement("img", {
    className: classes.image,
    src: image,
    alt: "Dialog ilustration"
  })), !!children && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: image ? 8 : 12 - contentSpacing
  }, children)))), /*#__PURE__*/React.createElement(DialogActions, {
    buttons: buttons,
    buttonPrimary: buttonPrimary,
    buttonSecondary: buttonSecondary,
    darkMode: darkMode
  }))), /*#__PURE__*/React.createElement(Hidden, {
    mdUp: true
  }, /*#__PURE__*/React.createElement(Drawer, {
    anchor: "bottom",
    open: open,
    onClose: onClose || function () {}
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.stripe
  }), /*#__PURE__*/React.createElement("div", {
    className: classes.closeIconContainer
  }, !!onClose && /*#__PURE__*/React.createElement(IconButton, {
    onClick: onClose,
    size: "small",
    icon: /*#__PURE__*/React.createElement(Times, null),
    className: classes.iconButton
  })), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    justify: "center",
    className: classes.container
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 10,
    md: 8
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    shade: darkMode ? 300 : 900,
    align: "center",
    gutterBottom: true
  }, title), /*#__PURE__*/React.createElement(Typography, {
    variant: "body1",
    align: "center"
  }, description), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    justify: "center",
    className: classes.content,
    spacing: 2
  }, !!image && !!showImageMobile && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 4,
    className: classes.imageContainer
  }, /*#__PURE__*/React.createElement("img", {
    className: classes.image,
    src: image,
    alt: "Dialog ilustration"
  })), !!children && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    md: 8
  }, children)))), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    className: classnames(classes.drawerActions, className),
    alignItems: "center",
    justify: "center"
  }, (!!buttonPrimary || !!buttonSecondary) && /*#__PURE__*/React.createElement(React.Fragment, null, !!buttonSecondary && /*#__PURE__*/React.createElement(Link, {
    to: buttonSecondary.to,
    target: "_blank",
    onClick: buttonSecondary.onClick,
    className: classes.secondaryButton,
    disabled: buttonSecondary === null || buttonSecondary === void 0 ? void 0 : buttonSecondary.disabled
  }, buttonSecondary.label), !!buttonPrimary && /*#__PURE__*/React.createElement(Button, {
    onClick: buttonPrimary.onClick,
    disabled: buttonPrimary === null || buttonPrimary === void 0 ? void 0 : buttonPrimary.disabled,
    isLoading: buttonPrimary === null || buttonPrimary === void 0 ? void 0 : buttonPrimary.isLoading,
    className: classes.primaryButton
  }, buttonPrimary.label)), !buttonPrimary && !buttonSecondary && buttons))));
};

Dialog.propTypes = {
  open: PropTypes.bool,
  variant: PropTypes.oneOf(["default", "empty"]),
  onClose: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  className: PropTypes.string,
  maxWidth: PropTypes.oneOf(["xs", "sm", "md", "lg", "xl"]),
  fullWidth: PropTypes.bool,
  title: PropTypes.string,
  description: PropTypes.oneOfType([PropTypes.instanceOf(Object), PropTypes.string]),
  image: PropTypes.string,
  buttons: PropTypes.instanceOf(Object),
  showImageMobile: PropTypes.bool,
  contentSpacing: PropTypes.number,
  fullWidthContent: PropTypes.bool,
  buttonPrimary: PropTypes.exact({
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
    isLoading: PropTypes.bool
  }),
  buttonSecondary: PropTypes.exact({
    label: PropTypes.string.isRequired,
    to: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool
  }),
  darkMode: PropTypes.bool
};
Dialog.defaultProps = {
  open: false,
  variant: "default",
  onClose: false,
  className: "",
  maxWidth: "md",
  fullWidth: true,
  title: "",
  description: "",
  image: "",
  buttons: undefined,
  showImageMobile: false,
  contentSpacing: 4,
  fullWidthContent: false,
  buttonPrimary: undefined,
  buttonSecondary: undefined,
  darkMode: false
};
export default Dialog;