import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";
/*
function getBackgroundColor(colors, typeColor) {
  switch (typeColor) {
    case "primary":
      return colors.primary[100];
    case "secondary":
      return colors.secondary[100];
    case "warning":
      return colors.warning[100];
    case "danger":
      return colors.danger[100];
    default:
      return colors.primary[100];
  }
}

function getColor(colors, typeColor) {
  switch (typeColor) {
    case "primary":
      return colors.primary[700];
    case "secondary":
      return colors.secondary[700];
    case "warning":
      return colors.warning[700];
    case "danger":
      return colors.danger[700];
    default:
      return colors[500];
  }
}
*/

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing,
      typography = _ref$cleverUI.typography;
  var sizing = typography.sizing;
  return {
    root: function root(_ref2) {
      var color = _ref2.color;
      return {
        borderRadius: "16px",
        backgroundColor: colors[color][100],
        color: colors[color][500],
        height: "auto",
        width: "fit-content",
        "& p": {
          wordBreak: "break-word"
        }
      };
    },
    medium: _objectSpread(_objectSpread({}, spacing["px-3"]), {}, {
      "& p": {
        fontSize: sizing.body1
      }
    }),
    small: _objectSpread(_objectSpread({}, spacing["px-2"]), {}, {
      "& p": {
        fontSize: "12px"
      }
    }),
    extraSmall: _objectSpread(_objectSpread({}, spacing["px-2"]), {}, {
      paddingTop: "1px !important",
      paddingBottom: "1px !important",
      "& p": {
        fontSize: "12px"
      }
    }),
    contained: _objectSpread({}, spacing["py-2"]),
    outlined: function outlined(_ref3) {
      var color = _ref3.color;
      return _objectSpread(_objectSpread({}, spacing["py-1"]), {}, {
        border: "solid 1px ".concat(colors[color][500])
      });
    },
    deleteIcon: _objectSpread({}, spacing["ml-2"]),
    loading: _objectSpread(_objectSpread(_objectSpread({}, spacing["m-0"]), spacing["ml-2"]), {}, {
      marginBottom: "-2px",
      width: "15px !important",
      height: "fit-content !important",
      maxHeight: "15px"
    }),
    itemFlex: {
      display: "flex"
    }
  };
};

export default makeStyles(styles);