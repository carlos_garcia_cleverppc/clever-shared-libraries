import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getColorsLabels } from "../styles/colors";
import Typography from "../Typography";
import Grid from "../Grid";
import Loading from "../Loading";
import IconButton from "../IconButton";
import Icon from "../Icon";
import getClasses from "./styles";
var colorsList = getColorsLabels(true);

var Chip = function Chip(props) {
  var className = props.className,
      label = props.label,
      color = props.color,
      onDelete = props.onDelete,
      loading = props.loading;
  var variant = props.variant,
      size = props.size,
      icon = props.icon;
  var classes = getClasses({
    color: color
  });

  var getIcon = function getIcon() {
    if (loading) {
      return /*#__PURE__*/React.createElement(Loading, {
        className: classes.loading
      });
    }

    if (onDelete) {
      return /*#__PURE__*/React.createElement(IconButton, {
        onClick: onDelete,
        className: classes.deleteIcon,
        size: "small",
        outlined: false,
        color: color,
        icon: /*#__PURE__*/React.createElement(Icon, {
          icon: icon,
          size: 18
        })
      });
    }

    return "";
  };

  return /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center",
    className: classnames(classes.root, classes[variant], classes[size], className)
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: true,
    className: classes.itemFlex
  }, /*#__PURE__*/React.createElement(Typography, {
    color: color
  }, label)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    className: classes.itemFlex
  }, getIcon()));
};

Chip.propTypes = {
  variant: PropTypes.oneOf(["contained", "outlined"]),
  className: PropTypes.string,
  label: PropTypes.string.isRequired,
  color: PropTypes.oneOf(colorsList),
  size: PropTypes.oneOf(["extraSmall", "small", "medium"]),
  onDelete: PropTypes.func,
  loading: PropTypes.bool,
  icon: PropTypes.string
};
Chip.defaultProps = {
  className: "",
  variant: "contained",
  color: "primary",
  size: "medium",
  onDelete: undefined,
  loading: false,
  icon: "Times"
};
export default Chip;