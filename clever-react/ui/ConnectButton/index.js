import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import MaterialButton from "@material-ui/core/Button";
import getClasses from "./styles";

var ConnectButton = function ConnectButton(props) {
  var children = props.children,
      className = props.className,
      size = props.size,
      onClick = props.onClick;
  var disabled = props.disabled,
      label = props.label,
      image = props.image;
  var classes = getClasses();
  return /*#__PURE__*/React.createElement(MaterialButton, {
    className: classnames(classes.root, className),
    size: size,
    onClick: onClick,
    disabled: disabled
  }, size === "medium" && children, /*#__PURE__*/React.createElement("div", {
    className: classes.logoContainer
  }, /*#__PURE__*/React.createElement("img", {
    src: image,
    alt: "Google Logo",
    className: classes.logo
  })), /*#__PURE__*/React.createElement("span", {
    className: classes.label
  }, label));
};

ConnectButton.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(["medium", "large"]),
  disabled: PropTypes.bool,
  onClick: PropTypes.func
};
ConnectButton.defaultProps = {
  className: "",
  size: "medium",
  disabled: false,
  onClick: function onClick() {}
};
export default ConnectButton;