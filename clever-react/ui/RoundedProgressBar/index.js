import _toConsumableArray from "@babel/runtime/helpers/toConsumableArray";
import _slicedToArray from "@babel/runtime/helpers/slicedToArray";

/* eslint-disable no-extra-boolean-cast */
import React, { useEffect, useState, useRef, createRef } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { getColorsLabels, getShades } from "../styles/colors";
import getClasses from "./styles";
import useTheme from "../styles/useTheme";
var colorsList = getColorsLabels(true);
var shadesList = getShades(true);

var BudgetManagement = function BudgetManagement(props) {
  var size = props.size,
      strokeWidth = props.strokeWidth,
      circleOneStroke = props.circleOneStroke,
      circleTwoStroke = props.circleTwoStroke,
      children = props.children;
  var progress = props.progress,
      color = props.color,
      shade = props.shade,
      progressesArray = props.progressesArray,
      className = props.className,
      id = props.id;
  var classes = getClasses({
    color: color,
    shade: shade
  });

  var _useTheme = useTheme(),
      colors = _useTheme.cleverUI.colors;

  var circleRef = useRef(null);
  var circleRefs = useRef([]);

  if (circleRefs.current.length !== progressesArray.length) {
    circleRefs.current = Array(progressesArray.length).fill().map(function (_, i) {
      return circleRefs.current[i] || /*#__PURE__*/createRef();
    });
  }

  var center = size / 2;
  var radius = size / 2 - strokeWidth / 2;
  var circumference = 2 * Math.PI * radius;

  var _useState = useState(1 * circumference),
      _useState2 = _slicedToArray(_useState, 2),
      offset = _useState2[0],
      setOffset = _useState2[1];

  var _useState3 = useState(progressesArray.map(function () {
    return 1 * circumference;
  })),
      _useState4 = _slicedToArray(_useState3, 2),
      offsetValues = _useState4[0],
      setOffsetValues = _useState4[1];

  useEffect(function () {
    if (progressesArray.length === 0) {
      var progressOffset = (100 - progress) / 100 * circumference;
      setOffset(progressOffset);
      circleRef.current.style = "transition: stroke-dashoffset 1250ms ease-in-out;";
    } else {
      var progresses = progressesArray.reverse().map(function (prog) {
        return prog.progress;
      }).reverse();
      var index = 0;

      var arr = _toConsumableArray(offsetValues);

      while (index < progressesArray.length) {
        var _progressOffset = (100 - progresses[index]) / 100 * circumference;

        arr[index] = _progressOffset;
        if (index < progresses.length - 1) progresses[index + 1] += progresses[index];
        circleRefs.current[index].current.style = "transition: stroke-dashoffset 1250ms ease-in-out;";
        index += 1;
      }

      setOffsetValues(arr.reverse());
    }
  }, [setOffset, circumference, progress, progressesArray, offset]);
  return /*#__PURE__*/React.createElement("div", {
    className: classnames(classes.roundedProgressContainer, className),
    id: id
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.children
  }, children), progressesArray.length === 0 && /*#__PURE__*/React.createElement("svg", {
    className: classes.svg,
    width: size,
    height: size
  }, /*#__PURE__*/React.createElement("circle", {
    className: classes.svgCircleBg,
    stroke: circleOneStroke,
    cx: center,
    cy: center,
    r: radius,
    strokeWidth: strokeWidth
  }), /*#__PURE__*/React.createElement("circle", {
    className: classes.svgCircle,
    ref: circleRef,
    stroke: circleTwoStroke,
    cx: center,
    cy: center,
    r: radius,
    strokeWidth: strokeWidth,
    strokeDasharray: circumference,
    strokeDashoffset: offset
  })), progressesArray.length > 0 && /*#__PURE__*/React.createElement("svg", {
    className: classes.svg,
    width: size,
    height: size
  }, /*#__PURE__*/React.createElement("circle", {
    className: classes.svgCircleBg,
    stroke: circleOneStroke,
    cx: center,
    cy: center,
    r: radius,
    strokeWidth: strokeWidth
  }), progressesArray.map(function (prog, index) {
    return /*#__PURE__*/React.createElement("circle", {
      className: classes.svgCircleMultiple,
      stroke: colors[prog.color][!!prog.shade ? prog.shade : 500],
      cx: center,
      cy: center,
      r: radius,
      strokeWidth: strokeWidth,
      strokeDasharray: circumference,
      strokeDashoffset: offsetValues[index],
      ref: circleRefs.current[index],
      key: "circle-".concat(index)
    });
  })));
};

BudgetManagement.propTypes = {
  size: PropTypes.number.isRequired,
  color: PropTypes.oneOf(colorsList),
  shade: PropTypes.oneOf(shadesList),
  progress: PropTypes.number,
  strokeWidth: PropTypes.number.isRequired,
  circleOneStroke: PropTypes.string,
  circleTwoStroke: PropTypes.string,
  progressesArray: PropTypes.instanceOf(Object)
};
BudgetManagement.defaultProps = {
  color: "green",
  shade: 500,
  progress: 0,
  circleOneStroke: "",
  circleTwoStroke: "",
  progressesArray: []
};
export default BudgetManagement;