import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var colors = _ref.cleverUI.colors;
  return {
    roundedProgressContainer: {
      display: "grid",
      alignItems: "center",
      justifyItems: "center"
    },
    children: {
      gridArea: "1/1"
    },
    svg: {
      transform: "rotate(-90deg)",
      gridArea: "1/1"
    },
    svgCircleBg: {
      fill: "none",
      stroke: colors.neutral[300]
    },
    svgCircle: function svgCircle(_ref2) {
      var color = _ref2.color,
          shade = _ref2.shade;
      return {
        fill: "none",
        stroke: colors[color][shade],
        strokeLinecap: "round"
      };
    },
    svgCircleMultiple: {
      fill: "none",
      strokeLinecap: "round"
    },
    svgCircleText: {
      fontSize: "2rem",
      textAnchor: "middle",
      fill: "#fff",
      fontWeight: "bold"
    }
  };
};

export default makeStyles(styles);