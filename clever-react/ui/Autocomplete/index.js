import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import MaterialAutocomplete from "@material-ui/lab/Autocomplete";
import getClasses from "./styles";

function Autocomplete(props) {
  var options = props.options,
      getOptionLabel = props.getOptionLabel,
      renderInput = props.renderInput,
      className = props.className,
      onChange = props.onChange,
      inputValue = props.inputValue,
      closeIcon = props.closeIcon,
      renderOption = props.renderOption,
      onInputChange = props.onInputChange,
      getOptionDisabled = props.getOptionDisabled,
      ListboxComponent = props.ListboxComponent,
      noOptionsText = props.noOptionsText;
  var classes = getClasses();
  return /*#__PURE__*/React.createElement(MaterialAutocomplete, {
    classes: {
      endAdornment: classes.endAdornment,
      listbox: classes.listbox,
      noOptions: classes.noOptions,
      input: classes.input
    },
    className: classnames(classes.root, className),
    options: options,
    getOptionLabel: getOptionLabel,
    renderInput: renderInput,
    onChange: onChange,
    inputValue: inputValue,
    closeIcon: closeIcon,
    forcePopupIcon: false,
    renderOption: renderOption,
    onInputChange: onInputChange,
    getOptionDisabled: getOptionDisabled,
    ListboxComponent: ListboxComponent,
    noOptionsText: noOptionsText
  });
}

Autocomplete.propTypes = {
  className: PropTypes.string,
  inputValue: PropTypes.string.isRequired,
  onInputChange: PropTypes.func,
  onChange: PropTypes.func,
  getOptionLabel: PropTypes.func,
  getOptionDisabled: PropTypes.func,
  options: PropTypes.instanceOf(Object).isRequired,
  renderInput: PropTypes.instanceOf(Object).isRequired,
  closeIcon: PropTypes.instanceOf(Object),
  renderOption: PropTypes.instanceOf(Object),
  noOptionsText: PropTypes.string
};
Autocomplete.defaultProps = {
  className: "",
  getOptionLabel: function getOptionLabel() {},
  getOptionDisabled: function getOptionDisabled() {},
  closeIcon: undefined,
  renderOption: undefined,
  onInputChange: function onInputChange() {},
  onChange: function onChange() {},
  noOptionsText: "No options"
};
export default Autocomplete;