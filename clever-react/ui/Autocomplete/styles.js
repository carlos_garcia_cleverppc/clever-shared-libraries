import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      spacing = _ref$cleverUI.spacing,
      typography = _ref$cleverUI.typography,
      colors = _ref$cleverUI.colors;
  return {
    root: _objectSpread({}, spacing["w-100"]),
    endAdorment: _objectSpread({}, spacing["pr-2"]),
    listbox: {
      "&::-webkit-scrollbar": {
        width: "12px",
        backgroundColor: "#fff"
      },
      "&::-webkit-scrollbar-track": {
        backgroundColor: "#fff"
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: colors.neutral[300],
        borderRadius: "16px",
        border: "4px solid #fff"
      }
    },
    input: {
      padding: "0.444rem !important",
      maxHeight: "22px"
    },
    noOptions: {
      fontSize: typography.fontSizeBase.constant + typography.fontSizeBase.unit,
      fontFamily: typography.font2
    }
  };
};

export default makeStyles(styles);