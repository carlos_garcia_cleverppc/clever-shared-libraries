import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    card: {
      width: "250px",
      textAlign: "center",
      backgroundColor: "white",
      borderRadius: "8px",
      overflow: "overlay",
      marginTop: "-164px",
      marginLeft: "40px",
      zIndex: "2",
      boxShadow: "0 2px 21px rgba(0,0,0,.1)"
    },
    header: {
      display: "flex",
      height: "45px",
      backgroundColor: colors.neutral[900],
      alignItems: "center",
      justifyContent: "center"
    },
    content: _objectSpread(_objectSpread({}, spacing["py-3"]), {}, {
      height: "75%"
    })
  };
};

export default makeStyles(styles);