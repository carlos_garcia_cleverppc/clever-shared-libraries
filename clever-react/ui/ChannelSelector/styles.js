import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    positionHorizontal: {
      display: "inline-flex"
    },
    container: function container(_ref2) {
      var size = _ref2.size;
      return _objectSpread(_objectSpread(_objectSpread({}, spacing["mb-5"]), spacing["mx-2"]), {}, {
        width: size,
        height: size,
        minWidth: size,
        position: "relative"
      });
    },
    circle: function circle(_ref3) {
      var color = _ref3.color;
      return {
        border: "3px solid"
      };
    },
    wrapper: {
      width: "100%",
      height: "100%",
      display: "grid",
      borderRadius: "50%",
      "& > *": {
        gridArea: "1/-1",
        borderRadius: "inherit"
      }
    },
    dotContainer: {
      height: "10px",
      width: "100%",
      textAlign: "center",
      position: "absolute",
      bottom: "-4px"
    },
    linkSize: function linkSize(_ref4) {
      var size = _ref4.size;
      return {
        width: size,
        height: size,
        minWidth: size
      };
    },
    defaultBrand: function defaultBrand(_ref5) {
      var size = _ref5.size;
      return {
        backgroundColor: colors.neutral[300],
        width: size,
        height: size,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: "50%",
        "& svg": {
          width: "62%",
          height: "62%"
        }
      };
    },
    activeAll: function activeAll(_ref6) {
      var size = _ref6.size;
      return {
        width: size - 6,
        height: size - 6
      };
    },
    allCopySize: function allCopySize(_ref7) {
      var _allCopySize = _ref7.allCopySize;
      return {
        fontSize: "".concat(_allCopySize, "px")
      };
    }
  };
};

export default makeStyles(styles);