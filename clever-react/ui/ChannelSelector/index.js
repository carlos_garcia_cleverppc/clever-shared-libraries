import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Circle from "../Icon/Circle";
import BrandIcon from "../BrandIcon";
import Link from "../Link";
import Typography from "../Typography";
import getClasses from "./styles";

var ChannelSelector = function ChannelSelector(props) {
  var className = props.className,
      color = props.color,
      size = props.size,
      channels = props.channels,
      allCopy = props.allCopy,
      position = props.position;
  var allCopySize = 40 / allCopy.length;
  var classes = getClasses({
    color: color,
    size: size,
    allCopySize: allCopySize
  });

  var getNameIcon = function getNameIcon(name, connected) {
    var auxName = name.toLowerCase();
    var result = "";

    if (auxName.includes("googleads")) {
      result = "GoogleAdsCircle";
    } else if (auxName.includes("analytics")) {
      result = "GoogleAnalyticsCircle";
    } else if (auxName.includes("merchant")) {
      result = "GoogleMerchantCircle";
    } else if (auxName.includes("facebook")) {
      result = "FacebookCircle";
    } else if (auxName.includes("twitter")) {
      result = "TwitterCircle";
    } else if (auxName.includes("microsoft")) {
      result = "MicrosoftCircle";
    } else if (auxName.includes("linkedin")) {
      result = "LinkedinCircle";
    } else if (auxName.includes("instagram")) {
      result = "InstagramCircle";
    }

    if (!connected) result = "".concat(result, "Unlinked");
    return result;
  };

  var getAllCircle = function getAllCircle(active) {
    return /*#__PURE__*/React.createElement("div", {
      className: classnames(classes.defaultBrand, active ? classes.activeAll : "")
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "f1-12",
      weight: "bold",
      shade: 900,
      className: classes.allCopySize
    }, allCopy));
  };

  var printIcon = function printIcon(name, active, connected) {
    return /*#__PURE__*/React.createElement("div", {
      className: classes.container,
      key: "channel-".concat(name)
    }, /*#__PURE__*/React.createElement("div", {
      className: classes.wrapper
    }, name === "all" ? /*#__PURE__*/React.createElement(Link, {
      onClick: function onClick() {
        return channels[name].onClick();
      },
      className: classnames(classes.linkSize, active ? classes.circle : "")
    }, getAllCircle(active)) : /*#__PURE__*/React.createElement(Link, {
      onClick: function onClick() {
        return channels[name].onClick();
      },
      className: classnames(classes.linkSize, active ? classes.circle : "")
    }, /*#__PURE__*/React.createElement(BrandIcon, {
      icon: getNameIcon(name, connected),
      size: active ? 36 : 42
    })), active && /*#__PURE__*/React.createElement("div", {
      className: classes.dotContainer
    }, /*#__PURE__*/React.createElement(Circle, {
      color: color,
      size: 6
    }))));
  };

  return /*#__PURE__*/React.createElement("div", {
    className: classnames(position === "horizontal" ? classes.positionHorizontal : "", className)
  }, Object.keys(channels).map(function (brandName) {
    return printIcon(brandName, channels[brandName].active, channels[brandName].connected);
  }));
};

ChannelSelector.propTypes = {
  color: PropTypes.string,
  size: PropTypes.number,
  channels: PropTypes.instanceOf(Object),
  allCopy: PropTypes.string,
  position: PropTypes.oneOf(["vertical", "horizontal"])
};
ChannelSelector.defaultProps = {
  color: "primary",
  size: 42,
  channels: [],
  allCopy: "All",
  position: "vertical"
};
export default ChannelSelector;