import React from "react";
import CheckboxTreeComponent from "react-checkbox-tree";
import PropTypes from "prop-types";
import classnames from "classnames";
import Checkbox from "../Checkbox";
import ChevronRight from "../Icon/ChevronRight";
import ChevronDown from "../Icon/ChevronDown";
import makeStyles from "../styles/makeStyles";
var getClasses = makeStyles(function (_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      typography = _ref$cleverUI.typography;
  return {
    node: {
      fontFamily: typography.font2,
      color: colors.neutral[700],
      "& ol": {
        "& li": {
          "& span": {
            "& label": {
              display: "flex",
              alignItems: "center",
              "&:hover": {
                backgroundColor: colors.primary[100],
                borderRadius: "8px",
                transition: "all .3s ease-in-out"
              }
            }
          }
        }
      }
    }
  };
});

var CheckboxTree = function CheckboxTree(props) {
  var classes = getClasses();
  var nodes = props.nodes,
      className = props.className,
      checked = props.checked,
      _onCheck = props.onCheck,
      expanded = props.expanded;
  var _onExpand = props.onExpand,
      onClick = props.onClick;
  var newNodes = nodes;
  newNodes.forEach(function (node) {
    node.className = classnames(classes.node, className);
  });
  return /*#__PURE__*/React.createElement(CheckboxTreeComponent, {
    nodes: nodes,
    checked: checked,
    expanded: expanded,
    onCheck: function onCheck(checkedAux) {
      return _onCheck(checkedAux);
    },
    onExpand: function onExpand(expandedAux) {
      return _onExpand(expandedAux);
    },
    onClick: onClick,
    onlyLeafCheckboxes: true,
    className: classnames(classes.node, className),
    showNodeIcon: false,
    expandOnClick: true,
    icons: {
      check: /*#__PURE__*/React.createElement(Checkbox, {
        size: "small",
        checked: true
      }),
      uncheck: /*#__PURE__*/React.createElement(Checkbox, {
        size: "small",
        checked: false
      }),
      expandClose: /*#__PURE__*/React.createElement(ChevronRight, {
        size: 24
      }),
      expandOpen: /*#__PURE__*/React.createElement(ChevronDown, {
        size: 24
      })
    }
  });
};

CheckboxTree.propTypes = {
  nodes: PropTypes.instanceOf(Array),
  checked: PropTypes.instanceOf(Array),
  expanded: PropTypes.instanceOf(Array),
  className: PropTypes.string,
  onCheck: PropTypes.func,
  onExpand: PropTypes.func,
  onClick: PropTypes.func
};
CheckboxTree.defaultProps = {
  nodes: [],
  className: "",
  expanded: [],
  checked: [],
  onCheck: function onCheck() {},
  onExpand: function onExpand() {},
  onClick: function onClick() {}
};
export default CheckboxTree;