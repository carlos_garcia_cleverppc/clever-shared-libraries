import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import getClasses from "./styles";

var List = function List(_ref) {
  var className = _ref.className,
      children = _ref.children;
  var classes = getClasses();
  return /*#__PURE__*/React.createElement("ul", {
    className: classnames(className, classes.root)
  }, children);
};

List.propTypes = {
  className: PropTypes.string
};
List.defaultProps = {
  className: ""
};
export default List;