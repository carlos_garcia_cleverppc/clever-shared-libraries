import makeStyles from "../../../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      shadows = _ref$cleverUI.shadows;
  return {
    root: {
      marginLeft: "auto",
      borderRadius: 50,
      "&:hover": {
        boxShadow: shadows.hover
      }
    }
  };
};

export default makeStyles(styles);