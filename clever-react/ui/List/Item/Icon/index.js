import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import IconButton from "../../../IconButton";
import getClasses from "./styles";

var ListItemIcon = function ListItemIcon(_ref) {
  var className = _ref.className,
      icon = _ref.icon,
      onClick = _ref.onClick;
  var classes = getClasses();
  return /*#__PURE__*/React.createElement(IconButton, {
    icon: icon,
    size: "small",
    onClick: onClick,
    className: classnames(className, classes.root),
    color: "primary",
    outlined: false
  });
};

ListItemIcon.propTypes = {
  className: PropTypes.string,
  icon: PropTypes.instanceOf(Object).isRequired,
  onClick: PropTypes.func
};
ListItemIcon.defaultProps = {
  className: "",
  onClick: function onClick() {}
};
export default ListItemIcon;