import _defineProperty from "@babel/runtime/helpers/defineProperty";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["component", "className", "onClick", "children", "selected", "padding", "classes", "hoverSelect"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import getClasses from "./styles";
var ListItem = /*#__PURE__*/React.forwardRef(function (props, ref) {
  var component = props.component,
      className = props.className,
      onClick = props.onClick,
      children = props.children,
      selected = props.selected,
      padding = props.padding,
      classes = props.classes,
      hoverSelect = props.hoverSelect,
      otherProps = _objectWithoutProperties(props, _excluded);

  var componentClasses = getClasses({
    padding: padding,
    onClick: onClick,
    hoverSelect: hoverSelect
  });
  var tagName = component;
  return /*#__PURE__*/React.createElement(tagName, _objectSpread({
    className: classnames(className, componentClasses.root, selected && classes !== null && classes !== void 0 && classes.selected ? classes.selected : ""),
    onClick: onClick,
    ref: ref
  }, otherProps), children);
});
ListItem.propTypes = {
  // eslint-disable-next-line react/no-unused-prop-types
  component: PropTypes.node,
  padding: PropTypes.bool,
  className: PropTypes.string,
  onClick: PropTypes.oneOfType([PropTypes.func, PropTypes.bool]),
  selected: PropTypes.bool,
  hoverSelect: PropTypes.bool
};
ListItem.defaultProps = {
  className: "",
  component: "li",
  padding: true,
  selected: false,
  hoverSelect: false
};
export default ListItem;