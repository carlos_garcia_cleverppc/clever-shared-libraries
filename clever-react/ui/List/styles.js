import makeStyles from "../styles/makeStyles";
var styles = {
  root: {
    listStyle: "none",
    padding: 0
  }
};
export default makeStyles(styles);