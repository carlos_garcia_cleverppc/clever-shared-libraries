import _extends from "@babel/runtime/helpers/extends";
import _defineProperty from "@babel/runtime/helpers/defineProperty";
import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import React, { useState, useLayoutEffect, useRef } from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import Modal from "@material-ui/core/Modal";
import ShowMore from "../Icon/ShowMore";
import IconButton from "../IconButton";
import Button from "../Button";
import Popper from "../Popper";
import MenuItem from "../Menu/Item";
import TextField from "../TextField";
import Typography from "../Typography";
import getClasses from "./styles";

var ModalInput = function ModalInput(props) {
  var open = props.open,
      _onSubmit = props.onSubmit,
      onClose = props.onClose;
  var title = props.title,
      icon = props.icon;
  var defaultValue = props.defaultValue,
      placeholder = props.placeholder,
      maxLength = props.maxLength;
  var menuOptions = props.menuOptions,
      footer = props.footer,
      disablePortal = props.disablePortal,
      className = props.className;
  var buttonText = props.buttonText,
      buttonDisabled = props.buttonDisabled,
      inputProps = props.inputProps;

  var _useState = useState({
    x: 0,
    y: 0
  }),
      _useState2 = _slicedToArray(_useState, 2),
      position = _useState2[0],
      setPosition = _useState2[1];

  var _useState3 = useState(defaultValue),
      _useState4 = _slicedToArray(_useState3, 2),
      inputValue = _useState4[0],
      setInputValue = _useState4[1];

  var _useState5 = useState(null),
      _useState6 = _slicedToArray(_useState5, 2),
      anchorEl = _useState6[0],
      setAnchorEl = _useState6[1];

  var _useState7 = useState(false),
      _useState8 = _slicedToArray(_useState7, 2),
      isClosing = _useState8[0],
      setIsClosing = _useState8[1];

  var paperRef = useRef();
  var classes = getClasses({
    target: position
  });

  var handleClose = function handleClose() {
    setIsClosing(true);
    setTimeout(function () {
      setAnchorEl(null);
      onClose();
      setIsClosing(false);
    }, 1000);
  };

  var isButtonDisabled = function isButtonDisabled() {
    if (inputProps !== null && inputProps !== void 0 && inputProps.value) return inputProps.value === "" || buttonDisabled;
    return inputValue === "" || buttonDisabled;
  };

  useLayoutEffect(function () {
    var element = document.getElementById("simple-popover-button1");
    if (!element || !paperRef.current) return;
    var rect = element.getBoundingClientRect();
    var target = {
      top: Math.floor(rect.y + rect.height / 2),
      left: Math.floor(rect.x + rect.width / 2)
    };

    var _paperRef$current$get = paperRef.current.getBoundingClientRect(),
        width = _paperRef$current$get.width,
        height = _paperRef$current$get.height,
        top = _paperRef$current$get.top,
        left = _paperRef$current$get.left;

    var origin = {
      top: Math.floor(top + height / 2),
      left: Math.floor(left + width / 2)
    };
    var x = target.left - origin.left;
    var y = target.top - origin.top;
    setPosition({
      x: x,
      y: y
    });
  }, [paperRef.current]);

  var tmp = _defineProperty({}, classes.modalOut, isClosing);

  return /*#__PURE__*/React.createElement(Modal, {
    open: open,
    onClose: handleClose,
    disablePortal: disablePortal,
    className: cx(classes.modal),
    hideBackdrop: isClosing
  }, /*#__PURE__*/React.createElement("div", {
    className: cx(classes.paper, tmp, className),
    ref: paperRef
  }, menuOptions && /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(IconButton, {
    onClick: function onClick(ev) {
      return setAnchorEl(ev.currentTarget);
    },
    icon: /*#__PURE__*/React.createElement(ShowMore, null),
    iconSize: 18,
    shade: 400,
    className: classes.optionsIcon
  }), /*#__PURE__*/React.createElement(Popper, {
    className: classes.menu,
    arrow: true,
    placement: "bottom-end",
    open: Boolean(anchorEl),
    handleClose: function handleClose() {
      return setAnchorEl(null);
    },
    anchorEl: anchorEl,
    transition: true,
    color: "white",
    disablePortal: disablePortal
  }, /*#__PURE__*/React.createElement("ul", {
    className: classes.menuList
  }, menuOptions.map(function (option) {
    return /*#__PURE__*/React.createElement(MenuItem, {
      onClick: option.onClick,
      key: "modal-input-option-".concat(option.label)
    }, option.icon, /*#__PURE__*/React.createElement(Typography, {
      color: option.color,
      variant: "f2-16"
    }, option.label));
  })))), (title || icon) && /*#__PURE__*/React.createElement("div", {
    className: classes.title
  }, icon, title && /*#__PURE__*/React.createElement(Typography, {
    variant: "f2-16",
    weight: "bold",
    shade: 700
  }, title)), /*#__PURE__*/React.createElement("form", {
    onSubmit: function onSubmit(ev) {
      ev.preventDefault();

      _onSubmit(inputValue);
    },
    className: classes.inputContainer
  }, /*#__PURE__*/React.createElement(TextField, _extends({
    variant: "standard",
    value: inputValue,
    onChange: function onChange(ev) {
      return setInputValue(ev.target.value);
    },
    placeholder: placeholder,
    autoFocus: true,
    inputProps: maxLength ? {
      maxLength: maxLength
    } : undefined,
    inputClass: classes.textField
  }, inputProps)), buttonText && /*#__PURE__*/React.createElement(Button, {
    type: "submit",
    size: "small",
    disabled: isButtonDisabled(),
    className: classes.submitButton
  }, buttonText)), footer && /*#__PURE__*/React.createElement("div", {
    className: classes.footer
  }, footer)));
};

ModalInput.propTypes = {
  open: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  title: PropTypes.string,
  icon: PropTypes.element,
  placeholder: PropTypes.string,
  defaultValue: PropTypes.string,
  maxLength: PropTypes.number,
  buttonText: PropTypes.string,
  buttonDisabled: PropTypes.bool,
  menuOptions: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    color: PropTypes.string
  })),
  footer: PropTypes.element,
  disablePortal: PropTypes.bool,
  className: PropTypes.string
};
ModalInput.defaultProps = {
  placeholder: "",
  defaultValue: "",
  disablePortal: false,
  className: "",
  buttonDisabled: false
};
export default ModalInput;