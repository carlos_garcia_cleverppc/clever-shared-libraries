import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      spacing = _ref$cleverUI.spacing,
      colors = _ref$cleverUI.colors;
  return {
    rootBudgetCard: _objectSpread(_objectSpread(_objectSpread({}, spacing["p-5"]), {}, {
      boxShadow: "none",
      textAlign: "center"
    }, spacing["pt-5"]), spacing["pb-4"]),
    budgetCardWarning: {
      backgroundColor: colors.warning[500]
    },
    budgetCardSuccess: {
      backgroundColor: colors.green[500]
    },
    budgetConversion: _objectSpread({}, spacing["pb-4"]),
    slider: _objectSpread(_objectSpread(_objectSpread({}, spacing["mt-5"]), spacing["w-100"]), {}, {
      display: "inline-flex",
      alignItems: "center",
      justifyContent: "center"
    }),
    sliderLimitL: _objectSpread(_objectSpread({}, spacing["pr-4"]), {}, {
      whiteSpace: "nowrap"
    }),
    sliderLimitR: _objectSpread(_objectSpread({}, spacing["pl-4"]), {}, {
      whiteSpace: "nowrap"
    }),
    colorSuccess: {
      color: colors.green[500]
    },
    colorWarning: {
      color: colors.warning[500]
    }
  };
};

export default makeStyles(styles);