import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import upperCase from "lodash-es/upperCase";
import round from "lodash-es/round";
import Slider from "../Slider";
import Card from "../Card";
import Grid from "../Grid";
import Typography from "../Typography";
import useMediaQuery from "../styles/useMediaQuery";
import { screenXs } from "../styles/breakpoints";
import getClasses from "./styles";

var BudgetSelector = function BudgetSelector(props) {
  var classes = getClasses();
  var selectedBudget = props.selectedBudget,
      recommendedBudget = props.recommendedBudget,
      minLimit = props.minLimit,
      maxLimit = props.maxLimit,
      step = props.step,
      sliderBubble = props.sliderBubble;
  var description = props.description,
      mainCurrency = props.mainCurrency,
      adwordsCurrencyName = props.adwordsCurrencyName,
      adwordsCurrencyRate = props.adwordsCurrencyRate,
      onChange = props.onChange,
      disabled = props.disabled;
  var isScreenXs = useMediaQuery(screenXs);
  var budgetColor = selectedBudget < recommendedBudget ? "Warning" : "Success";

  var handleOnChange = function handleOnChange(ev, value) {
    onChange(value);
  };

  return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Card, {
    className: classnames(classes.rootBudgetCard, classes["budgetCard".concat(budgetColor)])
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 1,
    justify: "center",
    alignItems: "baseline"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h1",
    color: "white"
  }, selectedBudget)), /*#__PURE__*/React.createElement(Grid, {
    item: true
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body2",
    weight: "bold",
    color: "white"
  }, " ", upperCase(mainCurrency)))), adwordsCurrencyRate !== 0 && upperCase(adwordsCurrencyName) !== upperCase(mainCurrency) && /*#__PURE__*/React.createElement(Typography, {
    variant: "body1",
    weight: "bold",
    color: "white",
    className: classes.budgetConversion
  }, "(", round(adwordsCurrencyRate * selectedBudget, 2), " ", upperCase(adwordsCurrencyName), ")"), description !== "" && /*#__PURE__*/React.createElement(Typography, {
    variant: "body0",
    color: "white"
  }, description)), /*#__PURE__*/React.createElement("div", {
    className: classes.slider
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body0",
    className: classes.sliderLimitL
  }, " ", minLimit, " ", upperCase(mainCurrency), " "), /*#__PURE__*/React.createElement(Slider, {
    value: selectedBudget,
    min: minLimit,
    max: maxLimit,
    step: step,
    onChange: handleOnChange,
    className: selectedBudget >= recommendedBudget ? classes.colorSuccess : classes.colorWarning,
    mobile: isScreenXs,
    valueLabelDisplay: sliderBubble ? "auto" : "off",
    disabled: disabled
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body0",
    className: classes.sliderLimitR
  }, " ", maxLimit, " ", upperCase(mainCurrency), " ")));
};

BudgetSelector.propTypes = {
  selectedBudget: PropTypes.number,
  recommendedBudget: PropTypes.number,
  minLimit: PropTypes.number,
  maxLimit: PropTypes.number,
  step: PropTypes.number,
  sliderBubble: PropTypes.bool,
  description: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Object)]),
  mainCurrency: PropTypes.string,
  adwordsCurrencyName: PropTypes.string,
  adwordsCurrencyRate: PropTypes.number,
  onChange: PropTypes.func,
  disabled: PropTypes.bool
};
BudgetSelector.defaultProps = {
  selectedBudget: 5,
  recommendedBudget: 0,
  minLimit: 5,
  maxLimit: 200,
  step: 5,
  sliderBubble: true,
  description: "",
  mainCurrency: "USD",
  adwordsCurrencyName: "",
  adwordsCurrencyRate: 0,
  onChange: function onChange() {},
  disabled: false
};
export default BudgetSelector;