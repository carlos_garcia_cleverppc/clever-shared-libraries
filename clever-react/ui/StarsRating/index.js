import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import StarRatingComponent from "react-star-rating-component";
import makeStyles from "../styles/makeStyles";
var getClasses = makeStyles(function () {
  return {
    stars: function stars(_ref) {
      var size = _ref.size;
      return {
        fontSize: "".concat(size, "px")
      };
    }
  };
});

var StarsRating = function StarsRating(props) {
  var className = props.className,
      defaultStars = props.defaultStars,
      selected = props.selected,
      setSelected = props.setSelected,
      size = props.size,
      disabled = props.disabled;
  var classes = getClasses({
    size: size
  });

  var _useState = useState(selected || defaultStars),
      _useState2 = _slicedToArray(_useState, 2),
      stars = _useState2[0],
      setStars = _useState2[1];

  var selectStar = function selectStar(value) {
    setStars(value);
    setSelected(value);
  };

  var selectHoldStars = function selectHoldStars(value) {
    setStars(value);
  };

  var defaultHoldStars = function defaultHoldStars() {
    var holdStars = selected || defaultStars;
    setStars(holdStars);
  };

  return /*#__PURE__*/React.createElement(StarRatingComponent, {
    name: "rating",
    value: stars,
    onStarClick: function onStarClick(ev) {
      return selectStar(ev);
    },
    onStarHover: function onStarHover(ev) {
      return selectHoldStars(ev);
    },
    onStarHoverOut: function onStarHoverOut(ev) {
      return defaultHoldStars(ev);
    },
    className: classnames(classes.stars, className),
    editing: !disabled
  });
};

StarsRating.propTypes = {
  className: PropTypes.string,
  selected: PropTypes.oneOf([1, 2, 3, 4, 5]),
  defaultStars: PropTypes.oneOf([1, 2, 3, 4, 5]),
  size: PropTypes.number,
  setSelected: PropTypes.func,
  disabled: PropTypes.bool
};
StarsRating.defaultProps = {
  className: "",
  selected: undefined,
  defaultStars: 5,
  size: 50,
  setSelected: function setSelected() {},
  disabled: false
};
export default StarsRating;