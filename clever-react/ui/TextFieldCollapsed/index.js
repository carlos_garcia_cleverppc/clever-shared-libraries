import _defineProperty from "@babel/runtime/helpers/defineProperty";
import _slicedToArray from "@babel/runtime/helpers/slicedToArray";

/* eslint-disable no-unused-expressions */
import React, { useRef, useState, useEffect } from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import IconButton from "../IconButton";
import Link from "../Icon/Link";
import Enter from "../Icon/Enter";
import ClickAwayListener from "../ClickAwayListener";
import useTransitionEnd from "./useTransitionEnd";
import getClasses from "./styles";

var TextFieldCollapsed = function TextFieldCollapsed(_ref) {
  var onSubmit = _ref.onSubmit,
      className = _ref.className,
      type = _ref.type,
      icon = _ref.icon;

  var _useState = useState(false),
      _useState2 = _slicedToArray(_useState, 2),
      open = _useState2[0],
      setOpen = _useState2[1];

  var _useState3 = useState(0),
      _useState4 = _slicedToArray(_useState3, 2),
      submitWidth = _useState4[0],
      setSubmitWidth = _useState4[1];

  var _useState5 = useState(""),
      _useState6 = _slicedToArray(_useState5, 2),
      value = _useState6[0],
      setValue = _useState6[1];

  var inputRef = useRef();
  var submitRef = useRef();
  var classes = getClasses({
    submitWidth: submitWidth
  });
  useTransitionEnd(function () {
    !open && setValue("");
  }, inputRef);
  useEffect(function () {
    open && inputRef.current.focus();
  }, [open]);
  useEffect(function () {
    var el = submitRef.current;
    var width = el ? el.getBoundingClientRect().width : 0;
    setSubmitWidth(width);
  }, [submitRef]);

  var toggleOpen = function toggleOpen() {
    return setOpen(function (state) {
      return !state;
    });
  };

  var handleChange = function handleChange(ev) {
    return setValue(ev.target.value);
  };

  var submit = function submit(ev) {
    ev.preventDefault();
    onSubmit(value);
    setOpen(false);
  };

  var handleClickAway = function handleClickAway() {
    return open && setOpen(false);
  };

  return /*#__PURE__*/React.createElement(ClickAwayListener, {
    onClickAway: handleClickAway
  }, /*#__PURE__*/React.createElement("div", {
    className: cx(classes.root, className, _defineProperty({}, classes.open, open))
  }, /*#__PURE__*/React.createElement(IconButton, {
    className: classes.handle,
    outlined: false,
    onClick: toggleOpen,
    color: "primary",
    active: open,
    icon: icon
  }), /*#__PURE__*/React.createElement("form", {
    className: classes.form,
    onSubmit: submit
  }, /*#__PURE__*/React.createElement("input", {
    ref: inputRef,
    className: classes.fieldInput,
    value: value,
    onChange: handleChange,
    type: type
  }), /*#__PURE__*/React.createElement(IconButton, {
    ref: submitRef,
    type: "submit",
    outlined: false,
    color: "primary",
    onClick: submit,
    icon: /*#__PURE__*/React.createElement(Enter, null),
    className: classes.submit
  }))));
};

export default TextFieldCollapsed;
TextFieldCollapsed.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  type: PropTypes.string,
  icon: PropTypes.element
};
TextFieldCollapsed.defaultProps = {
  type: "url",
  icon: /*#__PURE__*/React.createElement(Link, null)
};