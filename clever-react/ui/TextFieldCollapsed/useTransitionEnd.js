/* eslint-disable consistent-return */
import { useEffect } from "react";

var useTransitionEnd = function useTransitionEnd(callback, ref) {
  useEffect(function () {
    var el = ref.current;
    if (!el) return;
    el.addEventListener("transitionend", callback);
    return function () {
      el.removeEventListener("transitionend", callback);
    };
  }, [callback, ref]);
};

export default useTransitionEnd;