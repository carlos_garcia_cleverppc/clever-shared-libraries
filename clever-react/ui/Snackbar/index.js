import _extends from "@babel/runtime/helpers/extends";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["className", "onClose", "text", "textAction", "textColor", "onAction", "placement", "color", "anchorEl", "showActionButton", "isLoading"];

/* eslint-disable no-console */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import SnackbarMaterial from "@material-ui/core/Snackbar";
import Fade from "@material-ui/core/Fade";
import SnackbarContent from "@material-ui/core/SnackbarContent";
import { getColorsLabels } from "../styles/colors";
import Button from "../Link";
import Times from "../Icon/Times";
import Loading from "../Loading";
import getClasses from "./styles";
import TooltipBar from "./TooltipBar";
var colorsList = getColorsLabels();

var Snackbar = function Snackbar(props) {
  var className = props.className,
      onClose = props.onClose,
      text = props.text,
      textAction = props.textAction,
      textColor = props.textColor,
      onAction = props.onAction,
      placement = props.placement,
      color = props.color,
      anchorEl = props.anchorEl,
      showActionButton = props.showActionButton,
      isLoading = props.isLoading,
      otherProps = _objectWithoutProperties(props, _excluded);

  var classes = getClasses({
    color: color
  });

  var handleClose = function handleClose(event, reason) {
    if (reason === "clickaway") {
      return;
    }

    onClose();
  };

  var getAction = function getAction() {
    if (showActionButton && !isLoading) {
      return /*#__PURE__*/React.createElement(Button, {
        className: classes.action,
        color: color,
        onClick: onAction || handleClose
      }, textAction || /*#__PURE__*/React.createElement(Times, {
        size: 18,
        color: color
      }));
    }

    if (isLoading) {
      return /*#__PURE__*/React.createElement(Loading, {
        size: 16,
        color: color,
        className: classes.loading
      });
    }

    return /*#__PURE__*/React.createElement(React.Fragment, null);
  };

  if (anchorEl) {
    return /*#__PURE__*/React.createElement(TooltipBar, _extends({
      anchorEl: anchorEl,
      className: classnames(className, classes.content),
      placement: placement,
      onClose: handleClose,
      TransitionComponent: Fade,
      message: /*#__PURE__*/React.createElement("div", {
        className: classes.text
      }, text),
      action: getAction()
    }, otherProps));
  }

  return /*#__PURE__*/React.createElement(SnackbarMaterial, _extends({
    className: classnames(className, classes.root),
    onClose: handleClose,
    TransitionComponent: Fade
  }, otherProps), /*#__PURE__*/React.createElement(SnackbarContent, {
    className: classes.content,
    message: /*#__PURE__*/React.createElement("div", {
      className: classes.text
    }, text),
    action: getAction()
  }));
};

Snackbar.propTypes = {
  text: PropTypes.string.isRequired,
  open: PropTypes.bool,

  /**
  Executes when the visibility time has expired.
  */
  onClose: PropTypes.func,

  /**
  Executes when interacting with the action text.
  */
  onAction: PropTypes.func,
  showActionButton: PropTypes.bool,
  color: PropTypes.oneOf(colorsList),
  anchorOrigin: PropTypes.exact({
    vertical: PropTypes.oneOf(["top", "bottom"]),
    horizontal: PropTypes.oneOf(["left", "right", "center"])
  }),
  autoHideDuration: PropTypes.number,

  /**
  Shows Loading component instead of action button.
  */
  isLoading: PropTypes.bool,
  textAction: PropTypes.string,

  /**
  Allows the snackbar relative to a DOM element.
  */
  anchorEl: PropTypes.instanceOf(Element),

  /**
  Position relative to the anchorEl.
  */
  placement: PropTypes.oneOf(["top-start", "top", "top-end", "left-start", "left", "left-end", "right-start", "right", "right-end", "bottom-start", "bottom", "bottom-end"])
};
Snackbar.defaultProps = {
  open: false,
  color: "info",
  onClose: function onClose() {},
  showActionButton: true,
  isLoading: false,
  anchorOrigin: {
    vertical: "bottom",
    horizontal: "left"
  },
  autoHideDuration: 3000,
  placement: "bottom-start"
};
export default Snackbar;