import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable object-curly-newline */
import makeStyles from "../styles/makeStyles";
import getContrastText from "../styles/contrastText";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing,
      typography = _ref$cleverUI.typography,
      shadows = _ref$cleverUI.shadows,
      breakpoints = _ref.breakpoints;
  var sizing = typography.sizing;
  return {
    root: _defineProperty({}, breakpoints.up("sm"), {
      maxWidth: 360,
      width: "100%"
    }),
    content: function content(_ref2) {
      var _colors$color, _colors$color2, _colors$color3;

      var color = _ref2.color;
      return {
        flexWrap: "nowrap",
        border: "1px solid ".concat(((_colors$color = colors[color]) === null || _colors$color === void 0 ? void 0 : _colors$color[500]) || color),
        color: ((_colors$color2 = colors[color]) === null || _colors$color2 === void 0 ? void 0 : _colors$color2[500]) || color,
        backgroundColor: ((_colors$color3 = colors[color]) === null || _colors$color3 === void 0 ? void 0 : _colors$color3[100]) || getContrastText(color),
        borderRadius: "8px",
        fontFamily: typography.font2,
        lineHeight: typography.lineHeight,
        fontWeight: "bold",
        flexGrow: 1,
        boxShadow: shadows.normal,
        "& > :last-child": {
          marginRight: 0 // remove negative margin of Material ui

        }
      };
    },
    text: {
      fontSize: sizing.body0,
      display: "-webkit-box",
      "-webkit-box-orient": "vertical",
      "-webkit-line-clamp": 2,
      overflow: "hidden"
    },
    action: {
      whiteSpace: "nowrap",
      textTransform: "uppercase",
      fontSize: sizing.body0,
      "& svg": {
        display: "block"
      }
    },
    loading: _objectSpread({}, spacing["m-0"])
  };
};

export default makeStyles(styles);