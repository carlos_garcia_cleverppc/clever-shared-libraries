import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable react/require-default-props */
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Fade from "@material-ui/core/Fade";
import Popper from "@material-ui/core/Popper";
import makeStyles from "../styles/makeStyles";
var getClasses = makeStyles(function (_ref) {
  var spacing = _ref.cleverUI.spacing;
  return {
    root: {
      width: "100%",
      maxWidth: "344px"
    },
    grid: {
      display: "grid",
      padding: "6px 16px",
      columnGap: "16px",
      alignItems: "center",
      gridTemplateColumns: "1fr auto",
      boxXizing: "border-box"
    },
    content: spacing["py-2"],
    action: {
      display: "flex",
      placeItems: "center"
    }
  };
});

var TooltipBar = function TooltipBar(props) {
  var open = props.open,
      className = props.className,
      onClose = props.onClose,
      autoHideDuration = props.autoHideDuration,
      message = props.message,
      action = props.action,
      placement = props.placement,
      anchorEl = props.anchorEl,
      TransitionComponent = props.TransitionComponent;
  var classes = getClasses();
  useEffect(function () {
    var mounted = true;
    var timer;

    if (open) {
      timer = setTimeout(function () {
        if (mounted) {
          onClose();
        }
      }, autoHideDuration);
    } else {
      clearTimeout(timer);
    }

    return function () {
      clearTimeout(timer);
      mounted = false;
    };
  }, [open, autoHideDuration, onClose]);
  return /*#__PURE__*/React.createElement(Popper, {
    open: open,
    anchorEl: anchorEl,
    className: classes.root,
    placement: placement,
    modifiers: {
      offset: {
        offset: "0,16"
      }
    },
    transition: true
  }, function (_ref2) {
    var TransitionProps = _ref2.TransitionProps;
    return /*#__PURE__*/React.createElement(TransitionComponent, _extends({}, TransitionProps, {
      timeout: 300
    }), /*#__PURE__*/React.createElement("div", {
      className: classnames(className, classes.grid)
    }, /*#__PURE__*/React.createElement("div", {
      className: classes.content
    }, message), /*#__PURE__*/React.createElement("div", {
      className: classes.content
    }, action)));
  });
};

TooltipBar.propTypes = {
  action: PropTypes.func.isRequired,
  message: PropTypes.oneOf([PropTypes.string, PropTypes.node]).isRequired,
  anchorEl: PropTypes.instanceOf(Element).isRequired,
  placement: PropTypes.oneOf(["top-start", "top", "top-end", "left-start", "left", "left-end", "right-start", "right", "right-end", "bottom-start", "botton", "bottom-end"]),
  onClose: PropTypes.func,
  TransitionComponent: PropTypes.func,
  autoHideDuration: PropTypes.number,
  open: PropTypes.bool
};
TooltipBar.defaultProps = {
  onClose: function onClose() {},
  autoHideDuration: 3000,
  placement: "bottom-start",
  TransitionComponent: Fade,
  open: false
};
export default TooltipBar;