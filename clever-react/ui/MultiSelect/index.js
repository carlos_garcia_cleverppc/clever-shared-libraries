/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import List from "../List";
import ListItem from "../List/Item";
import Divider from "../Divider";
import Grid from "../Grid";
import Checkbox from "../Checkbox";
import Typography from "../Typography";
import getClasses from "./styles";

var MultiSelect = function MultiSelect(_ref) {
  var className = _ref.className,
      options = _ref.options,
      _onChange = _ref.onChange,
      checked = _ref.checked,
      disabled = _ref.disabled;
  var classes = getClasses();
  return /*#__PURE__*/React.createElement(List, {
    component: "nav",
    "aria-label": "main mailbox folders",
    className: classes.root
  }, options.map(function (item, index) {
    return /*#__PURE__*/React.createElement(ListItem, {
      key: item.id,
      className: classnames(classes.list, className)
    }, /*#__PURE__*/React.createElement(Grid, {
      container: true
    }, /*#__PURE__*/React.createElement(Grid, {
      item: true,
      className: index === 0 ? classes.firstCheckBox : classes.checkBox
    }, /*#__PURE__*/React.createElement(Checkbox, {
      checked: checked[item.id],
      onChange: function onChange() {
        return _onChange(item.id);
      },
      color: "primary",
      disabled: disabled
    }, /*#__PURE__*/React.createElement(Typography, {
      shade: 900
    }, item.label))), /*#__PURE__*/React.createElement(Grid, {
      item: true,
      xs: 12,
      className: classes.divider
    }, /*#__PURE__*/React.createElement(Divider, null))));
  }));
};

MultiSelect.propTypes = {
  className: PropTypes.string,
  options: PropTypes.instanceOf(Object).isRequired,
  checked: PropTypes.instanceOf(Object).isRequired,
  onChange: PropTypes.func,
  disabled: PropTypes.bool
};
MultiSelect.defaultProps = {
  className: "",
  onChange: function onChange() {},
  disabled: false
};
export default MultiSelect;