import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    root: {
      color: colors.primary[500],
      maxHeight: "400px",
      overflow: "auto",
      border: "solid 1px ".concat(colors.neutral[300]),
      borderRadius: "5px",
      "&::-webkit-scrollbar": {
        width: "12px",
        backgroundColor: "#fff"
      },
      "&::-webkit-scrollbar-track": {
        backgroundColor: "#fff"
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: colors.neutral[300],
        borderRadius: "16px",
        border: "4px solid #fff"
      }
    },
    colorBlue: {
      backgroundColor: colors.primary[100]
    },
    iconTimes: {
      "& path": {
        fill: colors.primary[100]
      }
    },
    iconPlus: {
      "& path": {
        fill: colors.primary[500]
      }
    },
    buttonPlus: {
      backgroundColor: colors.primary[300]
    },
    buttonTimes: {
      backgroundColor: colors.primary[500]
    },
    checkBox: _objectSpread({}, spacing["pl-4"]),
    firstCheckBox: _objectSpread(_objectSpread({}, spacing["pt-3"]), spacing["pl-4"]),
    divider: _objectSpread({}, spacing["px-3"])
  };
};

export default makeStyles(styles);