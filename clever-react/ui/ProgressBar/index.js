import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import React, { useEffect } from "react";
import PropTypes from "prop-types";
import LinearProgress from "@material-ui/core/LinearProgress";
import getClasses from "./styles";

var ProgressBar = function ProgressBar(props) {
  var classes = getClasses(props);
  var value = props.value,
      variant = props.variant,
      color = props.color,
      className = props.className;

  var _React$useState = React.useState(0),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      progress = _React$useState2[0],
      setProgress = _React$useState2[1];

  useEffect(function () {
    var timer = setInterval(function () {
      setProgress(function (oldProgress) {
        if (oldProgress === value) {
          return value;
        }

        return Math.min(oldProgress + 1, value);
      });
    }, 10);
    return function () {
      clearInterval(timer);
    };
  }, [value]);
  return /*#__PURE__*/React.createElement(LinearProgress, {
    className: className,
    classes: {
      root: classes.root,
      bar: classes.bar,
      barColorPrimary: classes.barColorPrimary
    },
    color: color,
    variant: variant,
    value: progress
  });
};

ProgressBar.propTypes = {
  value: PropTypes.number,
  variant: PropTypes.string,
  color: PropTypes.oneOf(["primary", "secondary"]),
  className: PropTypes.string
};
ProgressBar.defaultProps = {
  value: 25,
  variant: "determinate",
  color: "primary",
  className: ""
};
export default ProgressBar;