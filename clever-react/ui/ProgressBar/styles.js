import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var colors = _ref.cleverUI.colors;
  return {
    root: {
      height: "10px",
      borderRadius: "5px",
      backgroundColor: colors.neutral[100]
    },
    bar: {
      borderRadius: "5px"
    },
    barColorPrimary: function barColorPrimary(_ref2) {
      var color = _ref2.color;
      return {
        backgroundColor: color
      };
    },
    inputEnter: {
      right: 100
    },
    inputEnterActive: {
      right: 0,
      transition: "500ms ease-in all"
    }
  };
};

export default makeStyles(styles);