import makeStyles from "../styles/makeStyles";
var getClasses = makeStyles(function () {
  return {
    root: {
      width: "min-content",
      position: "relative"
    },
    actions: {
      position: "absolute",
      left: 0,
      bottom: 0,
      transform: "translateY(50%)",
      display: "flex",
      gap: ".5rem",
      paddingLeft: ".75rem",
      alignItems: "center"
    }
  };
});
export default getClasses;