import _extends from "@babel/runtime/helpers/extends";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["onSubmitUrl", "className"];
import React, { useRef } from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import IconButton from "../IconButton";
import Upload from "../Icon/Upload";
import ImageField from "../ImageField";
import TextFieldCollapsed from "../TextFieldCollapsed";
import getClasses from "./styles";

var ImageFieldWithUrl = function ImageFieldWithUrl(_ref) {
  var onSubmitUrl = _ref.onSubmitUrl,
      className = _ref.className,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = getClasses();
  var inputRef = useRef();

  var openImageField = function openImageField() {
    return inputRef.current.click();
  };

  return /*#__PURE__*/React.createElement("div", {
    className: cx(classes.root, className)
  }, /*#__PURE__*/React.createElement(ImageField, _extends({
    ref: inputRef
  }, props)), /*#__PURE__*/React.createElement("div", {
    className: classes.actions
  }, /*#__PURE__*/React.createElement(IconButton, {
    outlined: false,
    color: "primary",
    onClick: openImageField,
    icon: /*#__PURE__*/React.createElement(Upload, null)
  }), /*#__PURE__*/React.createElement(TextFieldCollapsed, {
    onSubmit: onSubmitUrl
  })));
};

export default ImageFieldWithUrl;
ImageFieldWithUrl.propTypes = {
  onSubmitUrl: PropTypes.func.isRequired,
  onChange: PropTypes.func,
  value: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  mbLimit: PropTypes.number,
  onError: PropTypes.func,

  /**
   * You can insert one of de values
   * or an Array of some of de values showed before Array[]
   * @default *
   */
  accept: PropTypes.oneOfType([PropTypes.oneOf(["*", "jpg", "jpeg", "png"]), PropTypes.arrayOf(["*", "jpg", "jpeg", "png"])]),
  label: PropTypes.string
};