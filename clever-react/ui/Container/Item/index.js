import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import makeStyles from "../../styles/makeStyles";
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var columns = _ref.columns,
          rows = _ref.rows;
      return {
        display: "block",
        gridColumn: "span ".concat(columns),
        gridRow: "span ".concat(rows)
      };
    },
    children: {
      display: "block"
    }
  };
});

var Item = function Item(_ref2) {
  var columns = _ref2.columns,
      rows = _ref2.rows,
      children = _ref2.children,
      layoutConfig = _ref2.layoutConfig,
      className = _ref2.className;
  var classes = getClasses({
    columns: columns,
    rows: rows
  });
  if (!layoutConfig || Object.keys(layoutConfig).length === 0) return new Error("\"Item\": Empty layout config");
  return /*#__PURE__*/React.createElement("div", {
    className: classnames(classes.root, className)
  }, children);
};

Item.propTypes = {
  columns: PropTypes.oneOf([1, 2, 3, 4]),
  rows: PropTypes.number,
  className: PropTypes.string
};
Item.defaultProps = {
  columns: 1,
  rows: 1,
  className: ""
};
export default Item;