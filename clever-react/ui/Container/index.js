import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable react/destructuring-assignment */

/* eslint-disable react/no-unused-prop-types */

/* eslint-disable object-curly-newline */
import React from "react";
import PropTypes from "prop-types";
import isArray from "lodash-es/isArray";
import map from "lodash-es/map";
import classnames from "classnames";
import makeStyles from "../styles/makeStyles";
var GAP_SIZE = 24;
var ROW_SIZE = 160;
var COLUMN_SIZE = 160;

function getLayoutValue(layout, value) {
  return value || layout;
}

function isAutoProp(layout, value) {
  return getLayoutValue(layout, value).toLowerCase() === "auto";
}

var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var children = _ref.children,
          columns = _ref.columns,
          rows = _ref.rows,
          maxWidth = _ref.maxWidth,
          columnSize = _ref.columnSize,
          rowGap = _ref.rowGap,
          columnGap = _ref.columnGap,
          rowSize = _ref.rowSize,
          layout = _ref.layout,
          alignRows = _ref.alignRows,
          alignColumns = _ref.alignColumns;
      var nodes = isArray(children) ? children : [children];
      var gridTemplateColumns = isAutoProp(layout, columnSize) ? "repeat(".concat(!columns ? nodes.length : columns, ", auto)") : "repeat(".concat(!columns ? nodes.length : columns, ", ").concat(COLUMN_SIZE * (!columns ? nodes.length : columns) / (!columns ? nodes.length : columns), "px)");
      var width = isAutoProp(layout, maxWidth) ? "100%" : "".concat(COLUMN_SIZE * (!columns ? nodes.length : columns) + columnGap * ((!columns ? nodes.length : columns) - 1), "px");
      var gridTemplateRows = isAutoProp(layout, rowSize) ? "repeat(".concat(rows, ", auto)") : "repeat(".concat(rows, ", ").concat(ROW_SIZE, "px)");
      var gridAutoRows = isAutoProp(layout, rowSize) ? "auto" : "".concat(ROW_SIZE, "px");
      return {
        display: "grid",
        maxWidth: width,
        width: width,
        gridTemplateColumns: gridTemplateColumns,
        gridTemplateRows: gridTemplateRows,
        gridAutoRows: gridAutoRows,
        gridAutoFlow: "row",
        columnGap: "".concat(columnGap, "px"),
        rowGap: "".concat(rowGap, "px"),
        justifyItems: alignColumns,
        alignItems: alignRows
      };
    }
  };
});

var Container = function Container(props) {
  var classes = getClasses(props);

  var getChildren = function getChildren() {
    var children = props.children;
    var nodes = isArray(children) ? children : [children];
    return map(nodes, function (n) {
      if (n.type) return _objectSpread(_objectSpread({}, n), {}, {
        props: _objectSpread(_objectSpread({}, n.props), {}, {
          layoutConfig: props
        })
      });
      return "";
    });
  };

  return /*#__PURE__*/React.createElement("div", {
    className: classnames(classes.root, props.className)
  }, getChildren());
};

Container.propTypes = {
  columns: PropTypes.oneOf([1, 2, 3, 4, 5, 6, ""]),
  rows: PropTypes.number.isRequired,
  rowGap: PropTypes.number,
  columnGap: PropTypes.number,
  alignColumns: PropTypes.oneOf(["start", "end", "center", "stretch"]),
  alignRows: PropTypes.oneOf(["start", "end", "center", "stretch"]),
  maxWidth: PropTypes.oneOf(["auto", "blocks", undefined]),
  layout: PropTypes.oneOf(["auto", "blocks"]),
  columnSize: PropTypes.oneOf(["auto", "blocks", undefined]),
  rowSize: PropTypes.oneOf(["auto", "blocks", undefined]),
  className: PropTypes.string
};
Container.defaultProps = {
  maxWidth: undefined,
  rowSize: undefined,
  rowGap: GAP_SIZE,
  columnGap: GAP_SIZE,
  columnSize: undefined,
  columns: "",
  layout: "blocks",
  alignColumns: "stretch",
  alignRows: "stretch",
  className: ""
};
export default Container;