function createMaterialTheme(theme) {
  return {
    typography: {
      fontFamily: theme.typography.font2,
      h1: theme.typography.sizing.h1,
      h2: theme.typography.sizing.h2,
      h3: theme.typography.sizing.h3,
      h4: theme.typography.sizing.h4,
      h5: theme.typography.sizing.h5,
      h6: theme.typography.sizing.h6,
      subtitle1: theme.typography.sizing.body3,
      subtitle2: theme.typography.sizing.body2,
      body1: theme.typography.sizing.body1,
      body2: theme.typography.sizing.body0,
      button: {
        fontFamily: theme.typography.font2,
        fontWeight: 900,
        fontSize: theme.typography.sizing.body1
      }
    },
    palette: {
      primary: theme.colors.primary,
      secondary: theme.colors.secondary,
      grey: theme.colors.neutral,
      text: {
        primary: theme.colors.neutral[900],
        secondary: theme.colors.neutral[500]
      }
    },
    shape: {
      borderRadius: 8
    },
    overrides: {
      MuiInputBase: {
        root: {
          fontFamily: theme.typography.font1
        }
      },
      MuiOutlinedInput: {
        notchedOutline: {
          fontSize: theme.typography.sizing.body1,
          fontWeight: 700,
          fontFamily: theme.typography.font2
        }
      },
      MuiInputLabel: {
        root: {
          fontSize: theme.typography.sizing.body1,
          fontWeight: 700,
          fontFamily: theme.typography.font2,
          color: theme.colors.neutral[600]
        }
      },
      MuiButton: {
        root: {
          transition: "background 0.3s, transform 0.1s",
          "&:not($disabled):active": {
            transform: "scale(0.95)"
          }
        }
      },
      MuiIconButton: {
        root: {
          transition: "background 0.3s, transform 0.1s",
          "&:not($disabled):active": {
            transform: "scale(0.95)"
          }
        }
      }
    },
    props: {
      MuiInput: {
        disableUnderline: true
      },
      MuiButton: {
        disableElevation: true
      },
      MuiButtonBase: {
        disableRipple: true
      }
    }
  };
}

export default createMaterialTheme;