import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import React from "react";
import PropTypes from "prop-types";
import { Provider as ReduxProvider } from "react-redux";
import { createStore } from "redux-dynamic-modules-core";
import { getThunkExtension } from "redux-dynamic-modules-thunk";

var Provider = function Provider(_ref) {
  var modules = _ref.modules,
      children = _ref.children;
  var store = createStore(_objectSpread({
    enhancements: [],
    extensions: [getThunkExtension()]
  }, modules));
  return /*#__PURE__*/React.createElement(ReduxProvider, {
    store: store
  }, children);
};

Provider.propTypes = {
  modules: PropTypes.arrayOf(PropTypes.instanceOf(Object))
};
Provider.defaultProps = {
  modules: []
};
export default Provider;