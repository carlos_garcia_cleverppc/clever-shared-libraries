import _slicedToArray from "@babel/runtime/helpers/slicedToArray";

/* eslint-disable react-hooks/exhaustive-deps */

/* eslint-disable react/no-multi-comp */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import mixpanel from "mixpanel-browser";
import produce from "immer";
import ModuleLoader from "../../ModuleLoader";
import { launchAction } from "../../../utils/redux";
import { init } from "../../../utils/tracking/mixpanel";
var INIT_MIXPANEL = "INIT_MIXPANEL";
var defaultState = {
  isInit: false,
  isSandbox: true,
  options: {},
  id: false
};

var Mixpanel = function Mixpanel(_ref) {
  var mixpanelId = _ref.mixpanelId,
      sandbox = _ref.sandbox,
      dispatch = _ref.dispatch,
      options = _ref.options,
      children = _ref.children;

  var _useState = useState(true),
      _useState2 = _slicedToArray(_useState, 2),
      isLoading = _useState2[0],
      setIsLoading = _useState2[1];

  useEffect(function () {
    if (!sandbox || !mixpanelId || mixpanelId.trim().length > 0) {
      mixpanel.init(mixpanelId);
    }

    init({
      options: options,
      id: mixpanelId,
      isSandbox: sandbox
    });
    launchAction(dispatch, INIT_MIXPANEL, {
      options: options,
      id: mixpanelId,
      isSandbox: sandbox
    });
    setIsLoading(false);
  }, [mixpanelId, sandbox, options]);
  if (isLoading) return /*#__PURE__*/React.createElement(React.Fragment, null);
  return /*#__PURE__*/React.createElement(React.Fragment, null, children);
};

Mixpanel.propTypes = {
  mixpanelId: PropTypes.string,
  sandbox: PropTypes.bool,
  options: PropTypes.instanceOf(Object)
};
Mixpanel.defaultProps = {
  mixpanelId: "",
  sandbox: false,
  options: {}
};

var Reducer = function Reducer(state, action) {
  return produce(state || defaultState, function (possibleState) {
    switch (action.type) {
      case INIT_MIXPANEL:
        {
          possibleState.options = action.payload.options;
          possibleState.id = action.payload.id;
          possibleState.isSandbox = action.payload.isSandbox;
          possibleState.isInit = true;
          break;
        }

      default:
        {// Do nothing
        }
    }
  });
};

var moduleConfig = {
  id: "MixpanelExtension",
  reducerMap: {
    mixpanelExtension: Reducer
  },
  initialActions: [],
  finalActions: []
};
export default (function (props) {
  return /*#__PURE__*/React.createElement(ModuleLoader, {
    Component: connect()(Mixpanel),
    config: moduleConfig,
    componentProps: props
  });
});