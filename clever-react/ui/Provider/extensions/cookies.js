import React from "react";
import { CookiesProvider } from "react-cookie";

var CookieProvider = function CookieProvider(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/React.createElement(CookiesProvider, null, children);
};

export default CookieProvider;