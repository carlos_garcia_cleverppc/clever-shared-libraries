/* eslint-disable prefer-rest-params */

/* eslint-disable no-inner-declarations */
import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import gtmParts from "react-google-tag-manager";

var GoogleTagManager = function GoogleTagManager(props) {
  var dataLayerName = props.dataLayerName,
      scriptId = props.scriptId,
      id = props.id,
      children = props.children;
  var additionalEvents = props.additionalEvents,
      previewVariables = props.previewVariables,
      sandbox = props.sandbox;
  useEffect(function () {
    if (!!id && id.trim().length > 0) {
      if (!window[dataLayerName]) {
        var gtmScriptNode = document.getElementById(scriptId); // eslint-disable-next-line no-eval

        eval(gtmScriptNode.textContent);
      }
    }
  }, [dataLayerName, id, scriptId, sandbox]);

  if (!!id && id.trim().length > 0) {
    var gtm = gtmParts({
      id: id,
      dataLayerName: dataLayerName,
      additionalEvents: additionalEvents,
      previewVariables: previewVariables.trim().length === 0 ? false : previewVariables
    });
    return /*#__PURE__*/React.createElement(React.Fragment, null, gtm.noScriptAsReact(), /*#__PURE__*/React.createElement("div", {
      id: scriptId || ""
    }, gtm.scriptAsReact()), children);
  }

  return /*#__PURE__*/React.createElement(React.Fragment, null, children);
};

GoogleTagManager.propTypes = {
  id: PropTypes.string.isRequired,
  dataLayerName: PropTypes.string,
  additionalEvents: PropTypes.instanceOf(Object),
  previewVariables: PropTypes.string,
  scriptId: PropTypes.string,
  sandbox: PropTypes.bool
};
GoogleTagManager.defaultProps = {
  dataLayerName: "dataLayer",
  additionalEvents: {},
  previewVariables: "",
  scriptId: "react-google-tag-manager-gtm",
  sandbox: false
};
export default GoogleTagManager;