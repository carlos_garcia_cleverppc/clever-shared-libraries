/* eslint-disable prefer-rest-params */

/* eslint-disable no-inner-declarations */
import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";

var GoogleTagManager = function GoogleTagManager(props) {
  var dataLayerName = props.dataLayerName,
      id = props.id,
      sandbox = props.sandbox,
      children = props.children;
  useEffect(function () {
    if (!!id && id.trim().length > 0 && !window.gtag && window[dataLayerName]) {
      var script = document.createElement("script");
      script.src = "https://www.googletagmanager.com/gtag/js?id=".concat(id);
      script.async = true;
      script.defer = false;
      document.body.appendChild(script);

      if (!sandbox) {
        var gtag = function gtag() {
          window[dataLayerName].push(arguments);
        };

        gtag("js", new Date());
        gtag("config", id);
        window.gtag = gtag;
        window.gtagConfig = {
          id: id,
          dataLayerName: dataLayerName,
          sandbox: sandbox
        };
      }
    }
  }, [dataLayerName, id, sandbox]);
  return /*#__PURE__*/React.createElement(React.Fragment, null, children);
};

GoogleTagManager.propTypes = {
  id: PropTypes.string.isRequired,
  dataLayerName: PropTypes.string,
  sandbox: PropTypes.bool
};
GoogleTagManager.defaultProps = {
  dataLayerName: "dataLayer",
  sandbox: false
};
export default GoogleTagManager;