import React from "react";
import PropTypes from "prop-types";
import { ThemeProvider, StylesProvider } from "@material-ui/styles";
import { createTheme } from "@material-ui/core/styles";
import themeConfig from "./themeConfig";
import GlobalStyles from "./GlobalStyles";

var Provider = function Provider(_ref) {
  var children = _ref.children,
      theme = _ref.theme;
  var themeBuilder = createTheme(themeConfig(theme));
  return /*#__PURE__*/React.createElement(StylesProvider, {
    injectFirst: true
  }, /*#__PURE__*/React.createElement(ThemeProvider, {
    theme: themeBuilder
  }, /*#__PURE__*/React.createElement(GlobalStyles, null), children));
};

Provider.propTypes = {
  theme: PropTypes.instanceOf(Object)
};
Provider.defaultProps = {
  theme: {}
};
export default Provider;