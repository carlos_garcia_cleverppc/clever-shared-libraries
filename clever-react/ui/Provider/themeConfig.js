import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable no-bitwise */
import forEach from "lodash-es/forEach";
import merge from "lodash-es/merge";
import getContrastText from "../styles/contrastText";
import generateSpacing from "../../utils/spacing";
import { getColors } from "../styles/colors";
import createMaterialTheme from "./createMaterialTheme";

function LightenDarkenColor(col, amt) {
  var usePound = col[0] === "#";
  var auxCol = usePound ? col.slice(1) : col;
  var num = parseInt(auxCol, 16);
  var r = (num >> 16) + amt;
  var b = (num >> 8 & 0x00ff) + amt;
  var g = (num & 0x0000ff) + amt;
  if (r > 255) r = 255;else if (r < 0) r = 0;
  if (b > 255) b = 255;else if (b < 0) b = 0;
  if (g > 255) g = 255;else if (g < 0) g = 0;
  return (usePound ? "#" : "") + (g | b << 8 | r << 16).toString(16);
}

export var lighter = function lighter(col, amt) {
  return LightenDarkenColor(col, amt);
};
export var darken = function darken(col, amt) {
  return LightenDarkenColor(col, amt * -1);
};

function resolveColor(palette, defaultPalette) {
  var isGray = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

  var colorPalette = _objectSpread({}, palette);

  if (Object.keys(palette).length === 0) {
    colorPalette = defaultPalette;
  } else if (!palette.main && palette.lighter) {
    colorPalette.lighter = palette.lighter;
    colorPalette.light = !palette.light ? darken(colorPalette.lighter, 15) : palette.light;
    colorPalette.main = darken(palette.light, 15);
    colorPalette.dark = !palette.dark ? darken(colorPalette.main, 15) : palette.dark;
    colorPalette.darker = !palette.darker ? darken(colorPalette.dark, 15) : palette.darker;
  } else if (!palette.main && palette.light) {
    colorPalette.lighter = !palette.lighter ? lighter(colorPalette.light, 15) : palette.lighter;
    colorPalette.light = palette.light;
    colorPalette.main = darken(palette.light, 15);
    colorPalette.dark = !palette.dark ? darken(colorPalette.main, 15) : palette.dark;
    colorPalette.darker = !palette.darker ? darken(colorPalette.dark, 15) : palette.darker;
  } else if (!palette.main && palette.dark) {
    colorPalette.darker = !palette.darker ? darken(colorPalette.dark, 15) : palette.darker;
    colorPalette.dark = palette.dark;
    colorPalette.main = lighter(palette.dark, 15);
    colorPalette.light = !palette.light ? lighter(colorPalette.main, 15) : palette.light;
    colorPalette.lighter = !palette.lighter ? lighter(colorPalette.light, 15) : palette.lighter;
  } else if (!palette.main && palette.darker) {
    colorPalette.darker = palette.darker;
    colorPalette.dark = !palette.dark ? lighter(colorPalette.darker, 15) : palette.dark;
    colorPalette.main = lighter(palette.dark, 15);
    colorPalette.light = !palette.light ? lighter(palette.main, 15) : palette.light;
    colorPalette.lighter = !palette.lighter ? lighter(palette.light, 15) : palette.lighter;
  } else {
    colorPalette.main = palette.main;
    colorPalette.dark = !palette.dark ? darken(colorPalette.main, 15) : palette.dark;
    colorPalette.darker = !palette.darker ? darken(colorPalette.dark, 15) : palette.darker;
    colorPalette.light = !palette.light ? lighter(colorPalette.main, 15) : palette.light;
    colorPalette.lighter = !palette.lighter ? lighter(colorPalette.light, 15) : palette.lighter;
  }

  colorPalette.contrastText = getContrastText(colorPalette.main);

  if (isGray) {
    if (!colorPalette.lighter) {
      colorPalette.lighter = lighter(colorPalette.light, 20);
    }

    if (!colorPalette.darker) {
      colorPalette.darker = darken(colorPalette.dark, 20);
    }
  }

  return colorPalette;
}

var defaultConfig = {
  cleverUI: {
    spacers: {
      values: [0, 4, 8, 12, 16, 24, 32, 48, 64, 96, 128, 192, 256, 384, 512],
      calculateSpace: function calculateSpace() {},
      unit: "px"
    },
    colors: getColors(),
    typography: {
      font1: ["Montserrat", "sans-serif"].join(","),
      font2: ["Nunito", "sans-serif"].join(","),
      fontWeight: 400,
      fontSizeBase: {
        constant: 18,
        unit: "px"
      },
      lineHeight: 1.25,
      color: "#4a4a4a",
      sizing: {
        h1: 36,
        h2: 24,
        h3: 21,
        h4: 18,
        h5: 18,
        h6: 18,
        body0: 14,
        body1: 18,
        body2: 21,
        body3: 24,
        span: 18
      },
      variants: {
        title: {},
        subtitle: {},
        title2: {},
        paragraph: {},
        inputText: {},
        small: {},
        caption: {}
      }
    },
    icons: {
      sizeBase: 24
    },
    shadows: {
      none: "none",
      hover: "0 2px 20px rgba(0,0,0,0.1)",
      normal: "0 2px 20px rgba(0,0,0,0.1)"
    },
    spaces: {
      xs: 8,
      sm: 16,
      md: 24,
      lg: 48,
      xl: 72,
      xxl: 96
    },
    breakpoints: {
      values: {
        sm: 800,
        md: 1400,
        lg: 1800
      },
      up: function up() {},
      down: function down() {}
    }
  }
};
var currentConfig = defaultConfig; // TODO: deprecated when remove from styles/sizing

export var getCurrentConfig = function getCurrentConfig() {
  return currentConfig;
};
export default (function (themeConfig) {
  var _title, _subtitle, _title2, _paragraph, _inputText;

  var cleverConfig = {};

  if (!!themeConfig && !!themeConfig.cleverUI) {
    cleverConfig = themeConfig.cleverUI;
  } else if (!!themeConfig && !themeConfig.materialUI) {
    cleverConfig = themeConfig;
  }

  if (cleverConfig.colors) {
    forEach(Object.keys(cleverConfig.colors), function (color) {
      cleverConfig.colors[color] = resolveColor(cleverConfig.colors[color], defaultConfig.cleverUI.colors[color], color === "gray");
    });
  }

  cleverConfig = merge(defaultConfig.cleverUI, cleverConfig);
  var materialTheme = createMaterialTheme(cleverConfig);
  var materialThemeCustomized = merge(materialTheme, themeConfig === null || themeConfig === void 0 ? void 0 : themeConfig.materialUI);
  cleverConfig.spacers = _objectSpread(_objectSpread({}, cleverConfig.spacers), {}, {
    calculateSpace: function calculateSpace(size) {
      return cleverConfig.spacers.values[size];
    },
    unit: "px"
  });
  cleverConfig.spacing = generateSpacing(cleverConfig.spacers.calculateSpace, cleverConfig.spacers.unit, cleverConfig.spacers.values.length);
  cleverConfig.breakpoints = _objectSpread(_objectSpread({}, cleverConfig.breakpoints), {}, {
    up: function up(breakpoint) {
      return "@media(min-width: ".concat(cleverConfig.breakpoints.values[breakpoint], "px)");
    },
    down: function down(breakpoint) {
      return "@media(max-width: ".concat(cleverConfig.breakpoints.values[breakpoint] - 1, "px)");
    }
  });
  cleverConfig.typography.variants = _objectSpread(_objectSpread({}, cleverConfig.typography.variants), {}, {
    title: (_title = {
      fontFamily: cleverConfig.typography.font1,
      fontSize: 21,
      fontWeight: 700,
      lineHeight: 1
    }, _defineProperty(_title, cleverConfig.breakpoints.up("md"), {
      fontSize: 24
    }), _defineProperty(_title, cleverConfig.breakpoints.up("lg"), {
      fontSize: 28
    }), _title),
    subtitle: (_subtitle = {
      fontFamily: cleverConfig.typography.font2,
      fontSize: 18,
      fontWeight: 400,
      lineHeight: 1.25
    }, _defineProperty(_subtitle, cleverConfig.breakpoints.up("sm"), {
      fontSize: 21
    }), _defineProperty(_subtitle, cleverConfig.breakpoints.up("md"), {
      fontSize: 24
    }), _defineProperty(_subtitle, cleverConfig.breakpoints.up("lg"), {
      fontSize: 28
    }), _subtitle),
    title2: (_title2 = {
      fontFamily: cleverConfig.typography.font1,
      fontSize: 14,
      fontWeight: 600,
      lineHeight: 1.5
    }, _defineProperty(_title2, cleverConfig.breakpoints.up("md"), {
      fontSize: 18
    }), _defineProperty(_title2, cleverConfig.breakpoints.up("lg"), {
      fontSize: 21
    }), _title2),
    paragraph: (_paragraph = {
      fontFamily: cleverConfig.typography.font2,
      fontSize: 14,
      fontWeight: 400,
      lineHeight: 1.5
    }, _defineProperty(_paragraph, cleverConfig.breakpoints.up("md"), {
      fontSize: 18
    }), _defineProperty(_paragraph, cleverConfig.breakpoints.up("lg"), {
      fontSize: 21
    }), _paragraph),
    inputText: (_inputText = {
      fontFamily: cleverConfig.typography.font1,
      fontSize: 14,
      fontWeight: 600,
      lineHeight: 1.5
    }, _defineProperty(_inputText, cleverConfig.breakpoints.up("md"), {
      fontSize: 16
    }), _defineProperty(_inputText, cleverConfig.breakpoints.up("lg"), {
      fontSize: 18
    }), _inputText),
    small: _defineProperty({
      fontFamily: cleverConfig.typography.font1,
      fontSize: 12,
      fontWeight: 500,
      lineHeight: 1.75
    }, cleverConfig.breakpoints.up("lg"), {
      fontSize: 14
    }),
    caption: _defineProperty({
      fontFamily: cleverConfig.typography.font1,
      fontSize: 12,
      fontWeight: 400,
      lineHeight: 1.75,
      letterSpacing: ".1em",
      textTransform: "uppercase"
    }, cleverConfig.breakpoints.up("lg"), {
      fontSize: 14
    })
  });
  currentConfig = _objectSpread({
    cleverUI: cleverConfig
  }, materialThemeCustomized);
  return currentConfig;
});
var currentEnvConfig = {
  analytics: {
    id: "",
    isSandBox: true,
    options: {}
  },
  mixpanel: {
    id: "",
    isSandBox: true,
    options: {}
  },
  tagManager: {
    id: "",
    isSandBox: true
  },
  gtag: {
    id: "",
    isSandBox: true
  },
  smartlook: {
    id: "",
    isSandBox: true
  }
};

var deprecationWarning = function deprecationWarning(message) {
  if (process.env.NODE_ENV === "development") {
    // eslint-disable-next-line no-console
    console.warn("[Clever-react]: ".concat(message));
  }
};

export var getEnvConfig = function getEnvConfig() {
  // deprecation warning
  deprecationWarning("The function 'getEnvConfig' is deprecated");
  return currentEnvConfig;
};
export var initEnvConfig = function initEnvConfig(_ref) {
  var analytics = _ref.analytics,
      gtag = _ref.gtag,
      tagManager = _ref.tagManager,
      mixpanel = _ref.mixpanel,
      smartlook = _ref.smartlook;
  // deprecation warning
  deprecationWarning("[Clever-react]: The function 'initEnvConfig' is deprecated");

  if (analytics) {
    currentEnvConfig.analytics = analytics;
  }

  if (!currentEnvConfig.analytics.options && currentEnvConfig.analytics.options === undefined) {
    currentEnvConfig.analytics = _objectSpread(_objectSpread({}, currentEnvConfig.analytics), {}, {
      options: {}
    });
  }

  if (mixpanel) {
    currentEnvConfig.mixpanel = mixpanel;
  }

  if (!currentEnvConfig.mixpanel.options && currentEnvConfig.mixpanel.options === undefined) {
    currentEnvConfig.mixpanel = _objectSpread(_objectSpread({}, currentEnvConfig.mixpanel), {}, {
      options: {}
    });
  }

  if (gtag) {
    currentEnvConfig.gtag = gtag;
  }

  if (tagManager) {
    currentEnvConfig.tagManager = tagManager;
  }

  if (smartlook) {
    currentEnvConfig.smartlook = smartlook;
  }

  return currentEnvConfig;
};