// export const adTipsRegEx = /((http|https):\/{2})?([0-9a-z_-]+\.)+[a-z]{1,4}/;
export var urlRegExp = /^((http|https):\/{2})?(([0-9a-zA-Z_-]+\.)+[0-9a-zA-Z]+(\/([~0-9a-zA-Z#+%@./_-]+))?[/]?)(\?((.*(=.*)?)(&?)))*$/;
export var dots = /.+(\w+(\.|,))\S+/;