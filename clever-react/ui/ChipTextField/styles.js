import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _alignRight;

  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      typography = _ref$cleverUI.typography,
      spacing = _ref$cleverUI.spacing,
      breakpoints = _ref.breakpoints;
  var sizing = typography.sizing;
  return {
    link: {
      textDecoration: "none"
    },
    autocomplete: _objectSpread({}, spacing["mb-3"]),
    selected: _objectSpread({}, spacing["pl-5"]),
    tools: _objectSpread({}, spacing["mb-4"]),
    chip: _objectSpread(_objectSpread({}, spacing["mr-2"]), spacing["mb-2"]),
    icon: _objectSpread(_objectSpread({}, spacing["mr-2"]), spacing["mb-0"]),
    alertText: _objectSpread(_objectSpread({}, spacing["mt-2"]), {}, {
      display: "inline-flex",
      flexWrap: "nowrap"
    }),
    charCount: {
      marginLeft: "-25px"
    },
    iconTooltip: _objectSpread({}, spacing["mt-1"]),
    inputEnter: {
      opacity: 0.01
    },
    inputEnterActive: {
      opacity: 1,
      transition: "opacity 500ms ease-in"
    },
    inputLeave: {
      opacity: 1
    },
    inputLeaveActive: {
      opacity: 0.01,
      transition: "opacity 300ms ease-in"
    },
    inputTransition: {
      zIndex: 1
    },
    error: {
      "& fieldset": {
        borderColor: colors.danger.main
      }
    },
    alignRight: (_alignRight = {
      textAlign: "right",
      fontFamily: typography.font1
    }, _defineProperty(_alignRight, breakpoints.down("sm"), {
      fontSize: "16px"
    }), _defineProperty(_alignRight, breakpoints.up("md"), {
      fontSize: sizing.body1
    }), _alignRight)
  };
};

export default makeStyles(styles);