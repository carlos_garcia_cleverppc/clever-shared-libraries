import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";
import { cssStringHex2rgba } from "../styles/colors";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing,
      typography = _ref$cleverUI.typography;
  return {
    paper: {
      borderRadius: "0",
      borderBottom: "1px solid ".concat(colors.neutral[300])
    },
    table: {
      borderCollapse: "collapse" // backgroundColor: "white",

    },
    row: {
      "&:nth-of-type(odd)": {
        backgroundColor: cssStringHex2rgba("#F0F7F7")
      },
      height: "36px"
    },
    rowHover: {
      "&:hover": {
        backgroundColor: cssStringHex2rgba(colors.neutral[100])
      }
    },
    header: {
      fontWeight: "bold",
      textTransform: "uppercase",
      backgroundColor: colors.primary[500],
      fontFamily: typography.font1,
      fontSize: "12px",
      color: colors.primary[100],
      "& tr": {
        "& th": _objectSpread(_objectSpread(_objectSpread({}, spacing["pt-2"]), spacing["pb-2"]), spacing["px-5"])
      }
    },
    label: {
      color: "".concat(colors.primary[500], " !important")
    },
    cell: {
      "& p": {
        // color: colors.neutral[700],
        fontFamily: typography.font1,
        fontSize: "12px",
        fontWeight: "600"
      }
    },
    headerSelectionIcon: {
      color: colors.primary[100]
    },
    selectionIcon: {
      color: colors.primary[500]
    }
  };
};

export default makeStyles(styles);