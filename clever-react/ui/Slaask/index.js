import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable react/no-this-in-sfc */

/* eslint-disable no-empty */

/* eslint-disable no-undef */

/* eslint-disable react/no-multi-comp */
import React, { useEffect } from "react";
import PropTypes from "prop-types";

var Slaask = function Slaask(_ref) {
  var params = _ref.params,
      token = _ref.token,
      identify = _ref.identify;
  useEffect(function () {
    var x = document.createElement("script");
    x.src = "https://cdn.slaask.com/chat.js";
    x.type = "text/javascript";
    x.async = "true";
    x.id = "slaask-script";

    x.onreadystatechange = function () {
      var x1 = document.readyState;

      if (!x1 || x1 === "complete" || x1 === "loaded") {
        try {
          _slaask.identify(identify, _objectSpread(_objectSpread({}, params), {}, {
            path: window.location.pathname
          }));

          _slaask.init(token);
        } catch (e) {}
      }
    };

    x.onload = x.onreadystatechange;
    setTimeout(function () {
      var t = document.getElementById("slaask-insert");

      try {
        if (t && t.parentNode && t.parentNode.insertBefore && !document.getElementById("slaask-script")) {
          t.parentNode.insertBefore(x, t);
        }
      } catch (e) {}
    }, 10000);
  }, [params, token, identify]);
  return /*#__PURE__*/React.createElement("div", {
    id: "slaask-insert"
  });
};

Slaask.propTypes = {
  identify: PropTypes.string.isRequired,
  token: PropTypes.string.isRequired,
  params: PropTypes.instanceOf(Object)
};
Slaask.defaultProps = {
  params: {}
};
export default Slaask;