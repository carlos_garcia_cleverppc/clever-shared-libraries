import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import makeStyles from "../styles/makeStyles";
import ImageIcon from "../Icon/Image";
import Card from "../Card";
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var size = _ref.size;
      return {
        width: size,
        height: size,
        display: "grid",
        placeItems: "center",
        boxSizing: "border-box",
        marginBottom: 0 // override Card styles

      };
    }
  };
});

var PlaceHolder = function PlaceHolder(_ref2) {
  var icon = _ref2.icon,
      className = _ref2.className,
      size = _ref2.size;
  var classes = getClasses({
    size: size
  });
  return /*#__PURE__*/React.createElement(Card, {
    variant: "striped",
    className: cx(classes.root, className)
  }, icon);
};

PlaceHolder.propTypes = {
  icon: PropTypes.element,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};
PlaceHolder.defaultProps = {
  icon: /*#__PURE__*/React.createElement(ImageIcon, {
    size: 32
  }),
  size: 120
};
export default PlaceHolder;