var isDefined = function isDefined(value) {
  return value != null;
};

var mergeRefs = function mergeRefs() {
  for (var _len = arguments.length, refs = new Array(_len), _key = 0; _key < _len; _key++) {
    refs[_key] = arguments[_key];
  }

  return function (value) {
    refs.forEach(function (ref) {
      if (typeof ref === "function") {
        ref(value);
      } else if (isDefined(ref)) {
        ref.current = value;
      }
    });
  };
};

export default mergeRefs;