import _slicedToArray from "@babel/runtime/helpers/slicedToArray";

/* eslint-disable consistent-return */
import { useEffect, useState } from "react";

var useIsDragover = function useIsDragover(ref) {
  var _useState = useState(false),
      _useState2 = _slicedToArray(_useState, 2),
      isDragover = _useState2[0],
      setIsDragover = _useState2[1];

  useEffect(function () {
    var el = ref.current;
    if (!el) return;

    var handleDrag = function handleDrag() {
      return setIsDragover(true);
    };

    var handleDragOut = function handleDragOut() {
      return setIsDragover(false);
    };

    el.addEventListener("dragleave", handleDragOut);
    el.addEventListener("drop", handleDragOut);
    el.addEventListener("dragover", handleDrag);
    return function () {
      el.removeEventListener("dragleave", handleDragOut);
      el.removeEventListener("drop", handleDragOut);
      el.removeEventListener("dragover", handleDrag);
    };
  }, [ref]);
  return isDragover;
};

export default useIsDragover;