import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import makeStyles from "../styles/makeStyles";
import PlaceHolder from "./PlaceHolder";
var getClasses = makeStyles(function (_ref) {
  var colors = _ref.cleverUI.colors;
  return {
    outline: {
      boxShadow: "inset 0 0 2px rgba(0,0,0, .2)",
      borderRadius: 12
    },
    image: function image(_ref2) {
      var size = _ref2.size;
      return {
        borderRadius: 12,
        objectFit: "contain",
        width: size,
        height: size
      };
    },
    background: {
      borderRadius: 12,
      backgroundColor: colors.gray.lighter,
      opacity: 0.05
    }
  };
});

var Preview = function Preview(_ref3) {
  var src = _ref3.src,
      className = _ref3.className,
      size = _ref3.size;
  var classes = getClasses({
    size: size
  });

  if (!src) {
    return /*#__PURE__*/React.createElement(PlaceHolder, null);
  }

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
    className: classes.background
  }), /*#__PURE__*/React.createElement("img", {
    className: cx(classes.image, className),
    src: src,
    alt: ""
  }), /*#__PURE__*/React.createElement("div", {
    className: classes.outline
  }));
};

Preview.propTypes = {
  src: PropTypes.string,
  size: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
};
Preview.defaultProps = {
  src: "",
  size: 120
};
export default Preview;