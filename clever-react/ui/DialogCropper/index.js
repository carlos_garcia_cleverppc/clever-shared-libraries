import _extends from "@babel/runtime/helpers/extends";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["title", "description", "buttonText", "open", "onClose", "value", "onSubmit", "maxHeight", "maxWidth"];
import React, { useRef } from "react";
import PropTypes from "prop-types";
import Dialog from "../Dialog";
import Cropper from "./Cropper";
import getClasses from "./styles";

var DialogCropper = function DialogCropper(props) {
  var title = props.title,
      description = props.description,
      buttonText = props.buttonText,
      open = props.open,
      onClose = props.onClose,
      value = props.value,
      onSubmit = props.onSubmit,
      maxHeight = props.maxHeight,
      maxWidth = props.maxWidth,
      cropperProps = _objectWithoutProperties(props, _excluded);

  var cropperRef = useRef();
  var classes = getClasses();

  var cropImage = function cropImage() {
    var cropperCanvasOptions = {
      maxWidth: maxWidth,
      maxHeight: maxHeight
    };
    var canvas = cropperRef.current.cropper.getCroppedCanvas(cropperCanvasOptions);
    onSubmit(canvas.toDataURL());
  };

  return /*#__PURE__*/React.createElement(Dialog, {
    open: open,
    onClose: onClose,
    description: description,
    title: title,
    buttonPrimary: {
      label: buttonText,
      onClick: cropImage
    }
  }, /*#__PURE__*/React.createElement(Cropper, _extends({
    ref: cropperRef,
    src: value,
    className: classes.cropper,
    minCropBoxWidth: 100,
    minCropBoxHeight: 100,
    guides: true,
    autoCropArea: 1,
    viewMode: 1
  }, cropperProps)));
};

DialogCropper.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  maxHeight: PropTypes.number,
  maxWidth: PropTypes.number
};
DialogCropper.defaultProps = {
  maxHeight: 500,
  maxWidth: 500
};
export default DialogCropper;