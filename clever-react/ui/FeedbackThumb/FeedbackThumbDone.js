import React from "react";
import { m } from "framer-motion";
import PropTypes from "prop-types";
import useTheme from "../styles/useTheme";
import makeStyles from "../styles/makeStyles";
import Icon from "../Icon";
var getClasses = makeStyles(function () {
  return {
    container: {
      display: "flex"
    },
    thumb: function thumb(_ref) {
      var c = _ref.c,
          variant = _ref.variant,
          size = _ref.size;
      return {
        position: "absolute",
        top: "".concat(variant !== "compact" ? 24 * c : size - size / 1.2, "px"),
        left: "".concat(variant !== "compact" ? 24 * c : size - size / 1.2, "px"),
        height: "".concat(variant !== "compact" ? 24 * c : size / 1.6, "px"),
        width: "".concat(variant !== "compact" ? 24 * c : size / 1.6, "px")
      };
    }
  };
});

var FeedbackThumbDone = function FeedbackThumbDone(_ref2) {
  var variant = _ref2.variant,
      opinion = _ref2.opinion,
      size = _ref2.size;

  var _useTheme = useTheme(),
      colors = _useTheme.cleverUI.colors;

  var c = size / 80;
  var classes = getClasses({
    c: c,
    variant: variant,
    size: size
  });
  var isPositive = opinion;
  var color = isPositive ? "yellow" : "primary";
  return /*#__PURE__*/React.createElement(m.div, {
    initial: {
      scale: 0
    },
    animate: {
      rotate: 360,
      scale: 1
    },
    transition: {
      ease: [0.4, 1.39, 0.93, 1.01]
    }
  }, variant !== "compact" && /*#__PURE__*/React.createElement("div", {
    className: classes.container
  }, /*#__PURE__*/React.createElement(Icon, {
    icon: isPositive ? "ThumbUp" : "ThumbDown",
    color: color,
    className: classes.thumb
  }), /*#__PURE__*/React.createElement("svg", {
    width: "".concat(size, "px"),
    size: "".concat(size, "px"),
    viewBox: "0 0 ".concat(size, " ").concat(size),
    version: "1.1"
  }, /*#__PURE__*/React.createElement("g", {
    id: "Final",
    stroke: "none",
    strokeWidth: "".concat(1 * c),
    fill: "none",
    fillRule: "evenodd"
  }, /*#__PURE__*/React.createElement("g", {
    id: "Group-2"
  }, /*#__PURE__*/React.createElement("circle", {
    id: "Oval",
    fill: colors[color][100],
    cx: "".concat(36 * c),
    cy: "".concat(36 * c),
    r: "".concat(23 * c)
  })), /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("circle", {
    id: "Oval-1",
    fill: colors[color][100],
    cx: "".concat(67 * c),
    cy: "".concat(52 * c),
    r: "".concat(2 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-2",
    fill: colors[color][100],
    cx: "".concat(57 * c),
    cy: "".concat(72 * c),
    r: "".concat(2 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-3",
    fill: colors[color][100],
    cx: "".concat(7 * c),
    cy: "".concat(62 * c),
    r: "".concat(2 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-4",
    fill: colors[color][100],
    cx: "".concat(3 * c),
    cy: "".concat(12 * c),
    r: "".concat(2 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-5",
    fill: colors[color][100],
    cx: "".concat(12 * c),
    cy: "".concat(32 * c),
    r: "".concat(2 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-6",
    fill: colors[color][100],
    cx: "".concat(21 * c),
    cy: "".concat(14 * c),
    r: "".concat(3 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-7",
    fill: colors[color][100],
    cx: "".concat(28 * c),
    cy: "".concat(74 * c),
    r: "".concat(3 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-8",
    fill: colors[color][100],
    cx: "".concat(61 * c),
    cy: "".concat(3 * c),
    r: "".concat(3 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-9",
    fill: colors[color][100],
    cx: "".concat(62 * c),
    cy: "".concat(14 * c),
    r: "".concat(1 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-10",
    fill: colors[color][100],
    cx: "".concat(41 * c),
    cy: "".concat(6 * c),
    r: "".concat(1 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-11",
    fill: colors[color][100],
    cx: "".concat(1 * c),
    cy: "".concat(46 * c),
    r: "".concat(1 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-12",
    fill: colors[color][100],
    cx: "".concat(26 * c),
    cy: "".concat(64 * c),
    r: "".concat(1 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-13",
    fill: colors[color][100],
    cx: "".concat(66 * c),
    cy: "".concat(65 * c),
    r: "".concat(1 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-14",
    fill: colors[color][100],
    cx: "".concat(79 * c),
    cy: "".concat(40 * c),
    r: "".concat(1 * c)
  }), /*#__PURE__*/React.createElement("circle", {
    id: "Oval-15",
    fill: colors[color][100],
    cx: "".concat(73 * c),
    cy: "".concat(27 * c),
    r: "".concat(2 * c)
  }))))), variant === "compact" && /*#__PURE__*/React.createElement("div", {
    className: classes.container
  }, /*#__PURE__*/React.createElement(Icon, {
    icon: isPositive ? "ThumbUp" : "ThumbDown",
    color: color,
    className: classes.thumb
  }), /*#__PURE__*/React.createElement("svg", {
    width: "".concat(size, "px"),
    size: "".concat(size, "px"),
    viewBox: "0 0 ".concat(size, " ").concat(size),
    version: "1.1"
  }, /*#__PURE__*/React.createElement("g", {
    id: "Final",
    stroke: "none",
    strokeWidth: "".concat(1 * c),
    fill: "none",
    fillRule: "evenodd"
  }, /*#__PURE__*/React.createElement("g", {
    id: "Group-2"
  }, /*#__PURE__*/React.createElement("circle", {
    id: "Oval",
    fill: colors[color][100],
    cx: size / 2,
    cy: size / 2,
    r: size / 2
  }))))));
};

FeedbackThumbDone.propTypes = {
  opinion: PropTypes.bool,
  size: PropTypes.number
};
FeedbackThumbDone.defaultProps = {
  opinion: true,
  size: 80
};
export default FeedbackThumbDone;