import React from "react";
import { LazyMotion, domAnimation, m } from "framer-motion";
import PropTypes from "prop-types";
import IconButton from "../IconButton";
import makeStyles from "../styles/makeStyles";
import Icon from "../Icon";
import Grid from "../Grid";
import FeedbackThumbDone from "./FeedbackThumbDone";
var getClasses = makeStyles(function () {
  return {
    thumbWrapper: function thumbWrapper(_ref) {
      var size = _ref.size;
      return {
        height: "".concat(size, "px")
      };
    },
    thumb: function thumb(_ref2) {
      var c = _ref2.c,
          variant = _ref2.variant,
          size = _ref2.size;
      return {
        height: "".concat(variant !== "compact" ? 24 * c : size / 1.6, "px !important"),
        width: "".concat(variant !== "compact" ? 24 * c : size / 1.6, "px !important")
      };
    }
  };
});

var FeedbackBlock = function FeedbackBlock(_ref3) {
  var variant = _ref3.variant,
      handleThumbClick = _ref3.handleThumbClick,
      isSelected = _ref3.isSelected,
      thumbUp = _ref3.thumbUp,
      size = _ref3.size;
  var c = size / 80;
  var classes = getClasses({
    size: size,
    c: c,
    variant: variant
  });
  var opinion = isSelected === "up";
  return /*#__PURE__*/React.createElement(LazyMotion, {
    features: domAnimation,
    strict: true
  }, isSelected && opinion === thumbUp && /*#__PURE__*/React.createElement(FeedbackThumbDone, {
    variant: variant,
    opinion: thumbUp,
    size: size
  }), !isSelected && /*#__PURE__*/React.createElement(m.div, null, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center",
    className: classes.thumbWrapper
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true
  }, /*#__PURE__*/React.createElement(IconButton, {
    icon: /*#__PURE__*/React.createElement(Icon, {
      icon: thumbUp ? "ThumbUp" : "ThumbDown",
      className: classes.thumb
    }),
    onClick: function onClick() {
      return handleThumbClick(thumbUp ? "up" : "down");
    }
  })))));
};

FeedbackBlock.propTypes = {
  isSelected: PropTypes.oneOf(["up", "down"]),
  handleThumbClick: PropTypes.func,
  thumbUp: PropTypes.bool,
  size: PropTypes.number,
  variant: PropTypes.oneOf(["default", "compact"])
};
FeedbackBlock.defaultProps = {
  isSelected: undefined,
  handleThumbClick: function handleThumbClick() {},
  thumbUp: false,
  size: 80,
  variant: "default"
};
export default FeedbackBlock;