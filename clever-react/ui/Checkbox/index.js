import _extends from "@babel/runtime/helpers/extends";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["className", "children", "disabled"];
import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import MaterialCheckbox from "@material-ui/core/Checkbox";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBox from "@material-ui/icons/CheckBox";
import getClasses from "./styles";

var Checkbox = function Checkbox(props) {
  var className = props.className,
      children = props.children,
      disabled = props.disabled,
      checkboxProps = _objectWithoutProperties(props, _excluded);

  var classes = getClasses({
    disabled: disabled
  });
  return /*#__PURE__*/React.createElement("label", {
    className: cx(classes.root, className)
  }, /*#__PURE__*/React.createElement(MaterialCheckbox, _extends({
    className: classes.checkbox,
    disabled: disabled
  }, checkboxProps)), /*#__PURE__*/React.createElement("div", null, children));
};

Checkbox.propTypes = {
  className: PropTypes.string,
  checked: PropTypes.bool.isRequired,
  value: PropTypes.string,
  onChange: PropTypes.func,
  color: PropTypes.oneOf(["primary", "secondary"]),
  disabled: PropTypes.bool,
  icon: PropTypes.instanceOf(Object),
  checkedIcon: PropTypes.instanceOf(Object),
  size: PropTypes.oneOf(["medium", "small"]),
  required: PropTypes.bool
};
Checkbox.defaultProps = {
  className: "",
  onChange: function onChange() {},
  value: "",
  color: "primary",
  disabled: false,
  icon: /*#__PURE__*/React.createElement(CheckBoxOutlineBlankIcon, null),
  checkedIcon: /*#__PURE__*/React.createElement(CheckBox, null),
  size: "medium",
  required: false
};
export default Checkbox;