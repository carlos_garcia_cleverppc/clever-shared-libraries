import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import MaterialTooltip from "@material-ui/core/Tooltip";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import getClasses from "./styles";
import Grid from "../Grid";
import Typography from "../Typography";
import Circle from "../Icon/Circle";
import { onKeyDown } from "../../utils/helpers";
import useMediaQuery from "../styles/useMediaQuery";
import { screenSm, screenXs } from "../styles/breakpoints";
import { getColorsLabels } from "../styles/colors";
var colorsList = getColorsLabels();

var Tooltip = function Tooltip(props) {
  var _useState = useState(false),
      _useState2 = _slicedToArray(_useState, 2),
      openState = _useState2[0],
      setOpenState = _useState2[1];

  var children = props.children,
      title = props.title,
      placement = props.placement,
      type = props.type,
      color = props.color;
  var icon = props.icon,
      description = props.description,
      className = props.className,
      spacing = props.spacing,
      clickable = props.clickable,
      textAlign = props.textAlign;
  var classes = getClasses({
    color: color,
    textAlign: textAlign
  });
  var isScreenSm = useMediaQuery(screenSm);
  var isScreenXs = useMediaQuery(screenXs);
  var aux = /*#__PURE__*/React.createElement(Grid, {
    container: true,
    className: classes.card
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 1
  }, icon), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 11
  }, /*#__PURE__*/React.createElement(Typography, {
    weight: "bold",
    variant: "body2",
    className: classes.text
  }, title)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body1"
  }, description)));

  var handleTooltipClose = function handleTooltipClose() {
    setOpenState(false);
  };

  var handleTooltipOpen = function handleTooltipOpen() {
    setOpenState(true);
  };

  var getSpacing = function getSpacing() {
    switch (spacing) {
      case 1:
        return classes.tooltipSpacingSmall;

      case 2:
        return classes.tooltipSpacingMed;

      case 3:
        return classes.tooltipSpacingLarge;

      default:
        return "";
    }
  };

  if (isScreenSm || isScreenXs || clickable) {
    return /*#__PURE__*/React.createElement(ClickAwayListener, {
      onClickAway: handleTooltipClose
    }, /*#__PURE__*/React.createElement(MaterialTooltip, {
      title: type === "simple" ? title : aux,
      classes: {
        tooltip: classnames(classes.tooltip, getSpacing(), color ? classes.tooltipDiv : ""),
        arrow: classes.arrow
      },
      placement: placement,
      arrow: true,
      className: className,
      onClose: handleTooltipClose,
      open: openState,
      interactive: true,
      disableFocusListener: true,
      disableHoverListener: true,
      disableTouchListener: true
    }, /*#__PURE__*/React.createElement("span", {
      className: classes.root,
      onClick: handleTooltipOpen,
      role: "button",
      tabIndex: 0,
      onKeyDown: onKeyDown(handleTooltipOpen)
    }, children)));
  }

  return /*#__PURE__*/React.createElement(MaterialTooltip, {
    title: type === "simple" ? title : aux,
    classes: {
      tooltip: classnames(classes.tooltip, getSpacing()),
      arrow: classes.arrow
    },
    placement: placement,
    arrow: true,
    className: className,
    interactive: true
  }, /*#__PURE__*/React.createElement("span", {
    className: classes.root
  }, children));
};

Tooltip.propTypes = {
  children: PropTypes.instanceOf(Object).isRequired,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Object)]).isRequired,
  placement: PropTypes.string,
  type: PropTypes.oneOf(["simple", "complete"]),
  icon: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Object)]),
  description: PropTypes.string,
  className: PropTypes.string,
  spacing: PropTypes.oneOf([0, 1, 2, 3]),
  clickable: PropTypes.bool,
  color: PropTypes.oneOf(colorsList),
  textAlign: PropTypes.oneOf(["left", "center", "right"])
};
Tooltip.defaultProps = {
  placement: "top",
  type: "simple",
  icon: /*#__PURE__*/React.createElement(Circle, null),
  description: "",
  className: "",
  spacing: 1,
  clickable: false,
  color: "navyBlue",
  textAlign: "center"
};
export default Tooltip;