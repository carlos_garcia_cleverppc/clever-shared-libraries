import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing,
      typography = _ref$cleverUI.typography;
  return {
    tooltip: function tooltip(_ref2) {
      var color = _ref2.color,
          textAlign = _ref2.textAlign,
          space = _ref2.space;
      return _objectSpread(_objectSpread({}, spacing["p-".concat(space + 1)]), {}, {
        fontSize: "12px",
        fontFamily: typography.font1,
        backgroundColor: colors[color][500],
        maxWidth: "350px",
        borderRadius: "8px",
        textAlign: textAlign,
        width: "100%",
        whiteSpace: "pre-line",
        display: "inline-block",
        zIndex: "100000"
      });
    },
    root: {
      display: "inline-block",
      width: "fit-content",
      outline: "none"
    },
    arrow: function arrow(_ref3) {
      var color = _ref3.color;
      return {
        color: colors[color][500]
      };
    },
    list: {
      backgroundColor: colors.neutral[300],
      padding: "1rem"
    },
    text: _objectSpread(_objectSpread({
      color: "white"
    }, spacing["ml-3"]), {}, {
      fontFamily: typography.font1
    }),
    card: {
      display: "flex"
    },
    icon: _objectSpread({}, spacing["mt-1"]),
    inline: {
      display: "inline"
    },
    tooltipSpacingSmall: _objectSpread({}, spacing["p-2"]),
    tooltipSpacingMed: _objectSpread({}, spacing["p-3"]),
    tooltipSpacingLarge: _objectSpread({}, spacing["p-4"]),
    tooltipDiv: function tooltipDiv(_ref4) {
      var color = _ref4.color;
      return {
        backgroundColor: colors[color][500]
      };
    }
  };
};

export default makeStyles(styles);