import _toConsumableArray from "@babel/runtime/helpers/toConsumableArray";
import _extends from "@babel/runtime/helpers/extends";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["color", "className"],
    _excluded2 = ["center", "className"];
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import MaterialCircular from "@material-ui/core/CircularProgress";
import { getColorsLabels } from "../styles/colors";
import Grid from "../Grid";
import getClasses from "./styles";
var colorsList = getColorsLabels(true);

var LoadingIcon = function LoadingIcon(_ref) {
  var color = _ref.color,
      className = _ref.className,
      props = _objectWithoutProperties(_ref, _excluded);

  var classes = getClasses({
    color: color
  });
  return /*#__PURE__*/React.createElement(MaterialCircular, _extends({
    className: classnames(className, classes.root)
  }, props));
};

var Loading = function Loading(_ref2) {
  var center = _ref2.center,
      className = _ref2.className,
      loadingIconProps = _objectWithoutProperties(_ref2, _excluded2);

  if (center) {
    return /*#__PURE__*/React.createElement(Grid, {
      container: true,
      justify: "center",
      align: "center",
      alignItems: "center",
      className: className,
      style: {
        height: "100%"
      }
    }, /*#__PURE__*/React.createElement(Grid, {
      item: true,
      xs: 12
    }, /*#__PURE__*/React.createElement(LoadingIcon, loadingIconProps)));
  }

  return /*#__PURE__*/React.createElement(LoadingIcon, _extends({
    className: className
  }, loadingIconProps));
};

Loading.propTypes = {
  size: PropTypes.number,
  color: PropTypes.oneOf([].concat(_toConsumableArray(colorsList), ["currentColor"])),
  center: PropTypes.bool
};
Loading.defaultProps = {
  size: 50,
  color: "primary",
  center: false
};
export default Loading;