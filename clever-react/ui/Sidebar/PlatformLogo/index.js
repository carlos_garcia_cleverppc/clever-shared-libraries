import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Image from "../../Image";
import { onKeyDown } from "../../../utils/helpers";
import getClasses from "./styles";

var PlatformLogo = function PlatformLogo(props) {
  var classes = getClasses({});
  var src = props.src,
      isSidebarCollapsed = props.isSidebarCollapsed,
      onClick = props.onClick,
      className = props.className;
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
    className: classnames(className, isSidebarCollapsed ? classes.imageCollapsed : classes.image, onClick ? classes.cursor : ""),
    onClick: onClick,
    onKeyDown: onKeyDown(onClick),
    role: "button",
    tabIndex: 0
  }, /*#__PURE__*/React.createElement(Image, {
    src: src,
    size: "responsive"
  })));
};

PlatformLogo.propTypes = {
  src: PropTypes.string,
  isSidebarCollapsed: PropTypes.bool,
  onClick: PropTypes.instanceOf(Object),
  className: PropTypes.string
};
PlatformLogo.defaultProps = {
  src: "",
  isSidebarCollapsed: false,
  onClick: null,
  className: ""
};
export default PlatformLogo;