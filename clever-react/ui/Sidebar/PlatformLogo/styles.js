import makeStyles from "../../styles/makeStyles";

var styles = function styles() {
  return {
    image: {
      maxWidth: "200px",
      height: "auto",
      outline: "none"
    },
    imageCollapsed: {
      maxWidth: "40px",
      height: "auto",
      outline: "none"
    },
    cursor: {
      cursor: "pointer"
    }
  };
};

export default makeStyles(styles);