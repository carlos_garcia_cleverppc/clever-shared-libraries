import _slicedToArray from "@babel/runtime/helpers/slicedToArray";

/* eslint-disable import/no-cycle */
import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import last from "lodash-es/last";
import UserAccountSelector from "../UserAccountSelector";
import Avatar from "../../Avatar";
import Grid from "../../Grid";
import Typography from "../../Typography";
import ChevronRight from "../../Icon/ChevronRight";
import { onKeyDown } from "../../../utils/helpers";
import getClasses from "./styles";

var UserAccount = function UserAccount(props) {
  var avatarSrc = props.avatarSrc,
      accountSelected = props.accountSelected,
      allAccounts = props.allAccounts,
      isSidebarCollapsed = props.isSidebarCollapsed,
      label = props.label,
      color = props.color;
  var handleOnClickItem = props.handleOnClickItem,
      handleOnClickLogOut = props.handleOnClickLogOut,
      handleAddAccount = props.handleAddAccount;
  var titleCopy = props.titleCopy,
      noAccountsCopy = props.noAccountsCopy,
      addAccountCopy = props.addAccountCopy,
      logOutCopy = props.logOutCopy;

  var _useState = useState(false),
      _useState2 = _slicedToArray(_useState, 2),
      openSelector = _useState2[0],
      setOpenSelector = _useState2[1];

  var _useState3 = useState(null),
      _useState4 = _slicedToArray(_useState3, 2),
      anchorEl = _useState4[0],
      setAnchorEl = _useState4[1];
  /* Cursor will be different if there are more accounts or not */


  var cursor = allAccounts.length > 0 ? "pointer" : "default";

  var handleClick = function handleClick(event) {
    /* This function only happen there are more accounts */
    if (allAccounts.length >= 1) {
      setAnchorEl(last(event.currentTarget.getElementsByTagName("svg")));
      setOpenSelector(!openSelector);
    }
  };

  var classes = getClasses({
    color: color,
    cursor: cursor
  });
  return /*#__PURE__*/React.createElement("div", {
    className: classes.root,
    onClick: handleClick,
    onKeyDown: onKeyDown(handleClick),
    role: "button",
    tabIndex: 0
  }, /*#__PURE__*/React.createElement(Avatar, {
    src: avatarSrc,
    size: 42,
    label: isSidebarCollapsed ? label : "",
    colorCircle: label !== "",
    className: !isSidebarCollapsed ? classes.accountInfo : "",
    color: color
  }), !isSidebarCollapsed && /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    className: classes.name
  }, /*#__PURE__*/React.createElement(Typography, {
    weight: "bold",
    variant: "f1-14",
    className: classes.accountName
  }, accountSelected.name), label !== "" && /*#__PURE__*/React.createElement(Typography, {
    color: color,
    weight: "bold",
    variant: "f1-8",
    className: classes.label
  }, label)), !!accountSelected.accountId && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body0"
  }, accountSelected.accountId))), allAccounts.length > 0 && /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(ChevronRight, {
    className: classnames(classes.arrow, openSelector ? classes.arrowPosition : ""),
    size: 20
  }), /*#__PURE__*/React.createElement(UserAccountSelector, {
    allAccounts: allAccounts,
    src: avatarSrc,
    handleOnClickItem: handleOnClickItem,
    handleOnClickLogOut: handleOnClickLogOut,
    handleAddAccount: handleAddAccount,
    titleCopy: titleCopy,
    noAccountsCopy: noAccountsCopy,
    addAccountCopy: addAccountCopy,
    logOutCopy: logOutCopy,
    open: openSelector,
    setOpen: setOpenSelector,
    anchorEl: anchorEl,
    setAnchorEl: setAnchorEl
  })));
};

UserAccount.propTypes = {
  avatarSrc: PropTypes.string,
  accountSelected: PropTypes.instanceOf(Object),
  allAccounts: PropTypes.instanceOf(Array),
  isSidebarCollapsed: PropTypes.bool,
  label: PropTypes.string,
  color: PropTypes.string,
  handleOnClickItem: PropTypes.instanceOf(Object),
  handleOnClickLogOut: PropTypes.instanceOf(Object),
  handleAddAccount: PropTypes.instanceOf(Object),
  titleCopy: PropTypes.string,
  noAccountsCopy: PropTypes.string,
  addAccountCopy: PropTypes.string,
  logOutCopy: PropTypes.string
};
UserAccount.defaultProps = {
  avatarSrc: "",
  accountSelected: {
    name: "",
    accountId: ""
  },
  allAccounts: [],
  isSidebarCollapsed: false,
  label: "",
  color: "primary",
  handleOnClickItem: function handleOnClickItem() {},
  handleOnClickLogOut: function handleOnClickLogOut() {},
  handleAddAccount: function handleAddAccount() {},
  titleCopy: "",
  noAccountsCopy: "",
  addAccountCopy: "",
  logOutCopy: ""
};
export default UserAccount;