import React from "react";
import PropTypes from "prop-types";
import Grid from "../../Grid";
import Icon from "../../Icon";
import IconButton from "../../IconButton";
import Badge from "../../Badge";
import Tooltip from "../../Tooltip";
import getClasses from "./styles";

var FooterActions = function FooterActions(props) {
  var items = props.items,
      color = props.color,
      isSidebarCollapsed = props.isSidebarCollapsed;
  var classes = getClasses();

  var printItem = function printItem(item) {
    /* Each item can has: name, icon, isSelected, badgeActive and onClick */
    var name = item.name ? item.name : "";
    var icon = item.icon ? item.icon : "";
    var isSelected = item.isSelected; // item.isSelected

    var badgeActive = !!item.badgeActive;
    var clickFunction = item.onClick ? item.onClick : function () {};
    var showTooltip = typeof item.tooltip !== "undefined" ? item.tooltip : true;
    var itemColor = item.color ? item.color : color;
    return /*#__PURE__*/React.createElement("div", null, !badgeActive && showTooltip && /*#__PURE__*/React.createElement(Tooltip, {
      title: name
    }, /*#__PURE__*/React.createElement(IconButton, {
      onClick: clickFunction,
      color: itemColor,
      icon: /*#__PURE__*/React.createElement(Icon, {
        icon: icon
      }),
      active: isSelected
    })), !badgeActive && !showTooltip && /*#__PURE__*/React.createElement(IconButton, {
      onClick: clickFunction,
      color: itemColor,
      icon: /*#__PURE__*/React.createElement(Icon, {
        icon: icon
      }),
      active: isSelected
    }), badgeActive && showTooltip && /*#__PURE__*/React.createElement(Tooltip, {
      title: name
    }, /*#__PURE__*/React.createElement(IconButton, {
      onClick: clickFunction,
      color: itemColor,
      active: isSelected,
      icon: /*#__PURE__*/React.createElement(Badge, {
        variant: "dot",
        invisible: false,
        color: "danger"
      }, /*#__PURE__*/React.createElement(Icon, {
        icon: icon
      }))
    })), badgeActive && !showTooltip && /*#__PURE__*/React.createElement(IconButton, {
      onClick: clickFunction,
      color: itemColor,
      active: isSelected,
      icon: /*#__PURE__*/React.createElement(Badge, {
        variant: "dot",
        invisible: false,
        color: "danger"
      }, /*#__PURE__*/React.createElement(Icon, {
        icon: icon
      }))
    }));
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, items.length > 0 && /*#__PURE__*/React.createElement("div", {
    id: "footer-actions",
    className: classes.root
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    className: isSidebarCollapsed ? classes.actionsList : ""
  }, items.map(function (action) {
    return /*#__PURE__*/React.createElement(Grid, {
      item: true,
      className: isSidebarCollapsed ? classes.iconButtonVertical : "",
      key: action.name
    }, printItem(action));
  }))));
};

FooterActions.propTypes = {
  items: PropTypes.instanceOf(Array),
  color: PropTypes.string,
  isSidebarCollapsed: PropTypes.bool
};
FooterActions.defaultProps = {
  items: [],
  color: "primary",
  isSidebarCollapsed: false
};
export default FooterActions;