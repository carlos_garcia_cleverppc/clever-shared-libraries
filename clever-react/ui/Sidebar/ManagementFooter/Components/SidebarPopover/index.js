import React from "react";
import ReactDOM from "react-dom";
import classnames from "classnames";
import PropTypes from "prop-types";
import { getColorsLabels, getShades } from "../../../../styles/colors";
import getClasses from "./styles";
var colorsList = getColorsLabels(true);
var shadesList = getShades();

var Popover = function Popover(props) {
  var children = props.children,
      onClose = props.onClose,
      anchorEl = props.anchorEl,
      open = props.open,
      arrow = props.arrow,
      width = props.width,
      zIndex = props.zIndex;
  var placement = props.placement,
      arrowColor = props.arrowColor,
      arrowShade = props.arrowShade,
      className = props.className;
  var classes = getClasses({
    anchorEl: anchorEl,
    open: open,
    placement: placement,
    paperWidth: width,
    arrowColor: arrowColor,
    arrowShade: arrowShade,
    zIndex: zIndex
  });
  return open && /*#__PURE__*/ReactDOM.createPortal( /*#__PURE__*/React.createElement(React.Fragment, null, open && /*#__PURE__*/React.createElement("div", {
    className: classes.portal,
    role: "presentation",
    onClick: onClose
  }), open && /*#__PURE__*/React.createElement("div", {
    className: arrow ? classnames(classes.popoverContainer, className.arrow ? className.arrow : "") : ""
  }, /*#__PURE__*/React.createElement("div", {
    className: classnames(classes.popover, className.popover ? className.popover : "")
  }, children))), document.body);
};

Popover.propTypes = {
  onClose: PropTypes.func.isRequired,
  arrow: PropTypes.bool,
  anchorEl: PropTypes.instanceOf(Object),
  open: PropTypes.bool.isRequired,
  children: PropTypes.instanceOf(Object).isRequired,
  width: PropTypes.number,
  zIndex: PropTypes.number,
  arrowColor: PropTypes.oneOf(colorsList),
  arrowShade: PropTypes.oneOf(shadesList),
  lockBodyScroll: PropTypes.bool,
  className: PropTypes.instanceOf(Object),
  placement: PropTypes.oneOf(["top-left", "top-center", "top-right", "bottom-left", "bottom-center", "bottom-right", "center-left", "center-right"])
};
Popover.defaultProps = {
  arrow: true,
  width: 375,
  placement: "bottom-center",
  arrowColor: "white",
  arrowShade: 500,
  lockBodyScroll: false,
  className: {},
  zIndex: 1000
};
export default Popover;