import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import React, { useEffect } from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import IconButton from "../../../IconButton";
import Popover from "../../../Popover";
import RoundedProgressBar from "../../../RoundedProgressBar";
import Typography from "../../../Typography";
import Link from "../../../Link";
import ArrowRight from "../../../Icon/ArrowRight";
import Times from "../../../Icon/Times";
import Warning from "../../../Icon/Warning";
import { getColorsLabels } from "../../../styles/colors";
import makeStyles from "../../../styles/makeStyles";
var colorsList = getColorsLabels(true);

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      spacing = _ref$cleverUI.spacing,
      colors = _ref$cleverUI.colors;
  return {
    spaceRight: _objectSpread({}, spacing["mr-2"]),
    avatarWrapper: {
      height: "42px",
      width: "42px"
    },
    opacity: {
      opacity: "30%"
    },
    popover: {
      zIndex: "1400 !important",
      width: "410px",
      height: "90px"
    },
    link: _objectSpread(_objectSpread(_objectSpread({}, spacing["py-3"]), spacing["px-3"]), {}, {
      display: "flex"
    }),
    copyLink: {
      display: "inline-flex",
      alignItems: "center"
    },
    icon: _objectSpread({}, spacing["ml-2"]),
    clickable: {
      cursor: "pointer"
    },
    warning: {
      top: "4px",
      zIndex: 2,
      position: "absolute",
      backgroundColor: colors.secondary[500],
      borderRadius: "7px"
    },
    popoverTransition: {
      transition: "all 300ms cubic-bezier(.4,1.39,.93,1) !important"
    }
  };
};

var getClasses = makeStyles(styles);

var ProgressAvatar = function ProgressAvatar(props) {
  var children = props.children,
      onClickProgress = props.onClickProgress,
      color = props.color,
      percentageDone = props.percentageDone,
      percentageCopy = props.percentageCopy;
  var classes = getClasses();

  var _React$useState = React.useState(false),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      open = _React$useState2[0],
      setOpen = _React$useState2[1];

  var _React$useState3 = React.useState(null),
      _React$useState4 = _slicedToArray(_React$useState3, 2),
      anchorEl = _React$useState4[0],
      setAnchorEl = _React$useState4[1];

  var _React$useState5 = React.useState(false),
      _React$useState6 = _slicedToArray(_React$useState5, 2),
      manuallyClosed = _React$useState6[0],
      setManuallyClosed = _React$useState6[1];

  useEffect(function () {
    if (open) {
      document.getElementById("popover-progress-avatar").firstChild.style.display = "none";
    }
  }, [open]);

  var openPopover = function openPopover() {
    setManuallyClosed(false);

    if (percentageDone < 100) {
      setAnchorEl(document.getElementById("progress-avatar"));
      setOpen(!!anchorEl);
    }
  };

  var closePopover = function closePopover() {
    setManuallyClosed(true);
    setAnchorEl(null);
    setOpen(false);
  };

  React.useEffect(function () {
    if (!manuallyClosed) {
      openPopover();
    }
  });
  return /*#__PURE__*/React.createElement(React.Fragment, null, !open && percentageDone < 100 && /*#__PURE__*/React.createElement(Warning, {
    color: "white",
    className: classes.warning,
    size: 14
  }), /*#__PURE__*/React.createElement("div", {
    onClick: openPopover,
    onKeyDown: openPopover,
    role: "button",
    tabIndex: 0,
    className: percentageDone < 100 ? classes.clickable : ""
  }, /*#__PURE__*/React.createElement(RoundedProgressBar, {
    size: 42,
    progress: percentageDone,
    strokeWidth: 4,
    color: color,
    className: classes.spaceRight,
    id: "progress-avatar",
    "aria-describedby": "progress-avatar"
  }, /*#__PURE__*/React.createElement("div", {
    className: classnames(classes.avatarWrapper, percentageDone < 100 ? classes.opacity : "")
  }, children))), percentageDone < 100 && /*#__PURE__*/React.createElement(Popover, {
    arrow: true,
    arrowPosition: "center",
    id: "popover-progress-avatar",
    open: open,
    anchorEl: anchorEl,
    placement: "top-center",
    classNameRoot: classes.popover,
    className: classes.popoverTransition,
    backgroundColor: "secondary",
    arrowColor: "secondary",
    arrowSize: 8,
    disableScrollLock: true,
    disablePortal: true,
    onClose: closePopover
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.link
  }, /*#__PURE__*/React.createElement(IconButton, {
    className: classes.iconTimes,
    color: "white",
    icon: /*#__PURE__*/React.createElement(Times, null),
    iconSize: 12,
    onClick: closePopover,
    active: true
  }), /*#__PURE__*/React.createElement(Link, {
    color: "white",
    gutterBottom: true,
    onClick: onClickProgress
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.copyLink
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "f2-12",
    color: "white"
  }, percentageCopy), /*#__PURE__*/React.createElement(ArrowRight, {
    color: "white",
    size: "12",
    className: classes.icon
  }))))));
};

ProgressAvatar.propTypes = {
  onClickProgress: PropTypes.func,
  color: PropTypes.oneOf(colorsList),
  percentageDone: PropTypes.number,
  percentageCopy: PropTypes.string
};
ProgressAvatar.defaultProps = {
  onClickProgress: function onClickProgress() {},
  color: "secondary",
  percentageDone: 0,
  percentageCopy: ""
};
export default ProgressAvatar;