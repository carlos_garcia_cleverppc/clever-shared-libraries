import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      spacing = _ref$cleverUI.spacing,
      shadows = _ref$cleverUI.shadows;
  return {
    mainContainer: _objectSpread(_objectSpread(_objectSpread({}, spacing["p-6"]), spacing["pb-4"]), {}, {
      backgroundColor: "white",
      borderRadius: "8px",
      boxShadow: shadows.normal,
      maxWidth: "375px"
    }),
    noAccounts: _objectSpread({}, spacing["my-4"]),
    newAccount: _objectSpread(_objectSpread({}, spacing["mt-2"]), spacing["mb-4"]),
    title: _objectSpread({}, spacing["my-2"]),
    accountList: {
      overflowY: "auto",
      maxHeight: "50vh",
      "&::-webkit-scrollbar": {
        width: "5px"
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#babac0",
        borderRadius: "16px",
        borderLeft: "1px solid #fff"
      }
    },
    listElement: _objectSpread(_objectSpread({}, spacing["mt-1"]), {}, {
      cursor: "pointer"
    }),
    addAccountContainer: {
      cursor: "pointer",
      display: "flex",
      alignItems: "center",
      justifyContent: "center"
    },
    addImage: _objectSpread({}, spacing["mr-2"]),
    divider: _objectSpread(_objectSpread({}, spacing["pl-2"]), spacing["pr-2"]),
    signOut: _objectSpread(_objectSpread(_objectSpread({
      textAlign: "right"
    }, spacing["pl-2"]), spacing["pr-2"]), spacing["my-2"]),
    accountInfo: _objectSpread({}, spacing["ml-3"]),
    name: _objectSpread(_objectSpread({}, spacing["mr-2"]), {}, {
      maxWidth: "150px",
      overflow: "hidden",
      textOverflow: "ellipsis",
      whiteSpace: "nowrap"
    })
  };
};

export default makeStyles(styles);