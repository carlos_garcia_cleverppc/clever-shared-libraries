/* eslint-disable jsx-a11y/anchor-is-valid */

/* eslint-disable import/no-cycle */
import React from "react";
import PropTypes from "prop-types";
import Link from "../../Link";
import PlusIcon from "../../Icon/PlusRound";
import Divider from "../../Divider";
import Grid from "../../Grid";
import List from "../../List";
import ListItem from "../../List/Item";
import Popover from "../../Popover";
import Avatar from "../../Avatar";
import Typography from "../../Typography";
import getClasses from "./styles";

var UserAccountsSelector = function UserAccountsSelector(props) {
  var allAccounts = props.allAccounts,
      handleOnClickItem = props.handleOnClickItem,
      handleOnClickLogOut = props.handleOnClickLogOut,
      handleAddAccount = props.handleAddAccount;
  var titleCopy = props.titleCopy,
      noAccountsCopy = props.noAccountsCopy,
      addAccountCopy = props.addAccountCopy,
      logOutCopy = props.logOutCopy;
  var open = props.open,
      setOpen = props.setOpen,
      anchorEl = props.anchorEl,
      setAnchorEl = props.setAnchorEl;
  var classes = getClasses();

  var printAccount = function printAccount(item) {
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Avatar, {
      src: item.src,
      size: 42
    }), /*#__PURE__*/React.createElement("div", {
      className: classes.accountInfo
    }, /*#__PURE__*/React.createElement(Grid, {
      container: true,
      alignItems: "center"
    }, /*#__PURE__*/React.createElement(Grid, {
      item: true
    }, /*#__PURE__*/React.createElement(Typography, {
      weight: "bold",
      variant: "f1-14",
      className: classes.name
    }, item.name)), !!item.accountId && /*#__PURE__*/React.createElement(Grid, {
      item: true
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "body0"
    }, "(", item.accountId, ")")))));
  };

  return /*#__PURE__*/React.createElement(Popover, {
    placement: "top-center",
    open: open,
    anchorEl: anchorEl,
    className: classes.accountsPopper,
    onClose: function onClose() {
      setAnchorEl(null);
      setOpen(false);
    },
    id: "accounts-popper",
    arrow: true,
    backgroundColor: "white"
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    className: classes.mainContainer,
    justify: "center"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    className: classes.title
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body0",
    weight: "bold",
    shade: 900
  }, titleCopy)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, allAccounts.length > 1 && /*#__PURE__*/React.createElement(List, {
    className: classes.accountList
  }, allAccounts.filter(function (item) {
    return !item.isSelected;
  }).map(function (account) {
    return /*#__PURE__*/React.createElement(ListItem, {
      onClick: function onClick() {
        return handleOnClickItem(account.accountId);
      },
      className: classes.listElement,
      key: account.name
    }, printAccount(account));
  })), allAccounts.length === 1 && /*#__PURE__*/React.createElement(Typography, {
    variant: "body0",
    align: "center",
    color: "neutral",
    className: classes.noAccounts
  }, noAccountsCopy)), addAccountCopy !== "" && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    className: classes.newAccount
  }, /*#__PURE__*/React.createElement("div", {
    onClick: handleAddAccount,
    onKeyDown: handleAddAccount,
    role: "button",
    tabIndex: 0,
    className: classes.addAccountContainer
  }, /*#__PURE__*/React.createElement(PlusIcon, {
    className: classes.addImage,
    color: "blue",
    size: 32
  }), /*#__PURE__*/React.createElement(Typography, {
    variant: "body0",
    weight: "bold",
    align: "center",
    color: "blue"
  }, addAccountCopy))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    className: classes.divider,
    xs: 12
  }, /*#__PURE__*/React.createElement(Divider, null)), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    className: classes.signOut,
    xs: 12
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    justify: "flex-end"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true
  }, /*#__PURE__*/React.createElement(Link, {
    href: "#",
    onClick: handleOnClickLogOut,
    color: "danger"
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "body0",
    weight: "bold",
    color: "danger"
  }, logOutCopy)))))));
};

UserAccountsSelector.propTypes = {
  allAccounts: PropTypes.instanceOf(Array),
  handleOnClickItem: PropTypes.instanceOf(Object),
  handleOnClickLogOut: PropTypes.instanceOf(Object),
  handleAddAccount: PropTypes.instanceOf(Object),
  titleCopy: PropTypes.string,
  noAccountsCopy: PropTypes.string,
  addAccountCopy: PropTypes.string,
  logOutCopy: PropTypes.string,
  open: PropTypes.bool,
  setOpen: PropTypes.instanceOf(Object),
  anchorEl: PropTypes.instanceOf(Object),
  setAnchorEl: PropTypes.instanceOf(Object)
};
UserAccountsSelector.defaultProps = {
  allAccounts: [],
  handleOnClickItem: function handleOnClickItem() {},
  handleOnClickLogOut: function handleOnClickLogOut() {},
  handleAddAccount: function handleAddAccount() {},
  titleCopy: "",
  noAccountsCopy: "",
  addAccountCopy: "",
  logOutCopy: "",
  open: false,
  setOpen: function setOpen() {},
  anchorEl: null,
  setAnchorEl: function setAnchorEl() {}
};
export default UserAccountsSelector;