import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../../styles/makeStyles";

var styles = function styles(_ref) {
  var spacing = _ref.cleverUI.spacing;
  return {
    root: {
      backgroundColor: "white",
      height: "100%",
      width: "100%"
    },
    rootCollapsed: {
      width: "160px"
    },
    platformLogo: _objectSpread(_objectSpread({}, spacing["pl-7"]), {}, {
      height: "200px",
      display: "flex",
      alignItems: "center"
    }),
    body: function body(_ref2) {
      var bodyHeight = _ref2.bodyHeight;
      return _objectSpread(_objectSpread({}, spacing["pl-7"]), {}, {
        height: bodyHeight,
        "&::-webkit-scrollbar": {
          width: "5px"
        },
        "&::-webkit-scrollbar-thumb": {
          backgroundColor: "#babac0",
          borderRadius: "16px",
          borderLeft: "1px solid #fff"
        }
      });
    },
    bodyDesktop: _objectSpread({}, spacing["pr-7"]),
    footer: _objectSpread(_objectSpread(_objectSpread({}, spacing["px-7"]), spacing["pb-5"]), spacing["pt-3"]),
    footerCollapsed: _objectSpread(_objectSpread(_objectSpread({}, spacing["pl-7"]), spacing["pb-5"]), spacing["pt-3"])
  };
};

export default makeStyles(styles);