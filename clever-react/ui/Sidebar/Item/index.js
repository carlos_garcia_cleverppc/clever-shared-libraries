import _defineProperty from "@babel/runtime/helpers/defineProperty";
import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Icon from "../../Icon";
import IconButton from "../../IconButton";
import Tooltip from "../../Tooltip";
import Typography from "../../Typography";
import ListItem from "../../List/Item";
import ChevronRight from "../../Icon/ChevronRight";
import Dropdown from "../Dropdown";
import getClasses from "./styles";

var SidebarItem = function SidebarItem(props) {
  var _classnames;

  var name = props.name,
      icon = props.icon,
      color = props.color,
      className = props.className,
      onClick = props.onClick,
      isSelected = props.isSelected;
  var childrenItems = props.childrenItems,
      isChild = props.isChild,
      isSidebarCollapsed = props.isSidebarCollapsed,
      label = props.label,
      openList = props.openList;
  /* DropdownState: openList can come from the project or if isSelected */

  var _useState = useState(isSelected || openList),
      _useState2 = _slicedToArray(_useState, 2),
      open = _useState2[0],
      setOpen = _useState2[1];

  var hasChildren = childrenItems.length > 0;
  var classes = getClasses({
    color: color
  });

  var handleOnClick = function handleOnClick() {
    if (hasChildren) setOpen(!open);
    onClick();
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(ListItem, {
    component: isSidebarCollapsed ? "li" : "button",
    key: name,
    onClick: handleOnClick,
    hoverSelect: !isSidebarCollapsed,
    className: classnames((_classnames = {}, _defineProperty(_classnames, classes.selectItemCollapsed, isSidebarCollapsed), _defineProperty(_classnames, classes.selectItemHover, !isSidebarCollapsed), _defineProperty(_classnames, classes.selectedSection, isSelected && !isChild), _defineProperty(_classnames, classes.selectItem, true), _defineProperty(_classnames, className, true), _classnames))
  }, !!isSidebarCollapsed && /*#__PURE__*/React.createElement(Tooltip, {
    title: name
  }, /*#__PURE__*/React.createElement(IconButton, {
    icon: /*#__PURE__*/React.createElement(Icon, {
      icon: icon
    }),
    color: isSelected ? color : "neutral",
    className: classes.iconButton
  })), !isSidebarCollapsed && /*#__PURE__*/React.createElement("div", {
    className: isChild ? classes.iconOpenChild : classes.iconOpen
  }, /*#__PURE__*/React.createElement(Icon, {
    className: classes.iconSize,
    icon: icon,
    color: isSelected ? color : "neutral",
    shade: isSelected ? 500 : 400
  })), !isSidebarCollapsed && /*#__PURE__*/React.createElement(Typography, {
    variant: isChild ? "f2-14" : "f1-18",
    weight: "bold",
    shade: isSelected ? 700 : 400
  }, name), label !== "" && /*#__PURE__*/React.createElement(Typography, {
    color: color,
    weight: "bold",
    variant: "f1-8",
    className: classes.label
  }, label), hasChildren && /*#__PURE__*/React.createElement(ChevronRight, {
    className: open ? classnames(classes.arrow, classes.arrowPosition) : classes.arrow
  })), hasChildren && open && /*#__PURE__*/React.createElement(Dropdown, {
    itemsList: childrenItems,
    isSidebarCollapsed: isSidebarCollapsed
  }));
};

SidebarItem.propTypes = {
  name: PropTypes.string,
  className: PropTypes.string,
  icon: PropTypes.string,
  isSelected: PropTypes.bool,
  color: PropTypes.string,
  childrenItems: PropTypes.instanceOf(Array),
  isChild: PropTypes.bool,
  onClick: PropTypes.func,
  isSidebarCollapsed: PropTypes.bool,
  label: PropTypes.string,
  openList: PropTypes.bool
};
SidebarItem.defaultProps = {
  name: "",
  className: "",
  icon: "",
  color: "primary",
  isSelected: false,
  childrenItems: [],
  isChild: false,
  onClick: function onClick() {},
  isSidebarCollapsed: false,
  label: "",
  openList: false
};
export default SidebarItem;