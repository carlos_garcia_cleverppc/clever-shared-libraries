import _extends from "@babel/runtime/helpers/extends";
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import makeStyles from "../styles/makeStyles";
import { iconSizes } from "../styles/icons";
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});

function TwitterCircle(props) {
  var classes = getClasses(props);
  var size = props.size,
      padding = props.padding,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var sizeIcon = size;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return (
    /*#__PURE__*/
    // eslint-disable-next-line react/jsx-props-no-spreading
    React.createElement("svg", _extends({
      viewBox: "0 0 16 16",
      fill: "none",
      height: sizeIcon,
      width: sizeIcon
    }, otherProps), /*#__PURE__*/React.createElement("path", {
      d: "M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16Z",
      fill: "#CAEDFD"
    }), /*#__PURE__*/React.createElement("path", {
      d: "M13.12 4.82496C12.7392 4.992 12.3334 5.10272 11.9104 5.15648C12.3456 4.89664 12.6778 4.48832 12.8339 3.99616C12.4282 4.23808 11.9802 4.40896 11.5027 4.50432C11.1174 4.09408 10.5683 3.84 9.96929 3.84C8.80704 3.84 7.87137 4.78336 7.87137 5.93984C7.87137 6.10624 7.88544 6.26624 7.92 6.41856C6.17472 6.33344 4.6304 5.49696 3.59296 4.22272C3.41184 4.53696 3.3056 4.89664 3.3056 5.28384C3.3056 6.01088 3.68 6.65536 4.23808 7.02848C3.9008 7.02208 3.56992 6.92416 3.2896 6.76992C3.2896 6.77632 3.2896 6.78464 3.2896 6.79296C3.2896 7.81312 4.01728 8.66048 4.97152 8.85568C4.80064 8.9024 4.6144 8.9248 4.42112 8.9248C4.28672 8.9248 4.15104 8.91712 4.02368 8.88896C4.29568 9.72032 5.06752 10.3315 5.98528 10.3514C5.27104 10.9101 4.36416 11.2467 3.3824 11.2467C3.21024 11.2467 3.04512 11.239 2.88 11.2179C3.80992 11.8176 4.912 12.16 6.10048 12.16C9.96352 12.16 12.0755 8.96 12.0755 6.18624C12.0755 6.09344 12.0723 6.00384 12.0678 5.91488C12.4845 5.6192 12.8346 5.24992 13.12 4.82496Z",
      fill: "#03A9F4"
    }))
  );
}

TwitterCircle.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(iconSizes),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  padding: PropTypes.number
};
TwitterCircle.defaultProps = {
  size: 24,
  className: "",
  rotation: 0,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default TwitterCircle;