import _extends from "@babel/runtime/helpers/extends";

/* eslint-disable max-len */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import makeStyles from "../styles/makeStyles";
import { iconSizes } from "../styles/icons";
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});

function FacebookMessengerIcon(props) {
  var classes = getClasses(props);
  var size = props.size,
      padding = props.padding,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var sizeIcon = size;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return (
    /*#__PURE__*/
    // eslint-disable-next-line react/jsx-props-no-spreading
    React.createElement("svg", _extends({
      width: sizeIcon,
      height: sizeIcon,
      viewBox: "0 0 16 16",
      fill: "none"
    }, otherProps), /*#__PURE__*/React.createElement("path", {
      d: "M8 1C4.05725 1 1 3.88899 1 7.7894C1 9.82972 1.8365 11.5936 3.198 12.8115C3.31175 12.9129 3.38175 13.0564 3.38525 13.2104L3.42375 14.4563C3.436 14.8535 3.8455 15.1125 4.2095 14.9515L5.599 14.3391C5.71625 14.2866 5.84925 14.2778 5.9735 14.3111C6.61225 14.4861 7.29125 14.5805 8 14.5805C11.9427 14.5805 15 11.6916 15 7.79115C15 3.89074 11.9427 1 8 1Z",
      fill: "url(#paint0_radial_1008:8)"
    }), /*#__PURE__*/React.createElement("path", {
      d: "M3.91834 9.72907L5.99259 6.55354C6.3227 6.04757 7.02882 5.9232 7.52488 6.28096L9.17545 7.47519C9.32726 7.58422 9.53557 7.58422 9.68562 7.47349L11.9134 5.84143C12.21 5.62337 12.5984 5.9675 12.4007 6.27244L10.3247 9.44627C9.99455 9.95224 9.28843 10.0766 8.79237 9.71884L7.14181 8.52461C6.98999 8.41558 6.78168 8.41558 6.63163 8.52632L4.4038 10.1584C4.10723 10.3764 3.71886 10.034 3.91834 9.72907Z",
      fill: "white"
    }), /*#__PURE__*/React.createElement("defs", null, /*#__PURE__*/React.createElement("radialGradient", {
      id: "paint0_radial_1008:8",
      cx: "0",
      cy: "0",
      r: "1",
      gradientUnits: "userSpaceOnUse",
      gradientTransform: "translate(3.695 14.9211) scale(15.26)"
    }, /*#__PURE__*/React.createElement("stop", {
      stopColor: "#0099FF"
    }), /*#__PURE__*/React.createElement("stop", {
      offset: "0.61",
      stopColor: "#A033FF"
    }), /*#__PURE__*/React.createElement("stop", {
      offset: "0.935",
      stopColor: "#FF5280"
    }), /*#__PURE__*/React.createElement("stop", {
      offset: "1",
      stopColor: "#FF7061"
    }))))
  );
}

FacebookMessengerIcon.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(iconSizes),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  padding: PropTypes.number
};
FacebookMessengerIcon.defaultProps = {
  size: 24,
  className: "",
  rotation: 0,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default FacebookMessengerIcon;