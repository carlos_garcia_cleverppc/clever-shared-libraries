import _extends from "@babel/runtime/helpers/extends";
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import makeStyles from "../styles/makeStyles";
import { iconSizes } from "../styles/icons";
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});

function InstagramCircle(props) {
  var classes = getClasses(props);
  var size = props.size,
      padding = props.padding,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var sizeIcon = size;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return (
    /*#__PURE__*/
    // eslint-disable-next-line react/jsx-props-no-spreading
    React.createElement("svg", _extends({
      viewBox: "0 0 16 16",
      fill: "none",
      height: sizeIcon,
      width: sizeIcon
    }, otherProps), /*#__PURE__*/React.createElement("path", {
      d: "M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16Z",
      fill: "#F5DFEE"
    }), /*#__PURE__*/React.createElement("path", {
      d: "M12.1389 6.28611C12.1194 5.84403 12.0479 5.54011 11.9454 5.27676C11.8398 4.99721 11.6772 4.74694 11.4642 4.53888C11.2561 4.32759 11.0041 4.16341 10.7278 4.05941C10.4629 3.95701 10.1605 3.88552 9.71831 3.86603C9.27285 3.84488 9.13143 3.84 8.00162 3.84C6.8718 3.84 6.73038 3.84488 6.28657 3.86438C5.84441 3.88387 5.54043 3.95542 5.27709 4.05776C4.99743 4.16341 4.74711 4.32594 4.53902 4.53888C4.32769 4.74694 4.16354 4.99886 4.05946 5.27517C3.95703 5.54011 3.88553 5.84238 3.86603 6.28446C3.84489 6.72983 3.84 6.87122 3.84 8.00082C3.84 9.13042 3.84489 9.27181 3.86438 9.71554C3.88388 10.1576 3.95544 10.4615 4.05787 10.7249C4.16354 11.0044 4.32769 11.2547 4.53902 11.4628C4.74711 11.674 4.99908 11.8382 5.27544 11.9422C5.54043 12.0446 5.84276 12.1161 6.28499 12.1356C6.72873 12.1552 6.87021 12.16 8.00003 12.16C9.12984 12.16 9.27126 12.1552 9.71507 12.1356C10.1572 12.1161 10.4612 12.0446 10.7246 11.9422C11.2838 11.726 11.726 11.284 11.9422 10.7249C12.0446 10.4599 12.1161 10.1576 12.1356 9.71554C12.1551 9.27181 12.16 9.13042 12.16 8.00082C12.16 6.87122 12.1583 6.72983 12.1389 6.28611ZM11.3895 9.68303C11.3716 10.0894 11.3033 10.3088 11.2464 10.4551C11.1066 10.8175 10.8188 11.1052 10.4563 11.245C10.31 11.3019 10.089 11.3701 9.68415 11.388C9.24522 11.4075 9.11359 11.4123 8.00327 11.4123C6.89295 11.4123 6.75966 11.4075 6.32232 11.388C5.91592 11.3701 5.69646 11.3019 5.55015 11.245C5.36974 11.1783 5.20553 11.0727 5.07224 10.9345C4.93406 10.7996 4.82839 10.6371 4.76172 10.4567C4.70482 10.3104 4.63656 10.0894 4.61871 9.68468C4.59915 9.24584 4.59433 9.11417 4.59433 8.00406C4.59433 6.89395 4.59915 6.76069 4.61871 6.3235C4.63656 5.91717 4.70482 5.69775 4.76172 5.55147C4.82839 5.37104 4.93406 5.20692 5.07389 5.07359C5.20877 4.93544 5.37133 4.82979 5.5518 4.76319C5.69811 4.70631 5.91922 4.63805 6.32398 4.62015C6.7629 4.60066 6.8946 4.59577 8.00485 4.59577C9.11682 4.59577 9.24846 4.60066 9.6858 4.62015C10.0922 4.63805 10.3117 4.70631 10.458 4.76319C10.6384 4.82979 10.8026 4.93544 10.9359 5.07359C11.0741 5.20851 11.1797 5.37104 11.2464 5.55147C11.3033 5.69775 11.3716 5.91876 11.3895 6.3235C11.409 6.76234 11.4139 6.89395 11.4139 8.00406C11.4139 9.11417 11.409 9.24419 11.3895 9.68303Z",
      fill: "#BC2A8D"
    }), /*#__PURE__*/React.createElement("path", {
      d: "M7.99999 5.76001C6.76335 5.76001 5.75999 6.7633 5.75999 8.00001C5.75999 9.23672 6.76335 10.24 7.99999 10.24C9.23671 10.24 10.24 9.23672 10.24 8.00001C10.24 6.7633 9.23671 5.76001 7.99999 5.76001ZM7.99999 9.45304C7.19772 9.45304 6.54696 8.80235 6.54696 8.00001C6.54696 7.19767 7.19772 6.54698 7.99999 6.54698C8.80233 6.54698 9.45303 7.19767 9.45303 8.00001C9.45303 8.80235 8.80233 9.45304 7.99999 9.45304Z",
      fill: "#BC2A8D"
    }), /*#__PURE__*/React.createElement("path", {
      d: "M10.56 5.92C10.56 6.18507 10.3451 6.4 10.08 6.4C9.81491 6.4 9.59999 6.18507 9.59999 5.92C9.59999 5.65487 9.81491 5.44 10.08 5.44C10.3451 5.44 10.56 5.65487 10.56 5.92Z",
      fill: "#BC2A8D"
    }))
  );
}

InstagramCircle.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(iconSizes),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  padding: PropTypes.number
};
InstagramCircle.defaultProps = {
  size: 24,
  className: "",
  rotation: 0,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default InstagramCircle;