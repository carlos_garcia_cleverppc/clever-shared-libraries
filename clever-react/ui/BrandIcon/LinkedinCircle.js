import _extends from "@babel/runtime/helpers/extends";
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import makeStyles from "../styles/makeStyles";
import { iconSizes } from "../styles/icons";
var getClasses = makeStyles(function () {
  return {
    root: function root(_ref) {
      var rotation = _ref.rotation,
          padding = _ref.padding,
          size = _ref.size;
      var res = {};

      if (rotation > 0) {
        res.transform = "rotate(".concat(rotation, "deg)");
      }

      if (padding > 0) {
        res.padding = "".concat(size * padding / 200, "px");
      }

      return res;
    }
  };
});

function FacebookCircle(props) {
  var classes = getClasses(props);
  var size = props.size,
      padding = props.padding,
      className = props.className,
      onMouseEnter = props.onMouseEnter,
      onMouseLeave = props.onMouseLeave;
  var sizeIcon = size;
  var otherProps = {
    className: classnames(className, classes.root),
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  };

  if (padding) {
    sizeIcon = sizeIcon * (100 - padding) / 100;
  }

  return (
    /*#__PURE__*/
    // eslint-disable-next-line react/jsx-props-no-spreading
    React.createElement("svg", _extends({
      viewBox: "0 0 16 16",
      fill: "none",
      height: sizeIcon,
      width: sizeIcon
    }, otherProps), /*#__PURE__*/React.createElement("path", {
      d: "M8 16C12.4183 16 16 12.4183 16 8C16 3.58172 12.4183 0 8 0C3.58172 0 0 3.58172 0 8C0 12.4183 3.58172 16 8 16Z",
      fill: "#E9E9E9"
    }), /*#__PURE__*/React.createElement("path", {
      d: "M12.205 3.00007H3.735C3.54271 2.99747 3.35724 3.0713 3.2194 3.20539C3.08156 3.33948 3.00263 3.52285 3 3.71514V12.2851C3.00263 12.4774 3.08156 12.6608 3.2194 12.7949C3.35724 12.929 3.54271 13.0028 3.735 13.0002H12.205C12.3973 13.0028 12.5828 12.929 12.7206 12.7949C12.8584 12.6608 12.9374 12.4774 12.94 12.2851V3.71514C12.9374 3.52285 12.8584 3.33948 12.7206 3.20539C12.5828 3.0713 12.3973 2.99747 12.205 3.00007ZM6.015 11.3701H4.515V6.87014H6.015V11.3701ZM5.265 6.24014C4.83422 6.24014 4.485 5.89092 4.485 5.46014C4.485 5.02935 4.83422 4.68014 5.265 4.68014C5.56476 4.64614 5.85747 4.78693 6.01804 5.04233C6.1786 5.29773 6.1786 5.62254 6.01804 5.87794C5.85747 6.13334 5.56476 6.27413 5.265 6.24014ZM11.425 11.3701H9.925V8.95514C9.925 8.35014 9.71 7.95514 9.165 7.95514C8.81954 7.95767 8.51224 8.17517 8.395 8.50014C8.35587 8.61766 8.33892 8.74142 8.345 8.86514V11.3651H6.845C6.845 11.3651 6.845 7.27514 6.845 6.86514H8.345V7.50014C8.6227 7.01827 9.14422 6.72961 9.7 6.75014C10.7 6.75014 11.425 7.39514 11.425 8.78014V11.3701Z",
      fill: "#0D67C2"
    }))
  );
}

FacebookCircle.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(iconSizes),
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  rotation: PropTypes.number,
  padding: PropTypes.number
};
FacebookCircle.defaultProps = {
  size: 24,
  className: "",
  rotation: 0,
  padding: 0,
  onMouseEnter: function onMouseEnter() {},
  onMouseLeave: function onMouseLeave() {}
};
export default FacebookCircle;