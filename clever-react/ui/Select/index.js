import _extends from "@babel/runtime/helpers/extends";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["label", "value", "name", "options", "printMenuItem", "size", "placeholder", "native"];
import React from "react";
import PropTypes from "prop-types";
import TextField from "../TextField";
import SelectItem from "./Item";
import getClasses from "./styles"; // eslint-disable-next-line no-unused-vars

function printMenuItemDefault(label, value, disabled, index) {
  return label;
}

var Select = function Select(props) {
  var label = props.label,
      value = props.value,
      name = props.name,
      options = props.options,
      printMenuItem = props.printMenuItem,
      size = props.size,
      placeholder = props.placeholder,
      _native = props["native"],
      selectProps = _objectWithoutProperties(props, _excluded);

  var classes = getClasses();
  return /*#__PURE__*/React.createElement(TextField, _extends({
    value: value,
    variant: "outlined",
    SelectProps: {
      classes: {
        icon: classes.selectIcon
      },
      MenuProps: {
        classes: {
          paper: classes.scroll
        }
      },
      "native": _native
    },
    select: true,
    label: !label ? "" : label.toString(),
    size: size
  }, selectProps), !!placeholder && value === "none" && /*#__PURE__*/React.createElement(SelectItem, {
    "native": _native,
    className: classes.menuItem,
    value: "none",
    disabled: true
  }, placeholder), options === null || options === void 0 ? void 0 : options.map(function (option, index) {
    return /*#__PURE__*/React.createElement(SelectItem, {
      "native": _native,
      className: classes.menuItem,
      key: "".concat(name, "-").concat(option.label),
      disabled: option.disabled || false,
      value: option.value
    }, printMenuItem(option.label, option.value, option.disabled || false, index));
  }));
};

Select.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
  options: PropTypes.instanceOf(Array).isRequired,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  printMenuItem: PropTypes.func,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
  size: PropTypes.oneOf(["small", "standard"]),
  startAdornment: PropTypes.instanceOf(Object),
  "native": PropTypes.bool
};
Select.defaultProps = {
  className: "",
  label: "",
  placeholder: "",
  printMenuItem: printMenuItemDefault,
  disabled: false,
  size: "standard",
  startAdornment: undefined,
  "native": false
};
export default Select;