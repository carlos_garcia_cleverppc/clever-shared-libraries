import _extends from "@babel/runtime/helpers/extends";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["native"];

/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import PropTypes from "prop-types";
import MenuItem from "@material-ui/core/MenuItem";
var SelectItem = /*#__PURE__*/React.forwardRef(function (_ref, ref) {
  var _native = _ref["native"],
      props = _objectWithoutProperties(_ref, _excluded);

  if (_native) {
    return /*#__PURE__*/React.createElement("option", props);
  }

  return /*#__PURE__*/React.createElement(MenuItem, _extends({}, props, {
    ref: ref
  }));
});
SelectItem.propTypes = {
  "native": PropTypes.bool
};
SelectItem.defaultProps = {
  "native": false
};
export default SelectItem;