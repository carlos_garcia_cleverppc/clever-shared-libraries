import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing,
      shadows = _ref$cleverUI.shadows;
  return {
    root: function root(_ref2) {
      var color = _ref2.color,
          shade = _ref2.shade;
      return {
        padding: "0.1px",
        backgroundColor: "".concat(colors[color][shade]),
        boxShadow: shadows.hover
      };
    },
    popper: function popper(_ref3) {
      var color = _ref3.color,
          shade = _ref3.shade;
      return {
        zIndex: 900,
        "&[x-placement*=\"bottom\"] $arrow": {
          width: 0,
          height: 0,
          borderLeft: "1em solid transparent",
          borderRight: "1em solid transparent",
          borderBottom: "1em solid ".concat(colors[color][shade]),
          marginTop: "-0.9em",
          "&:before": {
            borderWidth: "0 1em 1em 1em",
            borderColor: "transparent transparent white transparent"
          }
        },
        "&[x-placement*=\"top\"] $arrow": {
          bottom: 0,
          width: 0,
          height: 0,
          borderLeft: "1em solid transparent",
          borderRight: "1em solid transparent",
          borderTop: "1em solid ".concat(colors[color][shade]),
          marginBottom: "-0.9em",
          "&:before": {
            borderWidth: "1em 1em 0 1em",
            borderColor: "white transparent transparent transparent"
          }
        },
        "&[x-placement*=\"right\"] $arrow": {
          left: 0,
          width: 0,
          height: 0,
          borderTop: "1em solid transparent",
          borderBottom: "1em solid transparent",
          borderRight: "1em solid ".concat(colors[color][shade]),
          marginLeft: "-0.9em",
          "&:before": {
            borderWidth: "1em 1em 1em 0",
            borderColor: "transparent white transparent transparent"
          }
        },
        "&[x-placement*=\"left\"] $arrow": {
          right: 0,
          width: 0,
          height: 0,
          borderTop: "1em solid transparent",
          borderBottom: "1em solid transparent",
          borderLeft: "1em solid ".concat(colors[color][shade]),
          marginRight: "-0.9em",
          "&:before": {
            borderWidth: "1em 0 1em 1em",
            borderColor: "transparent transparent transparent white"
          }
        }
      };
    },
    arrow: function arrow() {
      return _objectSpread(_objectSpread({}, spacing["mt-2"]), {}, {
        position: "absolute",
        fontSize: "8px",
        width: "3em",
        height: "3em"
      });
    }
  };
};

export default makeStyles(styles);