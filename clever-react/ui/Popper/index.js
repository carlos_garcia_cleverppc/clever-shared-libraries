import _extends from "@babel/runtime/helpers/extends";
import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import MaterialPopper from "@material-ui/core/Popper";
import Fade from "@material-ui/core/Fade";
import { getColorsLabels, getShades } from "../styles/colors";
import getClasses from "./styles";
var colorsList = getColorsLabels();
var shadesList = getShades();

var Popper = function Popper(props) {
  var classes = getClasses(props);
  var children = props.children,
      open = props.open,
      id = props.id,
      anchorEl = props.anchorEl,
      transition = props.transition;
  var placement = props.placement,
      className = props.className,
      arrow = props.arrow,
      offset = props.offset;
  var preventOverflow = props.preventOverflow,
      flip = props.flip,
      handleClose = props.handleClose,
      disablePortal = props.disablePortal;

  var _useState = useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      arrowRef = _useState2[0],
      setArrowRef = _useState2[1];

  var handleArrowRef = function handleArrowRef(node) {
    setArrowRef(node);
  };

  return /*#__PURE__*/React.createElement(MaterialPopper, {
    className: classes.popper,
    placement: placement,
    id: id,
    open: open,
    anchorEl: anchorEl,
    transition: transition,
    modifiers: {
      offset: {
        enabled: !!offset,
        offset: offset
      },
      flip: {
        enabled: flip
      },
      preventOverflow: {
        enabled: preventOverflow,
        boundariesElement: preventOverflow
      },
      arrow: {
        enabled: arrow,
        element: arrowRef
      }
    },
    disablePortal: disablePortal
  }, function (_ref) {
    var TransitionProps = _ref.TransitionProps;
    return /*#__PURE__*/React.createElement(ClickAwayListener, {
      onClickAway: handleClose
    }, /*#__PURE__*/React.createElement(Fade, _extends({}, TransitionProps, {
      disableStrictModeCompat: true,
      timeout: 350
    }), /*#__PURE__*/React.createElement("div", {
      className: classnames(classes.root, className)
    }, arrow && /*#__PURE__*/React.createElement("span", {
      className: classes.arrow,
      ref: handleArrowRef
    }), children)));
  });
};

Popper.propTypes = {
  className: PropTypes.string,
  color: PropTypes.oneOf(colorsList),
  shade: PropTypes.oneOf(shadesList),
  placement: PropTypes.oneOf(["bottom-end", "bottom-start", "bottom", "left-end", "left-start", "left", "right-end", "right-start", "right", "top-end", "top-start", "top"]),
  arrow: PropTypes.bool,
  preventOverflow: PropTypes.string,
  flip: PropTypes.bool,
  handleClose: PropTypes.func,
  offset: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  disablePortal: PropTypes.bool
};
Popper.defaultProps = {
  className: "",
  color: "white",
  shade: 500,
  placement: "bottom",
  arrow: false,
  preventOverflow: "scrollParent",
  flip: true,
  handleClose: function handleClose() {},
  offset: false,
  disablePortal: false
};
export default Popper;