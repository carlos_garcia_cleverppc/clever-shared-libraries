import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import getClasses from "./styles";
var LoadingClever = /*#__PURE__*/React.forwardRef(function (_ref, ref) {
  var size = _ref.size,
      className = _ref.className;
  var classes = getClasses({
    size: size
  });
  return /*#__PURE__*/React.createElement("div", {
    ref: ref,
    className: cx(classes.root, className),
    role: "alert",
    "aria-label": "Loading"
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.inner
  }, /*#__PURE__*/React.createElement("div", null), /*#__PURE__*/React.createElement("div", null), /*#__PURE__*/React.createElement("div", null), /*#__PURE__*/React.createElement("div", null), /*#__PURE__*/React.createElement("div", null)));
});
export default LoadingClever;
LoadingClever.propTypes = {
  size: PropTypes.number
};
LoadingClever.defaultProps = {
  size: 200
};