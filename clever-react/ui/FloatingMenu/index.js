import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels } from "../styles/colors";
import Popper from "../Popper";
var colorsList = getColorsLabels(true);
var getClasses = makeStyles(function () {
  return {
    paper: {
      position: "fixed !important",
      transform: "translateX(-120px) translateY(-64px)",
      minWidth: "250px"
    }
  };
});

var FloatingMenu = function FloatingMenu(props) {
  var classes = getClasses();
  var className = props.className,
      children = props.children,
      color = props.color,
      anchorEl = props.anchorEl;
  return /*#__PURE__*/React.createElement(Popper, {
    placement: "top",
    open: !!anchorEl,
    anchorEl: anchorEl,
    transition: true,
    color: color,
    shade: 500,
    className: classnames(classes.paper, className)
  }, children);
};

FloatingMenu.propTypes = {
  className: PropTypes.string,
  color: PropTypes.oneOf(colorsList),
  anchorEl: PropTypes.instanceOf(Object)
};
FloatingMenu.defaultProps = {
  className: "",
  color: "neutral",
  anchorEl: null
};
export default FloatingMenu;