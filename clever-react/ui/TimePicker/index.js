import _extends from "@babel/runtime/helpers/extends";
import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import DateFnsUtils from "@date-io/date-fns";
import enLocale from "date-fns/locale/en-US";
import esLocale from "date-fns/locale/es";
import frLocale from "date-fns/locale/fr";
import { MuiPickersUtilsProvider, KeyboardTimePicker, TimePicker } from "@material-ui/pickers";
import ChevronDown from "../Icon/ChevronDown";
import getClasses from "./styles";

var CleverTimePicker = function CleverTimePicker(props) {
  var label = props.label,
      value = props.value,
      onChange = props.onChange,
      className = props.className,
      inputClass = props.inputClass,
      size = props.size,
      disablePast = props.disablePast,
      disableFuture = props.disableFuture,
      InputLabelProps = props.InputLabelProps,
      InputProps = props.InputProps,
      warning = props.warning,
      error = props.error,
      format = props.format,
      type = props.type,
      keyboardIcon = props.keyboardIcon,
      positionIcon = props.positionIcon,
      KeyboardButtonProps = props.KeyboardButtonProps,
      locale = props.locale;

  var getColor = function getColor() {
    if (warning) {
      return "warning";
    }

    if (error) {
      return "danger";
    }

    return "neutral";
  };

  var getLocale = function getLocale() {
    switch (locale) {
      case "es":
        return esLocale;

      case "fr":
        return frLocale;

      default:
        return enLocale;
    }
  };

  var getErrorMessages = function getErrorMessages() {
    switch (locale) {
      case "es":
        return {
          invalidDateMessage: "Formato de fecha inválido",
          maxDateMessage: "No se puede exceder la fecha máxima",
          minDateMessage: "No se puede preceder la fecha mínima"
        };

      case "fr":
        return {
          invalidDateMessage: "Format de date non valide",
          maxDateMessage: "La date maximale ne peut être dépassée",
          minDateMessage: "La date minimale ne peut être précédée"
        };

      default:
        return {
          invalidDateMessage: "Invalid date format",
          maxDateMessage: "Date shouldn’t exceed maximum date",
          minDateMessage: "Date shouldn’t be before minimum date"
        };
    }
  };

  var color = getColor();
  var classes = getClasses({
    color: color,
    size: size
  });

  if (type === "keyboard") {
    // Type: keyboard
    return /*#__PURE__*/React.createElement(MuiPickersUtilsProvider, {
      utils: DateFnsUtils,
      locale: getLocale()
    }, /*#__PURE__*/React.createElement(KeyboardTimePicker, _extends({
      autoOk: true,
      variant: "inline",
      ampm: false,
      label: label,
      disablePast: disablePast,
      disableFuture: disableFuture,
      value: value,
      disableToolbar: true,
      onChange: onChange,
      inputVariant: "outlined",
      className: className,
      keyboardIcon: keyboardIcon,
      format: format,
      InputAdornmentProps: {
        position: positionIcon
      },
      KeyboardButtonProps: _objectSpread({}, KeyboardButtonProps),
      InputProps: _objectSpread(_objectSpread({}, InputProps), {}, {
        classes: {
          input: classnames(classes.input, inputClass),
          root: classes.inputBase,
          notchedOutline: warning || error ? classes.notchedOutline : ""
        }
      }),
      InputLabelProps: _objectSpread(_objectSpread({}, InputLabelProps), {}, {
        classes: {
          root: warning || error ? classnames(classes.labelRoot, classes.notchedOutlineLabel) : classes.labelRoot,
          outlined: classes.labelOutlined,
          shrink: classes.shrink
        }
      })
    }, getErrorMessages())));
  }

  return (
    /*#__PURE__*/
    // Type: standard or select
    React.createElement(MuiPickersUtilsProvider, {
      utils: DateFnsUtils,
      locale: getLocale()
    }, /*#__PURE__*/React.createElement(TimePicker, _extends({
      autoOk: true,
      variant: "inline",
      ampm: false,
      label: label,
      disablePast: disablePast,
      disableFuture: disableFuture,
      value: value,
      onChange: onChange,
      inputVariant: "outlined",
      className: className,
      disableToolbar: true,
      format: format,
      InputProps: _objectSpread(_objectSpread(_objectSpread({}, InputProps), type === "select" && {
        startAdornment: keyboardIcon,
        endAdornment: /*#__PURE__*/React.createElement(ChevronDown, null)
      }), {}, {
        classes: {
          input: classnames(classes.input, _defineProperty({}, classes.selectInput, type === "select"), inputClass),
          root: type === "select" ? classes.selectInputBase : classes.inputBase,
          notchedOutline: warning || error ? classes.notchedOutline : ""
        }
      }),
      InputLabelProps: _objectSpread(_objectSpread({}, InputLabelProps), {}, {
        classes: {
          root: warning || error ? classnames(classes.labelRoot, classes.notchedOutlineLabel) : classes.labelRoot,
          outlined: classes.labelOutlined,
          shrink: classes.shrink
        }
      })
    }, getErrorMessages())))
  );
};

CleverTimePicker.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.instanceOf(Date)]),
  error: PropTypes.bool,
  warning: PropTypes.bool,
  disablePast: PropTypes.bool,
  disableFuture: PropTypes.bool,
  keyboardIcon: PropTypes.instanceOf(Object),
  positionIcon: PropTypes.oneOf(["start", "end"]),
  size: PropTypes.oneOf(["small", "standard"]),
  type: PropTypes.oneOf(["keyboard", "standard", "select"]),
  format: PropTypes.string,
  locale: PropTypes.oneOf(["en", "es", "fr"])
};
CleverTimePicker.defaultProps = {
  className: "",
  label: "",
  onChange: function onChange() {},
  value: "",
  error: false,
  warning: false,
  disablePast: false,
  disableFuture: false,
  keyboardIcon: undefined,
  positionIcon: "start",
  size: "standard",
  type: "standard",
  format: undefined,
  locale: "en"
};
export default CleverTimePicker;