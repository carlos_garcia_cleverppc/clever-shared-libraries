import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing,
      typography = _ref$cleverUI.typography;
  return {
    sameLine: {
      display: "inline-flex"
    },
    bottomSpace: _objectSpread({}, spacing["mb-3"]),
    alignBottom: {
      alignItems: "baseline"
    },
    alignCenter: {
      textAlign: "center"
    },
    branIcon: _objectSpread(_objectSpread({}, spacing["pr-2"]), {}, {
      height: "32px",
      width: "32px"
    }),
    progress: _objectSpread({}, spacing["mb-6"]),
    valueLabel: {
      top: "-20px",
      left: "-15px",
      boxShadow: "none",
      "& *": {
        fontSize: "10px",
        fontFamily: typography.f1,
        backgroundColor: "transparent"
      }
    },
    googleAds: {
      "& *": {
        color: colors.googleAds[500]
      }
    },
    facebookAds: {
      "& *": {
        color: colors.facebookAds[500]
      }
    },
    twitterAds: {
      "& *": {
        color: colors.twitterAds[500]
      }
    },
    linkedinAds: {
      "& *": {
        color: colors.linkedinAds[500]
      }
    },
    instagramAds: {
      "& *": {
        color: colors.instagramAds[500]
      }
    },
    microsoftAds: {
      "& *": {
        color: colors.microsoftAds[500]
      }
    },
    thumb: {
      height: "12px",
      width: "12px",
      marginTop: "-5px"
    },
    slider: {},
    textField: {
      height: "26px",
      maxWidth: "63px",
      "& div": {
        height: "26px",
        maxWidth: "63px"
      },
      "& input": {
        fontSize: "12px !important"
      }
    },
    sliderWrapper: _objectSpread(_objectSpread(_objectSpread({}, spacing["mx-4"]), spacing["pb-3"]), {}, {
      display: "block",
      width: "100%"
    }),
    align: {
      alignItems: "center"
    },
    button: _objectSpread(_objectSpread({}, spacing["mt-5"]), {}, {
      textAlign: "center"
    })
  };
};

export default makeStyles(styles);