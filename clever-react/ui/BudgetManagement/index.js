import _defineProperty from "@babel/runtime/helpers/defineProperty";
import _slicedToArray from "@babel/runtime/helpers/slicedToArray";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import nextId from "react-id-generator";
import AlertCard from "../AlertCard";
import BrandIcon from "../BrandIcon";
import Button from "../Button";
import Grid from "../Grid";
import RoundedProgressBar from "../RoundedProgressBar";
import Slider from "../Slider";
import TextField from "../TextField";
import Typography from "../Typography";
import getClasses from "./styles";
import useTheme from "../styles/useTheme";

var BudgetManagement = function BudgetManagement(props) {
  var classes = getClasses();

  var _useTheme = useTheme(),
      colors = _useTheme.cleverUI.colors;

  var budgetCopy = props.budgetCopy,
      currency = props.currency,
      channels = props.channels,
      textButton = props.textButton,
      onClickButton = props.onClickButton,
      minAlert = props.minAlert,
      maxAlert = props.maxAlert;

  var _useState = useState(channels),
      _useState2 = _slicedToArray(_useState, 2),
      finalChannels = _useState2[0],
      setFinalChannels = _useState2[1];

  var _useState3 = useState(Object.values(finalChannels).reduce(function (a, b) {
    return a + b;
  }, 0)),
      _useState4 = _slicedToArray(_useState3, 2),
      totalQuantity = _useState4[0],
      setTotalQuantity = _useState4[1];

  var _useState5 = useState(Object.keys(channels).map(function (channel) {
    return {
      color: channel,
      progress: channels[channel] * 100 / totalQuantity
    };
  })),
      _useState6 = _slicedToArray(_useState5, 2),
      progresses = _useState6[0],
      setProgresses = _useState6[1];

  var _useState7 = useState(false),
      _useState8 = _slicedToArray(_useState7, 2),
      showMinAlert = _useState8[0],
      setShowMinAlert = _useState8[1];

  var _useState9 = useState(false),
      _useState10 = _slicedToArray(_useState9, 2),
      showMaxAlert = _useState10[0],
      setShowMaxAlert = _useState10[1];

  useEffect(function () {
    var values = Object.values(finalChannels);

    if (values.includes(0)) {
      setShowMinAlert(true);
    } else {
      setShowMinAlert(false);
    }

    if (values.includes(200)) {
      setShowMaxAlert(true);
    } else {
      setShowMaxAlert(false);
    }

    setTotalQuantity(values.reduce(function (a, b) {
      return a + b;
    }, 0));
  }, [finalChannels]);
  useEffect(function () {
    setProgresses(Object.keys(finalChannels).map(function (channel) {
      return {
        color: channel,
        progress: finalChannels[channel] * 100 / totalQuantity
      };
    }));
  }, [totalQuantity]);

  var getNameIcon = function getNameIcon(name) {
    var auxName = name.toLowerCase();
    var result = "";

    if (auxName.includes("googleads")) {
      result = "GoogleAdsCircle";
    } else if (auxName.includes("analytics")) {
      result = "GoogleAnalyticsCircle";
    } else if (auxName.includes("merchant")) {
      result = "GoogleMerchantCircle";
    } else if (auxName.includes("facebook")) {
      result = "FacebookCircle";
    } else if (auxName.includes("twitter")) {
      result = "TwitterCircle";
    } else if (auxName.includes("microsoft")) {
      result = "MicrosoftCircle";
    } else if (auxName.includes("linkedin")) {
      result = "LinkedinCircle";
    } else if (auxName.includes("instagram")) {
      result = "InstagramCircle";
    }

    return result;
  };

  var handleOnChangeTextfield = function handleOnChangeTextfield(newValue, channel) {
    if (newValue < 0) {
      setFinalChannels(_objectSpread(_objectSpread({}, finalChannels), {}, _defineProperty({}, channel, 0)));
    } else if (newValue > 200) {
      setFinalChannels(_objectSpread(_objectSpread({}, finalChannels), {}, _defineProperty({}, channel, 200)));
    } else {
      setFinalChannels(_objectSpread(_objectSpread({}, finalChannels), {}, _defineProperty({}, channel, Number(newValue))));
    }
  };

  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(RoundedProgressBar, {
    size: 145,
    strokeWidth: 4,
    progressesArray: progresses,
    className: classes.progress
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.alignCenter
  }, /*#__PURE__*/React.createElement("div", {
    className: classnames(classes.sameLine, classes.alignBottom)
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "f1-42",
    weight: "bold",
    shade: 700
  }, totalQuantity), /*#__PURE__*/React.createElement(Typography, {
    variant: "f1-20",
    weight: "bold",
    shade: 700
  }, currency)), /*#__PURE__*/React.createElement(Typography, {
    variant: "f1-12",
    shade: 700
  }, budgetCopy))), showMinAlert && /*#__PURE__*/React.createElement(AlertCard, {
    color: "warning",
    text: minAlert.copyAlert,
    linkText: minAlert.copyOnClick,
    action: minAlert.onClick,
    className: classes.bottomSpace
  }), showMaxAlert && /*#__PURE__*/React.createElement(AlertCard, {
    color: "warning",
    text: maxAlert.copyAlert,
    linkText: maxAlert.copyOnClick,
    action: maxAlert.onClick,
    className: classes.bottomSpace
  }), /*#__PURE__*/React.createElement(Grid, {
    container: true
  }, Object.keys(finalChannels).map(function (channel) {
    return /*#__PURE__*/React.createElement(Grid, {
      item: true,
      xs: 12,
      className: classnames(classes.sameLine, classes.align),
      key: nextId("slider-")
    }, /*#__PURE__*/React.createElement("div", {
      className: classes.brandIcon
    }, /*#__PURE__*/React.createElement(BrandIcon, {
      icon: getNameIcon(channel),
      size: 32
    })), /*#__PURE__*/React.createElement("div", {
      className: classes.sliderWrapper
    }, /*#__PURE__*/React.createElement("div", {
      style: {
        marginLeft: "-9px",
        position: "relative",
        left: "".concat((channels[channel] - 0) * 100 / (200 - 0), "%"),
        bottom: "-12px",
        opacity: "50%"
      }
    }, /*#__PURE__*/React.createElement(Typography, {
      variant: "f1-10",
      color: channel
    }, channels[channel])), /*#__PURE__*/React.createElement("div", {
      style: {
        background: colors[channel][500],
        width: 8,
        height: 8,
        marginLeft: "-7px",
        borderRadius: "4px",
        position: "relative",
        left: "".concat((channels[channel] - 0) * 100 / (200 - 0), "%"),
        bottom: "-18px",
        opacity: "50%"
      }
    }), /*#__PURE__*/React.createElement(Slider, {
      value: finalChannels[channel],
      onChange: function onChange(event, newValue) {
        return setFinalChannels(_objectSpread(_objectSpread({}, finalChannels), {}, _defineProperty({}, channel, Number(newValue))));
      },
      color: channel,
      valueLabelDisplay: "on",
      min: 0,
      max: 200,
      step: 5,
      valueLabelClass: classnames(classes.valueLabel, classes[channel]),
      thumbClass: classes.thumb,
      className: classes.slider
    })), /*#__PURE__*/React.createElement(TextField, {
      value: finalChannels[channel],
      onChange: function onChange(event) {
        return handleOnChangeTextfield(event.target.value, channel);
      },
      className: classes.textField,
      id: "budget-management-".concat(channel)
    }));
  }), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    className: classes.button
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: function onClick() {
      return onClickButton(finalChannels);
    }
  }, textButton))));
};

BudgetManagement.propTypes = {
  budgetCopy: PropTypes.string,
  currency: PropTypes.string,
  channels: PropTypes.instanceOf(Object),
  textButton: PropTypes.string,
  onClickButton: PropTypes.func,
  minAlert: PropTypes.instanceOf(Object),
  maxAlert: PropTypes.instanceOf(Object)
};
BudgetManagement.defaultProps = {
  budgetCopy: "",
  currency: "",
  channels: {},
  textButton: "",
  onClickButton: function onClickButton() {},
  minAlert: {},
  maxAlert: {}
};
export default BudgetManagement;