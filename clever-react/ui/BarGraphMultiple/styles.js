import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var spacing = _ref.cleverUI.spacing;
  return {
    root: function root(_ref2) {
      var width = _ref2.width,
          height = _ref2.height;
      return {
        width: width,
        height: height
      };
    },
    chartContainer: function chartContainer(_ref3) {
      var width = _ref3.width,
          height = _ref3.height;
      return {
        width: width,
        height: height
      };
    },
    legend: function legend(_ref4) {
      var width = _ref4.width;
      return _objectSpread(_objectSpread({}, spacing["mx-2"]), {}, {
        maxWidth: width - 16,
        textAlign: "center"
      });
    },
    legendOption: _objectSpread(_objectSpread({}, spacing["mx-2"]), {}, {
      display: "inline-block"
    }),
    legendOptionIcon: {
      display: "inline-block",
      height: "20px"
    },
    legendOptionText: {
      display: "inline-block",
      height: "20px",
      verticalAlign: "super"
    },
    legendOptionColor: _objectSpread(_objectSpread({}, spacing["mx-2"]), {}, {
      display: "inline-block"
    }),
    tooltipContainer: {
      display: "inline-block"
    },
    tooltipMetric: {
      alignItems: "center"
    },
    tooltipIcon: _objectSpread(_objectSpread({}, spacing["mr-2"]), {}, {
      display: "inline-block"
    }),
    tooltipText: {
      display: "inline-block",
      verticalAlign: "text-top",
      marginTop: "2px"
    }
  };
};

export default makeStyles(styles);