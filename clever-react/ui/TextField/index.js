import _extends from "@babel/runtime/helpers/extends";
import _defineProperty from "@babel/runtime/helpers/defineProperty";
import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import _objectWithoutProperties from "@babel/runtime/helpers/objectWithoutProperties";
var _excluded = ["className", "InputProps", "size", "onChange", "warning", "error", "value", "inputClass", "checkErrorType", "hasErrorType", "startAdornment"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable max-len */

/* eslint-disable no-useless-escape */
import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import toLower from "lodash-es/toLower";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "../InputAdornment";
import getClasses from "./styles";

var CleverTextField = function CleverTextField(props) {
  var className = props.className,
      InputProps = props.InputProps,
      size = props.size,
      onChange = props.onChange,
      warning = props.warning,
      error = props.error,
      value = props.value,
      inputClass = props.inputClass,
      checkErrorType = props.checkErrorType,
      hasErrorType = props.hasErrorType,
      startAdornment = props.startAdornment,
      textFieldProps = _objectWithoutProperties(props, _excluded);

  var classes = getClasses();

  var _useState = useState(false),
      _useState2 = _slicedToArray(_useState, 2),
      errorType = _useState2[0],
      setErrorType = _useState2[1];

  var urlRegExp = /^((http|https):\/{2})?(([0-9a-zA-Z_-]+\.)+[a-zA-Z]+(\/([~0-9a-zA-Z#+%@./_-]+))?[/]?)(\?((.*(=.*)?)(&?)))*$/;
  var blackList = /^(?:(?!\.youtube\.|\.facebook\.|\.instagram\.|\.google\.|\.mackeeper\.|\.youtu\.be|\.test\.|\.blogspot\.|\.blogger\.|\.twitter\.|\.wordpress\.|\.teespring\.|\.amazon\.).)*$/;
  var emailRegexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  var headlinesRegEx = /[-!$%^&*()_+|~=\`{}\[\]:";'<>?\/]/;
  var descriptionsRegEx = /[-!$%^&*()_+|~=\`{}\[\]:"'<>?\/]/;
  var dots = /.+(\w+(\.|,))\S+/;

  var handleCheckErrorType = function handleCheckErrorType(newValue) {
    var valid = true;

    switch (checkErrorType) {
      case "url":
        valid = urlRegExp.test(newValue);
        break;

      case "url_blacklist":
        valid = urlRegExp.test(newValue) && blackList.test(toLower(newValue));
        break;

      case "email":
        valid = emailRegexp.test(newValue);
        break;

      case "headline":
        valid = newValue.length <= 30 && !headlinesRegEx.test(newValue) && !urlRegExp.test(newValue) && !dots.test(newValue);
        break;

      case "description":
        valid = newValue.length <= 90 && !descriptionsRegEx.test(newValue) && !urlRegExp.test(newValue) && !dots.test(newValue);
        break;

      default:
        break;
    }

    hasErrorType(!valid);
    setErrorType(!valid);
  };

  useEffect(function () {
    if (checkErrorType !== "") handleCheckErrorType(value);
  }, []);

  var handleOnChange = function handleOnChange(ev) {
    onChange(ev);

    if (checkErrorType !== "") {
      handleCheckErrorType(ev.target.value);
    }
  };

  var getAdornment = function getAdornment() {
    if (startAdornment) {
      return /*#__PURE__*/React.createElement(InputAdornment, {
        position: "start"
      }, startAdornment);
    }

    return InputProps === null || InputProps === void 0 ? void 0 : InputProps.startAdornment;
  };

  return /*#__PURE__*/React.createElement(TextField, _extends({}, textFieldProps, {
    className: classnames(_defineProperty({}, classes.warning, warning), className),
    InputProps: _objectSpread(_objectSpread({}, InputProps), {}, {
      classes: {
        input: classnames(classes.input, _defineProperty({}, classes.smallText, size === "small"), inputClass)
      },
      startAdornment: getAdornment()
    }),
    onChange: handleOnChange,
    color: "secondary",
    error: error || errorType,
    size: size === "standard" ? "medium" : size,
    value: value
  }));
};

CleverTextField.propTypes = {
  className: PropTypes.string,
  variant: PropTypes.oneOf(["standard", "outlined"]),
  select: PropTypes.bool,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.bool]),
  disabled: PropTypes.bool,
  type: PropTypes.string,
  error: PropTypes.bool,
  warning: PropTypes.bool,
  fullWidth: PropTypes.bool,
  multiline: PropTypes.bool,
  rows: PropTypes.number,
  inputClass: PropTypes.string,
  size: PropTypes.oneOf(["small", "standard"]),
  checkErrorType: PropTypes.oneOf(["", "url", "url_blacklist", "email", "headline", "description"]),
  hasErrorType: PropTypes.func,
  startAdornment: PropTypes.node
};
CleverTextField.defaultProps = {
  variant: "outlined",
  className: "",
  select: false,
  label: "",
  placeholder: "",
  onChange: function onChange() {},
  disabled: false,
  type: "text",
  error: false,
  warning: false,
  fullWidth: true,
  value: "",
  multiline: false,
  rows: 3,
  inputClass: "",
  size: "standard",
  checkErrorType: "",
  hasErrorType: function hasErrorType() {}
};
export default CleverTextField;