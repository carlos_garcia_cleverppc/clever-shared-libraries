import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";
var getClasses = makeStyles(function (_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing,
      shadows = _ref$cleverUI.shadows;
  return {
    root: {
      width: "fit-content",
      display: "inline-block"
    },
    panel: _objectSpread({}, spacing["mb-4"]),
    cropper: _objectSpread(_objectSpread(_objectSpread({}, spacing["mx-auto"]), spacing["mb-2"]), {}, {
      minHeight: 144,
      maxHeight: 400,
      maxWidth: 600,
      width: "100%"
    }),
    loader: _objectSpread({}, spacing["h-100"]),
    logoPlaceholder: {
      width: "9rem",
      height: "9rem",
      alignItems: "center",
      justifyContent: "center"
    },
    newLogo: _objectSpread(_objectSpread({}, spacing["p-2"]), {}, {
      display: "flex",
      background: "transparent",
      border: "solid 1px ".concat(colors.gray.main),
      width: "9rem",
      height: "9rem",
      borderRadius: "12px",
      boxSizing: "border-box",
      textAlign: "center"
    }),
    imagePreview: _objectSpread(_objectSpread(_objectSpread({
      maxWidth: "100%",
      maxHeight: "100%"
    }, spacing["w-auto"]), spacing["h-auto"]), spacing["m-auto"]),
    actions: _objectSpread(_objectSpread(_objectSpread({}, spacing["mnt-5"]), spacing["mb-4"]), {}, {
      marginLeft: "5rem",
      display: "flex",
      position: "absolute"
    }),
    actionsMobile: {
      marginLeft: 0,
      position: "absolute"
    },
    urlInputContainer: _objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread({}, spacing["px-6"]), spacing["mnx-3"]), spacing["pb-1"]), spacing["pt-3"]), {}, {
      backgroundColor: "white",
      boxShadow: shadows.hover
    }),
    input: {
      maxWidth: "175px",
      width: "100%",
      minWidth: "16px",
      "& input": {
        padding: "0px !important"
      }
    },
    inputEnter: {
      width: 0
    },
    inputEnterActive: {
      width: "175px",
      transition: "width 500ms ease-in-out"
    },
    inputLeave: {
      width: "200px"
    },
    inputLeaveActive: {
      width: 0,
      transition: "width 300ms ease-in-out"
    },
    buttonEnter: {
      transform: "translate(-10px, 0)"
    },
    buttonEnterActive: {
      transform: "translate(0, 0)",
      transition: "all 500ms ease-in-out"
    },
    buttonLeave: {},
    buttonLeaveActive: {
      transform: "translate(0, 0)",
      transition: "all 800ms ease-in-out"
    },
    uploadLogoBtn: _objectSpread({}, spacing["mr-3"]),
    inputTransition: {
      zIndex: 1
    },
    iconButton: {
      zIndex: 2
    },
    hiddeInput: {
      display: "none"
    }
  };
});
export default getClasses;