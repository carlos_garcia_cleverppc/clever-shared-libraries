import _asyncToGenerator from "@babel/runtime/helpers/asyncToGenerator";
import _regeneratorRuntime from "@babel/runtime/regenerator";

if (!String.prototype.startsWith) {
  String.prototype.startsWith = function (search, pos) {
    return this.substr(!pos || pos < 0 ? 0 : +pos, search.length) === search;
  };
}

var DEFAULT_RATIO = 1;

function validateInput(base64String, maxWidth, maxHeight) {
  var validationResult = {
    isValid: false,
    errorMessage: "An error occurred."
  };

  if (!base64String) {
    validationResult.errorMessage = "The input parameter base64String is ".concat(base64String, ".");
  } else if (typeof base64String !== "string") {
    validationResult.errorMessage = "The input parameter base64String is not of type \"string\".";
  } else if (!base64String.startsWith("data:image")) {
    validationResult.errorMessage = "The input parameter base64String does not start with \"data:image\".";
  } else if (!maxWidth) {
    validationResult.errorMessage = "The input parameter maxWidth is ".concat(maxWidth, ".");
  } else if (typeof maxWidth !== "number") {
    validationResult.errorMessage = "The input parameter maxWidth is not of type \"number\".";
  } else if (maxWidth < 2) {
    validationResult.errorMessage = "The input parameter maxWidth must be at least 2 pixel.";
  } else if (!maxHeight) {
    validationResult.errorMessage = "The input parameter maxHeight is ".concat(maxHeight, ".");
  } else if (typeof maxHeight !== "number") {
    validationResult.errorMessage = "The input parameter maxHeight is not of type \"number\".";
  } else if (maxHeight < 2) {
    validationResult.errorMessage = "The input parameter maxHeight must be at least 2 pixel.";
  } else {
    validationResult.isValid = true;
    validationResult.errorMessage = null;
  }

  return validationResult;
}

function maxWidthRatioFunction(imageWidth, imageHeight, targetWidth) {
  var ratio = DEFAULT_RATIO;

  if (imageWidth > targetWidth) {
    ratio = targetWidth / imageWidth;
  }

  return {
    width: ratio,
    height: ratio
  };
}

function maxHeightRatioFunction(imageWidth, imageHeight, targetWidth, targetHeight) {
  var ratio = DEFAULT_RATIO;

  if (imageHeight > targetHeight) {
    ratio = targetHeight / imageHeight;
  }

  return {
    width: ratio,
    height: ratio
  };
}

function maxWidthMaxHeightRatioFunction(imageWidth, imageHeight, targetWidth, targetHeight) {
  var widthRatio = targetWidth / imageWidth;
  var heightRatio = targetHeight / imageHeight;
  return {
    width: widthRatio,
    height: heightRatio
  };
}

function resizeBase64ForMaxWidth(_x, _x2, _x3, _x4, _x5) {
  return _resizeBase64ForMaxWidth.apply(this, arguments);
}

function _resizeBase64ForMaxWidth() {
  _resizeBase64ForMaxWidth = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime.mark(function _callee(base64String, maxWidth, maxHeight, successCallback, errorCallback) {
    var validationResult;
    return _regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            validationResult = validateInput(base64String, maxWidth, maxHeight);

            if (!(validationResult.isValid === true)) {
              _context.next = 6;
              break;
            }

            _context.next = 4;
            return resizeBase64(base64String, maxWidth, maxHeight, maxWidthRatioFunction, successCallback, errorCallback);

          case 4:
            _context.next = 7;
            break;

          case 6:
            errorCallback(validationResult.errorMessage);

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _resizeBase64ForMaxWidth.apply(this, arguments);
}

function resizeBase64ForMaxHeight(_x6, _x7, _x8, _x9, _x10) {
  return _resizeBase64ForMaxHeight.apply(this, arguments);
}

function _resizeBase64ForMaxHeight() {
  _resizeBase64ForMaxHeight = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime.mark(function _callee2(base64String, maxWidth, maxHeight, successCallback, errorCallback) {
    var validationResult;
    return _regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            validationResult = validateInput(base64String, maxWidth, maxHeight);

            if (!(validationResult.isValid === true)) {
              _context2.next = 6;
              break;
            }

            _context2.next = 4;
            return resizeBase64(base64String, maxWidth, maxHeight, maxHeightRatioFunction, successCallback, errorCallback);

          case 4:
            _context2.next = 7;
            break;

          case 6:
            errorCallback(validationResult.errorMessage);

          case 7:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _resizeBase64ForMaxHeight.apply(this, arguments);
}

function resizeBase64ForMaxWidthAndMaxHeight(_x11, _x12, _x13, _x14, _x15) {
  return _resizeBase64ForMaxWidthAndMaxHeight.apply(this, arguments);
}

function _resizeBase64ForMaxWidthAndMaxHeight() {
  _resizeBase64ForMaxWidthAndMaxHeight = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime.mark(function _callee3(base64String, maxWidth, maxHeight, successCallback, errorCallback) {
    var validationResult;
    return _regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            validationResult = validateInput(base64String, maxWidth, maxHeight);

            if (!(validationResult.isValid === true)) {
              _context3.next = 6;
              break;
            }

            _context3.next = 4;
            return resizeBase64(base64String, maxWidth, maxHeight, maxWidthMaxHeightRatioFunction, successCallback, errorCallback);

          case 4:
            _context3.next = 7;
            break;

          case 6:
            errorCallback(validationResult.errorMessage);

          case 7:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _resizeBase64ForMaxWidthAndMaxHeight.apply(this, arguments);
}

function resizeBase64(_x16, _x17, _x18, _x19, _x20, _x21) {
  return _resizeBase.apply(this, arguments);
}

function _resizeBase() {
  _resizeBase = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime.mark(function _callee5(base64String, maxWidth, maxHeight, ratioFunction, successCallback, errorCallback) {
    var canvas, ctx, canvasCopy, copyContext;
    return _regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            // Create and initialize two canvas
            canvas = document.createElement("canvas");
            ctx = canvas.getContext("2d");
            canvasCopy = document.createElement("canvas");
            copyContext = canvasCopy.getContext("2d"); // Create original image

            _context5.next = 6;
            return new Promise(function (resolve) {
              var img = new Image();
              img.onload = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime.mark(function _callee4() {
                var ratioResult, widthRatio, heightRatio;
                return _regeneratorRuntime.wrap(function _callee4$(_context4) {
                  while (1) {
                    switch (_context4.prev = _context4.next) {
                      case 0:
                        ratioResult = ratioFunction(img.width, img.height, maxWidth, maxHeight);
                        widthRatio = ratioResult.width;
                        heightRatio = ratioResult.height; // Draw original image in second canvas

                        // Draw original image in second canvas
                        canvasCopy.width = img.width;
                        canvasCopy.height = img.height;
                        copyContext.drawImage(img, 0, 0); // Copy and resize second canvas to first canvas

                        // Copy and resize second canvas to first canvas
                        canvas.width = img.width * widthRatio;
                        canvas.height = img.height * heightRatio;
                        ctx.drawImage(canvasCopy, 0, 0, canvasCopy.width, canvasCopy.height, 0, 0, canvas.width, canvas.height);
                        _context4.next = 11;
                        return successCallback(canvas.toDataURL());

                      case 11:
                        resolve("");

                      case 12:
                      case "end":
                        return _context4.stop();
                    }
                  }
                }, _callee4);
              }));

              img.onerror = function () {
                errorCallback("Error while loading image.");
              };

              img.src = base64String;
            });

          case 6:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));
  return _resizeBase.apply(this, arguments);
}

export { resizeBase64ForMaxWidth };
export { resizeBase64ForMaxHeight };
export { resizeBase64ForMaxWidthAndMaxHeight };