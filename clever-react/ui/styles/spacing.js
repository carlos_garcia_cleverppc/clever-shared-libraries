import { getCurrentConfig } from "../Provider/themeConfig";
import generateSpacing from "../../utils/spacing";
var _getCurrentConfig$cle = getCurrentConfig().cleverUI.spacers,
    calculateSpace = _getCurrentConfig$cle.calculateSpace,
    unit = _getCurrentConfig$cle.unit;
export default generateSpacing(calculateSpace, unit);