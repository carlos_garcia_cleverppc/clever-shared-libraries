import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import _toConsumableArray from "@babel/runtime/helpers/toConsumableArray";
import _typeof from "@babel/runtime/helpers/typeof";
import allColors from "../Provider/colors";
export function getColors() {
  var colors = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : allColors;
  var level = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  var flattened = {};
  Object.keys(colors).forEach(function (key) {
    if (_typeof(colors[key]) === "object" && colors[key] !== null && level < 1) {
      Object.assign(flattened, getColors(colors[key], 1));
    } else {
      flattened[key] = colors[key];
    }
  });
  return flattened;
}
export function getColorsLabels() {
  var inherit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var colors = Object.keys(getColors());
  if (inherit) colors.push("inherit");
  return colors;
}
export function getShades() {
  return ["lighter", "light", "main", "dark", "darker", 50, 100, 200, 300, 400, 500, 600, 700, 800, 900];
}
export function hex2rgb(hex) {
  return hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, function (m, r, g, b) {
    return "#".concat(r).concat(r).concat(g).concat(g).concat(b).concat(b);
  }).substring(1).match(/.{2}/g).map(function (x) {
    return parseInt(x, 16);
  });
}
export function hex2rgba(hex) {
  var rgb = hex2rgb(hex);
  var min = Math.min.apply(Math, _toConsumableArray(rgb));
  var alpha = (255 - min) / 255;
  var r = (rgb[0] - min) / alpha,
      g = (rgb[1] - min) / alpha,
      b = (rgb[2] - min) / alpha;
  return [r, g, b, alpha];
}
export function cssStringHex2rgba(hex) {
  var _hex2rgba = hex2rgba(hex),
      _hex2rgba2 = _slicedToArray(_hex2rgba, 4),
      r = _hex2rgba2[0],
      g = _hex2rgba2[1],
      b = _hex2rgba2[2],
      a = _hex2rgba2[3];

  return "rgba(".concat(r, ", ").concat(g, ", ").concat(b, ", ").concat(a.toFixed(3), ")");
}