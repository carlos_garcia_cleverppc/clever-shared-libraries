function getContrastText(hex) {
  var auxHex = hex;

  if (hex.indexOf("#") === 0) {
    auxHex = hex.slice(1);
  } // convert 3-digit hex to 6-digits.


  if (hex.length === 3) {
    auxHex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
  }

  if (auxHex.length !== 6) {
    throw new Error("Invalid HEX color.");
  }

  var r = parseInt(auxHex.slice(0, 2), 16);
  var g = parseInt(auxHex.slice(2, 4), 16);
  var b = parseInt(auxHex.slice(4, 6), 16);
  return r * 0.299 + g * 0.587 + b * 0.114 > 186 ? "#232735" : "#fff";
}

export default getContrastText;