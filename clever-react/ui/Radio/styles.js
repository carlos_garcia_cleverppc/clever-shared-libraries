import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var spacing = _ref.cleverUI.spacing;
  return {
    root: {
      display: "inline-flex",
      "align-items": "center",
      outline: "none",
      cursor: "pointer"
    },
    radio: function radio(_ref2) {
      var size = _ref2.size;
      return _objectSpread(_objectSpread({}, spacing["p-2"]), {}, {
        "& span svg": {
          width: "".concat(size, "px"),
          height: "".concat(size, "px")
        }
      });
    }
  };
};

export default makeStyles(styles);