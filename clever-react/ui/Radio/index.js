import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import MaterialRadio from "@material-ui/core/Radio";
import CircleIcon from "@material-ui/icons/RadioButtonUnchecked";
import CheckCircleIcon from "@material-ui/icons/RadioButtonChecked";
import { onKeyDown } from "../../utils/helpers";
import getClasses from "./styles";

var Radio = function Radio(props) {
  var checked = props.checked,
      onChange = props.onChange,
      value = props.value,
      className = props.className;
  var size = props.size,
      disabled = props.disabled,
      children = props.children,
      icon = props.icon,
      checkedIcon = props.checkedIcon;
  var color = props.color;
  var classes = getClasses({
    size: size
  });
  return /*#__PURE__*/React.createElement("div", {
    className: classnames(classes.root, className),
    onClick: function onClick() {
      return onChange({
        target: {
          value: value
        }
      });
    },
    onKeyDown: onKeyDown(function () {
      return onChange({
        target: {
          value: value
        }
      });
    }),
    role: "button",
    tabIndex: 0
  }, /*#__PURE__*/React.createElement(MaterialRadio, {
    checked: checked,
    value: value,
    disabled: disabled,
    icon: icon,
    classes: {
      root: classes.radio
    },
    checkedIcon: checkedIcon,
    color: color
  }), /*#__PURE__*/React.createElement("div", null, children));
};

Radio.propTypes = {
  className: PropTypes.string,
  checked: PropTypes.bool.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  color: PropTypes.oneOf(["primary", "secondary"]),
  disabled: PropTypes.bool,
  size: PropTypes.number,
  icon: PropTypes.instanceOf(Object),
  checkedIcon: PropTypes.instanceOf(Object)
};
Radio.defaultProps = {
  className: "",
  onChange: function onChange() {},
  color: "primary",
  disabled: false,
  size: 20,
  icon: /*#__PURE__*/React.createElement(CircleIcon, null),
  checkedIcon: /*#__PURE__*/React.createElement(CheckCircleIcon, null)
};
export default Radio;