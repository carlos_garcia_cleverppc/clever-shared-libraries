/* eslint-disable react/jsx-props-no-spreading */

/* eslint-disable object-property-newline */
import React from "react";
import PropTypes from "prop-types";
import MaterialGrid from "@material-ui/core/Grid";

var Grid = function Grid(props) {
  var container = props.container,
      item = props.item,
      align = props.align,
      alignItems = props.alignItems;
  var justify = props.justify,
      spacing = props.spacing,
      direction = props.direction;
  var xs = props.xs,
      sm = props.sm,
      md = props.md,
      lg = props.lg,
      xl = props.xl;
  var children = props.children,
      className = props.className,
      style = props.style;
  var auxProps = {
    container: container,
    item: item,
    align: align,
    alignItems: alignItems,
    className: className,
    direction: direction,
    justifyContent: justify,
    xs: xs,
    sm: sm,
    md: md,
    lg: lg,
    xl: xl,
    style: style
  };

  if (container && typeof spacing === "number") {
    auxProps.spacing = spacing;
  } else if (container && spacing !== undefined && spacing) {
    auxProps.spacing = 2;
  } else {
    auxProps.spacing = undefined;
  }

  return /*#__PURE__*/React.createElement(MaterialGrid, auxProps, children);
};

Grid.propTypes = {
  spacing: PropTypes.oneOfType([PropTypes.bool, PropTypes.oneOf([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])])
};
Grid.defaultProps = {
  spacing: false
};
export default Grid;