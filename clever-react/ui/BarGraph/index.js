import _defineProperty from "@babel/runtime/helpers/defineProperty";
import _toConsumableArray from "@babel/runtime/helpers/toConsumableArray";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import React from "react";
import PropTypes from "prop-types";
import { Group } from "@vx/group";
import { Bar } from "@vx/shape";
import { scaleLinear, scaleBand } from "@vx/scale";
import { useTooltip, TooltipWithBounds } from "@vx/tooltip";
import { localPoint } from "@vx/event";
import { Text } from "@vx/text";
import { AxisLeft } from "@vx/axis";
import { GridRows } from "@vx/grid";
import compact from "lodash-es/compact";
import isEmpty from "lodash-es/isEmpty";
import Typography from "../Typography";
import getClasses from "./styles";
import useTheme from "../styles/useTheme";

var BarGraph = function BarGraph(props) {
  var color = props.color,
      type = props.type,
      data = props.data,
      height = props.height,
      width = props.width;
  var showValues = props.showValues,
      axis = props.axis;
  var axisNum = 3;
  var classes = getClasses(props);

  var _useTheme = useTheme(),
      _useTheme$cleverUI = _useTheme.cleverUI,
      colors = _useTheme$cleverUI.colors,
      spacing = _useTheme$cleverUI.spacing;

  var myChartRef = /*#__PURE__*/React.createRef();

  var getShow = function getShow() {
    if (showValues === "responsive") {
      if (width / data.length < 40) {
        return "odd";
      }

      return "all";
    }

    return showValues;
  };

  var show = getShow();
  var showAxis = axis && !isEmpty(compact(data.map(function (v) {
    return v.y !== 0;
  })));
  var marginTop = show === "" ? 0 : 20;
  var marginLeft = showAxis ? 30 : 0; // Then we'll create some bounds

  var xMax = width - marginLeft;
  var yMax = height - marginTop; // We'll make some helpers to get at the data we want

  var x = function x(d) {
    return d.x;
  };

  var y = function y(d) {
    return +d.y * 100;
  }; // And then scale the graph by our data


  var xScale = scaleBand({
    range: [0, xMax],
    round: true,
    domain: data.map(x),
    padding: 0.4
  });
  var yMaxValue = Math.max.apply(Math, _toConsumableArray(data.map(y)));
  var yScale = scaleLinear({
    range: [yMax, 0],
    round: true,
    domain: [0, yMaxValue === 0 ? yMax : yMaxValue]
  }); // Compose together the scale and accessor functions to get point functions

  var compose = function compose(scale, accessor) {
    return function (data2) {
      return scale(accessor(data2));
    };
  };

  var xPoint = compose(xScale, x);
  var yPoint = compose(yScale, y);

  var _useTooltip = useTooltip(),
      tooltipData = _useTooltip.tooltipData,
      tooltipLeft = _useTooltip.tooltipLeft,
      tooltipTop = _useTooltip.tooltipTop,
      tooltipOpen = _useTooltip.tooltipOpen,
      showTooltip = _useTooltip.showTooltip,
      hideTooltip = _useTooltip.hideTooltip;

  var handleMouseOver = function handleMouseOver(event, value) {
    var position = myChartRef.current.getBoundingClientRect();
    var coords = localPoint(event.target.ownerSVGElement, event);
    showTooltip({
      tooltipLeft: coords.x,
      tooltipTop: coords.y - position.height,
      tooltipData: value
    });
  };

  var tooltipStyles = _objectSpread(_objectSpread(_objectSpread(_objectSpread(_objectSpread({
    zIndex: 100000,
    backgroundColor: colors.navyBlue[500],
    minWidth: "110px",
    borderRadius: "8px",
    display: "inline-block",
    whiteSpace: "pre-line"
  }, spacing["pl-3"]), spacing["pr-5"]), spacing["pt-3"]), spacing["pb-4"]), {}, {
    textAlign: "left"
  });

  var yScaleGrid = scaleLinear({
    domain: [0, Math.max.apply(Math, _toConsumableArray(data.map(function (v) {
      return v.y;
    })))],
    nice: true
  });
  yScaleGrid.range([yMax, 0]);
  yScaleGrid.ticks(axisNum);
  var ticks = yScaleGrid.ticks(axisNum);
  var tickFormats = yScaleGrid.tickFormat(axisNum, "~s");
  ticks.map(tickFormats); // Finally we'll embed it all in an SVG

  return /*#__PURE__*/React.createElement("div", {
    className: classes.root
  }, /*#__PURE__*/React.createElement("svg", {
    width: width,
    height: height,
    ref: myChartRef
  }, !!showAxis && /*#__PURE__*/React.createElement(GridRows, {
    scale: yScaleGrid,
    width: xMax,
    left: marginLeft + 5,
    numTicks: axisNum
  }), data.map(function (d, i) {
    var barHeight = yMax - yPoint(d) === 0 ? 2 : yMax - yPoint(d);
    var yValue = yMax - barHeight - marginTop;
    return /*#__PURE__*/React.createElement(Group, {
      key: "bar-".concat(d.x),
      top: show !== "" ? marginTop : 0,
      left: showAxis ? marginLeft : 0
    }, (show === "all" || show === "even" && i % 2 === 0 || show === "odd" && i % 2 !== 0) && /*#__PURE__*/React.createElement(Text, {
      verticalAnchor: "start",
      textAnchor: "middle",
      style: {
        fontSize: "14px",
        zIndex: 1000000,
        paddingTop: "15px"
      },
      fill: colors[color][500],
      fontFamily: "Helvetica",
      dx: xPoint(d) + 3,
      dy: yValue,
      fontWeight: 600
    }, d.y ? d.y : "0"), !!showAxis && /*#__PURE__*/React.createElement(AxisLeft, {
      scale: yScaleGrid,
      stroke: "transparent",
      tickStroke: "transparent",
      tickValues: ticks,
      tickFormat: tickFormats,
      left: -20,
      tickLabelProps: function tickLabelProps() {
        return {
          fill: colors.neutral[300],
          fontSize: 12,
          fontFamily: "Helvetica"
        };
      }
    }), /*#__PURE__*/React.createElement(Bar, {
      x: xPoint(d),
      y: yMax - barHeight,
      height: barHeight,
      width: 6,
      fill: colors[color][500],
      rx: 3,
      onMouseOver: function onMouseOver(ev) {
        return handleMouseOver(ev, d);
      },
      onMouseOut: hideTooltip
    }));
  })), tooltipOpen && /*#__PURE__*/React.createElement(TooltipWithBounds // set this to random so it correctly updates with parent bounds
  , {
    key: Math.random(),
    top: tooltipTop,
    left: tooltipLeft,
    style: tooltipStyles
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "f1-12",
    weight: "bold",
    color: "white"
  }, tooltipData.y, " ", type), /*#__PURE__*/React.createElement(Typography, {
    variant: "f2-12"
  }, tooltipData.x)));
};

BarGraph.propTypes = {
  showValues: PropTypes.oneOf(["", "all", "odd", "even", "responsive"]),
  height: PropTypes.number,
  width: PropTypes.number,
  axis: PropTypes.bool,
  data: PropTypes.instanceOf(Array),
  type: PropTypes.string,
  color: PropTypes.string
};
BarGraph.defaultProps = {
  showValues: "",
  height: 300,
  width: 300,
  axis: false,
  data: [],
  type: "",
  color: "primary"
};
export default BarGraph;