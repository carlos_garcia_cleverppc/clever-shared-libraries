import makeStyles from "../styles/makeStyles";

var styles = function styles() {
  return {
    root: function root(_ref) {
      var height = _ref.height,
          width = _ref.width;
      return {
        height: height,
        width: width
      };
    }
  };
};

export default makeStyles(styles);