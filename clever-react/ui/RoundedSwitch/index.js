/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { onKeyDown } from "../../utils/helpers";
import useMediaQuery from "../styles/useMediaQuery";
import { screenSm, screenXs } from "../styles/breakpoints";
import { getColorsLabels } from "../styles/colors";
import Typography from "../Typography";
import getClasses from "./styles";
var colorsList = getColorsLabels();

var RoundedSwitch = function RoundedSwitch(props) {
  var classes = getClasses(props);
  var checked = props.checked,
      onChange = props.onChange,
      options = props.options;
  var className = props.className;
  var isScreenSm = useMediaQuery(screenSm);
  var isScreenXs = useMediaQuery(screenXs);
  return /*#__PURE__*/React.createElement("div", {
    onClick: onChange,
    onKeyDown: onKeyDown(onChange),
    className: classnames(classes.root, className, isScreenXs || isScreenSm ? "" : classes.desktopCursor),
    role: "button",
    tabIndex: 0
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.option0
  }, /*#__PURE__*/React.createElement(Typography, {
    weight: "bold",
    align: "center",
    variant: "f2-14"
  }, options[0])), /*#__PURE__*/React.createElement("div", {
    className: classnames(classes.button, checked ? classes.checkedButton : "")
  }, /*#__PURE__*/React.createElement(Typography, {
    color: "white",
    weight: "bold",
    align: "center",
    variant: "f2-14"
  }, !checked ? options[0] : "", !checked ? "" : options[1])), /*#__PURE__*/React.createElement("div", {
    className: classes.option1
  }, /*#__PURE__*/React.createElement(Typography, {
    weight: "bold",
    align: "center",
    variant: "f2-14"
  }, options[1])));
};

RoundedSwitch.propTypes = {
  checked: PropTypes.bool,
  options: PropTypes.instanceOf(Array).isRequired,
  color: PropTypes.oneOf(colorsList),
  // no borrar
  onChange: PropTypes.func,
  className: PropTypes.string,
  buttonWidth: PropTypes.string // no borrar

};
RoundedSwitch.defaultProps = {
  checked: false,
  color: "secondary",
  onChange: function onChange() {},
  className: "",
  buttonWidth: "128px"
};
export default RoundedSwitch;