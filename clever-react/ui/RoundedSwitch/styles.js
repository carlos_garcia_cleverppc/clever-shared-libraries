import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var colors = _ref.cleverUI.colors;
  return {
    root: {
      height: "32px",
      width: "256px",
      borderRadius: "16px",
      backgroundColor: "white",
      position: "relative",
      "&:focus": {
        outline: "none"
      }
    },
    desktopCursor: {
      cursor: "pointer"
    },
    button: function button(_ref2) {
      var buttonWidth = _ref2.buttonWidth,
          color = _ref2.color;
      return {
        height: "32px",
        width: buttonWidth,
        borderRadius: "16px",
        background: colors[color][500],
        position: "absolute",
        zIndex: 1,
        transition: "all 700ms cubic-bezier(.01, .81, .44, 1)",
        display: "flex",
        alignItems: "center",
        justifyContent: "center"
      };
    },
    checkedButton: function checkedButton(_ref3) {
      var buttonWidth = _ref3.buttonWidth;
      return {
        transform: "translate(".concat(buttonWidth, ", 0)"),
        transition: "all 700ms cubic-bezier(.01, .81, .44, 1)"
      };
    },
    option0: function option0(_ref4) {
      var buttonWidth = _ref4.buttonWidth;
      return {
        height: "32px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        width: buttonWidth,
        position: "absolute"
      };
    },
    option1: function option1(_ref5) {
      var buttonWidth = _ref5.buttonWidth;
      return {
        height: "32px",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        width: buttonWidth,
        left: buttonWidth,
        position: "absolute"
      };
    }
  };
};

export default makeStyles(styles);