/* eslint-disable react/jsx-props-no-spreading */
import React, { Suspense } from "react";
import Loading from "../Loading";

function asyncComponent(Component) {
  var loader = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : /*#__PURE__*/React.createElement(Loading, {
    center: true
  });
  return function (props) {
    return /*#__PURE__*/React.createElement(Suspense, {
      fallback: loader
    }, /*#__PURE__*/React.createElement(Component, props));
  };
}

export default asyncComponent;