import React from "react";
import PropTypes from "prop-types";
import { DynamicModuleLoader } from "redux-dynamic-modules-react";

var ModuleLoader = function ModuleLoader(_ref) {
  var Component = _ref.Component,
      config = _ref.config,
      componentProps = _ref.componentProps;
  return /*#__PURE__*/React.createElement(DynamicModuleLoader, {
    modules: [config]
  }, /*#__PURE__*/React.createElement(Component, componentProps));
};

ModuleLoader.propTypes = {
  config: PropTypes.instanceOf(Object).isRequired,
  Component: PropTypes.oneOfType([PropTypes.instanceOf(Object), PropTypes.node, PropTypes.element]).isRequired,
  componentProps: PropTypes.instanceOf(Object)
};
ModuleLoader.defaultProps = {
  componentProps: {}
};
export default ModuleLoader;