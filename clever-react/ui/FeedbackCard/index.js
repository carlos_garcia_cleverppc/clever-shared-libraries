import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import React, { useState } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Card from "../Card";
import FeedbackThumbs from "../FeedbackThumb";
import Grid from "../Grid";
import Typography from "../Typography";
import getClasses from "./styles";

var FeedbackCard = function FeedbackCard(props) {
  var timeCopy = props.timeCopy,
      showBadge = props.showBadge,
      comment = props.comment,
      thumbUpSelected = props.thumbUpSelected,
      thumbDownSelected = props.thumbDownSelected;
  var onClickThumbUp = props.onClickThumbUp,
      onClickThumbDown = props.onClickThumbDown,
      onClick = props.onClick,
      cardSelected = props.cardSelected,
      className = props.className;

  var _useState = useState(thumbUpSelected ? "up" : undefined),
      _useState2 = _slicedToArray(_useState, 2),
      isSelectedThumbUp = _useState2[0],
      setIsSelectedThumbUp = _useState2[1];

  var _useState3 = useState(thumbDownSelected ? "down" : undefined),
      _useState4 = _slicedToArray(_useState3, 2),
      isSelectedThumbDown = _useState4[0],
      setIsSelectedThumbDown = _useState4[1];

  var getBorderColor = function getBorderColor() {
    if (cardSelected) {
      return "green";
    }

    return "neutral";
  };

  var classes = getClasses({
    borderColor: getBorderColor()
  });

  var _handleThumbClick = function handleThumbClick(feedback) {
    if (feedback === "up") {
      setIsSelectedThumbUp("up");
      setIsSelectedThumbDown(undefined);
      onClickThumbUp();
    } else {
      setIsSelectedThumbUp(undefined);
      setIsSelectedThumbDown("down");
      onClickThumbDown();
    }
  };

  return /*#__PURE__*/React.createElement("div", {
    className: className
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.time
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "f2-11"
  }, timeCopy), showBadge && /*#__PURE__*/React.createElement("div", {
    className: classes.badge
  })), /*#__PURE__*/React.createElement(Card, {
    className: classnames(classes.card, onClick !== undefined ? classes.pointer : ""),
    onClick: onClick
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "f2-14",
    shade: 900
  }, comment)), /*#__PURE__*/React.createElement(Grid, {
    container: true,
    justify: "flex-end",
    alignItems: "center",
    className: classes.thumbs
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true
  }, /*#__PURE__*/React.createElement(FeedbackThumbs, {
    handleThumbClick: function handleThumbClick() {
      return _handleThumbClick("up");
    },
    isSelected: isSelectedThumbUp,
    thumbUp: true,
    size: 50
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true
  }, /*#__PURE__*/React.createElement(FeedbackThumbs, {
    handleThumbClick: function handleThumbClick() {
      return _handleThumbClick("down");
    },
    isSelected: isSelectedThumbDown,
    size: 50
  }))));
};

FeedbackCard.propTypes = {
  timeCopy: PropTypes.string,
  showBadge: PropTypes.bool,
  comment: PropTypes.string,
  cardSelected: PropTypes.bool,
  thumbDownSelected: PropTypes.bool,
  thumbUpSelected: PropTypes.bool,
  onClickThumbDown: PropTypes.instanceOf(Object),
  onClickThumbUp: PropTypes.instanceOf(Object),
  onClick: PropTypes.instanceOf(Object)
};
FeedbackCard.defaultProps = {
  timeCopy: "",
  showBadge: false,
  comment: "",
  cardSelected: false,
  thumbDownSelected: false,
  thumbUpSelected: false,
  onClickThumbDown: function onClickThumbDown() {},
  onClickThumbUp: function onClickThumbUp() {},
  onClick: undefined
};
export default FeedbackCard;