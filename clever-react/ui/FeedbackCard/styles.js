import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    positionHorizontal: {
      display: "inline-flex"
    },
    card: function card(_ref2) {
      var borderColor = _ref2.borderColor;
      return _objectSpread(_objectSpread(_objectSpread({}, spacing["px-4"]), spacing["py-5"]), {}, {
        backgroundColor: colors.neutral[100],
        borderWidth: "2px",
        borderColor: borderColor === "neutral" ? colors.neutral[100] : colors[borderColor][500],
        width: "264px"
      });
    },
    pointer: {
      cursor: "pointer"
    },
    time: _objectSpread(_objectSpread({}, spacing["mb-2"]), {}, {
      display: "flex",
      alignItems: "baseline"
    }),
    badge: _objectSpread(_objectSpread({}, spacing["ml-2"]), {}, {
      height: "8px",
      width: "8px",
      borderRadius: "50%",
      backgroundColor: colors.danger[500]
    }),
    thumbs: {
      width: "300px"
    }
  };
};

export default makeStyles(styles);