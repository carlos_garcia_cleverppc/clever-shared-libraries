import React from "react";
import PropTypes from "prop-types";
import MaterialDrawer from "@material-ui/core/Drawer";

var Drawer = function Drawer(props) {
  var children = props.children,
      open = props.open,
      onClose = props.onClose;
  var anchor = props.anchor;
  return /*#__PURE__*/React.createElement(MaterialDrawer, {
    anchor: anchor,
    open: open,
    onClose: onClose
  }, children);
};

Drawer.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func
};
Drawer.defaultProps = {
  open: false,
  onClose: function onClose() {}
};
export default Drawer;