import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import MaterialExpansionPanel from "@material-ui/core/Accordion";
import ExpansionPanelSummary from "@material-ui/core/AccordionSummary";
import ExpansionPanelDetails from "@material-ui/core/AccordionActions";
import { getColorsLabels } from "../styles/colors";
import Select from "../Icon/Select";
import ChevronDown from "../Icon/ChevronDown";
import Grid from "../Grid";
import Typography from "../Typography";
import getClasses from "./styles";
var colorsList = getColorsLabels(true);

var Accordion = function Accordion(props) {
  var className = props.className,
      onChange = props.onChange,
      expanded = props.expanded,
      expandIcon = props.expandIcon,
      classes = props.classes;
  var title = props.title,
      children = props.children,
      unmountOnExit = props.unmountOnExit,
      variant = props.variant,
      color = props.color;
  var header = props.header,
      subtitle = props.subtitle,
      badge = props.badge,
      badgeProps = props.badgeProps;
  var classesCustom = getClasses({
    color: color,
    badgeProps: badgeProps
  });
  return /*#__PURE__*/React.createElement(MaterialExpansionPanel, {
    onChange: onChange,
    expanded: expanded,
    className: classnames(className, variant === "custom" && !expanded ? classesCustom.accordion : "", variant === "custom" && expanded ? classesCustom.accordionSelected : ""),
    TransitionProps: {
      unmountOnExit: unmountOnExit
    }
  }, /*#__PURE__*/React.createElement(ExpansionPanelSummary, {
    expandIcon: variant === "custom" ? /*#__PURE__*/React.createElement(ChevronDown, {
      color: expanded ? color : "neutral"
    }) : expandIcon,
    classes: _objectSpread(_objectSpread({}, classes), {}, {
      expanded: variant === "custom" ? classesCustom.accordionExpanded : "",
      expandIcon: variant === "custom" ? classesCustom.accordionChevron : ""
    })
  }, variant === "custom" && /*#__PURE__*/React.createElement(Grid, {
    container: true,
    className: classesCustom.title,
    alignItems: "center",
    justify: "space-between"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    className: classesCustom.title
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "f1-14",
    weight: "bold",
    shade: 900
  }, title)), badge && /*#__PURE__*/React.createElement(Grid, {
    item: true
  }, /*#__PURE__*/React.createElement(Typography, {
    color: badgeProps.color,
    weight: "bold",
    variant: "f1-8",
    className: classesCustom.badge
  }, badgeProps.label)), !!subtitle && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, typeof subtitle === "string" && /*#__PURE__*/React.createElement(Typography, {
    variant: "f1-12",
    shade: 700,
    className: classesCustom.subtitle
  }, subtitle), typeof subtitle !== "string" && subtitle)), variant === "standard" && header), /*#__PURE__*/React.createElement(ExpansionPanelDetails, null, children));
};

Accordion.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  expandIcon: PropTypes.instanceOf(Object),
  header: PropTypes.node,
  title: PropTypes.string,
  subtitle: PropTypes.node,
  expanded: PropTypes.bool,
  badge: PropTypes.bool,
  badgeProps: PropTypes.exact({
    label: PropTypes.string,
    color: PropTypes.oneOf(colorsList)
  }),
  unmountOnExit: PropTypes.bool,
  variant: PropTypes.oneOf(["standard", "custom"]),
  color: PropTypes.oneOf(colorsList)
};
Accordion.defaultProps = {
  className: "",
  onChange: function onChange() {},
  expandIcon: /*#__PURE__*/React.createElement(Select, {
    size: 10
  }),
  header: "",
  title: "",
  subtitle: "",
  expanded: true,
  badge: false,
  badgeProps: {
    label: "Completed",
    color: "primary"
  },
  unmountOnExit: false,
  variant: "standard",
  color: "primary"
};
export default Accordion;