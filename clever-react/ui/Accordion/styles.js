import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    accordion: _objectSpread(_objectSpread({}, spacing["mb-4"]), {}, {
      border: "2px solid",
      borderColor: colors.neutral[300],
      boxShadow: "none",
      borderRadius: "8px !important"
    }),
    accordionSelected: function accordionSelected(_ref2) {
      var color = _ref2.color;
      return _objectSpread(_objectSpread({}, spacing["mb-4"]), {}, {
        border: "2px solid",
        borderColor: colors[color][500],
        boxShadow: "none",
        borderRadius: "8px !important"
      });
    },
    accordionExpanded: {
      minHeight: "48px !important",
      "& div": {
        minHeight: "fit-content !important"
      },
      "& div:first-child": {
        margin: "12px 0px"
      }
    },
    accordionChevron: {
      marginTop: "-3px",
      alignSelf: "flex-start"
    },
    title: {
      margin: "0px !important"
    },
    badge: function badge(_ref3) {
      var badgeProps = _ref3.badgeProps;
      return _objectSpread(_objectSpread(_objectSpread({}, spacing["p-1"]), spacing["ml-2"]), {}, {
        backgroundColor: colors[badgeProps.color][100],
        width: "fit-content",
        height: "fit-content",
        borderRadius: "8px"
      });
    },
    subtitle: _objectSpread(_objectSpread({}, spacing["mt-1"]), {}, {
      fontStyle: "italic"
    })
  };
};

export default makeStyles(styles);