/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from "react";
import PropTypes from "prop-types";
import MaterialTabs from "@material-ui/core/Tabs";
import makeStyles from "../styles/makeStyles";
import { getColorsLabels } from "../styles/colors";
var getClasses = makeStyles(function (_ref) {
  var colors = _ref.cleverUI.colors;
  return {
    flexContainer: {
      borderBottom: "solid 1px ".concat(colors.neutral[300])
    },
    indicator: function indicator(_ref2) {
      var color = _ref2.color;
      return {
        height: "3px",
        backgroundColor: colors[color][500]
      };
    }
  };
});
var colorsList = getColorsLabels(true);

var Tabs = function Tabs(props) {
  var children = props.children,
      value = props.value,
      onChange = props.onChange,
      color = props.color,
      variant = props.variant,
      scrollButtons = props.scrollButtons;
  var classes = getClasses({
    color: color
  });
  return /*#__PURE__*/React.createElement(MaterialTabs, {
    value: value,
    onChange: onChange,
    classes: {
      flexContainer: classes.flexContainer,
      indicator: classes.indicator
    },
    variant: variant,
    scrollButtons: scrollButtons
  }, children);
};

Tabs.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onChange: PropTypes.func,
  color: PropTypes.oneOf(colorsList),
  variant: PropTypes.oneOf(["standard", "scrollable"]),
  scrollButtons: PropTypes.oneOf(["auto", "desktop", "on", "off"])
};
Tabs.defaultProps = {
  onChange: function onChange() {},
  color: "primary",
  variant: "standard",
  scrollButtons: "auto"
};
export default Tabs;