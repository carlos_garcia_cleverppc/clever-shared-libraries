import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import MaterialButton from "@material-ui/core/Button";
import { getColorsLabels } from "../styles/colors";
import useMediaQuery from "../styles/useMediaQuery";
import { screenSm, screenXs } from "../styles/breakpoints";
import ArrowRight from "../Icon/ArrowRight";
import Grid from "../Grid";
import getClasses from "./styles";
var colorsList = getColorsLabels();

var RoundedButton = function RoundedButton(props) {
  var children = props.children,
      className = props.className,
      size = props.size,
      onClick = props.onClick;
  var href = props.href,
      disabled = props.disabled,
      target = props.target,
      ripple = props.ripple,
      disabledClickable = props.disabledClickable;
  var height = props.height,
      hover = props.hover;
  var color = props.color;
  var white = color === "white";
  if (white) color = "primary";
  var classes = getClasses({
    color: color
  });
  var isScreenSm = useMediaQuery(screenSm);
  var isScreenXs = useMediaQuery(screenXs);

  var getStyles = function getStyles() {
    var classNames = [];

    if (height === "narrow") {
      classNames.push(classes.narrow);
    } else {
      classNames.push(classes.normal);
    }

    if (size === "large") {
      classNames.push(classes.baseStyles);
      classNames.push(classes.large);
      classNames.push(classes.contained);
      if (disabled || disabledClickable) classNames.push(classes.containedDisabled);
      if (isScreenSm || isScreenXs) classNames.push(classes.mobileBaseStyles);
    } else {
      classNames.push(classes.baseStyles);
      classNames.push(classes.regular);
      classNames.push(classes.contained);
      if (disabled || disabledClickable) classNames.push(classes.containedDisabled);
      if (isScreenSm || isScreenXs) classNames.push(classes.mobileBaseStyles);
    }

    if (hover) classNames.push(classes.hover);
    if (white) classNames.push(classes.white);else classNames.push(classes.color);
    classNames.push(className);
    return classnames(classNames);
  };

  return /*#__PURE__*/React.createElement(MaterialButton, {
    className: getStyles(),
    size: size,
    onClick: onClick,
    href: href,
    disabled: disabled,
    target: target,
    disableRipple: !ripple
  }, size === "medium" && /*#__PURE__*/React.createElement("div", {
    className: classes.text
  }, children), size === "large" && /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center"
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: true,
    className: classes.text
  }, children), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    className: classes.iconContainer
  }, /*#__PURE__*/React.createElement(ArrowRight, {
    color: disabled || disabledClickable ? color : "white",
    shade: disabled || disabledClickable ? 300 : 500,
    size: 10
  }))));
};

RoundedButton.propTypes = {
  className: PropTypes.string,
  size: PropTypes.oneOf(["medium", "large"]),
  href: PropTypes.string,
  disabled: PropTypes.bool,
  disabledClickable: PropTypes.bool,
  target: PropTypes.string,
  onClick: PropTypes.func,
  hover: PropTypes.bool,
  ripple: PropTypes.bool,
  color: PropTypes.oneOf(colorsList)
};
RoundedButton.defaultProps = {
  className: "",
  size: "medium",
  href: "",
  disabled: false,
  disabledClickable: false,
  target: "",
  onClick: function onClick() {},
  hover: true,
  ripple: true,
  color: "primary"
};
export default RoundedButton;