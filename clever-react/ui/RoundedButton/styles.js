import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      typography = _ref$cleverUI.typography,
      spacing = _ref$cleverUI.spacing,
      shadows = _ref$cleverUI.shadows;
  var sizing = typography.sizing;
  return {
    baseStyles: {
      height: "48px",
      fontSize: typography.fontSizeBase.constant + typography.fontSizeBase.unit,
      fontFamily: typography.font2,
      fontWeight: "900",
      borderRadius: "8px",
      boxShadow: "none",
      "& svg": {
        height: "14px",
        width: "14px"
      },
      "& p": {
        fontWeight: "900"
      }
    },
    mobileBaseStyles: {
      width: "100%"
    },
    contained: function contained(_ref2) {
      var color = _ref2.color;
      return {
        backgroundColor: colors[color][500],
        color: colors.neutral[100]
      };
    },
    containedDisabled: function containedDisabled(_ref3) {
      var color = _ref3.color;
      return {
        color: "".concat(colors[color][300], " !important"),
        backgroundColor: colors[color][100]
      };
    },
    outlined: function outlined(_ref4) {
      var color = _ref4.color;
      return {
        color: colors[color][500],
        border: "solid"
      };
    },
    outlinedDisabled: function outlinedDisabled(_ref5) {
      var color = _ref5.color;
      return {
        color: "".concat(colors[color][300], " !important"),
        borderColor: colors[color][100]
      };
    },
    regular: {
      minWidth: "192px"
    },
    large: {
      minWidth: "256px"
    },
    iconContainer: _objectSpread(_objectSpread({}, spacing["pr-3"]), {}, {
      display: "flex"
    }),
    normal: {
      height: "48px",
      borderRadius: "24px"
    },
    narrow: {
      height: "34px",
      borderRadius: "17px",
      fontFamily: typography.fontFamilyBody,
      fontSize: sizing.body0
    },
    color: function color(_ref6) {
      var _color = _ref6.color;
      return {
        backgroundColor: _color,
        "&:hover": {
          backgroundColor: colors[_color][500]
        },
        "& p": {
          color: "".concat(colors.neutral[100], " !important")
        }
      };
    },
    hover: function hover(_ref7) {
      var color = _ref7.color;
      return {
        "&:hover": {
          backgroundColor: colors[color][700],
          boxShadow: shadows.hover
        }
      };
    },
    white: {
      color: colors.neutral[500],
      backgroundColor: "white !important",
      "&:hover": {
        backgroundColor: "white"
      }
    },
    text: _objectSpread({}, spacing["px-4"])
  };
};

export default makeStyles(styles);