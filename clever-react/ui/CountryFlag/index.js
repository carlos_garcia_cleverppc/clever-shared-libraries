import React from "react";
import ReactCountryFlag from "react-country-flag";
import PropTypes from "prop-types";

var CountryFlag = function CountryFlag(props) {
  var countryCode = props.countryCode,
      className = props.className,
      svg = props.svg;
  return /*#__PURE__*/React.createElement(ReactCountryFlag, {
    countryCode: countryCode,
    className: className,
    svg: svg
  });
};

CountryFlag.propTypes = {
  className: PropTypes.string,
  countryCode: PropTypes.string,
  svg: PropTypes.bool
};
CountryFlag.defaultProps = {
  className: "",
  countryCode: "",
  svg: false
};
export default CountryFlag;