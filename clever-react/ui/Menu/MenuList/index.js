import React from "react";
import PropTypes from "prop-types";
import MenuList from "@material-ui/core/MenuList"; // import styles from "./styles";

var CleverMenuList = function CleverMenuList(props) {
  var onClick = props.onClick,
      className = props.className,
      children = props.children,
      onKeyDown = props.onKeyDown,
      autoFocusItem = props.autoFocusItem,
      id = props.id;
  return /*#__PURE__*/React.createElement(MenuList, {
    className: className,
    onClick: onClick,
    onKeyDown: onKeyDown,
    id: id,
    autoFocusItem: autoFocusItem
  }, children);
};

CleverMenuList.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  onKeyDown: PropTypes.func
};
CleverMenuList.defaultProps = {
  className: "",
  onClick: function onClick() {},
  onKeyDown: function onKeyDown() {}
};
export default CleverMenuList;