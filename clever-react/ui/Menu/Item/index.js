import _extends from "@babel/runtime/helpers/extends";
import React from "react";
import PropTypes from "prop-types";
import MenuItem from "@material-ui/core/MenuItem"; // import styles from "./styles";

var CleverMenuItem = /*#__PURE__*/React.forwardRef(function (props, ref) {
  var variant = props.variant,
      onClick = props.onClick,
      className = props.className,
      children = props.children,
      value = props.value;

  if (variant === "header") {
    return /*#__PURE__*/React.createElement("div", {
      className: className,
      ref: ref
    }, children);
  }

  return /*#__PURE__*/React.createElement(MenuItem, _extends({}, props, {
    ref: ref,
    className: className,
    onClick: onClick,
    value: value
  }), children);
});
CleverMenuItem.propTypes = {
  className: PropTypes.string,
  variant: PropTypes.oneOf(["default", "header"]),
  onClick: PropTypes.func,
  value: PropTypes.oneOfType([PropTypes.number, PropTypes.string, PropTypes.bool])
};
CleverMenuItem.defaultProps = {
  className: "",
  variant: "default",
  onClick: function onClick() {},
  value: ""
};
export default CleverMenuItem;