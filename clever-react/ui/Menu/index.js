import React from "react";
import PropTypes from "prop-types";
import Menu from "@material-ui/core/Menu";

var CleverMenu = function CleverMenu(props) {
  var className = props.className,
      children = props.children,
      anchorEl = props.anchorEl,
      open = props.open,
      onClose = props.onClose;
  return /*#__PURE__*/React.createElement(Menu, {
    className: className,
    anchorEl: anchorEl,
    open: open,
    onClose: onClose
  }, children);
};

CleverMenu.propTypes = {
  className: PropTypes.string,
  onClose: PropTypes.func,
  open: PropTypes.bool,
  anchorEl: PropTypes.oneOfType([PropTypes.element, PropTypes.func, PropTypes.instanceOf(Object)])
};
CleverMenu.defaultProps = {
  className: "",
  onClose: function onClose() {},
  open: false,
  anchorEl: null
};
export default CleverMenu;