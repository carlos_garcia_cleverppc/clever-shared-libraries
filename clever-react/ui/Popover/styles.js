/* eslint-disable max-len */
import split from "lodash-es/split";
import makeStyles from "../styles/makeStyles";

var getLeftRoot = function getLeftRoot(placement, anchorEl, arrowSize) {
  var dimensions = anchorEl ? anchorEl.getBoundingClientRect() : {
    x: 0
  };
  if (split(placement, "-")[0] === "center") return split(placement, "-")[1] === "left" ? "-".concat(arrowSize / 2, "px !important") : "".concat(arrowSize / 2, "px !important");
  if (split(placement, "-")[1] === "left") return dimensions.x >= arrowSize ? "-".concat(arrowSize, "px !important") : "".concat(dimensions.x, "px !important");
  if (split(placement, "-")[1] === "center") return dimensions.x >= arrowSize ? "" : "".concat(dimensions.x, "px !important");
  if (split(placement, "-")[1] === "right") return dimensions.x >= arrowSize ? "".concat(arrowSize, "px !important") : "".concat(dimensions.x, "px !important");
  return "";
};

var getTopSpacing = function getTopSpacing(placement, arrowSize) {
  if (split(placement, "-")[0] === "top") return "-".concat(arrowSize / 2, "px !important");
  if (split(placement, "-")[0] === "bottom") return "".concat(arrowSize / 2, "px !important");
  return "";
};

var getPositionX = function getPositionX(placement, anchorEl, paperDimensions, arrowPosition, arrowSize) {
  /** Function to move arrow among X axis */
  if (split(placement, "-")[0] === "center") {
    if (split(placement, "-")[1] === "right") return "-".concat(arrowSize / 2, "px");
    if (split(placement, "-")[1] === "left") return "calc(100% + ".concat(arrowSize / 2, "px)");
  } else {
    var dimensions = anchorEl ? anchorEl.getBoundingClientRect() : {
      x: 0
    };
    var dimensionsPaper = paperDimensions ? paperDimensions.getBoundingClientRect() : {
      x: 0
    }; // Distance in X axis from the upper left corner of paper to the upper left corner of button

    var moveX = dimensions.x - dimensionsPaper.x;
    if (arrowPosition === "left") return "".concat(moveX + 8 + arrowSize / 2, "px"); // Plus 8 for borderRadius, 16 for the middle of the arrow

    if (arrowPosition === "right") return "".concat(moveX + dimensions.width - 8 - arrowSize / 2, "px"); // Minus 8 for borderRadius

    return "".concat(moveX + dimensions.width / 2, "px");
  }

  return "";
};

var getPositionY = function getPositionY(placement, arrowSize) {
  if (split(placement, "-")[0] === "center") return "50%";
  if (split(placement, "-")[0] === "top") return "100%";
  return split(placement, "-")[0] === "bottom" ? "-".concat(arrowSize, "px") : "0%";
};

function getColor(colors, color, shade) {
  switch (color) {
    case "inherit":
      return "inherit";

    case "white":
      return "white";

    default:
      return colors[color][shade];
  }
}

var styles = function styles(_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      shadows = _ref$cleverUI.shadows;
  return {
    root: function root(_ref2) {
      var placement = _ref2.placement,
          arrow = _ref2.arrow,
          anchorEl = _ref2.anchorEl,
          arrowSize = _ref2.arrowSize;
      return {
        zIndex: "900 !important",
        top: !arrow ? "" : "".concat(getTopSpacing(placement, arrowSize)),
        left: !arrow ? "" : "".concat(getLeftRoot(placement, anchorEl, arrowSize))
      };
    },
    paper: function paper(_ref3) {
      var placement = _ref3.placement,
          arrow = _ref3.arrow,
          backgroundColor = _ref3.backgroundColor,
          anchorEl = _ref3.anchorEl,
          arrowSize = _ref3.arrowSize,
          paperDimensions = _ref3.paperDimensions,
          arrowColor = _ref3.arrowColor,
          arrowColorShade = _ref3.arrowColorShade,
          arrowPosition = _ref3.arrowPosition;
      return {
        backgroundColor: getColor(colors, backgroundColor, 500),
        boxShadow: shadows.normal,
        overflow: "unset",
        borderRadius: "8px",
        transform: "none !important",
        "&:after": {
          content: !arrow ? "" : "''",
          position: !arrow ? "" : "absolute",
          top: !arrow ? "" : "".concat(getPositionY(placement, arrowSize)),
          left: !arrow ? "" : "".concat(getPositionX(placement, anchorEl, paperDimensions, arrowPosition, arrowSize)),
          zIndex: !arrow ? "" : "1",
          border: !arrow ? "" : "solid ".concat(arrowSize / 2, "px transparent"),
          borderTopColor: arrow && "".concat(split(placement, "-")[0]) === "top" ? getColor(colors, arrowColor, arrowColorShade) : "",
          borderRightColor: arrow && "".concat(split(placement, "-")[0]) === "center" && "".concat(split(placement, "-")[1]) === "right" ? getColor(colors, arrowColor, arrowColorShade) : "",
          borderLeftColor: arrow && "".concat(split(placement, "-")[0]) === "center" && "".concat(split(placement, "-")[1]) === "left" ? getColor(colors, arrowColor, arrowColorShade) : "",
          borderBottomColor: arrow && "".concat(split(placement, "-")[0]) === "bottom" ? getColor(colors, arrowColor, arrowColorShade) : "",
          marginLeft: !arrow ? "" : "-".concat(arrowSize / 2, "px"),
          marginTop: arrow && "".concat(split(placement, "-")[0]) === "center" ? "-".concat(arrowSize / 2, "px") : ""
        }
      };
    }
  };
};

export default makeStyles(styles);