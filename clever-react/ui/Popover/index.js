import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import React, { useState } from "react";
import classnames from "classnames";
import PropTypes from "prop-types";
import split from "lodash-es/split";
import MaterialPopover from "@material-ui/core/Popover";
import getClasses from "./styles";

var Popover = function Popover(props) {
  var children = props.children,
      open = props.open,
      id = props.id,
      anchorEl = props.anchorEl,
      arrow = props.arrow,
      arrowColor = props.arrowColor,
      arrowColorShade = props.arrowColorShade,
      arrowPosition = props.arrowPosition,
      arrowSize = props.arrowSize;
  var placement = props.placement,
      className = props.className,
      classNameRoot = props.classNameRoot,
      onClose = props.onClose,
      backgroundColor = props.backgroundColor,
      disableScrollLock = props.disableScrollLock,
      disablePortal = props.disablePortal;

  var _useState = useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      paperDimensions = _useState2[0],
      setPaperDimensions = _useState2[1];

  var classes = getClasses({
    placement: placement,
    arrow: arrow,
    backgroundColor: backgroundColor,
    anchorEl: anchorEl,
    paperDimensions: paperDimensions,
    arrowColor: arrowColor,
    arrowColorShade: arrowColorShade,
    arrowPosition: arrowPosition,
    arrowSize: arrowSize
  });
  var positionAnchor = split(placement, "-")[0];
  var alignAnchor = split(placement, "-")[1];

  var getInversePosition = function getInversePosition(position) {
    if (position === "top") return "bottom";
    if (position === "bottom") return "top";
    return "center";
  };

  var getInverseAlign = function getInverseAlign(align) {
    if (align === "left") return "right";
    if (align === "right") return "left";
    return "center";
  };

  if (!anchorEl) return /*#__PURE__*/React.createElement("div", null);
  return /*#__PURE__*/React.createElement(MaterialPopover, {
    id: id,
    open: open,
    anchorEl: anchorEl,
    onClose: onClose,
    TransitionProps: {
      onEntering: function onEntering() {
        return setPaperDimensions(document.querySelector("#".concat(id)).children[2]);
      }
    },
    anchorOrigin: {
      vertical: positionAnchor,
      horizontal: alignAnchor
    },
    transformOrigin: {
      vertical: getInversePosition(positionAnchor),
      horizontal: positionAnchor === "center" ? getInverseAlign(alignAnchor) : alignAnchor
    },
    classes: {
      paper: classnames(anchorEl ? classes.paper : "", className),
      root: classnames(classes.root, classNameRoot)
    },
    transitionDuration: 250,
    disableScrollLock: disableScrollLock,
    disablePortal: disablePortal
  }, children);
};

Popover.propTypes = {
  arrow: PropTypes.bool,
  placement: PropTypes.oneOf(["top-left", "top-center", "top-right", "bottom-left", "bottom-center", "bottom-right", "center-left", "center-right"]),
  anchorEl: PropTypes.instanceOf(Node),
  children: PropTypes.instanceOf(Object).isRequired,
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  className: PropTypes.string,
  classNameRoot: PropTypes.string,
  id: PropTypes.string,
  backgroundColor: PropTypes.string,
  arrowColor: PropTypes.string,
  arrowColorShade: PropTypes.number,
  arrowPosition: PropTypes.oneOf(["left", "center", "right"]),
  arrowSize: PropTypes.number,
  disablePortal: PropTypes.bool,
  disableScrollLock: PropTypes.bool
};
Popover.defaultProps = {
  arrow: false,
  placement: "bottom-center",
  className: "",
  classNameRoot: "",
  id: "",
  backgroundColor: "white",
  arrowColor: "white",
  anchorEl: null,
  arrowColorShade: 500,
  arrowPosition: "center",
  arrowSize: 32,
  disablePortal: false,
  disableScrollLock: false
};
export default Popover;