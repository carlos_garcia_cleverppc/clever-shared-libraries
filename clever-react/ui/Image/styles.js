import _defineProperty from "@babel/runtime/helpers/defineProperty";
import _slicedToArray from "@babel/runtime/helpers/slicedToArray";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import isString from "lodash-es/isString";
import makeStyles from "../styles/makeStyles";

function buildImgSize(size) {
  if (isString(size) && size.indexOf("x") !== -1) {
    var _size$split = size.split("x"),
        _size$split2 = _slicedToArray(_size$split, 2),
        width = _size$split2[0],
        height = _size$split2[1];

    return {
      width: "".concat(width, "px"),
      height: "".concat(height, "px")
    };
  }

  if (size === "responsive") {
    return {
      height: "auto",
      width: "100%"
    };
  }

  if (size === "auto") {
    return {
      height: "auto",
      width: "auto"
    };
  }

  return {
    height: "".concat(size, "px"),
    width: "".concat(size, "px")
  };
}

export default makeStyles(function (_ref) {
  var spacing = _ref.cleverUI.spacing;
  return {
    root: function root(_ref2) {
      var size = _ref2.size;
      return _objectSpread({}, buildImgSize(size));
    },
    circle: {
      display: "block",
      borderRadius: "50%",
      backgroundColor: "#fff",
      verticalAlign: "baseline"
    },
    center: _objectSpread(_objectSpread({}, spacing["mx-auto"]), {}, {
      display: "block"
    })
  };
});