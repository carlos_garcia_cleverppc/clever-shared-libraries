/* eslint-disable react/destructuring-assignment */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import getClasses from "./styles";

var Image = function Image(_ref) {
  var className = _ref.className,
      src = _ref.src,
      variant = _ref.variant,
      alt = _ref.alt,
      center = _ref.center,
      size = _ref.size;
  var classes = getClasses({
    size: size
  });
  return /*#__PURE__*/React.createElement("img", {
    className: classnames(className, classes.root, classes[variant], center ? classes.center : ""),
    src: src,
    alt: alt
  });
};

Image.propTypes = {
  className: PropTypes.string,
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
  center: PropTypes.bool,
  variant: PropTypes.oneOf(["default", "circle"]),
  size: function size(props, propName, componentName) {
    if (props[propName] !== "responsive" && props[propName] !== "auto" && !/^\d+$/.test(props[propName]) && !/^\d+x\d+/.test(props[propName])) {
      return new Error("Invalid prop `".concat(propName, "` supplied to ").concat(componentName, ". Validation failed."));
    }

    return null;
  }
};
Image.defaultProps = {
  className: "",
  variant: "default",
  size: "responsive",
  alt: "",
  center: true
};
export default Image;