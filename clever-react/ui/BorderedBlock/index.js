/* eslint-disable no-extra-boolean-cast */
import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import Card from "../Card";
import Grid from "../Grid";
import Link from "../Link";
import Typography from "../Typography";
import Divider from "../Divider";
import Image from "../Image";
import ArrowRight from "../Icon/ArrowRight";
import { getColorsLabels, getShades } from "../styles/colors";
import { screenSm, screenXs } from "../styles/breakpoints";
import useMediaQuery from "../styles/useMediaQuery";
import getClasses from "./styles";
var colorsList = getColorsLabels(true);
var shadesList = getShades();

var BorderedBlock = function BorderedBlock(props) {
  var image = props.image,
      title = props.title,
      description = props.description,
      quantityImage = props.quantityImage,
      textImage = props.textImage,
      content = props.content;
  var actionText = props.actionText,
      actionOnClick = props.actionOnClick,
      borderColor = props.borderColor,
      borderShade = props.borderShade,
      textBadge = props.textBadge,
      children = props.children;
  var footerContent = props.footerContent,
      titleLines = props.titleLines,
      descriptionLines = props.descriptionLines,
      footerLines = props.footerLines,
      className = props.className;
  var isScreenSm = useMediaQuery(screenSm);
  var isScreenXs = useMediaQuery(screenXs);
  var isMobile = isScreenSm || isScreenXs;
  var width = isMobile ? 2 : 4;
  var classes = getClasses({
    borderColor: borderColor,
    borderShade: borderShade,
    width: width,
    titleLines: titleLines,
    descriptionLines: descriptionLines,
    footerLines: footerLines,
    isMobile: isMobile
  });
  return /*#__PURE__*/React.createElement(Card, {
    className: classnames(classes.card, className)
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    alignItems: "center",
    className: classes.container
  }, textBadge !== "" && /*#__PURE__*/React.createElement(React.Fragment, null, width > 2 && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12,
    className: classes.badge
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "f1-10",
    className: classes.textBadge
  }, textBadge), /*#__PURE__*/React.createElement("div", {
    className: classes.wrapper
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.dot
  }), /*#__PURE__*/React.createElement("div", {
    className: classes.ripple
  }))), width <= 2 && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, /*#__PURE__*/React.createElement(Grid, {
    container: true,
    spacing: 1,
    justify: "center",
    alignItems: "stretch",
    className: classes.metricContainer
  }, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 6,
    className: classes.imageItem
  }, /*#__PURE__*/React.createElement(Image, {
    src: image,
    center: true,
    className: classes.image
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 6,
    className: classes.metricItem
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.badge
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "f1-10",
    className: classes.textBadge
  }, textBadge), /*#__PURE__*/React.createElement("div", {
    className: classes.wrapper
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.dot
  }), /*#__PURE__*/React.createElement("div", {
    className: classes.ripple
  }))), (!!textImage || !!quantityImage) && /*#__PURE__*/React.createElement("div", {
    className: classes.imageInfo
  }, quantityImage !== undefined && /*#__PURE__*/React.createElement(Typography, {
    variant: "f1-40",
    weight: "bold",
    shade: "darker"
  }, quantityImage), !!textImage && /*#__PURE__*/React.createElement(Typography, {
    variant: "f2-10",
    shade: "darker"
  }, textImage)))))), width > 2 && /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 4,
    className: classes.firstItem
  }, /*#__PURE__*/React.createElement(Image, {
    src: image,
    center: true,
    className: classes.image
  }), (!!textImage || !!quantityImage) && /*#__PURE__*/React.createElement("div", {
    className: classes.imageInfo
  }, quantityImage !== undefined && /*#__PURE__*/React.createElement(Typography, {
    variant: "f1-40",
    weight: "bold",
    shade: "darker"
  }, quantityImage), !!textImage && /*#__PURE__*/React.createElement(Typography, {
    variant: "f2-10",
    shade: "darker"
  }, textImage))), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: width <= 2 ? 12 : 8
  }, /*#__PURE__*/React.createElement(Typography, {
    variant: "h4",
    shade: "darker",
    className: classes.title
  }, title), !!content && /*#__PURE__*/React.createElement("div", {
    className: classes.contentDiv
  }, content), /*#__PURE__*/React.createElement(Typography, {
    variant: "body0",
    gutterBottom: true,
    className: classes.description
  }, description), !!children ? children : null, actionText !== "" && /*#__PURE__*/React.createElement(Link, {
    onClick: actionOnClick,
    color: "neutral",
    className: classes.link
  }, actionText, /*#__PURE__*/React.createElement(ArrowRight, {
    size: 12,
    className: classes.iconMargin
  }))), !!footerContent && /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement(Grid, {
    item: true,
    xs: 12
  }, /*#__PURE__*/React.createElement(Divider, {
    dashed: true,
    className: classes.divider
  })), /*#__PURE__*/React.createElement(Grid, {
    item: true,
    variant: "body0",
    xs: 12
  }, footerContent))));
};

BorderedBlock.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  quantityImage: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  textImage: PropTypes.string,
  actionText: PropTypes.string,
  actionOnClick: PropTypes.instanceOf(Object),
  borderColor: PropTypes.oneOf(colorsList),
  borderShade: PropTypes.oneOf(shadesList),
  textBadge: PropTypes.string,
  titleLines: PropTypes.number,
  descriptionLines: PropTypes.number
};
BorderedBlock.defaultProps = {
  image: "",
  title: "",
  description: "",
  quantityImage: undefined,
  textImage: "",
  actionText: "",
  actionOnClick: function actionOnClick() {},
  borderColor: "neutral",
  borderShade: 500,
  textBadge: "",
  titleLines: 3,
  descriptionLines: 4
};
export default BorderedBlock;