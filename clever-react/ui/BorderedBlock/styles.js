import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";
var styles = makeStyles(function (_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing;
  return {
    card: function card(_ref2) {
      var borderColor = _ref2.borderColor,
          borderShade = _ref2.borderShade,
          width = _ref2.width;
      return {
        borderColor: colors[borderColor][borderShade],
        borderWidth: "2px",
        width: "".concat(160 * width + (width - 1) * 24, "px")
      };
    },
    container: _objectSpread(_objectSpread({}, spacing["py-5"]), spacing["px-4"]),
    firstItem: _objectSpread({
      textAlign: "center"
    }, spacing["pr-4"]),
    imageInfo: _objectSpread({}, spacing["mnt-4"]),
    title: _objectSpread({}, spacing["mb-2"]),
    link: {
      fontSize: "14px !important",
      textDecoration: "underline !important",
      display: "flex",
      alignItems: "center"
    },
    badge: {
      textAlign: "right",
      display: "flex",
      justifyContent: "flex-end"
    },
    textBadge: _objectSpread(_objectSpread({}, spacing["pr-6"]), {}, {
      marginTop: "-5px",
      letterSpacing: "2px"
    }),
    "@keyframes ripple": {
      "0%": {
        boxShadow: "inset 0 0 0 12px currentColor"
      },
      "100%": {
        transform: "scale(3)",
        boxShadow: "inset 0 0 0 0 currentColor"
      }
    },
    wrapper: {
      height: "12px",
      width: "12px",
      transform: "translate(-50%, -50%)",
      position: "absolute"
    },
    dot: {
      height: "100%",
      width: "100%",
      backgroundColor: colors.secondary[500],
      borderRadius: "50%",
      position: "relative",
      zIndex: 1
    },
    ripple: {
      position: "absolute",
      top: 0,
      right: 0,
      color: colors.secondary[100],
      height: "100%",
      width: "100%",
      borderRadius: "50%",
      animation: "$ripple 1600ms infinite 800ms cubic-bezier(0.64, 0.42, 0.54, 1)",
      "&:last-child": {
        animationDelay: "-1600ms"
      },
      "@media (prefers-reduced-motion)": {
        animationPlayState: "paused",
        animationDelay: "-1400ms",
        "&:last-child": {
          animationDelay: "-800ms"
        }
      }
    },
    iconMargin: _objectSpread({}, spacing["ml-2"]),
    copyContent: _objectSpread(_objectSpread({
      backgroundColor: colors.neutral[100],
      borderRadius: "5%"
    }, spacing["p-5"]), spacing["my-4"]),
    copyButton: {
      position: "absolute",
      top: "68px",
      right: "35px"
    },
    contentDiv: _objectSpread({}, spacing["my-2"]),
    description: function description(_ref3) {
      var descriptionLines = _ref3.descriptionLines;
      return _objectSpread(_objectSpread(_objectSpread({}, spacing["mb-2"]), spacing["pr-1"]), {}, {
        overflow: "hidden",
        textOverflow: "ellipsis",
        display: "-webkit-box",
        "-webkit-line-clamp": descriptionLines,
        "-webkit-box-orient": "vertical"
      });
    },
    divider: function divider(_ref4) {
      var width = _ref4.width;
      return {
        marginLeft: "-14px",
        width: "".concat(160 * width + (width - 1) * 24, "px")
      };
    },
    image: function image(_ref5) {
      var isMobile = _ref5.isMobile;
      return {
        display: isMobile ? "flex" : "",
        maxWidth: "270px",
        marginTop: isMobile ? "" : "auto",
        maxHeight: "120px",
        marginBottom: isMobile ? "" : "auto"
      };
    },
    metricContainer: {
      minHeight: "140px"
    },
    imageItem: {
      margin: "auto",
      textAlign: "center"
    },
    metricItem: {
      display: "grid",
      textAlign: "center"
    }
  };
});
export default styles;