var style = document.createElement("style");
style.innerHTML = "._onboarding-disable-scroll { overflow: hidden }";
document.getElementsByTagName("head")[0].appendChild(style);
export var disableScroll = function disableScroll() {
  return document.body.classList.add("_onboarding-disable-scroll");
};
export var enableScroll = function enableScroll() {
  return document.body.classList.remove("_onboarding-disable-scroll");
};