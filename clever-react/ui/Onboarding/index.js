import _asyncToGenerator from "@babel/runtime/helpers/asyncToGenerator";
import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import _regeneratorRuntime from "@babel/runtime/regenerator";
import React, { useState, useCallback, useRef, useEffect, useLayoutEffect } from "react";
import PropTypes from "prop-types";
import FocusLock, { MoveFocusInside } from "react-focus-lock";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import { getColorsLabels } from "../styles/colors";
import Button from "../Link";
import Typography from "../Typography";
import Backdrop from "../Backdrop";
import Portal from "./components/Portal";
import Badge from "./components/Badge";
import Dot from "./components/Dot";
import Popover from "./components/Popover";
import useStyles from "./styles";
import { getCoords } from "./utilsDom";
import { enableScroll, disableScroll } from "./scrollLock";

var createAnchorEl = function createAnchorEl(_ref) {
  var x = _ref.x,
      y = _ref.y;
  return {
    clientWidth: 0,
    clientHeight: 0,
    getBoundingClientRect: function getBoundingClientRect() {
      return {
        left: x,
        right: x,
        top: y,
        bottom: y
      };
    }
  };
};

var colorsList = getColorsLabels(true);

var Onboarding = function Onboarding(props) {
  var color = props.color,
      open = props.open,
      steps = props.steps,
      onClose = props.onClose,
      FocusLockDisabled = props.FocusLockDisabled;
  var nextText = props.nextText,
      previousText = props.previousText,
      finishText = props.finishText;
  var classes = useStyles();

  var _useState = useState(0),
      _useState2 = _slicedToArray(_useState, 2),
      current = _useState2[0],
      _setCurrent = _useState2[1];

  var _useState3 = useState(null),
      _useState4 = _slicedToArray(_useState3, 2),
      position = _useState4[0],
      setPosition = _useState4[1];

  var _useState5 = useState(false),
      _useState6 = _slicedToArray(_useState5, 2),
      isMoving = _useState6[0],
      setMoving = _useState6[1];

  var popperRef = useRef();

  var isCompleted = function isCompleted(arr, currentPosition) {
    return !arr[currentPosition];
  };

  var setCurrent = useCallback(function (newPosition) {
    return _setCurrent(function (lastPosition) {
      var step = steps[lastPosition];

      if (step.onAfter) {
        setMoving(true);
        step.onAfter().then(function () {
          return setMoving(false);
        });
      }

      return newPosition;
    });
  }, [steps]);
  useEffect(function () {
    if (open) {
      disableScroll();
    } else {
      enableScroll();
    }
  }, [open]);
  useLayoutEffect(function () {
    function loadNext() {
      return _loadNext.apply(this, arguments);
    }

    function _loadNext() {
      _loadNext = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime.mark(function _callee() {
        var step, coords;
        return _regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                setMoving(true);
                step = steps[current];

                if (!step.onBefore) {
                  _context.next = 5;
                  break;
                }

                _context.next = 5;
                return step.onBefore();

              case 5:
                coords = getCoords(step.selector, {
                  placement: step.placement
                });
                setPosition(coords);
                setMoving(false);

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));
      return _loadNext.apply(this, arguments);
    }

    if (open) loadNext();
  }, [current, steps, open]);

  var handleClose = function handleClose() {
    onClose();
    setCurrent(0);
  };

  var next = function next(stepNumber) {
    if (isCompleted(steps, stepNumber)) {
      handleClose();
    } else {
      setCurrent(stepNumber);
    }
  };

  if (!open) {
    return null;
  }

  return /*#__PURE__*/React.createElement(Portal, null, /*#__PURE__*/React.createElement(Backdrop, {
    className: classes.backdrop,
    open: true
  }, !isMoving && position && /*#__PURE__*/React.createElement(FocusLock, {
    shards: [popperRef],
    disabled: FocusLockDisabled
  }, /*#__PURE__*/React.createElement("div", {
    style: {
      position: "absolute",
      top: position.y,
      left: position.x
    }
  }, /*#__PURE__*/React.createElement(Badge, {
    color: color
  }), /*#__PURE__*/React.createElement(ClickAwayListener, {
    onClickAway: handleClose
  }, /*#__PURE__*/React.createElement(Popover, {
    open: true,
    ref: popperRef,
    anchorEl: createAnchorEl(position)
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.header
  }, /*#__PURE__*/React.createElement(Typography, {
    component: "div",
    variant: "f1-14",
    shade: 900,
    weight: "bold",
    gutterBottom: true
  }, steps[current].title)), /*#__PURE__*/React.createElement(Typography, {
    component: "div",
    variant: "f2-14"
  }, steps[current].content), /*#__PURE__*/React.createElement(MoveFocusInside, null, /*#__PURE__*/React.createElement("div", {
    className: classes.navigation
  }, /*#__PURE__*/React.createElement(Button, {
    disabled: current === 0,
    onClick: function onClick() {
      return next(current - 1);
    }
  }, previousText), /*#__PURE__*/React.createElement("div", {
    className: classes.dots
  }, steps.map(function (_, index) {
    return /*#__PURE__*/React.createElement(Dot, {
      key: index,
      color: color,
      actived: index === current
    });
  })), /*#__PURE__*/React.createElement(Button, {
    "data-autofocus": true,
    onClick: function onClick() {
      return next(current + 1);
    }
  }, current === steps.length - 1 ? finishText : nextText)))))))));
};

Onboarding.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  color: PropTypes.oneOf(colorsList),
  steps: PropTypes.arrayOf(PropTypes.exact({
    selector: PropTypes.string.isRequired,
    title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
    content: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
    placement: PropTypes.oneOf(["left", "right", "top", "bottom"]),
    onAfter: PropTypes.func,
    onBefore: PropTypes.func
  })).isRequired,
  nextText: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  finishText: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  previousText: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  FocusLockDisabled: PropTypes.bool
};
Onboarding.defaultProps = {
  open: false,
  color: "primary",
  onClose: function onClose() {},
  finishText: "Finish",
  previousText: "Previous",
  nextText: "Next",
  FocusLockDisabled: false
};
export default Onboarding;