import { useEffect, useRef } from "react";
import { createPortal } from "react-dom";

var Portal = function Portal(_ref) {
  var children = _ref.children;
  var ref = useRef(null);

  if (ref.current === null) {
    ref.current = document.createElement("div");
    ref.current.setAttribute("id", "_clever-tour");
  }

  useEffect(function () {
    document.body.appendChild(ref.current);
    return function () {
      document.body.removeChild(ref.current);
    };
  }, [ref]);
  return /*#__PURE__*/createPortal(children, ref.current);
};

export default Portal;