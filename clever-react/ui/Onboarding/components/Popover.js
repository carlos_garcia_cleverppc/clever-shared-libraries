import _slicedToArray from "@babel/runtime/helpers/slicedToArray";
import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import React, { useState } from "react";
import PropTypes from "prop-types";
import Popper from "@material-ui/core/Popper";
import makeStyles from "../../styles/makeStyles";
var useStyles = makeStyles(function (_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      colors = _ref$cleverUI.colors,
      spacing = _ref$cleverUI.spacing,
      zIndex = _ref.zIndex;
  return {
    paper: _objectSpread(_objectSpread({}, spacing["p-6"]), {}, {
      backgroundColor: "white",
      maxWidth: "90vw",
      width: "400px",
      borderRadius: 8,
      boxSizing: "border-box"
    }),
    popper: {
      zIndex: zIndex.tooltip,
      "&[x-placement*=\"bottom\"] $arrow": {
        top: 0,
        left: 0,
        marginTop: "-10px",
        "&::before": {
          borderWidth: "0 10px 10px 10px",
          borderColor: "transparent transparent white transparent"
        }
      },
      "&[x-placement*=\"top\"] $arrow": {
        bottom: 0,
        left: 0,
        marginBottom: "-10px",
        "&::before": {
          borderWidth: "10px 10px 0 10px",
          borderColor: "white transparent transparent transparent"
        }
      }
    },
    arrow: {
      position: "absolute",
      "&::before": {
        content: "\"\"",
        margin: "auto",
        display: "block",
        width: 0,
        height: 0,
        borderStyle: "solid"
      }
    }
  };
});
var Popover = /*#__PURE__*/React.forwardRef(function (_ref2, ref) {
  var anchorEl = _ref2.anchorEl,
      children = _ref2.children;
  var classes = useStyles();

  var _useState = useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      arrowRef = _useState2[0],
      setArrowRef = _useState2[1];

  return /*#__PURE__*/React.createElement(Popper, {
    ref: ref,
    open: Boolean(anchorEl),
    anchorEl: anchorEl,
    className: classes.popper,
    placement: "bottom-start",
    modifiers: {
      flip: {
        behavior: ["bottom", "top"]
      },
      offset: {
        offset: "-40,36"
      },
      preventOverflow: {
        enabled: true,
        boundariesElement: "scrollParent"
      },
      arrow: {
        enabled: true,
        element: arrowRef
      }
    }
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.paper
  }, children), /*#__PURE__*/React.createElement("span", {
    className: classes.arrow,
    ref: setArrowRef
  }));
});
Popover.propTypes = {
  anchorEl: PropTypes.exact({
    clientWidth: PropTypes.number.isRequired,
    clientHeight: PropTypes.number.isRequired,
    getBoundingClientRect: PropTypes.func.isRequired
  }).isRequired
};
export default Popover;