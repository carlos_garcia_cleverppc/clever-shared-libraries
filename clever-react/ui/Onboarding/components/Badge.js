/* eslint-disable react/no-unused-prop-types */
import React from "react";
import PropTypes from "prop-types";
import makeStyles from "../../styles/makeStyles";
import { getColorsLabels } from "../../styles/colors";
var duration = 1600;
var colorsList = getColorsLabels();
var useStyles = makeStyles(function (_ref) {
  var colors = _ref.cleverUI.colors;
  return {
    "@keyframes ripple": {
      "0%": {
        boxShadow: "inset 0 0 0 14px currentColor"
      },
      "100%": {
        transform: "scale(3)",
        boxShadow: "inset 0 0 0 0 currentColor"
      }
    },
    wrapper: {
      height: function height(props) {
        return props.size;
      },
      width: function width(props) {
        return props.size;
      },
      transform: "translate(-50%, -50%)",
      position: "absolute"
    },
    dot: {
      height: "100%",
      width: "100%",
      backgroundColor: function backgroundColor(props) {
        return colors[props.color][500];
      },
      borderRadius: "50%",
      position: "relative",
      zIndex: 1
    },
    ripple: {
      position: "absolute",
      top: 0,
      left: 0,
      color: function color(props) {
        return colors[props.color][300];
      },
      height: "100%",
      width: "100%",
      borderRadius: "50%",
      animation: "$ripple ".concat(duration, "ms infinite ").concat(duration / 2, "ms cubic-bezier(0.64, 0.42, 0.54, 1)"),
      "&:last-child": {
        animationDelay: "-".concat(duration, "ms")
      },
      "@media (prefers-reduced-motion)": {
        animationPlayState: "paused",
        animationDelay: "-".concat(duration - 200, "ms"),
        "&:last-child": {
          animationDelay: "-".concat(duration / 2, "ms")
        }
      }
    }
  };
});
var Badge = /*#__PURE__*/React.forwardRef(function (props, ref) {
  var classes = useStyles(props);
  return /*#__PURE__*/React.createElement("div", {
    ref: ref,
    className: classes.wrapper
  }, /*#__PURE__*/React.createElement("div", {
    className: classes.dot
  }), /*#__PURE__*/React.createElement("div", {
    className: classes.ripple
  }), /*#__PURE__*/React.createElement("div", {
    className: classes.ripple
  }));
});
Badge.propTypes = {
  size: PropTypes.number,
  color: PropTypes.oneOf(colorsList)
};
Badge.defaultProps = {
  size: 14,
  color: "primary"
};
export default Badge;