import React from "react";
import PropTypes from "prop-types";
import makeStyles from "../../styles/makeStyles";
import { getColorsLabels } from "../../styles/colors";
var colorsList = getColorsLabels(true);
var useStyles = makeStyles(function (_ref) {
  var colors = _ref.cleverUI.colors;
  return {
    wrapper: {
      width: 11,
      padding: 0,
      height: 24,
      flex: "0 1 11px",
      display: "inline-flex",
      border: 0,
      backgroundColor: "transparent",
      color: function color(props) {
        return props.actived ? colors[props.color][700] : colors[props.color][300];
      }
    },
    dot: {
      backgroundColor: "currentColor",
      height: 4,
      width: 4,
      margin: "auto",
      borderRadius: 9999
    }
  };
});

var Dot = function Dot(_ref2) {
  var actived = _ref2.actived,
      color = _ref2.color;
  var classes = useStyles({
    actived: actived,
    color: color
  });
  return /*#__PURE__*/React.createElement("div", {
    className: classes.wrapper
  }, /*#__PURE__*/React.createElement("span", {
    className: classes.dot
  }));
};

Dot.propTypes = {
  color: PropTypes.oneOf(colorsList),
  actived: PropTypes.bool.isRequired
};
Dot.defaultProps = {
  color: "primary"
};
export default Dot;