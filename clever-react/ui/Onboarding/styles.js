import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import makeStyles from "../styles/makeStyles";
var useStyles = makeStyles(function (_ref) {
  var _ref$cleverUI = _ref.cleverUI,
      spacing = _ref$cleverUI.spacing,
      typography = _ref$cleverUI.typography,
      zIndex = _ref.zIndex;
  var sizing = typography.sizing;
  return {
    backdrop: {
      zIndex: zIndex.modal + 1
    },
    header: spacing["pt-3"],
    navigation: _objectSpread(_objectSpread({}, spacing["pt-4"]), {}, {
      display: "grid",
      gridTemplateColumns: "1fr auto 1fr",
      alignItems: "center",
      justifyItems: "center",
      "& > :first-child": {
        justifySelf: "start"
      },
      "& > :last-child": {
        justifySelf: "end"
      },
      "& > a": {
        fontSize: sizing.body0
      }
    }),
    dots: {
      display: "flex"
    }
  };
});
export default useStyles;