/* eslint-disable import/prefer-default-export */
export function getCoords(selector) {
  var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref$gap = _ref.gap,
      gap = _ref$gap === void 0 ? 32 : _ref$gap,
      _ref$placement = _ref.placement,
      placement = _ref$placement === void 0 ? "right" : _ref$placement;

  var element = document.querySelector(selector);
  var coordinates = element.getBoundingClientRect();

  switch (placement) {
    case "left":
    case "right":
      return {
        y: coordinates.top + coordinates.height / 2,
        x: placement === "right" ? coordinates.right + gap : coordinates.left - gap
      };

    case "top":
    case "bottom":
      return {
        x: coordinates.left + coordinates.width / 2,
        y: placement === "bottom" ? coordinates.bottom + gap : coordinates.top - gap
      };

    default:
      return {
        x: 0,
        y: 0
      };
  }
}