export const StylesDecorator = (storyFn) => (
  <ThemeProvider theme={customTheme}>
    <StylesProvider injectFirst>{storyFn()}</StylesProvider>
  </ThemeProvider>
);
