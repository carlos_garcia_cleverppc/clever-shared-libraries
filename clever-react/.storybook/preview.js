import React from "react";
import Provider from "../src/ui/Provider";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
};

export const decorators = [
  (Story) => (
    <Provider theme={{}}>
      <Story />
    </Provider>
  ),
];
