import { addons } from "@storybook/addons";
import { create } from "@storybook/theming";
import pkg from "../package.json";

const theme = create({
  base: "light",
  brandTitle: `Clever React v${pkg.version}`,
});

addons.setConfig({ theme });
