import forEach from "lodash-es/forEach";
export var actions = {
  LAUNCH_ERROR: "LAUNCH_ERROR"
};

function hasError(payload) {
  var _payload$error;

  return !!payload && Object.keys(payload).length > 0 && (payload === null || payload === void 0 ? void 0 : (_payload$error = payload.error) === null || _payload$error === void 0 ? void 0 : _payload$error.trim().length) > 0 && payload.status;
}

export var launchAction = function launchAction(dispatch, type, payload) {
  var hasErrorPayload = false;
  var statusCode = 200;
  forEach(Object.keys(payload), function (key) {
    if (hasError(payload[key])) {
      hasErrorPayload = true;
      statusCode = payload[key].status;
    }
  });

  if (hasErrorPayload) {
    dispatch({
      type: actions.LAUNCH_ERROR,
      payload: {
        hasError: true,
        code: statusCode
      }
    });
  }

  dispatch({
    type: type,
    payload: payload
  });
};