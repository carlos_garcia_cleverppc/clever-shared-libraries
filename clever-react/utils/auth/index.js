import queryString from "query-string";
import { getStorageInstance } from "../storage";
export var AUTH_TOKEN = "AUTH_TOKEN";
var storage = getStorageInstance();

function getFromStorage() {
  var token = storage.getItem(AUTH_TOKEN);
  return token;
}

function getFromUrl() {
  var _queryString$parse = queryString.parse(window.location.search),
      jwt = _queryString$parse.jwt;

  return jwt;
}

function hasTokenInStorage() {
  var token = storage.getItem(AUTH_TOKEN);
  return !!token && token.trim().length > 0;
}

function hasTokenInUrl() {
  var _queryString$parse2 = queryString.parse(window.location.search),
      jwt = _queryString$parse2.jwt;

  return !!jwt && jwt.trim().length > 0;
}

export var getAuthToken = function getAuthToken() {
  if (hasTokenInStorage()) return getFromStorage();
  if (hasTokenInUrl()) return getFromUrl();
  return false;
};
export var saveAuthToken = function saveAuthToken(token) {
  if (storage.isSupported()) {
    storage.setItem(AUTH_TOKEN, token);
  }
};
export var deleteAuthToken = function deleteAuthToken() {
  storage.setItem(AUTH_TOKEN, "");
};
export var hasAuthToken = function hasAuthToken() {
  return hasTokenInStorage() || hasTokenInUrl();
};