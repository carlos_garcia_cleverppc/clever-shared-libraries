import _defineProperty from "@babel/runtime/helpers/defineProperty";

/* eslint-disable no-param-reassign */
import camelCase from "lodash-es/camelCase";
export var isValidString = function isValidString(candidateString) {
  return (candidateString === null || candidateString === void 0 ? void 0 : candidateString.trim().length) > 0;
};
export var isValidUrl = function isValidUrl(urlString) {
  if (!isValidString(urlString)) return false;
  urlString = urlString.toLowerCase();
  if (!/^(?:http(s)?:\/\/)?[\w.-]+(?:.[\w.-]+)+[\w\-._~:/?#[\]@!$&'()*+,;=.]+$/.test(urlString)) return false;
  if (urlString.indexOf("youtube.") !== -1) return false;
  if (urlString.indexOf("youtu.") !== -1) return false;
  if (urlString.indexOf("twitter.") !== -1) return false;
  if (urlString.indexOf("facebook.") !== -1) return false;
  if (urlString.indexOf("google.") !== -1) return false;
  return true;
};
export var isValidEmail = function isValidEmail(email) {
  return isValidString(email) && /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+.)+[^<>()[\].,;:\s@"]{2,})$/i.test(email);
};
export var handleCheckboxChange = function handleCheckboxChange(name) {
  return function (setState, event) {
    setState(_defineProperty({}, name, event.target.checked));
  };
};
export var handleTextChange = function handleTextChange(name) {
  return function (setState, event) {
    setState(_defineProperty({}, name, event.target.value));
  };
};
export var isObject = function isObject(o) {
  return o instanceof Object && o.constructor === Object;
};
export var resolveCamelCaseObject = function resolveCamelCaseObject(object) {
  if (isObject(object)) {
    var newObject = {};
    Object.keys(object).forEach(function (key) {
      newObject[camelCase(key)] = resolveCamelCaseObject(object[key]);
    });
    return newObject;
  }

  if (object instanceof Array) {
    return object.map(function (value) {
      return resolveCamelCaseObject(value);
    });
  }

  return object;
};
export var onKeyDown = function onKeyDown(action) {
  return function (ev) {
    var key = ev.key,
        keyCode = ev.keyCode;

    if (key === "Enter" || keyCode === 32) {
      ev.preventDefault();
      action();
    }
  };
};
export default {
  isValidString: isValidString,
  isValidUrl: isValidUrl,
  handleCheckboxChange: handleCheckboxChange,
  handleTextChange: handleTextChange,
  isObject: isObject,
  resolveCamelCaseObject: resolveCamelCaseObject,
  onKeyDown: onKeyDown
};