import { useCookies as useCookiesLib, withCookies as withCookiesLib } from "react-cookie";
export var useCookies = useCookiesLib;
export var withCookies = withCookiesLib;
export default useCookies;