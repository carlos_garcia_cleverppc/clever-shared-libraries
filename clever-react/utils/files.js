// eslint-disable-next-line no-restricted-properties
export var bytesToMb = function bytesToMb(bytes) {
  return bytes / Math.pow(1024, 2);
};
export var getBase64Image = function getBase64Image(file) {
  return new Promise(function (resolve, reject) {
    var reader = new FileReader(file);

    reader.onload = function (e) {
      resolve(e.target.result);
    };

    reader.onError = function (e) {
      reject(e);
    };

    try {
      reader.readAsDataURL(file);
    } catch (e) {
      reject(e);
    }
  });
};
export var getBase64MbSize = function getBase64MbSize(base64) {
  var strFormated = base64.split(",")[1].split("=")[0];
  var strLength = strFormated.length;
  var bytes = strLength - strLength / 8 * 2;
  return bytesToMb(bytes);
};