import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable no-console */
import ReactGA from "react-ga";
import { getStorageInstance } from "../storage";
var storage = getStorageInstance();
var config = {
  isSandbox: true,
  isInit: false,
  id: "",
  options: {}
};
export var init = function init(_ref) {
  var options = _ref.options,
      isSandbox = _ref.isSandbox,
      id = _ref.id;

  if (!config.isInit) {
    config = _objectSpread(_objectSpread({}, config), {}, {
      isSandbox: isSandbox,
      options: options,
      id: id,
      isInit: true
    });
  }
};

function getLastEvent(category) {
  var auxEvent = storage.getItem("LAST_ANALYTICS_".concat(category));

  if (!auxEvent) {
    storage.setItem("LAST_ANALYTICS_".concat(category), "");
    return "no-event";
  }

  return auxEvent;
}

function updateLastEvent(category, eventName) {
  storage.setItem("LAST_ANALYTICS_".concat(category), eventName);
}

var genericHandler = function genericHandler(category) {
  return function (action) {
    var label = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    var lastEvent = getLastEvent(category);

    if (lastEvent && lastEvent.toLowerCase() !== action.toLowerCase()) {
      updateLastEvent(category, action);

      if (!config.isSandbox) {
        if (!label) {
          ReactGA.event({
            category: category,
            action: action
          });
        } else {
          ReactGA.event({
            category: category,
            action: action,
            label: label
          });
        }
      } else {
        // eslint-disable-next-line no-console
        console.log("SANDBOX: Google Track --> In ".concat(config.id, " Category: ").concat(category, ", Action: ").concat(action));
      }
    }
  };
};

var conversion = function conversion(action, label, value) {
  var force = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
  var lastEvent = getLastEvent("Conversion");

  if (force || lastEvent && lastEvent.toLowerCase() !== action.toLowerCase()) {
    updateLastEvent("Conversion", action);

    if (!config.isSandbox) {
      if (!label) {
        ReactGA.event({
          category: "Conversion",
          action: action,
          value: value
        });
      } else {
        ReactGA.event({
          category: "Conversion",
          action: action,
          value: value,
          label: label
        });
      }
    } else {
      console.log("SANDBOX: Google Track --> In ".concat(config.id, " Category: Conversion, Action: ").concat(action, ", Value: ").concat(value));
    }
  }
};

export default {
  pageView: genericHandler("PageView"),
  event: genericHandler("Event"),
  label: genericHandler("Label"),
  conversion: conversion
};