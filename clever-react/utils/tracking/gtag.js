/* eslint-disable no-console */
import { getEnvConfig } from "../../ui/Provider/themeConfig";
var gtagConfig = getEnvConfig.gtag;

var event = function event(type, name, data) {
  if (!gtagConfig.isSandbox && !!window.gtag) {
    if (window.gtag) {
      window.gtag(type, name, data);
    }
  } else {
    console.log("SANDBOX: Gtag --> In ".concat(gtagConfig.id, " Type: ").concat(type, ", Name: ").concat(name, ", Data: ").concat(JSON.stringify(data)));
  }
};

export default {
  event: event
};