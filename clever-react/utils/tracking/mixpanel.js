import _defineProperty from "@babel/runtime/helpers/defineProperty";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable no-console */
import mixpanel from "mixpanel-browser";
import { getStorageInstance } from "../storage";
var storage = getStorageInstance();
var config = {
  isSandbox: true,
  isInit: false,
  id: "",
  options: {}
};
export var init = function init(_ref) {
  var options = _ref.options,
      isSandbox = _ref.isSandbox,
      id = _ref.id;

  if (!config.isInit) {
    config = _objectSpread(_objectSpread({}, config), {}, {
      isSandbox: isSandbox,
      options: options,
      id: id,
      isInit: true
    });
  }
};

function getLastEvent(category) {
  var auxEvent = storage.getItem("LAST_MIXPANEL_".concat(category));

  if (!auxEvent) {
    storage.setItem("LAST_MIXPANEL_".concat(category), "");
    return "no-event";
  }

  return auxEvent;
}

function updateLastEvent(category, eventName) {
  storage.setItem("LAST_MIXPANEL_".concat(category), eventName);
}

var identify = function identify(userId) {
  return mixpanel.identify(userId);
};

var genericHandler = function genericHandler(category) {
  return function (eventName, userId, platform, userEmail) {
    var lastEvent = getLastEvent(category);

    if (lastEvent && lastEvent.toLowerCase() !== eventName.toLowerCase()) {
      updateLastEvent(category, eventName);

      if (!config.isSandbox) {
        mixpanel.people.set({
          $email: userEmail
        });
        mixpanel.track(eventName, {
          platform: platform
        });
      } else {
        console.log("SANDBOX: Mixpanel Track --> In ".concat(config.id, " Category: ").concat(category, ", Action: ").concat(eventName));
      }
    }
  };
};

var conversion = function conversion(action, value) {
  var lastEvent = getLastEvent("conversion");

  if (lastEvent && lastEvent.toLowerCase() !== action.toLowerCase()) {
    updateLastEvent("conversion", action);

    if (!config.isSandbox) {
      mixpanel.event({
        category: "Conversion",
        action: action,
        value: value
      });
    } else {
      console.log("SANDBOX: Google Track --> In ".concat(config.id, " Category: Conversion, Action: ").concat(action));
    }
  }
};

export default {
  pageView: genericHandler("PageView"),
  event: genericHandler("Event"),
  label: genericHandler("Label"),
  conversion: conversion,
  identify: identify
};