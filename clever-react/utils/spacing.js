import forEach from "lodash-es/forEach";
import range from "lodash-es/range";
export default (function (calculateSpace, unit, spacersSize) {
  var spacersProps = {
    margin: "m",
    padding: "p"
  };
  var sizesProps = {
    height: "h",
    width: "w"
  };
  var sizes = {
    0: 0,
    10: "10%",
    20: "20%",
    25: "25%",
    30: "30%",
    40: "40%",
    50: "50%",
    60: "60%",
    70: "70%",
    75: "75%",
    80: "80%",
    90: "90%",
    100: "100%",
    auto: "auto"
  };
  var styles = {
    "m-auto": {
      margin: "auto"
    },
    "mt-auto": {
      marginTop: "auto"
    },
    "mr-auto": {
      marginRight: "auto"
    },
    "mb-auto": {
      marginBottom: "auto"
    },
    "ml-auto": {
      marginLeft: "auto"
    },
    "my-auto": {
      marginTop: "auto",
      marginBottom: "auto"
    },
    "mx-auto": {
      marginLeft: "auto",
      marginRight: "auto"
    }
  };
  forEach(range(spacersSize), function (key) {
    forEach(Object.keys(spacersProps), function (prop) {
      styles["".concat(spacersProps[prop], "-").concat(key)] = {};
      styles["".concat(spacersProps[prop], "t-").concat(key)] = {};
      styles["".concat(spacersProps[prop], "r-").concat(key)] = {};
      styles["".concat(spacersProps[prop], "b-").concat(key)] = {};
      styles["".concat(spacersProps[prop], "l-").concat(key)] = {};
      styles["".concat(spacersProps[prop], "x-").concat(key)] = {};
      styles["".concat(spacersProps[prop], "y-").concat(key)] = {};
      styles["".concat(spacersProps[prop], "-").concat(key)]["".concat(prop)] = "".concat(calculateSpace(key)).concat(unit);
      styles["".concat(spacersProps[prop], "t-").concat(key)]["".concat(prop, "Top")] = "".concat(calculateSpace(key)).concat(unit);
      styles["".concat(spacersProps[prop], "r-").concat(key)]["".concat(prop, "Right")] = "".concat(calculateSpace(key)).concat(unit);
      styles["".concat(spacersProps[prop], "b-").concat(key)]["".concat(prop, "Bottom")] = "".concat(calculateSpace(key)).concat(unit);
      styles["".concat(spacersProps[prop], "l-").concat(key)]["".concat(prop, "Left")] = "".concat(calculateSpace(key)).concat(unit);
      styles["".concat(spacersProps[prop], "x-").concat(key)]["".concat(prop, "Left")] = "".concat(calculateSpace(key)).concat(unit);
      styles["".concat(spacersProps[prop], "x-").concat(key)]["".concat(prop, "Right")] = "".concat(calculateSpace(key)).concat(unit);
      styles["".concat(spacersProps[prop], "y-").concat(key)]["".concat(prop, "Top")] = "".concat(calculateSpace(key)).concat(unit);
      styles["".concat(spacersProps[prop], "y-").concat(key)]["".concat(prop, "Bottom")] = "".concat(calculateSpace(key)).concat(unit);

      if (spacersProps[prop] === "m") {
        styles["".concat(spacersProps[prop], "n-").concat(key)] = {};
        styles["".concat(spacersProps[prop], "nt-").concat(key)] = {};
        styles["".concat(spacersProps[prop], "nr-").concat(key)] = {};
        styles["".concat(spacersProps[prop], "nb-").concat(key)] = {};
        styles["".concat(spacersProps[prop], "nl-").concat(key)] = {};
        styles["".concat(spacersProps[prop], "nx-").concat(key)] = {};
        styles["".concat(spacersProps[prop], "ny-").concat(key)] = {};
        styles["".concat(spacersProps[prop], "n-").concat(key)]["".concat(prop)] = "-".concat(calculateSpace(key)).concat(unit);
        styles["".concat(spacersProps[prop], "nt-").concat(key)]["".concat(prop, "Top")] = "-".concat(calculateSpace(key)).concat(unit);
        styles["".concat(spacersProps[prop], "nr-").concat(key)]["".concat(prop, "Right")] = "-".concat(calculateSpace(key)).concat(unit);
        styles["".concat(spacersProps[prop], "nb-").concat(key)]["".concat(prop, "Bottom")] = "-".concat(calculateSpace(key)).concat(unit);
        styles["".concat(spacersProps[prop], "nl-").concat(key)]["".concat(prop, "Left")] = "-".concat(calculateSpace(key)).concat(unit);
        styles["".concat(spacersProps[prop], "nx-").concat(key)]["".concat(prop, "Left")] = "-".concat(calculateSpace(key)).concat(unit);
        styles["".concat(spacersProps[prop], "nx-").concat(key)]["".concat(prop, "Right")] = "-".concat(calculateSpace(key)).concat(unit);
        styles["".concat(spacersProps[prop], "ny-").concat(key)]["".concat(prop, "Top")] = "-".concat(calculateSpace(key)).concat(unit);
        styles["".concat(spacersProps[prop], "ny-").concat(key)]["".concat(prop, "Bottom")] = "-".concat(calculateSpace(key)).concat(unit);
      }
    });
  });
  forEach(Object.keys(sizes), function (key) {
    forEach(Object.keys(sizesProps), function (prop) {
      styles["".concat(sizesProps[prop], "-").concat(key)] = {};
      styles["".concat(sizesProps[prop], "-").concat(key)]["".concat(prop)] = sizes[key];
    });
  });
  return styles;
});